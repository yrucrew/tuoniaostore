package com.tuoniaostore.good.cache.impl;

import com.tuoniaostore.datamodel.good.GoodImage;
import com.tuoniaostore.good.cache.GoodImageCache;
import com.tuoniaostore.good.data.GoodImageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@CacheConfig(cacheNames = "goodImage")
public class GoodImageCacheImpl implements GoodImageCache {

    @Autowired
    GoodImageMapper mapper;

    @Override
    @CacheEvict(value = "goodImage", allEntries = true)
    public void addGoodImage(GoodImage goodImage) {
        mapper.addGoodImage(goodImage);
    }

    @Override
    @Cacheable(value = "goodImage", key = "'getGoodImage_'+#id")
    public GoodImage getGoodImage(String id) {
        return mapper.getGoodImage(id);
    }

    @Override
    @Cacheable(value = "goodImage", key = "'getGoodImages_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#title+'_'+#goodTemplateId")
    public List<GoodImage> getGoodImages(int status, int pageIndex, int pageSize, String title, String goodTemplateId) {
        return mapper.getGoodImages(status, pageIndex, pageSize, title, goodTemplateId);
    }

    @Override
    @Cacheable(value = "goodImage", key = "'getGoodImageAll'")
    public List<GoodImage> getGoodImageAll() {
        return mapper.getGoodImageAll();
    }

    @Override
    @Cacheable(value = "goodImage", key = "'getGoodImageCount_'+#status+'_'+#title+'_'+#goodTemplateId")
    public int getGoodImageCount(int status, String title, String goodTemplateId) {
        return mapper.getGoodImageCount(status, title, goodTemplateId);
    }

    @Override
    @CacheEvict(value = "goodImage", allEntries = true)
    public void changeGoodImage(GoodImage goodImage) {
        mapper.changeGoodImage(goodImage);
    }

    @Override
    @Cacheable(value = "goodImage", key = "'getGoodImageGoodIdCount_'+#goodTemplateId+'_'+#retrieveStatus")
    public int getGoodImageGoodIdCount(String goodTemplateId, int retrieveStatus) {
        return mapper.getGoodImageGoodIdCount(goodTemplateId,retrieveStatus);
    }
}
