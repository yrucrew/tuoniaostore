package com.tuoniaostore.good.cache.impl;

import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.datamodel.good.GoodPrice;
import com.tuoniaostore.good.cache.GoodPricePosCache;
import com.tuoniaostore.good.cache.remote.GoodsDataCacheService;
import com.tuoniaostore.good.data.GoodPricePosMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@CacheConfig(cacheNames = "goodPricePos")
public class GoodPricePosCacheImpl  implements GoodPricePosCache {

    @Autowired
    GoodPricePosMapper mapper;

    @Autowired
    GoodsDataCacheService dataCacheService;

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodPricePos", allEntries = true),
            @CacheEvict(value = "goodBarcode", allEntries = true),
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodType", allEntries = true),
            @CacheEvict(value = "goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void addGoodPricePos(GoodPrice goodPrice) {
        mapper.addGoodPricePos(goodPrice);
        dataCacheService.loaderGoodPrice();
    }

    @Override
    @Cacheable(value = "goodPricePos", key = "'getGoodPricePos_'+#id")
    public   GoodPrice getGoodPricePos(String id) {
        return mapper.getGoodPricePos(id);
    }

    @Override
    @Cacheable(value = "goodPricePos", key = "'getGoodPricePoss_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#title+'_'+#goodId")
    public List<GoodPrice> getGoodPricePoss(int status, int pageIndex, int pageSize, String title, String goodId) {
        return mapper.getGoodPricePoss(status,pageIndex, pageSize,  title,goodId);
    }

    @Override
    @Cacheable(value = "goodPricePos", key = "'getGoodPricePosByGoodIdAll'+'_'+#goodId")
    public List<GoodPrice> getGoodPricePosByGoodIdAll(String goodId) {
        return mapper.getGoodPricePosByGoodIdAll(goodId);
    }

    @Override
    @Cacheable(value = "goodPricePos", key = "'getGoodPricePosByGoodIdCount'+'_'+#goodId")
    public int getGoodPricePosByGoodIdCount(String goodId) {
        return mapper.getGoodPricePosByGoodIdCount(goodId);
    }

    @Override
    @Cacheable(value = "goodPricePos", key = "'getGoodPricePosByTitle'+'_'+#title")
    public GoodPrice getGoodPricePosByTitle(String title) {
        return mapper.getGoodPricePosByTitle(title);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodPricePos", allEntries = true),
            @CacheEvict(value = "goodBarcode", allEntries = true),
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodType", allEntries = true),
            @CacheEvict(value = "goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void updateGoodPricePos(String id, String title, String unitId, String goodId) {
        mapper.updateGoodPricePos(id,title,unitId,goodId);
        dataCacheService.loaderGoodPrice();
    }

    @Override
    @Cacheable(value = "goodPricePos", key = "'getAllGoodPriceByTitleAndGoodId_'+#title+'_'+#goodId")
    public GoodPrice getAllGoodPriceByTitleAndGoodId(String title, String goodId) {
        return mapper.getAllGoodPriceByTitleAndGoodId(title,goodId);
    }

    @Override
    @Cacheable(value = "goodPrice_goodPricePos_goodUnit", key = "'getPriceTitleAndUnitTitleByPriceIds_'+#ids")
    public List<GoodPriceCombinationVO> getPriceTitleAndUnitTitleByPriceIds(List<String> ids) {
        return mapper.getPriceTitleAndUnitTitleByPriceIds(ids);
    }

    @Override
    @Cacheable(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", key = "'getAllGoodTemplateByPriceIds_'+#ids+'_'+#nameOrBarcode+'_'+#pageIndex+'_'+#pageSize")
    public List<GoodPriceCombinationVO> getAllGoodTemplateByPriceIds(List<String> ids,String nameOrBarcode,Integer pageIndex, Integer pageSize) {
        return mapper.getAllGoodTemplateByPriceIds(ids,nameOrBarcode,pageIndex,pageSize);
    }

    @Override
    @Cacheable(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", key = "'getAllGoodTemplateByPriceIdsCount_'+#ids+'_'+#nameOrBarcode")
    public int getAllGoodTemplateByPriceIdsCount(List<String> ids, String nameOrBarcode) {
        return mapper.getAllGoodTemplateByPriceIdsCount(ids,nameOrBarcode);
    }

    @Override
    @Cacheable(value = "goodPricePos", key = "'getGoodPricePosAll'")
    public List<GoodPrice> getGoodPricePosAll() {
        return mapper.getGoodPricePosAll();
    }

    @Override
    @Cacheable(value = "goodPricePos", key = "'getGoodPricePosTop15'+#goodId")
    public List<GoodPrice> getGoodPricePosTop15(String goodId) {
        return mapper.getGoodPricePosTop15(goodId);
    }

    @Override
    @Cacheable(value = "goodPricePos", key = "'getGoodPricePosCount_'+#status+'_'+#title+'_'+#goodId")
    public int getGoodPricePosCount(int status, String title,String goodId) {
        return mapper.getGoodPricePosCount(status, title,goodId);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodPricePos", allEntries = true),
            @CacheEvict(value = "goodBarcode", allEntries = true),
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodType", allEntries = true),
            @CacheEvict(value = "goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void changeGoodPricePos(GoodPrice goodPrice) {
        mapper.changeGoodPricePos(goodPrice);
        dataCacheService.loaderGoodPrice();
    }

}
