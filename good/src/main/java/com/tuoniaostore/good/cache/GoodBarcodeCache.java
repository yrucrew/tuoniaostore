package com.tuoniaostore.good.cache;

import com.tuoniaostore.datamodel.good.GoodBarcode;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodBarcodeCache {

    void addGoodBarcode(GoodBarcode goodBarcode);

    GoodBarcode getGoodBarcode(String id);

    List<GoodBarcode> getGoodBarcodes(int pageIndex, int pageSize, int status, String name);

    List<GoodBarcode> getGoodBarcodeAll();

    int getGoodBarcodeCount(int status, String name);

    void changeGoodBarcode(GoodBarcode goodBarcode);

    GoodBarcode getGoodBarcodeByPriceId(String priceId);

    GoodBarcode getGoodBarcodeByBarcode(String barcode);

    List<GoodBarcode> getBarcodesByPriceId(String priceId);

    List<GoodBarcode> getGoodBarcodeByBarcodeLike(String barcode);

    void batchAddGoodBarcode(List<GoodBarcode> barcodes);

    List<GoodBarcode> getGoodBarcodeByPriceIds(List<String> s);

    List<GoodBarcode> getGoodBarcodeByBarcodes(List<String> list);

    List<GoodBarcode> getGoodBarcodesByPriceId(String goodPrice);

    void updateGoodBarcodeParam(Map<String, Object> map);
}
