package com.tuoniaostore.good.cache;
import com.tuoniaostore.datamodel.good.GoodTemplatePropertiesValue;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodTemplatePropertiesValueCache {

    void addGoodTemplatePropertiesValue(GoodTemplatePropertiesValue goodTemplatePropertiesValue);

    GoodTemplatePropertiesValue getGoodTemplatePropertiesValue(String id);

    List<GoodTemplatePropertiesValue> getGoodTemplatePropertiesValues(int pageIndex, int pageSize, int status, String name);
    List<GoodTemplatePropertiesValue> getGoodTemplatePropertiesValueAll();
    int getGoodTemplatePropertiesValueCount(int status, String name);
    void changeGoodTemplatePropertiesValue(GoodTemplatePropertiesValue goodTemplatePropertiesValue);

}
