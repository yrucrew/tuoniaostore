package com.tuoniaostore.good.cache.impl;

import com.tuoniaostore.commons.constant.payment.RelationTransTypeEnum;
import com.tuoniaostore.commons.constant.user.RetrieveStatusEnum;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.datamodel.good.GoodBrand;
import com.tuoniaostore.datamodel.good.GoodPrice;
import com.tuoniaostore.datamodel.good.GoodTemplate;
import com.tuoniaostore.datamodel.good.GoodType;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.cache.GoodTemplateCache;
import com.tuoniaostore.good.cache.remote.GoodsDataCacheService;
import com.tuoniaostore.good.data.GoodTemplateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@CacheConfig(cacheNames = "goodTemplate")
public class GoodTemplateCacheImpl implements GoodTemplateCache {

    @Autowired
    GoodTemplateMapper mapper;
    @Autowired
    private GoodDataAccessManager dataAccessManager;
    @Autowired
    GoodsDataCacheService dataCacheService;


    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodBrand_goodTemplate_goodTemplatePos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void addGoodTemplate(GoodTemplate goodTemplate) {
        mapper.addGoodTemplate(goodTemplate);
        // dataCacheService.loaderGoodTemplate();
    }

    @Override
    @Cacheable(value = "goodTemplate", key = "'getGoodTemplate_'+#id")
    public GoodTemplate getGoodTemplate(String id) {
        GoodTemplate goodTemplate = dataCacheService.getGoodTemplate(id);
        if (goodTemplate != null) {
            return goodTemplate;
        } else {
            return mapper.getGoodTemplate(id);
        }
    }

    @Override
    @Cacheable(value = "goodTemplate", key = "'getGoodTemplates_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name+'_'+#retrieveStatus")
    public List<GoodTemplate> getGoodTemplates(int status, int pageIndex, int pageSize, String name, int retrieveStatus, HttpServletRequest request) {
        List<GoodTemplate> goodTemplates = mapper.getGoodTemplates(status, pageIndex, pageSize, name, retrieveStatus);
//        fillGoodTemplate(goodTemplates, request);
        return goodTemplates;
    }

    @Override
    @Cacheable(value = "goodTemplate", key = "'getGoodTemplateAll'")
    public List<GoodTemplate> getGoodTemplateAll(HttpServletRequest request) {
        List<GoodTemplate> goodTemplates = mapper.getGoodTemplateAll();
      //  fillGoodTemplate(goodTemplates, request);
        return goodTemplates;
    }

    @Override
    @Cacheable(value = "GoodTemplate", key = "'getGoodTemplateAllByPage_'+#pageStartIndex+#pageSize")
    public List<GoodTemplate> getGoodTemplateAllByPage(Integer pageStartIndex, Integer pageSize) {
        return mapper.getGoodTemplateAllByPage(pageStartIndex, pageSize);
    }

    @Override
    public List<GoodTemplate> loaderGoodTemplate() {
        return mapper.loaderGoodTemplate();
    }

    @Override
    @Cacheable(value = "goodTemplate", key = "'getGoodTemplateCount_'+#status+'_'+#name+'_'+#retrieveStatus")
    public int getGoodTemplateCount(int status, String name, int retrieveStatus) {
        return mapper.getGoodTemplateCount(status, name, retrieveStatus);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodBrand_goodTemplate_goodTemplatePos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void changeGoodTemplate(GoodTemplate goodTemplate) {
        mapper.changeGoodTemplate(goodTemplate);
        dataCacheService.loaderGoodTemplate();
    }

    @Override
    @Cacheable(value = "goodTemplate", key = "'getGoodTemplateByName_'+#goodName+'_'+#ids")
    public List<GoodTemplate> getGoodTemplateByName(String goodName, List<String> ids) {
        return mapper.getGoodTemplateByName(goodName, ids);
    }

    @Override
    @Cacheable(value = "goodTemplate", key = "'getGoodtemplateByBrandId_'+#brandId")
    public List<GoodTemplate> getGoodtemplateByBrandId(String brandId) {
        return mapper.getGoodtemplateByBrandId(brandId);
    }

    @Override
    @Cacheable(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", key = "'listGoodTemplatesForAddChainGood'")
    public List<GoodTemplate> listGoodTemplatesForAddChainGood(List<String> lists) {
        return mapper.listGoodTemplatesForAddChainGood(lists);
    }


    private void fillGoodTemplate(List<GoodTemplate> goodTemplates, HttpServletRequest request) {
        int size = goodTemplates.size();
        for (int i = 0; i < size; i++) {
            try {

                SysUser sysUser = SysUserRmoteService.getSysUser(goodTemplates.get(i).getUserId(), request);
                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        goodTemplates.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        goodTemplates.get(i).setUserName(sysUser.getShowName());
                    } else {
                        goodTemplates.get(i).setUserName(sysUser.getDefaultName());
                    }
                }
                if (goodTemplates.get(i).getBrandId() != null) {
                    GoodBrand brand = dataAccessManager.getGoodBrand(goodTemplates.get(i).getBrandId());
                    if (brand != null) {
                        goodTemplates.get(i).setBrandName(brand.getName());
                    }
                }
                String typeId = goodTemplates.get(i).getTypeId();
                GoodType type = null;
                if (typeId != null && !typeId.equals("")) {
                    type = dataAccessManager.getGoodType(typeId);
                }
                if (type != null) {
                    goodTemplates.get(i).setTypeName(type.getTitle());
                }
                int imageCount = dataAccessManager.getGoodImageGoodIdCount(goodTemplates.get(i).getId());
                goodTemplates.get(i).setImageCount(imageCount);
                int labelCount = dataAccessManager.getGoodPriceByGoodIdCount(goodTemplates.get(i).getId());
                goodTemplates.get(i).setLableCount(labelCount);
                goodTemplates.get(i).setLableContent(getPriceTop15(goodTemplates.get(i).getId()));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String getPriceTop15(String goodId) {
        List<GoodPrice> goodPrices = dataAccessManager.getGoodPriceTop15(goodId);
        int size = goodPrices.size();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < size; i++) {
            if (i < size - 1) {
                sb.append(goodPrices.get(i).getTitle() + ",");
            } else {
                sb.append(goodPrices.get(i).getTitle());
            }
        }
        return sb.toString();
    }

    @Override
    @Cacheable(value = "goodTemplate", key = "'getGoodTemplateisRepetition_'+#name")
    public GoodTemplate getGoodTemplateisRepetition(String name) {
        return mapper.getGoodTemplateisRepetition(name);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodBrand_goodTemplate_goodTemplatePos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void deleteGoodTemplate(List<String> idList) {
        mapper.deleteGoodTemplate(idList);
        dataCacheService.loaderGoodTemplate();
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodBrand_goodTemplate_goodTemplatePos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void updateGoodTemplateByParam(Map<String, Object> map) {
        mapper.updateGoodTemplateByParam(map);
        dataCacheService.loaderGoodTemplate();
    }

    @Override
    @Cacheable(value = "goodTemplate", key = "'getGoodTemplatesByGoodNameLikeAndGoodStatus_'+#goodName+'_'+#goodStatus+'_'+#retrieveStatus")
    public List<GoodTemplate> getGoodTemplatesByGoodNameLikeAndGoodStatus(String goodName, Integer goodStatus, int retrieveStatus) {
        return mapper.getGoodTemplatesByGoodNameLikeAndGoodStatus(goodName, goodStatus, retrieveStatus);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodBrand_goodTemplate_goodTemplatePos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void batchAddGoodTemplate(List<GoodTemplate> goodTemplateList) {
        mapper.batchAddGoodTemplate(goodTemplateList);
        dataCacheService.loaderGoodTemplate();
    }
}
