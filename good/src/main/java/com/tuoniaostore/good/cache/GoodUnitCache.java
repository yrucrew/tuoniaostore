package com.tuoniaostore.good.cache;
import com.tuoniaostore.commons.http.HttpRequest;
import com.tuoniaostore.datamodel.good.GoodType;
import com.tuoniaostore.datamodel.good.GoodUnit;
import com.tuoniaostore.datamodel.user.SysUser;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodUnitCache {

    void addGoodUnit(GoodUnit goodUnit);

    GoodUnit getGoodUnit(String id);

    List<GoodUnit> getGoodUnits(int pageIndex, int pageSize, int status, String  title,int retrieveStatus,HttpServletRequest request);
    List<GoodUnit> getGoodUnitAll(HttpServletRequest request);
    int getGoodUnitCount(int status, String  title,int retrieveStatus);
    void changeGoodUnit(GoodUnit goodUnit);
    List<GoodUnit> loaderGoodsUnits();
    GoodUnit getGoodUnitByTitle(String title);
    List<GoodUnit> getGoodUnitisRepetition(Map<String,Object> map);

    void deleteGoodUnit(List<String> ids);
}
