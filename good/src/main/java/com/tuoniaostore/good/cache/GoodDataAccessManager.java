package com.tuoniaostore.good.cache;

import com.tuoniaostore.commons.constant.user.RetrieveStatusEnum;
import com.tuoniaostore.commons.http.HttpRequest;
import com.tuoniaostore.datamodel.good.*;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.datamodel.vo.good.GoodTypeOne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
public class GoodDataAccessManager {


    @Autowired
    GoodBarcodeCache goodBarcodeCache;

    public void addGoodBarcode(GoodBarcode goodBarcode) {
        goodBarcodeCache.addGoodBarcode(goodBarcode);
    }

    public GoodBarcode getGoodBarcodeByPriceId(String priceId) {
        return goodBarcodeCache.getGoodBarcodeByPriceId(priceId);
    }

    public GoodBarcode getGoodBarcode(String id) {
        return goodBarcodeCache.getGoodBarcode(id);
    }

    public List<GoodBarcode> getGoodBarcodes(int status, int pageIndex, int pageSize, String name) {
        return goodBarcodeCache.getGoodBarcodes(status, pageIndex, pageSize, name);
    }

    public List<GoodBarcode> getGoodBarcodeAll() {
        return goodBarcodeCache.getGoodBarcodeAll();
    }

    public int getGoodBarcodeCount(int status, String name) {
        return goodBarcodeCache.getGoodBarcodeCount(status, name);
    }

    public void changeGoodBarcode(GoodBarcode goodBarcode) {
        goodBarcodeCache.changeGoodBarcode(goodBarcode);
    }

    public GoodBarcode getGoodBarcodeByBarcode(String barcode) {
        return goodBarcodeCache.getGoodBarcodeByBarcode(barcode);
    }

    public List<GoodBarcode> getBarcodesByPriceId(String priceId) {
        return goodBarcodeCache.getBarcodesByPriceId(priceId);
    }

    public List<GoodBarcode> getGoodBarcodeByBarcodeLike(String barcode) {
        return goodBarcodeCache.getGoodBarcodeByBarcodeLike(barcode);
    }

    public void batchAddGoodBarcode(List<GoodBarcode> barcodes) {
        goodBarcodeCache.batchAddGoodBarcode(barcodes);
    }

    public List<GoodBarcode> getGoodBarcodeByPriceIds(List<String> s) {
        return goodBarcodeCache.getGoodBarcodeByPriceIds(s);
    }

    public List<GoodBarcode> getGoodBarcodeByBarcodes(List<String> list) {
        return goodBarcodeCache.getGoodBarcodeByBarcodes(list);
    }

    public List<GoodBarcode> getGoodBarcodesByPriceId(String goodPrice) {
        return goodBarcodeCache.getGoodBarcodesByPriceId(goodPrice);
    }

    public void updateGoodBarcodeParam(Map<String, Object> map) {
        goodBarcodeCache.updateGoodBarcodeParam(map);
    }

    @Autowired
    GoodBrandCache goodBrandCache;

    public void addGoodBrand(GoodBrand goodBrand) {
        goodBrandCache.addGoodBrand(goodBrand);
    }

    public GoodBrand getGoodBrand(String id) {
        return goodBrandCache.getGoodBrand(id);
    }

    public GoodBrand getGoodBrandForDelete(String id) {
        return goodBrandCache.getGoodBrandForDelete(id);
    }

    public List<GoodBrand> getGoodBrands(int status, int pageIndex, int pageSize, String name) {
        return goodBrandCache.getGoodBrands(status, pageIndex, pageSize, name);
    }

    public List<GoodBrand> getGoodBrandAll() {
        return goodBrandCache.getGoodBrandAll();
    }

    public List<GoodBrand> loaderGoodBrand() {
        return goodBrandCache.loaderGoodBrand();
    }

    public int getGoodBrandCount(int status, String name) {
        return goodBrandCache.getGoodBrandCount(status, name);
    }

    public void changeGoodBrand(GoodBrand goodBrand) {
        goodBrandCache.changeGoodBrand(goodBrand);
    }

    public List<GoodBrand> getGoodBranByIds(List<String> goodBrands, String name) {
        return goodBrandCache.getGoodBranByIds(goodBrands, name);
    }

    public List<String> getGoodTemplateIdsByBrandName(String goodBrandName) {
        return goodBrandCache.getGoodTemplateIdsByBrandName(goodBrandName);
    }

    public List<GoodBrand> getBrandByBrandNameAndStatus(String keyWord, int status) {
        return goodBrandCache.getBrandByBrandNameAndStatus(keyWord, status);
    }

    public void deleteGoodBrand(List<String> id) {
        goodBrandCache.deleteGoodBrand(id);
    }

    public List<GoodBrand> getGoodBrandsByPage(Integer pageStartIndex, Integer pageSize) {
        return goodBrandCache.getGoodBrandsByPage(pageStartIndex, pageSize);
    }

    public GoodBrand getGoodBrandisRepetition(String name) {
        return goodBrandCache.getGoodBrandisRepetition(name);
    }

    public void updateGoodBrandByParam(Map<String, Object> map) {
        goodBrandCache.updateGoodBrandByParam(map);
    }

    public void batchAddGoodBrand(List<GoodBrand> goodBrandListd) {
        goodBrandCache.batchAddGoodBrand(goodBrandListd);
    }

    @Autowired
    GoodImageCache goodImageCache;

    public int getGoodImageGoodIdCount(String goodTemplateId) {
        return goodImageCache.getGoodImageGoodIdCount(goodTemplateId, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void addGoodImage(GoodImage goodImage) {
        goodImageCache.addGoodImage(goodImage);
    }

    public GoodImage getGoodImage(String id) {
        return goodImageCache.getGoodImage(id);
    }

    public List<GoodImage> getGoodImages(int status, int pageIndex, int pageSize, String title, String goodTemplateId) {
        return goodImageCache.getGoodImages(status, pageIndex, pageSize, title, goodTemplateId);
    }

    public List<GoodImage> getGoodImageAll() {
        return goodImageCache.getGoodImageAll();
    }

    public int getGoodImageCount(int status, String title, String goodTemplateId) {
        return goodImageCache.getGoodImageCount(status, title, goodTemplateId);
    }

    public void changeGoodImage(GoodImage goodImage) {
        goodImageCache.changeGoodImage(goodImage);
    }


    @Autowired
    GoodPriceCache goodPriceCache;

    /**
     * 获取商品总标签数
     *
     * @param goodId
     * @return
     */
    public List<GoodPrice> getGoodPriceByGoodIdAll(String goodId) {
        return goodPriceCache.getGoodPriceByGoodIdAll(goodId);
    }

    public int getGoodPriceByGoodIdCount(String goodId) {
        return goodPriceCache.getGoodPriceByGoodIdCount(goodId);
    }

    public List<GoodPrice> getGoodPriceTop15(String goodId) {
        return goodPriceCache.getGoodPriceTop15(goodId);
    }

    public void addGoodPrice(GoodPrice goodPrice) {
        goodPriceCache.addGoodPrice(goodPrice);
    }

    public GoodPrice getGoodPrice(String id) {
        return goodPriceCache.getGoodPrice(id);
    }

    public List<GoodPrice> getGoodPrices(int status, int pageIndex, int pageSize, String title, String goodId) {
        return goodPriceCache.getGoodPrices(status, pageIndex, pageSize, title, goodId);
    }

    public List<GoodPrice> getGoodPriceAll() {
        return goodPriceCache.getGoodPriceAll();
    }

    public List<GoodPrice> getGoodPriceAllByPage(List<String> templateIds, List<String> inPriceIds, Integer pageIndex, Integer pageSize) {
        return goodPriceCache.getGoodPriceAllByPage(templateIds, inPriceIds, pageIndex, pageSize);
    }

    public List<GoodPrice> loaderGoodPrice() {
        return goodPriceCache.loaderGoodPrice();
    }

    public int getGoodPriceCount(int status, String title, String goodId) {
        return goodPriceCache.getGoodPriceCount(status, title, goodId);
    }

    public void changeGoodPrice(GoodPrice goodPrice) {
        goodPriceCache.changeGoodPrice(goodPrice);
    }

    public void updateGoodPrice(String id, String title, String unitId, String goodId) {
        goodPriceCache.updateGoodPrice(id, title, unitId, goodId);
    }

    public void batchDeleteGoodPrice(List<String> ids) {
        goodPriceCache.batchDeleteGoodPrice(ids);
    }

    public GoodPrice getGoodPriceisRepetition(String goodId, String title) {
        return goodPriceCache.getGoodPriceisRepetition(goodId, title);
    }

    public void batchAddGoodPrice(List<GoodPrice> list) {
        goodPriceCache.batchAddGoodPrice(list);
    }

    public void updateGoodPriceByParam(Map<String, Object> map) {
        goodPriceCache.updateGoodPriceByParam(map);
    }


    public List<GoodPriceCombinationVO> showAllGoodPriceis(Map<String, Object> map, int pageIndex, Integer pageSize) {
        return goodPriceCache.showAllGoodPriceis(map, pageIndex, pageSize);
    }

    public int showAllCountGoodPriceis(Map<String, Object> map) {
        return goodPriceCache.showAllCountGoodPriceis(map);
    }

    public List<GoodPrice> getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(String priceTitle, Integer priceStatus, List<String> goodTemplates, List<String> barcodePriceId, int pageStartIndex, int pageSize) {
        return goodPriceCache.getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(priceTitle, priceStatus, goodTemplates, barcodePriceId, pageStartIndex, pageSize);
    }

    public List<GoodPrice> getGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(String priceTitle, Integer priceStatus, List<String> goodTemplates, List<String> barcodePriceId, List<String> priceIds, int pageStartIndex, int pageSize) {
        return goodPriceCache.getGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(priceTitle, priceStatus, goodTemplates, barcodePriceId, priceIds, pageStartIndex, pageSize);
    }

    public int getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(String priceTitle, Integer priceStatus, List<String> goodTemplates, List<String> barcodePriceId) {
        return goodPriceCache.getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(priceTitle, priceStatus, goodTemplates, barcodePriceId);
    }

    public int getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(String priceTitle, Integer priceStatus, List<String> goodTemplates, List<String> barcodePriceId, List<String> priceIds) {
        return goodPriceCache.getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(priceTitle, priceStatus, goodTemplates, barcodePriceId, priceIds);
    }

    public List<GoodPrice> getGoodPriceNotAll(List<String> priceIds, List<String> templateIds, List<String> inPriceIds, int pageStartIndex, int pageSize) {
        return goodPriceCache.getGoodPriceNotAll(priceIds, templateIds, inPriceIds, pageStartIndex, pageSize);
    }

    public int getGoodPriceNotAllCount(List<String> priceIds, List<String> templateIds, List<String> inPriceIds) {
        return goodPriceCache.getGoodPriceNotAllCount(priceIds, templateIds, inPriceIds);
    }

    public int getGoodPriceCountAll(List<String> templateIds, List<String> inPriceIds) {
        return goodPriceCache.getGoodPriceCountAll(templateIds, inPriceIds);
    }

    public void batchDeleteGoodPriceByGoodId(List<String> goodIds) {
        goodPriceCache.batchDeleteGoodPriceByGoodId(goodIds);
    }

    public void updateGoodPriceStatusByGoodId(Map<String, Object> map) {
        goodPriceCache.updateGoodPriceStatusByGoodId(map);
    }

    public void changeIconByBarcode(String icon, String id) {
        goodPriceCache.changeIconByBarcode(icon, id);
    }

    @Autowired
    GoodPricePosCache goodPricePosCache;

    /**
     * 获取商品总标签数
     *
     * @param goodId
     * @return
     */
    public List<GoodPrice> getGoodPricePosByGoodIdAll(String goodId) {
        return goodPricePosCache.getGoodPricePosByGoodIdAll(goodId);
    }

    public int getGoodPricePosByGoodIdCount(String goodId) {
        return goodPricePosCache.getGoodPricePosByGoodIdCount(goodId);
    }

    public List<GoodPrice> getGoodPricePosTop15(String goodId) {
        return goodPricePosCache.getGoodPricePosTop15(goodId);
    }

    public void addGoodPricePos(GoodPrice goodPrice) {
        goodPricePosCache.addGoodPricePos(goodPrice);
    }

    public GoodPrice getGoodPricePos(String id) {
        return goodPricePosCache.getGoodPricePos(id);
    }

    public List<GoodPrice> getGoodPricePoss(int status, int pageIndex, int pageSize, String title, String goodId) {
        return goodPricePosCache.getGoodPricePoss(status, pageIndex, pageSize, title, goodId);
    }

    public List<GoodPrice> getGoodPricePosAll() {
        return goodPricePosCache.getGoodPricePosAll();
    }

    public int getGoodPricePosCount(int status, String title, String goodId) {
        return goodPricePosCache.getGoodPricePosCount(status, title, goodId);
    }

    public void changeGoodPricePos(GoodPrice goodPrice) {
        goodPricePosCache.changeGoodPricePos(goodPrice);
    }

    public GoodPrice getGoodPricePosByTitle(String title) {
        return goodPricePosCache.getGoodPricePosByTitle(title);
    }

    public void updateGoodPricePos(String id, String title, String unitId, String goodId) {
        goodPricePosCache.updateGoodPricePos(id, title, unitId, goodId);
    }

    public GoodPrice getAllGoodPriceByTitleAndGoodId(String title, String goodId) {
        return goodPricePosCache.getAllGoodPriceByTitleAndGoodId(title, goodId);
    }

    public List<GoodPriceCombinationVO> getPriceTitleAndUnitTitleByPriceIds(List<String> ids) {
        return goodPricePosCache.getPriceTitleAndUnitTitleByPriceIds(ids);
    }

    //查询s的所有的信息
    public List<GoodPriceCombinationVO> getAllGoodTemplateByPriceIds(List<String> s, String nameOrBarcode, Integer pageIndex, Integer pageSize) {
        return goodPricePosCache.getAllGoodTemplateByPriceIds(s, nameOrBarcode, pageIndex, pageSize);
    }

    public int getAllGoodTemplateByPriceIdsCount(List<String> s, String nameOrBarcode) {
        return goodPricePosCache.getAllGoodTemplateByPriceIdsCount(s, nameOrBarcode);
    }

    @Autowired
    GoodTemplateCache goodTemplateCache;

    public void addGoodTemplate(GoodTemplate goodTemplate) {
        goodTemplateCache.addGoodTemplate(goodTemplate);
    }

    public GoodTemplate getGoodTemplate(String id) {
        return goodTemplateCache.getGoodTemplate(id);
    }

    public List<GoodTemplate> getGoodTemplates(int status, int pageIndex, int pageSize, String name, HttpServletRequest request) {
        return goodTemplateCache.getGoodTemplates(status, pageIndex, pageSize, name, RetrieveStatusEnum.NORMAL.getKey(), request);
    }

    public List<GoodTemplate> getGoodTemplateAll(HttpServletRequest request) {
        return goodTemplateCache.getGoodTemplateAll(request);
    }

    public List<GoodTemplate> getGoodTemplateAllByPage(Integer pageStartIndex, Integer pageSize) {
        return goodTemplateCache.getGoodTemplateAllByPage(pageStartIndex, pageSize);
    }

    public int getGoodTemplateCount(int status, String name) {
        return goodTemplateCache.getGoodTemplateCount(status, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeGoodTemplate(GoodTemplate goodTemplate) {
        goodTemplateCache.changeGoodTemplate(goodTemplate);
    }

    public List<GoodTemplate> getGoodTemplateByName(String goodName, List<String> ids) {
        return goodTemplateCache.getGoodTemplateByName(goodName, ids);
    }

    public List<GoodTemplate> getGoodtemplateByBrandId(String brandId) {
        return goodTemplateCache.getGoodtemplateByBrandId(brandId);
    }

    public List<GoodTemplate> listGoodTemplatesForAddChainGood(List<String> lists) {
        return goodTemplateCache.listGoodTemplatesForAddChainGood(lists);
    }

    public GoodTemplate getGoodTemplateisRepetition(String name) {
        return goodTemplateCache.getGoodTemplateisRepetition(name);
    }

    public void deleteGoodTemplate(List<String> idList) {
        goodTemplateCache.deleteGoodTemplate(idList);
    }

    public void updateGoodTemplateByParam(Map<String, Object> map) {
        goodTemplateCache.updateGoodTemplateByParam(map);
    }

    public List<GoodTemplate> loaderGoodTemplate() {
        return goodTemplateCache.loaderGoodTemplate();
    }

    public List<GoodTemplate> getGoodTemplatesByGoodNameLikeAndGoodStatus(String goodName, Integer goodStatus) {
        return goodTemplateCache.getGoodTemplatesByGoodNameLikeAndGoodStatus(goodName, goodStatus, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void batchAddGoodTemplate(List<GoodTemplate> goodTemplateList) {
        goodTemplateCache.batchAddGoodTemplate(goodTemplateList);
    }

    @Autowired
    GoodTemplatePosCache goodTemplatePosCache;

    public void addGoodTemplatePos(GoodTemplate goodTemplate) {
        goodTemplatePosCache.addGoodTemplatePos(goodTemplate);
    }

    public GoodTemplate getGoodTemplatePos(String id) {
        return goodTemplatePosCache.getGoodTemplatePos(id);
    }

    public List<GoodTemplate> getGoodTemplatePoss(int status, int pageIndex, int pageSize, String name, SysUser sysUser) {
        return goodTemplatePosCache.getGoodTemplatePoss(status, pageIndex, pageSize, name, RetrieveStatusEnum.NORMAL.getKey(), sysUser);
    }

    public List<GoodTemplate> getGoodTemplatePosAll(SysUser sysUser) {
        return goodTemplatePosCache.getGoodTemplatePosAll(sysUser);
    }

    public int getGoodTemplatePosCount(int status, String name) {
        return goodTemplatePosCache.getGoodTemplatePosCount(status, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeGoodTemplatePos(GoodTemplate goodTemplate) {
        goodTemplatePosCache.changeGoodTemplatePos(goodTemplate);
    }

    public GoodTemplate getGoodTemplatePosByName(String name) {
        return goodTemplatePosCache.getGoodTemplatePosByName(name);
    }

    @Autowired
    GoodTemplatePropertiesCache goodTemplatePropertiesCache;

    public void addGoodTemplateProperties(GoodTemplateProperties goodTemplateProperties) {
        goodTemplatePropertiesCache.addGoodTemplateProperties(goodTemplateProperties);
    }

    public GoodTemplateProperties getGoodTemplateProperties(String id) {
        return goodTemplatePropertiesCache.getGoodTemplateProperties(id);
    }

    public List<GoodTemplateProperties> getGoodTemplatePropertiess(int status, int pageIndex, int pageSize, String name) {
        return goodTemplatePropertiesCache.getGoodTemplatePropertiess(status, pageIndex, pageSize, name);
    }

    public List<GoodTemplateProperties> getGoodTemplatePropertiesAll() {
        return goodTemplatePropertiesCache.getGoodTemplatePropertiesAll();
    }

    public int getGoodTemplatePropertiesCount(int status, String name) {
        return goodTemplatePropertiesCache.getGoodTemplatePropertiesCount(status, name);
    }

    public void changeGoodTemplateProperties(GoodTemplateProperties goodTemplateProperties) {
        goodTemplatePropertiesCache.changeGoodTemplateProperties(goodTemplateProperties);
    }

    @Autowired
    GoodTemplatePropertiesValueCache goodTemplatePropertiesValueCache;

    public void addGoodTemplatePropertiesValue(GoodTemplatePropertiesValue goodTemplatePropertiesValue) {
        goodTemplatePropertiesValueCache.addGoodTemplatePropertiesValue(goodTemplatePropertiesValue);
    }

    public GoodTemplatePropertiesValue getGoodTemplatePropertiesValue(String id) {
        return goodTemplatePropertiesValueCache.getGoodTemplatePropertiesValue(id);
    }

    public List<GoodTemplatePropertiesValue> getGoodTemplatePropertiesValues(int status, int pageIndex, int pageSize, String name) {
        return goodTemplatePropertiesValueCache.getGoodTemplatePropertiesValues(status, pageIndex, pageSize, name);
    }

    public List<GoodTemplatePropertiesValue> getGoodTemplatePropertiesValueAll() {
        return goodTemplatePropertiesValueCache.getGoodTemplatePropertiesValueAll();
    }

    public int getGoodTemplatePropertiesValueCount(int status, String name) {
        return goodTemplatePropertiesValueCache.getGoodTemplatePropertiesValueCount(status, name);
    }

    public void changeGoodTemplatePropertiesValue(GoodTemplatePropertiesValue goodTemplatePropertiesValue) {
        goodTemplatePropertiesValueCache.changeGoodTemplatePropertiesValue(goodTemplatePropertiesValue);
    }

    @Autowired
    GoodTerminalCache goodTerminalCache;

    public void addGoodTerminal(GoodTerminal goodTerminal) {
        goodTerminalCache.addGoodTerminal(goodTerminal);
    }

    public GoodTerminal getGoodTerminal(String id) {
        return goodTerminalCache.getGoodTerminal(id);
    }

    public List<GoodTerminal> getGoodTerminals(int status, int pageIndex, int pageSize, String name) {
        return goodTerminalCache.getGoodTerminals(status, pageIndex, pageSize, name);
    }

    public List<GoodTerminal> getGoodTerminalAll() {
        return goodTerminalCache.getGoodTerminalAll();
    }

    public int getGoodTerminalCount(int status, String name) {
        return goodTerminalCache.getGoodTerminalCount(status, name);
    }

    public void changeGoodTerminal(GoodTerminal goodTerminal) {
        goodTerminalCache.changeGoodTerminal(goodTerminal);
    }

    @Autowired
    GoodTypeCache goodTypeCache;

    public List<GoodType> loaderGoodType() {
        return goodTypeCache.loaderGoodType();
    }

    public void addGoodType(GoodType goodType) {
        goodTypeCache.addGoodType(goodType);
    }

    public List<GoodType> getGoodTypeParent(String parentId) {
        return goodTypeCache.getGoodTypeParent(parentId);
    }

    public GoodType getGoodType(String id) {
        return goodTypeCache.getGoodType(id);
    }

    public List<GoodType> getGoodTypes(int status, int pageIndex, int pageSize, String title, HttpServletRequest request) {
        return goodTypeCache.getGoodTypes(status, pageIndex, pageSize, title, RetrieveStatusEnum.NORMAL.getKey(), request);
    }

    public List<GoodType> getGoodTypeAll(HttpServletRequest request) {
        return goodTypeCache.getGoodTypeAll(request);
    }

    public int getGoodTypeCount(int status, String title) {
        return goodTypeCache.getGoodTypeCount(status, title, RetrieveStatusEnum.NORMAL.getKey());
    }

    public int getGoodTypeCountForSecondType(int status, String title) {
        return goodTypeCache.getGoodTypeCountForSecondType(status, title, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeGoodType(GoodType goodType) {
        goodTypeCache.changeGoodType(goodType);
    }

    public List<GoodType> getGoodsTypesByUserId(List<String> strings, Integer pageLimit, Integer pageSize) {
        return goodTypeCache.getGoodsTypesByUserId(strings, pageLimit, pageSize);
    }

    public GoodType getGoodTypeByTitle(String title) {
        return goodTypeCache.getGoodTypeByTitle(title);
    }

    public List<GoodType> getGoodTypeisRepetition(Map<String, Object> map) {
        return goodTypeCache.getGoodTypeisRepetition(map);
    }

    public void deleteGoodType(List<String> strings) {
        goodTypeCache.deleteGoodType(strings);
    }

    public GoodType getGoodTypeByid(String id) {
        return goodTypeCache.getGoodType(id);
    }

    /**
     * 根据id和type修改分类名称
     *
     * @param id, typeName, key
     * @return void
     * @author oy
     * @date 2019/4/11
     */
    public void updateGoodTypeByIdAndType(String id, String typeName) {
        goodTypeCache.updateGoodTypeByIdAndType(id, typeName);
    }

    /**
     * getGoodTypeOneTwoLeave
     *
     * @return
     */
    public List<GoodTypeOne> getGoodTypeOneTwoLeave(List<String> ids) {
        return goodTypeCache.getGoodTypeOneTwoLeave(ids);
    }

    public List<GoodType> getGoodsTypesByUserIdAndNameLike(List<String> idList, String typeName) {
        return goodTypeCache.getGoodsTypesByUserIdAndNameLike(idList, typeName);
    }

    public List<GoodType> getGoodsTypesOneByUserIdAndNameLike(List<String> idList, String typeName) {
        return goodTypeCache.getGoodsTypesOneByUserIdAndNameLike(idList, typeName);
    }

    public List<GoodType> getGoodTypesByPage(String title,Integer pageStartIndex, Integer pageSize) {
        return goodTypeCache.getGoodTypesByPage(title,pageStartIndex, pageSize);
    }

    public List<GoodType> getGoodTypesAndSecondTypeByTypeId(String ids, Integer pageStartIndex, Integer pageSize) {
        return goodTypeCache.getGoodTypesAndSecondTypeByTypeId(ids, pageStartIndex, pageSize);
    }

    public List<GoodType> getGoodTypeOne(Integer pageStartIndex, Integer pageSize) {
        return goodTypeCache.getGoodTypeOne(pageStartIndex, pageSize);
    }

    public GoodType getGoodTypeOneByTwoTypeId(String typeTwoId) {
        return goodTypeCache.getGoodTypeOneByTwoTypeId(typeTwoId);
    }

    public void batchAddGoodType(List<GoodType> goodTypeList) {
        goodTypeCache.batchAddGoodType(goodTypeList);
    }

    @Autowired
    GoodUnitCache goodUnitCache;

    public void addGoodUnit(GoodUnit goodUnit) {
        goodUnitCache.addGoodUnit(goodUnit);
    }

    public GoodUnit getGoodUnit(String id) {
        return goodUnitCache.getGoodUnit(id);
    }

    public List<GoodUnit> getGoodUnits(int status, int pageIndex, int pageSize, String title, HttpServletRequest request) {
        return goodUnitCache.getGoodUnits(status, pageIndex, pageSize, title, RetrieveStatusEnum.NORMAL.getKey(), request);
    }

    public List<GoodUnit> getGoodUnitAll(HttpServletRequest request) {
        return goodUnitCache.getGoodUnitAll(request);
    }

    public List<GoodUnit> loaderGoodsUnits() {
        return goodUnitCache.loaderGoodsUnits();
    }

    public int getGoodUnitCount(int status, String title) {
        return goodUnitCache.getGoodUnitCount(status, title, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeGoodUnit(GoodUnit goodUnit) {
        goodUnitCache.changeGoodUnit(goodUnit);
    }

    public GoodUnit getGoodUnitByTitle(String title) {
        return goodUnitCache.getGoodUnitByTitle(title);
    }

    public List<GoodUnit> getGoodUnitisRepetition(Map<String, Object> map) {
        return goodUnitCache.getGoodUnitisRepetition(map);
    }

    public void deleteGoodUnit(List<String> ids) {
        goodUnitCache.deleteGoodUnit(ids);
    }

    @Autowired
    GoodShopCartCache goodShopCartCache;

    public void deleteGoodShopCart(List<String> ids, String userId) {
        goodShopCartCache.deleteGoodShopCart(ids, userId);
    }

    public void changeGoodShopCart(Map<String, Object> map) {
        goodShopCartCache.changeGoodShopCart(map);
    }

    public List<GoodShopCart> getGoodShopCartByMap(Map<String, Object> map) {
        return goodShopCartCache.getGoodShopCartByMap(map);
    }

    public GoodShopCart getGoodShopCartById(String id) {
        return goodShopCartCache.getGoodShopCartById(id);
    }

    public void addGoodShopCart(GoodShopCart goodShopCart) {
        goodShopCartCache.addGoodShopCart(goodShopCart);
    }

    public GoodShopCart getGoodShopCartByUserIdAndGoodId(String userId, String goodId) {
        return goodShopCartCache.getGoodShopCartByUserIdAndGoodId(userId, goodId);
    }

    public void changeGoodShopCartStauts(Map<String, Object> map) {
        goodShopCartCache.changeGoodShopCartStauts(map);
    }

    public List<GoodShopCart> getGoodShopCartByMapByPage(Map<String, Object> map, Integer pageStartIndex, Integer pageSize) {
        return goodShopCartCache.getGoodShopCartByMapByPage(map,pageStartIndex,pageSize);
    }
}
