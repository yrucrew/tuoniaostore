package com.tuoniaostore.good.cache;
import com.tuoniaostore.datamodel.good.GoodTemplateProperties;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodTemplatePropertiesCache {

    void addGoodTemplateProperties(GoodTemplateProperties goodTemplateProperties);

    GoodTemplateProperties getGoodTemplateProperties(String id);

    List<GoodTemplateProperties> getGoodTemplatePropertiess(int pageIndex, int pageSize, int status, String name);
    List<GoodTemplateProperties> getGoodTemplatePropertiesAll();
    int getGoodTemplatePropertiesCount(int status, String name);
    void changeGoodTemplateProperties(GoodTemplateProperties goodTemplateProperties);

}
