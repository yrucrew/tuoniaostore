package com.tuoniaostore.good.cache.impl;

import com.tuoniaostore.commons.http.HttpRequest;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.datamodel.good.GoodUnit;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodUnitCache;
import com.tuoniaostore.good.cache.remote.GoodsDataCacheService;
import com.tuoniaostore.good.data.GoodUnitMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@CacheConfig(cacheNames = "goodUnit")
public class GoodUnitCacheImpl implements GoodUnitCache {

    @Autowired
    GoodUnitMapper mapper;
    @Autowired
    GoodsDataCacheService goodsDataCacheService;

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true)
    })
    public void addGoodUnit(GoodUnit goodUnit) {
        goodsDataCacheService.loaderGoodsUnits();
        mapper.addGoodUnit(goodUnit);
    }

    @Override
    @Cacheable(value = "goodUnit", key = "'getGoodUnit_'+#id")
    public GoodUnit getGoodUnit(String id) {
        GoodUnit goodUnit = goodsDataCacheService.getGoodsUnit(id);
        if (goodUnit != null) {
            return goodUnit;
        } else {
        return mapper.getGoodUnit(id);
        }
    }

    @Override
    @Cacheable(value = "goodUnit", key = "'getGoodUnits_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#title+'_'+#retrieveStatus")
    public List<GoodUnit> getGoodUnits(int status, int pageIndex, int pageSize, String title, int retrieveStatus, HttpServletRequest request) {
        List<GoodUnit> goodUnits = mapper.getGoodUnits(status, pageIndex, pageSize, title, retrieveStatus);
        fillGoodUnits(goodUnits, request);
        return goodUnits;
    }

    @Override
    @Cacheable(value = "goodUnit", key = "'getGoodUnitAll'")
    public List<GoodUnit> getGoodUnitAll(HttpServletRequest request) {
        List<GoodUnit> goodUnits = mapper.getGoodUnitAll();
        fillGoodUnits(goodUnits,request);
        return goodUnits;
    }


    @Override
    public List<GoodUnit> loaderGoodsUnits() {
        List<GoodUnit> goodUnits = mapper.loaderGoodsUnits();
        return goodUnits;
    }

    @Override
    @Cacheable(value = "goodUnit", key = "'getGoodUnitCount_'+#status+'_'+#title+'_'+#retrieveStatus")
    public int getGoodUnitCount(int status, String title, int retrieveStatus) {
        return mapper.getGoodUnitCount(status, title, retrieveStatus);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true)
    })
    public void changeGoodUnit(GoodUnit goodUnit) {
        mapper.changeGoodUnit(goodUnit);
        goodsDataCacheService.loaderGoodsUnits();
    }

    @Override
    @Cacheable(value = "goodUnit", key = "'getGoodUnitByTitle_'+#title")
    public GoodUnit getGoodUnitByTitle(String title) {
        return mapper.getGoodUnitByTitle(title);
    }

    private void fillGoodUnits(List<GoodUnit> goodUnits,HttpServletRequest request) {
        int size = goodUnits.size();
        for (int i = 0; i < size; i++) {
            try {

                SysUser sysUser= SysUserRmoteService.getSysUser(goodUnits.get(i).getUserId(),request);

                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        goodUnits.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        goodUnits.get(i).setUserName(sysUser.getShowName());
                    } else {
                        goodUnits.get(i).setUserName(sysUser.getDefaultName());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    @Cacheable(value = "goodUnit", key = "'getGoodUnitisRepetition_'+#map")
    public List<GoodUnit> getGoodUnitisRepetition(Map<String,Object> map){
        return mapper.getGoodUnitisRepetition(map);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true)
    })
    public void deleteGoodUnit(List<String> ids) {
        mapper.deleteGoodUnit(ids);
        goodsDataCacheService.loaderGoodsUnits();
    }
}
