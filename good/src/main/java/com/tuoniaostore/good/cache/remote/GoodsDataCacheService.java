package com.tuoniaostore.good.cache.remote;

import com.tuoniaostore.commons.thread.AsyncScheduledService;
import com.tuoniaostore.datamodel.good.*;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.data.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Component
public class GoodsDataCacheService {
    private static final Logger logger = LoggerFactory.getLogger(GoodsDataCacheService.class);
    // 商品单位专用读写锁
    private static final ReentrantReadWriteLock GOODS_UNIT_READ_WRITE_LOCK = new ReentrantReadWriteLock();
    // 商品单位专用写锁
    private static final ReentrantReadWriteLock.WriteLock GOODS_UNIT_WRITE_LOCK = GOODS_UNIT_READ_WRITE_LOCK.writeLock();
    // 所有商品单位映射关系
    private static final Map<String, GoodUnit> goodsUnitMap = new ConcurrentHashMap<>();


    // 商品品牌专用读写锁
    private static final ReentrantReadWriteLock BRAND_READ_WRITE_LOCK = new ReentrantReadWriteLock();
    // 商品品牌专用写锁
    private static final ReentrantReadWriteLock.WriteLock BRAND_WRITE_LOCK = BRAND_READ_WRITE_LOCK.writeLock();
    // 所有商品品牌映射关系
    private static final Map<String, GoodBrand> goodsBrandMap = new ConcurrentHashMap<>();


    // 商品分类专用读写锁
    private static final ReentrantReadWriteLock TYPE_READ_WRITE_LOCK = new ReentrantReadWriteLock();
    // 商品分类专用写锁
    private static final ReentrantReadWriteLock.WriteLock TYPE_WRITE_LOCK = TYPE_READ_WRITE_LOCK.writeLock();
    // 所有商品分类映射关系
    private static final Map<String, GoodType> goodTypeMap = new ConcurrentHashMap<>();

    // 商品模版专用读写锁
    private static final ReentrantReadWriteLock GOODSTEMPLATE_READ_WRITE_LOCK = new ReentrantReadWriteLock();
    // 商品模版专用写锁
    private static final ReentrantReadWriteLock.WriteLock GOODSTEMPLATE_WRITE_LOCK = GOODSTEMPLATE_READ_WRITE_LOCK.writeLock();
    // 所有商品模版映射关系
    private static final Map<String, GoodTemplate> goodTemplateMap = new ConcurrentHashMap<>();

    // 商品标签专用读写锁
    private static final ReentrantReadWriteLock GOODS_PRICE_READ_WRITE_LOCK = new ReentrantReadWriteLock();
    // 商品标签专用写锁
    private static final ReentrantReadWriteLock.WriteLock GOODS_PRICE_WRITE_LOCK = GOODS_PRICE_READ_WRITE_LOCK.writeLock();
    // 所有商品标签映射关系
    private static final Map<String, GoodPrice> goodPriceMap = new ConcurrentHashMap<>();
    @Autowired
    GoodPriceMapper goodPriceMapper;

    @Autowired
    GoodBarcodeMapper goodBarcodeMapper;

    @Autowired
    GoodTemplateMapper goodTemplateMapper;
    @Autowired
    GoodUnitMapper goodUnitMapper;

    @Autowired
    GoodTypeMapper goodTypeMapper;

    @Autowired
    GoodBrandMapper goodBrandMapper;


    public static final long WAIT_LOCK_TIME_OUT = 2;
    public static final TimeUnit WAIT_LOCK_TIME_UNIT = TimeUnit.SECONDS;


    public void initGoodsCache() {
        loaderGoodsUnits();
        loaderGoodBrand();
        loaderGoodTyp();
        loaderGoodTemplate();
        loaderGoodPrice();
    }

    public GoodUnit getGoodsUnit(String id) {
        if(goodsUnitMap.get(id)!=null) {
            return goodsUnitMap.get(id);
        }else {
          return   goodUnitMapper.getGoodUnit(id);
        }
    }

    @Async
    public void loaderGoodsUnits() {
        AsyncScheduledService.submitDataSaveTask(new Runnable() {
            @Override
            public void run() {
                List<GoodUnit> goodsUnits = goodUnitMapper.loaderGoodsUnits();
                Map<String, GoodUnit> tmpGoodsUnitMap = new ConcurrentHashMap<>();
                goodsUnitMap.clear();
                for (GoodUnit goodUnit : goodsUnits) {
                    tmpGoodsUnitMap.put(goodUnit.getId(), goodUnit);
                }
                try {
                    if (GOODS_UNIT_WRITE_LOCK.tryLock(WAIT_LOCK_TIME_OUT, WAIT_LOCK_TIME_UNIT)) {
                        try {
                            goodsUnitMap.clear();
                            goodsUnitMap.putAll(tmpGoodsUnitMap);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            GOODS_UNIT_WRITE_LOCK.unlock();
                        }
                    }
                } catch (Throwable cause) {
                    cause.printStackTrace();
                }
            }
        }, 0, TimeUnit.SECONDS);
    }

    public GoodBrand getGoodBrand(String id) {
        if(goodsBrandMap.get(id)!=null) {
            return goodsBrandMap.get(id);
        }else {
            return goodBrandMapper.getGoodBrand(id);
        }
    }

    @Async
    public void loaderGoodBrand() {
        AsyncScheduledService.submitDataSaveTask(new Runnable() {
            @Override
            public void run() {
                List<GoodBrand> goodBrands = goodBrandMapper.loaderGoodBrand();
                Map<String, GoodBrand> tmpGoodsBrandMap = new ConcurrentHashMap<>();
                goodsBrandMap.clear();
                for (GoodBrand brand : goodBrands) {
                    tmpGoodsBrandMap.put(brand.getId(), brand);
                }
                try {
                    if (BRAND_WRITE_LOCK.tryLock(WAIT_LOCK_TIME_OUT, WAIT_LOCK_TIME_UNIT)) {
                        try {
                            goodsBrandMap.clear();
                            goodsBrandMap.putAll(tmpGoodsBrandMap);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            try {
                                //临时处理
                                //BRAND_WRITE_LOCK.unlock();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Throwable cause) {
                    cause.printStackTrace();
                }
            }
        }, 0, TimeUnit.SECONDS);

    }

    public GoodType getGoodType(String id) {
        if(goodTypeMap.get(id)!=null) {
            return goodTypeMap.get(id);
        }else {
            return goodTypeMapper.getGoodType(id);
        }
    }

    @Async
    public void loaderGoodTyp() {
        List<GoodType> goodTypes = goodTypeMapper.loaderGoodType();
        Map<String, GoodType> tmpGoodsTypeMap = new ConcurrentHashMap<>();

        for (GoodType type : goodTypes) {
            tmpGoodsTypeMap.put(type.getId(), type);
        }
        goodTypeMap.clear();
        try {
            if (TYPE_WRITE_LOCK.tryLock(WAIT_LOCK_TIME_OUT, WAIT_LOCK_TIME_UNIT)) {
                try {
                    goodTypeMap.clear();
                    goodTypeMap.putAll(tmpGoodsTypeMap);
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    TYPE_WRITE_LOCK.unlock();
                }
            }
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public GoodTemplate getGoodTemplate(String id) {
        if(goodTemplateMap.get(id)!=null) {
            return goodTemplateMap.get(id);
        }else {
            return goodTemplateMapper.getGoodTemplate(id);
        }
    }

    @Async
    public void loaderGoodTemplate() {
        List<GoodTemplate> goodTemplates = goodTemplateMapper.loaderGoodTemplate();
        Map<String, GoodTemplate> tmpGoodTemplateMap = new ConcurrentHashMap<>();

        for (GoodTemplate goodTemplate : goodTemplates) {
            tmpGoodTemplateMap.put(goodTemplate.getId(), goodTemplate);
        }
        goodTemplateMap.clear();
        try {
            if (GOODSTEMPLATE_WRITE_LOCK.tryLock(WAIT_LOCK_TIME_OUT, WAIT_LOCK_TIME_UNIT)) {
                try {
                    goodTemplateMap.clear();
                    goodTemplateMap.putAll(tmpGoodTemplateMap);
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    GOODSTEMPLATE_WRITE_LOCK.unlock();
                }
            }
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public GoodPrice getGoodPrice(String id) {

        if(goodPriceMap.get(id)!=null){
            return goodPriceMap.get(id);
        }else {
            return goodPriceMapper.getGoodPrice(id);
        }
    }

    @Async
    public void loaderGoodPrice() {
        List<GoodPrice> goodPrices = goodPriceMapper.loaderGoodPrice();
        Map<String, GoodPrice> tmpGoodPriceMap = new ConcurrentHashMap<>();
        for (GoodPrice goodPrice : goodPrices) {
            tmpGoodPriceMap.put(goodPrice.getId(), goodPrice);
        }
        goodPriceMap.clear();
        try {
            if (GOODS_PRICE_WRITE_LOCK.tryLock(WAIT_LOCK_TIME_OUT, WAIT_LOCK_TIME_UNIT)) {
                try {
                    goodPriceMap.clear();
                    goodPriceMap.putAll(tmpGoodPriceMap);
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    GOODS_PRICE_WRITE_LOCK.unlock();
                }
            }
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
    }
}
