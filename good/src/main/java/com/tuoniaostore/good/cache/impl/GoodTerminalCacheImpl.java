package com.tuoniaostore.good.cache.impl;
        import com.tuoniaostore.datamodel.good.GoodTerminal;
        import com.tuoniaostore.good.cache.GoodTerminalCache;
        import com.tuoniaostore.good.cache.remote.GoodsDataCacheService;
        import com.tuoniaostore.good.data.GoodTerminalMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;

        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@CacheConfig(cacheNames = "goodTerminal")
public class GoodTerminalCacheImpl implements GoodTerminalCache {

    @Autowired
    GoodTerminalMapper mapper;

    @Override
    @CacheEvict(value = "goodTerminal", allEntries = true)
    public void addGoodTerminal(GoodTerminal goodTerminal) {
        mapper.addGoodTerminal(goodTerminal);
    }

    @Override
    @Cacheable(value = "goodTerminal", key = "'getGoodTerminal_'+#id")
    public   GoodTerminal getGoodTerminal(String id) {
        return mapper.getGoodTerminal(id);
    }

    @Override
    @Cacheable(value = "goodTerminal", key = "'getGoodTerminals_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<GoodTerminal> getGoodTerminals(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getGoodTerminals(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "goodTerminal", key = "'getGoodTerminalAll'")
    public List<GoodTerminal> getGoodTerminalAll() {
        return mapper.getGoodTerminalAll();
    }

    @Override
    @Cacheable(value = "goodTerminal", key = "'getGoodTerminalCount_'+#status+'_'+#name")
    public int getGoodTerminalCount(int status, String name) {
        return mapper.getGoodTerminalCount(status, name);
    }

    @Override
    @CacheEvict(value = "goodTerminal", allEntries = true)
    public void changeGoodTerminal(GoodTerminal goodTerminal) {
        mapper.changeGoodTerminal(goodTerminal);
    }

}
