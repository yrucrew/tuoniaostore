package com.tuoniaostore.good.cache.impl;

import com.tuoniaostore.datamodel.good.GoodShopCart;
import com.tuoniaostore.good.cache.GoodShopCartCache;
import com.tuoniaostore.good.data.GoodShopCartMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 商品购物车
 * @author sqd
 * @date 2019/4/5
 */
@Component
@CacheConfig(cacheNames = "goodShopCart")
public class GoodShopCartCacheImpl implements GoodShopCartCache {

    @Autowired
    GoodShopCartMapper mapper;


    @Override
    @CacheEvict(value = "goodShopCart", allEntries = true)
    public void deleteGoodShopCart(List<String> ids,String userId) {
        mapper.deleteGoodShopCart(ids,userId);
    }

    @Override
    @CacheEvict(value = "goodShopCart", allEntries = true)
    public void changeGoodShopCart(Map<String, Object> map) {
        mapper.changeGoodShopCart(map);
    }

    @Override
    @Cacheable(value = "goodShopCart", key = "'getGoodShopCartByMap'+'_'+#map")
    public List<GoodShopCart> getGoodShopCartByMap(Map<String, Object> map) {
        return mapper.getGoodShopCartByMap(map);
    }

    @Override
    @Cacheable(value = "goodShopCart", key = "'getGoodShopCartById'+'_'+#id")
    public GoodShopCart getGoodShopCartById(String id) {
        return mapper.getGoodShopCartById(id);
    }

    @Override
    @CacheEvict(value = "goodShopCart", allEntries = true)
    public void addGoodShopCart(GoodShopCart goodShopCart) {
        mapper.addGoodShopCart(goodShopCart);
    }

    @Override
    @Cacheable(value = "goodShopCart", key = "'getGoodShopCartByUserIdAndGoodId'+'_'+#userId+'_'+#goodId")
    public GoodShopCart getGoodShopCartByUserIdAndGoodId(String userId, String goodId) {
        return mapper.getGoodShopCartByUserIdAndGoodId(userId,goodId);
    }

    @Override
    @CacheEvict(value = "goodShopCart", allEntries = true)
    public void changeGoodShopCartStauts(Map<String, Object> map) {
        mapper.changeGoodShopCartStauts(map);
    }

    @Override
    @Cacheable(value = "goodShopCart", key = "'getGoodShopCartByMapByPage_'+'_'+#map+'_'+#pageStartIndex+'_'+#pageSize")
    public List<GoodShopCart> getGoodShopCartByMapByPage(Map<String, Object> map, Integer pageStartIndex, Integer pageSize) {
        return mapper.getGoodShopCartByMapByPage(map,pageStartIndex,pageSize);
    }

}
