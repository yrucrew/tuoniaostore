package com.tuoniaostore.good.cache;

import com.tuoniaostore.datamodel.good.GoodTemplate;
import com.tuoniaostore.datamodel.user.SysUser;

import java.util.List;

public interface GoodTemplatePosCache {
    void addGoodTemplatePos(GoodTemplate goodTemplate);
    GoodTemplate getGoodTemplatePos(String id);
    List<GoodTemplate> getGoodTemplatePoss(int pageIndex, int pageSize, int status, String name, int retrieveStatus, SysUser sysUser);
    List<GoodTemplate> getGoodTemplatePosAll( SysUser sysUser);
    int getGoodTemplatePosCount(int status, String name,int retrieveStatus);
    void changeGoodTemplatePos(GoodTemplate goodTemplate);

    GoodTemplate getGoodTemplatePosByName(String name);
}
