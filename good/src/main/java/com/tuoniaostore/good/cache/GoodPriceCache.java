package com.tuoniaostore.good.cache;

import com.tuoniaostore.datamodel.good.GoodPrice;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodPriceCache {

    void addGoodPrice(GoodPrice goodPrice);

    GoodPrice getGoodPrice(String id);

    List<GoodPrice> getGoodPrices(int pageIndex, int pageSize, int status, String title, String goodId);

    List<GoodPrice> getGoodPriceAll();

    List<GoodPrice> getGoodPriceAllByPage(List<String> templateIds, List<String> inPriceIds, Integer pageIndex, Integer pageSize);

    List<GoodPrice> loaderGoodPrice();

    int getGoodPriceCount(int status, String title, String goodId);

    void changeGoodPrice(GoodPrice goodPrice);

    List<GoodPrice> getGoodPriceTop15(String goodId);

    List<GoodPrice> getGoodPriceByGoodIdAll(String goodId);

    int getGoodPriceByGoodIdCount(String goodId);

    void updateGoodPrice(String id, String title, String unitId, String goodId);

    void batchDeleteGoodPrice(List<String> ids);

    GoodPrice getGoodPriceisRepetition(String goodId, String title);

    void batchAddGoodPrice(List<GoodPrice> list);

    void updateGoodPriceByParam(Map<String, Object> map);

    List<GoodPriceCombinationVO> showAllGoodPriceis(Map<String, Object> map, Integer pageIndex, Integer pageSize);

    int showAllCountGoodPriceis(Map<String, Object> map);

    List<GoodPrice> getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(String priceTitle, Integer priceStatus, List<String> goodTemplates, List<String> barcodePriceId, int pageStartIndex, int pageSize);

    List<GoodPrice> getGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(String priceTitle, Integer priceStatus, List<String> goodTemplates, List<String> barcodePriceId, List<String> priceIds, int pageStartIndex, int pageSize);

    int getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(String priceTitle, Integer priceStatus, List<String> goodTemplates, List<String> barcodePriceId);

    int getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(String priceTitle, Integer priceStatus, List<String> goodTemplates, List<String> barcodePriceId, List<String> priceIds);

    List<GoodPrice> getGoodPriceNotAll(List<String> priceIds, List<String> templateIds, List<String> inPriceIds, int pageStartIndex, int pageSize);

    int getGoodPriceNotAllCount(List<String> priceIds, List<String> templateIds, List<String> inPriceIds);

    int getGoodPriceCountAll(List<String> templateIds, List<String> inPriceIds);

    void batchDeleteGoodPriceByGoodId(List<String> goodIds);

    void updateGoodPriceStatusByGoodId(Map<String, Object> map);

    void changeIconByBarcode(String icon, String id);
}
