package com.tuoniaostore.good.cache.impl;
        import com.tuoniaostore.datamodel.good.GoodTemplatePropertiesValue;
        import com.tuoniaostore.good.cache.GoodTemplatePropertiesValueCache;
        import com.tuoniaostore.good.data.GoodTemplatePropertiesValueMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;

        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@CacheConfig(cacheNames = "goodTemplatePropertiesValue")
public class GoodTemplatePropertiesValueCacheImpl implements GoodTemplatePropertiesValueCache {

    @Autowired
    GoodTemplatePropertiesValueMapper mapper;

    @Override
    @CacheEvict(value = "goodTemplatePropertiesValue", allEntries = true)
    public void addGoodTemplatePropertiesValue(GoodTemplatePropertiesValue goodTemplatePropertiesValue) {
        mapper.addGoodTemplatePropertiesValue(goodTemplatePropertiesValue);
    }

    @Override
    @Cacheable(value = "goodTemplatePropertiesValue", key = "'getGoodTemplatePropertiesValue_'+#id")
    public   GoodTemplatePropertiesValue getGoodTemplatePropertiesValue(String id) {
        return mapper.getGoodTemplatePropertiesValue(id);
    }

    @Override
    @Cacheable(value = "goodTemplatePropertiesValue", key = "'getGoodTemplatePropertiesValues_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<GoodTemplatePropertiesValue> getGoodTemplatePropertiesValues( int status,int pageIndex, int pageSize, String name) {
        return mapper.getGoodTemplatePropertiesValues(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "goodTemplatePropertiesValue", key = "'getGoodTemplatePropertiesValueAll'")
    public List<GoodTemplatePropertiesValue> getGoodTemplatePropertiesValueAll() {
        return mapper.getGoodTemplatePropertiesValueAll();
    }

    @Override
    @Cacheable(value = "goodTemplatePropertiesValue", key = "'getGoodTemplatePropertiesValueCount_'+#status+'_'+#name")
    public int getGoodTemplatePropertiesValueCount(int status, String name) {
        return mapper.getGoodTemplatePropertiesValueCount(status, name);
    }

    @Override
    @CacheEvict(value = "goodTemplatePropertiesValue", allEntries = true)
    public void changeGoodTemplatePropertiesValue(GoodTemplatePropertiesValue goodTemplatePropertiesValue) {
        mapper.changeGoodTemplatePropertiesValue(goodTemplatePropertiesValue);
    }

}
