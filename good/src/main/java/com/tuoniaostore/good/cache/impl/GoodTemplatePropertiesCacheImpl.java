package com.tuoniaostore.good.cache.impl;
        import com.tuoniaostore.datamodel.good.GoodTemplateProperties;
        import com.tuoniaostore.good.cache.GoodTemplatePropertiesCache;
        import com.tuoniaostore.good.data.GoodTemplatePropertiesMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;

        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@CacheConfig(cacheNames = "goodTemplateProperties")
public class GoodTemplatePropertiesCacheImpl implements GoodTemplatePropertiesCache {

    @Autowired
    GoodTemplatePropertiesMapper mapper;

    @Override
    @CacheEvict(value = "goodTemplateProperties", allEntries = true)
    public void addGoodTemplateProperties(GoodTemplateProperties goodTemplateProperties) {
        mapper.addGoodTemplateProperties(goodTemplateProperties);
    }

    @Override
    @Cacheable(value = "goodTemplateProperties", key = "'getGoodTemplateProperties_'+#id")
    public   GoodTemplateProperties getGoodTemplateProperties(String id) {
        return mapper.getGoodTemplateProperties(id);
    }

    @Override
    @Cacheable(value = "goodTemplateProperties", key = "'getGoodTemplatePropertiess_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<GoodTemplateProperties> getGoodTemplatePropertiess(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getGoodTemplatePropertiess(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "goodTemplateProperties", key = "'getGoodTemplatePropertiesAll'")
    public List<GoodTemplateProperties> getGoodTemplatePropertiesAll() {
        return mapper.getGoodTemplatePropertiesAll();
    }

    @Override
    @Cacheable(value = "goodTemplateProperties", key = "'getGoodTemplatePropertiesCount_'+#status+'_'+#name")
    public int getGoodTemplatePropertiesCount(int status, String name) {
        return mapper.getGoodTemplatePropertiesCount(status, name);
    }

    @Override
    @CacheEvict(value = "goodTemplateProperties", allEntries = true)
    public void changeGoodTemplateProperties(GoodTemplateProperties goodTemplateProperties) {
        mapper.changeGoodTemplateProperties(goodTemplateProperties);
    }

}
