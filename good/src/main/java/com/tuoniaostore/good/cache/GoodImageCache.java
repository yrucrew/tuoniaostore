package com.tuoniaostore.good.cache;

import com.tuoniaostore.datamodel.good.GoodImage;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodImageCache {

    void addGoodImage(GoodImage goodImage);

    GoodImage getGoodImage(String id);

    int getGoodImageGoodIdCount(String goodTemplateId, int retrieveStatus);

    List<GoodImage> getGoodImages(int status, int pageIndex, int pageSize, String title, String goodTemplateId);

    List<GoodImage> getGoodImageAll();

    int getGoodImageCount(int status, String title, String goodTemplateId);

    void changeGoodImage(GoodImage goodImage);

}
