package com.tuoniaostore.good.cache;


import com.tuoniaostore.datamodel.good.GoodShopCart;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能描述
 * @author sqd
 * @date 2019/4/5
 * @param
 * @return
 */
public interface GoodShopCartCache {
    void deleteGoodShopCart(List<String> ids,String userId);
    void changeGoodShopCart(Map<String,Object> map);
    List<GoodShopCart> getGoodShopCartByMap(Map<String,Object> map );
    GoodShopCart getGoodShopCartById(String id);
    void addGoodShopCart(GoodShopCart goodShopCart);
    GoodShopCart getGoodShopCartByUserIdAndGoodId(String userId,String goodId);
    void  changeGoodShopCartStauts(Map<String,Object> map);

    List<GoodShopCart> getGoodShopCartByMapByPage(Map<String, Object> map, Integer pageStartIndex, Integer pageSize);
}
