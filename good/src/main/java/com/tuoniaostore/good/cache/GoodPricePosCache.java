package com.tuoniaostore.good.cache;

import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.datamodel.good.GoodPrice;

import java.util.List;
import java.util.Map;

public interface GoodPricePosCache {
    void addGoodPricePos(GoodPrice goodPrice);

    GoodPrice getGoodPricePos(String id);

    List<GoodPrice> getGoodPricePoss(int pageIndex, int pageSize, int status, String title, String goodId);

    List<GoodPrice> getGoodPricePosAll();

    int getGoodPricePosCount(int status, String title, String goodId);

    void changeGoodPricePos(GoodPrice goodPrice);

    List<GoodPrice> getGoodPricePosTop15(String goodId);

    List<GoodPrice> getGoodPricePosByGoodIdAll(String goodId);

    int getGoodPricePosByGoodIdCount(String goodId);

    GoodPrice getGoodPricePosByTitle(String title);

    void updateGoodPricePos(String id, String title, String unitId, String goodId);

    GoodPrice getAllGoodPriceByTitleAndGoodId(String title, String goodId);

    List<GoodPriceCombinationVO> getPriceTitleAndUnitTitleByPriceIds(List<String> ids);

    List<GoodPriceCombinationVO> getAllGoodTemplateByPriceIds(List<String> s,String nameOrBarcode,Integer pageIndex, Integer pageSize);


    int getAllGoodTemplateByPriceIdsCount(List<String> s,String nameOrBarcode);
}
