package com.tuoniaostore.good.cache.impl;

import com.tuoniaostore.datamodel.good.GoodBrand;
import com.tuoniaostore.datamodel.good.GoodPrice;
import com.tuoniaostore.datamodel.good.GoodTemplate;
import com.tuoniaostore.datamodel.good.GoodType;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.cache.GoodTemplatePosCache;
import com.tuoniaostore.good.cache.remote.GoodsDataCacheService;
import com.tuoniaostore.good.data.GoodTemplatePosMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@CacheConfig(cacheNames = "goodTemplatePos")
public class GoodTemplatePosCacheImpl  implements GoodTemplatePosCache {

    @Autowired
    GoodTemplatePosMapper mapper;
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Autowired
    GoodsDataCacheService goodsDataCacheService;

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodTemplatePos", allEntries = true),
            @CacheEvict(value = "goodBrand_goodTemplate_goodTemplatePos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void addGoodTemplatePos(GoodTemplate goodTemplate) {
        mapper.addGoodTemplatePos(goodTemplate);
        goodsDataCacheService.loaderGoodTemplate();
    }

    @Override
    @Cacheable(value = "goodTemplatePos", key = "'getGoodTemplatePos_'+#id")
    public   GoodTemplate getGoodTemplatePos(String id) {
        return mapper.getGoodTemplatePos(id);
    }

    @Override
    @Cacheable(value = "goodTemplatePos", key = "'getGoodTemplates_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name+'_'+#retrieveStatus")
    public List<GoodTemplate> getGoodTemplatePoss(int status, int pageIndex, int pageSize, String name, int retrieveStatus, SysUser sysUser) {
        List<GoodTemplate>  goodTemplates = mapper.getGoodTemplatePoss(status,pageIndex, pageSize,  name,retrieveStatus);
        fillGoodTemplate(goodTemplates,sysUser);
        return goodTemplates;
    }

    @Override
    @Cacheable(value = "goodTemplate", key = "'getGoodTemplateAll'")
    public List<GoodTemplate> getGoodTemplatePosAll( SysUser sysUser) {
        List<GoodTemplate>  goodTemplates =mapper.getGoodTemplatePosAll();
        fillGoodTemplate(goodTemplates,sysUser);
        return goodTemplates;
    }

    @Override
    @Cacheable(value = "goodTemplate", key = "'getGoodTemplateCount_'+#status+'_'+#name+'_'+#retrieveStatus")
    public int getGoodTemplatePosCount(int status, String name,int retrieveStatus) {
        return mapper.getGoodTemplatePosCount(status, name,retrieveStatus);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodTemplatePos", allEntries = true),
            @CacheEvict(value = "goodBrand_goodTemplate_goodTemplatePos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void changeGoodTemplatePos(GoodTemplate goodTemplate) {
        mapper.changeGoodTemplatePos(goodTemplate);
        goodsDataCacheService.loaderGoodTemplate();
    }

    @Override
    @Cacheable(value = "goodTemplate", key = "'getGoodTemplatePosByName_'+#name")
    public GoodTemplate getGoodTemplatePosByName(String name) {
        return mapper.getGoodTemplatePosByName(name);
    }

    private void fillGoodTemplate(List<GoodTemplate> goodTemplates,SysUser sysUser) {
        int size = goodTemplates.size();
        for (int i = 0; i < size; i++) {
            try {

                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        goodTemplates.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        goodTemplates.get(i).setUserName(sysUser.getShowName());
                    } else {
                        goodTemplates.get(i).setUserName(sysUser.getDefaultName());
                    }
                }
                GoodBrand brand = dataAccessManager.getGoodBrand(goodTemplates.get(i).getBrandId());
                if (brand != null) {
                    goodTemplates.get(i).setBrandName(brand.getName());
                }
                GoodType type = dataAccessManager.getGoodType(goodTemplates.get(i).getTypeId());
                if (type != null) {
                    goodTemplates.get(i).setTypeName(type.getTitle());
                }
                int imageCount = dataAccessManager.getGoodImageGoodIdCount(goodTemplates.get(i).getId());
                goodTemplates.get(i).setImageCount(imageCount);
                int labelCount = dataAccessManager.getGoodPriceByGoodIdCount(goodTemplates.get(i).getId());
                goodTemplates.get(i).setLableCount(labelCount);
                goodTemplates.get(i).setLableContent(getPriceTop15(goodTemplates.get(i).getId()));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private String getPriceTop15(String goodId) {
        List<GoodPrice> goodPrices = dataAccessManager.getGoodPriceTop15(goodId);
        int size = goodPrices.size();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < size; i++) {
            if (i < size - 1) {
                sb.append(goodPrices.get(i).getTitle() + ",");
            } else {
                sb.append(goodPrices.get(i).getTitle());
            }
        }
        return sb.toString();
    }

}
