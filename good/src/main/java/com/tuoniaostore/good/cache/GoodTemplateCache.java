package com.tuoniaostore.good.cache;

import com.tuoniaostore.datamodel.good.GoodPrice;
import com.tuoniaostore.datamodel.good.GoodTemplate;
import com.tuoniaostore.datamodel.user.SysUser;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodTemplateCache {

    void addGoodTemplate(GoodTemplate goodTemplate);

    GoodTemplate getGoodTemplate(String id);

    List<GoodTemplate> getGoodTemplates(int pageIndex, int pageSize, int status, String name, int retrieveStatus, HttpServletRequest request);

    List<GoodTemplate> getGoodTemplateAll(HttpServletRequest request);

    List<GoodTemplate> getGoodTemplateAllByPage(Integer pageStartIndex, Integer pageSize);

    List<GoodTemplate> loaderGoodTemplate();

    int getGoodTemplateCount(int status, String name, int retrieveStatus);

    void changeGoodTemplate(GoodTemplate goodTemplate);

    /**
     * 通过商品名字查找商品的模板
     *
     * @param goodName
     * @return void
     * @author oy
     * @date 2019/4/3
     */
    List<GoodTemplate> getGoodTemplateByName(String goodName, List<String> ids);

    /**
     * 根据品牌id 获取模板id
     *
     * @param brandId
     * @return java.util.List<com.tuoniaostore.datamodel.good.GoodTemplate>
     * @author oy
     * @date 2019/4/15
     */
    List<GoodTemplate> getGoodtemplateByBrandId(String brandId);

    /**
     * 获取商品模板和 商品价格 的一二级标题
     *
     * @param
     * @return java.util.List<com.tuoniaostore.datamodel.good.GoodTemplate>
     * @author oy
     * @date 2019/4/23
     */
    List<GoodTemplate> listGoodTemplatesForAddChainGood(List<String> lists);

    GoodTemplate getGoodTemplateisRepetition(String name);

    void deleteGoodTemplate(List<String> idList);

    void updateGoodTemplateByParam(Map<String, Object> map);

    List<GoodTemplate> getGoodTemplatesByGoodNameLikeAndGoodStatus(String goodName, Integer goodStatus, int retrieveStatus);

    void batchAddGoodTemplate(List<GoodTemplate> goodTemplateList);
}
