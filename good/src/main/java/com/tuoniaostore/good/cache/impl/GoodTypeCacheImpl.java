package com.tuoniaostore.good.cache.impl;

import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.datamodel.good.GoodType;
import com.tuoniaostore.datamodel.user.SysArea;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodTypeCache;
import com.tuoniaostore.good.cache.remote.GoodsDataCacheService;
import com.tuoniaostore.good.data.GoodTypeMapper;
import com.tuoniaostore.datamodel.vo.good.GoodTypeOne;
import com.tuoniaostore.datamodel.vo.good.GoodTypeTwo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@CacheConfig(cacheNames = "goodType")
public class GoodTypeCacheImpl implements GoodTypeCache {

    @Autowired
    GoodTypeMapper mapper;
    @Autowired
    GoodsDataCacheService goodsDataCacheService;

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodType", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void addGoodType(GoodType goodType) {
        mapper.addGoodType(goodType);
        goodsDataCacheService.loaderGoodTyp();//分类加载缓存
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodType_'+#id")
    public GoodType getGoodType(String id) {
        GoodType goodType = goodsDataCacheService.getGoodType(id);
        if (goodType != null) {
            return goodType;
        }
        return mapper.getGoodType(id);
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypes_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#title+'_'+#retrieveStatus")
    public List<GoodType> getGoodTypes(int status, int pageIndex, int pageSize, String title, int retrieveStatus, HttpServletRequest request) {
        List<GoodType> goodTypes = mapper.getGoodTypes(status, pageIndex, pageSize, title, retrieveStatus);
        fillType(goodTypes, request);
        return goodTypes;
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypeParent_'+#parentId")
    public List<GoodType> getGoodTypeParent(String parentId) {
        return mapper.getGoodTypeParent(parentId);
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypeAll'")
    public List<GoodType> getGoodTypeAll(HttpServletRequest request) {
        List<GoodType> goodTypes = mapper.getGoodTypeAll();
        fillType(goodTypes, request);
        return goodTypes;
    }

    @Override
    public List<GoodType> loaderGoodType() {
        return mapper.loaderGoodType();
    }

    private void fillType(List<GoodType> goodTypes, HttpServletRequest request) {

        for (int i = 0; i < goodTypes.size(); i++) {
            try {
                GoodType goodType = getGoodType(goodTypes.get(i).getParentId());
                if (goodType != null) {
                    goodTypes.get(i).setParentName(goodType.getTitle());
                }
                try {

                    SysUser sysUser = SysUserRmoteService.getSysUser(request);
                    if (sysUser != null) {
                        if (sysUser.getRealName() != null) {
                            goodTypes.get(i).setUserName(sysUser.getRealName());
                        } else if (sysUser.getShowName() != null) {
                            goodTypes.get(i).setUserName(sysUser.getShowName());
                        } else {
                            goodTypes.get(i).setUserName(sysUser.getDefaultName());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypeCount_'+#status+'_'+#title+'_'+#retrieveStatus")
    public int getGoodTypeCount(int status, String title, int retrieveStatus) {
        return mapper.getGoodTypeCount(status, title, retrieveStatus);
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypeCountForSecondType_'+#status+'_'+#title+'_'+#retrieveStatus")
    public int getGoodTypeCountForSecondType(int status, String title, int retrieveStatus) {
        return mapper.getGoodTypeCountForSecondType(status, title, retrieveStatus);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodType", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void changeGoodType(GoodType goodType) {
        mapper.changeGoodType(goodType);
        goodsDataCacheService.loaderGoodTyp();//分类加载缓存
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodsTypesByUserId_'+#strings+'_'+#pageLimit+'_'+#pageSize")
    public List<GoodType> getGoodsTypesByUserId(List<String> strings, Integer pageLimit, Integer pageSize) {
        return mapper.getGoodsTypesByUserId(strings, pageLimit, pageSize);
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypeByTitle_'+#title")
    public GoodType getGoodTypeByTitle(String title) {
        return mapper.getGoodTypeByTitle(title);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodType", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void updateGoodTypeByIdAndType(String id, String typeName) {
        mapper.updateGoodTypeByIdAndType(id, typeName);
        goodsDataCacheService.loaderGoodTyp();
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypeOneTwoLeave_'+#ids")
    public List<GoodTypeOne> getGoodTypeOneTwoLeave(List<String> ids) {
        return mapper.getGoodTypeOneTwoLeave(ids);
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypeTwoLeave_'")
    public List<GoodTypeTwo> getGoodTypeTwoLeave() {
//        return mapper.getGoodTypeTwoLeave();
        return mapper.getGoodTypeTwoLeave(null);//目前没放id 需要的时候 可以从方法上面调用下来
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodsTypesByUserIdAndNameLike_'+#idList+'_'+#typeName")
    public List<GoodType> getGoodsTypesByUserIdAndNameLike(List<String> idList, String typeName) {
        return mapper.getGoodsTypesByUserIdAndNameLike(idList, typeName);
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodsTypesOneByUserIdAndNameLike_'+#idList+'_'+#typeName")
    public List<GoodType> getGoodsTypesOneByUserIdAndNameLike(List<String> idList, String typeName) {
        return mapper.getGoodsTypesOneByUserIdAndNameLike(idList, typeName);
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypesByPage_'+#title+'_'+#pageStartIndex+'_'+#pageSize")
    public List<GoodType> getGoodTypesByPage(String title,Integer pageStartIndex, Integer pageSize) {
        return mapper.getGoodTypesByPage(title,pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypeisRepetition_'+#map")
    public List<GoodType> getGoodTypeisRepetition(Map<String, Object> map) {
        return mapper.getGoodTypeisRepetition(map);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodType", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void deleteGoodType(List<String> strings) {
        mapper.deleteGoodType(strings);
        goodsDataCacheService.loaderGoodTyp();
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypesAndSecondTypeByTypeId_'+#ids+'_'+#pageStartIndex+'_'+#pageSize")
    public List<GoodType> getGoodTypesAndSecondTypeByTypeId(String ids, Integer pageStartIndex, Integer pageSize) {
        return mapper.getGoodTypesAndSecondTypeByTypeId(ids, pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypeOne_'+#pageStartIndex+'_'+#pageSize")
    public List<GoodType> getGoodTypeOne(Integer pageStartIndex, Integer pageSize) {
        return mapper.getGoodTypeOne(pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypeByid_'+#id")
    public GoodType getGoodTypeByid(String id) {
        return mapper.getGoodType(id);
    }

    @Override
    @Cacheable(value = "goodType", key = "'getGoodTypeOneByTwoTypeId_'+#typeTwoId")
    public GoodType getGoodTypeOneByTwoTypeId(String typeTwoId) {
        return mapper.getGoodTypeOneByTwoTypeId(typeTwoId);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodType", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void batchAddGoodType(List<GoodType> goodTypeList) {
        mapper.batchAddGoodType(goodTypeList);
        goodsDataCacheService.loaderGoodTyp();
    }
}
