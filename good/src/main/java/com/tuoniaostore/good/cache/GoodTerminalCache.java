package com.tuoniaostore.good.cache;
import com.tuoniaostore.datamodel.good.GoodTerminal;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodTerminalCache {

    void addGoodTerminal(GoodTerminal goodTerminal);

    GoodTerminal getGoodTerminal(String id);

    List<GoodTerminal> getGoodTerminals(int pageIndex, int pageSize, int status, String name);
    List<GoodTerminal> getGoodTerminalAll();
    int getGoodTerminalCount(int status, String name);
    void changeGoodTerminal(GoodTerminal goodTerminal);

}
