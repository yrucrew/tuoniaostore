package com.tuoniaostore.good.cache.impl;

import com.tuoniaostore.datamodel.good.GoodBrand;
import com.tuoniaostore.good.cache.GoodBrandCache;
import com.tuoniaostore.good.cache.remote.GoodsDataCacheService;
import com.tuoniaostore.good.data.GoodBrandMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 商品归属品牌
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@CacheConfig(cacheNames = "goodBrand")
public class GoodBrandCacheImpl implements GoodBrandCache {

    @Autowired
    GoodBrandMapper mapper;
    @Autowired
    GoodsDataCacheService dataCacheService;

    @Override
    @CacheEvict(value = "goodBrand", allEntries = true)
    public void addGoodBrand(GoodBrand goodBrand) {
        mapper.addGoodBrand(goodBrand);
        dataCacheService.loaderGoodBrand();
    }

    @Override
    @Cacheable(value = "goodBrand", key = "'getGoodBrand_'+#id")
    public GoodBrand getGoodBrand(String id) {
        // return mapper.getGoodBrand(id);
        GoodBrand goodBrand = dataCacheService.getGoodBrand(id);
        if (goodBrand != null) {
            return goodBrand;
        } else {
            return mapper.getGoodBrand(id);
        }
    }

    @Override
    @Cacheable(value = "goodBrand", key = "'getGoodBrands_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<GoodBrand> getGoodBrands(int status, int pageIndex, int pageSize, String name) {
        return mapper.getGoodBrands(status, pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "goodBrand", key = "'getGoodBrandAll'")
    public List<GoodBrand> getGoodBrandAll() {
        return mapper.getGoodBrandAll();
    }

    @Override
    public List<GoodBrand> loaderGoodBrand() {
        return mapper.loaderGoodBrand();
    }

    @Override
    @Cacheable(value = "goodBrand", key = "'getGoodBrandCount_'+#status+'_'+#name")
    public int getGoodBrandCount(int status, String name) {
        return mapper.getGoodBrandCount(status, name);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodBrand", allEntries = true),
            @CacheEvict(value = "goodBrand_goodTemplate_goodTemplatePos", allEntries = true)
    })
    public void changeGoodBrand(GoodBrand goodBrand) {
        mapper.changeGoodBrand(goodBrand);
        dataCacheService.loaderGoodBrand();
    }

    @Override
    @Cacheable(value = "goodBrand", key = "'getGoodBranByIds_'+#goodBrands+'_'+#name")
    public List<GoodBrand> getGoodBranByIds(List<String> goodBrands, String name) {
        return mapper.getGoodBranByIds(goodBrands, name);
    }

    @Override
    @Cacheable(value = "goodBrand_goodTemplate_goodTemplatePos", key = "'getGoodTemplateIdsByBrandName_'+#goodBrandName")
    public List<String> getGoodTemplateIdsByBrandName(String goodBrandName) {
        return mapper.getGoodTemplateIdsByBrandName(goodBrandName);
    }

    @Override
    @Cacheable(value = "goodBrand", key = "'getBrandByBrandNameAndStatus_'+#keyWord+'_'+#status")
    public List<GoodBrand> getBrandByBrandNameAndStatus(String keyWord, int status) {
        return mapper.getBrandByBrandNameAndStatus(keyWord, status);
    }

    @Override
    @CacheEvict(value = "goodBrand", allEntries = true)
    public void deleteGoodBrand(List<String> id) {
        mapper.deleteGoodBrand(id);
        dataCacheService.loaderGoodBrand();
    }

    @Override
    @Cacheable(value = "goodBrand", key = "'getGoodBrandsByPage_'+#pageStartIndex+'_'+#pageSize")
    public List<GoodBrand> getGoodBrandsByPage(Integer pageStartIndex, Integer pageSize) {
        return mapper.getGoodBrandsByPage(pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "goodBrand", key = "'getGoodBrandisRepetition_'+#name")
    public GoodBrand getGoodBrandisRepetition(String name) {
        return mapper.getGoodBrandisRepetition(name);
    }

    @Override
    @Cacheable(value = "goodBrand", key = "'getGoodBrandForDelete_'+#id")
    public GoodBrand getGoodBrandForDelete(String id) {
        return mapper.getGoodBrandForDelete(id);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodBrand", allEntries = true),
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplate_pos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void updateGoodBrandByParam(Map<String, Object> map) {
        mapper.updateGoodBrandByParam(map);
        dataCacheService.loaderGoodBrand();
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodBrand", allEntries = true),
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplate_pos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void batchAddGoodBrand(List<GoodBrand> goodBrandListd) {
        mapper.batchAddGoodBrand(goodBrandListd);
        dataCacheService.loaderGoodBrand();
    }
}
