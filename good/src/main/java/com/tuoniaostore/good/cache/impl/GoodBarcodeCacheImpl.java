package com.tuoniaostore.good.cache.impl;

import com.tuoniaostore.datamodel.good.GoodBarcode;
import com.tuoniaostore.good.cache.GoodBarcodeCache;
import com.tuoniaostore.good.data.GoodBarcodeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@CacheConfig(cacheNames = "goodBarcode")
public class GoodBarcodeCacheImpl implements GoodBarcodeCache {

    @Autowired
    GoodBarcodeMapper mapper;



    @Override
    @CacheEvict(value = "goodBarcode", allEntries = true)
    public void addGoodBarcode(GoodBarcode goodBarcode) {
        mapper.addGoodBarcode(goodBarcode);
    }

    @Override
    @Cacheable(value = "goodBarcode", key = "'getGoodBarcode_'+#id")
    public GoodBarcode getGoodBarcode(String id) {
        return mapper.getGoodBarcode(id);
    }

    @Override
    @Cacheable(value = "goodBarcode", key = "'getGoodBarcodes_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<GoodBarcode> getGoodBarcodes(int status, int pageIndex, int pageSize, String name) {
        return mapper.getGoodBarcodes(status, pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "goodBarcode", key = "'getGoodBarcodeByPriceId_'+#priceId")
    public GoodBarcode getGoodBarcodeByPriceId(String priceId) {
        return mapper.getGoodBarcodeByPriceId(priceId);
    }

    @Override
    @Cacheable(value = "goodBarcode", key = "'getGoodBarcodeAll'")
    public List<GoodBarcode> getGoodBarcodeAll() {

        return mapper.getGoodBarcodeAll();
    }

    @Override
    @Cacheable(value = "goodBarcode", key = "'getGoodBarcodeCount_'+#status+'_'+#name")
    public int getGoodBarcodeCount(int status, String name) {
        return mapper.getGoodBarcodeCount(status, name);
    }

    @Override
    @Cacheable(value = "goodBarcode", key = "'getGoodBarcodeByBarcode_'+#barcode")
    public GoodBarcode getGoodBarcodeByBarcode(String barcode) {
        return mapper.getGoodBarcodeByBarcode(barcode);
    }

    @Override
    @Cacheable(value = "goodBarcode", key = "'getBarcodesByPriceId_'+#priceId")
    public List<GoodBarcode> getBarcodesByPriceId(String priceId) {
        return mapper.getBarcodesByPriceId(priceId);
    }

    @Override
    @Cacheable(value = "goodBarcode", key = "'getGoodBarcodeByBarcodeLike_'+#barcode")
    public List<GoodBarcode> getGoodBarcodeByBarcodeLike(String barcode) {
        return mapper.getGoodBarcodeByBarcodeLike(barcode);
    }

    @Override
    @CacheEvict(value = "goodBarcode", allEntries = true)
    public void batchAddGoodBarcode(List<GoodBarcode> barcodes) {
        mapper.batchAddGoodBarcode(barcodes);
    }

    @Override
    @Cacheable(value = "goodBarcode", key = "'getGoodBarcodeByPriceIds_'+#s")
    public List<GoodBarcode> getGoodBarcodeByPriceIds(List<String> s) {
        return mapper.getGoodBarcodeByPriceIds(s);
    }

    @Override
    @Cacheable(value = "goodBarcode", key = "'getGoodBarcodeByBarcodes_'+#list")
    public List<GoodBarcode> getGoodBarcodeByBarcodes(List<String> list) {
        return mapper.getGoodBarcodeByBarcodes(list);
    }

    @Override
    @Cacheable(value = "goodBarcode", key = "'getGoodBarcodesByPriceId_'+#goodPrice")
    public List<GoodBarcode> getGoodBarcodesByPriceId(String goodPrice) {
        return mapper.getGoodBarcodesByPriceId(goodPrice);
    }

    @Override
    @CacheEvict(value = "goodBarcode", allEntries = true)
    public void updateGoodBarcodeParam(Map<String, Object> map) {
         mapper.updateGoodBarcodeParam(map);
    }

    @Override
    @CacheEvict(value = "goodBarcode", allEntries = true)
    public void changeGoodBarcode(GoodBarcode goodBarcode) {
        mapper.changeGoodBarcode(goodBarcode);
    }



}
