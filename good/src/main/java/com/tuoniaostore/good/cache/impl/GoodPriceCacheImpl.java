package com.tuoniaostore.good.cache.impl;

import com.tuoniaostore.datamodel.good.GoodPrice;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.good.cache.GoodPriceCache;
import com.tuoniaostore.good.cache.remote.GoodsDataCacheService;
import com.tuoniaostore.good.data.GoodPriceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@CacheConfig(cacheNames = "goodPrice")
public class GoodPriceCacheImpl implements GoodPriceCache {

    @Autowired
    GoodPriceMapper mapper;
    @Autowired
    GoodsDataCacheService dataCacheService;

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplatePos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void addGoodPrice(GoodPrice goodPrice) {
        mapper.addGoodPrice(goodPrice);
       // dataCacheService.loaderGoodPrice();
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPrice_'+#id")
    public   GoodPrice getGoodPrice(String id) {
        GoodPrice goodPrice=dataCacheService.getGoodPrice(id);
        if(goodPrice!=null){
            return goodPrice;
        }else {
            return mapper.getGoodPrice(id);
        }

    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPrices_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#title+'_'+#goodId")
    public List<GoodPrice> getGoodPrices(int status,int pageIndex, int pageSize,  String title,String goodId) {
        return mapper.getGoodPrices(status,pageIndex, pageSize,  title,goodId);
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPriceByGoodIdAll'+'_'+#goodId")
    public List<GoodPrice> getGoodPriceByGoodIdAll(String goodId) {
        return mapper.getGoodPriceByGoodIdAll(goodId);
    }

    @Override
    public int getGoodPriceByGoodIdCount(String goodId) {
        return mapper.getGoodPriceByGoodIdCount(goodId);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplatePos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void updateGoodPrice(String id, String title, String unitId, String goodId) {
        mapper.updateGoodPrice(id,title,unitId,goodId);
        dataCacheService.loaderGoodPrice();
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplate_pos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void batchDeleteGoodPrice(List<String> ids) {
        mapper.batchDeleteGoodPrice(ids);
        dataCacheService.loaderGoodPrice();
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPriceAll'")
    public List<GoodPrice> getGoodPriceAll() {
        return mapper.getGoodPriceAll();
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPriceAllByPage'+'_'+#templateIds+'_'+#inPriceIds+'_'+#pageIndex+'_'+#pageSize")
    public List<GoodPrice> getGoodPriceAllByPage(List<String> templateIds,List<String> inPriceIds,Integer pageIndex,Integer pageSize) {
        return mapper.getGoodPriceAllByPage(templateIds,inPriceIds,pageIndex,pageSize);
    }

    @Override
    public List<GoodPrice> loaderGoodPrice() {
        return mapper.loaderGoodPrice();
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPriceTop15'+#goodId")
    public List<GoodPrice> getGoodPriceTop15(String goodId) {
        return mapper.getGoodPriceTop15(goodId);
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPriceCount_'+#status+'_'+#title+'_'+#goodId")
    public int getGoodPriceCount(int status, String title,String goodId) {
        return mapper.getGoodPriceCount(status, title,goodId);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplate_pos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void changeGoodPrice(GoodPrice goodPrice) {
        mapper.changeGoodPrice(goodPrice);
       // dataCacheService.loaderGoodPrice();
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPriceisRepetition_'+#goodId+'_'+#title")
    public GoodPrice getGoodPriceisRepetition(String  goodId, String  title){
        return mapper.getGoodPriceisRepetition(goodId,title);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplate_pos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void batchAddGoodPrice(List<GoodPrice> list) {
        mapper.batchAddGoodPrice(list);
       // dataCacheService.loaderGoodPrice();
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplate_pos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void updateGoodPriceByParam(Map<String, Object> map) {
        mapper.updateGoodPriceByParam(map);
       // dataCacheService.loaderGoodPrice();
    }

    @Override
    @Cacheable(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", key = "'showAllGoodPriceis_'+#map+'_'+#pageIndex+'_'+#pageSize")
    public List<GoodPriceCombinationVO> showAllGoodPriceis(Map<String, Object> map, Integer pageIndex, Integer pageSize) {
        return mapper.showAllGoodPriceis(map,pageIndex,pageSize);
    }

    @Override
    @Cacheable(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", key = "'showAllCountGoodPriceis_'+#map")
    public int showAllCountGoodPriceis(Map<String, Object> map) {
        return mapper.showAllCountGoodPriceis(map);
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId_'+#priceTitle+'_'+#priceStatus+'_'+#goodTemplates+'_'+#barcodePriceId+'_'+#pageStartIndex+'_'+#pageSize")
    public List<GoodPrice> getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(String priceTitle, Integer priceStatus, List<String> goodTemplates, List<String> barcodePriceId, int pageStartIndex, int pageSize) {
        return mapper.getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(priceTitle,priceStatus,goodTemplates,barcodePriceId,pageStartIndex,pageSize);
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice_'+#priceTitle+'_'+#priceStatus+'_'+#goodTemplates+'_'+#barcodePriceId+'_'+#priceIds+'_'+#pageStartIndex+'_'+#pageSize")
    public List<GoodPrice> getGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(String priceTitle, Integer priceStatus, List<String> goodTemplates, List<String> barcodePriceId, List<String> priceIds, int pageStartIndex, int pageSize) {
        return mapper.getGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(priceTitle,priceStatus,goodTemplates,barcodePriceId,priceIds,pageStartIndex,pageSize);
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId_'+#priceTitle+'_'+#priceStatus+'_'+#goodTemplates+'_'+#barcodePriceId")
    public int getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(String priceTitle, Integer priceStatus, List<String> goodTemplates, List<String> barcodePriceId) {
        return mapper.getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(priceTitle,priceStatus,goodTemplates,barcodePriceId);
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice_'+#priceTitle+'_'+#priceStatus+'_'+#goodTemplates+'_'+#barcodePriceId+'_'+#priceIds")
    public int getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(String priceTitle, Integer priceStatus, List<String> goodTemplates, List<String> barcodePriceId, List<String> priceIds) {
        return mapper.getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(priceTitle,priceStatus,goodTemplates,barcodePriceId,priceIds);
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPriceNotAll_'+#priceIds+'_'+#templateIds+'_'+#inPriceIds+#pageStartIndex+'_'+#pageSize")
    public List<GoodPrice> getGoodPriceNotAll(List<String> priceIds,List<String> templateIds,List<String> inPriceIds, int pageStartIndex, int pageSize) {
        return mapper.getGoodPriceNotAll(priceIds,templateIds,inPriceIds,pageStartIndex,pageSize);
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPriceNotAllCount_'+#priceIds+'_'+#templateIds+'_'+#inPriceIds")
    public int getGoodPriceNotAllCount(List<String> priceIds,List<String> templateIds,List<String> inPriceIds) {
        return mapper.getGoodPriceNotAllCount(priceIds,templateIds,inPriceIds);
    }

    @Override
    @Cacheable(value = "goodPrice", key = "'getGoodPriceCountAll'+'_'+#templateIds+'_'+#inPriceIds")
    public int getGoodPriceCountAll(List<String> templateIds,List<String> inPriceIds) {
        return mapper.getGoodPriceCountAll(templateIds,inPriceIds);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplate_pos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void batchDeleteGoodPriceByGoodId( List<String> goodIds) {
        mapper.batchDeleteGoodPriceByGoodId(goodIds);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplate_pos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void updateGoodPriceStatusByGoodId(Map<String, Object> map) {
        mapper.updateGoodPriceStatusByGoodId(map);
    }


    @Override
    @Caching(evict = {
            @CacheEvict(value = "goodPrice", allEntries = true),
            @CacheEvict(value = "goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplate_pos", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit", allEntries = true),
            @CacheEvict(value = "goodPrice_goodPricePos_goodUnit_goodTemplate", allEntries = true),
            @CacheEvict(value = "goodTemplateGood_goodTemplateGoodPos_goodPrice_goodPricePos", allEntries = true)
    })
    public void changeIconByBarcode(String icon, String id) {
        mapper.changeIconByBarcode(icon,id);
    }
}
