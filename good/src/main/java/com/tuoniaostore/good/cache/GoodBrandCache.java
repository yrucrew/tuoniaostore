package com.tuoniaostore.good.cache;
import com.tuoniaostore.datamodel.good.GoodBrand;

import java.util.List;
import java.util.Map;

/**
 * 商品归属品牌
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodBrandCache {

    void addGoodBrand(GoodBrand goodBrand);

    GoodBrand getGoodBrand(String id);

    List<GoodBrand> getGoodBrands(int pageIndex, int pageSize, int status, String name);
    List<GoodBrand> getGoodBrandAll();
    List<GoodBrand> loaderGoodBrand();
    int getGoodBrandCount(int status, String name);
    void changeGoodBrand(GoodBrand goodBrand);

    /***
     * 通过ids 查找品牌
     * @author oy
     * @date 2019/4/13
     * @param goodBrands
     * @return java.util.List<com.tuoniaostore.datamodel.good.GoodBrand>
     */
    List<GoodBrand> getGoodBranByIds(List<String> goodBrands,String name);

    /**
     * 查询品牌对应的模板ids
     * @author oy
     * @date 2019/4/15
     * @param goodBrandName
     * @return java.util.List<java.lang.String>
     */
    List<String> getGoodTemplateIdsByBrandName(String goodBrandName);

    List<GoodBrand> getBrandByBrandNameAndStatus(String keyWord, int status);


    void deleteGoodBrand(List<String> id);

    List<GoodBrand> getGoodBrandsByPage(Integer pageStartIndex, Integer pageSize);

    GoodBrand getGoodBrandisRepetition(String name);

    GoodBrand getGoodBrandForDelete(String id);

    void updateGoodBrandByParam(Map<String, Object> map);

    void batchAddGoodBrand(List<GoodBrand> goodBrandListd);
}
