package com.tuoniaostore.good.cache;
import com.tuoniaostore.datamodel.good.GoodType;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.vo.good.GoodTypeOne;
import com.tuoniaostore.datamodel.vo.good.GoodTypeTwo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodTypeCache {

    void addGoodType(GoodType goodType);

    GoodType getGoodType(String id);

    List<GoodType> getGoodTypes(int pageIndex, int pageSize, int status, String title,int retrieveStatus,HttpServletRequest request);
    List<GoodType> getGoodTypeAll(HttpServletRequest request);
    List<GoodType> loaderGoodType();

    int getGoodTypeCount(int status, String title,int retrieveStatus);

    int getGoodTypeCountForSecondType(int status, String title, int key);

    void changeGoodType(GoodType goodType);

    List<GoodType> getGoodTypeParent(String parentId);

    /**
     * 通过ids查找所有的品牌实体
     * @param strings
     */
    List<GoodType> getGoodsTypesByUserId(List<String> strings,Integer pageLimit,Integer pageSize);

    GoodType getGoodTypeByTitle(String title);

    /**
     *  通过id 修改类型名称
     * @author oy
     * @date 2019/4/11
     * @param id, typeName, type
     * @return void
     */
    void updateGoodTypeByIdAndType(String id, String typeName);


    List<GoodTypeOne> getGoodTypeOneTwoLeave(List<String> ids);

    public List<GoodTypeTwo> getGoodTypeTwoLeave();

    List<GoodType> getGoodsTypesByUserIdAndNameLike(List<String> idList, String typeName);

    List<GoodType> getGoodsTypesOneByUserIdAndNameLike(List<String> idList, String typeName);

    List<GoodType> getGoodTypesByPage(String title,Integer pageStartIndex, Integer pageSize);

    List<GoodType> getGoodTypeisRepetition(Map<String,Object> map);

    void deleteGoodType(List<String> strings);

    List<GoodType> getGoodTypesAndSecondTypeByTypeId(String ids, Integer pageStartIndex, Integer pageSize);

    List<GoodType> getGoodTypeOne(Integer pageStartIndex, Integer pageSize);

    GoodType getGoodTypeByid(String id);

    GoodType getGoodTypeOneByTwoTypeId(String typeTwoId);

    void batchAddGoodType(List<GoodType> goodTypeList);

}
