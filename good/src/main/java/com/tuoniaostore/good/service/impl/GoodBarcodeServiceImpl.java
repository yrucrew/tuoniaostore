package com.tuoniaostore.good.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.good.GoodBarcode;
import com.tuoniaostore.datamodel.good.GoodPrice;
import com.tuoniaostore.datamodel.good.GoodTemplate;
import com.tuoniaostore.datamodel.good.GoodUnit;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodBarcodeService;
import com.tuoniaostore.good.vo.GoodLableVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("goodBarcodeService")
public class GoodBarcodeServiceImpl extends BasicWebService implements GoodBarcodeService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Override
    public JSONObject addGoodBarcode() {
        GoodBarcode goodBarcode = new GoodBarcode();
        String idInit = goodBarcode.getId();
        //条码ID
        String id = getUTF("id", null);
        //条码
        String barcode = getUTF("barcode", null);
        //价格ID
        String priceId = getUTF("priceId", null);
        //父条码Id
        String barcodeParentId = getUTF("barcodeParentId", null);
        if( dataAccessManager.getGoodBarcodeByBarcode(barcode)!=null){
            return fail("该条码已存在！！！");
        }

        if(id == null){
            goodBarcode.setId(id);
        }else{
            goodBarcode.setId(idInit);
        }
        goodBarcode.setBarcode(barcode);
        goodBarcode.setPriceId(priceId);
        goodBarcode.setBarcodeParentId(barcodeParentId);

        try {
            SysUser sysUser=getUser();
            if(sysUser!=null){
                goodBarcode.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dataAccessManager.addGoodBarcode(goodBarcode);
        return success();
    }

    @Override
    public JSONObject getGoodBarcodeByPriceId() {
        String priceId = getUTF("priceId");
        GoodBarcode goodBarcode = dataAccessManager.getGoodBarcodeByPriceId(priceId);
        return success(goodBarcode);
    }

    @Override
    public JSONObject getGoodBarcode() {
        GoodBarcode goodBarcode = dataAccessManager.getGoodBarcode(getId());
        return success(goodBarcode);
    }

    @Override
    public JSONObject getGoodBarcodes() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<GoodBarcode> goodBarcodes = dataAccessManager.getGoodBarcodes(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getGoodBarcodeCount(status, name);
        return success(goodBarcodes, count);
    }

    @Override
    public JSONObject getGoodBarcodeAll() {
        List<GoodBarcode> goodBarcodes = dataAccessManager.getGoodBarcodeAll();
        return success(goodBarcodes);
    }

    @Override
    public JSONObject getGoodBarcodeCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getGoodBarcodeCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeGoodBarcode() {
        //条码
        String barcode = getUTF("barcode", null);
        //价格ID
        String priceId = getUTF("id", null);
        //父条码Id
        String barcodeParentId = getUTF("barcodeParentId", null);

        GoodBarcode goodBarcode = dataAccessManager.getGoodBarcodeByPriceId(priceId);
        if(!goodBarcode.getBarcode().equals(barcode)){
            if( dataAccessManager.getGoodBarcodeByBarcode(barcode)!=null){
                return fail("该条码已存在！！！");
            }
        }
        if(goodBarcode!=null){
            goodBarcode.setBarcode(barcode);
            goodBarcode.setPriceId(priceId);
            goodBarcode.setBarcodeParentId(barcodeParentId);
            dataAccessManager.changeGoodBarcode(goodBarcode);
        }else {
            goodBarcode=new GoodBarcode();
            goodBarcode.setBarcode(barcode);
            goodBarcode.setPriceId(priceId);
            goodBarcode.setBarcodeParentId(barcodeParentId);
            try {
                SysUser sysUser=getUser();
                if(sysUser!=null){
                    goodBarcode.setUserId(sysUser.getUserId());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            dataAccessManager.addGoodBarcode(goodBarcode);
        }

        return success("修改成功!");
    }

    /**
     * 商家端：商品 添加商品 填写条码 返回对应的默认价格标签和单位
     * @author oy
     * @date 2019/4/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getGoodMsgByGoodBarcode() {
        //获取填写的条码
        String barcode = getUTF("barcode");
        if(barcode == null || barcode.equals("")){
            return null;
        }

        //查找good_barcode
        JSONObject jsonObject = new JSONObject();
        GoodLableVO vo = new GoodLableVO();

        GoodBarcode goodBarcodeByBarcode = dataAccessManager.getGoodBarcodeByBarcode(barcode);
        if(goodBarcodeByBarcode != null){//继续获取
            String priceId = goodBarcodeByBarcode.getPriceId();
            GoodPrice goodPrice = null;
            if(priceId != null){
                //通过priceId查找价格表 获取模板信息
                goodPrice = dataAccessManager.getGoodPrice(priceId);
                if(goodPrice == null){//标准模板里面是否有
                    //查询一下 good_price_id 看有没有
                    goodPrice = dataAccessManager.getGoodPricePos(priceId);
                }
                if(goodPrice != null){//找了两次没找到 直接设置为空
                    vo.setPriceId(goodPrice.getId());//价格id
                    vo.setPriceTitle(goodPrice.getTitle());//价格标签
                    vo.setCostPrice(goodPrice.getCostPrice());//进货价
                    vo.setSalePrice(goodPrice.getSalePrice());//销售价

                    String unitId = goodPrice.getUnitId();
                    if(unitId != null){//单位不为空
                        GoodUnit goodUnit = dataAccessManager.getGoodUnit(unitId);
                        if (goodUnit != null && !goodUnit.equals("")) {
                            vo.setUnit(goodUnit.getTitle());//单位标签
                            vo.setUnitId(goodUnit.getId());//单位id
                        }
                    }
                }
            }
        }
        jsonObject.put("vo",vo);
        return success(jsonObject);
    }

    @Override
    public JSONObject getGoodMsgByGoodBarcode2() {
        //获取填写的条码
        String barcode = getUTF("barcode");
        if(barcode == null || barcode.equals("")){
            return null;
        }
        //查找good_barcode
        GoodBarcode goodBarcodeByBarcode = dataAccessManager.getGoodBarcodeByBarcode(barcode);
        return success(goodBarcodeByBarcode);
    }

    @Override
    public JSONObject getBarcodesByPriceId() {
        String priceId = getUTF("priceId");
        List<GoodBarcode> goodBarcode = dataAccessManager.getBarcodesByPriceId(priceId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",goodBarcode);
        return jsonObject;
    }

    @Override
    public JSONObject getGoodBarcodeByBarcodesLike() {
        //获取barcode
        String barcode = getUTF("barcode",null);
        List<GoodBarcode> list = dataAccessManager.getGoodBarcodeByBarcodeLike(barcode);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",list);
        return jsonObject;
    }

    @Override
    public JSONObject getGoodBarcodeByPriceIds() {
        String priceIds = getUTF("priceIds");
        JSONObject jsonObject = new JSONObject();
        if(priceIds != null){
            List<String> s = JSONObject.parseArray(priceIds, String.class);
            if(s != null && s.size() > 0){
                List<GoodBarcode> barcodes = dataAccessManager.getGoodBarcodeByPriceIds(s);
                jsonObject.put("list",barcodes);
            }
        }
        return jsonObject;
    }

    /**
     * 修改条码
     * @author oy
     * @date 2019/5/25
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    @Transactional
    public JSONObject changeGoodBarcodeById() {
        String barcodeId = getUTF("barcodeId");
        String barcode = getUTF("barcode");

        //查询当前条码
        GoodBarcode goodBarcode = dataAccessManager.getGoodBarcode(barcodeId);
        if(goodBarcode == null){
            return fail("条码不存在！");
        }

        //查询修改的条码 是否已经存在我们的数据中
        GoodBarcode goodBarcodeByBarcode = dataAccessManager.getGoodBarcodeByBarcode(barcode);

        if(goodBarcodeByBarcode != null){
            return fail("当前条码已存在！");
        }

        //现在进行修改
        Map<String,Object> map = new HashMap<>();
        map.put("id",barcodeId);
        map.put("barcode",barcode);
        dataAccessManager.updateGoodBarcodeParam(map);
        return success("修改条码成功！");
    }

    /**
     * 通过goodTemplateId获取所有的条码
     * @author oy
     * @date 2019/6/5
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getGoodBarcodeByGoodTemplateId() {
        String goodId = getUTF("goodId");
        //获取所有的价格id
        List<String> priceIds = null;
        List<GoodPrice> goodPriceByGoodIdAll = dataAccessManager.getGoodPriceByGoodIdAll(goodId);//价格标签
        Map<String, String> priceTitleMap = goodPriceByGoodIdAll.stream().collect(Collectors.toMap(GoodPrice::getId, GoodPrice::getTitle));
        GoodTemplate goodTemplate = dataAccessManager.getGoodTemplate(goodId);

        if(goodPriceByGoodIdAll != null && goodPriceByGoodIdAll.size() > 0){
            priceIds = goodPriceByGoodIdAll.stream().map(GoodPrice::getId).distinct().collect(Collectors.toList());
        }
        //通过priceIds 查询所有的条码
        List<GoodBarcode> barcodeByPriceIds = null;
        if(priceIds != null && priceIds.size() > 0){
            barcodeByPriceIds = dataAccessManager.getGoodBarcodeByPriceIds(priceIds);
            barcodeByPriceIds.forEach(x->{
                if(goodTemplate != null){
                    String name = goodTemplate.getName();
                    String s = priceTitleMap.get(x.getPriceId());
                    x.setGoodName(name+s);
                }
            });
        }
        return success(barcodeByPriceIds);
    }

}
