package com.tuoniaostore.good.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.good.GoodTableNameEnum;
import com.tuoniaostore.commons.http.HttpRequest;
import com.tuoniaostore.commons.support.community.SysRetrieceRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.datamodel.good.GoodUnit;
import com.tuoniaostore.datamodel.user.SysRetrieve;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Service("goodUnitService")
public class GoodUnitServiceImpl extends BasicWebService implements GoodUnitService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Override
    public JSONObject addGoodUnit() {
        GoodUnit goodUnit = new GoodUnit();
        String idInit = goodUnit.getId();
        String id = getUTF("id", null);
        //单位标题
        String title = getUTF("title", null);
        //状态 0-不启动 1启动
        Integer status = getIntParameter("status", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        String remarks = getUTF("remarks", null);
        //
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);

        GoodUnit goodUnitByTitle = dataAccessManager.getGoodUnitByTitle(title);
        if (goodUnitByTitle != null) {
            return fail("该单位已存在！！");
        }

        if (id == null) {
            goodUnit.setId(idInit);
        } else {
            goodUnit.setId(id);
        }
        goodUnit.setTitle(title);
        goodUnit.setStatus(status);
        goodUnit.setSort(sort);
        try {
            SysUser sysUser = getUser();
            if (sysUser != null) {
                goodUnit.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        goodUnit.setRemarks(remarks);
        goodUnit.setRetrieveStatus(retrieveStatus);
        dataAccessManager.addGoodUnit(goodUnit);
        return success();
    }

    @Override
    public JSONObject getGoodUnitTree() {
        JSONArray jsonArray = new JSONArray();
        List<GoodUnit> list = dataAccessManager.getGoodUnitAll(getHttpServletRequest());
        if (list != null) {
            int count = list.size();
            for (int i = 0; i < count; i++) {
                JSONObject json = new JSONObject();
                json.put("id", list.get(i).getId());
                json.put("title", list.get(i).getTitle());
                json.put("children", null);
                jsonArray.add(json);
            }
        }
        return success(jsonArray);
    }

    @Override
    public JSONObject getGoodUnitByTitle() {
        String title = getUTF("title");
        GoodUnit goodUnitByTitle = dataAccessManager.getGoodUnitByTitle(title);
        return success(goodUnitByTitle);
    }

    @Override
    public JSONObject getGoodUnitisRepetition() {
        String title = getUTF("title");
        HashMap<String, Object> map = new HashMap<>();
        map.put("title", title);
        List<GoodUnit> goodTypeisRepetition = dataAccessManager.getGoodUnitisRepetition(map);
        JSONObject jsonObject = new JSONObject();
        if (goodTypeisRepetition.size() > 0) {
            //单位类别重复
            jsonObject.put("code", 1);
            return success(jsonObject);
        } else {
            jsonObject.put("code", 0);
            return success(jsonObject);
        }
    }

    @Override
    public JSONObject deleteGoodsUnits() {
        String ids = getUTF("ids");
        List<String> strings = JSONObject.parseArray(ids, String.class);
        if(strings != null){
            collectRetrice(strings);
            dataAccessManager.deleteGoodUnit(strings);
        }else{
            return fail("ids不能为空！");
        }
        return success("删除单位成功!");
    }

    private void collectRetrice(List<String> list){
        if(list != null && list.size() > 0){
            List<SysRetrieve> list1 = new ArrayList<>();
            list.forEach(x->{
                GoodUnit goodUnit = dataAccessManager.getGoodUnit(x);
                SysRetrieve sysRetrieve = new SysRetrieve();
                sysRetrieve.setRetrieveTableId(x);
                sysRetrieve.setRetrieveTableName(GoodTableNameEnum.goodUnit.getValue());
                sysRetrieve.setTitle(goodUnit.getTitle());
                sysRetrieve.setCreateTime(new Date());
                sysRetrieve.setUserId(getUser().getId());
                list1.add(sysRetrieve);
            });
            //添加
            try {
                SysRetrieceRemoteService.addSysRetrieves(list1,getHttpServletRequest());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public JSONObject getGoodUnit() {
        GoodUnit goodUnit = dataAccessManager.getGoodUnit(getId());
        return success(goodUnit);
    }

    @Override
    public JSONObject getGoodUnits() {
        String title = getUTF("title", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<GoodUnit> goodUnits = dataAccessManager.getGoodUnits(status, getPageStartIndex(getPageSize()), getPageSize(), title, getHttpServletRequest());

        int count = dataAccessManager.getGoodUnitCount(status, title);
        return success(goodUnits, count);
    }


    @Override
    public JSONObject getGoodUnitAll() {

        List<GoodUnit> goodUnits = dataAccessManager.getGoodUnitAll(getHttpServletRequest());
        return success(goodUnits);
    }

    @Override
    public JSONObject getGoodUnitCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getGoodUnitCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeGoodUnit() {
        GoodUnit goodUnit = dataAccessManager.getGoodUnit(getId());
        String id = getUTF("id", null);
        //单位标题
        String title = getUTF("title", null);
        if (!goodUnit.getTitle().equals(title)) {
            GoodUnit goodUnitByTitle = dataAccessManager.getGoodUnitByTitle(title);
            if (goodUnitByTitle != null) {
                return fail("该单位已存在！！");
            }
        }
        //状态 0-不启动 1启动
        Integer status = getIntParameter("status", 0);
        Integer sort = getIntParameter("sort", 0);
        String remarks = getUTF("remarks", null);
        goodUnit.setId(id);
        goodUnit.setTitle(title);
        goodUnit.setStatus(status);
        goodUnit.setSort(sort);
        goodUnit.setRemarks(remarks);
        dataAccessManager.changeGoodUnit(goodUnit);
        return success();
    }

    @Override
    public JSONObject loaderGoodsUnits() {
        return success(dataAccessManager.loaderGoodsUnits());
    }
}
