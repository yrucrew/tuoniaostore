package com.tuoniaostore.good.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodTemplatePropertiesService {

    JSONObject addGoodTemplateProperties();

    JSONObject getGoodTemplateProperties();

    JSONObject getGoodTemplatePropertiess();

    JSONObject getGoodTemplatePropertiesCount();
    JSONObject getGoodTemplatePropertiesAll();

    JSONObject changeGoodTemplateProperties();
}
