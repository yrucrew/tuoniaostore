package com.tuoniaostore.good.service;
import com.alibaba.fastjson.JSONObject;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodPricePosService {

    JSONObject addGoodPricePos();

    JSONObject getGoodPricePos();

    JSONObject getGoodPricePoss();

    JSONObject getGoodPricePosCount();
    JSONObject getGoodPricePosAll();

    JSONObject changeGoodPricePos();
    JSONObject getGoodPricePosByGoodIdAll();

    JSONObject getGoodPricePosByTitle();

    JSONObject updateGoodPricePos();

    JSONObject getAllGoodPriceByTitleAndGoodId();

    /**
     * 通过价格id 获取单位 和价格标签
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getAllGoodPricesByPriceIds();


    JSONObject getAllGoodTemplateByPriceIds();

    JSONObject getAllGoodTemplateByPriceIds2();

    JSONObject getAllGoodTemplateAllExtra2();
}
