package com.tuoniaostore.good.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.datamodel.good.*;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("goodImageService")
public class GoodImageServiceImpl extends BasicWebService implements GoodImageService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Override
    public JSONObject addGoodImage() {
        GoodImage goodImage = new GoodImage();
        //
        String title = getUTF("title", null);
        //
        String imageUrl = getUTF("imageUrl", null);
        //
        String goodTemplateId = getUTF("goodTemplateId", null);
        //
        Integer status = getIntParameter("status", 0);
        //类型
        Integer type = getIntParameter("type", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        String remark = getUTF("remark", null);
        goodImage.setTitle(title);
        goodImage.setImageUrl(imageUrl);
        goodImage.setGoodTemplateId(goodTemplateId);
        goodImage.setStatus(status);
        goodImage.setType(type);
        goodImage.setSort(sort);
        try {
            SysUser sysUser = getUser();
            if (sysUser != null) {
                goodImage.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        goodImage.setRemark(remark);
        dataAccessManager.addGoodImage(goodImage);
        return success();
    }

    @Override
    public JSONObject getGoodImage() {
        GoodImage goodImage = dataAccessManager.getGoodImage(getId());
        return success(goodImage);
    }

    @Override
    public JSONObject getGoodImages() {
        String title = getUTF("title", null);
        String goodId = getUTF("goodId", null);
        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        List<GoodImage> goodImages = dataAccessManager.getGoodImages(status, getPageStartIndex(getPageSize()), getPageSize(), title, goodId);
        int size = goodImages.size();
        for (int i = 0; i < size; i++) {
            try {
                SysUser sysUser = SysUserRmoteService.getSysUser(goodImages.get(i).getUserId(), getHttpServletRequest());
                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        goodImages.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        goodImages.get(i).setUserName(sysUser.getShowName());
                    } else {
                        goodImages.get(i).setUserName(sysUser.getDefaultName());
                    }
                }
                GoodTemplate goodTemplate = dataAccessManager.getGoodTemplate(goodImages.get(i).getGoodTemplateId());
                if (goodTemplate != null) {
                    goodImages.get(i).setGoodTemplateName(goodTemplate.getName());
                }
                if (goodImages.get(i).getPriceId() != null) {
                    GoodPrice goodPrice = dataAccessManager.getGoodPrice(goodImages.get(i).getPriceId());
                    if (goodPrice != null) {
                        goodImages.get(i).setPriceTitle(goodPrice.getTitle());
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int count = dataAccessManager.getGoodImageCount(status, title, goodId);
        return success(goodImages, count);
    }

    @Override
    public JSONObject getGoodImageAll() {
        List<GoodImage> goodImages = dataAccessManager.getGoodImageAll();
        return success(goodImages);
    }

    @Override
    public JSONObject getGoodImageCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        return success();
    }

    @Override
    public JSONObject changeGoodImage() {
        GoodImage goodImage = dataAccessManager.getGoodImage(getId());
        //
        String title = getUTF("title", null);
        //
        String imageUrl = getUTF("imageUrl", null);

        String goodTemplateId = getUTF("goodTemplateId", null);

        Integer status = getIntParameter("status", 0);
        //类型
        Integer type = getIntParameter("type", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        String remark = getUTF("remark", null);
        String priceId = getUTF("priceId", null);
        goodImage.setTitle(title);
        goodImage.setImageUrl(imageUrl);
        goodImage.setGoodTemplateId(goodTemplateId);
        goodImage.setStatus(status);
        goodImage.setType(type);
        goodImage.setSort(sort);
        goodImage.setRemark(remark);
        goodImage.setPriceId(priceId);
        dataAccessManager.changeGoodImage(goodImage);
        return success();
    }

}
