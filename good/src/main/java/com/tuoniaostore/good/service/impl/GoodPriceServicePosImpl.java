package com.tuoniaostore.good.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.datamodel.good.*;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodPricePosService;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service("goodPricePosService")
public class GoodPriceServicePosImpl extends BasicWebService implements GoodPricePosService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Override
    public JSONObject addGoodPricePos() {
        GoodPrice goodPrice = new GoodPrice();
        //标签名称
        String idInit = goodPrice.getId();
        String id = getUTF("id", null);
        String title = getUTF("title", null);
        String barcode = getUTF("barcode", null);
        //成本价
        Long costPrice = getLongParameter("costPrice", 0);
        //建议售价
        Long salePrice = getLongParameter("salePrice", 0);
        //交易价
        Long sellPrice = getLongParameter("sellPrice", 0);
        //同行价
        Long colleaguePrice = getLongParameter("colleaguePrice", 0);
        //市场价
        Long marketPrice = getLongParameter("marketPrice", 0);
        //会员价
        Long memberPrice = getLongParameter("memberPrice", 0);
        //促销价
        Long discountPrice = getLongParameter("discountPrice", 0);
        //备注
        String remark = getUTF("remark", null);
        //商品ID
        String goodId = getUTF("goodId", null);
        //数量
        Integer num = getIntParameter("num", 0);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //小图标
        String icon = getUTF("icon", null);
        //单位名称
        String unitId = getUTF("unitId", null);
        //默认选中项
        Integer top = getIntParameter("top", 0);
        //0.可用1.禁用
        Integer status = getIntParameter("status", 0);
        if (id == null) {
            goodPrice.setId(idInit);
        } else {
            goodPrice.setId(id);
        }
        goodPrice.setTitle(title);
        goodPrice.setCostPrice(costPrice);
        goodPrice.setSalePrice(salePrice);
        goodPrice.setSellPrice(sellPrice);
        goodPrice.setColleaguePrice(colleaguePrice);
        goodPrice.setMarketPrice(marketPrice);
        goodPrice.setMemberPrice(memberPrice);
        goodPrice.setDiscountPrice(discountPrice);
        goodPrice.setRemark(remark);
        goodPrice.setGoodId(goodId);
        goodPrice.setNum(num);

        try {
            SysUser sysUser = getUser();
            if (sysUser != null) {
                goodPrice.setUserId(sysUser.getUserId());
            }
            if (barcode != null && barcode.trim().length() > 0) {
                GoodBarcode goodBarcode = dataAccessManager.getGoodBarcodeByBarcode(barcode);
                if (goodBarcode != null) {
                    return fail("该条码已存在");
                } else {
                    GoodBarcode code = new GoodBarcode();
                    code.setBarcode(barcode);
                    code.setPriceId(goodPrice.getId());
                    if (sysUser != null) {
                        code.setUserId(sysUser.getId());
                    }
                    dataAccessManager.addGoodBarcode(code);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        goodPrice.setSort(sort);
        goodPrice.setIcon(icon);
        goodPrice.setUnitId(unitId);
        goodPrice.setTop(top);
        goodPrice.setStatus(status);
        dataAccessManager.addGoodPricePos(goodPrice);
        return success();
    }

    @Override
    public JSONObject getGoodPricePos() {
        GoodPrice goodPrice = dataAccessManager.getGoodPricePos(getId());
        return success(goodPrice);
    }

    @Override
    public JSONObject getGoodPricePoss() {
        String goodId = getUTF("goodId");
        String title = getUTF("title", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<GoodPrice> goodPrices = dataAccessManager.getGoodPricePoss(status, getPageStartIndex(getPageSize()), getPageSize(), title, goodId);
        int size = goodPrices.size();
        for (int i = 0; i < size; i++) {
            try {
                SysUser sysUser = SysUserRmoteService.getSysUser(goodPrices.get(i).getUserId(), getHttpServletRequest());
                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        goodPrices.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        goodPrices.get(i).setUserName(sysUser.getShowName());
                    } else {
                        goodPrices.get(i).setUserName(sysUser.getDefaultName());
                    }
                }


                GoodUnit goodUnit = dataAccessManager.getGoodUnit(goodPrices.get(i).getUnitId());
                if (goodUnit != null) {
                    goodPrices.get(i).setUnitName(goodUnit.getTitle());
                }
                GoodTemplate goodTemplate = dataAccessManager.getGoodTemplatePos(goodPrices.get(i).getGoodId());
                if (goodTemplate != null) {
                    goodPrices.get(i).setGoodName(goodTemplate.getName());
                }
                GoodBarcode goodBarcode = dataAccessManager.getGoodBarcodeByPriceId(goodPrices.get(i).getId());
                if (goodBarcode != null) {
                    goodPrices.get(i).setBarcode(goodBarcode.getBarcode());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int count = dataAccessManager.getGoodPricePosCount(status, title, goodId);
        return success(goodPrices, count);
    }

    @Override
    public JSONObject getGoodPricePosAll() {
        List<GoodPrice> goodPrices = dataAccessManager.getGoodPricePosAll();
        return success(goodPrices);
    }

    @Override
    public JSONObject getGoodPricePosCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        return success();
    }

    @Override
    public JSONObject changeGoodPricePos() {
        GoodPrice goodPrice = dataAccessManager.getGoodPricePos(getId());
        //价格ID
        String id = getUTF("id", null);
        //标签名称
        String title = getUTF("title", null);
        //成本价
        Long costPrice = getLongParameter("costPrice", 0);
        //建议售价
        Long salePrice = getLongParameter("salePrice", 0);
        //交易价
        Long sellPrice = getLongParameter("sellPrice", 0);
        //同行价
        Long colleaguePrice = getLongParameter("colleaguePrice", 0);
        //市场价
        Long marketPrice = getLongParameter("marketPrice", 0);
        //会员价
        Long memberPrice = getLongParameter("memberPrice", 0);
        //促销价
        Long discountPrice = getLongParameter("discountPrice", 0);
        //备注
        String remark = getUTF("remark", null);
        //商品ID
        String goodId = getUTF("goodId", null);
        //数量
        Integer num = getIntParameter("num", 0);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //小图标
        String icon = getUTF("icon", null);
        //单位名称
        String unitId = getUTF("unitId", null);
        //默认选中项
        Integer top = getIntParameter("top", 0);
        //0.可用1.禁用
        Integer status = getIntParameter("status", 0);
        goodPrice.setId(id);
        goodPrice.setTitle(title);
        goodPrice.setCostPrice(costPrice);
        goodPrice.setSalePrice(salePrice);
        goodPrice.setSellPrice(sellPrice);
        goodPrice.setColleaguePrice(colleaguePrice);
        goodPrice.setMarketPrice(marketPrice);
        goodPrice.setMemberPrice(memberPrice);
        goodPrice.setDiscountPrice(discountPrice);
        goodPrice.setRemark(remark);
        goodPrice.setGoodId(goodId);
        goodPrice.setNum(num);
        goodPrice.setSort(sort);
        goodPrice.setIcon(icon);
        goodPrice.setUnitId(unitId);
        goodPrice.setTop(top);
        goodPrice.setStatus(status);
        dataAccessManager.changeGoodPricePos(goodPrice);
        return success();
    }

    @Override
    public JSONObject getGoodPricePosByGoodIdAll() {
        String goodId = getUTF("goodId");
        List<GoodPrice> goodPrices = dataAccessManager.getGoodPricePosByGoodIdAll(goodId);
        return success(goodPrices);
    }

    @Override
    public JSONObject getGoodPricePosByTitle() {
        String title = getUTF("title");
        GoodPrice goodPrices = dataAccessManager.getGoodPricePosByTitle(title);
        return success(goodPrices);
    }

    @Override
    public JSONObject updateGoodPricePos() {
        String title = getUTF("title");
        String id = getUTF("priceId1");
        String unitId = getUTF("unitId", null);
        String goodId = getUTF("templateId", null);
        dataAccessManager.updateGoodPricePos(id, title, unitId, goodId);
        return success();
    }

    @Override
    public JSONObject getAllGoodPriceByTitleAndGoodId() {
        String title = getUTF("title");
        String goodId = getUTF("goodId");

        GoodPrice goodPrice = dataAccessManager.getAllGoodPriceByTitleAndGoodId(title, goodId);
        return success(goodPrice);
    }

    /**
     * 功能描述
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/13
     */
    @Override
    public JSONObject getAllGoodPricesByPriceIds() {
        String priceIds = getUTF("priceIds");
        List<String> ids = JSONObject.parseArray(priceIds, String.class);
        List<GoodPriceCombinationVO> vos = null;
        if (ids != null) {
            vos = dataAccessManager.getPriceTitleAndUnitTitleByPriceIds(ids);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list", vos);
        return jsonObject;
    }


    /**
     * 根据价格id 获取当前的一些模板信息
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/22
     */
    @Override
    public JSONObject getAllGoodTemplateByPriceIds() {
        String priceIds = getUTF("priceIds");
        List<String> s = JSONObject.parseArray(priceIds, String.class);
        JSONObject jsonObject = new JSONObject();
        if (s != null && s.size() > 0) {
            List<GoodPriceCombinationVO> list = dataAccessManager.getAllGoodTemplateByPriceIds(s, null, null, null);
            jsonObject.put("list", list);
        }
        return jsonObject;
    }

    @Override
    public JSONObject getAllGoodTemplateByPriceIds2() {
        String priceIds = getUTF("priceIds");
        String nameOrBarcode = getUTF("nameOrBarcode", null);
        List<String> s = JSONObject.parseArray(priceIds, String.class);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 1);
        if (s != null && s.size() > 0) {
            List<GoodPriceCombinationVO> list = dataAccessManager.getAllGoodTemplateByPriceIds(s, nameOrBarcode, null, null);
            jsonObject.put("list", list);
            jsonObject.put("code", 0);
        }
        return jsonObject;
    }


    @Override
    public JSONObject getAllGoodTemplateAllExtra2() {
        String goodPriceId = getUTF("priceId", null);
        String goodBarcodeName = getUTF("goodBarcode", null);
        String goodName = getUTF("goodName", null);
        boolean flag = false;
        List<String> stringList = null;
        if (goodPriceId != null && !goodPriceId.equals("")) {
            stringList = JSONObject.parseArray(goodPriceId, String.class);
            if (stringList != null && stringList.size() > 0) {
                flag = true;
            }
        }

        //barcode
        List<String> barcodePriceId = new ArrayList<>();//条码价格id 供价格id 筛选
        //是否从条码里面拿商品价格id
        boolean isPriceId = false;
        Map<String, List<GoodBarcode>> goodBarcodeCache = new HashMap<>();//存放条码缓存
        List<GoodBarcode> goodBarcodeByBarcodeLike = null;
        if (goodBarcodeName != null && !goodBarcodeName.equals("")) {
            goodBarcodeByBarcodeLike = dataAccessManager.getGoodBarcodeByBarcodeLike(goodBarcodeName);
            isPriceId = true;
        } else {
            goodBarcodeByBarcodeLike = dataAccessManager.getGoodBarcodeAll();
        }

        if (goodBarcodeByBarcodeLike != null && goodBarcodeByBarcodeLike.size() > 0) {
            //统计当前条码数量（因为一个价格标签有多个条码的存在）
            int count = (int) goodBarcodeByBarcodeLike.stream().map(GoodBarcode::getPriceId).distinct().count();
            for (int i = 0; i < count; i++) {
                //判断当前价格标签是否加入到条码中
                GoodBarcode goodBarcode = goodBarcodeByBarcodeLike.get(i);
                String priceId = goodBarcode.getPriceId();
                if (!barcodePriceId.contains(priceId)) {
                    barcodePriceId.add(priceId);
                    List<GoodBarcode> list = new ArrayList<>();
                    list.add(goodBarcode);
                    goodBarcodeCache.put(priceId, list);
                } else {
                    List<GoodBarcode> barcodes = goodBarcodeCache.get(priceId);
                    barcodes.add(goodBarcode);
                    goodBarcodeCache.put(priceId, barcodes);
                }
            }
        } else {
            return success(goodBarcodeByBarcodeLike, 0);
        }

        //goodTemplate
        List<String> goodTemplates = new ArrayList<>(); //商品模板id
        Map<String, GoodTemplate> goodTemplateeCache = new HashMap<>(); //商品缓存
        List<GoodTemplate> gts = null;
        if (goodName != null && !goodName.equals("")) {
            gts = dataAccessManager.getGoodTemplatesByGoodNameLikeAndGoodStatus(goodName, 0);
            if (gts == null) {
                return success(gts, 0);
            } else {
                int count = gts.size();
                for (int i = 0; i < count; i++) {
                    goodTemplates.add(gts.get(i).getId());
                    goodTemplateeCache.put(gts.get(i).getId(), gts.get(i));
                }
            }
        }

        //价格标签 priceTitle priceStatus
        int count = 0;
        List<GoodPrice> goodPriceList = null;
        if (isPriceId) {
            goodPriceList = dataAccessManager.getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(null, 0, goodTemplates, barcodePriceId, getPageStartIndex(getPageSize()), getPageSize());
        } else {
            goodPriceList = dataAccessManager.getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(null, 0, goodTemplates, null, getPageStartIndex(getPageSize()), getPageSize());
        }
        if (goodPriceList != null && goodPriceList.size() > 0) {
            //查询总条数
            if (isPriceId) {
                count = dataAccessManager.getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(null, 0, goodTemplates, barcodePriceId);
            } else {
                count = dataAccessManager.getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(null, 0, goodTemplates, null);
            }

            List<GoodPriceCombinationVO> gpcv = new ArrayList<>();
            for (GoodPrice x : goodPriceList) {
                GoodPriceCombinationVO vo = new GoodPriceCombinationVO();
                vo.setPriceTitle(x.getTitle());
                vo.setPriceId(x.getId());
                vo.setPriceNum(x.getNum());
                vo.setPriceIcon(x.getIcon());
                vo.setPriceSort(x.getSort());
                vo.setPriceStatus(x.getStatus());
                vo.setPriceTitle(x.getTitle());
                vo.setPriceRemark(x.getRemark());
                vo.setCostPrice(x.getCostPrice());
                vo.setSalePrice(x.getSalePrice());
                vo.setSellPrice(x.getSellPrice());
                vo.setColleaguePrice(x.getColleaguePrice());
                vo.setDiscountPrice(x.getDiscountPrice());
                vo.setMarketPrice(x.getMarketPrice());
                vo.setMemberPrice(x.getMemberPrice());

                if (flag) {
                    if (stringList.contains(x.getId())) {
                        vo.setChecked(1);
                    } else {
                        vo.setChecked(0);
                    }
                }

                //用户
                String userId = x.getUserId();
                if (userId != null) {
                    SysUser sysUser = SysUserRmoteService.getSysUser(userId, getHttpServletRequest());
                    if (sysUser != null) {
                        if (sysUser.getRealName() != null) {
                            vo.setCreateUserName(sysUser.getRealName());
                        } else if (sysUser.getShowName() != null) {
                            vo.setCreateUserName(sysUser.getShowName());
                        } else {
                            vo.setCreateUserName(sysUser.getDefaultName());
                        }
                    }
                }

                //商品模板
                GoodTemplate good = null;
                if (goodTemplateeCache != null && goodTemplateeCache.entrySet().size() != 0) {
                    good = goodTemplateeCache.get(x.getGoodId());
                } else {
                    good = dataAccessManager.getGoodTemplate(x.getGoodId());
                }
                String typeId = null;
                if (good != null) {
                    vo.setGoodTemplateId(good.getId());
                    vo.setGoodName(good.getName());
                    vo.setShelfLife(good.getShelfLife());
                    vo.setImageUrl(good.getImageUrl());
                    vo.setGoodStatus(good.getStatus());
                    vo.setGoodBanner(good.getBannerUrl());
                    //品牌
                    String brandId = good.getBrandId();
                    if (brandId != null && !brandId.equals("")) {
                        GoodBrand goodBrand = dataAccessManager.getGoodBrand(brandId);
                        if (goodBrand != null) {
                            vo.setBrandId(goodBrand.getId());
                            vo.setBrandName(goodBrand.getName());
                            vo.setBrandLogo(goodBrand.getLogo());
                        }
                    }
                    typeId = good.getTypeId();
                }

                List<GoodBarcode> goodBarcode = null;
                if (goodBarcodeCache != null && goodBarcodeCache.entrySet().size() != 0) {
                    goodBarcode = goodBarcodeCache.get(x.getId());
                } else {
                    goodBarcode = dataAccessManager.getBarcodesByPriceId(x.getId());
                }

                if (goodBarcode != null && goodBarcode.size() > 0) {
                    String barcodeId = goodBarcode.stream().map(GoodBarcode::getId).distinct().collect(Collectors.joining(","));
                    String barcodeName = goodBarcode.stream().map(GoodBarcode::getBarcode).distinct().collect(Collectors.joining(","));
                    vo.setBarcodeId(barcodeId);//条码id
                    vo.setBarcode(barcodeName);//条码
                }

                //类别
                GoodType type2 = null;
                if (typeId != null && !typeId.equals("")) {
                    type2 = dataAccessManager.getGoodType(typeId);//二级
                    if (type2 != null) {
                        vo.setTypeNameTwoName(type2.getTitle());
                        vo.setTypeNameTwoId(type2.getId());
                        vo.setTypeIconTwo(type2.getIcon());
                        vo.setTypeImageTwo(type2.getImage());
                        String parentId = type2.getParentId();
                        GoodType type1 = null;
                        if (parentId != null && !parentId.equals("")) {
                            type1 = dataAccessManager.getGoodType(parentId);//一级
                        }
                        if (type1 != null) {
                            vo.setTypeNameOneName(type1.getTitle());
                            vo.setTypeNameOneId(type1.getId());
                            vo.setTypeIconOne(type1.getIcon());
                            vo.setTypeImageOne(type1.getImage());
                        }
                    }
                }

                //单位
                String unitId = x.getUnitId();
                if (unitId != null && !unitId.equals("")) {
                    vo.setUnitTitleId(unitId);
                    GoodUnit goodUnit = dataAccessManager.getGoodUnit(unitId);
                    if (goodUnit != null) {
                        vo.setUnitTitle(goodUnit.getTitle());
                    }
                }
                gpcv.add(vo);
            }
            return success(gpcv, count);
        }
        return success(goodPriceList, count);
    }


}
