package com.tuoniaostore.good.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.good.GoodTerminal;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodTerminalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("goodTerminalService")
public class GoodTerminalServiceImpl extends BasicWebService implements GoodTerminalService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Override
    public JSONObject addGoodTerminal() {
        GoodTerminal goodTerminal = new GoodTerminal();
        //
        String id = getUTF("id", null);
        //显示终端
        String terminalId = getUTF("terminalId", null);
        //价格Id
        String goodPriceId = getUTF("goodPriceId", null);
        //
        String userId = getUTF("userId", null);
        goodTerminal.setId(id);
        goodTerminal.setTerminalId(terminalId);
        goodTerminal.setGoodPriceId(goodPriceId);
        try {
            SysUser sysUser=getUser();
            if(sysUser!=null){
                goodTerminal.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dataAccessManager.addGoodTerminal(goodTerminal);
        return success();
    }

    @Override
    public JSONObject getGoodTerminal() {
        GoodTerminal goodTerminal = dataAccessManager.getGoodTerminal(getId());
        return success(goodTerminal);
    }

    @Override
    public JSONObject getGoodTerminals() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<GoodTerminal> goodTerminals = dataAccessManager.getGoodTerminals(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getGoodTerminalCount(status, name);
        return success(goodTerminals, count);
    }

    @Override
    public JSONObject getGoodTerminalAll() {
        List<GoodTerminal> goodTerminals = dataAccessManager.getGoodTerminalAll();
        return success(goodTerminals);
    }

    @Override
    public JSONObject getGoodTerminalCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getGoodTerminalCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeGoodTerminal() {
        GoodTerminal goodTerminal = dataAccessManager.getGoodTerminal(getId());
        //
        String id = getUTF("id", null);
        //显示终端
        String terminalId = getUTF("terminalId", null);
        //价格Id
        String goodPriceId = getUTF("goodPriceId", null);
        //
        String userId = getUTF("userId", null);
        goodTerminal.setId(id);
        goodTerminal.setTerminalId(terminalId);
        goodTerminal.setGoodPriceId(goodPriceId);
        goodTerminal.setUserId(userId);
        dataAccessManager.changeGoodTerminal(goodTerminal);
        return success();
    }

}
