package com.tuoniaostore.good.service;
import com.alibaba.fastjson.JSONObject;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodTemplatePosService {

    JSONObject addGoodTemplatePos();

    JSONObject getGoodTemplatePos();

    JSONObject getGoodTemplatePoss();

    JSONObject getGoodTemplatePosCount();
    JSONObject getGoodTemplatePosAll();

    JSONObject changeGoodTemplatePos();

    JSONObject getGoodTemplatePosByName();
}
