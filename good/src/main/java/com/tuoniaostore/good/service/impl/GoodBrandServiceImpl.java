package com.tuoniaostore.good.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.good.GoodTableNameEnum;
import com.tuoniaostore.commons.constant.supplychain.HomepageGoodsSettingTypeEnum;
import com.tuoniaostore.commons.support.community.SysRetrieceRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplychainHomepageGoodsSettingRemoteService;
import com.tuoniaostore.datamodel.good.GoodBrand;
import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageGoodsSetting;
import com.tuoniaostore.datamodel.user.SysRetrieve;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service("goodBrandService")
public class GoodBrandServiceImpl extends BasicWebService implements GoodBrandService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Override
    public JSONObject addGoodBrand() {
        GoodBrand goodBrand = new GoodBrand();
        //
        String id = getUTF("id", null);
        //类型
        Integer type = getIntParameter("type", 0);
        //品牌名字
        String name = getUTF("name", null);
        //商标图
        String logo = getUTF("logo", null);
        //简介
        String introduce = getUTF("introduce", null);

        Integer status = getIntParameter("status", 0);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //备注
        String remark = getUTF("remark", null);
        //
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        if(id != null && !id.equals("")){
            goodBrand.setId(id);
        }
        GoodBrand goodBrandisRepetition = dataAccessManager.getGoodBrandisRepetition(name);
        if(goodBrandisRepetition!=null){
            return fail("该品牌已存在！！！");
        }
        goodBrand.setType(type);
        goodBrand.setName(name);
        goodBrand.setLogo(logo);
        goodBrand.setIntroduce(introduce);
        goodBrand.setStatus(status);
        try {
            SysUser sysUser=getUser();
            if(sysUser!=null){
                goodBrand.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        goodBrand.setSort(sort);
        goodBrand.setRemark(remark);
        goodBrand.setRetrieveStatus(retrieveStatus);
        dataAccessManager.addGoodBrand(goodBrand);
        return success();
    }

    @Override
    public JSONObject getGoodBrand() {
        GoodBrand goodBrand = dataAccessManager.getGoodBrand(getId());
        return success(goodBrand);
    }

    @Override
    public JSONObject getGoodBrands() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<GoodBrand> goodBrands = dataAccessManager.getGoodBrands(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int size=goodBrands.size();
        for (int i = 0; i < size; i++) {
            try {
                SysUser user = SysUserRemoteService.getSysUser(goodBrands.get(i).getUserId(), getHttpServletRequest());
                if(user != null){
                    if (user.getRealName() != null) {
                        goodBrands.get(i).setUserName(user.getRealName());
                    } else if (user.getDefaultName() != null) {
                        goodBrands.get(i).setUserName(user.getDefaultName());
                    } else {
                        goodBrands.get(i).setUserName(user.getPhoneNumber());
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        int count = dataAccessManager.getGoodBrandCount(status, name);
        return success(goodBrands, count);
    }

    @Override
    public JSONObject getGoodBrandAll() {
        List<GoodBrand> goodBrands = dataAccessManager.getGoodBrandAll();
        return success(goodBrands);
    }

    @Override
    public JSONObject getGoodBrandCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getGoodBrandCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeGoodBrand() {
        GoodBrand goodBrand = dataAccessManager.getGoodBrand(getId());
        //
        String id = getUTF("id", null);
        //类型
        Integer type = getIntParameter("type", 0);
        //品牌名字
        String name = getUTF("name", null);
        //商标图
        String logo = getUTF("logo", null);
        //简介
        String introduce = getUTF("introduce", null);

        Integer status = getIntParameter("status", 0); 
        //排序
        Integer sort = getIntParameter("sort", 0);
        //备注
        String remark = getUTF("remark", null);
        if(!goodBrand.getName().equals(name)){
            GoodBrand goodBrandisRepetition = dataAccessManager.getGoodBrandisRepetition(name);
            if(goodBrandisRepetition!=null){
                return fail("该品牌已存在！！！");
            }
        }
        goodBrand.setId(id);
        goodBrand.setType(type);
        goodBrand.setName(name);
        goodBrand.setLogo(logo);
        goodBrand.setIntroduce(introduce);
        goodBrand.setStatus(status);
        goodBrand.setSort(sort);
        goodBrand.setRemark(remark);
        dataAccessManager.changeGoodBrand(goodBrand);
        return success();
    }
    /***
     * 根据品牌brandIds查找对应的记录
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getBarcodesByBrandIds() {
        String brandIds = getUTF("brandIds");
        String brandName = getUTF("brandName",null);
        List<String> goodBrands = JSONObject.parseArray(brandIds, String.class);
        List<GoodBrand> list = null;
        if(goodBrands != null && goodBrands.size() > 0){
            list  = dataAccessManager.getGoodBranByIds(goodBrands,brandName);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",list);
        return jsonObject;
    }


    /***
     * 根据品牌 brandName 查找对应的记录
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getBarcodesByBrandName() {
        String goodBrandName = getUTF("goodBrandName");
        if(goodBrandName == null || goodBrandName.equals("")){
            return fail("商品品牌名不能为空！");
        }
        List<String> list = dataAccessManager.getGoodTemplateIdsByBrandName(goodBrandName);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",list);
        return success(jsonObject);
    }

    /**
     * 通过名字和状态查找商品
     * @author oy
     * @date 2019/4/20
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getBrandByBrandNameAndStatus() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        //查找对应的记录
        List<GoodBrand> lists  = dataAccessManager.getBrandByBrandNameAndStatus(keyWord,status);
        return success(lists);
    }

    /***
     * 逻辑删除商品品牌
     * @author oy
     * @date 2019/4/20
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    @Transactional
    public JSONObject deleteGoodBrand() {
        String ids = getUTF("ids");
        List<String> strings = JSONObject.parseArray(ids, String.class);
        if(strings != null){
            dataAccessManager.deleteGoodBrand(strings);
            //同时将对应的品牌放入回收站
            collectRetrice(strings);
        }
        return success("删除品牌成功!");
    }

    private void collectRetrice(List<String> list){
        if(list != null && list.size() > 0){
            List<SysRetrieve> list1 = new ArrayList<>();
            list.forEach(x->{
                GoodBrand goodBrand = dataAccessManager.getGoodBrandForDelete(x);
                SysRetrieve sysRetrieve = new SysRetrieve();
                sysRetrieve.setRetrieveTableId(x);
                sysRetrieve.setRetrieveTableName(GoodTableNameEnum.goodBrand.getValue());
                sysRetrieve.setTitle(goodBrand.getName());
                sysRetrieve.setCreateTime(new Date());
                sysRetrieve.setUserId(getUser().getId());
                list1.add(sysRetrieve);
            });
            //添加
            try {
                SysRetrieceRemoteService.addSysRetrieves(list1,getHttpServletRequest());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public JSONObject getBrandByHomePageGood() {
        String areaId = getUTF("areaId");

        if(areaId == null || areaId.equals("")){
            return  fail("areaId不能为空");
        }
        List<SupplychainHomepageGoodsSetting> homePageGoodByAreaId = SupplychainHomepageGoodsSettingRemoteService.getHomePageGoodByAreaId(areaId, HomepageGoodsSettingTypeEnum.HOT_BRANDS.getKey(),getHttpServletRequest());
        List<GoodBrand> goodBrandAll = dataAccessManager.getGoodBrandsByPage(getPageStartIndex(getPageSize()),getPageSize());
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int goodBrandCount = dataAccessManager.getGoodBrandCount(status, name);

        if(goodBrandAll != null && goodBrandAll.size() > 0 && homePageGoodByAreaId != null && homePageGoodByAreaId.size() > 0){
            List<Object> collect = goodBrandAll.stream().map(item -> {
                String id = item.getId();
                Optional<SupplychainHomepageGoodsSetting> first = homePageGoodByAreaId.stream().filter(x -> x.getValue().equals(id)).findFirst();
                if(first.isPresent()){
                    item.setCheakFlag(true);
                }else{
                    item.setCheakFlag(false);
                }
                return item;
            }).collect(Collectors.toList());
            return success(collect,goodBrandCount);
        }
        return success();
    }

    /**
     * 查询品牌名是否存在
     * @author sqd
     * @date 2019/5/3
     * @param
     * @return
     */
    @Override
    public JSONObject getGoodBrandisRepetition(){
        String name = getUTF("name");
       GoodBrand goodBrandisRepetition = dataAccessManager.getGoodBrandisRepetition(name);
        if(goodBrandisRepetition!=null){
            //品牌名重复
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code", 1);
            return success(jsonObject);
        }else{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code", 0);
            return success(jsonObject);
        }
    }
}
