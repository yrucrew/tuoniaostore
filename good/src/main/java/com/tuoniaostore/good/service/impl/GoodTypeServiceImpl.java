package com.tuoniaostore.good.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.good.GoodTableNameEnum;
import com.tuoniaostore.commons.constant.supplychain.HomepageGoodsSettingTypeEnum;
import com.tuoniaostore.commons.support.community.SysRetrieceRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplychainHomepageGoodsSettingRemoteService;
import com.tuoniaostore.datamodel.good.GoodType;
import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageGoodsSetting;
import com.tuoniaostore.datamodel.user.SysRetrieve;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodTypeService;
import com.tuoniaostore.datamodel.vo.good.GoodTypeOne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;


@Service("goodTypeService")
public class GoodTypeServiceImpl extends BasicWebService implements GoodTypeService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Override
    public JSONObject addGoodType() {
        GoodType goodType = new GoodType();
        //商品类型标题
        String idInit = goodType.getId();
        String id = getUTF("id", null);
        String title = getUTF("title", null);
        //父级ID (归属上级)
        String parentId = getUTF("parentId", null);
        //状态 0-可用 1-提交审核 2-审核不通过 3-失效
        Integer status = getIntParameter("status", 0);
        //排序 值小的靠上
        Integer sort = getIntParameter("sort", 0);
        //是否置顶 0表示不置顶
        Integer top = getIntParameter("top", 0);
        //icon图片链接
        String icon = getUTF("icon", null);
        //展示的图片
        String image = getUTF("image", null);
        //
        String remark = getUTF("remark", null);
        Integer type = getIntParameter("type", 0);

        if (id == null) {
            goodType.setId(idInit);
        } else {
            goodType.setId(id);
        }
        GoodType types = dataAccessManager.getGoodTypeByTitle(title);
        if(types!=null){
            return fail("该分类已存在!!!");
        }
        goodType.setTitle(title);
        goodType.setParentId(parentId);
        goodType.setStatus(status);
        goodType.setSort(sort);
        goodType.setTop(top);
        goodType.setIcon(icon);
        goodType.setImage(image);
        goodType.setType(type);

        try {
            SysUser sysUser = getUser();
            if (sysUser != null) {
                goodType.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        goodType.setRemark(remark);
        dataAccessManager.addGoodType(goodType);
        return success();
    }

    @Override
    public JSONObject getGoodType() {
        GoodType goodType = dataAccessManager.getGoodType(getId());
        return success(goodType);
    }

    @Override
    public JSONObject getGoodTypes() {
        String title = getUTF("title", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<GoodType> goodTypes = dataAccessManager.getGoodTypes(status, getPageStartIndex(getPageSize()), getPageSize(), title, getHttpServletRequest() );

        int count = dataAccessManager.getGoodTypeCount(status, title);

        return success(goodTypes, count);
    }

    @Override
    public JSONObject getGoodTypeAll() {
        List<GoodType> goodTypes = dataAccessManager.getGoodTypeAll(getHttpServletRequest());
        return success(goodTypes);
    }

    @Override
    public JSONObject getGoodTypeCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getGoodTypeCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeGoodType() {
        GoodType goodType = dataAccessManager.getGoodType(getId());
        //
        String id = getUTF("id", null);
        //商品类型标题
        String title = getUTF("title", null);
        //父级ID (归属上级)
        String parentId = getUTF("parentId", null);
        //状态 0-可用 1-提交审核 2-审核不通过 3-失效
        Integer status = getIntParameter("status", 0);
        //排序 值小的靠上
        Integer sort = getIntParameter("sort", 0);
        //是否置顶 0表示不置顶
        Integer top = getIntParameter("top", 0);
        //icon图片链接
        String icon = getUTF("icon", null);
        //展示的图片
        String image = getUTF("image", null);
        //
        String remark = getUTF("remark", null);
        if(!goodType.getTitle().equals(title)) {
            GoodType type = dataAccessManager.getGoodTypeByTitle(title);
            if (type != null) {
                return fail("该分类已存在!!!");
            }
        }
        goodType.setId(id);
        goodType.setTitle(title);
        goodType.setParentId(parentId);
        goodType.setStatus(status);
        goodType.setSort(sort);
        goodType.setTop(top);
        goodType.setIcon(icon);
        goodType.setImage(image);
        goodType.setRemark(remark);

        dataAccessManager.changeGoodType(goodType);
        return success();
    }

    @Override
    public JSONObject getGoodTypeParentTree() {
        List<GoodType> list = dataAccessManager.getGoodTypeParent("afdd09ad5d664557b485f58f48572e32ZnfsvX");
        JSONArray jsonArray = new JSONArray();
        if (list != null) {
            int count = list.size();
            for (int i = 0; i < count; i++) {
                JSONObject json = new JSONObject();
                json.put("id", list.get(i).getId());
                json.put("title", list.get(i).getTitle());
                json.put("children", null);
                jsonArray.add(json);
            }
        }
        return success(jsonArray);
    }

    /**
     * 根据类别list id 查找对应的类别信息
     *
     * @return
     */
    @Override
    public JSONObject getGoodTypesById() {
        String ids = getUTF("ids");
        String[] split = ids.split(",");
        //获取到id list
        List<String> strings = Arrays.asList(split);
        List<GoodType> goodsTypesByUserId = dataAccessManager.getGoodsTypesByUserId(strings,getPageStartIndex(getPageSize()),getPageSize());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list", goodsTypesByUserId);
        return jsonObject;
    }

    @Override
    public JSONObject getGoodTypesById2() {
        String ids = getUTF("ids");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",1);
        if(ids != null && !ids.equals("")){
            List<String> strings = JSONObject.parseArray(ids, String.class);
            //获取到id list
            List<GoodType> goodsTypesByUserId = dataAccessManager.getGoodsTypesByUserId(strings,null,null);
            jsonObject.put("code",0);
            jsonObject.put("list", goodsTypesByUserId);
        }
        return jsonObject;
    }

    @Override
    public JSONObject getGoodTypeByTitle() {
        String title = getUTF("title");
        GoodType goodType = dataAccessManager.getGoodTypeByTitle(title);
        return success(goodType);
    }

    /**
     * 商家端：修改分类名称
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/11
     */
    @Override
    public JSONObject changeGoodTypeById() {
        String id = getUTF("id");//分类id
        String typeName = getUTF("typeName");//分类名称

        if (id == null || id.equals("")) {
            return fail("分类id不能为空！");
        }

        if (typeName == null || typeName.equals("")) {
            return fail("分类名称不能为空！");
        }
        GoodType goodType = dataAccessManager.getGoodTypeByTitle(typeName);
        if(goodType!=null){
            return fail("该分类已存在!!!");
        }
        dataAccessManager.updateGoodTypeByIdAndType(id, typeName);
        return success("修改成功！");
    }

    /***
     * 商品类别一二级菜单
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getAllGoodTypeOneTwo() {
        List<GoodTypeOne> goodTypeOneTwoLeave = dataAccessManager.getGoodTypeOneTwoLeave(null);
        return success(goodTypeOneTwoLeave);
    }

    @Override
    public JSONObject getGoodTypesAndNameLike() {
        String typeIds = getUTF("typeIds");
        String typeName = getUTF("typeName", null);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 1);
        if (typeIds != null && !typeIds.equals("")) {
            List<String> idList = JSONObject.parseArray(typeIds, String.class);
            List<GoodType> goodTypes = dataAccessManager.getGoodsTypesByUserIdAndNameLike(idList, typeName);
            jsonObject.put("list", goodTypes);
            jsonObject.put("code", 0);
        }
        return jsonObject;
    }

    @Override
    public JSONObject getGoodTypesOneNameLike() {
        String typeName = getUTF("typeName");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 1);
        if (typeName != null && !typeName.equals("")) {
            List<GoodType> goodTypes = dataAccessManager.getGoodsTypesByUserIdAndNameLike(null, typeName);
            jsonObject.put("list", goodTypes);
            jsonObject.put("code", 0);
        }
        return jsonObject;
    }

    @Override
    public JSONObject getGoodTypesNameLike() {
        String typeName = getUTF("typeName");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",1);
        if (typeName != null && !typeName.equals("")) {
            List<GoodType> goodTypes = dataAccessManager.getGoodsTypesByUserIdAndNameLike(null, typeName);
            jsonObject.put("list", goodTypes);
            jsonObject.put("code",0);
        }
        return jsonObject;
    }


    @Override
    public JSONObject getTypeByHomePageGood() {
        String areaId = getUTF("areaId",null);
        String title = getUTF("title", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainHomepageGoodsSetting> homePageGoodByAreaId = SupplychainHomepageGoodsSettingRemoteService.getHomePageGoodByAreaId(areaId, HomepageGoodsSettingTypeEnum.HOT_CATEGORY.getKey(), getHttpServletRequest());
        List<GoodType> goodTypes = dataAccessManager.getGoodTypesByPage(title,getPageStartIndex(getPageSize()), getPageSize());

        int count = dataAccessManager.getGoodTypeCountForSecondType(status, title);
        if (goodTypes != null && goodTypes.size() > 0 && homePageGoodByAreaId != null && homePageGoodByAreaId.size() > 0) {
            List<Object> collect = goodTypes.stream().map(item -> {
                String id = item.getId();
                Optional<SupplychainHomepageGoodsSetting> first = homePageGoodByAreaId.stream().filter(x -> x.getValue().equals(id)).findFirst();
                if (first.isPresent()) {
                    item.setCheakFlag(true);
                } else {
                    item.setCheakFlag(false);
                }
                return item;
            }).collect(Collectors.toList());
            return success(collect, count);
        }
        return success(goodTypes,count);
    }


    public JSONArray getGoodTypeChild(String parentId) {
        List<GoodType> list = dataAccessManager.getGoodTypeParent(parentId);

        JSONArray jsonArray = new JSONArray();

        if (list != null) {
            int count = list.size();
            for (int i = 0; i < count; i++) {
                JSONObject json = new JSONObject();
                json.put("id", list.get(i).getId());
                json.put("name", list.get(i).getTitle());
                if (!list.get(i).getId().equals("afdd09ad5d664557b485f58f48572e32ZnfsvX")) {
                    JSONArray child = getGoodTypeChild(list.get(i).getId());
                    if (child != null) {
                        json.put("children", child);
                    }
                }
                jsonArray.add(json);
            }
        }
        return jsonArray;
    }

    @Override
    public JSONObject getGoodTypeTreeAll() {
        JSONArray jsonArray = getGoodTypeChild("afdd09ad5d664557b485f58f48572e32ZnfsvX");
        return success(jsonArray);
    }

    @Override
    public JSONObject getGoodTypeParent() {
        List<GoodType> list = dataAccessManager.getGoodTypeParent("afdd09ad5d664557b485f58f48572e32ZnfsvX");
        return success(list);
    }

    @Override
    public JSONObject getGoodTypeTree() {
        List<GoodType> list = dataAccessManager.getGoodTypeParent("afdd09ad5d664557b485f58f48572e32ZnfsvX");
        JSONArray jsonArray = new JSONArray();
        if (list != null) {
            int count = list.size();
            for (int i = 0; i < count; i++) {
                JSONObject json = new JSONObject();
                json.put("id", list.get(i).getId());
                json.put("title", list.get(i).getTitle());
                if (!list.get(i).getId().equals("afdd09ad5d664557b485f58f48572e32ZnfsvX")) {
                    List<GoodType> child = dataAccessManager.getGoodTypeParent(list.get(i).getId());
                    if (child == null || child.size() == 0) {
                        child.add(list.get(i));
                    }
                    json.put("children", child);
                }
                jsonArray.add(json);
            }
        }
        return success(jsonArray);
    }


    /**
     * 查询品牌名是否存在
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/3
     */
    @Override
    public JSONObject getGoodTypeisRepetition() {
        String title = getUTF("title");
        HashMap<String, Object> map = new HashMap<>();
        map.put("title", title);
        List<GoodType> goodTypeisRepetition = dataAccessManager.getGoodTypeisRepetition(map);
        JSONObject jsonObject = new JSONObject();
        if (goodTypeisRepetition.size() > 0) {
            //商品类别重复
            jsonObject.put("code", 1);
            return success(jsonObject);
        } else {
            jsonObject.put("code", 0);
            return success(jsonObject);
        }
    }

    /**
     * 批量删除对应的商品分类
     * @author oy
     * @date 2019/5/5
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    @Transactional
    public JSONObject deleteGoodType() {
        String ids = getUTF("ids");
        if(ids != null && !ids.equals("")){
            List<String> strings = JSONObject.parseArray(ids, String.class);
            if(strings != null && strings.size() > 0){
                collectRetrice(strings);
                dataAccessManager.deleteGoodType(strings);
            }
        }else{
            return fail("商品类型id不能为空！");
        }
        return success("删除成功！");
    }

    private void collectRetrice(List<String> list){
        if(list != null && list.size() > 0){
            List<SysRetrieve> list1 = new ArrayList<>();
            list.forEach(x->{
                GoodType goodType = dataAccessManager.getGoodType(x);
                SysRetrieve sysRetrieve = new SysRetrieve();
                sysRetrieve.setRetrieveTableId(x);
                sysRetrieve.setRetrieveTableName(GoodTableNameEnum.goodType.getValue());
                sysRetrieve.setTitle(goodType.getTitle());
                sysRetrieve.setCreateTime(new Date());
                sysRetrieve.setUserId(getUser().getId());
                list1.add(sysRetrieve);
            });
            //添加
            try {
                SysRetrieceRemoteService.addSysRetrieves(list1,getHttpServletRequest());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 查询当前的一级类下面的二级分类
     * @author oy
     * @date 2019/5/6
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getGoodTypesAndSecondTypeByTypeId() {
        JSONObject jsonObject = new JSONObject();
        String ids = getUTF("ids");
        if(ids != null && !ids.equals("")){
            List<String> strings = JSONObject.parseArray(ids, String.class);
            jsonObject.put("code",1);
            if(strings != null && strings.size() > 0){
                List<GoodTypeOne> goodTypeOneTwoLeave = dataAccessManager.getGoodTypeOneTwoLeave(strings);
                jsonObject.put("list",goodTypeOneTwoLeave);
                jsonObject.put("code",0);
                return jsonObject;
            }
        }
        return jsonObject;
    }

    @Override
    public JSONObject getGoodSecondTypeByTypeId() {
        JSONObject jsonObject = new JSONObject();
        String firstId = getUTF("firstId");
        jsonObject.put("code",1);
        if(firstId != null && !firstId.equals("")){
            List<GoodType> goodTypeParent = dataAccessManager.getGoodTypeParent(firstId);
            jsonObject.put("code",0);
            jsonObject.put("list",goodTypeParent);
        }
        return jsonObject;
    }

    @Override
    public JSONObject getGoodTypesByPage() {
        String ids = getUTF("ids");
        int limit = getIntParameter("limit");
        int page = getIntParameter("page");

        String[] split = ids.split(",");
        //获取到id list
        List<String> strings = Arrays.asList(split);
        List<GoodType> goodsTypesByUserId = dataAccessManager.getGoodsTypesByUserId(strings,limit,page);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list", goodsTypesByUserId);
        jsonObject.put("code", 0);
        return jsonObject;
    }


    @Override
    public JSONObject getGoodTypeOne() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",1);
        List<GoodType> list = dataAccessManager.getGoodTypeOne(null,null);
        if(list != null && list.size() > 0){
            jsonObject.put("list",list);
            jsonObject.put("code",0);
        }
        return jsonObject;
    }

    @Override
    public JSONObject getGoodTypeOneTwoLeave() {
        List<GoodTypeOne> goodTypeOneTwoLeave = dataAccessManager.getGoodTypeOneTwoLeave(null);
        return  success(goodTypeOneTwoLeave);
    }

    @Override
    public JSONObject getGoodTypeByid() {
        String id = getUTF("id");
        GoodType goodTypeByid = dataAccessManager.getGoodTypeByid(id);
        return success(goodTypeByid);
    }

    @Override
    public JSONObject getGoodTypeOneByTwoTypeId() {
        String typeTwoId = getUTF("typeTwoId");

        if(typeTwoId != null && !typeTwoId.equals("")){
            GoodType goodType = dataAccessManager.getGoodTypeOneByTwoTypeId(typeTwoId);
            if(goodType != null){
                return success(goodType);
            }
        }
        return success();
    }

}
