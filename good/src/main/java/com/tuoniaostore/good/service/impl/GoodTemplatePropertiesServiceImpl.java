package com.tuoniaostore.good.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.good.GoodTemplateProperties;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodTemplatePropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("goodTemplatePropertiesService")
public class GoodTemplatePropertiesServiceImpl extends BasicWebService implements GoodTemplatePropertiesService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Override
    public JSONObject addGoodTemplateProperties() {
        GoodTemplateProperties goodTemplateProperties = new GoodTemplateProperties();
        //自定义值
        String showName = getUTF("showName", null);
        //
        Integer status = getIntParameter("status", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        //创建人
        String userId = getUTF("userId", null);
        //备注
        String remark = getUTF("remark", null);
        goodTemplateProperties.setShowName(showName);
        goodTemplateProperties.setStatus(status);
        goodTemplateProperties.setSort(sort);
        try {
            SysUser sysUser=getUser();
            if(sysUser!=null){
                goodTemplateProperties.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        goodTemplateProperties.setRemark(remark);
        dataAccessManager.addGoodTemplateProperties(goodTemplateProperties);
        return success();
    }

    @Override
    public JSONObject getGoodTemplateProperties() {
        GoodTemplateProperties goodTemplateProperties = dataAccessManager.getGoodTemplateProperties(getId());
        return success(goodTemplateProperties);
    }

    @Override
    public JSONObject getGoodTemplatePropertiess() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<GoodTemplateProperties> goodTemplatePropertiess = dataAccessManager.getGoodTemplatePropertiess(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getGoodTemplatePropertiesCount(status, name);
        return success(goodTemplatePropertiess, count);
    }

    @Override
    public JSONObject getGoodTemplatePropertiesAll() {
        List<GoodTemplateProperties> goodTemplatePropertiess = dataAccessManager.getGoodTemplatePropertiesAll();
        return success(goodTemplatePropertiess);
    }

    @Override
    public JSONObject getGoodTemplatePropertiesCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getGoodTemplatePropertiesCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeGoodTemplateProperties() {
        GoodTemplateProperties goodTemplateProperties = dataAccessManager.getGoodTemplateProperties(getId());
        //
        String id = getUTF("id", null);
        //自定义值
        String showName = getUTF("showName", null);
        //
        Integer status = getIntParameter("status", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        //备注
        String remark = getUTF("remark", null);
        goodTemplateProperties.setId(id);
        goodTemplateProperties.setShowName(showName);
        goodTemplateProperties.setStatus(status);
        goodTemplateProperties.setSort(sort);
        goodTemplateProperties.setRemark(remark);
        dataAccessManager.changeGoodTemplateProperties(goodTemplateProperties);
        return success();
    }

}
