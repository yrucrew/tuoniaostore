package com.tuoniaostore.good.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.good.GoodTableNameEnum;
import com.tuoniaostore.commons.support.community.SysRetrieceRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.commons.support.good.GoodUnitRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplyChainGoodRemoteService;
import com.tuoniaostore.commons.utils.ChinesePinyinUtil;
import com.tuoniaostore.datamodel.good.*;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import com.tuoniaostore.datamodel.user.SysRetrieve;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodTemplateService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;


@Service("goodTemplateService")
public class GoodTemplateServiceImpl extends BasicWebService implements GoodTemplateService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Override
    public JSONObject loadGood() {

        // loadGoodType();
        try {
            Workbook wb = readExcel("/vue/0000.xlsx");

            Sheet sheet = null;
            Row row = null;
            List<Map<String, String>> list = null;
            String cellData = null;
            String columns[] = {"品牌名称", "默认条形码", "商品名称", "规格", "单位", "保质期", "装箱数", "建议零售价", "零售默认价格", "中分类"};
            if (wb != null) {
                //用来存放表中数据
                list = new ArrayList<Map<String, String>>();
                //获取第一个sheet
                sheet = wb.getSheetAt(0);
                //获取最大行数
                int rownum = sheet.getPhysicalNumberOfRows();
                //获取第一行
                row = sheet.getRow(0);
                //获取最大列数
                int colnum = row.getPhysicalNumberOfCells();
                for (int i = 1; i < rownum; i++) {
                    Map<String, String> map = new LinkedHashMap<String, String>();
                    row = sheet.getRow(i);
                    if (row != null) {

                        for (int j = 0; j < colnum; j++) {

                            cellData = (String) getCellFormatValue(row.getCell(j));
                            map.put(columns[j], cellData);
                        }
                    } else {
                        break;
                    }
                    list.add(map);
                }
            }
            //遍历解析出来的list
            for (Map<String, String> map : list) {
                String band = "";
                String barcode = "";
                String goodsName = "";
                String priceName = "";
                String unit = "";
                int shelf_life = 0;
                int num = 0;
                long price = 0L;
                long costprice = 0L;
                String type = "";
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    if (entry.getKey().equals("品牌名称")) {
                        band = entry.getValue().trim();
                    } else if (entry.getKey().equals("默认条形码")) {
                        try {
                            if (entry.getValue() != null && entry.getValue().trim().length() > 0 && entry.getValue().trim().indexOf("TN") == -1) {
                                BigDecimal bigDecimal = new BigDecimal(entry.getValue().trim());
                                barcode = bigDecimal.toPlainString();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (entry.getKey().equals("商品名称")) {
                        goodsName = entry.getValue().trim();
                    } else if (entry.getKey().equals("规格")) {
                        priceName = entry.getValue().trim();
                    } else if (entry.getKey().equals("单位")) {
                        unit = entry.getValue().trim();
                    } else if (entry.getKey().equals("保质期")) {
                        if (entry.getValue().trim() != null && entry.getValue().trim().length() > 0) {
                            Double dt = Double.parseDouble(entry.getValue().trim());
                            shelf_life = getInt(dt);
                        }
                    } else if (entry.getKey().equals("装箱数")) {
                        if (entry.getValue().trim() != null && entry.getValue().trim().length() > 0) {
                            Double dt = Double.parseDouble(entry.getValue().trim().replace("8*8", "8"));
                            num = getInt(dt);
                        }
                    } else if (entry.getKey().equals("建议零售价")) {
                        Double d = Double.parseDouble(entry.getValue().trim()) * 100;
                        price = Math.round(d);
                    } else if (entry.getKey().equals("零售默认价格")) {
                        if (entry.getValue() != null && entry.getValue().trim().length() > 0) {
                            Double d = Double.parseDouble(entry.getValue().trim().replace("(空白)", "0")) * 100;
                            costprice = Math.round(d);
                        }
                    } else if (entry.getKey().equals("中分类")) {
                        type = entry.getValue().trim();
                    }
                    System.out.print(entry.getKey() + ":" + entry.getValue() + ",");
                }
                GoodTemplate goodTemplate = new GoodTemplate();
                goodTemplate.setStatus(0);
                goodTemplate.setName(goodsName);
                goodTemplate.setShelfLife(shelf_life);
                GoodType goodType = dataAccessManager.getGoodTypeByTitle(type);
                if (goodType != null) {
                    goodTemplate.setTypeId(goodType.getId());
                }
                GoodBrand goodBrand = dataAccessManager.getGoodBrandisRepetition(band);
                if (goodBrand != null) {
                    goodTemplate.setBrandId(goodBrand.getId());
                }
                goodTemplate.setSort(0);
                String pingYin = ChinesePinyinUtil.getPingYin(goodsName);
                String pinYinHeadChar = ChinesePinyinUtil.getPinYinHeadChar(goodsName);
                goodTemplate.setFirstLetter(pinYinHeadChar);
                goodTemplate.setAllLetter(pingYin);
                GoodTemplate t = dataAccessManager.getGoodTemplateisRepetition(goodsName.trim());
                if (t == null) {
                    // dataAccessManager.addGoodTemplate(goodTemplate);
                    // t = goodTemplate;
                }

                GoodPrice goodPrice = new GoodPrice();
                GoodUnit goodUnitByTitle = dataAccessManager.getGoodUnitByTitle(unit);
                if (goodUnitByTitle != null) {
                    String unitId = goodUnitByTitle.getId();
                    goodPrice.setUnitId(unitId);
                }
                goodPrice.setTemplateAllLetter(pingYin);
                goodPrice.setTemplateFirstLetter(pinYinHeadChar);

                goodPrice.setCostPrice(costprice);
                goodPrice.setSellPrice(price);
                goodPrice.setSalePrice(price);
                goodPrice.setMarketPrice(price);
                goodPrice.setMemberPrice(price);
                goodPrice.setColleaguePrice(price);
                goodPrice.setDiscountPrice(price);
                goodPrice.setNum(num);
                goodPrice.setGoodId(t.getId());
                goodPrice.setGoodName(priceName);
                goodPrice.setTitle(priceName);
                goodPrice.setStatus(0);
                goodPrice.setSort(0);
                String pingYins = ChinesePinyinUtil.getPingYin(priceName);
                String pinYinHeadChars = ChinesePinyinUtil.getPinYinHeadChar(priceName);
                goodPrice.setAllLetter(pingYins);
                goodPrice.setFirstLetter(pinYinHeadChars);
                GoodPrice gp = dataAccessManager.getGoodPriceisRepetition(t.getId(), priceName);
                if (gp == null) {
                    // dataAccessManager.addGoodPrice(goodPrice);
                    gp = goodPrice;
                } else {
                    gp.setCostPrice(costprice);
                    gp.setSellPrice(price);
                    gp.setSalePrice(price);
                    gp.setMarketPrice(price);
                    gp.setMemberPrice(price);
                    gp.setColleaguePrice(price);
                    gp.setDiscountPrice(price);
                    dataAccessManager.changeGoodPrice(gp);
                }


//                try {
//                    GoodBarcode goodBarcode = new GoodBarcode();
//                    BigDecimal bd = new BigDecimal(barcode);
//                    goodBarcode.setBarcode(bd.toPlainString());
//                    goodBarcode.setPriceId(gp.getId());
//                    GoodBarcode b = dataAccessManager.getGoodBarcodeByBarcode(goodBarcode.getBarcode());
//                    if (b == null) {
//                        dataAccessManager.addGoodBarcode(goodBarcode);
//                        b = goodBarcode;
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                System.out.println();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return success();
    }

    public JSONObject loadGoodType() {
        try {
            Workbook wb = readExcel("/vue/222.xlsx");

            Sheet sheet = null;
            Row row = null;
            List<Map<String, String>> list = null;
            String cellData = null;
            String columns[] = {"一级分类", "二级分类"};
            if (wb != null) {
                //用来存放表中数据
                list = new ArrayList<Map<String, String>>();
                //获取第一个sheet
                sheet = wb.getSheetAt(0);
                //获取最大行数
                int rownum = sheet.getPhysicalNumberOfRows();
                //获取第一行
                row = sheet.getRow(0);
                //获取最大列数
                int colnum = row.getPhysicalNumberOfCells();
                for (int i = 1; i < rownum; i++) {
                    Map<String, String> map = new LinkedHashMap<String, String>();
                    row = sheet.getRow(i);
                    if (row != null) {

                        for (int j = 0; j < colnum; j++) {

                            cellData = (String) getCellFormatValue(row.getCell(j));
                            map.put(columns[j], cellData);
                        }
                    } else {
                        break;
                    }
                    list.add(map);
                }
            }
            //遍历解析出来的list
            for (Map<String, String> map : list) {


                String type = "";
                String types = "";
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    if (entry.getKey().equals("二级分类")) {
                        type = entry.getValue().trim();
                    } else if (entry.getKey().equals("一级分类")) {
                        types = entry.getValue().trim();
                    }
                    System.out.print(entry.getKey() + ":" + entry.getValue() + ",");
                }

                GoodType g = dataAccessManager.getGoodTypeByTitle(types.trim());

                GoodType goodType = new GoodType();
                goodType.setTitle(type);
                if (g != null) {
                    goodType.setParentId(g.getId());
                }
                dataAccessManager.addGoodType(goodType);

                System.out.println();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return success();
    }

    //读取excel
    public static Workbook readExcel(String filePath) {
        Workbook wb = null;
        if (filePath == null) {
            return null;
        }
        String extString = filePath.substring(filePath.lastIndexOf("."));
        InputStream is = null;
        try {
            is = new FileInputStream(filePath);
            if (".xls".equals(extString)) {
                return wb = new HSSFWorkbook(is);
            } else if (".xlsx".equals(extString)) {
                return wb = new XSSFWorkbook(is);
            } else {
                return wb = null;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return wb;
    }


    public static Object getCellFormatValue(Cell cell) {
        Object cellValue = null;
        if (cell != null) {
            //判断cell类型
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_NUMERIC: {
                    cellValue = String.valueOf(cell.getNumericCellValue());
                    break;
                }
                case Cell.CELL_TYPE_FORMULA: {
                    //判断cell是否为日期格式
                    if (DateUtil.isCellDateFormatted(cell)) {
                        //转换为日期格式YYYY-mm-dd
                        cellValue = cell.getDateCellValue();
                    } else {
                        //数字
                        cellValue = String.valueOf(cell.getNumericCellValue());
                    }
                    break;
                }
                case Cell.CELL_TYPE_STRING: {
                    cellValue = cell.getRichStringCellValue().getString();
                    break;
                }
                default:
                    cellValue = "";
            }
        } else {
            cellValue = "";
        }
        return cellValue;
    }

    public static int getInt(double number) {
        BigDecimal bd = new BigDecimal(number).setScale(0, BigDecimal.ROUND_HALF_UP);
        return Integer.parseInt(bd.toString());
    }

    @Override
    public JSONObject addGoodTemplate() {
        GoodTemplate goodTemplate = new GoodTemplate();
        //商品名字
        String name = getUTF("name", null);
        //
        String bannerUrl = getUTF("bannerUrl", null);
        //品牌ID
        String brandId = getUTF("brandId", null);
        //归属类型
        String typeId = getUTF("typeId", null);
        //0.普通1.价格
        Integer priceType = getIntParameter("priceType", 0);
        //状态 0-可用 1-提交审核 2-审核不通过 3-失效
        Integer status = getIntParameter("status", 0);
        //
        String imageUrl = getUTF("imageUrl", null);
        //商品描述
        String description = getUTF("description", null);
        //保质期(天)
        Integer shelfLife = getIntParameter("shelfLife", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        String remark = getUTF("remark", null);
        goodTemplate.setName(name);
        goodTemplate.setBannerUrl(bannerUrl);
        goodTemplate.setBrandId(brandId);
        goodTemplate.setTypeId(typeId);
        goodTemplate.setPriceType(priceType);
        goodTemplate.setStatus(status);
        goodTemplate.setImageUrl(imageUrl);
        goodTemplate.setDescription(description);
        goodTemplate.setShelfLife(shelfLife);
        goodTemplate.setSort(sort);
        GoodTemplate goodTemplateisRepetition = dataAccessManager.getGoodTemplateisRepetition(name);
        if (goodTemplateisRepetition != null) {
            return fail("该商品模版已存在！！！");
        }
        try {
            SysUser sysUser = getUser();
            if (sysUser != null) {
                goodTemplate.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        goodTemplate.setRemark(remark);
        dataAccessManager.addGoodTemplate(goodTemplate);
        return success();
    }

    /***
     * 添加模板信息（里面含有价格标签）
     * @author oy
     * @date 2019/5/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    @Transactional
    public JSONObject addGoodTemplate2() {
        String goodTemplateEntity = getUTF("goodTemplateEntity");
        if (goodTemplateEntity != null && !goodTemplateEntity.equals("")) {
            GoodTemplate goodTemplate = JSONObject.parseObject(goodTemplateEntity, GoodTemplate.class);
            if (goodTemplate != null) {
                List<GoodPrice> list = goodTemplate.getList();
                String id = goodTemplate.getId();
                String userId = getUser().getId();
                //保存对应的标签
                List<GoodBarcode> barcodes = new ArrayList<>();//要添加的价格标签
                if (list != null && list.size() > 0) {//遍历对应的集合填充一下数据
                    list.forEach(x -> {
                        x.setGoodId(id);
                        x.setRetrieveStatus(0);
                        x.setStatus(0);
                        x.setTop(0);
                        x.setUserId(getUser().getId());
                        GoodBarcode barcode = new GoodBarcode();
                        barcode.setPriceId(x.getId());
                        barcode.setBarcode(x.getBarcode());
                        barcode.setUserId(userId);
                        barcodes.add(barcode);
                    });
                    dataAccessManager.batchAddGoodBarcode(barcodes);//添加对应的条码
                    dataAccessManager.batchAddGoodPrice(list);//保存对应的价格标签
                }
                goodTemplate.setStatus(0);
                goodTemplate.setRetrieveStatus(0);
                goodTemplate.setUserId(userId);
                dataAccessManager.addGoodTemplate(goodTemplate);//保存商品的模板
            }
        }
        return success("添加成功!");
    }

    @Override
    public JSONObject getGoodTemplate() {
        GoodTemplate goodTemplate = dataAccessManager.getGoodTemplate(getId());
        return success(goodTemplate);
    }

    @Override
    public JSONObject getGoodTemplates() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.ALL.getKey());

        List<GoodTemplate> goodTemplates = dataAccessManager.getGoodTemplates(status, getPageStartIndex(getPageSize()), getPageSize(), name, getHttpServletRequest());
        fillGoodTemplate(goodTemplates,getHttpServletRequest());
        int size = goodTemplates.size();
        for (int i = 0; i < size; i++) {
            try {
                SysUser user = SysUserRemoteService.getSysUser(goodTemplates.get(i).getUserId(), getHttpServletRequest());
                if (user.getRealName() != null) {
                    goodTemplates.get(i).setUserName(user.getRealName());
                } else if (user.getDefaultName() != null) {
                    goodTemplates.get(i).setUserName(user.getDefaultName());
                } else {
                    goodTemplates.get(i).setUserName(user.getPhoneNumber());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        int count = dataAccessManager.getGoodTemplateCount(status, name);
        return success(goodTemplates, count);
    }


    private void fillGoodTemplate(List<GoodTemplate> goodTemplates, HttpServletRequest request) {
        int size = goodTemplates.size();
        for (int i = 0; i < size; i++) {
            try {

                SysUser sysUser = SysUserRmoteService.getSysUser(goodTemplates.get(i).getUserId(), request);
                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        goodTemplates.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        goodTemplates.get(i).setUserName(sysUser.getShowName());
                    } else {
                        goodTemplates.get(i).setUserName(sysUser.getDefaultName());
                    }
                }
                if (goodTemplates.get(i).getBrandId() != null) {
                    GoodBrand brand = dataAccessManager.getGoodBrand(goodTemplates.get(i).getBrandId());
                    if (brand != null) {
                        goodTemplates.get(i).setBrandName(brand.getName());
                    }
                }
                String typeId = goodTemplates.get(i).getTypeId();
                GoodType type = null;
                if (typeId != null && !typeId.equals("")) {
                    type = dataAccessManager.getGoodType(typeId);
                }
                if (type != null) {
                    goodTemplates.get(i).setTypeName(type.getTitle());
                }
                int imageCount = dataAccessManager.getGoodImageGoodIdCount(goodTemplates.get(i).getId());
                goodTemplates.get(i).setImageCount(imageCount);
                int labelCount = dataAccessManager.getGoodPriceByGoodIdCount(goodTemplates.get(i).getId());
                goodTemplates.get(i).setLableCount(labelCount);
                goodTemplates.get(i).setLableContent(getPriceTop15(goodTemplates.get(i).getId()));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String getPriceTop15(String goodId) {
        List<GoodPrice> goodPrices = dataAccessManager.getGoodPriceTop15(goodId);
        int size = goodPrices.size();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < size; i++) {
            if (i < size - 1) {
                sb.append(goodPrices.get(i).getTitle() + ",");
            } else {
                sb.append(goodPrices.get(i).getTitle());
            }
        }
        return sb.toString();
    }

    @Override
    public JSONObject getGoodTemplateAll() {

        List<GoodTemplate> goodTemplates = dataAccessManager.getGoodTemplateAll(getHttpServletRequest());
        return success(goodTemplates);
    }

    @Override
    public JSONObject getGoodTemplateCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        int count = dataAccessManager.getGoodTemplateCount(status, keyWord);
        return success();
    }

    @Override
    @Transactional
    public JSONObject changeGoodTemplate() {
        GoodTemplate goodTemplate = dataAccessManager.getGoodTemplate(getId());
        //
        String id = getUTF("id", null);
        //商品名字
        String name = getUTF("name", null);
        //
        String bannerUrl = getUTF("bannerUrl", null);
        //品牌ID
        String brandId = getUTF("brandId", null);
        //归属类型
        String typeId = getUTF("typeId", null);
//        //0.普通1.价格
//        Integer priceType = getIntParameter("priceType", 0);
        //状态 0-可用 1-禁用
        Integer status = getIntParameter("status", 0);
        //
        String imageUrl = getUTF("imageUrl", null);
        //商品描述
        String description = getUTF("description", null);
        //保质期(天)
        Integer shelfLife = getIntParameter("shelfLife", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        String remark = getUTF("remark", null);
        if (!goodTemplate.getName().equals(name)) {
            GoodTemplate goodTemplateisRepetition = dataAccessManager.getGoodTemplateisRepetition(name);
            if (goodTemplateisRepetition != null) {
                return fail("该商品模版已存在！！！");
            }
        }
        goodTemplate.setId(id);
        goodTemplate.setName(name);
        goodTemplate.setBannerUrl(bannerUrl);
        goodTemplate.setBrandId(brandId);
        goodTemplate.setTypeId(typeId);
//        goodTemplate.setPriceType(priceType);
        goodTemplate.setStatus(status);

        goodTemplate.setImageUrl(imageUrl);
        goodTemplate.setDescription(description);
        goodTemplate.setShelfLife(shelfLife);
        goodTemplate.setSort(sort);
        goodTemplate.setRemark(remark);
        dataAccessManager.changeGoodTemplate(goodTemplate);

        //更新对应的价格标签状态
        Map<String, Object> map = new HashMap<>();
        map.put("status", status);
        map.put("goodId", id);

        //异步更新状态
        updateSupplychainGoodStatusForAsync(map);
        dataAccessManager.updateGoodPriceStatusByGoodId(map);//更新价格标签表
        return success();
    }

    /**
     * 异步更新商店的商品信息
     *
     * @param map
     * @return void
     * @author oy
     * @date 2019/5/27
     */
    @Async
    public void updateSupplychainGoodStatusForAsync(Map<String, Object> map) {
        SupplyChainGoodRemoteService.updateSupplychainGoodStatus(map, getHttpServletRequest());
    }


    /**
     * 商家端：填写商品名称 获取对应的模糊查询模板（防止用户加入太多垃圾模板）
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/3
     */
    @Override
    public JSONObject getGoodTemplateByName() {
        String goodName = getUTF("goodName");
        if (goodName == null || goodName.equals("")) {//说明没有内容
            return success();
        }
        List<GoodTemplate> goodTemplateByName = dataAccessManager.getGoodTemplateByName(goodName, null);//从两个标准模板里面查找
        //直接返回就行
        return success(goodTemplateByName);
    }

    /**
     * 根据品牌获取对应的模板
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/15
     */
    @Override
    public JSONObject getGoodTemplatesByBrandId() {
        String brandId = getUTF("brandId");
        if (brandId == null || brandId.equals("")) {
            return fail("品牌id不能为空！");
        }
        List<GoodTemplate> list = dataAccessManager.getGoodtemplateByBrandId(brandId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list", list);
        return jsonObject;
    }

    /**
     * 展示模板数据，供添加商店商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/23
     */
    @Override
    public JSONObject listGoodTemplatesForAddChainGood() {
        String shopId = getUTF("shopId", null);

        //通过多对多查找出 对应的商品模板和价格表的模板数据
        List<GoodTemplate> list = dataAccessManager.listGoodTemplatesForAddChainGood(null);
        //所有的信息
        //需要筛选一下 去查询当前商店 仓库id
        if (shopId != null && !shopId.equals("")) {
            List<SupplychainGood> supplychainGoodByShopId = SupplyChainGoodRemoteService.getSupplychainGoodByShopId(shopId, getHttpServletRequest());
            List<String> collect = supplychainGoodByShopId.stream().map(SupplychainGood::getPriceId).collect(Collectors.toList());
            if (supplychainGoodByShopId != null && supplychainGoodByShopId.size() > 0) {
                if (list != null && list.size() > 0) {
                    list.forEach(x -> {
                        List<GoodPrice> list1 = x.getList();
                        if (list1 != null && list1.size() > 0) {
                            List<GoodPrice> collect1 = list1.stream().filter(item -> !collect.contains(item.getId())).collect(Collectors.toList());
                            x.setList(collect1);
                        }
                    });
                }
            }
        }
        return success(list);
    }

    /**
     * 查询商品模版名是否存在
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/3
     */
    @Override
    public JSONObject getGoodTemplateisRepetition() {
        String name = getUTF("name");
        GoodTemplate goodTemplateisRepetition = dataAccessManager.getGoodTemplateisRepetition(name);

        if (goodTemplateisRepetition != null) {
            //商品类别重复
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code", 1);
            return success(jsonObject);
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code", 0);
            return success(jsonObject);
        }
    }

    @Override
    @Transactional
    public JSONObject deleteGoodTemplate() {
        String ids = getUTF("ids");
        if (ids != null) {
            List<String> idList = JSONObject.parseArray(ids, String.class);
            collectRetrice(idList);

            //根据当前的商品id 去删除对应的价格标签
            dataAccessManager.batchDeleteGoodPriceByGoodId(idList);
            dataAccessManager.deleteGoodTemplate(idList);
        } else {
            return fail("ids不能为空");
        }
        return success("删除成功！");
    }

    private void collectRetrice(List<String> list) {
        if (list != null && list.size() > 0) {
            List<SysRetrieve> list1 = new ArrayList<>();
            list.forEach(x -> {
                GoodTemplate goodTemplate = dataAccessManager.getGoodTemplate(x);
                SysRetrieve sysRetrieve = new SysRetrieve();
                sysRetrieve.setRetrieveTableId(x);
                sysRetrieve.setRetrieveTableName(GoodTableNameEnum.goodTemplate.getValue());
                sysRetrieve.setTitle(goodTemplate.getName());
                sysRetrieve.setCreateTime(new Date());
                sysRetrieve.setUserId(getUser().getId());
                list1.add(sysRetrieve);
            });
            //添加
            try {
                SysRetrieceRemoteService.addSysRetrieves(list1, getHttpServletRequest());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 商家绑定商品选择列表
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/7
     */
    @Override
    public JSONObject listGoodTemplatesForAddChainGood2() {
        //搜索条件  商品名称或者条形码
        String shopId = getUTF("shopId");
        String goodName = getUTF("goodName", null);
        String goodBarCodeName = getUTF("goodBarcode", null);

        List<SupplychainGood> supplychainGoodByShopId = SupplyChainGoodRemoteService.getSupplychainGoodByShopId(shopId, getHttpServletRequest());

        List<String> priceIds = null;//已有的价格id
        if (supplychainGoodByShopId != null && supplychainGoodByShopId.size() > 0) {
            priceIds = supplychainGoodByShopId.stream().map(SupplychainGood::getPriceId).collect(Collectors.toList());
        }

        //barcode
        List<String> barcodePriceId = new ArrayList<>();//条码价格id 供价格id 筛选
        //是否从条码里面拿商品价格id
        boolean isPriceId = false;
        Map<String, List<GoodBarcode>> goodBarcodeCache = new HashMap<>();//存放条码缓存
        List<GoodBarcode> goodBarcodeByBarcodeLike = null;
        if (goodBarCodeName != null && !goodBarCodeName.equals("")) {
            goodBarcodeByBarcodeLike = dataAccessManager.getGoodBarcodeByBarcodeLike(goodBarCodeName);
            isPriceId = true;
        } else {
            goodBarcodeByBarcodeLike = dataAccessManager.getGoodBarcodeAll();
        }

        if (goodBarcodeByBarcodeLike != null && goodBarcodeByBarcodeLike.size() > 0) {
            //统计当前条码数量（因为一个价格标签有多个条码的存在）
            int count = (int) goodBarcodeByBarcodeLike.stream().map(GoodBarcode::getPriceId).distinct().count();
            for (int i = 0; i < count; i++) {
                //判断当前价格标签是否加入到条码中
                GoodBarcode goodBarcode = goodBarcodeByBarcodeLike.get(i);
                String priceId = goodBarcode.getPriceId();
                if (!barcodePriceId.contains(priceId)) {
                    barcodePriceId.add(priceId);
                    List<GoodBarcode> list = new ArrayList<>();
                    list.add(goodBarcode);
                    goodBarcodeCache.put(priceId, list);
                } else {
                    List<GoodBarcode> barcodes = goodBarcodeCache.get(priceId);
                    barcodes.add(goodBarcode);
                    goodBarcodeCache.put(priceId, barcodes);
                }
            }
        } else {
            return success(goodBarcodeByBarcodeLike, 0);
        }

        //goodTemplate
        List<String> goodTemplates = new ArrayList<>(); //商品模板id
        Map<String, GoodTemplate> goodTemplateeCache = new HashMap<>(); //商品缓存
        List<GoodTemplate> gts = null;
        if (goodName != null && !goodName.equals("")) {
            gts = dataAccessManager.getGoodTemplatesByGoodNameLikeAndGoodStatus(goodName, 0);
            if (gts == null || gts.size() == 0) {
                return success(gts, 0);
            } else {
                int count = gts.size();
                for (int i = 0; i < count; i++) {
                    goodTemplates.add(gts.get(i).getId());
                    goodTemplateeCache.put(gts.get(i).getId(), gts.get(i));
                }
            }
        }

        //价格标签 priceTitle priceStatus
        int count = 0;
        List<GoodPrice> goodPriceList = null;
        if (isPriceId) {
            if (priceIds != null && priceIds.size() > 0) {
                goodPriceList = dataAccessManager.getGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(null, 0, goodTemplates, barcodePriceId, priceIds, getPageStartIndex(getPageSize()), getPageSize());
            } else {
                goodPriceList = dataAccessManager.getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(null, 0, goodTemplates, barcodePriceId, getPageStartIndex(getPageSize()), getPageSize());
            }
        } else {
            if (priceIds != null && priceIds.size() > 0) {
                goodPriceList = dataAccessManager.getGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(null, 0, goodTemplates, null, priceIds, getPageStartIndex(getPageSize()), getPageSize());
            } else {
                goodPriceList = dataAccessManager.getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(null, 0, goodTemplates, null, getPageStartIndex(getPageSize()), getPageSize());
            }
        }
        if (goodPriceList != null && goodPriceList.size() > 0) {
            //查询总条数
            if (isPriceId) {
                if (priceIds != null && priceIds.size() > 0) {
                    count = dataAccessManager.getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(null, 0, goodTemplates, barcodePriceId, priceIds);
                } else {
                    count = dataAccessManager.getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(null, 0, goodTemplates, barcodePriceId);
                }
            } else {
                if (priceIds != null && priceIds.size() > 0) {
                    count = dataAccessManager.getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(null, 0, goodTemplates, null, priceIds);
                } else {
                    count = dataAccessManager.getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(null, 0, goodTemplates, null);
                }
            }

            List<GoodPriceCombinationVO> gpcv = new ArrayList<>();
            if (goodPriceList != null && goodPriceList.size() > 0) {
                goodPriceList.forEach(x -> {
                    GoodPriceCombinationVO vo = new GoodPriceCombinationVO();
                    vo.setPriceTitle(x.getTitle());
                    vo.setPriceId(x.getId());
                    vo.setPriceNum(x.getNum());
                    vo.setPriceIcon(x.getIcon());
                    vo.setPriceSort(x.getSort());
                    vo.setPriceStatus(x.getStatus());
                    vo.setPriceTitle(x.getTitle());
                    vo.setPriceRemark(x.getRemark());
                    vo.setCostPrice(x.getCostPrice());
                    vo.setSalePrice(x.getSalePrice());
                    vo.setSellPrice(x.getSellPrice());
                    vo.setColleaguePrice(x.getColleaguePrice());
                    vo.setDiscountPrice(x.getDiscountPrice());
                    vo.setMarketPrice(x.getMarketPrice());
                    vo.setMemberPrice(x.getMemberPrice());

                    //用户
                    String userId = x.getUserId();
                    if (userId != null) {
                        SysUser sysUser = SysUserRmoteService.getSysUser(userId, getHttpServletRequest());
                        if (sysUser != null) {
                            if (sysUser.getRealName() != null) {
                                vo.setCreateUserName(sysUser.getRealName());
                            } else if (sysUser.getShowName() != null) {
                                vo.setCreateUserName(sysUser.getShowName());
                            } else {
                                vo.setCreateUserName(sysUser.getDefaultName());
                            }
                        }
                    }

                    //商品模板
                    GoodTemplate good = null;
                    if (goodTemplateeCache != null && goodTemplateeCache.entrySet().size() != 0) {
                        good = goodTemplateeCache.get(x.getGoodId());
                    } else {
                        good = dataAccessManager.getGoodTemplate(x.getGoodId());
                    }
                    String typeId = null;
                    if (good != null) {
                        vo.setGoodTemplateId(good.getId());
                        vo.setGoodName(good.getName());
                        vo.setShelfLife(good.getShelfLife());
                        vo.setImageUrl(good.getImageUrl());
                        vo.setGoodStatus(good.getStatus());
                        vo.setGoodBanner(good.getBannerUrl());
                        //品牌
                        String brandId = good.getBrandId();
                        if (brandId != null && !brandId.equals("")) {
                            GoodBrand goodBrand = dataAccessManager.getGoodBrand(brandId);
                            if (goodBrand != null) {
                                vo.setBrandId(goodBrand.getId());
                                vo.setBrandName(goodBrand.getName());
                                vo.setBrandLogo(goodBrand.getLogo());
                            }
                        }
                        typeId = good.getTypeId();
                    }

                    List<GoodBarcode> goodBarcode = null;
                    if (goodBarcodeCache != null && goodBarcodeCache.entrySet().size() != 0) {
                        goodBarcode = goodBarcodeCache.get(x.getId());
                    } else {
                        goodBarcode = dataAccessManager.getBarcodesByPriceId(x.getId());
                    }

                    if (goodBarcode != null && goodBarcode.size() > 0) {
                        String barcodeId = goodBarcode.stream().map(GoodBarcode::getId).distinct().collect(Collectors.joining(","));
                        String barcodeName = goodBarcode.stream().map(GoodBarcode::getBarcode).distinct().collect(Collectors.joining(","));
                        vo.setBarcodeId(barcodeId);//条码id
                        vo.setBarcode(barcodeName);//条码
                    }

                    //类别
                    GoodType type2 = null;
                    if (typeId != null && !typeId.equals("")) {
                        type2 = dataAccessManager.getGoodType(typeId);//二级
                        if (type2 != null) {
                            vo.setTypeNameTwoName(type2.getTitle());
                            vo.setTypeNameTwoId(type2.getId());
                            vo.setTypeIconTwo(type2.getIcon());
                            vo.setTypeImageTwo(type2.getImage());
                            String parentId = type2.getParentId();
                            GoodType type1 = null;
                            if (parentId != null && !parentId.equals("")) {
                                type1 = dataAccessManager.getGoodType(parentId);//一级
                            }
                            if (type1 != null) {
                                vo.setTypeNameOneName(type1.getTitle());
                                vo.setTypeNameOneId(type1.getId());
                                vo.setTypeIconOne(type1.getIcon());
                                vo.setTypeImageOne(type1.getImage());
                            }
                        }
                    }

                    //单位
                    String unitId = x.getUnitId();
                    if (unitId != null && !unitId.equals("")) {
                        vo.setUnitTitleId(unitId);
                        GoodUnit goodUnit = dataAccessManager.getGoodUnit(unitId);
                        if (goodUnit != null) {
                            vo.setUnitTitle(goodUnit.getTitle());
                        }
                    }
                    gpcv.add(vo);
                });
            }
            return success(gpcv, count);
        }
        return success(goodPriceList, count);
    }


}