package com.tuoniaostore.good.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.good.GoodShopCatStatus;
import com.tuoniaostore.commons.support.good.GoodShopCartRemoteService;
import com.tuoniaostore.commons.support.payment.PaymentCurrencyAccountService;
import com.tuoniaostore.commons.support.supplychain.SupplyChainGoodRemoteService;
import com.tuoniaostore.datamodel.good.GoodShopCart;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.vo.supplychain.SupplychainGoodVo;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.datamodel.vo.supplychain.SupplychainGoodAndShopVO;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodShopCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service("goodShopCartService")
public class GoodShopCartServiceImpl extends BasicWebService implements GoodShopCartService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;


    @Override
    public JSONObject addGoodShopCart() {
        SysUser sysUser= null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(sysUser == null){
            return fail(1001,"用户不能为空");
        }
        String listjson = getUTF("list", null);
        List<OrderGoodSnapshot> orderGoodSnapshotsList = JSONArray.parseArray(listjson, OrderGoodSnapshot.class);
        HashMap<String, Object> map = new HashMap<>();
        map.put("userId",sysUser.getId());
        List<GoodShopCart> cartBynumber = dataAccessManager.getGoodShopCartByMap(map);
        String sequenceNumber ="";
        if(cartBynumber.size()>0){
            sequenceNumber = cartBynumber.get(0).getSequenceNumber();
        }else {
            sequenceNumber=uniquePrimaryKey();
        }

        //添加购物车
        for (OrderGoodSnapshot orderGoodSnapshot : orderGoodSnapshotsList) {
            map.put("goodsId",orderGoodSnapshot.getGoodId());
            List<GoodShopCart> goodShopCartByMap = dataAccessManager.getGoodShopCartByMap(map);
            if(goodShopCartByMap.size() <= 0){
                GoodShopCart goodShopCart = new GoodShopCart();
                goodShopCart.setGoodId(orderGoodSnapshot.getGoodId());
                goodShopCart.setGoodNumber(orderGoodSnapshot.getNormalQuantity());
                goodShopCart.setUserId(sysUser.getId());
                goodShopCart.setSequenceNumber(sequenceNumber);
                goodShopCart.setStatus(GoodShopCatStatus.UNPURCHASED.getKey());
                goodShopCart.setCreateTime(new Date());
                dataAccessManager.addGoodShopCart(goodShopCart);
            }
        }
         return success();
    }


    @Override
    public JSONObject getGoodShopCartByUserId() {
        SysUser sysUser= null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(sysUser == null){
            return fail(1001,"用户不能为空");
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("userId", sysUser.getId());

//        List<GoodShopCart> list = dataAccessManager.getGoodShopCartByMap(map);
        List<GoodShopCart> list = dataAccessManager.getGoodShopCartByMapByPage(map,getPageStartIndex(getPageSize()),getPageSize());

      /*  String ids = "";
        for (GoodShopCart goodShopCart : list) {
            ids+=goodShopCart.getGoodId()+",";
        }*/
        List<SupplychainGoodAndShopVO> goodList=null;
        try {
            //远程调用查询商品数据
            goodList = GoodShopCartRemoteService.getSupplychainGoodByGoodShopCartList(list , getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success(goodList);
    }
    /**
     * 购物车数量修改
     * @author sqd
     * @date 2019/4/9
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject changeGoodShopCart() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user == null){
            fail("用户登录失效");
        }
        HashMap<String, Object> map = new HashMap<>();
        int goodNumber = getIntParameter("goodNumber");
        String id = getUTF("id");
        map.put("goodNumber",goodNumber);
        map.put("userId",user.getId());
        map.put("id",id);
        if(goodNumber <= 0){
            fail("商品数量不能小于1");
        }else{
            dataAccessManager.changeGoodShopCart(map);
        }
        return success();
    }


    /**
     * 下单付款确认界面数据查询
     * @author sqd
     * @date 2019/4/10
     * @param
     * @return
     */
    @Override
    public JSONObject selectGoodInventory() {
        String mapStr =getUTF("map");
        Map map = JSONObject.parseObject(mapStr, Map.class);
        String goodIds = getUTF("goodIds");
        List<SupplychainGoodAndShopVO> goodList=null;
        JSONObject jsonObject = new JSONObject();
        try {
            //远程调用查询商品数据
            goodList = GoodShopCartRemoteService.getSupplychainGoodByUserId(goodIds, getHttpServletRequest());
            double orderAmountMoney=0;
            double distributionCost=0;
            if(goodList != null && goodList.size() > 0 ){
                for (SupplychainGoodAndShopVO supplychainGoodAndShopVO : goodList) {
                    long shopdistributionFee = 0;//店铺商品费用
                    for (SupplychainGoodVo goodVo : supplychainGoodAndShopVO.getList()) {
                        Integer goodNumber = (Integer) map.get(goodVo.getId());
                        goodVo.setGoodNumber(goodNumber);
                        if(goodVo.getGoodNumber()!=null){
                            orderAmountMoney+=goodVo.getSellPrice()*goodVo.getGoodNumber();
                            shopdistributionFee+=orderAmountMoney;
                        }
                    }
                    long distributionFee = supplychainGoodAndShopVO.getDistributionCost();
                   /*  配送费不根据起订值算  //是否需要运费
                    if(supplychainGoodAndShopVO.getMoq() == null ||  shopdistributionFee >= supplychainGoodAndShopVO.getMoq()){
                        distributionFee = 0;
                    }*/
                    //配送费
                    distributionCost+=distributionFee;
                }
            }else{
                return  fail("商品已下架");
            }
            //远程调用支付模块查询余额
            JSONObject paymentCurrencyAccount = PaymentCurrencyAccountService.getPaymentCurrencyAccount(getHttpServletRequest());
            Object balance = null;
            Object vipbalance = null;
            try {
                if(paymentCurrencyAccount != null){
                    balance = (Object)paymentCurrencyAccount.get("balance");
                    vipbalance =(Object) paymentCurrencyAccount.get("vipBalance");
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            jsonObject.put("balance",balance);        //商家余额
            jsonObject.put("vipbalance",vipbalance);  //会员余额
            jsonObject.put("goodList",goodList);    //商品清单
            jsonObject.put("orderAmountMoney",orderAmountMoney);  //订单总金额
            jsonObject.put("distributionCost",distributionCost); // 优惠金额
            jsonObject.put("practicalMoney",(orderAmountMoney+distributionCost)); //实际金额
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success(jsonObject);
    }

    /**
     * 查询购物车商品
     * @author sqd
     * @date 2019/4/11
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getGoodShopCartByUserIdAndGoodId() {
        String userId = getUTF("userId");
        String goodId = getUTF("goodId");
        GoodShopCart goodShopCartByUserIdAndGoodId = dataAccessManager.getGoodShopCartByUserIdAndGoodId(userId, goodId);
        return success(goodShopCartByUserIdAndGoodId);
    }

    @Override
    public JSONObject changeGoodShopCartStauts() {
        HashMap<String, Object> map = new HashMap<>();
        SysUser user = getUser();
        String status = getUTF("status");
        String goodId = getUTF("goodId");
        map.put("status",status);
        map.put("goodId",goodId);
        map.put("userId",user.getId());
        if(!status.isEmpty() && !goodId.isEmpty() && !user.getId().isEmpty()){
            dataAccessManager.changeGoodShopCartStauts(map);
        }
        return success();
    }

    @Override
    public JSONObject deleteGoodShopCartIds() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user == null){
            fail("用户登录失效");
        }
        String ids = getUTF("ids");
        String[] split = ids.split(",");
        List<String> idlist = Arrays.asList(split);
        dataAccessManager.deleteGoodShopCart(idlist,user.getId());
        return success();
    }

    /**
     * 购物车信息统计
     * @author oy
     * @date 2019/4/16
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getGoodShopCartMessage() {
        //获取当前用户
        SysUser user = getUser();
        String userId = user.getId();

        //通过用户id 获取购物车统计信息
        Map<String,Object> map = new HashMap<>();
        map.put("userId",userId);

        Integer sum = null;//购物车数量
        List<String> goodIds = null;

        List<GoodShopCart> goodShopCartByMap = dataAccessManager.getGoodShopCartByMap(map);
        if(goodShopCartByMap != null){
            goodIds = goodShopCartByMap.stream().map(GoodShopCart::getGoodId).collect(Collectors.toList());//商品id
            sum = goodShopCartByMap.stream().mapToInt(GoodShopCart::getGoodNumber).sum();//购物车数量
        }else{
            sum = 0;
        }
        Long totalMoney = 0L;//返回的总金额
        JSONObject jsonObject = new JSONObject();
        if(goodIds != null && goodIds.size() > 0){
            //远程调用chain_good服务 获取商品的信息集合
            List<SupplychainGood> supplychainGoodBindRelationship = SupplyChainGoodRemoteService.getSupplychainGoodByIds(goodIds, getHttpServletRequest());
            //将数据封装一下
            if(supplychainGoodBindRelationship != null && supplychainGoodBindRelationship.size() > 0){
                for (int i = 0; i < supplychainGoodBindRelationship.size(); i++) {
                    SupplychainGood supplychainGood = supplychainGoodBindRelationship.get(i);
                    //计算单价乘以数量
                    Optional<GoodShopCart> first1 = goodShopCartByMap.stream().filter(item -> item.getGoodId().equals(supplychainGood.getId())).findFirst();
                    if(first1.isPresent()){//存在
                        GoodShopCart goodShopCart = first1.get();
                        Integer goodNumber = goodShopCart.getGoodNumber();
                        Long sellPrice = supplychainGood.getSellPrice();
                        if(goodNumber >= 1){//商品为1
                            totalMoney += sellPrice * goodNumber;
                        }else{
                            totalMoney += sellPrice;
                        }
                    }
                }
            }
        }
        jsonObject.put("sum",sum);//购物车总数量
        jsonObject.put("totalMoney",totalMoney);//总价钱
        return success(jsonObject);
    }

    @Override
    public JSONObject addGoodShopCartgood() {
        SysUser user = getUser();
        HashMap<String, Object> map = new HashMap<>();
        String goodsId = getUTF("goodsId");
        Integer startingQuantity = getIntParameter("startingQuantity",1);

        //判断当前商品是否下架 或者 失效？
        int status = 1;
        SupplychainGood good = SupplyChainGoodRemoteService.getSupplychainGoodByIdAndStatus(goodsId,status,getHttpServletRequest());
        if(good == null){
            return fail("该商品已失效或者已下架!");
        }

        map.put("userId",user.getId());
        map.put("goodsId",goodsId);
        //获取序列号
        List<GoodShopCart> cartBynumber = dataAccessManager.getGoodShopCartByMap(map);
        String sequenceNumber ="";
        if(cartBynumber.size()>0){
            sequenceNumber = cartBynumber.get(0).getSequenceNumber();
        }else {
            sequenceNumber=uniquePrimaryKey();
        }

        List<GoodShopCart> shopCarts = dataAccessManager.getGoodShopCartByMap(map);
        if(shopCarts.size() > 0){
            map.put("goodNumber",shopCarts.get(0).getGoodNumber() + 1 * startingQuantity);
            map.put("id",goodsId);//changeGoodShopCart接口参数 id为商品id
            dataAccessManager.changeGoodShopCart(map);
        }else{
            GoodShopCart goodShopCart = new GoodShopCart();
            goodShopCart.setGoodId(goodsId);
            goodShopCart.setUserId(user.getId());
            goodShopCart.setCreateTime(new Date());
            goodShopCart.setGoodNumber(1 * startingQuantity);
            goodShopCart.setStatus(1);
            goodShopCart.setSequenceNumber(sequenceNumber);
            dataAccessManager.addGoodShopCart(goodShopCart);
        }
        return success();
    }

    @Override
    public JSONObject getGoodShopCartListByUserId() {
        String userId = getUTF("userId");
        Map<String,Object> map = new HashMap<>();
        map.put("userId",userId);
        List<GoodShopCart> goodShopCartByMap = dataAccessManager.getGoodShopCartByMap(map);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",goodShopCartByMap);
        return jsonObject;
    }


}
