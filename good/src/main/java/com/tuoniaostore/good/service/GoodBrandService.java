package com.tuoniaostore.good.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 商品归属品牌
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodBrandService {

    JSONObject addGoodBrand();

    JSONObject getGoodBrand();

    JSONObject getGoodBrands();

    JSONObject getGoodBrandCount();
    JSONObject getGoodBrandAll();

    JSONObject changeGoodBrand();

    /***
     * 根据品牌brandIds查找对应的记录
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getBarcodesByBrandIds();

    /***
     * 根据品牌brandIds查找对应的记录
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getBarcodesByBrandName();

    /**
     * 根据关键字和状态搜索商品品牌
     * @author oy
     * @date 2019/4/20
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getBrandByBrandNameAndStatus();


    /**
     * 删除对应的品牌
     * @author oy
     * @date 2019/4/20
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject deleteGoodBrand();


    JSONObject getBrandByHomePageGood();

    /**
     * 查询品牌名是否存在
     * @author sqd
     * @date 2019/5/3
     * @param
     * @return
     */
    JSONObject getGoodBrandisRepetition();
}
