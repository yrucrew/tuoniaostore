package com.tuoniaostore.good.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.good.GoodTemplatePropertiesValue;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodTemplatePropertiesValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("goodTemplatePropertiesValueService")
public class GoodTemplatePropertiesValueServiceImpl extends BasicWebService implements GoodTemplatePropertiesValueService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Override
    public JSONObject addGoodTemplatePropertiesValue() {
        GoodTemplatePropertiesValue goodTemplatePropertiesValue = new GoodTemplatePropertiesValue();
        //
        String id = getUTF("id", null);
        //
        String itemTemplateId = getUTF("itemTemplateId", "0");
        //
        String itemTemplatePropertiesId = getUTF("itemTemplatePropertiesId", "0");
        //
        String content = getUTF("content", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        Integer status = getIntParameter("status", 0);
        //
        String remark = getUTF("remark", null);
        //
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        goodTemplatePropertiesValue.setId(id);
        goodTemplatePropertiesValue.setItemTemplateId(itemTemplateId);
        goodTemplatePropertiesValue.setItemTemplatePropertiesId(itemTemplatePropertiesId);
        goodTemplatePropertiesValue.setContent(content);
        goodTemplatePropertiesValue.setSort(sort);
        try {
            SysUser sysUser=getUser();
            if(sysUser!=null){
                goodTemplatePropertiesValue.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        goodTemplatePropertiesValue.setStatus(status);
        goodTemplatePropertiesValue.setRemark(remark);
        goodTemplatePropertiesValue.setRetrieveStatus(retrieveStatus);
        dataAccessManager.addGoodTemplatePropertiesValue(goodTemplatePropertiesValue);
        return success();
    }

    @Override
    public JSONObject getGoodTemplatePropertiesValue() {
        GoodTemplatePropertiesValue goodTemplatePropertiesValue = dataAccessManager.getGoodTemplatePropertiesValue(getId());
        return success(goodTemplatePropertiesValue);
    }

    @Override
    public JSONObject getGoodTemplatePropertiesValues() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<GoodTemplatePropertiesValue> goodTemplatePropertiesValues = dataAccessManager.getGoodTemplatePropertiesValues(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getGoodTemplatePropertiesValueCount(status, name);
        return success(goodTemplatePropertiesValues, count);
    }

    @Override
    public JSONObject getGoodTemplatePropertiesValueAll() {
        List<GoodTemplatePropertiesValue> goodTemplatePropertiesValues = dataAccessManager.getGoodTemplatePropertiesValueAll();
        return success(goodTemplatePropertiesValues);
    }

    @Override
    public JSONObject getGoodTemplatePropertiesValueCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getGoodTemplatePropertiesValueCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeGoodTemplatePropertiesValue() {
        GoodTemplatePropertiesValue goodTemplatePropertiesValue = dataAccessManager.getGoodTemplatePropertiesValue(getId());
        //
        String id = getUTF("id", null);
        //
        String itemTemplateId = getUTF("itemTemplateId", "0");
        //
        String itemTemplatePropertiesId = getUTF("itemTemplatePropertiesId", "0");
        //
        String content = getUTF("content", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //创建人
        String userId = getUTF("userId", null);
        //
        Integer status = getIntParameter("status", 0);
        //
        String remark = getUTF("remark", null);
        goodTemplatePropertiesValue.setId(id);
        goodTemplatePropertiesValue.setItemTemplateId(itemTemplateId);
        goodTemplatePropertiesValue.setItemTemplatePropertiesId(itemTemplatePropertiesId);
        goodTemplatePropertiesValue.setContent(content);
        goodTemplatePropertiesValue.setSort(sort);
        goodTemplatePropertiesValue.setUserId(userId);
        goodTemplatePropertiesValue.setStatus(status);
        goodTemplatePropertiesValue.setRemark(remark);
        dataAccessManager.changeGoodTemplatePropertiesValue(goodTemplatePropertiesValue);
        return success();
    }

}
