package com.tuoniaostore.good.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodTemplateService {

    JSONObject addGoodTemplate();

    JSONObject addGoodTemplate2();

    JSONObject getGoodTemplate();

    JSONObject getGoodTemplates();

    JSONObject getGoodTemplateCount();
    JSONObject getGoodTemplateAll();

    JSONObject changeGoodTemplate();

    /**
     * 商家端：填写商品名称 获取对应的模糊查询模板（防止用户加入太多垃圾模板）
     * @author oy
     * @date 2019/4/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getGoodTemplateByName();

    /**
     * 根据品牌获取对应的模板
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getGoodTemplatesByBrandId();

    /**
     * 展示模板数据，供添加商店商品
     * @author oy
     * @date 2019/4/23
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listGoodTemplatesForAddChainGood();

    /**
     * 查询商品名称是否存在
     * @author sqd
     * @date 2019/5/3
     * @param
     * @return
     */
    JSONObject getGoodTemplateisRepetition();

    /***
     * 删除商品
     * @author oy
     * @date 2019/5/5
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject deleteGoodTemplate();

    /**
     * 商家绑定商品选择列表
     * @author oy
     * @date 2019/5/7
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject listGoodTemplatesForAddChainGood2();
    JSONObject loadGood();

}
