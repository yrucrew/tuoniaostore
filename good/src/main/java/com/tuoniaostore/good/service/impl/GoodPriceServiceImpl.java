package com.tuoniaostore.good.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.file.oss.AliyunOSSUtils;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.commons.support.good.GoodBrandRemoteService;
import com.tuoniaostore.commons.support.good.GoodTypeRemoteService;
import com.tuoniaostore.commons.support.good.GoodUnitRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplyChainGoodRemoteService;
import com.tuoniaostore.commons.utils.ChinesePinyinUtil;
import com.tuoniaostore.commons.utils.ExcelUtils;
import com.tuoniaostore.datamodel.good.*;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("goodPriceService")
public class GoodPriceServiceImpl extends BasicWebService implements GoodPriceService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Override
    public JSONObject addGoodPrice() {
        GoodPrice goodPrice = new GoodPrice();
        //标签名称
        String title = getUTF("title");
        String barcode = getUTF("barcode", null);
        //成本价
        Long costPrice = getLongParameter("costPrice", 0);
        //建议售价
        Long salePrice = getLongParameter("salePrice", 0);
        //交易价
        Long sellPrice = getLongParameter("sellPrice", 0);
        //同行价
        Long colleaguePrice = getLongParameter("colleaguePrice", 0);
        //市场价
        Long marketPrice = getLongParameter("marketPrice", 0);
        //会员价
        Long memberPrice = getLongParameter("memberPrice", 0);
        //促销价
        Long discountPrice = getLongParameter("discountPrice", 0);
        //备注
        String remark = getUTF("remark", null);
        //商品ID
        String goodId = getUTF("goodId");
        //数量
        Integer num = getIntParameter("num", 0);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //小图标
        String icon = getUTF("icon", null);
        //单位名称
        String unitId = getUTF("unitId", null);
        //默认选中项
        Integer top = getIntParameter("top", 0);
        //0.可用1.禁用
        Integer status = getIntParameter("status", 0);
        if(title != null && !title.equals("") && goodId != null && !goodId.equals("")){
            GoodPrice goodPriceisRepetition = dataAccessManager.getGoodPriceisRepetition(goodId, title);
            if(goodPriceisRepetition != null){
                return fail("该标签已经存在！");
            }
        }else{
            return fail("标签名或商品id不能为空！");
        }
        goodPrice.setTitle(title);
        goodPrice.setCostPrice(costPrice);
        goodPrice.setSalePrice(salePrice);
        goodPrice.setSellPrice(sellPrice);
        goodPrice.setColleaguePrice(colleaguePrice);
        goodPrice.setMarketPrice(marketPrice);
        goodPrice.setMemberPrice(memberPrice);
        goodPrice.setDiscountPrice(discountPrice);
        goodPrice.setRemark(remark);
        goodPrice.setGoodId(goodId);
        goodPrice.setNum(num);
        try {
            SysUser sysUser = getUser();
            if (sysUser != null) {
                goodPrice.setUserId(sysUser.getUserId());
            }
            if (barcode != null && barcode.trim().length() > 0) {
                GoodBarcode goodBarcode = dataAccessManager.getGoodBarcodeByBarcode(barcode);
                if (goodBarcode != null) {
                    return fail("该条码已存在");
                } else {
                    GoodBarcode code = new GoodBarcode();
                    code.setBarcode(barcode);
                    code.setPriceId(goodPrice.getId());
                    if (sysUser != null) {
                        code.setUserId(sysUser.getId());
                    }
                    dataAccessManager.addGoodBarcode(code);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        goodPrice.setSort(sort);
        goodPrice.setIcon(icon);
        goodPrice.setUnitId(unitId);
        goodPrice.setTop(top);
        goodPrice.setStatus(status);
        dataAccessManager.addGoodPrice(goodPrice);
        return success();
    }

    /**
     * 价格标签添加2
     * @author oy
     * @date 2019/5/16
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    @Transactional
    public JSONObject addGoodPrice2() {
        GoodPrice goodPrice = new GoodPrice();
        //标签名称
        String title = getUTF("title");
        String goodId = getUTF("goodId");////商品ID
        //条码
        String barcodes = getUTF("barcodes");//多个条码

        //单位名称
        String unitId = getUTF("unitId");
        //成本价
        Long costPrice = getLongParameter("costPrice", 0);
        //交易价
        Long sellPrice = getLongParameter("sellPrice", 0);
        //建议售价
        Long salePrice = getLongParameter("salePrice", 0);
        //同行价
        Long colleaguePrice = getLongParameter("colleaguePrice", 0);
        //会员价
        Long memberPrice = getLongParameter("memberPrice", 0);
        //市场价
        Long marketPrice = getLongParameter("marketPrice", 0);
        //促销价
        Long discountPrice = getLongParameter("discountPrice", 0);
        //备注
        String remark = getUTF("remark", null);
        //数量
        Integer num = getIntParameter("num", 0);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //小图标
        String icon = getUTF("icon", null);
        //默认选中项
        Integer top = getIntParameter("top", 0);
        //0.可用1.禁用
        Integer status = getIntParameter("status", 0);

        //验证当前的价格标签是否存在
        GoodPrice goodPriceisRepetition = dataAccessManager.getGoodPriceisRepetition(goodId, title);
        if(goodPriceisRepetition != null){
            return fail("标签不能重复!");
        }
        //重复的条码名字
        String barcodeName = null;
        //条码id
        List<String> barcode = null;

        if(barcodes != null && !barcodes.equals("")){
            barcode = JSONObject.parseArray(barcodes, String.class);
            if(barcode != null && barcode.size() > 0){
                List<GoodBarcode> goodBarcodeByBarcodes = dataAccessManager.getGoodBarcodeByBarcodes(barcode);
                if(goodBarcodeByBarcodes != null && goodBarcodeByBarcodes.size() > 0){//说明中间有重复的条码了
                    barcodeName = goodBarcodeByBarcodes.stream().map(GoodBarcode::getBarcode).collect(Collectors.joining(","));
                }
            }
        }
        if(barcodeName != null){
            return fail("重复的条码有："+barcodeName);
        }
        goodPrice.setTitle(title);
        goodPrice.setCostPrice(costPrice);
        goodPrice.setSalePrice(salePrice);
        goodPrice.setSellPrice(sellPrice);
        goodPrice.setColleaguePrice(colleaguePrice);
        goodPrice.setMarketPrice(marketPrice);
        goodPrice.setMemberPrice(memberPrice);
        goodPrice.setDiscountPrice(discountPrice);
        goodPrice.setRemark(remark);
        goodPrice.setGoodId(goodId);
        goodPrice.setNum(num);
        goodPrice.setSort(sort);
        goodPrice.setIcon(icon);
        goodPrice.setUnitId(unitId);
        goodPrice.setTop(top);
        goodPrice.setStatus(status);

        //添加条码
        List<GoodBarcode> goodBarcodeList = new ArrayList<>();
        if(barcode != null && barcode.size() > 0){
            barcode.forEach(x->{
                GoodBarcode goodBarcode = new GoodBarcode();
                goodBarcode.setBarcode(x);
                goodBarcode.setUserId(getUser().getId());
                goodBarcode.setPriceId(goodPrice.getId());
                goodBarcodeList.add(goodBarcode);
            });
        }
        if(goodBarcodeList != null && goodBarcodeList.size() > 0){
            dataAccessManager.batchAddGoodBarcode(goodBarcodeList);
        }
        dataAccessManager.addGoodPrice(goodPrice);
        return success("添加成功！");
    }

    /**
     * 修改对应的价格标签
     * @author oy
     * @date 2019/5/16
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    @Transactional
    public JSONObject editGoodPrice2() {
        String id = getId();
        GoodPrice goodPrice = dataAccessManager.getGoodPrice(id);
        //标签名称
        String title = getUTF("title");
        String goodId = getUTF("goodId");////商品ID
        //单位名称
        String unitId = getUTF("unitId");
        //条码
        String barcodes = getUTF("barcodes");//多个条码
        //成本价
        Long costPrice = getLongParameter("costPrice", 0);
        //交易价
        Long sellPrice = getLongParameter("sellPrice", 0);
        //建议售价
        Long salePrice = getLongParameter("salePrice", 0);
        //同行价
        Long colleaguePrice = getLongParameter("colleaguePrice", 0);
        //会员价
        Long memberPrice = getLongParameter("memberPrice", 0);
        //促销价
        Long discountPrice = getLongParameter("discountPrice", 0);
        //市场价
        Long marketPrice = getLongParameter("marketPrice", 0);
        //备注
        String remark = getUTF("remark", null);
        //数量
        Integer num = getIntParameter("num", 0);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //小图标
        String icon = getUTF("icon", null);
        //默认选中项
        Integer top = getIntParameter("top", 0);
        //0.可用1.禁用
        Integer status = getIntParameter("status", 0);

        //验证当前的价格标签是否存在
        GoodPrice goodPriceisRepetition = dataAccessManager.getGoodPriceisRepetition(goodId, title);
        if(goodPriceisRepetition != null){
            //判断是否是当前这个
            String id1 = goodPriceisRepetition.getId();
            if(!id.equals(id1)){
                return fail("标签不能重复!");
            }
        }
        //重复的条码名字
        String barcodeName = null;
        //条码id
        List<String> barcode = null;

        if(barcodes != null && !barcodes.equals("")){
            barcode = JSONObject.parseArray(barcodes, String.class);
            if(barcode != null && barcode.size() > 0){
                List<GoodBarcode> goodBarcodeByBarcodes = dataAccessManager.getGoodBarcodeByBarcodes(barcode);
                if(goodBarcodeByBarcodes != null && goodBarcodeByBarcodes.size() > 0){//说明中间有重复的条码了
                    barcodeName = goodBarcodeByBarcodes.stream().map(item -> {
                        if (!item.getPriceId().equals(id)) {
                            return item.getBarcode();
                        }
                        return null;
                    }).filter(x->x != null).collect(Collectors.joining(","));
                }
            }
        }
        if(barcodeName != null && !barcodeName.equals("")){
            return fail("重复的条码有："+barcodeName);
        }

        goodPrice.setTitle(title);
        goodPrice.setCostPrice(costPrice);
        goodPrice.setSalePrice(salePrice);
        goodPrice.setSellPrice(sellPrice);
        goodPrice.setColleaguePrice(colleaguePrice);
        goodPrice.setMarketPrice(marketPrice);
        goodPrice.setMemberPrice(memberPrice);
        goodPrice.setDiscountPrice(discountPrice);
        goodPrice.setRemark(remark);
        goodPrice.setGoodId(goodId);
        goodPrice.setNum(num);
        goodPrice.setSort(sort);
        goodPrice.setIcon(icon);
        goodPrice.setUnitId(unitId);
        goodPrice.setTop(top);
        goodPrice.setStatus(status);

        //添加条码
        List<GoodBarcode> goodBarcodeList = new ArrayList<>();
        if(barcode != null && barcode.size() > 0){
            barcode.forEach(x->{
                GoodBarcode goodBarcode = new GoodBarcode();
                goodBarcode.setBarcode(x);
                goodBarcode.setUserId(getUser().getId());
                goodBarcode.setPriceId(goodPrice.getId());
                goodBarcodeList.add(goodBarcode);
            });
        }

        if(goodBarcodeList != null && goodBarcodeList.size() > 0){
            dataAccessManager.batchAddGoodBarcode(goodBarcodeList);//添加条码信息(筛选已有的)
        }
        dataAccessManager.changeGoodPrice(goodPrice);
        return success("修改成功！");
    }

    /**
     * 价格标签页面修改部分的参数
     * @author oy
     * @date 2019/5/17
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    @Transactional
    public JSONObject updateSomeParamForGoodPriceList() {
        String imageUrl = getUTF("imageUrl",null);
        String brandLogo = getUTF("brandLogo",null);
        String goodBanner = getUTF("goodBanner",null);
        String priceIcon = getUTF("priceIcon",null);

        String goodId = getUTF("goodId",null);
        String priceId = getUTF("priceId",null);
        String brandId = getUTF("brandId",null);


        if(goodId != null && !goodId.equals("")){
            Map<String,Object> map = new HashMap<>();
            map.put("id",goodId);
            if(imageUrl != null && !imageUrl.equals("")){
                map.put("imageUrl",imageUrl);
            }
            if(goodBanner != null && !goodBanner.equals("")){
                map.put("goodBanner",goodBanner);
            }
            if(goodBanner != null || imageUrl != null){
                dataAccessManager.updateGoodTemplateByParam(map);
            }
        }

        if(priceId != null && !priceId.equals("")){
            Map<String,Object> map = new HashMap<>();
            map.put("id",priceId);
            if(priceIcon != null && !priceIcon.equals("")){
                map.put("priceIcon",priceIcon);
                dataAccessManager.updateGoodPriceByParam(map);
            }
        }

        if(brandId != null && !brandId.equals("")){
            Map<String,Object> map = new HashMap<>();
            map.put("id",brandId);
            if(brandLogo != null && !brandLogo.equals("")){
                map.put("brandLogo",brandLogo);
                dataAccessManager.updateGoodBrandByParam(map);
            }
        }
        return success("修改成功！");
    }

    @Override
    public JSONObject getDateByPriceId() {
        GoodPriceCombinationVO vo = new GoodPriceCombinationVO();
        String priceId = getUTF("priceId");
        if(priceId != null && !priceId.equals("")){
            GoodPrice goodPrice = dataAccessManager.getGoodPrice(priceId);
            if(goodPrice != null){
                //价格标签
                vo.setPriceId(priceId);
                //条码信息
                String id = goodPrice.getId();
                List<GoodBarcode> goodBarcodes = dataAccessManager.getGoodBarcodesByPriceId(id);//多个条码对应的一个价格标签的
                if(goodBarcodes != null && goodBarcodes.size() > 0){
                    String barcodes = goodBarcodes.stream().map(GoodBarcode::getBarcode).collect(Collectors.joining(","));
                    vo.setBarcode(barcodes);
                }

                //分类信息
                String unitId = goodPrice.getUnitId();
                if(unitId != null && !unitId.equals("")){
                    GoodUnit goodUnit = dataAccessManager.getGoodUnit(unitId);
                    if(goodUnit != null){
                        vo.setUnitTitleId(goodUnit.getId());//单位id
                        vo.setUnitTitle(goodUnit.getTitle());//单位名称
                    }
                }

                //分类信息
                String goodId = goodPrice.getGoodId();
                //根据商品类型 获取对应的分类
                GoodTemplate goodTemplate = dataAccessManager.getGoodTemplate(goodId);

                if(goodTemplate != null){
                    //templateId
                    vo.setGoodTemplateId(goodTemplate.getId());//模板id
                    vo.setGoodName(goodTemplate.getName());//模板名字

                    String typeId = goodTemplate.getTypeId();//分类id
                    //根据分类id

                    //二级分类
                    String parentId = null;
                    if(typeId != null && !typeId.equals("")){
                        GoodType goodType = dataAccessManager.getGoodType(typeId);
                        if(goodType != null){
                            vo.setTypeNameTwoId(goodType.getId());
                            vo.setTypeNameTwoName(goodType.getTitle());
                            parentId = goodType.getParentId();
                        }
                    }
                    //一级分类
                    if(parentId != null && !parentId.equals("")){
                        System.out.println("-------------父类id："+parentId);
                        GoodType goodType1 = dataAccessManager.getGoodType(parentId);
                        if(goodType1 != null){
                            vo.setTypeNameOneId(goodType1.getId());
                            vo.setTypeNameOneName(goodType1.getTitle());
                        }
                    }
                }

            }
        }
        return success(vo);
    }

    /**
     * 修改价格标签中参数
     * @author oy
     * @date 2019/5/25
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject updateByPriceByParam() {
        String priceId = getUTF("priceId");//价格id
        String goodTemplateId = getUTF("goodTemplateId");//模板id

        String priceTitle = getUTF("priceTitle",null);//价格标签
        Long priceNum = getLongParameter("priceNum");
        Long priceSort = getLongParameter("priceSort");
        String priceRemark = getUTF("priceRemark",null);
        Long sellPrice = getLongParameter("sellPrice");
        Long marketPrice = getLongParameter("marketPrice");
        Long colleaguePrice = getLongParameter("colleaguePrice");
        Long costPrice = getLongParameter("costPrice");
        Long salePrice = getLongParameter("salePrice");
        Long discountPrice = getLongParameter("discountPrice");
        Long memberPrice = getLongParameter("memberPrice");
        Integer status = getIntParameter("priceStatus",0);

        Map<String,Object> map = new HashMap<>();//参数map
        map.put("id",priceId);
        //判断是否修改价格标签
        if(priceTitle != null && !priceTitle.equals("")){
            //根据 价格标签 和 商品模板标签 去查找当前是否有存在的
            GoodPrice goodPriceisRepetition = dataAccessManager.getGoodPriceisRepetition(goodTemplateId, priceTitle);
            if(goodPriceisRepetition != null){
                String id = goodPriceisRepetition.getId();
                if(!priceId.equals(id)){
                    return fail("该标签名已存在！");
                }
            }
            map.put("title",priceTitle);
        }
        if(priceRemark != null){
            map.put("remark",priceRemark);
        }
        map.put("num",priceNum);
        map.put("status",status);
        map.put("sort",priceSort);
        map.put("sellPrice",sellPrice);
        map.put("marketPrice",marketPrice);
        map.put("colleaguePrice",colleaguePrice);
        map.put("costPrice",costPrice);
        map.put("salePrice",salePrice);
        map.put("discountPrice",discountPrice);
        map.put("memberPrice",memberPrice);
        dataAccessManager.updateGoodPriceByParam(map);
        return success("修改规格成功！");
    }

    /**
     * 自动化导入商品标签
     * @author oy
     * @date 2019/5/27
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject importGood() {
        List<Map<String, String>> maps = ExcelUtils.reaExcel();
        //遍历读取出来的map  需要添加的表 good_template good_price good_barcode
        int a = 0;
        List<GoodPrice> goodPriceList = new ArrayList<>();
        List<GoodBarcode> goodBarcodeList = new ArrayList<>();
        List<GoodTemplate> goodTemplateList = new ArrayList<>();
        List<GoodBrand> goodBrandListd = new ArrayList<>();
        List<GoodType> goodTypeList = new ArrayList<>();

        if(maps != null && maps.size() > 0){
            for (int j = 0; j < maps.size(); j++) {
                Map<String, String> x = maps.get(j);
                //获取详细信息   String columns[] = {"goodBrandName", "barcode", "goodName", "priceName", "unitName", "shelfLife","num", "salePrice", "costPrice","oneTypeName","twoTpeName"};
                String goodBrandName = x.get("goodBrandName");
                String barcode = x.get("barcode");


                String goodName = x.get("goodName");
                String priceName = x.get("priceName");
                String unitName = x.get("unitName");
                String shelfLife = x.get("shelfLife");
                String num = x.get("num");
                String salePrice = x.get("salePrice");
                String costPrice = x.get("costPrice");
                String twoTpeName = x.get("twoTypeName");
                String oneTypeName = x.get("oneTypeName");

                //价格标签
                GoodPrice goodPrice = new GoodPrice();
                //获取对应的条码
                GoodBarcode goodBarcode = new GoodBarcode();
                //商品模板
                GoodTemplate goodTemplate = new GoodTemplate();
                //品牌
                GoodBrand goodBrand = new GoodBrand();
                //分类
                GoodType goodType = new GoodType();

                //分类处理
                if(twoTpeName != null && !twoTpeName.equals("")){
                    if(oneTypeName != null && !oneTypeName.equals("")){
                        GoodType goodTypeByTitle = dataAccessManager.getGoodTypeByTitle(oneTypeName);
                        if(goodTypeByTitle != null){
                            String id = goodTypeByTitle.getId();
                            goodType.setParentId(id);
                        }
                    }

                    GoodType goodTypeByTitle = GoodTypeRemoteService.getGoodTypeByTitle(twoTpeName, getHttpServletRequest());
                    if(goodTypeByTitle != null){
                        String typeId = goodTypeByTitle.getId();
                        goodTemplate.setTypeId(typeId);
                    }else{
                        goodType.setTitle(twoTpeName);
                        goodTemplate.setTypeId(goodType.getId());
                        goodTypeList.add(goodType);
                    }
                }


                if(shelfLife != null && !shelfLife.equals("")){
                    double v = Double.parseDouble(shelfLife);
                    goodTemplate.setShelfLife((int)v);
                }

                if(goodName != null && !goodName.equals("")){
                    goodTemplate.setName(goodName);
                    String pingYin = ChinesePinyinUtil.getPingYin(goodName);
                    String pinYinHeadChar = ChinesePinyinUtil.getPinYinHeadChar(goodName);
                    goodTemplate.setFirstLetter(pinYinHeadChar);
                    goodTemplate.setAllLetter(pingYin);
                    goodPrice.setTemplateAllLetter(pingYin);
                    goodPrice.setTemplateFirstLetter(pinYinHeadChar);
                }

                //品牌处理
                if(goodBrandName != null && !goodBrandName.equals("")){
                    goodBrand.setName(goodBrandName);
                    goodTemplate.setBrandId(goodBrand.getId());
                }

                //条码处理
                if(barcode != null && !barcode.trim().equals("")){
                    try {
                        BigDecimal bd = new BigDecimal(barcode);
                        goodBarcode.setBarcode(bd.toPlainString());
                        goodBarcode.setPriceId(goodPrice.getId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    goodBarcodeList.add(goodBarcode);
                }

                //单位处理
                if(unitName != null && !unitName.trim().equals("")){
                    try {
                        GoodUnit goodUnitByTitle = GoodUnitRemoteService.getGoodUnitByTitle(unitName, getHttpServletRequest());
                        if(goodUnitByTitle != null){
                            String unitId = goodUnitByTitle.getId();
                            goodPrice.setUnitId(unitId);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                //数字
                if(num != null & !num.trim().equals("")){
                    int v = 0;
                    try {
                        v = (int)Double.parseDouble(num);
                    } catch (NumberFormatException e) {
                        goodPrice.setNum(0);
                    }
                    goodPrice.setNum(v);
                }

                if(costPrice != null & !costPrice.equals("")){
                    long v = (long) Double.parseDouble(costPrice) * 100;
                    goodPrice.setCostPrice(v);
                }

                if(salePrice != null & !salePrice.equals("")){//销售价
                    long v = (long) Double.parseDouble(salePrice) * 100;
                    goodPrice.setSellPrice(v);
                    goodPrice.setSalePrice(v);
                    goodPrice.setMarketPrice(v);
                    goodPrice.setMemberPrice(v);
                    goodPrice.setColleaguePrice(v);
                    goodPrice.setDiscountPrice(v);
                }

                if(priceName != null && !priceName.trim().equals("")){
                    String pingYin = ChinesePinyinUtil.getPingYin(priceName);
                    String pinYinHeadChar = ChinesePinyinUtil.getPinYinHeadChar(priceName);
                    goodPrice.setAllLetter(pingYin);
                    goodPrice.setFirstLetter(pinYinHeadChar);
                    goodPrice.setGoodId(goodTemplate.getId());
                    goodPrice.setTitle(priceName);
                    if(goodPrice.getCostPrice() == null){
                        goodPrice.setCostPrice(0l);
                    }
                    if(goodPrice.getSellPrice() == null){
                        goodPrice.setSellPrice(0l);
                    }
                    goodPrice.setMarketPrice(goodPrice.getSalePrice());
                    goodPrice.setMemberPrice(goodPrice.getSalePrice());
                    goodPrice.setColleaguePrice(goodPrice.getSalePrice());
                    goodPrice.setDiscountPrice(goodPrice.getSalePrice());
                    goodPriceList.add(goodPrice);
                }

                goodTemplateList.add(goodTemplate);
                goodBrandListd.add(goodBrand);

                if(a == 100 || j == maps.size() -1){
                    dataAccessManager.batchAddGoodBarcode(goodBarcodeList);
                    dataAccessManager.batchAddGoodPrice(goodPriceList);
                    dataAccessManager.batchAddGoodBrand(goodBrandListd);
                    dataAccessManager.batchAddGoodTemplate(goodTemplateList);
                    dataAccessManager.batchAddGoodType(goodTypeList);
                    goodBarcodeList.clear();
                    goodPriceList.clear();
                    goodBrandListd.clear();
                    goodTemplateList.clear();
                    goodTypeList.clear();
                    a = 0;
                }
                a++;
            }
        }


        return null;
    }

    /**
     * 验证价格标签是否能添加
     * @author oy
     * @date 2019/5/16
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject validateGoodPrice() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        //标签名称
        String title = getUTF("title");
        String goodId = getUTF("goodId");////商品ID
        String priceId = getUTF("priceId",null);
        //条码
        String barcode = getUTF("barcode");

        String barcodeName = null;

        if(barcode != null && !barcode.equals("")){
            List<String> list = JSONObject.parseArray(barcode, String.class);
            if(barcode != null && list.size() > 0){
                List<GoodBarcode> goodBarcodeByBarcodes = dataAccessManager.getGoodBarcodeByBarcodes(list);
                if(goodBarcodeByBarcodes != null && goodBarcodeByBarcodes.size() > 0){//说明中间有重复的条码了
                    barcodeName = goodBarcodeByBarcodes.stream().map(item -> {
                        if (!item.getPriceId().equals(priceId)) {
                            return item.getBarcode();
                        }
                        return null;
                    }).filter(x->x != null).collect(Collectors.joining(","));
                }
            }
        }
        if(barcodeName != null && !barcodeName.equals("")){
            jsonObject.put("key",1);
            jsonObject.put("msg","条码:barcodeName"+"重复");
        }

        //验证当前的价格标签是否存在
        GoodPrice goodPriceisRepetition = dataAccessManager.getGoodPriceisRepetition(goodId, title);
        if(goodPriceisRepetition != null){
            jsonObject.put("key",2);
            jsonObject.put("msg","价格标签不能重复！");
        }

        if(barcodeName == null && goodPriceisRepetition == null){
            jsonObject.put("key",0);
            jsonObject.put("msg","可以添加！");
        }
        return jsonObject;
    }

    @Override
    public JSONObject getGoodPrice() {
        GoodPrice goodPrice = dataAccessManager.getGoodPrice(getId());
        return success(goodPrice);
    }

    @Override
    public JSONObject getGoodPrices() {
        String goodId = getUTF("goodId");
        String title = getUTF("title", null);
        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        List<GoodPrice> goodPrices = dataAccessManager.getGoodPrices(status, getPageStartIndex(getPageSize()), getPageSize(), title, goodId);
        int size = goodPrices.size();
        for (int i = 0; i < size; i++) {
            try {
                SysUser sysUser = SysUserRmoteService.getSysUser(goodPrices.get(i).getUserId(), getHttpServletRequest());
                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        goodPrices.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        goodPrices.get(i).setUserName(sysUser.getShowName());
                    } else if (sysUser.getDefaultName() != null) {
                        goodPrices.get(i).setUserName(sysUser.getDefaultName());
                    } else {
                        goodPrices.get(i).setUserName(sysUser.getPhoneNumber());
                    }
                }
                GoodUnit goodUnit = dataAccessManager.getGoodUnit(goodPrices.get(i).getUnitId());
                if (goodUnit != null) {
                    goodPrices.get(i).setUnitName(goodUnit.getTitle());
                }
                GoodTemplate goodTemplate = dataAccessManager.getGoodTemplate(goodPrices.get(i).getGoodId());
                if (goodTemplate != null) {
                    goodPrices.get(i).setGoodName(goodTemplate.getName());
                }
                GoodBarcode goodBarcode = dataAccessManager.getGoodBarcodeByPriceId(goodPrices.get(i).getId());
                if (goodBarcode != null) {
                    String barcode = goodBarcode.getBarcode();
                    goodPrices.get(i).setBarcode(barcode);
                    //添加一个箱码 boxBarcode 2019-06-05
                    String barcodeParentId = goodBarcode.getBarcodeParentId();
                    if(barcodeParentId != null && !barcodeParentId.equals("")){
                        GoodBarcode goodBarcode1 = dataAccessManager.getGoodBarcode(barcodeParentId);
                        goodPrices.get(i).setBoxBarcode(goodBarcode1.getBarcode());
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int count = dataAccessManager.getGoodPriceCount(status, title, goodId);
        return success(goodPrices, count);
    }

    @Override
    public JSONObject getGoodPriceAll() {
        List<GoodPrice> goodPrices = dataAccessManager.getGoodPriceAll();
        return success(goodPrices);
    }

    @Override
    public JSONObject getGoodPriceCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        return success();
    }

    @Override
    public JSONObject changeGoodPrice() {
        GoodPrice goodPrice = dataAccessManager.getGoodPrice(getId());
        //价格ID
        String id = getUTF("id", null);
        //标签名称
        String title = getUTF("title", null);
        //成本价
        Long costPrice = getLongParameter("costPrice", 0);
        //建议售价
        Long salePrice = getLongParameter("salePrice", 0);
        //交易价
        Long sellPrice = getLongParameter("sellPrice", 0);
        //同行价
        Long colleaguePrice = getLongParameter("colleaguePrice", 0);
        //市场价
        Long marketPrice = getLongParameter("marketPrice", 0);
        //会员价
        Long memberPrice = getLongParameter("memberPrice", 0);
        //促销价
        Long discountPrice = getLongParameter("discountPrice", 0);
        //备注
        String remark = getUTF("remark", null);
        //商品ID
        String goodId = getUTF("goodId", null);
        //数量
        Integer num = getIntParameter("num", 0);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //小图标
        String icon = getUTF("icon", null);
        String barcode = getUTF("barcode", null);
        //单位名称
        String unitId = getUTF("unitId", null);
        //默认选中项
        Integer top = getIntParameter("top", 0);
        //0.可用1.禁用
        Integer status = getIntParameter("status", 0);
        goodPrice.setId(id);
        goodPrice.setTitle(title);
        goodPrice.setCostPrice(costPrice);
        goodPrice.setSalePrice(salePrice);
        goodPrice.setSellPrice(sellPrice);
        goodPrice.setColleaguePrice(colleaguePrice);
        goodPrice.setMarketPrice(marketPrice);
        goodPrice.setMemberPrice(memberPrice);
        goodPrice.setDiscountPrice(discountPrice);
        goodPrice.setRemark(remark);
        goodPrice.setGoodId(goodId);
        goodPrice.setNum(num);
        goodPrice.setSort(sort);
        goodPrice.setIcon(icon);
        goodPrice.setUnitId(unitId);
        goodPrice.setTop(top);
        goodPrice.setStatus(status);

        //更新对应的价格标签状态
        Map<String,Object> map = new HashMap<>();
        map.put("status",status);
        map.put("priceId",id);

        //异步更新状态
        updateSupplychainGoodStatusForAsync(map);

        dataAccessManager.changeGoodPrice(goodPrice);
        return success();
    }

    /**
     * 异步更新商店的商品信息
     * @author oy
     * @date 2019/5/27
     * @param map
     * @return void
     */
    @Async
    public void updateSupplychainGoodStatusForAsync(Map<String,Object> map){
        SupplyChainGoodRemoteService.updateSupplychainGoodStatus(map,getHttpServletRequest());
    }

    @Override
    public JSONObject getGoodPriceByGoodIdAll() {
        String goodId = getUTF("goodId");
        List<GoodPrice> goodPrices = dataAccessManager.getGoodPriceByGoodIdAll(goodId);
        return success(goodPrices);
    }

    @Override
    public JSONObject updateGoodPrice() {
        String title = getUTF("title");
        String id = getUTF("priceId1");
        String unitId = getUTF("unitId", null);
        String goodId = getUTF("templateId", null);
        dataAccessManager.updateGoodPrice(id, title, unitId, goodId);
        return success();
    }

    @Override
    public JSONObject batchDeleteGoodPrice() {
        String ids = getUTF("ids");
        if (ids != null || !ids.equals("")) {
            List<String> strings = JSONObject.parseArray(ids, String.class);
            dataAccessManager.batchDeleteGoodPrice(strings);
            return success("删除成功！");
        }
        return fail("参数错误！");
    }

    /**
     * 查询商品标签名是否存在
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/3
     */
    @Override
    public JSONObject getGoodPriceisRepetition() {
        String title = getUTF("title");
        String goodId = getUTF("goodId");
        GoodPrice goodPriceisRepetition = dataAccessManager.getGoodPriceisRepetition(goodId, title);
        if (goodPriceisRepetition != null) {

            return fail("该商品标签已存在！！！");
        } else {

            return success();
        }
    }

    /**
     * 展示所有的价格标签
     * @author oy
     * @date 2019/5/16
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject showAllGoodPriceis() {
        //条件查询 标签名称 标签状态 标签条形码 模板名称 模板状态
        String priceTitle = getUTF("priceTitle",null);
        Integer priceStatus = getIntParameter("priceStatus",0);//0.可用1.禁用
        String barcode = getUTF("barcode",null);
        String goodName = getUTF("goodName",null);
        Integer goodStatus = getIntParameter("goodStatus",0);// 0-可用 1-提交审核 2-审核不通过 3-失效

        Map<String,Object> map = new HashMap<>();
        map.put("priceTitle",priceTitle);
        map.put("barcode",barcode);
        map.put("goodName",goodName);

        if(priceStatus == -1){
            map.put("priceStatus",null);
        }else{
            map.put("priceStatus",priceStatus);
        }

        if(goodStatus == -1){
            map.put("goodStatus",null);
        }else{
            map.put("goodStatus",goodStatus);
        }

        List<GoodPriceCombinationVO> list = dataAccessManager.showAllGoodPriceis(map,getPageStartIndex(getPageSize()),getPageSize());
        int size = 0;
        if(list != null && list.size() > 0){
            list.forEach(x->{
                String createUser = x.getCreateUser();
                if(createUser != null && !createUser.equals("")){
                    try {
                        SysUser sysUser = SysUserRemoteService.getSysUser(createUser, getHttpServletRequest());
                        if(sysUser != null){
                            if (sysUser.getRealName() != null) {
                                x.setCreateUserName(sysUser.getRealName());
                            } else if (sysUser.getDefaultName() != null) {
                                x.setCreateUserName(sysUser.getDefaultName());
                            } else {
                                x.setCreateUserName(sysUser.getPhoneNumber());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            size = dataAccessManager.showAllCountGoodPriceis(map);
        }
        return success(list,size);
    }

    /**
     * 查询所有的规格信息
     * @author oy
     * @date 2019/5/18
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject showAllGoodPriceis2() {
        //条件查询 标签名称 标签状态 标签条形码 模板名称 模板状态
        String priceTitle = getUTF("priceTitle",null);
        Integer priceStatus = getIntParameter("priceStatus",0);//0.可用1.禁用
        String barcode = getUTF("barcode",null);
        String goodName = getUTF("goodName",null);
        Integer goodStatus = getIntParameter("goodStatus",StatusEnum.ALL.getKey());// 0-可用 1-提交审核 2-审核不通过 3-失效

        //barcode
        List<String> barcodePriceId = new ArrayList<>();//条码价格id 供价格id 筛选
        //是否从条码里面拿商品价格id
        boolean isPriceId = false;
        Map<String,List<GoodBarcode>> goodBarcodeCache = new HashMap<>();//存放条码缓存
        List<GoodBarcode> goodBarcodeByBarcodeLike = null;
        if(barcode != null && !barcode.equals("")){
            goodBarcodeByBarcodeLike = dataAccessManager.getGoodBarcodeByBarcodeLike(barcode);
            isPriceId = true;
        }else{
            goodBarcodeByBarcodeLike = dataAccessManager.getGoodBarcodeAll();
        }

        if(goodBarcodeByBarcodeLike == null || goodBarcodeByBarcodeLike.size() == 0){
            return success(goodBarcodeByBarcodeLike,0);
        }else{
            //统计当前条码数量（因为一个价格标签有多个条码的存在）
            int count = (int)goodBarcodeByBarcodeLike.stream().map(GoodBarcode::getPriceId).distinct().count();
            for(int i=0;i<count;i++){
                //判断当前价格标签是否加入到条码中
                GoodBarcode goodBarcode = goodBarcodeByBarcodeLike.get(i);
                String priceId = goodBarcode.getPriceId();
                if(!barcodePriceId.contains(priceId)){
                    barcodePriceId.add(priceId);
                    List<GoodBarcode> list = new ArrayList<>();
                    list.add(goodBarcode);
                    goodBarcodeCache.put(priceId,list);
                }else{
                    List<GoodBarcode> barcodes = goodBarcodeCache.get(priceId);
                    barcodes.add(goodBarcode);
                    goodBarcodeCache.put(priceId,barcodes);
                }
            }
        }
        //goodTemplate
        List<String> goodTemplates = new ArrayList<>(); //商品模板id
        Map<String,GoodTemplate> goodTemplateeCache = new HashMap<>(); //商品缓存
        List<GoodTemplate> gts = null;
        if((goodName!=null && !goodName.equals("")) || (goodStatus!= null && goodStatus.equals(""))){
            gts = dataAccessManager.getGoodTemplatesByGoodNameLikeAndGoodStatus(goodName,goodStatus);
            if(gts==null){
                return success();
            }else{
                int count = gts.size();
                for(int i=0;i<count;i++){
                    goodTemplates.add(gts.get(i).getId());
                    goodTemplateeCache.put(gts.get(i).getId(),gts.get(i));
                }
            }
        }

        //价格标签 priceTitle priceStatus
        int count = 0;
        List<GoodPrice> goodPriceList = null;
        if(isPriceId){
            goodPriceList =dataAccessManager.getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(priceTitle,priceStatus,goodTemplates,barcodePriceId,getPageStartIndex(getPageSize()),getPageSize());
        }else{
            goodPriceList =dataAccessManager.getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(priceTitle,priceStatus,goodTemplates,null,getPageStartIndex(getPageSize()),getPageSize());
        }

        if(goodPriceList != null && goodPriceList.size() > 0){
           //查询总条数
            if(isPriceId){
                count = dataAccessManager.getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(priceTitle,priceStatus,goodTemplates,barcodePriceId);
            }else{
                count = dataAccessManager.getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(priceTitle,priceStatus,goodTemplates,null);
            }
           List<GoodPriceCombinationVO> gpcv = new ArrayList<>();
           goodPriceList.forEach(x->{
               GoodPriceCombinationVO vo = new GoodPriceCombinationVO();
               vo.setPriceTitle(x.getTitle());
               vo.setPriceId(x.getId());
               vo.setPriceNum(x.getNum());
               vo.setPriceIcon(x.getIcon());
               vo.setPriceSort(x.getSort());
               vo.setPriceStatus(x.getStatus());
               vo.setPriceTitle(x.getTitle());
               vo.setPriceRemark(x.getRemark());
               vo.setCostPrice(x.getCostPrice());
               vo.setSalePrice(x.getSalePrice());
               vo.setSellPrice(x.getSellPrice());
               vo.setColleaguePrice(x.getColleaguePrice());
               vo.setDiscountPrice(x.getDiscountPrice());
               vo.setMarketPrice(x.getMarketPrice());
               vo.setMemberPrice(x.getMemberPrice());

               //用户
               String userId = x.getUserId();
               if(userId != null){
                   SysUser sysUser = SysUserRmoteService.getSysUser(userId, getHttpServletRequest());
                   if (sysUser != null) {
                       if (sysUser.getRealName() != null) {
                           vo.setCreateUserName(sysUser.getRealName());
                       } else if (sysUser.getShowName() != null) {
                           vo.setCreateUserName(sysUser.getShowName());
                       } else {
                           vo.setCreateUserName(sysUser.getDefaultName());
                       }
                   }
               }
               //商品模板
               GoodTemplate good = null;
               if(goodTemplateeCache != null && goodTemplateeCache.entrySet().size() != 0){
                   good = goodTemplateeCache.get(x.getGoodId());
               }else{
                   good = dataAccessManager.getGoodTemplate(x.getGoodId());
               }
               String typeId = null;
               if(good != null){
                   vo.setGoodTemplateId(good.getId());
                   vo.setGoodName(good.getName());
                   vo.setShelfLife(good.getShelfLife());
                   vo.setImageUrl(good.getImageUrl());
                   vo.setGoodStatus(good.getStatus());
                   vo.setGoodBanner(good.getBannerUrl());
                   //品牌
                   String brandId = good.getBrandId();
                   if(brandId != null && !brandId.equals("")){
                       GoodBrand goodBrand = dataAccessManager.getGoodBrand(brandId);
                       if(goodBrand != null){
                           vo.setBrandId(goodBrand.getId());
                           vo.setBrandName(goodBrand.getName());
                           vo.setBrandLogo(goodBrand.getLogo());
                       }
                   }
                   typeId = good.getTypeId();
               }

               List<GoodBarcode> goodBarcode = null;
               if(goodBarcodeCache != null && goodBarcodeCache.entrySet().size() != 0){
                   goodBarcode = goodBarcodeCache.get(x.getId());
               }else{
                   goodBarcode = dataAccessManager.getBarcodesByPriceId(x.getId());
               }

               if(goodBarcode != null && goodBarcode.size() > 0){
                   String barcodeId = goodBarcode.stream().map(GoodBarcode::getId).distinct().collect(Collectors.joining(","));
                   String barcodeName = goodBarcode.stream().map(GoodBarcode::getBarcode).distinct().collect(Collectors.joining(","));
                   vo.setBarcodeId(barcodeId);//条码id
                   vo.setBarcode(barcodeName);//条码
               }

               //类别
               GoodType type2 = null;
               if(typeId != null && !typeId.equals("")){
                   type2 = dataAccessManager.getGoodType(typeId);//二级
                   if(type2 != null){
                       vo.setTypeNameTwoName(type2.getTitle());
                       vo.setTypeNameTwoId(type2.getId());
                       vo.setTypeIconTwo(type2.getIcon());
                       vo.setTypeImageTwo(type2.getImage());
                       String parentId = type2.getParentId();
                       GoodType type1 = null;
                       if(parentId != null && !parentId.equals("")){
                           type1 = dataAccessManager.getGoodType(parentId);//一级
                       }
                       if(type1 != null){
                           vo.setTypeNameOneName(type1.getTitle());
                           vo.setTypeNameOneId(type1.getId());
                           vo.setTypeIconOne(type1.getIcon());
                           vo.setTypeImageOne(type1.getImage());
                       }
                   }
               }

               //单位
               String unitId = x.getUnitId();
               if(unitId != null && !unitId.equals("")){
                   vo.setUnitTitleId(unitId);
                   GoodUnit goodUnit = dataAccessManager.getGoodUnit(unitId);
                   if(goodUnit != null){
                       vo.setUnitTitle(goodUnit.getTitle());
                   }
               }
               gpcv.add(vo);
           });
           return success(gpcv,count);
       }
       return  success(goodPriceList,count);
    }

    @Override
    public JSONObject changeIconByBarcode() {
        String icon=getUTF("icon");
        dataAccessManager.changeIconByBarcode(icon,getId());
        return success();
    }

    @Override
    public JSONObject importIcon() {
        try {
            String filepath = "/vue/7777";

            File file = new File(filepath);
            if (!file.isDirectory()) {
                System.out.println("文件");
                System.out.println("path=" + file.getPath());
                System.out.println("absolutepath=" + file.getAbsolutePath());
                System.out.println("name=" + file.getName());

            } else if (file.isDirectory()) {
                System.out.println("文件夹");
                String[] filelist = file.list();
                for (int i = 0; i < filelist.length; i++) {
                    File readfile = new File(filepath + "/" + filelist[i]);
                    if (!readfile.isDirectory()) {
                        try {
                           // FileInputStream inputStream = new FileInputStream( readfile.getPath());
                          // AliyunOSSUtils.uploadImageFiles("goods/"+readfile.getName(), "ostrichapps",inputStream);
                            String barcode = readfile.getName().substring(0, readfile.getName().indexOf("."));
                            GoodBarcode goodBarcode=dataAccessManager.getGoodBarcodeByBarcode(barcode);
                            System.out.println("===================="+barcode);
                            if(goodBarcode!=null&&goodBarcode.getPriceId()!=null) {
                                dataAccessManager.changeIconByBarcode("https://ostrichapps.oss-cn-shenzhen.aliyuncs.com/goods/" + readfile.getName(), goodBarcode.getPriceId());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }

            }

        } catch (Exception e) {
            //   System.out.println("readfile()   Exception:" + e.getMessage());
        }
        return success();
    }
}
