package com.tuoniaostore.good.service;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.datamodel.good.GoodShopCart;

/**
 * 商品购物车
 * @author sqd
 * @date 2019/4/5
 */
public interface GoodShopCartService {
     JSONObject addGoodShopCart();
     JSONObject getGoodShopCartByUserId();
     JSONObject changeGoodShopCart();
     JSONObject selectGoodInventory();
     JSONObject getGoodShopCartByUserIdAndGoodId();
     JSONObject changeGoodShopCartStauts();
     JSONObject deleteGoodShopCartIds();
     JSONObject getGoodShopCartMessage();
     JSONObject addGoodShopCartgood();

    JSONObject getGoodShopCartListByUserId();
}
