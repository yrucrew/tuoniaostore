package com.tuoniaostore.good.service;
import com.alibaba.fastjson.JSONObject;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodBarcodeService {

    JSONObject addGoodBarcode();

    JSONObject getGoodBarcode();

    JSONObject getGoodBarcodes();

    JSONObject getGoodBarcodeCount();
    JSONObject getGoodBarcodeAll();
    JSONObject getGoodBarcodeByPriceId();
    JSONObject changeGoodBarcode();
    /**
     * 商家端：商品 添加商品 填写条码 返回对应的默认价格标签和单位
     * @author oy
     * @date 2019/4/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getGoodMsgByGoodBarcode();

    /**
     * 商家端：商品 添加商品 填写条码 返回对应GoodBarcode
     * @author oy
     * @date 2019/4/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getGoodMsgByGoodBarcode2();


    JSONObject getBarcodesByPriceId();

    /**
     * 根据模糊查询 返回条码集合
     * @author oy
     * @date 2019/4/24
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getGoodBarcodeByBarcodesLike();

    JSONObject getGoodBarcodeByPriceIds();

    JSONObject changeGoodBarcodeById();

    JSONObject getGoodBarcodeByGoodTemplateId();
}
