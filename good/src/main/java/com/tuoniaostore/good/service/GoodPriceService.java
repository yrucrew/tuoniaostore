package com.tuoniaostore.good.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodPriceService {

    JSONObject addGoodPrice();

    JSONObject getGoodPrice();

    JSONObject getGoodPrices();

    JSONObject getGoodPriceCount();
    JSONObject getGoodPriceAll();

    JSONObject changeGoodPrice();
    JSONObject getGoodPriceByGoodIdAll();

    JSONObject updateGoodPrice();

    JSONObject batchDeleteGoodPrice();
    JSONObject getGoodPriceisRepetition();

    JSONObject showAllGoodPriceis();

    JSONObject showAllGoodPriceis2();

    JSONObject addGoodPrice2();

    JSONObject validateGoodPrice();

    JSONObject editGoodPrice2();

    JSONObject updateSomeParamForGoodPriceList();

    JSONObject getDateByPriceId();

    JSONObject updateByPriceByParam();

    JSONObject importGood();
    JSONObject changeIconByBarcode();
    JSONObject importIcon();
}
