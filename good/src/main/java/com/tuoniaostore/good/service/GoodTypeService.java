package com.tuoniaostore.good.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodTypeService {

    JSONObject addGoodType();

    JSONObject getGoodType();

    JSONObject getGoodTypes();

    JSONObject getGoodTypeCount();
    JSONObject getGoodTypeAll();
    JSONObject getGoodTypeTreeAll();
    JSONObject changeGoodType();
    JSONObject getGoodTypeParent();
    JSONObject getGoodTypeTree();
    JSONObject getGoodTypeParentTree();

    /**
     * 根据list<IDS>查找对应的商品类别
     * @return
     */
    JSONObject getGoodTypesById();

    JSONObject getGoodTypesById2();

    JSONObject getGoodTypeByTitle();

    /**
     * 商家端：修改分类名称
     * @author oy
     * @date 2019/4/11
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject changeGoodTypeById();

    /**
     * 商品类别一二级菜单
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getAllGoodTypeOneTwo();

    JSONObject getGoodTypesAndNameLike();

    JSONObject getGoodTypesOneNameLike();

    JSONObject getGoodTypesNameLike();

    JSONObject getTypeByHomePageGood();
    /**
     * 查询类别是否存在
     * @author sqd
     * @date 2019/5/3
     * @param
     * @return
     */
    JSONObject getGoodTypeisRepetition();

    JSONObject deleteGoodType();

    JSONObject getGoodTypesAndSecondTypeByTypeId();

    JSONObject getGoodSecondTypeByTypeId();

    JSONObject getGoodTypesByPage();

    JSONObject getGoodTypeOne();

    JSONObject getGoodTypeOneTwoLeave();

    JSONObject getGoodTypeByid();

    JSONObject getGoodTypeOneByTwoTypeId();
}
