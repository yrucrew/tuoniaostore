package com.tuoniaostore.good.service;
import com.alibaba.fastjson.JSONObject;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public interface GoodUnitService {

    JSONObject addGoodUnit();

    JSONObject getGoodUnit();

    JSONObject getGoodUnits();

    JSONObject getGoodUnitCount();
    JSONObject getGoodUnitAll();
    JSONObject loaderGoodsUnits();

    JSONObject changeGoodUnit();
    JSONObject getGoodUnitTree();

    JSONObject getGoodUnitByTitle();
    /**
     * 查询单位是否存在
     * @author sqd
     * @date 2019/5/3
     * @param
     * @return
     */
    JSONObject getGoodUnitisRepetition();

    JSONObject deleteGoodsUnits();
}
