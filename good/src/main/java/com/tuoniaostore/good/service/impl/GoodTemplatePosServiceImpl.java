package com.tuoniaostore.good.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.good.GoodTemplate;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.good.cache.GoodDataAccessManager;
import com.tuoniaostore.good.service.GoodTemplatePosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("goodTemplatePosService")
public class GoodTemplatePosServiceImpl extends BasicWebService implements GoodTemplatePosService {
    @Autowired
    private GoodDataAccessManager dataAccessManager;

    @Override
    public JSONObject addGoodTemplatePos() {
        GoodTemplate goodTemplate = new GoodTemplate();
        //商品名字
        String idInit = goodTemplate.getId();
        String id = getUTF("id", null);
        String name = getUTF("name", null);
        String bannerUrl = getUTF("bannerUrl", null);
        //品牌ID
        String brandId = getUTF("brandId", null);
        //归属类型
        String typeId = getUTF("typeId", null);
        //0.普通1.价格
        Integer priceType = getIntParameter("priceType", 0);
        //状态 0-可用 1-提交审核 2-审核不通过 3-失效
        Integer status = getIntParameter("status", 0);
        //
        String imageUrl = getUTF("imageUrl", null);
        //商品描述
        String description = getUTF("description", null);
        //保质期(天)
        Integer shelfLife = getIntParameter("shelfLife", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        String remark = getUTF("remark", null);
        goodTemplate.setName(name);
        goodTemplate.setBannerUrl(bannerUrl);
        goodTemplate.setBrandId(brandId);
        goodTemplate.setTypeId(typeId);
        goodTemplate.setPriceType(priceType);
        goodTemplate.setStatus(status);
        goodTemplate.setImageUrl(imageUrl);
        goodTemplate.setDescription(description);
        goodTemplate.setShelfLife(shelfLife);
        goodTemplate.setSort(sort);
        if(id == null){
            goodTemplate.setId(idInit);
        }else{
            goodTemplate.setId(id);
        }

        try {
            SysUser sysUser = getUser();
            if (sysUser != null) {
                goodTemplate.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        goodTemplate.setRemark(remark);
        dataAccessManager.addGoodTemplatePos(goodTemplate);
        return success();
    }

    @Override
    public JSONObject getGoodTemplatePos() {
        GoodTemplate goodTemplate = dataAccessManager.getGoodTemplatePos(getId());
        return success(goodTemplate);
    }

    @Override
    public JSONObject getGoodTemplatePoss() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        SysUser sysUser= null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<GoodTemplate> goodTemplates = dataAccessManager.getGoodTemplatePoss(status, getPageStartIndex(getPageSize()), getPageSize(), name,sysUser);

        int count = dataAccessManager.getGoodTemplatePosCount(status, name);
        return success(goodTemplates, count);
    }





    @Override
    public JSONObject getGoodTemplatePosAll() {
        SysUser sysUser= null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<GoodTemplate> goodTemplates = dataAccessManager.getGoodTemplatePosAll(sysUser);
        return success(goodTemplates);
    }

    @Override
    public JSONObject getGoodTemplatePosCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        int count = dataAccessManager.getGoodTemplatePosCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeGoodTemplatePos() {
        GoodTemplate goodTemplate = dataAccessManager.getGoodTemplatePos(getId());
        //
        String id = getUTF("id", null);
        //商品名字
        String name = getUTF("name", null);
        //
        String bannerUrl = getUTF("bannerUrl", null);
        //品牌ID
        String brandId = getUTF("brandId", null);
        //归属类型
        String typeId = getUTF("typeId", null);
        //0.普通1.价格
        Integer priceType = getIntParameter("priceType", 0);
        //状态 0-可用 1-提交审核 2-审核不通过 3-失效
        Integer status = getIntParameter("status", 0);
        //
        String imageUrl = getUTF("imageUrl", null);
        //商品描述
        String description = getUTF("description", null);
        //保质期(天)
        Integer shelfLife = getIntParameter("shelfLife", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        String remark = getUTF("remark", null);
        goodTemplate.setId(id);
        goodTemplate.setName(name);
        goodTemplate.setBannerUrl(bannerUrl);
        goodTemplate.setBrandId(brandId);
        goodTemplate.setTypeId(typeId);
        goodTemplate.setPriceType(priceType);
        goodTemplate.setStatus(status);
        goodTemplate.setImageUrl(imageUrl);
        goodTemplate.setDescription(description);
        goodTemplate.setShelfLife(shelfLife);
        goodTemplate.setSort(sort);
        goodTemplate.setRemark(remark);
        dataAccessManager.changeGoodTemplatePos(goodTemplate);
        return success();
    }

    @Override
    public JSONObject getGoodTemplatePosByName() {
        String name = getUTF("name");
        GoodTemplate goodTemplate = dataAccessManager.getGoodTemplatePosByName(name);
        return success(goodTemplate);
    }

}
