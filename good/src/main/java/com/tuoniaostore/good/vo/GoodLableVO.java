package com.tuoniaostore.good.vo;

import java.io.Serializable;

/**
 * @author oy
 * @description 添加商品 填写条形码 获取对应的信息
 * @date 2019/4/3
 */
public class GoodLableVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String priceId;//价格id
    private String priceTitle;//价格标签
    private Long costPrice;//商品进价
    private Long salePrice;//商品售价

    private String unitId;//单位id
    private String unit;//单位

    public GoodLableVO() {
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getPriceTitle() {
        return priceTitle;
    }

    public void setPriceTitle(String priceTitle) {
        this.priceTitle = priceTitle;
    }

    public Long getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Long costPrice) {
        this.costPrice = costPrice;
    }

    public Long getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Long salePrice) {
        this.salePrice = salePrice;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

}
