package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.good.GoodTemplate;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Mapper
public interface GoodTemplatePosMapper {
    String column = "`id`, " +//
            "`name`, " +//商品名字
            "`all_letter`, " +//全拼
            "`banner_url`, " +//
            "`banner_url`, " +//
            "`brand_id`, " +//品牌ID
            "`type_id`, " +//归属类型
            "`price_type`, " +//0.普通1.价格
            "`status`, " +//状态 0-可用 1-提交审核 2-审核不通过 3-失效
            "`image_url`, " +//
            "`description`, " +//商品描述
            "`create_time`, " +//上传时间
            "`shelf_life`, " +//保质期(天)
            "`sort`, " +//
            "`user_id`, " +//创建人
            "`remark`, " +//
            "`retrieve_status`"//
            ;

    @Insert("insert into good_template_pos (" +
            " `id`,  " +
            " `name`,  " +
            " `first_letter`,  " +
            " `all_letter`,  " +
            " `banner_url`,  " +
            " `brand_id`,  " +
            " `type_id`,  " +
            " `price_type`,  " +
            " `status`,  " +
            " `image_url`,  " +
            " `description`,  " +
            " `shelf_life`,  " +
            " `sort`,  " +
            " `user_id`,  " +
            " `remark`  " +
            ")values(" +
            "#{id},  " +
            "#{name},  " +
            "#{firstLetter},  " +
            "#{allLetter},  " +
            "#{bannerUrl},  " +
            "#{brandId},  " +
            "#{typeId},  " +
            "#{priceType},  " +
            "#{status},  " +
            "#{imageUrl},  " +
            "#{description},  " +
            "#{shelfLife},  " +
            "#{sort},  " +
            "#{userId},  " +
            "#{remark}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addGoodTemplatePos(GoodTemplate goodTemplate);

    @Results(id = "goodTemplate", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "name", column = "name"),
            @Result(property = "firstLetter", column = "first_letter"),
            @Result(property = "bannerUrl", column = "banner_url"),
            @Result(property = "brandId", column = "brand_id"),
            @Result(property = "typeId", column = "type_id"),
            @Result(property = "priceType", column = "price_type"),
            @Result(property = "status", column = "status"),
            @Result(property = "imageUrl", column = "image_url"),
            @Result(property = "description", column = "description"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "shelfLife", column = "shelf_life"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "remark", column = "remark"),
            @Result(property = "retrieveStatus", column = "retrieve_status")})
    @Select("select " + column + " from good_template where id = #{id}")
    GoodTemplate getGoodTemplatePos(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_template_pos   where   retrieve_status=#{retrieveStatus} " +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodTemplate")
    List<GoodTemplate> getGoodTemplatePoss(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from good_template_pos   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("goodTemplate")
    List<GoodTemplate> getGoodTemplatePosAll();

    @Select("<script>select count(1) from good_template_pos   where  retrieve_status=#{retrieveStatus} " +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getGoodTemplatePosCount(@Param("status") int status, @Param("name") String name,@Param("retrieveStatus")int retrieveStatus);

    @Update("update good_template_pos  " +
            "set " +
            "`name` = #{name}  , " +
            "`banner_url` = #{bannerUrl}  , " +
            "`brand_id` = #{brandId}  , " +
            "`type_id` = #{typeId}  , " +
            "`price_type` = #{priceType}  , " +
            "`status` = #{status}  , " +
            "`image_url` = #{imageUrl}  , " +
            "`description` = #{description}  , " +
            "`shelf_life` = #{shelfLife}  , " +
            "`sort` = #{sort}  , " +
            "`remark` = #{remark}    " +
            " where id = #{id}")
    void changeGoodTemplatePos(GoodTemplate goodTemplate);

    @Select("<script> select  " + column + "  from good_template_pos   where   retrieve_status=0  and status=0 and name = #{name} " +
            "union " +
            "select " + column + "  from good_template  where  retrieve_status=0  and status=0 and name = #{name} limit 1</script>")
    @ResultMap("goodTemplate")
    GoodTemplate getGoodTemplatePosByName(@Param("name") String name);
}
