package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.good.GoodShopCart;
import org.apache.ibatis.annotations.*;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商品购物车
 * @author sqd
 * @date 2019/4/5
 */
@Component
@Mapper
public interface GoodShopCartMapper {

    String column =  "`id`," +
            "`good_id`, " +
            "`user_id`, " +
            "`sequence_number`, " +
            "`good_number`, " +
            "`create_time`, " +
            "`status`";

   @Results(id = "goodShopCart", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "goodId", column = "good_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "sequenceNumber", column = "sequence_number"),
            @Result(property = "goodNumber", column = "good_number"),
            @Result(property = "createTime", column = "create_time"),
           @Result(property = "status", column = "status"),
  })
   @Select("select "+column+" from good_shop_cat where id = #{id}")
   GoodShopCart getGoodShopCartById(@Param("id") String id);


    @Delete("<script>delete from good_shop_cat where   user_id = #{userId} and status = 1 " +
            " and  good_id in <foreach collection=\"ids\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></script>")
    @CacheEvict(value = "goodShopCart", allEntries = true)
    void deleteGoodShopCart(@Param("ids") List<String> id,@Param("userId") String userId);

    @Update("update good_shop_cat set good_number = #{map.goodNumber} where good_id =#{map.id} and user_id = #{map.userId} and status = 1")
    @CacheEvict(value = "goodShopCart",allEntries = true)
    void  changeGoodShopCart(@Param("map") Map<String,Object> map);

    @Select("<script> select "+column+" from good_shop_cat where status = 1" +
             "<if test=\"map.userId != null and map.userId != ''\" >and  user_id = #{map.userId}</if>" +
             "<if test=\"map.goodsId != null and map.goodsId != ''\" >and good_id = #{map.goodsId}</if>" +
            " </script>")
    @ResultMap("goodShopCart")
    List<GoodShopCart> getGoodShopCartByMap(@Param("map") Map<String,Object> map);

    @Select("<script> select "+column+" from good_shop_cat where status = 1" +
            "<if test=\"map.userId != null and map.userId != ''\" >and  user_id = #{map.userId}</if>" +
            "<if test=\"map.goodsId != null and map.goodsId != ''\" >and good_id = #{map.goodsId}</if>" +
            " order by create_time desc <when test=\"pageStartIndex != null and pageSize != null\">" +
            "  limit #{pageStartIndex}, #{pageSize}" +
            "</when>" +
            " </script>")
    @ResultMap("goodShopCart")
    List<GoodShopCart> getGoodShopCartByMapByPage(@Param("map") Map<String, Object> map,@Param("pageStartIndex") Integer pageStartIndex,@Param("pageSize") Integer pageSize);

    @Insert("insert into good_shop_cat(`id`,`good_id`,`user_id`,`good_number`,`sequence_number`,`status`,`create_time`) values (#{id},#{goodId},#{userId},#{goodNumber},#{sequenceNumber},1,#{createTime})")
    void  addGoodShopCart(GoodShopCart goodShopCart);

    @Select("select "+column+" from good_shop_cat where good_id = #{goodId} and user_id = #{userId} and status = 1 ")
    @ResultMap("goodShopCart")
    GoodShopCart getGoodShopCartByUserIdAndGoodId(@Param("userId") String userId,@Param("goodId") String goodId);

    @Update("<script> update good_shop_cat set status = #{map.status} where 1 = 1 " +
            "<if test=\"map.id != null and map.id != ''\" >and  id =#{map.id}</if>" +
            "<if test=\"map.userId != null and map.userId != ''\" >and  user_id =#{map.userId}</if>" +
            "<if test=\"map.goodId != null and map.goodId != ''\" >and  good_id =#{map.goodId}</if>" +
            "</script>")
    @CacheEvict(value = "goodShopCart",allEntries = true)
    void  changeGoodShopCartStauts(@Param("map") Map<String,Object> map);

}
