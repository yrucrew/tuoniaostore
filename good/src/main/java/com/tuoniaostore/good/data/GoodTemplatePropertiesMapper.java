package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.good.GoodTemplateProperties;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@Mapper
public interface GoodTemplatePropertiesMapper {

    String column = "`id`, " +//
            "`show_name`, " +//自定义值
            "`status`, " +//
            "`sort`, " +//
            "`user_id`, " +//创建人
            "`remark`, " +//备注
            "`retrieve_status`, " +//
            "`create_time`"//
            ;

    @Insert("insert into good_template_properties (" +
            " `id`,  " +
            " `show_name`,  " +
            " `status`,  " +
            " `sort`,  " +
            " `user_id`,  " +
            " `remark`  " +
            ")values(" +
            "#{id},  " +
            "#{showName},  " +
            "#{status},  " +
            "#{sort},  " +
            "#{userId},  " +
            "#{remark}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addGoodTemplateProperties(GoodTemplateProperties goodTemplateProperties);

    @Results(id = "goodTemplateProperties", value = {
            @Result(property = "id", column = "id"), @Result(property = "showName", column = "show_name"), @Result(property = "status", column = "status"), @Result(property = "sort", column = "sort"),
            @Result(property = "userId", column = "user_id"), @Result(property = "remark", column = "remark"), @Result(property = "retrieveStatus", column = "retrieve_status"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from good_template_properties where id = #{id} and retrieve_status = 0 ")
    GoodTemplateProperties getGoodTemplateProperties(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_template_properties   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodTemplateProperties")
    List<GoodTemplateProperties> getGoodTemplatePropertiess(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from good_template_properties   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("goodTemplateProperties")
    List<GoodTemplateProperties> getGoodTemplatePropertiesAll();

    @Select("<script>select count(1) from good_template_properties   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getGoodTemplatePropertiesCount(@Param("status") int status, @Param("name") String name);

    @Update("update good_template_properties  " +
            "set " +
            "`show_name` = #{showName}  , " +
            "`status` = #{status}  , " +
            "`sort` = #{sort}  , " +
            "`remark` = #{remark}  , " +
            " where id = #{id}")
    void changeGoodTemplateProperties(GoodTemplateProperties goodTemplateProperties);

}
