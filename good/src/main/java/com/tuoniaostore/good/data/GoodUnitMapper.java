package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.good.GoodType;
import com.tuoniaostore.datamodel.good.GoodUnit;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@Mapper
public interface GoodUnitMapper {

    String column = "`id`, " +//
            "`title`, " +//单位标题
            "`status`, " +//状态 0-不启动 1启动
            "`create_time`, " +//
            "`sort`, " +//
            "`user_id`, " +//创建人
            "`remarks`, " +//
            "`retrieve_status`"//
            ;

    @Insert("insert into good_unit (" +
            " `id`,  " +
            " `title`,  " +
            " `status`,  " +
            " `sort`,  " +
            " `user_id`,  " +
            " `remarks`  " +
            ")values(" +
            "#{id},  " +
            "#{title},  " +
            "#{status},  " +
            "#{sort},  " +
            "#{userId},  " +
            "#{remarks}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addGoodUnit(GoodUnit goodUnit);

    @Results(id = "goodUnit", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "status", column = "status"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "remarks", column = "remarks"),
            @Result(property = "retrieveStatus", column = "retrieve_status")})
    @Select("select " + column + " from good_unit where id = #{id}")
    GoodUnit getGoodUnit(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_unit   where   retrieve_status = #{retrieveStatus} " +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodUnit")
    List<GoodUnit> getGoodUnits( @Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,@Param("title") String title
            ,@Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from good_unit   where   retrieve_status = 0  and status=0 </script>")
    @ResultMap("goodUnit")
    List<GoodUnit> getGoodUnitAll();


    @Select("<script>select  " + column + "  from good_unit   where   retrieve_status = 0</script>")
    @ResultMap("goodUnit")
    List<GoodUnit> loaderGoodsUnits();

    @Select("<script>select count(1) from good_unit   where   retrieve_status = #{retrieveStatus}" +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when>" +
            "</script>")
    int getGoodUnitCount(@Param("status") int status, @Param("title") String title ,@Param("retrieveStatus") int retrieveStatus);

    @Update("update good_unit  " +
            "set " +
            "`title` = #{title}  , " +
            "`status` = #{status}  , " +
            "`sort` = #{sort}  , " +
            "`remarks` = #{remarks}   " +
            " where id = #{id}")
    void changeGoodUnit(GoodUnit goodUnit);

    @Select("<script> select  " + column + "  from good_unit   where   retrieve_status=0  and status=0 and title = #{title} limit 1 </script>")
    @ResultMap("goodUnit")
    GoodUnit getGoodUnitByTitle(@Param("title") String title);

    @Select("<script>select  " + column + "  from good_unit   where   1 = 1 and retrieve_status = 0 " +
            " <when test=\"map.title != null and map.title.trim() != ''\">" +
            " and  title = #{map.title}" +
            "</when></script>")
    @ResultMap("goodUnit")
    List<GoodUnit> getGoodUnitisRepetition(@Param("map") Map<String, Object> map);

    @Update("<script> update good_unit set retrieve_status = 1 where id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    void deleteGoodUnit(@Param("lists") List<String> ids);
}
