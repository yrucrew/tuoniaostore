package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.good.GoodType;
import com.tuoniaostore.datamodel.vo.good.GoodTypeOne;
import com.tuoniaostore.datamodel.vo.good.GoodTypeTwo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@Mapper
public interface GoodTypeMapper{

    String column = "`id`, " +//
            "`title`, " +//商品类型标题
            "`parent_id`, " +//父级ID (归属上级)
            "`status`, " +//状态 0-可用 1-提交审核 2-审核不通过 3-失效
            "`sort`, " +//排序 值小的靠上
            "`top`, " +//是否置顶 0表示不置顶
            "`icon`, " +//icon图片链接
            "`image`, " +//展示的图片
            "`user_id`, " +//创建人
            "`remark`, " +//
            "`retrieve_status`, " +//
            "`create_time`"//
            ;

    @Insert("insert into good_type (" +
            " `id`,  " +
            " `title`,  " +
            " `parent_id`,  " +
            " `status`,  " +
            " `sort`,  " +
            " `top`,  " +
            " `icon`,  " +
            " `image`,  " +
            " `user_id`,  " +
            " `type`,   " +
            " `remark`   " +
            ")values(" +
            "#{id},  " +
            "#{title},  " +
            "#{parentId},  " +
            "#{status},  " +
            "#{sort},  " +
            "#{top},  " +
            "#{icon},  " +
            "#{image},  " +
            "#{userId},  " +
            "#{type},  " +
            "#{remark}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addGoodType(GoodType goodType);

    @Results(id = "goodType", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "parentId", column = "parent_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "top", column = "top"),
            @Result(property = "icon", column = "icon"),
            @Result(property = "image", column = "image"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "remark", column = "remark"),
            @Result(property = "retrieveStatus", column = "retrieve_status"),
            @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from good_type where id = #{id}")
    GoodType getGoodType(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_type   where   retrieve_status=#{retrieveStatus} " +
            " <when test=\"status != null and status  !=-1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodType")
    List<GoodType> getGoodTypes(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("title") String title, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from good_type   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("goodType")
    List<GoodType> getGoodTypeAll();

    @Select("<script>select  " + column + "  from good_type   where   retrieve_status=0   </script>")
    @ResultMap("goodType")
    List<GoodType> loaderGoodType();

    @Select("<script>select  " + column + "  from good_type   where  `parent_id` =#{parentId} and  retrieve_status=0  and status=0   order by sort desc  </script>")
    @ResultMap("goodType")
    List<GoodType> getGoodTypeParent(@Param("parentId") String parentId);

    @Select("<script>select count(1) from good_type   where   retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status  !=-1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when></script>")
    int getGoodTypeCount(@Param("status") int status, @Param("title") String title, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select count(1) from good_type   where   retrieve_status=#{retrieveStatus} and parent_id != 'afdd09ad5d664557b485f58f48572e32ZnfsvX' and id != 'afdd09ad5d664557b485f58f48572e32ZnfsvX'  " +
            " <when test=\"status != null and status  !=-1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when></script>")
    int getGoodTypeCountForSecondType(int status, String title, int retrieveStatus);

    @Update("<script>update good_type  " +
            "<set> " +
            "<if test=\"title != null and title != ''\">`title` = #{title}  , </if> " +
            "<if test=\"parentId != null and parentId != ''\">`parent_id` = #{parentId}  , </if> " +
            "<if test=\"status != null\">`status` = #{status}  ,  </if> " +
            "<if test=\"sort != null\">`sort` = #{sort}  ,  </if> " +
            "<if test=\"top != null\">`top` = #{top}  ,  </if> " +
            "<if test=\"icon != null  and title != ''\">`icon` = #{icon}  ,  </if> " +
            "<if test=\"image != null  and title != ''\">`image` = #{image}  ,  </if> " +
            "<if test=\"remark != null  and title != ''\">`remark` = #{remark}  ,  </if> " +
            "</set> where id = #{id}</script>")
    void changeGoodType(GoodType goodType);

    /**
     * 通过ids 查找所有的类别
     * @param strings
     */
    @Select("<script>select  " + column + "  from good_type   where   status = 0 and retrieve_status=0  " +
            "<if test=\"lists != null\">and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            " <when test=\"pageIndex != null and pageSize != null\"> " +
            "  limit #{pageIndex}, #{pageSize} " +
            "</when></script>")
    @ResultMap("goodType")
    List<GoodType> getGoodsTypesByUserId(@Param("lists") List<String> strings,@Param("pageIndex") Integer pageLimit,@Param("pageSize")Integer pageSize);

    @Select("<script>select  " + column + "  from good_type   where  retrieve_status=0  and status=0 and  title = #{title} order by create_time desc limit 1 </script>")
    @ResultMap("goodType")
    GoodType getGoodTypeByTitle(@Param("title") String title);

    @Update("update good_type  " +
            "set " +
            "`title` = #{title} " +
            " where id = #{id}" )
    void updateGoodTypeByIdAndType(@Param("id") String id, @Param("title") String typeName);

    @Results(id = "goodTypeLeave", value = {
            @Result(property = "oneLeaveId", column = "id"),
            @Result(property = "oneLeaveName", column = "title"),
            @Result(property = "parentId", column = "parentId"),
            @Result(property = "list", column = "id",
                    many = @Many(select = "com.tuoniaostore.good.data.GoodTypeMapper.getGoodTypeTwoLeave"))
    })
    @Select(" <script> select id,title,parent_id as parentId from good_type where parent_id = 'afdd09ad5d664557b485f58f48572e32ZnfsvX' and id != 'afdd09ad5d664557b485f58f48572e32ZnfsvX'" +
            "<if test=\"lists != null and lists.size > 0\">and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>" +
            "  and  retrieve_status = 0  and status = 0 order by sort desc </script> ")
    List<GoodTypeOne> getGoodTypeOneTwoLeave(@Param("lists") List<String> ids);


    @Results(id="goodTypeTwoLeave",value = {
            @Result(property = "twoLeaveId", column = "id"),
            @Result(property = "twoLeaveName", column = "title"),
            @Result(property = "icon", column = "icon")
    })
    @Select("select id,title,icon from good_type where parent_id = #{id} and status = 0 and retrieve_status=0 ")
    List<GoodTypeTwo> getGoodTypeTwoLeave(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_type   where   retrieve_status=0 and status = 0 <if test=\"typeName != null and typeName !='' \">and title like concat('%',#{typeName},'%')</if>" +
            "<if test=\"lists != null\">and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if></script>")
    @ResultMap("goodType")
    List<GoodType> getGoodsTypesByUserIdAndNameLike(@Param("lists") List<String> idList, @Param("typeName") String typeName);

    @Select("<script>select  " + column + "  from good_type   where   retrieve_status=0 and parent_id = 'afdd09ad5d664557b485f58f48572e32ZnfsvX' <if test=\"typeName != null and typeName !='' \">and title like concat('%',#{typeName},'%')</if>" +
            "<if test=\"lists != null\">and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if></script>")
    @ResultMap("goodType")
    List<GoodType> getGoodsTypesOneByUserIdAndNameLike(List<String> idList, String typeName);

    @Select("<script>select  " + column + "  from good_type  where status = 0 and retrieve_status = 0  " +
            " <if test=\"title != null and title.trim() != ''\">and title like concat('%',#{title},'%')</if> " +
            " and parent_id != 'afdd09ad5d664557b485f58f48572e32ZnfsvX' and id != 'afdd09ad5d664557b485f58f48572e32ZnfsvX' " +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodType")
    List<GoodType> getGoodTypesByPage(@Param("title") String title,@Param("pageIndex") Integer pageStartIndex, @Param("pageSize") Integer pageSize);

    @Select("<script>select  " + column + "  from good_type   where    retrieve_status = 0 " +
            " <when test=\"map.title != null and map.title.trim() != ''\">" +
            " and  title = #{map.title}" +
            "</when></script>")
    @ResultMap("goodType")
    List<GoodType> getGoodTypeisRepetition(@Param("map") Map<String, Object> map);

    @Update("<script> update good_type set retrieve_status = 1 where id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> " +
            " and  id != 'afdd09ad5d664557b485f58f48572e32ZnfsvX'</script>")
    void deleteGoodType(@Param("lists") List<String> strings);

    @Select("<script>select  " + column + "  from good_type   where   retrieve_status=0 and status = 0 " +
            "and parent_id in <foreach collection=\"lists\" item=\"item2\" open=\"(\" close=\")\" separator=\",\">#{item2}</foreach> " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize} " +
            "</when>"+
            "</script>")
    @ResultMap("goodType")
    List<GoodType> getGoodTypesAndSecondTypeByTypeId(@Param("lists") String ids, @Param("pageIndex") Integer pageStartIndex,@Param("pageSize") Integer pageSize);

    @Select("<script>select  " + column + "  from good_type   where   retrieve_status=0 and status = 0 " +
            "and parent_id = 'afdd09ad5d664557b485f58f48572e32ZnfsvX' and id != 'afdd09ad5d664557b485f58f48572e32ZnfsvX' " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize} " +
            "</when>"+
            "</script>")
    @ResultMap("goodType")
    List<GoodType> getGoodTypeOne(@Param("pageIndex")Integer pageStartIndex,@Param("pageSize") Integer pageSize);


    @Select("<script>select  " + column + "  from good_type   where   retrieve_status=0 and status = 0 " +
            "and id  = (SELECT parent_id FROM `good_type` where id = #{typeTwoId}) " +
            "</script>")
    @ResultMap("goodType")
    GoodType getGoodTypeOneByTwoTypeId(@Param("typeTwoId") String typeTwoId);

    @Insert("<script> insert into good_type " +
            " (`id`,  " +
            " `title`,  " +
            " `parent_id`,  " +
            " `status`,  " +
            " `sort`,  " +
            " `top`,  " +
            " `icon`,  " +
            " `image`,  " +
            " `user_id`,  " +
            " `remark`,  " +
            " `type`  " +
            ") " +
            "<foreach collection=\"lists\" item=\"item\" separator=\"UNION ALL\"> " +
            " (SELECT #{item.id},#{item.title},#{item.parentId},#{item.status}, #{item.sort}, #{item.top}, #{item.icon}, #{item.image}, #{item.userId}, #{item.remark} , #{item.type}  " +
            "FROM DUAL WHERE (SELECT COUNT(*) FROM  good_type " +
            " WHERE title = #{item.title}) <![CDATA[<]]> 1 ) " +
            "</foreach> </script>")
    void batchAddGoodType(@Param("lists") List<GoodType> goodTypeList);

}
