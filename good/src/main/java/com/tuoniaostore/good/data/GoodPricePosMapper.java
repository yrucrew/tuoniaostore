package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.datamodel.good.GoodPrice;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Mapper
public interface GoodPricePosMapper {
    String column = "`id`, " +//价格ID
            "`title`, " +//标签名称
            "`first_letter`, " +//首字母
            "`all_letter`, " +//全拼
            "`template_first_letter`, " +//模板首字母
            "`template_all_letter`, " +//模板全拼
            "`cost_price`, " +//成本价
            "`sale_price`, " +//建议售价
            "`sell_price`, " +//交易价
            "`colleague_price`, " +//同行价
            "`market_price`, " +//市场价
            "`member_price`, " +//会员价
            "`discount_price`, " +//促销价
            "`remark`, " +//备注
            "`good_id`, " +//商品ID
            "`num`, " +//数量
            "`user_id`, " +//
            "`create_time`, " +//
            "`sort`, " +//排序
            "`icon`, " +//小图标
            "`unit_id`, " +//单位名称
            "`top`, " +//默认选中项
            "`status`"//0.可用1.禁用
            ;

    @Insert("insert into good_price_pos (" +
            " `id`,  " +
            " `title`,  " +
            "`t1.first_letter`, " +//首字母
            "`t1.all_letter`, " +//全拼
            "`t1.template_first_letter`, " +//模板首字母
            "`t1.template_first_letter`, " +//模板全拼
            " `cost_price`,  " +
            " `sale_price`,  " +
            " `sell_price`,  " +
            " `colleague_price`,  " +
            " `market_price`,  " +
            " `member_price`,  " +
            " `discount_price`,  " +
            " `remark`,  " +
            " `good_id`,  " +
            " `num`,  " +
            " `user_id`,  " +
            " `sort`,  " +
            " `icon`,  " +
            " `unit_id`,  " +
            " `top`,  " +
            " `status` " +
            ")values(" +
            "#{id},  " +
            "#{title},  " +
            " `first_letter`,  " +
            " `all_letter`,  " +
            " `template_first_letter`,  " +
            " `template_all_letter`,  " +
            "#{costPrice},  " +
            "#{salePrice},  " +
            "#{sellPrice},  " +
            "#{colleaguePrice},  " +
            "#{marketPrice},  " +
            "#{memberPrice},  " +
            "#{discountPrice},  " +
            "#{remark},  " +
            "#{goodId},  " +
            "#{num},  " +
            "#{userId},  " +
            "#{sort},  " +
            "#{icon},  " +
            "#{unitId},  " +
            "#{top},  " +
            "#{status} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addGoodPricePos(GoodPrice goodPrice);

    @Results(id = "goodPrice", value = {
            @Result(property = "id", column = "id"), @Result(property = "title", column = "title"),
            @Result(property = "firstLetter", column = "first_letter"),
            @Result(property = "allLetter", column = "all_letter"),
            @Result(property = "templateFirstLetter", column = "template_first_letter"),
            @Result(property = "templateAllLetter", column = "template_all_letter"),
            @Result(property = "costPrice", column = "cost_price"),
            @Result(property = "salePrice", column = "sale_price"),
            @Result(property = "sellPrice", column = "sell_price"),
            @Result(property = "colleaguePrice", column = "colleague_price"),
            @Result(property = "marketPrice", column = "market_price"),
            @Result(property = "memberPrice", column = "member_price"),
            @Result(property = "discountPrice", column = "discount_price"),
            @Result(property = "remark", column = "remark"),
            @Result(property = "goodId", column = "good_id"),
            @Result(property = "num", column = "num"), @Result(property = "userId", column = "user_id"), @Result(property = "createTime", column = "create_time"), @Result(property = "sort", column = "sort"), @Result(property = "icon", column = "icon"), @Result(property = "unitId", column = "unit_id"), @Result(property = "top", column = "top"), @Result(property = "status", column = "status")})
    @Select("select " + column + " from good_price_pos where id = #{id}")
    GoodPrice getGoodPricePos(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_price_pos   where     status=#{status} and good_id=#{goodId} " +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodPrice")
    List<GoodPrice> getGoodPricePoss(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("title") String title, @Param("goodId") String goodId);

    @Select("<script>select  " + column + "  from good_price_pos   where   retrieve_status = 0 and status=0 </script>")
    @ResultMap("goodPrice")
    List<GoodPrice> getGoodPricePosAll();
    @Select("<script>select  " + column + "  from good_price_pos   where good_id = #{goodId} and retrieve_status = 0 order by sort desc limit 15;</script>")
    @ResultMap("goodPrice")
    List<GoodPrice> getGoodPricePosTop15(@Param("goodId") String goodId);

    @Select("<script>select  " + column + "  from good_price_pos    where good_id = #{goodId} and retrieve_status = 0 order by sort desc </script>")
    @ResultMap("goodPrice")
    List<GoodPrice> getGoodPricePosByGoodIdAll(@Param("goodId") String goodId);
    @Select("<script>select count(1) from good_price   where  retrieve_status = 0 and status=#{status} and good_id=#{goodId} " +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when></script>")
    int getGoodPricePosCount(@Param("status") int status, @Param("title") String title, @Param("goodId") String goodId);

    @Select("SELECT count(1) FROM  good_price_pos where good_id = #{goodId} and retrieve_status = 0")
    int getGoodPricePosByGoodIdCount(@Param("goodId") String goodId);

    @Update("update good_price_pos  " +
            "set " +
            "`title` = #{title}  , " +
            "`cost_price` = #{costPrice}  , " +
            "`sale_price` = #{salePrice}  , " +
            "`sell_price` = #{sellPrice}  , " +
            "`colleague_price` = #{colleaguePrice}  , " +
            "`market_price` = #{marketPrice}  , " +
            "`member_price` = #{memberPrice}  , " +
            "`discount_price` = #{discountPrice}  , " +
            "`remark` = #{remark}  , " +
            "`good_id` = #{goodId}  , " +
            "`num` = #{num}  , " +
            "`sort` = #{sort}  , " +
            "`icon` = #{icon}  , " +
            "`unit_id` = #{unitId}  , " +
            "`top` = #{top}  , " +
            "`status` = #{status}  " +
            " where id = #{id}")
    void changeGoodPricePos(GoodPrice goodPrice);

    @Select("<script>select  " + column + "  from good_price_pos where title = #{title} and retrieve_status = 0 " +
            "union" +
            "select  " + column + "  from good_price where title = #{title} and retrieve_status = 0 " +
            "  order by create limit 1 </script>")
    @ResultMap("goodPrice")
    GoodPrice getGoodPricePosByTitle(@Param("title") String title);

    @Update("<script> update good_price_pos  " +
            "<trim prefix='set' suffixOverrides=','>" +
            "<if test=\"title!=null\">title = #{title},</if>" +
            "<if test=\"goodId!=null\">good_id = #{goodId},</if>" +
            "<if test=\"unitId!=null\">unit_id = #{unitId},</if>" +
            "</trim>" +
            " where id = #{id} </script>")
    void updateGoodPricePos(@Param("id") String id, @Param("title")String title, @Param("unitId")String unitId, @Param("goodId")String goodId);

    @Select("<script>select  " + column + "  from good_price_pos where title = #{title} and  good_id = #{goodId} and retrieve_status = 0 " +
            "union" +
            "select  " + column + "  from good_price where title = #{title} and  good_id = #{goodId} and retrieve_status = 0 " +
            "</script>")
    @ResultMap("goodPrice")
    GoodPrice getAllGoodPriceByTitleAndGoodId(@Param("title")String title, @Param("goodId")String goodId);


    @Select("<script> select t3.id AS priceId,t3.icon AS priceIcon,t3.title AS priceTitle,t4.title AS unitTitle,t5.barcode as barcode from  " +
            "( SELECT id,title,unit_id,icon FROM `good_price` t1 where 1 = 1 and retrieve_status = 0 " +
            "<if test=\"lists != null and lists.size() > 0 \"> and t1.id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> " +
            "UNION " +
            "SELECT id,title,unit_id,icon FROM `good_price_pos` t2 where 1 = 1 and retrieve_status = 0 " +
            "<if test=\"lists != null and lists.size() > 0 \"> and t2.id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> )t3 " +
            "left JOIN good_unit t4 ON t3.unit_id = t4.id  " +
            "left join good_barcode t5 on t3.id = t5.price_id </script>")
    @Results(id = "goodPriceCombinationVO", value = {
            @Result(property = "priceId", column = "priceId"),
            @Result(property = "priceTitle", column = "priceTitle"),
            @Result(property = "priceIcon", column = "priceIcon"),
            @Result(property = "barcode", column = "barcode"),
            @Result(property = "unitTitle", column = "unitTitle")
    })
    List<GoodPriceCombinationVO> getPriceTitleAndUnitTitleByPriceIds(@Param("lists") List<String> ids);

    @Select("<script>  select t1.id as priceId,t1.title as priceTitle,t1.sell_price as sellPrice,t1.icon as priceIcon,t1.sale_price as salePrice,t1.colleague_price as colleaguePrice,t1.member_price as memberPrice,t1.discount_price as discountPrice,t1.market_price as marketPrice,t2.name as goodName,t2.image_url as imageUrl,t3.id as unitTitleId, t3.title as unitTitle, " +
            "t2.id  as goodTemplateId,t7.id as typeNameTwoId,t7.title as typeNameTwoName,t7.parent_id as typeNameOneId,t9.title as typeNameOneName,t11.id as barcodeId,GROUP_CONCAT(t11.barcode) as barcode," +
            "t1.cost_price as costPrice,t13.name as brandName,t13.id as brandId ,t13.logo as brandLogo,t7.icon as typeIconTwo,t7.image as typeImageTwo,t9.icon as typeIconOne,t9.image as typeImageOne,t2.shelf_life as shelfLife from ( " +
            "SELECT id,title,sell_price,icon,sale_price,colleague_price,member_Price,discount_price,market_price,good_id,unit_id,cost_price FROM `good_price` where 1 = 1 and retrieve_status = 0 <if test=\"lists != null and lists.size > 0\">and id in  <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>  " +
            "UNION " +
            "SELECT id,title,sell_price,icon,sale_price,colleague_price,member_Price,discount_price ,market_price,good_id,unit_id,cost_price FROM `good_price_pos` where 1 = 1 and retrieve_status = 0 <if test=\"lists != null and lists.size > 0\">and id in  <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>  " +
            ")t1 " +
            "INNER JOIN good_template t2 ON t1.good_id = t2.id " +
            "LEFT JOIN good_unit t3 ON t1.unit_id = t3.id " +
            "LEFT JOIN good_type t7 ON t2.type_id = t7.id " +
            "LEFT JOIN good_type t9 ON t7.parent_id = t9.id " +
            "LEFT JOIN good_barcode t11 ON t1.id = t11.price_id " +
            "LEFT JOIN good_brand t13 ON t13.id = t2.brand_id " +
            "GROUP BY t1.id "+
            "<if test=\"nameOrBarcode != null and nameOrBarcode != ''\"> where t2.name like concat('%',#{nameOrBarcode},'%')  or t11.barcode like concat('%',#{nameOrBarcode},'%') </if> " +
            "UNION " +
            "select t4.id as priceId,t4.title as priceTitle,t4.sell_price as sellPrice,t4.icon as priceIcon,t4.sale_price as salePrice,t4.colleague_price as colleaguePrice,t4.member_price as memberPrice,t4.discount_price as discountPrice,t4.market_price as marketPrice,t5.name as goodName,t5.image_url as imageUrl,t6.id as unitTitleId, t6.title as unitTitle, " +
            "t5.id  as goodTemplateId,t8.id as typeNameTwoId,t8.title as typeNameTwoName,t8.parent_id as typeNameTwoId,t10.title as typeNameTwoName,t12.id as barcodeId,GROUP_CONCAT(t12.barcode) as barcode," +
            "t4.cost_price as costPrice,t14.name as brandName,t14.id as brandId,t14.Logo as brandLogo,t8.icon as typeIconTwo,t8.image as typeImageTwo,t10.icon as typeIconOne,t10.image as typeImageOne,t5.shelf_life as shelfLife from " +
            "(" +
            "SELECT id,title,sell_price,sale_price,icon,colleague_price,member_Price,discount_price,market_price,good_id,unit_id,cost_price FROM `good_price` where 1 = 1 and retrieve_status = 0 <if test=\"lists != null and lists.size > 0\">and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>  " +
            "UNION " +
            "SELECT id,title,sell_price,sale_price,icon,colleague_price,member_Price,discount_price,market_price,good_id,unit_id,cost_price FROM `good_price_pos` where 1 = 1 and retrieve_status = 0 <if test=\"lists != null and lists.size > 0\">and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>  " +
            ")t4 " +
            "INNER JOIN good_template_pos t5 ON t4.good_id = t5.id " +
            "LEFT JOIN good_unit t6 ON t4.unit_id = t6.id " +
            "LEFT JOIN good_type t8 ON t5.type_id = t8.id " +
            "LEFT JOIN good_type t10 ON t8.parent_id = t10.id " +
            "LEFT JOIN good_barcode t12 ON t4.id = t12.price_id " +
            "LEFT JOIN good_brand t14 ON t14.id = t5.brand_id " +
            "GROUP BY t4.id "+
            "<if test=\"nameOrBarcode != null and nameOrBarcode != ''\"> where t5.name like concat('%',#{nameOrBarcode},'%')  or t12.barcode like concat('%',#{nameOrBarcode},'%') </if> " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when> "+
            "</script>")
    @Results(id = "goodPriceCombinationVO2", value = {
            @Result(property = "priceId", column = "priceId"),
            @Result(property = "priceTitle", column = "priceTitle"),
            @Result(property = "priceIcon", column = "priceIcon"),
            @Result(property = "sellPrice", column = "sellPrice"),
            @Result(property = "costPrice", column = "costPrice"),
            @Result(property = "salePrice", column = "salePrice"),
            @Result(property = "colleaguePrice", column = "colleaguePrice"),
            @Result(property = "memberPrice", column = "memberPrice"),
            //@Result(property = "discountPrice,", column = "discountPrice"),
//            @Result(property = "marketPrice,", column = "marketPrice"),
            @Result(property = "goodName", column = "goodName"),
            @Result(property = "shelfLife", column = "shelfLife"),
            @Result(property = "imageUrl", column = "imageUrl"),
            @Result(property = "unitTitleId", column = "unitTitleId"),
            @Result(property = "unitTitle", column = "unitTitle"),
            @Result(property = "goodTemplateId", column = "goodTemplateId"),
            @Result(property = "typeNameOneId", column = "typeNameOneId"),
            @Result(property = "typeNameOneName", column = "typeNameOneName"),
            @Result(property = "typeNameTwoId", column = "typeNameTwoId"),
            @Result(property = "typeNameTwoName", column = "typeNameTwoName"),
            @Result(property = "barcodeId", column = "barcodeId"),
            @Result(property = "barcode", column = "barcode"),
            @Result(property = "brandId", column = "brandId"),
            @Result(property = "brandLogo", column = "brandLogo"),
            @Result(property = "brandName", column = "brandName"),
            @Result(property = "typeIconTwo", column = "typeIconTwo"),
            @Result(property = "typeImageTwo", column = "typeImageTwo"),
            @Result(property = "typeIconOne", column = "typeIconOne"),
            @Result(property = "typeImageOne", column = "typeImageOne")
    })
    List<GoodPriceCombinationVO> getAllGoodTemplateByPriceIds(@Param("lists") List<String> s,@Param("nameOrBarcode") String nameOrBarcode,@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    @Select("<script> select count(*) from ( select t1.id as priceId,t1.title as priceTitle,t1.sell_price as sellPrice,t1.sale_price as salePrice,t1.colleague_price as colleaguePrice,t1.member_price as memberPrice,t1.discount_price as discountPrice,t1.market_price as marketPrice,t2.name as goodName,t2.image_url as imageUrl,t3.id as unitTitleId, t3.title as unitTitle, " +
            "t2.id  as goodTemplateId,t7.id as typeNameTwoId,t7.title as typeNameTwoName,t7.parent_id as typeNameOneId,t9.title as typeNameOneName,t11.id as barcodeId,GROUP_CONCAT(t11.barcode) as barcode," +
            "t1.cost_price as costPrice,t13.name as brandName,t13.id as brandId ,t13.logo as brandLogo,t7.icon as typeIconTwo,t7.image as typeImageTwo,t9.icon as typeIconOne,t9.image as typeImageOne,t2.shelf_life as shelfLife from ( " +
            "SELECT id,title,sell_price,sale_price,colleague_price,member_Price,discount_price,market_price,good_id,unit_id,cost_price FROM `good_price` where 1 = 1 and retrieve_status = 0 <if test=\"lists != null and lists.size > 0\">and id in  <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>  " +
            "UNION " +
            "SELECT id,title,sell_price,sale_price,colleague_price,member_Price,discount_price ,market_price,good_id,unit_id,cost_price FROM `good_price_pos` where 1 = 1 and retrieve_status = 0 <if test=\"lists != null and lists.size > 0\">and id in  <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>  " +
            ")t1 " +
            "INNER JOIN good_template t2 ON t1.good_id = t2.id " +
            "LEFT JOIN good_unit t3 ON t1.unit_id = t3.id " +
            "LEFT JOIN good_type t7 ON t2.type_id = t7.id " +
            "LEFT JOIN good_type t9 ON t7.parent_id = t9.id " +
            "LEFT JOIN good_barcode t11 ON t1.id = t11.price_id " +
            "LEFT JOIN good_brand t13 ON t13.id = t2.brand_id " +
            "GROUP BY t1.id "+
            "<if test=\"nameOrBarcode != null and nameOrBarcode != ''\"> where t2.name like concat('%',#{nameOrBarcode},'%')  or t11.barcode like concat('%',#{nameOrBarcode},'%') </if> " +
            "UNION " +
            "select t4.id as priceId,t4.title as priceTitle,t4.sell_price as sellPrice,t4.sale_price as salePrice,t4.colleague_price as colleaguePrice,t4.member_price as memberPrice,t4.discount_price as discountPrice,t4.market_price as marketPrice,t5.name as goodName,t5.image_url as imageUrl,t6.id as unitTitleId, t6.title as unitTitle, " +
            "t5.id  as goodTemplateId,t8.id as typeNameTwoId,t8.title as typeNameTwoName,t8.parent_id as typeNameTwoId,t10.title as typeNameTwoName,t12.id as barcodeId,GROUP_CONCAT(t12.barcode) as barcode," +
            "t4.cost_price as costPrice,t14.name as brandName,t14.id as brandId,t14.Logo as brandLogo,t8.icon as typeIconTwo,t8.image as typeImageTwo,t10.icon as typeIconOne,t10.image as typeImageOne,t5.shelf_life as shelfLife from " +
            "(" +
            "SELECT id,title,sell_price,sale_price,colleague_price,member_Price,discount_price,market_price,good_id,unit_id,cost_price FROM `good_price` where 1 = 1 and retrieve_status = 0 <if test=\"lists != null and lists.size > 0\">and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>  " +
            "UNION " +
            "SELECT id,title,sell_price,sale_price,colleague_price,member_Price,discount_price,market_price,good_id,unit_id,cost_price FROM `good_price_pos` where 1 = 1 and retrieve_status = 0 <if test=\"lists != null and lists.size > 0\">and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>  " +
            ")t4 " +
            "INNER JOIN good_template_pos t5 ON t4.good_id = t5.id " +
            "LEFT JOIN good_unit t6 ON t4.unit_id = t6.id " +
            "LEFT JOIN good_type t8 ON t5.type_id = t8.id " +
            "LEFT JOIN good_type t10 ON t8.parent_id = t10.id " +
            "LEFT JOIN good_barcode t12 ON t4.id = t12.price_id " +
            "LEFT JOIN good_brand t14 ON t14.id = t5.brand_id " +
            "GROUP BY t4.id "+
            "<if test=\"nameOrBarcode != null and nameOrBarcode != ''\"> where t5.name like concat('%',#{nameOrBarcode},'%')  or t12.barcode like concat('%',#{nameOrBarcode},'%') </if> " +
            ")t100 </script>")
    int getAllGoodTemplateByPriceIdsCount(@Param("lists")List<String> ids, @Param("nameOrBarcode")String nameOrBarcode);
}
