package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.good.GoodTemplatePropertiesValue;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@Mapper
public interface GoodTemplatePropertiesValueMapper {

    String column = "`id`, " +//
            "`good_template_id`, " +//
            "`good_template_properties_id`, " +//
            "`content`, " +//
            "`sort`, " +//
            "`user_id`, " +//创建人
            "`status`, " +//
            "`remark`, " +//
            "`retrieve_status`, " +//
            "`create_time`"//
            ;

    @Insert("insert into good_template_properties_value (" +
            " `id`,  " +
            " `good_template_id`,  " +
            " `good_template_properties_id`,  " +
            " `content`,  " +
            " `sort`,  " +
            " `user_id`,  " +
            " `status`,  " +
            " `remark`  " +
            ")values(" +
            "#{id},  " +
            "#{goodTemplateId},  " +
            "#{goodTemplatePropertiesId},  " +
            "#{content},  " +
            "#{sort},  " +
            "#{userId},  " +
            "#{status},  " +
            "#{remark}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addGoodTemplatePropertiesValue(GoodTemplatePropertiesValue goodTemplatePropertiesValue);

    @Results(id = "goodTemplatePropertiesValue", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "goodTemplateId", column = "good_template_id"),
            @Result(property = "goodTemplatePropertiesId", column = "good_template_properties_id"),
            @Result(property = "content", column = "content"), @Result(property = "sort", column = "sort"),
            @Result(property = "userId", column = "user_id"), @Result(property = "status", column = "status"), @Result(property = "remark", column = "remark"), @Result(property = "retrieveStatus", column = "retrieve_status"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from good_template_properties_value where id = #{id}")
    GoodTemplatePropertiesValue getGoodTemplatePropertiesValue(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_template_properties_value   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodTemplatePropertiesValue")
    List<GoodTemplatePropertiesValue> getGoodTemplatePropertiesValues(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from good_template_properties_value   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("goodTemplatePropertiesValue")
    List<GoodTemplatePropertiesValue> getGoodTemplatePropertiesValueAll();

    @Select("<script>select count(1) from good_template_properties_value   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getGoodTemplatePropertiesValueCount(@Param("status") int status, @Param("name") String name);

    @Update("update good_template_properties_value  " +
            "set " +
            "`good_template_id` = #{goodTemplateId}  , " +
            "`good_template_properties_id` = #{goodTemplatePropertiesId}  , " +
            "`content` = #{content}  , " +
            "`sort` = #{sort}  , " +
            "`user_id` = #{userId}  , " +
            "`status` = #{status}  , " +
            "`remark` = #{remark}  , " +
            "`retrieve_status` = #{retrieveStatus}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeGoodTemplatePropertiesValue(GoodTemplatePropertiesValue goodTemplatePropertiesValue);

}
