package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.good.GoodPrice;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@Mapper
public interface GoodPriceMapper {

    String column = "`id`, " +//价格ID
            "`title`, " +//标签名称
            "`first_letter`, " +//首字母
            "`all_letter`, " +//全拼
            "`template_first_letter`, " +//模板首字母
            "`template_all_letter`, " +//模板全拼
            "`cost_price`, " +//成本价
            "`sale_price`, " +//建议售价
            "`sell_price`, " +//交易价
            "`colleague_price`, " +//同行价
            "`market_price`, " +//市场价
            "`member_price`, " +//会员价
            "`discount_price`, " +//促销价
            "`remark`, " +//备注
            "`good_id`, " +//商品ID
            "`num`, " +//数量
            "`user_id`, " +//
            "`create_time`, " +//
            "`sort`, " +//排序
            "`icon`, " +//小图标
            "`unit_id`, " +//单位名称
            "`top`, " +//默认选中项
            "`status`,"+//0.可用1.禁用
            "`retrieve_status`"//0.可用1.禁用
            ;

    String column2 = "t1.id, " +//价格ID
            "t1.title, " +//标签名称
            "t1.first_letter, " +//首字母
            "t1.all_letter, " +//全拼
            "t1.template_first_letter, " +//模板首字母
            "t1.template_first_letter, " +//模板全拼
            "t1.cost_price, " +//成本价
            "t1.sale_price, " +//建议售价
            "t1.sell_price, " +//交易价
            "t1.colleague_price, " +//同行价
            "t1.market_price, " +//市场价
            "t1.member_price, " +//会员价
            "t1.discount_price, " +//促销价
            "t1.remark, " +//备注
            "t1.good_id, " +//商品ID
            "t1.num, " +//数量
            "t1.user_id, " +//
            "t1.create_time, " +//
            "t1.sort, " +//排序
            "t1.icon, " +//小图标
            "t1.unit_id, " +//单位名称
            "t1.top, " +//默认选中项
            "t1.status,"+//0.可用1.禁用
            "t1.retrieve_status"//0.可用1.禁用
            ;

    @Insert("insert into good_price (" +
            " `id`,  " +
            " `title`,  " +
            " `first_letter`,  " +
            " `all_letter`,  " +
            " `template_first_letter`,  " +
            " `template_all_letter`,  " +
            " `cost_price`,  " +
            " `sale_price`,  " +
            " `sell_price`,  " +
            " `colleague_price`,  " +
            " `market_price`,  " +
            " `member_price`,  " +
            " `discount_price`,  " +
            " `remark`,  " +
            " `good_id`,  " +
            " `num`,  " +
            " `user_id`,  " +
            " `sort`,  " +
            " `icon`,  " +
            " `unit_id`,  " +
            " `top`,  " +
            " `status` " +
            ")values(" +
            "#{id},  " +
            "#{title},  " +
            "#{firstLetter},  " +
            "#{allLetter},  " +
            "#{templateFirstLetter},  " +
            "#{templateAllLetter},  " +
            "#{costPrice},  " +
            "#{salePrice},  " +
            "#{sellPrice},  " +
            "#{colleaguePrice},  " +
            "#{marketPrice},  " +
            "#{memberPrice},  " +
            "#{discountPrice},  " +
            "#{remark},  " +
            "#{goodId},  " +
            "#{num},  " +
            "#{userId},  " +
            "#{sort},  " +
            "#{icon},  " +
            "#{unitId},  " +
            "#{top},  " +
            "#{status} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addGoodPrice(GoodPrice goodPrice);

    @Insert("<script> insert into good_price (" +
            " `id`,  " +
            " `title`,  " +
            " `first_letter`,  " +
            " `all_letter`,  " +
            " `template_first_letter`,  " +
            " `template_all_letter`,  " +
            " `cost_price`,  " +
            " `sale_price`,  " +
            " `sell_price`,  " +
            " `colleague_price`,  " +
            " `market_price`,  " +
            " `member_price`,  " +
            " `discount_price`,  " +
            " `remark`,  " +
            " `good_id`,  " +
            " `num`,  " +
            " `user_id`,  " +
            " `sort`,  " +
            " `icon`,  " +
            " `unit_id`,  " +
            " `top`,  " +
            " `status` " +
            ")values" +
            "<foreach collection=\"lists\" item=\"item\" separator=\",\"> " +
            "(" +
            "#{item.id},  " +
            "#{item.title},  " +
            "#{item.firstLetter},  " +
            "#{item.allLetter},  " +
            "#{item.templateFirstLetter},  " +
            "#{item.templateAllLetter},  " +
            "#{item.costPrice},  " +
            "#{item.salePrice},  " +
            "#{item.sellPrice},  " +
            "#{item.colleaguePrice},  " +
            "#{item.marketPrice},  " +
            "#{item.memberPrice},  " +
            "#{item.discountPrice},  " +
            "#{item.remark},  " +
            "#{item.goodId},  " +
            "#{item.num},  " +
            "#{item.userId},  " +
            "#{item.sort},  " +
            "#{item.icon},  " +
            "#{item.unitId},  " +
            "#{item.top},  " +
            "#{item.status} " +
            ")</foreach></script>")
    void batchAddGoodPrice(@Param("lists") List<GoodPrice> list);

    @Results(id = "goodPrice", value = {
            @Result(property = "id", column = "id"), @Result(property = "title", column = "title"),
            @Result(property = "firstLetter", column = "first_letter"),
            @Result(property = "allLetter", column = "all_letter"),
            @Result(property = "templateFirstLetter", column = "template_first_letter"),
            @Result(property = "templateAllLetter", column = "template_all_letter"),
            @Result(property = "costPrice", column = "cost_price"),
            @Result(property = "salePrice", column = "sale_price"),
            @Result(property = "sellPrice", column = "sell_price"),
            @Result(property = "colleaguePrice", column = "colleague_price"),
            @Result(property = "marketPrice", column = "market_price"),
            @Result(property = "memberPrice", column = "member_price"),
            @Result(property = "discountPrice", column = "discount_price"),
            @Result(property = "remark", column = "remark"),
            @Result(property = "retrieveStatus", column = "retrieve_status"),
            @Result(property = "goodId", column = "good_id"),
            @Result(property = "num", column = "num"), @Result(property = "userId", column = "user_id"), @Result(property = "createTime", column = "create_time"), @Result(property = "sort", column = "sort"), @Result(property = "icon", column = "icon"), @Result(property = "unitId", column = "unit_id"), @Result(property = "top", column = "top"), @Result(property = "status", column = "status")})
    @Select("select " + column + " from good_price where id = #{id} and retrieve_status = 0 limit 1 ")
    GoodPrice getGoodPrice(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_price   where   good_id=#{goodId} and retrieve_status = 0 " +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when>" +
            " <when test=\"status != null and status != -1 \">" +
            "  and status=#{status}  " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodPrice")
    List<GoodPrice> getGoodPrices(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("title") String title, @Param("goodId") String goodId);

    @Select("<script>select  " + column + "  from good_price   where     status=0  and retrieve_status = 0 </script>")
    @ResultMap("goodPrice")
    List<GoodPrice> getGoodPriceAll();

    @Select("<script>select  " + column + "  from good_price   where    status=0  and retrieve_status = 0 " +
            "<if test=\"tempalteIds != null and tempalteIds.size() > 0 \"> and good_id in <foreach collection=\"tempalteIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>" +
            "<if test=\"inPriceIds != null and inPriceIds.size() > 0 \"> and id in <foreach collection=\"inPriceIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>" +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when> "+
            " </script>")
    @ResultMap("goodPrice")
    List<GoodPrice> getGoodPriceAllByPage(@Param("tempalteIds") List<String> templateIds,@Param("inPriceIds") List<String> inPriceIds,@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);

    @Select("<script>select  " + column + "  from good_price  where  retrieve_status = 0  </script>")
    @ResultMap("goodPrice")
    List<GoodPrice> loaderGoodPrice();
    @Select("<script>select  " + column + "  from good_price     where good_id = #{goodId} and retrieve_status = 0  order by sort desc limit 15 </script>")
    @ResultMap("goodPrice")
    List<GoodPrice> getGoodPriceTop15(@Param("goodId") String goodId);

    @Select("<script>select  " + column + "  from good_price       where good_id = #{goodId}  and retrieve_status = 0  order by sort desc </script>")
    @ResultMap("goodPrice")
    List<GoodPrice> getGoodPriceByGoodIdAll(@Param("goodId") String goodId);
    @Select("<script>select count(1) from good_price   where retrieve_status = 0 and  good_id=#{goodId} " +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when>" +
            " <when test=\"status != null and status != -1 \">" +
            "  and status=#{status}  " +
            " </when>" +
            "</script>")
    int getGoodPriceCount(@Param("status") int status, @Param("title") String title, @Param("goodId") String goodId);

    @Select("SELECT count(1) FROM  good_price where good_id = #{goodId} and retrieve_status = 0 ")
    int getGoodPriceByGoodIdCount(@Param("goodId") String goodId);

    @Select("<script>SELECT count(1) FROM  good_price where retrieve_status = 0 " +
            "<if test=\"tempalteIds != null and tempalteIds.size() > 0 \"> and good_id in <foreach collection=\"tempalteIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>" +
            "<if test=\"inPriceIds != null and inPriceIds.size() > 0 \"> and id in <foreach collection=\"inPriceIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>" +
            "</script>")
    int getGoodPriceCountAll(@Param("tempalteIds") List<String> templateIds,@Param("inPriceIds")List<String> inPriceIds);

    @Update("<script>update good_price  " +
            "<set> " +
            "<if test=\"title != null and title != ''\">title = #{title}  , </if> " +
            "<if test=\"costPrice != null \">cost_price = #{costPrice}  ,</if>" +
            "<if test=\"salePrice != null \">sale_price = #{salePrice}  , </if>" +
            "<if test=\"sellPrice != null \">sell_price = #{sellPrice}  ,  </if>" +
            "<if test=\"colleaguePrice != null \">colleague_price = #{colleaguePrice}  , </if>" +
            "<if test=\"marketPrice != null \">market_price = #{marketPrice}  ,  </if>" +
            "<if test=\"memberPrice != null \">member_price = #{memberPrice}  , </if>" +
            "<if test=\"discountPrice != null \">discount_price = #{discountPrice}  , </if>" +
            "<if test=\"remark != null and remark != ''\">remark = #{remark}  ,  </if> " +
            "<if test=\"goodId != null and goodId != ''\">good_id = #{goodId}  ,  </if> " +
            "<if test=\"num != null \">num = #{num}  ,  </if>" +
            "<if test=\"sort != null \">sort = #{sort}  ,   </if>" +
            "<if test=\"icon != null and icon != ''\"> icon = #{icon}  ,   </if> " +
            "<if test=\"unitId != null and unitId != ''\"> unit_id = #{unitId}  ,  </if> " +
            "<if test=\"top != null \"> top = #{top}  ,  </if>" +
            "<if test=\"status != null \">status = #{status} ,  </if>" +
            "</set> where id = #{id}</script>")
    void changeGoodPrice(GoodPrice goodPrice);

    @Update("<script> update good_price  " +
            "<trim prefix='set' suffixOverrides=','>" +
            "<if test=\"title!=null\">title = #{title},</if>" +
            "<if test=\"goodId!=null\">good_id = #{goodId},</if>" +
            "<if test=\"unitId!=null\">unit_id = #{unitId},</if>" +
            "</trim>" +
            " where id = #{id} </script>")
    void updateGoodPrice(String id, String title, String unitId, String goodId);


    //勿删！供goodTemplateMapper调用 多对多查询使用
    @Select("SELECT  "+ column +" FROM  good_price where good_id = #{goodId} and retrieve_status = 0 " +
            "UNION " +
            "SELECT  "+ column +" FROM  good_price_pos where good_id = #{goodId} and retrieve_status = 0 ")
    @ResultMap("goodPrice")
    List<GoodPrice> listGoodPrice(@Param("goodId") String goodId);

    @Update("<script> update good_price  " +
            "set retrieve_status = 1 "+
            " where id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    void batchDeleteGoodPrice(@Param("lists") List<String> ids);

    @Select("<script>" +
               " SELECT  "+ column +" FROM  good_price where good_id = #{goodId} and title = #{title} and retrieve_status = 0  limit 1" +
            "</script>")
    @ResultMap("goodPrice")
     GoodPrice getGoodPriceisRepetition(@Param("goodId") String  goodId,@Param("title") String  title);

    @Update("<script> update good_price " +
            "<set> " +
            "<when test=\"map.priceIcon != null and map.priceIcon != ''\"> icon = #{map.priceIcon}, </when> " +
            "<when test=\"map.title != null and map.title != ''\"> title = #{map.title}, </when> " +
            "<when test=\"map.remark != null and map.remark != ''\"> remark = #{map.remark}, </when> " +
            "<when test=\"map.num != null \"> num = #{map.num}, </when> " +
            "<when test=\"map.status != null \"> status = #{map.status}, </when> " +
            "<when test=\"map.sort != null \"> sort = #{map.sort}, </when> " +
            "<when test=\"map.sellPrice != null\"> sell_price = #{map.sellPrice}, </when> " +
            "<when test=\"map.marketPrice != null\"> market_price = #{map.marketPrice}, </when> " +
            "<when test=\"map.colleaguePrice != null\"> colleague_price = #{map.colleaguePrice}, </when> " +
            "<when test=\"map.costPrice != null\"> cost_price = #{map.costPrice}, </when> " +
            "<when test=\"map.salePrice != null \"> sale_price = #{map.salePrice}, </when> " +
            "<when test=\"map.discountPrice != null \"> discount_price = #{map.discountPrice}, </when> " +
            "<when test=\"map.memberPrice != null \"> member_price = #{map.memberPrice}, </when> " +
            "</set> where id = #{map.id}" +
            "</script>")
    void updateGoodPriceByParam(@Param("map") Map<String, Object> map);


    @Select("<script>  select t1.user_id as createUser,t1.icon as priceIcon,t1.num as priceNum,t1.sort as priceSort,t1.remark as priceRemark,t1.id as priceId,t1.title as priceTitle,t1.sell_price as sellPrice,t1.sale_price as salePrice,t1.colleague_price as colleaguePrice,t1.member_price as memberPrice,t1.discount_price as discountPrice,t1.market_price as marketPrice," +
            "t2.name as goodName,t2.image_url as imageUrl,t2.banner_url as goodBanner ,t3.id as unitTitleId, t3.title as unitTitle, " +
            "t2.id  as goodTemplateId,t7.id as typeNameTwoId,t7.title as typeNameTwoName,t7.parent_id as typeNameOneId,t9.title as typeNameOneName,t11.id as barcodeId,GROUP_CONCAT(t11.barcode) as barcode," +
            "t1.cost_price as costPrice,t13.name as brandName,t13.id as brandId ,t13.logo as brandLogo,t7.icon as typeIconTwo,t7.image as typeImageTwo,t9.icon as typeIconOne,t9.image as typeImageOne ,t1.status as priceStatus ,t2.status as goodStatus from ( " +
            "SELECT id,title,sell_price,sale_price,colleague_price,member_Price,discount_price,market_price,good_id,unit_id,cost_price,status,user_id,icon,num,sort,remark FROM `good_price` " +
            " where retrieve_status = 0  " +
            "<when test=\"map.priceStatus != null \"> and status = #{map.priceStatus} </when> " +
            ")t1 " +
            "INNER JOIN good_template t2 ON t1.good_id = t2.id " +
            "LEFT JOIN good_unit t3 ON t1.unit_id = t3.id " +
            "LEFT JOIN good_type t7 ON t2.type_id = t7.id " +
            "LEFT JOIN good_type t9 ON t7.parent_id = t9.id " +
            "LEFT JOIN good_barcode t11 ON t1.id = t11.price_id " +
            "LEFT JOIN good_brand t13 ON t13.id = t2.brand_id " +
            "where 1 = 1 " +
            "<when test=\"map.priceTitle != null and map.priceTitle != ''\">and t1.title like concat('%',#{map.priceTitle},'%')</when>" +
            "<when test=\"map.barcode != null and map.barcode != ''\">and t11.barcode like concat('%',#{map.barcode},'%')</when>" +
            "<when test=\"map.goodName != null and map.goodName != ''\">and t2.name like concat('%',#{map.goodName},'%')</when>" +
            "<when test=\"map.goodStatus != null \">and t2.status = #{map.goodStatus} </when>" +
            "GROUP BY t1.id "+
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when> "+
            "</script>")
    @Results(id = "goodPriceCombinationVO2", value = {
            @Result(property = "priceId", column = "priceId"),
            @Result(property = "priceTitle", column = "priceTitle"),
            @Result(property = "sellPrice", column = "sellPrice"),
            @Result(property = "costPrice", column = "costPrice"),
            @Result(property = "salePrice", column = "salePrice"),
            @Result(property = "colleaguePrice", column = "colleaguePrice"),
            @Result(property = "memberPrice", column = "memberPrice"),
            @Result(property = "goodName", column = "goodName"),
            @Result(property = "shelfLife", column = "shelfLife"),
            @Result(property = "imageUrl", column = "imageUrl"),
            @Result(property = "unitTitleId", column = "unitTitleId"),
            @Result(property = "unitTitle", column = "unitTitle"),
            @Result(property = "goodTemplateId", column = "goodTemplateId"),
            @Result(property = "typeNameOneId", column = "typeNameOneId"),
            @Result(property = "typeNameOneName", column = "typeNameOneName"),
            @Result(property = "typeNameTwoId", column = "typeNameTwoId"),
            @Result(property = "typeNameTwoName", column = "typeNameTwoName"),
            @Result(property = "barcodeId", column = "barcodeId"),
            @Result(property = "barcode", column = "barcode"),
            @Result(property = "brandId", column = "brandId"),
            @Result(property = "brandLogo", column = "brandLogo"),
            @Result(property = "brandName", column = "brandName"),
            @Result(property = "typeIconTwo", column = "typeIconTwo"),
            @Result(property = "typeImageTwo", column = "typeImageTwo"),
            @Result(property = "typeIconOne", column = "typeIconOne"),
            @Result(property = "typeImageOne", column = "typeImageOne")
    })
    List<GoodPriceCombinationVO> showAllGoodPriceis(@Param("map") Map<String, Object> map,@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    @Select("<script> select count(*) from (select GROUP_CONCAT(t11.barcode) as barcode " +
            "from ( " +
            "SELECT id,title,sell_price,sale_price,colleague_price,member_Price,discount_price,market_price,good_id,unit_id,cost_price,status,user_id,icon,num,sort,remark FROM `good_price` " +
            " where retrieve_status = 0  " +
            "<when test=\"map.priceStatus != null\"> and status = #{map.priceStatus} </when> " +
            ")t1 " +
            "INNER JOIN good_template t2 ON t1.good_id = t2.id " +
            "LEFT JOIN good_unit t3 ON t1.unit_id = t3.id " +
            "LEFT JOIN good_barcode t11 ON t1.id = t11.price_id " +
            "LEFT JOIN good_brand t13 ON t13.id = t2.brand_id " +
            "where 1 = 1 " +
            "<when test=\"map.priceTitle != null and map.priceTitle != ''\">and t1.title like concat('%',#{map.priceTitle},'%')</when>" +
            "<when test=\"map.barcode != null and map.barcode != ''\">and t11.barcode like concat('%',#{map.barcode},'%')</when>" +
            "<when test=\"map.goodName != null and map.goodName != ''\">and t2.name like concat('%',#{map.goodName},'%')</when>" +
            "<when test=\"map.goodStatus != null \">and t2.status = #{map.goodStatus} </when>" +
            "GROUP BY t1.id )t100 "+
            "</script>")
    int showAllCountGoodPriceis(@Param("map") Map<String, Object> map);

    @Select("<script> select " +column+" from  good_price where retrieve_status = 0 " +
            "<when test=\"priceTitle != null and priceTitle != ''\">and title like concat('%',#{priceTitle},'%')</when> " +
            "<when test=\"priceStatus != null and priceStatus != -1\">and status = #{priceStatus}</when> " +
            "<if test=\"goodIds != null and goodIds.size() > 0\">and good_id in  <foreach collection=\"goodIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> "+
            "<if test=\"lists != null and lists.size() > 0\">and id in  <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> "+
            " <when test=\"pageIndex != null and pageSize != null\"> " +
            "  limit #{pageIndex}, #{pageSize} " +
            "</when></script>")
    @ResultMap("goodPrice")
    List<GoodPrice> getGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(@Param("priceTitle") String priceTitle, @Param("priceStatus") Integer priceStatus,@Param("goodIds") List<String> goodTemplates, @Param("lists") List<String> barcodePriceId,
                                                                      @Param("pageIndex") int pageStartIndex, @Param("pageSize") int pageSize);

    @Select("<script> select " +column2+" from  good_price t1 left join" +
            " (select id from good_price where id in <foreach collection=\"notInPrice\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )t2 " +
            "on t1.id = t2.id  where t2.id is null and t1.retrieve_status = 0 " +
            "<when test=\"priceTitle != null and priceTitle != ''\">and t1.title like concat('%',#{priceTitle},'%')</when> " +
            "<when test=\"priceStatus != null and priceStatus != -1\">and t1.status = #{priceStatus}</when> " +
            "<if test=\"goodIds != null and goodIds.size() > 0\">and t1.good_id in  <foreach collection=\"goodIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> "+
            "<if test=\"lists != null and lists.size() > 0\">and t1.id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> "+
            " <when test=\"pageIndex != null and pageSize != null\"> " +
            "  limit #{pageIndex}, #{pageSize} " +
            "</when></script>")
    @ResultMap("goodPrice")
    List<GoodPrice> getGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(@Param("priceTitle") String priceTitle, @Param("priceStatus") Integer priceStatus,@Param("goodIds") List<String> goodTemplates, @Param("lists") List<String> barcodePriceId,
                                                                                   @Param("notInPrice") List<String> priceIds,@Param("pageIndex") int pageStartIndex, @Param("pageSize") int pageSize);
    @Select("<script> select count(1) from  good_price where retrieve_status = 0 " +
            "<when test=\"priceTitle != null and priceTitle != ''\">and title like concat('%',#{priceTitle},'%')</when> " +
            "<when test=\"priceStatus != null and priceStatus != -1\">and status = #{priceStatus}</when> " +
            "<if test=\"goodIds != null and goodIds.size() > 0\">and good_id in  <foreach collection=\"goodIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> "+
            "<if test=\"lists != null and lists.size() > 0\">and id in  <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> "+
            " </script>")
    int getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceId(@Param("priceTitle") String priceTitle, @Param("priceStatus") Integer priceStatus,@Param("goodIds") List<String> goodTemplates, @Param("lists") List<String> barcodePriceId);


    @Select("<script> select count(1) from  good_price t1 left join (select id from good_price where id in <foreach collection=\"notInPrice\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )t2 " +
            "on t1.id = t2.id  where t2.id is null and t1.retrieve_status = 0 " +
            "<when test=\"priceTitle != null and priceTitle != ''\">and t1.title like concat('%',#{priceTitle},'%')</when> " +
            "<when test=\"priceStatus != null and priceStatus != -1\">and t1.status = #{priceStatus}</when> " +
            "<if test=\"goodIds != null and goodIds.size() > 0\">and t1.good_id in  <foreach collection=\"goodIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> "+
            "<if test=\"lists != null and lists.size() > 0\">and t1.id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> "+
            "</script>")
    int getCountGoodPriceByTitleAndStatusAndGoodIdInAndPriceIdAndNotInPrice(@Param("priceTitle") String priceTitle, @Param("priceStatus") Integer priceStatus,@Param("goodIds") List<String> goodTemplates, @Param("lists") List<String> barcodePriceId,@Param("notInPrice")List<String> priceIds);


    @Select("<script> select " +column2+" from  good_price " +
            "t1 left join(select "+column+ " from good_price where retrieve_status = 0  " +
            "<if test=\"lists != null and lists.size() > 0\">and id in  <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> "+
            " )t2 on t1.id = t2.id  where t2.id is null " +
            "<if test=\"tempalteIds != null and tempalteIds.size() > 0 \"> and t1.good_id in <foreach collection=\"tempalteIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>" +
            "<if test=\"inPriceIds != null and inPriceIds.size() > 0 \"> and t1.id in <foreach collection=\"inPriceIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>" +
            "<when test=\"pageIndex != null and pageSize != null\"> " +
            "  limit #{pageIndex}, #{pageSize} " +
            "</when></script>")
    @ResultMap("goodPrice")
    List<GoodPrice> getGoodPriceNotAll(@Param("lists") List<String> priceIds,@Param("tempalteIds") List<String> templateIds,@Param("inPriceIds")List<String> inPriceIds,@Param("pageIndex") int pageStartIndex,@Param("pageSize")int pageSize);

    @Select("<script> select count(1) from  good_price t1 left join(select "+column+ " from good_price where retrieve_status = 0  " +
           "<if test=\"lists != null and lists.size() > 0\">and id in  <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> "+
            " )t2 on t1.id = t2.id  where t2.id is null " +
            "<if test=\"tempalteIds != null and tempalteIds.size() > 0 \"> and t1.good_id in <foreach collection=\"tempalteIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>" +
            "<if test=\"inPriceIds != null and inPriceIds.size() > 0 \"> and t1.id in <foreach collection=\"inPriceIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>" +
            "</script>")
    int getGoodPriceNotAllCount(@Param("lists") List<String> priceIds,@Param("tempalteIds") List<String> templateIds,@Param("inPriceIds")List<String> inPriceIds);

    @Update("<script> update good_price set retrieve_status = 1 where good_id in <foreach collection=\"tempalteIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    void batchDeleteGoodPriceByGoodId(@Param("tempalteIds")  List<String> goodIds);

    @Update("<script> update good_price " +
            "<set>" +
            "<if test=\"map.status != null\" >status = #{map.status}</if>" +
            "</set> where good_id = #{map.goodId}" +
            " </script>")
    void updateGoodPriceStatusByGoodId(@Param("map") Map<String, Object> map);


    @Update(" UPDATE `good`.`good_price` SET `icon` = #{icon} " +
            "WHERE `id` =#{id}")
    void  changeIconByBarcode(@Param("icon")String icon,@Param("id") String id);
}
