package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.good.GoodTerminal;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@Mapper
public interface GoodTerminalMapper {

    String column = "`id`, " +//
            "`terminal_id`, " +//显示终端
            "`good_price_id`, " +//价格Id
            "`user_id`, " +//
            "`create_time`"//
            ;

    @Insert("insert into good_terminal (" +
            " `id`,  " +
            " `terminal_id`,  " +
            " `good_price_id`,  " +
            " `user_id`  " +
            ")values(" +
            "#{id},  " +
            "#{terminalId},  " +
            "#{goodPriceId},  " +
            "#{userId}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addGoodTerminal(GoodTerminal goodTerminal);

    @Results(id = "goodTerminal", value = {
            @Result(property = "id", column = "id"), @Result(property = "terminalId", column = "terminal_id"),
            @Result(property = "goodPriceId", column = "good_price_id"),
            @Result(property = "userId", column = "user_id"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from good_terminal where id = #{id}")
    GoodTerminal getGoodTerminal(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_terminal   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodTerminal")
    List<GoodTerminal> getGoodTerminals( @Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from good_terminal   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("goodTerminal")
    List<GoodTerminal> getGoodTerminalAll();

    @Select("<script>select count(1) from good_terminal   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getGoodTerminalCount(@Param("status") int status, @Param("name") String name);

    @Update("update good_terminal  " +
            "set " +
            "`terminal_id` = #{terminalId}  , " +
            "`good_price_id` = #{goodPriceId}  , " +
            "`user_id` = #{userId}    " +
            " where id = #{id}")
    void changeGoodTerminal(GoodTerminal goodTerminal);

}
