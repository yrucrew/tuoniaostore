package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.good.GoodTemplate;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@Mapper
public interface GoodTemplateMapper {

    String column = "`id`, " +//
            "`name`, " +//商品名字
            "`first_letter`, " +//首字母
            "`all_letter`, " +//全拼
            "`banner_url`, " +//
            "`brand_id`, " +//品牌ID
            "`type_id`, " +//归属类型
            "`price_type`, " +//0.普通1.价格
            "`status`, " +//状态 0-可用 1-提交审核 2-审核不通过 3-失效
            "`image_url`, " +//
            "`description`, " +//商品描述
            "`create_time`, " +//上传时间
            "`shelf_life`, " +//保质期(天)
            "`sort`, " +//
            "`user_id`, " +//创建人
            "`remark`, " +//
            "`retrieve_status`"//
            ;

    @Insert("<script>insert into good_template (" +
            " `id`,  " +
            " `name`,  " +
            " `first_letter`,  " +
            " `all_letter`,  " +
            " `banner_url`,  " +
            " `brand_id`,  " +
            " `type_id`,  " +
            " `price_type`,  " +
            " `status`,  " +
            " `image_url`,  " +
            " `description`,  " +
            " `shelf_life`,  " +
            " `sort`,  " +
            " `user_id`,  " +
            " `remark`  " +
            ")" +
            "(select #{id},  " +
            "#{name},  " +
            "#{firstLetter},  " +
            "#{allLetter},  " +
            "#{bannerUrl},  " +
            "#{brandId},  " +
            "#{typeId},  " +
            "#{priceType},  " +
            "#{status},  " +
            "#{imageUrl},  " +
            "#{description},  " +
            "#{shelfLife},  " +
            "#{sort},  " +
            "#{userId},  " +
            "#{remark} from dual where (select count(*) from good_template where name = #{name} and retrieve_status = 0 ) <![CDATA[<]]> 1 ) " +
            "</script>")
    @Options(useGeneratedKeys = true)
    void addGoodTemplate(GoodTemplate goodTemplate);

    @Results(id = "goodTemplate", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "firstLetter", column = "first_letter"),
            @Result(property = "allLetter", column = "all_letter"),
            @Result(property = "bannerUrl", column = "banner_url"),
            @Result(property = "brandId", column = "brand_id"),
            @Result(property = "typeId", column = "type_id"),
            @Result(property = "priceType", column = "price_type"),
            @Result(property = "status", column = "status"),
            @Result(property = "imageUrl", column = "image_url"),
            @Result(property = "description", column = "description"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "shelfLife", column = "shelf_life"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "remark", column = "remark"),
            @Result(property = "retrieveStatus", column = "retrieve_status")})
    @Select("select " + column + " from good_template where id = #{id}")
    GoodTemplate getGoodTemplate(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_template   where   retrieve_status=#{retrieveStatus} " +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodTemplate")
    List<GoodTemplate> getGoodTemplates(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name,@Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from good_template   where  retrieve_status=0  and  status=0 </script>")//
    @ResultMap("goodTemplate")
    List<GoodTemplate> getGoodTemplateAll();

    @Select("<script>select  *  from good_template  where retrieve_status=0 " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    List<GoodTemplate> getGoodTemplateAllByPage(@Param("pageIndex")Integer pageStartIndex, @Param("pageSize")Integer pageSize);

    @Select("<script>select  " + column + "  from good_template where   retrieve_status=0  </script>")//
    @ResultMap("goodTemplate")
    List<GoodTemplate> loaderGoodTemplate();

    @Select("<script>select count(1) from good_template   where    retrieve_status=#{retrieveStatus} " +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getGoodTemplateCount(@Param("status") int status, @Param("name") String name,@Param("retrieveStatus")int retrieveStatus);

    @Update("<script>update good_template  " +
            "<set>" +
            "<when test=\"name != null\"> name = #{name}  , </when>" +
            "<when test=\"firstLetter != null\"> first_letter = #{firstLetter}  , </when>" +
            "<when test=\"allLetter != null\"> all_letter = #{allLetter}  , </when>" +
            "<when test=\"bannerUrl != null\"> banner_url = #{bannerUrl}  , </when>" +
            "<when test=\"brandId != null\"> brand_id = #{brandId}  ,</when> " +
            "<when test=\"typeId != null\"> type_id = #{typeId}  ,</when> " +
            "<when test=\"priceType != null\"> price_type = #{priceType}  ,</when> " +
            "<when test=\"status != null\"> status = #{status}  ,</when> " +
            "<when test=\"imageUrl != null\"> image_url = #{imageUrl}  ,</when> " +
            "<when test=\"description != null\"> description = #{description}  ,</when> " +
            "<when test=\"shelfLife != null\"> shelf_life = #{shelfLife}  , </when> " +
            "<when test=\"sort != null\"> sort = #{sort}  ,</when> " +
            "<when test=\"remark != null\"> remark = #{remark}  ,</when> " +
            "</set> where id = #{id}</script>")
    void changeGoodTemplate(GoodTemplate goodTemplate);

    @Select("<script>select "+ column +"from good_template where retrieve_status=0 <if test=\"goodName != null and goodName!= ''\">and name like concat('%',#{goodName},'%')</if>  " +
            "<if test=\"lists != null and lists.size > 0 \">and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            "UNION " +
            "SELECT "+ column +" FROM `good_template_pos` where retrieve_status=0 <if test=\"goodName != null and goodName!= ''\">and name like concat('%',#{goodName},'%')</if> " +
            "<if test=\"lists != null and lists.size > 0 \">and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            " limit 1</script>")
    List<GoodTemplate> getGoodTemplateByName(@Param("goodName") String goodName,@Param("lists") List<String> ids);

    @Select("<script>select "+ column +"from good_template where  brand_id = #{brandId} where retrieve_status=0 " +
            "UNION " +
            "SELECT "+ column +" FROM `good_template_pos`  where  brand_id = #{brandId} where retrieve_status=0 </script>")
    List<GoodTemplate> getGoodtemplateByBrandId(@Param("brandId") String brandId);


    @Results(id = "goodTemplateList", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "firstLetter", column = "first_letter"),
            @Result(property = "allLetter", column = "all_letter"),
            @Result(property = "bannerUrl", column = "banner_url"),
            @Result(property = "brandId", column = "brand_id"),
            @Result(property = "typeId", column = "type_id"),
            @Result(property = "priceType", column = "price_type"),
            @Result(property = "status", column = "status"),
            @Result(property = "imageUrl", column = "image_url"),
            @Result(property = "description", column = "description"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "shelfLife", column = "shelf_life"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "remark", column = "remark"),
            @Result(property = "retrieveStatus", column = "retrieve_status"),
            @Result(property = "list",column = "id",many = @Many(select = "com.tuoniaostore.good.data.GoodPriceMapper.listGoodPrice"))
    })
    @Select("<script> select "+ column +"from good_template where  retrieve_status=0 " +
            "<if test=\"lists != null and list.size > 0\"> and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>  " +
            "union " +
            " select "+ column +"from good_template_pos where retrieve_status=0 " +
            "<if test=\"lists != null and list.size > 0\"> and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>  " +
            "</script> ")
    List<GoodTemplate> listGoodTemplatesForAddChainGood(@Param("lists") List<String> ids);


    @Select("<script>select  " + column + "  from good_template   where   retrieve_status=0   " +
            " <when test=\"name != null and name.trim() != ''\">" +
            " and  name = #{name}" +
            "</when> limit 1</script>")
    @ResultMap("goodTemplate")
    GoodTemplate getGoodTemplateisRepetition(@Param("name") String name);



    @Update("<script> update good_template set retrieve_status = 1 where id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></script>")
    void deleteGoodTemplate(@Param("lists")List<String> idList);

    @Update("<script> update good_template " +
            "<set>" +
            "<when test=\"map.imageUrl != null\"> image_url = #{map.imageUrl},  </when>" +
            "<when test=\"map.goodBanner != null\"> banner_url = #{map.goodBanner},  </when>" +
            "</set> where id = #{map.id}" +
            "</script>")
    void updateGoodTemplateByParam(@Param("map") Map<String, Object> map);


    @Select("<script>select  " + column + "  from good_template   where   retrieve_status=#{retrieveStatus}   " +
            " <when test=\"name != null and name.trim() != ''\">" +
            " and  name like concat('%',#{name},'%') " +
            "</when>" +
            "<when test=\"goodStatus != null and goodStatus != -1 \">and status = #{goodStatus}</when>" +
            " </script>")
    @ResultMap("goodTemplate")
    List<GoodTemplate> getGoodTemplatesByGoodNameLikeAndGoodStatus(@Param("name") String name,@Param("goodStatus")Integer goodStatus,@Param("retrieveStatus") int retrieveStatus);

    @Insert("<script> insert into good_template " +
            " (`id`,  " +
            " `name`,  " +
            " `first_letter`,  " +
            " `all_letter`,  " +
            " `banner_url`,  " +
            " `brand_id`,  " +
            " `type_id`,  " +
            " `price_type`,  " +
            " `image_url`,  " +
            " `description`,  " +
            " `shelf_life`,  " +
            " `sort`,  " +
            " `user_id`,  " +
            " `remark` " +
            ") " +
            "<foreach collection=\"lists\" item=\"item\" separator=\"UNION ALL\"> " +
            "  (SELECT #{item.id},#{item.name},#{item.firstLetter},#{item.allLetter}, #{item.bannerUrl} , #{item.brandId}, #{item.typeId}, #{item.priceType}  " +
            " , #{item.imageUrl}, #{item.description}, #{item.description}, #{item.shelfLife}, #{item.userId}, #{item.remark}  FROM DUAL WHERE (SELECT COUNT(*) FROM  good_template " +
            " WHERE name = #{item.name} ) <![CDATA[<]]> 1 ) " +
            "</foreach> </script>")
    void batchAddGoodTemplate(@Param("lists") List<GoodTemplate> goodTemplateList);

}
