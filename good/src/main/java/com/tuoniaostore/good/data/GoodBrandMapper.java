package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.good.GoodBrand;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 商品归属品牌
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@Mapper
public interface GoodBrandMapper {

    String column = "`id`, " +//
            "`type`, " +//类型
            "`name`, " +//品牌名字
            "`logo`, " +//商标图
            "`introduce`, " +//简介
            "`status`, " +//{
            "`create_time`, " +//创建时间
            "`user_id`, " +//用户名
            "`sort`, " +//排序
            "`remark`, " +//备注
            "`retrieve_status`"//
            ;

    @Insert("<script> insert into good_brand (" +
            " `id`,  " +
            " `type`,  " +
            " `name`,  " +
            " `logo`,  " +
            " `introduce`,  " +
            " `status`,  " +
            " `user_id`,  " +
            " `sort`,  " +
            " `remark`   " +
            ")  (select " +
            "#{id},  " +
            "#{type},  " +
            "#{name},  " +
            "#{logo},  " +
            "#{introduce},  " +
            "#{status},  " +
            "#{userId},  " +
            "#{sort},  " +
            "#{remark}  " +
            " from dual where (select  count(*) from good_brand where name = #{name} and retrieve_status = 0) <![CDATA[<]]> 1) </script>")
    @Options(useGeneratedKeys = true)
    void addGoodBrand(GoodBrand goodBrand);

    @Results(id = "goodBrand", value = {
            @Result(property = "id", column = "id"), @Result(property = "type", column = "type"),
            @Result(property = "name", column = "name"), @Result(property = "logo", column = "logo"), @Result(property = "introduce", column = "introduce"), @Result(property = "status", column = "status"), @Result(property = "createTime", column = "create_time"),
            @Result(property = "userId", column = "user_id"), @Result(property = "sort", column = "sort"), @Result(property = "remark", column = "remark"), @Result(property = "retrieveStatus", column = "retrieve_status")})
    @Select("select " + column + " from good_brand where id = #{id} and retrieve_status = 0")
    GoodBrand getGoodBrand(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_brand   where   retrieve_status=0  " +
            "<when test=\"status != '-1'.toString()\">and status=#{status}</when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodBrand")
    List<GoodBrand> getGoodBrands(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from good_brand   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("goodBrand")
    List<GoodBrand> getGoodBrandAll();

    @Select("<script>select  " + column + "  from good_brand   where   retrieve_status=0  </script>")
    @ResultMap("goodBrand")
    List<GoodBrand> loaderGoodBrand();

    @Select("<script>select count(1) from good_brand   where   retrieve_status=0 " +
            " <if test=\"status != null and status != '-1'.toString() \">" +
            "  and status=#{status} " +
            " </if>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "</script>")
    int getGoodBrandCount(@Param("status") int status, @Param("name") String name);

    @Update("update good_brand  " +
            "set " +
            "`type` = #{type}  , " +
            "`name` = #{name}  , " +
            "`logo` = #{logo}  , " +
            "`introduce` = #{introduce}  , " +
            "`status` = #{status}  , " +
            "`create_time` = #{createTime}  , " +
            "`user_id` = #{userId}  , " +
            "`sort` = #{sort}  , " +
            "`remark` = #{remark}  , " +
            "`retrieve_status` = #{retrieveStatus}  " +
            " where id = #{id}")
    void changeGoodBrand(GoodBrand goodBrand);

    @Select("<script>select  " + column + "  from good_brand   where   retrieve_status=0  and status=0 <if test=\"name != null and name != ''\"> and name like concat('%', #{name}, '%')</if> " +
            "<if test=\"lists != null\"> and id in<foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>" +
            " </script>")
    @ResultMap("goodBrand")
    List<GoodBrand> getGoodBranByIds(@Param("lists") List<String> goodBrands,@Param("name") String name);

    @Select("<script> select id from " +
            "(select id,brand_id FROM good_template where retrieve_status = 0 " +
            " UNION " +
            "select id,brand_id FROM good_template_pos where retrieve_status = 0 ) t2" +
            "where t2.brand_id in (SELECT t1.id FROM `good_brand` t1 where t1.name concat ('%', #{name}, '%') )" +
            " </script>")
    List<String> getGoodTemplateIdsByBrandName(@Param("name") String goodBrandName);

    @Select("<script>select "+ column +" from good_brand   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    @ResultMap("goodBrand")
    List<GoodBrand> getBrandByBrandNameAndStatus(@Param("name") String keyWord, @Param("status")int status);

    @Update("<script>update good_brand  " +
            "set " +
            "`retrieve_status` = 1 " +
            " where id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    void deleteGoodBrand(@Param("lists") List<String> id);

    @Select("<script>select  " + column + "  from good_brand   where   retrieve_status=0  " +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodBrand")
    List<GoodBrand> getGoodBrandsByPage(@Param("pageIndex") Integer pageStartIndex, @Param("pageSize") Integer pageSize);

    @Select("<script>select  " + column + "  from good_brand   where   1 = 1 and retrieve_status = 0 " +
            " <when test=\"name != null and name.trim() != ''\">" +
            " and  name = #{name}" +
            "</when> limit 1 </script>")
    @ResultMap("goodBrand")
   GoodBrand getGoodBrandisRepetition(@Param("name")String name);

    @Select("<script>select  " + column + "  from good_brand  where id = #{id}</script>")
    @ResultMap("goodBrand")
    GoodBrand getGoodBrandForDelete(@Param("id") String id);

    @Update("<script>  update good_brand " +
            "<set>" +
            "<when test=\"map.brandLogo != null and map.brandLogo != ''\">logo = #{map.brandLogo},</when> " +
            "</set> where id = #{map.id} "+
            " </script>")
    void updateGoodBrandByParam(@Param("map") Map<String, Object> map);

    @Insert("<script> insert into good_brand " +
            " (`id`,  " +
            " `type`,  " +
            " `name`,  " +
            " `logo`,  " +
            " `introduce`,  " +
            " `status`,  " +
            " `user_id`,  " +
            " `sort`,  " +
            " `remark`,  " +
            " `create_time`  " +
            ") " +
            "<foreach collection=\"lists\" item=\"item\" separator=\"UNION ALL\"> " +
            " (SELECT #{item.id},#{item.type},#{item.name},#{item.logo}, #{item.introduce}, #{item.status}, #{item.userId}, #{item.sort}, #{item.remark}, #{item.createTime}  " +
            " FROM DUAL WHERE (SELECT COUNT(*) FROM  good_brand  WHERE name = #{item.name} and retrieve_status = 0 ) <![CDATA[<]]> 1 ) " +
            "</foreach> </script>")
    void batchAddGoodBrand(@Param("lists") List<GoodBrand> goodBrandListd);
}
