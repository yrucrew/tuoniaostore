package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.good.GoodBarcode;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@Mapper
public interface GoodBarcodeMapper {

    String column = "`id`, " +//条码ID
            "`barcode`, " +//条码
            "`price_id`, " +//价格ID
            "`barcode_parent_id`, " +//父条码Id
            "`user_id`, " +//添加用户ID
            "`create_time`"//
            ;

    @Insert("insert into good_barcode (" +
            " `id`,  " +
            " `barcode`,  " +
            " `price_id`,  " +
            " `barcode_parent_id`,  " +
            " `user_id`  " +
            ")values(" +
            "#{id},  " +
            "#{barcode},  " +
            "#{priceId},  " +
            "#{barcodeParentId},  " +
            "#{userId} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addGoodBarcode(GoodBarcode goodBarcode);

    @Insert("<script> insert into good_barcode " +
            " (`id`,  " +
            " `barcode`,  " +
            " `price_id`,  " +
            " `barcode_parent_id`,  " +
            " `user_id`  " +
            ") " +
            "<foreach collection=\"lists\" item=\"item\" separator=\"UNION ALL\"> " +
            " (SELECT #{item.id},#{item.barcode},#{item.priceId},#{item.barcodeParentId}, #{item.userId}  FROM DUAL WHERE (SELECT COUNT(*) FROM  good_barcode " +
            " WHERE price_id = #{item.priceId} and barcode = #{item.barcode} ) <![CDATA[<]]> 1 ) " +
            "</foreach> </script>")
    void batchAddGoodBarcode(@Param("lists") List<GoodBarcode> barcodes);

    @Results(id = "goodBarcode", value = {
            @Result(property = "id", column = "id"), @Result(property = "barcode", column = "barcode"), @Result(property = "priceId", column = "price_id"), @Result(property = "barcodeParentId", column = "barcode_parent_id"),
            @Result(property = "userId", column = "user_id"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from good_barcode where id = #{id}")
    GoodBarcode getGoodBarcode(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_barcode" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodBarcode")
    List<GoodBarcode> getGoodBarcodes(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from good_barcode </script>")
    @ResultMap("goodBarcode")
    List<GoodBarcode> getGoodBarcodeAll();

    @Select("<script>select  " + column + "  from good_barcode  where price_id = #{priceId}  limit 1 </script>")
    @ResultMap("goodBarcode")
     GoodBarcode getGoodBarcodeByPriceId(@Param("priceId") String priceId);

    @Select("<script>select  " + column + "  from good_barcode   where barcode =#{barcode}  limit 1 </script>")
    @ResultMap("goodBarcode")
     GoodBarcode getGoodBarcodeByBarcode(@Param("barcode") String barcode);

    @Select("<script>select count(1) from good_barcode   " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getGoodBarcodeCount(@Param("status") int status, @Param("name") String name);

    @Update("update good_barcode  " +
            "set " +
            "`barcode` = #{barcode}  , " +
            "`price_id` = #{priceId}  , " +
            "`barcode_parent_id` = #{barcodeParentId}  , " +
            "`user_id` = #{userId} " +
            " where id = #{id}")
    void changeGoodBarcode(GoodBarcode goodBarcode);

    @Select("<script>select  " + column + "  from good_barcode  where price_id = #{priceId}</script>")
    @ResultMap("goodBarcode")
    List<GoodBarcode> getBarcodesByPriceId(@Param("priceId") String priceId);

    @Select("<script>select  " + column + "  from good_barcode  where 1 = 1 <if test=\"barcode != null and barcode != ''\">and barcode like concat('%', #{barcode}, '%') </if> </script>")
    @ResultMap("goodBarcode")
    List<GoodBarcode> getGoodBarcodeByBarcodeLike(@Param("barcode")String barcode);

    @Select("<script>select  " + column + "  from good_barcode  where price_id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    @ResultMap("goodBarcode")
    List<GoodBarcode> getGoodBarcodeByPriceIds(@Param("lists") List<String> s);

    @Select("<script>select  " + column + "  from good_barcode  where barcode in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    @ResultMap("goodBarcode")
    List<GoodBarcode> getGoodBarcodeByBarcodes(@Param("lists") List<String> list);

    @Select("<script>select  " + column + "  from good_barcode  where  price_id = #{priceId}</script>")
    @ResultMap("goodBarcode")
    List<GoodBarcode> getGoodBarcodesByPriceId(@Param("priceId") String goodPrice);

    @Update("<script> update good_barcode " +
            "<set>" +
            "<if test=\"map.id != null \"> barcode = #{map.barcode}, </if> " +
            "</set> where id = #{map.id}" +
            " </script>")
    void updateGoodBarcodeParam(@Param("map") Map<String, Object> map);
}
