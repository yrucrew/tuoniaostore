package com.tuoniaostore.good.data;

import com.tuoniaostore.datamodel.good.GoodImage;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Component
@Mapper
public interface GoodImageMapper {

    String column = "`id`, " +//
            "`title`, " +//
            "`image_url`, " +//
            "`good_template_id`, " +//
            "`status`, " +//
            "`type`, " +//类型
            "`sort`, " +//
            "`user_id`, " +//创建人
            "`price_id`, " +//
            "`remark`, " +//
            "`create_time`, " +//
            "`retrieve_status`"//
            ;

    @Insert("insert into good_image (" +
            " `id`,  " +
            " `title`,  " +
            " `image_url`,  " +
            " `good_template_id`,  " +
            " `status`,  " +
            " `type`,  " +
            " `sort`,  " +
            " `user_id`,  " +
            " `price_id`,  " +
            " `remark`  " +
            ")values(" +
            "#{id},  " +
            "#{title},  " +
            "#{imageUrl},  " +
            "#{goodTemplateId},  " +
            "#{status},  " +
            "#{type},  " +
            "#{sort},  " +
            "#{userId},  " +
            "#{priceId},  " +
            "#{remark}   " +
            ")")
    @Options(useGeneratedKeys = true)
    void addGoodImage(GoodImage goodImage);

    @Results(id = "goodImage", value = {
            @Result(property = "id", column = "id"), @Result(property = "title", column = "title"),
            @Result(property = "imageUrl", column = "image_url"),
            @Result(property = "goodTemplateId", column = "good_template_id"),
            @Result(property = "priceId", column = "price_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "type", column = "type"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "remark", column = "remark"),
            @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from good_image where id = #{id}")
    GoodImage getGoodImage(@Param("id") String id);

    @Select("<script>select  " + column + "  from good_image   where   retrieve_status=0  and good_template_id=#{goodTemplateId} " +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status}" +
            " </when>" +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("goodImage")
    List<GoodImage> getGoodImages(@Param("status") int status, @Param("pageIndex") int pageIndex,
                                  @Param("pageSize") int pageSize, @Param("title") String title, @Param("goodTemplateId") String goodTemplateId);

    @Select("<script>select  " + column + "  from good_image   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("goodImage")
    List<GoodImage> getGoodImageAll();

    @Select("<script>select count(1) from good_image   where   retrieve_status=0    and good_template_id=#{goodTemplateId}" +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status}" +
            " </when>" +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when></script>")
    int getGoodImageCount(@Param("status") int status, @Param("title") String title, @Param("goodTemplateId") String goodTemplateId);

    @Select("<script>select count(1) from good_image   where   retrieve_status=#{retrieveStatus}  and good_template_id=#{goodTemplateId}</script>")
    int getGoodImageGoodIdCount(@Param("goodTemplateId") String goodTemplateId,@Param("retrieveStatus") int retrieveStatus);

    @Update("update good_image  " +
            "set " +
            "`title` = #{title}  , " +
            "`image_url` = #{imageUrl}  , " +
            "`good_template_id` = #{goodTemplateId}  , " +
            "`status` = #{status}  , " +
            "`type` = #{type}  , " +
            "`sort` = #{sort}  , " +
            "`price_id` = #{priceId}  , " +
            "`remark` = #{remark}   " +
            " where id = #{id}")
    void changeGoodImage(GoodImage goodImage);

}
