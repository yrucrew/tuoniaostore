package com.tuoniaostore.good.controller;

import com.alibaba.fastjson.JSONObject;

import com.tuoniaostore.good.service.GoodShopCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 购物车
 * @author sqd
 * @date 2019/4/5
 * @param
 * @return
 */
@Controller
@RequestMapping("goodShopCart")
public class GoodShopCartController {
    @Autowired
    private GoodShopCartService goodShopCartService;

    /**
     * 商品添加购物车
     * @author sqd
     * @date 2019/4/5
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("addgoodShopCart")
    @ResponseBody
    public JSONObject addGoodBrand() {
        return goodShopCartService.addGoodShopCart();
    }
    /**
     * 查询购物车商品
     * @author sqd
     * @date 2019/4/9
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodShopCartByUserId")
    @ResponseBody
    public JSONObject getGoodShopCartByUserId() {
        return goodShopCartService.getGoodShopCartByUserId();
    }

    /**
     * 修改购物车商品数量
     * @author sqd
     * @date 2019/4/9
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("changeGoodShopCartByUserId")
    @ResponseBody
    public JSONObject changeGoodShopCartByUserId() {
        return goodShopCartService.changeGoodShopCart();
    }
    /**
     * 下单付款确认界面数据查询
     * @author sqd
     * @date 2019/4/10
     * @param
     * @return
     */
    @RequestMapping("selectGoodInventory")
    @ResponseBody
    public JSONObject selectGoodInventory() {
        return goodShopCartService.selectGoodInventory();
    }

    /**
     * 查询购物车商品
     * @author sqd
     * @date 2019/4/11
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodShopCartByUserIdAndGoodId")
    @ResponseBody
    public JSONObject getGoodShopCartByUserIdAndGoodId() {
        return goodShopCartService.getGoodShopCartByUserIdAndGoodId();
    }

    /**
     * 修改购物车商品状态
     * @author sqd
     * @date 2019/4/11
     * @param
     * @return
     */
    @RequestMapping("changeGoodShopCartStauts")
    @ResponseBody
    public JSONObject changeGoodShopCartStauts() {
        return goodShopCartService.changeGoodShopCartStauts();
    }

    /**
     * 购物车商品删除
     * @author sqd
     * @date 2019/4/12
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("deleteGoodShopCartIds")
    @ResponseBody
    public  JSONObject deleteGoodShopCartIds(){
        return goodShopCartService.deleteGoodShopCartIds();
    }

    /**
     * 购物车的信息统计（多少件商品，合计多少钱）
     * @author oy
     * @date 2019/4/16
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodShopCartMessage")
    @ResponseBody
    public  JSONObject getGoodShopCartMessage(){
        return goodShopCartService.getGoodShopCartMessage();
    }

    /**
     * 单个商品添加购物车
     * @author sqd
     * @date 2019/4/17
     * @param
     * @return
     */
    @RequestMapping("addGoodShopCartgood")
    @ResponseBody
    public  JSONObject addGoodShopCartgood(){
        return goodShopCartService.addGoodShopCartgood();
    }

    /**
     * 通过用户id 获取当前的购物车商品信息
     * @author oy
     * @date 2019/5/29
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodShopCartListByUserId")
    @ResponseBody
    public  JSONObject getGoodShopCartListByUserId(){
        return goodShopCartService.getGoodShopCartListByUserId();
    }

}
