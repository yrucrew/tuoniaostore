package com.tuoniaostore.good.controller;

import com.tuoniaostore.good.service.GoodBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * 商品归属品牌
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Controller
@RequestMapping("goodBrand")
public class GoodBrandController {
    @Autowired
    private GoodBrandService goodBrandService;


    @RequestMapping("addGoodBrand")
    @ResponseBody
    public JSONObject addGoodBrand() {
        return goodBrandService.addGoodBrand();
    }

    @RequestMapping("getGoodBrand")
    @ResponseBody
    public JSONObject getGoodBrand() {
        return goodBrandService.getGoodBrand();
    }


    @RequestMapping("getGoodBrands")
    @ResponseBody
    public JSONObject getGoodBrands() {
        return goodBrandService.getGoodBrands();
    }

    @RequestMapping("getGoodBrandAll")
    @ResponseBody
    public JSONObject getGoodBrandAll() {
        return goodBrandService.getGoodBrandAll();
    }

    @RequestMapping("getGoodBrandCount")
    @ResponseBody
    public JSONObject getGoodBrandCount() {
        return goodBrandService.getGoodBrandCount();
    }

    @RequestMapping("changeGoodBrand")
    @ResponseBody
    public JSONObject changeGoodBrand() {
        return goodBrandService.changeGoodBrand();
    }

    @RequestMapping("deleteGoodBrand")
    @ResponseBody
    public JSONObject deleteGoodBrand() {
        return goodBrandService.deleteGoodBrand();
    }


    /***
     * 根据品牌brandIds查找对应的记录
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getBarcodesByBrandIds")
    @ResponseBody
    public JSONObject getBarcodesByBrandIds() {
        return goodBrandService.getBarcodesByBrandIds();
    }

    /***
     * 根据品牌 brandName 查找对应的记录
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getBarcodesByBrandName")
    @ResponseBody
    public JSONObject getBarcodesByBrandName() {
        return goodBrandService.getBarcodesByBrandName();
    }

    /***
     * 模糊查询品牌
     * @author oy
     * @date 2019/4/20
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getBrandByBrandNameAndStatus")
    @ResponseBody
    public JSONObject getBrandByBrandNameAndStatus() {
        return goodBrandService.getBrandByBrandNameAndStatus();
    }

    /**
     * 推荐品牌的品牌列表获取
     * @author oy
     * @date 2019/5/1
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getBrandByHomePageGood")
    @ResponseBody
    public JSONObject getBrandByHomePageGood() {
        return goodBrandService.getBrandByHomePageGood();
    }

    /**
     * 验证品牌名是否重复
     * @author sqd
     * @date 2019/5/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodBrandisRepetition")
    @ResponseBody
    public JSONObject getGoodBrandisRepetition() {
        return goodBrandService.getGoodBrandisRepetition();
    }

}
