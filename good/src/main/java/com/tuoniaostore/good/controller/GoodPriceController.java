package com.tuoniaostore.good.controller;

import com.tuoniaostore.good.service.GoodPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Controller
@RequestMapping("goodPrice")
public class GoodPriceController {
    @Autowired
    private GoodPriceService goodPriceService;


    @RequestMapping("addGoodPrice")
    @ResponseBody
    public JSONObject addGoodPrice() {
        return goodPriceService.addGoodPrice();
    }

    @RequestMapping("getGoodPrice")
    @ResponseBody
    public JSONObject getGoodPrice() {
        return goodPriceService.getGoodPrice();
    }


    @RequestMapping("getGoodPrices")
    @ResponseBody
    public JSONObject getGoodPrices() {
        return goodPriceService.getGoodPrices();
    }

    @RequestMapping("getGoodPriceAll")
    @ResponseBody
    public JSONObject getGoodPriceAll() {
        return goodPriceService.getGoodPriceAll();
    }

    @RequestMapping("getGoodPriceCount")
    @ResponseBody
    public JSONObject getGoodPriceCount() {
        return goodPriceService.getGoodPriceCount();
    }

    @RequestMapping("changeGoodPrice")
    @ResponseBody
    public JSONObject changeGoodPrice() {
        return goodPriceService.changeGoodPrice();
    }

    @RequestMapping("getGoodPriceByGoodIdAll")
    @ResponseBody
    public JSONObject getGoodPriceByGoodIdAll() {
        return goodPriceService.getGoodPriceByGoodIdAll();
    }

    @RequestMapping("updateGoodPrice")
    @ResponseBody
    public JSONObject updateGoodPrice() {
        return goodPriceService.updateGoodPrice();
    }

    @RequestMapping("batchDeleteGoodPrice")
    @ResponseBody
    public JSONObject batchDeleteGoodPrice() {
        return goodPriceService.batchDeleteGoodPrice();
    }

    /**
     * 验证商品标签名是否重复
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/3
     */
    @RequestMapping("getGoodPriceisRepetition")
    @ResponseBody
    public JSONObject getGoodPriceisRepetition() {
        return goodPriceService.getGoodPriceisRepetition();
    }

    /**
     * 展示所有的商品标签
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/16
     */
    @RequestMapping("showAllGoodPriceis")
    @ResponseBody
    public JSONObject showAllGoodPriceis() {
        return goodPriceService.showAllGoodPriceis();
    }


    /**
     * 展示所有的商品标签2
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/17
     */
    @RequestMapping("showAllGoodPriceis2")
    @ResponseBody
    public JSONObject showAllGoodPriceis2() {
        return goodPriceService.showAllGoodPriceis2();
    }


    /***
     * 新增价格标签
     * @author oy
     * @date 2019/5/16
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("addGoodPrice2")
    @ResponseBody
    public JSONObject addGoodPrice2() {
        return goodPriceService.addGoodPrice2();
    }

    /***
     * 修改价格标签
     * @author oy
     * @date 2019/5/16
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("editGoodPrice2")
    @ResponseBody
    public JSONObject editGoodPrice2() {
        return goodPriceService.editGoodPrice2();
    }

    /***
     * 验证价格标签是否存在
     * @author oy
     * @date 2019/5/16
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("validateGoodPrice")
    @ResponseBody
    public JSONObject validateGoodPrice() {
        return goodPriceService.validateGoodPrice();
    }

    /**
     * 价格标签页面修改部分的参数
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/17
     */
    @RequestMapping("updateSomeParamForGoodPriceList")
    @ResponseBody
    public JSONObject updateSomeParamForGoodPriceList() {
        return goodPriceService.updateSomeParamForGoodPriceList();
    }

    /**
     * 根据价格标签获取对应的模板数据（java 缓存拿）
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/21
     */
    @RequestMapping("getDateByPriceId")
    @ResponseBody
    public JSONObject getDateByPriceId() {
        return goodPriceService.getDateByPriceId();
    }

    /**
     * 修改价格标签中的参数
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/25
     */
    @RequestMapping("updateByPriceByParam")
    @ResponseBody
    public JSONObject updateByPriceByParam() {
        return goodPriceService.updateByPriceByParam();
    }

    /**
     * 自动化导入商品标签
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/27
     */
    @RequestMapping("importGood")
    @ResponseBody
    public JSONObject importGood() {
        return goodPriceService.importGood();
    }

    @RequestMapping("changeIconByBarcode")
    @ResponseBody
    public JSONObject changeIconByBarcode() {
        return goodPriceService.changeIconByBarcode();
    }

    @RequestMapping("importIcon")
    @ResponseBody
    public JSONObject importIcon() {
        return goodPriceService.importIcon();
    }


}
