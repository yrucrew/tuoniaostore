package com.tuoniaostore.good.controller;
        import com.tuoniaostore.good.service.GoodUnitService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Controller
@RequestMapping("goodUnit")
public class GoodUnitController {
    @Autowired
    private GoodUnitService goodUnitService;
    @RequestMapping("addGoodUnit")
    @ResponseBody
    public JSONObject addGoodUnit() {
        return goodUnitService.addGoodUnit();
    }

    @RequestMapping("getGoodUnitTree")
    @ResponseBody
    public JSONObject getGoodUnitTree() {
        return goodUnitService.getGoodUnitTree();
    }

    @RequestMapping("getGoodUnit")
    @ResponseBody
    public JSONObject getGoodUnit() {
        return goodUnitService.getGoodUnit();
    }


    @RequestMapping("getGoodUnits")
    @ResponseBody
    public JSONObject getGoodUnits() {
        return goodUnitService.getGoodUnits();
    }

    @RequestMapping("getGoodUnitAll")
    @ResponseBody
    public JSONObject getGoodUnitAll() {
        return goodUnitService.getGoodUnitAll();
    }

    @RequestMapping("getGoodUnitCount")
    @ResponseBody
    public JSONObject getGoodUnitCount() {
        return goodUnitService.getGoodUnitCount();
    }

    @RequestMapping("changeGoodUnit")
    @ResponseBody
    public JSONObject changeGoodUnit() {
        return goodUnitService.changeGoodUnit();
    }

    @RequestMapping("loaderGoodsUnits")
    @ResponseBody
    public JSONObject loaderGoodsUnits() {
        return goodUnitService.loaderGoodsUnits();
    }

    @RequestMapping("deleteGoodsUnits")
    @ResponseBody
    public JSONObject deleteGoodsUnits() {
        return goodUnitService.deleteGoodsUnits();
    }


    /***
     * 通过title 查找对应的unit
     * @author oy
     * @date 2019/4/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodUnitByTitle")
    @ResponseBody
    public JSONObject getGoodUnitByTitle() {
        return goodUnitService.getGoodUnitByTitle();
    }

    /**
     * 验证单位名是否重复
     * @author sqd
     * @date 2019/5/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodUnitisRepetition")
    @ResponseBody
    public JSONObject getGoodUnitisRepetition() {
        return goodUnitService.getGoodUnitisRepetition();
    }
}
