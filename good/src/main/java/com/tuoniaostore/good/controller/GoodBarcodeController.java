package com.tuoniaostore.good.controller;
        import com.tuoniaostore.good.service.GoodBarcodeService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Controller
@RequestMapping("goodBarcode")
public class GoodBarcodeController {
    @Autowired
    private GoodBarcodeService goodBarcodeService;


    @RequestMapping("addGoodBarcode")
    @ResponseBody
    public JSONObject addGoodBarcode() {
        return goodBarcodeService.addGoodBarcode();
    }

    @RequestMapping("getGoodBarcode")
    @ResponseBody
    public JSONObject getGoodBarcode() {
        return goodBarcodeService.getGoodBarcode();
    }


    @RequestMapping("getGoodBarcodes")
    @ResponseBody
    public JSONObject getGoodBarcodes() {
        return goodBarcodeService.getGoodBarcodes();
    }

    @RequestMapping("getGoodBarcodeAll")
    @ResponseBody
    public JSONObject getGoodBarcodeAll() {
        return goodBarcodeService.getGoodBarcodeAll();
    }

    @RequestMapping("getGoodBarcodeCount")
    @ResponseBody
    public JSONObject getGoodBarcodeCount() {
        return goodBarcodeService.getGoodBarcodeCount();
    }

    @RequestMapping("changeGoodBarcode")
    @ResponseBody
    public JSONObject changeGoodBarcode() {
        return goodBarcodeService.changeGoodBarcode();
    }


    @RequestMapping("getGoodBarcodeByPriceId")
    @ResponseBody
    public JSONObject getGoodBarcodeByPriceId() {
        return goodBarcodeService.getGoodBarcodeByPriceId();
    }

    /**
     * 商家端：商品 添加商品 填写条码 返回对应的默认价格标签和单位
     * @author oy
     * @date 2019/4/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodMsgByGoodBarcode")
    @ResponseBody
    public JSONObject getGoodMsgByGoodBarcode() {
        return goodBarcodeService.getGoodMsgByGoodBarcode();
    }

    /**
     * 商家端：商品 添加商品 填写条码
     * @author oy
     * @date 2019/4/11
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodMsgByGoodBarcode2")
    @ResponseBody
    public JSONObject getGoodMsgByGoodBarcode2() {
        return goodBarcodeService.getGoodMsgByGoodBarcode2();
    }

    /**
     * 根据价格id 获取条码的集合
     * @author oy
     * @date 2019/4/11
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getBarcodesByPriceId")
    @ResponseBody
    public JSONObject getBarcodesByPriceId() {
        return goodBarcodeService.getBarcodesByPriceId();
    }

    /**
     * 根据模糊查询 返回条码集合
     * @author oy
     * @date 2019/4/24
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodBarcodeByBarcodesLike")
    @ResponseBody
    public JSONObject getGoodBarcodeByBarcodesLike() {
        return goodBarcodeService.getGoodBarcodeByBarcodesLike();
    }

    /**
     * 通过价格id的集合 查询对应的条码集合（远端）
     * @author oy
     * @date 2019/5/14
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodBarcodeByPriceIds")
    @ResponseBody
    public JSONObject getGoodBarcodeByPriceIds() {
        return goodBarcodeService.getGoodBarcodeByPriceIds();
    }

    /**
     * 修改条码(商品规格页面)
     * @author oy
     * @date 2019/5/25
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("changeGoodBarcodeById")
    @ResponseBody
    public JSONObject changeGoodBarcodeById() {
        return goodBarcodeService.changeGoodBarcodeById();
    }

    /**
     * 通过goodTemplateId获取所有的条码
     * @author oy
     * @date 2019/6/5
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodBarcodeByGoodTemplateId")
    @ResponseBody
    public JSONObject getGoodBarcodeByGoodTemplateId() {
        return goodBarcodeService.getGoodBarcodeByGoodTemplateId();
    }

}
