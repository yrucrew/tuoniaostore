package com.tuoniaostore.good.controller;
        import com.tuoniaostore.good.service.GoodTemplateService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Controller
@RequestMapping("goodTemplate")
public class GoodTemplateController {
    @Autowired
    private GoodTemplateService goodTemplateService;


    @RequestMapping("addGoodTemplate")
    @ResponseBody
    public JSONObject addGoodTemplate() {
        return goodTemplateService.addGoodTemplate();
    }

    /***
     * 添加模板信息（里面含有价格标签）
     * @author oy
     * @date 2019/5/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("addGoodTemplate2")
    @ResponseBody
    public JSONObject addGoodTemplate2() {
        return goodTemplateService.addGoodTemplate2();
    }

    @RequestMapping("getGoodTemplate")
    @ResponseBody
    public JSONObject getGoodTemplate() {
        return goodTemplateService.getGoodTemplate();
    }


    @RequestMapping("getGoodTemplates")
    @ResponseBody
    public JSONObject getGoodTemplates() {
        return goodTemplateService.getGoodTemplates();
    }

    @RequestMapping("getGoodTemplateAll")
    @ResponseBody
    public JSONObject getGoodTemplateAll() {
        return goodTemplateService.getGoodTemplateAll();
    }

    @RequestMapping("getGoodTemplateCount")
    @ResponseBody
    public JSONObject getGoodTemplateCount() {
        return goodTemplateService.getGoodTemplateCount();
    }

    @RequestMapping("changeGoodTemplate")
    @ResponseBody
    public JSONObject changeGoodTemplate() {
        return goodTemplateService.changeGoodTemplate();
    }

    @RequestMapping("deleteGoodTemplate")
    @ResponseBody
    public JSONObject deleteGoodTemplate() {
        return goodTemplateService.deleteGoodTemplate();
    }


    /**
     * 商家端：填写商品名称 获取对应的模糊查询模板（防止用户加入太多垃圾模板）
     * @author oy
     * @date 2019/4/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodTemplateByName")
    @ResponseBody
    public JSONObject getGoodTemplateByName() {
        return goodTemplateService.getGoodTemplateByName();
    }

    /**
     * 根据品牌获取对应的模板
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodTemplatesByBrandId")
    @ResponseBody
    public JSONObject getGoodTemplatesByBrandId() {
        return goodTemplateService.getGoodTemplatesByBrandId();
    }

    /**
     * 展示模板数据，供添加商店商品
     * @author oy
     * @date 2019/4/23
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("listGoodTemplatesForAddChainGood")
    @ResponseBody
    public JSONObject listGoodTemplatesForAddChainGood() {
        return goodTemplateService.listGoodTemplatesForAddChainGood();
    }

    /***
     * 商家绑定商品选择列表
     * @author oy
     * @date 2019/5/7
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("listGoodTemplatesForAddChainGood2")
    @ResponseBody
    public JSONObject listGoodTemplatesForAddChainGood2() {
        return goodTemplateService.listGoodTemplatesForAddChainGood2();
    }



    /**
     * 验证商品名是否重复
     * @author sqd
     * @date 2019/5/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodTemplateisRepetition")
    @ResponseBody
    public JSONObject getGoodTemplateisRepetition() {
        return goodTemplateService.getGoodTemplateisRepetition();
    }
    /**
     * 验证商品名是否重复
     * @author sqd
     * @date 2019/5/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("loadGood")
    @ResponseBody
    public JSONObject loadGood() {
        return goodTemplateService.loadGood();
    }


}
