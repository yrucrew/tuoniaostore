package com.tuoniaostore.good.controller;
        import com.tuoniaostore.good.service.GoodTypeService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Controller
@RequestMapping("goodType")
public class GoodTypeController {
    @Autowired
    private GoodTypeService goodTypeService;

    @RequestMapping("addGoodType")
    @ResponseBody
    public JSONObject addGoodType() {
        return goodTypeService.addGoodType();
    }

    @RequestMapping("getGoodType")
    @ResponseBody
    public JSONObject getGoodType() {
        return goodTypeService.getGoodType();
    }

    @RequestMapping("getGoodTypes")
    @ResponseBody
    public JSONObject getGoodTypes() {
        return goodTypeService.getGoodTypes();
    }

    @RequestMapping("getGoodTypeAll")
    @ResponseBody
    public JSONObject getGoodTypeAll() {
        return goodTypeService.getGoodTypeAll();
    }

    @RequestMapping("getGoodTypeTreeAll")
    @ResponseBody
    public JSONObject getGoodTypeTreeAll() {
        return goodTypeService.getGoodTypeTreeAll();
    }

    @RequestMapping("getGoodTypeCount")
    @ResponseBody
    public JSONObject getGoodTypeCount() {
        return goodTypeService.getGoodTypeCount();
    }

    @RequestMapping("changeGoodType")
    @ResponseBody
    public JSONObject changeGoodType() {
        return goodTypeService.changeGoodType();
    }

    @RequestMapping("getGoodTypeParentTree")
    @ResponseBody
    public JSONObject getGoodTypeParentTree() {
        return goodTypeService.getGoodTypeParentTree();
    }

    @RequestMapping("getGoodTypeTree")
    @ResponseBody
    public JSONObject getGoodTypeTree() {
        return goodTypeService.getGoodTypeTree();
    }

    @RequestMapping("getGoodTypeParent")
    @ResponseBody
    public JSONObject getGoodTypeParent() {
        return goodTypeService.getGoodTypeParent();
    }


    @RequestMapping("getGoodTypesById")
    @ResponseBody
    public JSONObject getGoodTypesById() {
        return goodTypeService.getGoodTypesById();
    }

    @RequestMapping("getGoodTypesById2")
    @ResponseBody
    public JSONObject getGoodTypesById2() {
        return goodTypeService.getGoodTypesById2();
    }

    @RequestMapping("getGoodTypesByPage")
    @ResponseBody
    public JSONObject getGoodTypesByPage() {
        return goodTypeService.getGoodTypesByPage();
    }

    @RequestMapping("getGoodTypeByTitle")
    @ResponseBody
    public JSONObject getGoodTypeByTitle() {
        return goodTypeService.getGoodTypeByTitle();
    }

    @RequestMapping("deleteGoodType")
    @ResponseBody
    public JSONObject deleteGoodType() {
        return goodTypeService.deleteGoodType();
    }

    @RequestMapping("getGoodTypeOne")
    @ResponseBody
    public JSONObject getGoodTypeOne() {
        return goodTypeService.getGoodTypeOne();
    }

    /**
     * 商家端：修改分类名称
     * @author oy
     * @date 2019/4/11
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("changeGoodTypeById")
    @ResponseBody
    public JSONObject changeGoodTypeById() {
        return goodTypeService.changeGoodTypeById();
    }

    /**
     * 商品类别一二级菜单
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getAllGoodTypeOneTwo")
    @ResponseBody
    public JSONObject getAllGoodTypeOneTwo() {
        return goodTypeService.getAllGoodTypeOneTwo();
    }

    /**
     * 根据ids 和 name 模糊查询
     * @author oy
     * @date 2019/4/29
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodTypesAndNameLike")
    @ResponseBody
    public JSONObject getGoodTypesAndNameLike() {
        return goodTypeService.getGoodTypesAndNameLike();
    }

    /**
     * 根据name 模糊查询
     * @author oy
     * @date 2019/5/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodTypesNameLike")
    @ResponseBody
    public JSONObject getGoodTypesNameLike() {
        return goodTypeService.getGoodTypesNameLike();
    }

    /**
     * 根据name 模糊查询 一级分类
     * @author oy
     * @date 2019/5/20
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodTypesOneNameLike")
    @ResponseBody
    public JSONObject getGoodTypesOneNameLike() {
        return goodTypeService.getGoodTypesOneNameLike();
    }


    /**
     * 首页获取分类 筛选已选
     * @author oy
     * @date 2019/5/1
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getTypeByHomePageGood")
    @ResponseBody
    public JSONObject getTypeByHomePageGood() {
        return goodTypeService.getTypeByHomePageGood();
    }


    /**
     * 验证分类名是否重复
     * @author sqd
     * @date 2019/5/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodTypeisRepetition")
    @ResponseBody
    public JSONObject getGoodTypeisRepetition() {
        return goodTypeService.getGoodTypeisRepetition();
    }

    /**
     * 根据分类id 查找对应的分类信息或者第二级的信息
     * @author oy
     * @date 2019/5/6
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodTypesAndSecondTypeByTypeId")
    @ResponseBody
    public JSONObject getGoodTypesAndSecondTypeByTypeId() {
        return goodTypeService.getGoodTypesAndSecondTypeByTypeId();
    }

    /**
     * 根据商品类别id 去查找对应的二级分类
     * @author oy
     * @date 2019/5/7
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodSecondTypeByTypeId")
    @ResponseBody
    public JSONObject getGoodSecondTypeByTypeId() {
        return goodTypeService.getGoodSecondTypeByTypeId();
    }

    /**
     * 查询商品所有分类
     * @author sqd
     * @date 2019/5/25
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodTypeOneTwoLeave")
    @ResponseBody
    public JSONObject getGoodTypeOneTwoLeave() {
        return goodTypeService.getGoodTypeOneTwoLeave();
    }

    @RequestMapping("getGoodTypeByid")
    @ResponseBody
    public JSONObject getGoodTypeByid() {
        return goodTypeService.getGoodTypeByid();
    }


    /**
     * 通过二级类 查找对应的一级分类
     * @author oy
     * @date 2019/5/27
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getGoodTypeOneByTwoTypeId")
    @ResponseBody
    public JSONObject getGoodTypeOneByTwoTypeId() {
        return goodTypeService.getGoodTypeOneByTwoTypeId();
    }


}
