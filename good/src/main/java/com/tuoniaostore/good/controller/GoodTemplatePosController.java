package com.tuoniaostore.good.controller;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.good.service.GoodTemplatePosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Controller
@RequestMapping("goodTemplatePos")
public class GoodTemplatePosController {
    @Autowired
    private GoodTemplatePosService goodTemplatePosService;


    @RequestMapping("addGoodTemplatePos")
    @ResponseBody
    public JSONObject addGoodTemplatePos() {
        return goodTemplatePosService.addGoodTemplatePos();
    }

    @RequestMapping("getGoodTemplatePos")
    @ResponseBody
    public JSONObject getGoodTemplate() {
        return goodTemplatePosService.getGoodTemplatePos();
    }


    @RequestMapping("getGoodTemplatePoss")
    @ResponseBody
    public JSONObject getGoodTemplatePoss() {
        return goodTemplatePosService.getGoodTemplatePoss();
    }

    @RequestMapping("getGoodTemplatePosAll")
    @ResponseBody
    public JSONObject getGoodTemplatePosAll() {
        return goodTemplatePosService.getGoodTemplatePosAll();
    }

    @RequestMapping("getGoodTemplateCountPos")
    @ResponseBody
    public JSONObject getGoodTemplateCount() {
        return goodTemplatePosService.getGoodTemplatePosCount();
    }

    @RequestMapping("changeGoodTemplatePos")
    @ResponseBody
    public JSONObject changeGoodTemplatePos() {
        return goodTemplatePosService.changeGoodTemplatePos();
    }

    @RequestMapping("getGoodTemplatePosByName")
    @ResponseBody
    public JSONObject getGoodTemplatePosByName() {
        return goodTemplatePosService.getGoodTemplatePosByName();
    }

}
