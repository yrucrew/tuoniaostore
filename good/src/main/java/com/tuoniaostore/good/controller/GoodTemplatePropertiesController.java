package com.tuoniaostore.good.controller;
        import com.tuoniaostore.good.service.GoodTemplatePropertiesService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Controller
@RequestMapping("goodTemplateProperties")
public class GoodTemplatePropertiesController {
    @Autowired
    private GoodTemplatePropertiesService goodTemplatePropertiesService;


    @RequestMapping("addGoodTemplateProperties")
    @ResponseBody
    public JSONObject addGoodTemplateProperties() {
        return goodTemplatePropertiesService.addGoodTemplateProperties();
    }

    @RequestMapping("getGoodTemplateProperties")
    @ResponseBody
    public JSONObject getGoodTemplateProperties() {
        return goodTemplatePropertiesService.getGoodTemplateProperties();
    }


    @RequestMapping("getGoodTemplatePropertiess")
    @ResponseBody
    public JSONObject getGoodTemplatePropertiess() {
        return goodTemplatePropertiesService.getGoodTemplatePropertiess();
    }

    @RequestMapping("getGoodTemplatePropertiesAll")
    @ResponseBody
    public JSONObject getGoodTemplatePropertiesAll() {
        return goodTemplatePropertiesService.getGoodTemplatePropertiesAll();
    }

    @RequestMapping("getGoodTemplatePropertiesCount")
    @ResponseBody
    public JSONObject getGoodTemplatePropertiesCount() {
        return goodTemplatePropertiesService.getGoodTemplatePropertiesCount();
    }

    @RequestMapping("changeGoodTemplateProperties")
    @ResponseBody
    public JSONObject changeGoodTemplateProperties() {
        return goodTemplatePropertiesService.changeGoodTemplateProperties();
    }

}
