package com.tuoniaostore.good.controller;
        import com.tuoniaostore.good.service.GoodImageService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Controller
@RequestMapping("goodImage")
public class GoodImageController {
    @Autowired
    private GoodImageService goodImageService;


    @RequestMapping("addGoodImage")
    @ResponseBody
    public JSONObject addGoodImage() {
        return goodImageService.addGoodImage();
    }

    @RequestMapping("getGoodImage")
    @ResponseBody
    public JSONObject getGoodImage() {
        return goodImageService.getGoodImage();
    }


    @RequestMapping("getGoodImages")
    @ResponseBody
    public JSONObject getGoodImages() {
        return goodImageService.getGoodImages();
    }

    @RequestMapping("getGoodImageAll")
    @ResponseBody
    public JSONObject getGoodImageAll() {
        return goodImageService.getGoodImageAll();
    }

    @RequestMapping("getGoodImageCount")
    @ResponseBody
    public JSONObject getGoodImageCount() {
        return goodImageService.getGoodImageCount();
    }

    @RequestMapping("changeGoodImage")
    @ResponseBody
    public JSONObject changeGoodImage() {
        return goodImageService.changeGoodImage();
    }

}
