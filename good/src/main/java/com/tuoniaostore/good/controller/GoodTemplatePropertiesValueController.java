package com.tuoniaostore.good.controller;
        import com.tuoniaostore.good.service.GoodTemplatePropertiesValueService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Controller
@RequestMapping("goodTemplatePropertiesValue")
public class GoodTemplatePropertiesValueController {
    @Autowired
    private GoodTemplatePropertiesValueService goodTemplatePropertiesValueService;


    @RequestMapping("addGoodTemplatePropertiesValue")
    @ResponseBody
    public JSONObject addGoodTemplatePropertiesValue() {
        return goodTemplatePropertiesValueService.addGoodTemplatePropertiesValue();
    }

    @RequestMapping("getGoodTemplatePropertiesValue")
    @ResponseBody
    public JSONObject getGoodTemplatePropertiesValue() {
        return goodTemplatePropertiesValueService.getGoodTemplatePropertiesValue();
    }


    @RequestMapping("getGoodTemplatePropertiesValues")
    @ResponseBody
    public JSONObject getGoodTemplatePropertiesValues() {
        return goodTemplatePropertiesValueService.getGoodTemplatePropertiesValues();
    }

    @RequestMapping("getGoodTemplatePropertiesValueAll")
    @ResponseBody
    public JSONObject getGoodTemplatePropertiesValueAll() {
        return goodTemplatePropertiesValueService.getGoodTemplatePropertiesValueAll();
    }

    @RequestMapping("getGoodTemplatePropertiesValueCount")
    @ResponseBody
    public JSONObject getGoodTemplatePropertiesValueCount() {
        return goodTemplatePropertiesValueService.getGoodTemplatePropertiesValueCount();
    }

    @RequestMapping("changeGoodTemplatePropertiesValue")
    @ResponseBody
    public JSONObject changeGoodTemplatePropertiesValue() {
        return goodTemplatePropertiesValueService.changeGoodTemplatePropertiesValue();
    }

}
