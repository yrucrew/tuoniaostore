package com.tuoniaostore.good.controller;
        import com.tuoniaostore.good.service.GoodTerminalService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Controller
@RequestMapping("goodTerminal")
public class GoodTerminalController {
    @Autowired
    private GoodTerminalService goodTerminalService;


    @RequestMapping("addGoodTerminal")
    @ResponseBody
    public JSONObject addGoodTerminal() {
        return goodTerminalService.addGoodTerminal();
    }

    @RequestMapping("getGoodTerminal")
    @ResponseBody
    public JSONObject getGoodTerminal() {
        return goodTerminalService.getGoodTerminal();
    }


    @RequestMapping("getGoodTerminals")
    @ResponseBody
    public JSONObject getGoodTerminals() {
        return goodTerminalService.getGoodTerminals();
    }

    @RequestMapping("getGoodTerminalAll")
    @ResponseBody
    public JSONObject getGoodTerminalAll() {
        return goodTerminalService.getGoodTerminalAll();
    }

    @RequestMapping("getGoodTerminalCount")
    @ResponseBody
    public JSONObject getGoodTerminalCount() {
        return goodTerminalService.getGoodTerminalCount();
    }

    @RequestMapping("changeGoodTerminal")
    @ResponseBody
    public JSONObject changeGoodTerminal() {
        return goodTerminalService.changeGoodTerminal();
    }

}
