package com.tuoniaostore.good.controller;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.good.service.GoodPricePosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
@Controller
@RequestMapping("goodPricePos")
public class GoodPricePosController {
    @Autowired
    private GoodPricePosService goodPricePosService;


    @RequestMapping("addGoodPricePos")
    @ResponseBody
    public JSONObject addGoodPricePos() {
        return goodPricePosService.addGoodPricePos();
    }

    @RequestMapping("getGoodPricePos")
    @ResponseBody
    public JSONObject getGoodPricePos() {
        return goodPricePosService.getGoodPricePos();
    }


    @RequestMapping("getGoodPricePoss")
    @ResponseBody
    public JSONObject getGoodPricePoss() {
        return goodPricePosService.getGoodPricePoss();
    }

    @RequestMapping("getGoodPriceAll")
    @ResponseBody
    public JSONObject getGoodPricePosAll() {
        return goodPricePosService.getGoodPricePosAll();
    }

    @RequestMapping("getGoodPricePosCount")
    @ResponseBody
    public JSONObject getGoodPricePosCount() {
        return goodPricePosService.getGoodPricePosCount();
    }

    @RequestMapping("changeGoodPricePos")
    @ResponseBody
    public JSONObject changeGoodPricePos() {
        return goodPricePosService.changeGoodPricePos();
    }

    @RequestMapping("getGoodPricePosByGoodIdAll")
    @ResponseBody
    public JSONObject getGoodPricePosByGoodIdAll() {
        return goodPricePosService.getGoodPricePosByGoodIdAll();
    }

    @RequestMapping("getGoodPricePosByTitle")
    @ResponseBody
    public JSONObject getGoodPricePosByTitle() {
        return goodPricePosService.getGoodPricePosByTitle();
    }

    /**
     * 更新价格表关联id
     * @author oy
     * @date 2019/4/9
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("updateGoodPricePos")
    @ResponseBody
    public JSONObject updateGoodPricePos() {
        return goodPricePosService.updateGoodPricePos();
    }

    /**
     * 获取两个表中的模板 根据商品模板id 和 标签名（唯一）
     * @author oy
     * @date 2019/4/12
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getAllGoodPriceByTitleAndGoodId")
    @ResponseBody
    public JSONObject getAllGoodPriceByTitleAndGoodId() {
        return goodPricePosService.getAllGoodPriceByTitleAndGoodId();
    }


    /**
     * 通过价格id 获取单位 和价格标签
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getAllGoodPricesByPriceIds")
    @ResponseBody
    public JSONObject getAllGoodPricesByPriceIds() {
        return goodPricePosService.getAllGoodPricesByPriceIds();
    }


    /**
     * 根据价格id 获取当前的一些模板信息
     * @author oy
     * @date 2019/4/22
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getAllGoodTemplateByPriceIds")
    @ResponseBody
    public JSONObject getAllGoodTemplateByPriceIds() {
        return goodPricePosService.getAllGoodTemplateByPriceIds();
    }

    /**
     * 根据价格id 获取当前的一些模板信息 根据模板名字 或者 条码 条件查找
     * @author oy
     * @date 2019/4/26
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getAllGoodTemplateByPriceIds2")
    @ResponseBody
    public JSONObject getAllGoodTemplateByPriceIds2() {
        return goodPricePosService.getAllGoodTemplateByPriceIds2();
    }


    /**
     * 获取当前所有的商品规格
     * @author oy
     * @date 2019/5/28
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getAllGoodTemplateAllExtra2")
    @ResponseBody
    public JSONObject getAllGoodTemplateAllExtra2() {
        return goodPricePosService.getAllGoodTemplateAllExtra2();
    }

}
