package com.tuoniaostore.logistics;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.exception.Nearby123shopIllegalArgumentException;
import com.tuoniaostore.commons.exception.Nearby123shopRuntimeException;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by liyunbiao on 2019/3/23.
 */
@Service
@Aspect
public class LogisticsAop  extends BasicWebService {

    private static final Logger logger = LoggerFactory.getLogger(LogisticsAop.class);

    @Around(value = "execution(* com.tuoniaostore.logistics.controller.*.*(..))")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        //拦截的方法参数
        Object[] args = point.getArgs();
        //拦截的实体类
        Object target = point.getTarget();
        //拦截的方法名称
        String methodName = point.getSignature().getName();
        try {
//            int status = SysUserRmoteService.signatureSecurity(getHttpServletRequest());
//            if (1001 == status) {
//                return fail(1001, "登录已过期请重新登录");
//            }
            return point.proceed(args);
        } catch (Nearby123shopIllegalArgumentException ex) {
            logger.error("物流中心切面拦截异常(非法参数)，错误信息：" + ex.getMessage());
            return fail(ex.getMessage());
        } catch (Nearby123shopRuntimeException ex) {
            logger.error("物流中心切面拦截异常(运行时异常)，错误信息：" + ex.getMessage());
            return fail(ex.getCode(), ex.getMessage());
        } catch (Throwable cause) {
            logger.error("物流中心切面拦截异常(未知错误)", cause);
            return fail("系统错误，请稍后重试！");
        }
    }
}