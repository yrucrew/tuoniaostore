package com.tuoniaostore.logistics.cache;
import com.tuoniaostore.datamodel.logistics.LogisticsRelationshipBinding;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
public interface LogisticsRelationshipBindingCache {

    void addLogisticsRelationshipBinding(LogisticsRelationshipBinding logisticsRelationshipBinding);

    LogisticsRelationshipBinding getLogisticsRelationshipBinding(String id);

    List<LogisticsRelationshipBinding> getLogisticsRelationshipBindings(int status, int pageIndex, int pageSize, String name);
    List<LogisticsRelationshipBinding> getLogisticsRelationshipBindingAll();
    int getLogisticsRelationshipBindingCount(int status, String name);
    void changeLogisticsRelationshipBinding(LogisticsRelationshipBinding logisticsRelationshipBinding);

}
