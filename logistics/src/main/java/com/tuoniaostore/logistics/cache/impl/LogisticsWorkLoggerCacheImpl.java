package com.tuoniaostore.logistics.cache.impl;
        import com.tuoniaostore.datamodel.logistics.LogisticsWorkLogger;
        import com.tuoniaostore.logistics.cache.LogisticsWorkLoggerCache;
        import com.tuoniaostore.logistics.data.LogisticsWorkLoggerMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Component
@CacheConfig(cacheNames = "logisticsWorkLogger")
public class LogisticsWorkLoggerCacheImpl implements LogisticsWorkLoggerCache {

    @Autowired
    LogisticsWorkLoggerMapper mapper;

    @Override
    @CacheEvict(value = "logisticsWorkLogger", allEntries = true)
    public void addLogisticsWorkLogger(LogisticsWorkLogger logisticsWorkLogger) {
        mapper.addLogisticsWorkLogger(logisticsWorkLogger);
    }

    @Override
    @Cacheable(value = "logisticsWorkLogger", key = "'getLogisticsWorkLogger_'+#id")
    public   LogisticsWorkLogger getLogisticsWorkLogger(String id) {
        return mapper.getLogisticsWorkLogger(id);
    }

    @Override
    @Cacheable(value = "logisticsWorkLogger", key = "'getLogisticsWorkLoggers_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<LogisticsWorkLogger> getLogisticsWorkLoggers(int status, int pageIndex, int pageSize, String name) {
        return mapper.getLogisticsWorkLoggers(pageIndex, pageSize, status, name);
    }

    @Override
    @Cacheable(value = "logisticsWorkLogger", key = "'getLogisticsWorkLoggerAll'")
    public List<LogisticsWorkLogger> getLogisticsWorkLoggerAll() {
        return mapper.getLogisticsWorkLoggerAll();
    }

    @Override
    @Cacheable(value = "logisticsWorkLogger", key = "'getLogisticsWorkLoggerCount_'+#status+'_'+#name")
    public int getLogisticsWorkLoggerCount(int status, String name) {
        return mapper.getLogisticsWorkLoggerCount(status, name);
    }

    @Override
    @CacheEvict(value = "logisticsWorkLogger", allEntries = true)
    public void changeLogisticsWorkLogger(LogisticsWorkLogger logisticsWorkLogger) {
        mapper.changeLogisticsWorkLogger(logisticsWorkLogger);
    }

}
