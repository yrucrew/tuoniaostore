package com.tuoniaostore.logistics.cache;
import com.tuoniaostore.datamodel.logistics.LogisticsCourierProperties;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
public interface LogisticsCourierPropertiesCache {

    void addLogisticsCourierProperties(LogisticsCourierProperties logisticsCourierProperties);

    LogisticsCourierProperties getLogisticsCourierProperties(String id);

    List<LogisticsCourierProperties> getLogisticsCourierPropertiess(int status, int pageIndex, int pageSize, String name);
    List<LogisticsCourierProperties> getLogisticsCourierPropertiesAll();
    int getLogisticsCourierPropertiesCount(int status, String name);
    void changeLogisticsCourierProperties(LogisticsCourierProperties logisticsCourierProperties);

}
