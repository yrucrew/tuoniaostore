package com.tuoniaostore.logistics.cache.impl;
        import com.tuoniaostore.datamodel.logistics.LogisticsCarType;
        import com.tuoniaostore.logistics.cache.LogisticsCarTypeCache;
        import com.tuoniaostore.logistics.data.LogisticsCarTypeMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Component
@CacheConfig(cacheNames = "logisticsCarType")
public class LogisticsCarTypeCacheImpl implements LogisticsCarTypeCache {

    @Autowired
    LogisticsCarTypeMapper mapper;

    @Override
    @CacheEvict(value = "logisticsCarType", allEntries = true)
    public void addLogisticsCarType(LogisticsCarType logisticsCarType) {
        mapper.addLogisticsCarType(logisticsCarType);
    }

    @Override
    @Cacheable(value = "logisticsCarType", key = "'getLogisticsCarType_'+#id")
    public   LogisticsCarType getLogisticsCarType(String id) {
        return mapper.getLogisticsCarType(id);
    }

    @Override
    @Cacheable(value = "logisticsCarType", key = "'getLogisticsCarTypes_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<LogisticsCarType> getLogisticsCarTypes(int status, int pageIndex, int pageSize, String name) {
        return mapper.getLogisticsCarTypes(pageIndex, pageSize, status, name);
    }

    @Override
    @Cacheable(value = "logisticsCarType", key = "'getLogisticsCarTypeAll'")
    public List<LogisticsCarType> getLogisticsCarTypeAll() {
        return mapper.getLogisticsCarTypeAll();
    }

    @Override
    @Cacheable(value = "logisticsCarType", key = "'getLogisticsCarTypeCount_'+#status+'_'+#name")
    public int getLogisticsCarTypeCount(int status, String name) {
        return mapper.getLogisticsCarTypeCount(status, name);
    }

    @Override
    @CacheEvict(value = "logisticsCarType", allEntries = true)
    public void changeLogisticsCarType(LogisticsCarType logisticsCarType) {
        mapper.changeLogisticsCarType(logisticsCarType);
    }

}
