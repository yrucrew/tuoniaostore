package com.tuoniaostore.logistics.cache;
import com.tuoniaostore.datamodel.logistics.LogisticsCarType;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
public interface LogisticsCarTypeCache {

    void addLogisticsCarType(LogisticsCarType logisticsCarType);

    LogisticsCarType getLogisticsCarType(String id);

    List<LogisticsCarType> getLogisticsCarTypes(int status, int pageIndex, int pageSize, String name);
    List<LogisticsCarType> getLogisticsCarTypeAll();
    int getLogisticsCarTypeCount(int status, String name);
    void changeLogisticsCarType(LogisticsCarType logisticsCarType);

}
