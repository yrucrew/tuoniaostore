package com.tuoniaostore.logistics.cache.impl;
        import com.tuoniaostore.datamodel.logistics.LogisticsRelationshipBinding;
        import com.tuoniaostore.logistics.cache.LogisticsRelationshipBindingCache;
        import com.tuoniaostore.logistics.data.LogisticsRelationshipBindingMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Component
@CacheConfig(cacheNames = "logisticsRelationshipBinding")
public class LogisticsRelationshipBindingCacheImpl implements LogisticsRelationshipBindingCache {

    @Autowired
    LogisticsRelationshipBindingMapper mapper;

    @Override
    @CacheEvict(value = "logisticsRelationshipBinding", allEntries = true)
    public void addLogisticsRelationshipBinding(LogisticsRelationshipBinding logisticsRelationshipBinding) {
        mapper.addLogisticsRelationshipBinding(logisticsRelationshipBinding);
    }

    @Override
    @Cacheable(value = "logisticsRelationshipBinding", key = "'getLogisticsRelationshipBinding_'+#id")
    public   LogisticsRelationshipBinding getLogisticsRelationshipBinding(String id) {
        return mapper.getLogisticsRelationshipBinding(id);
    }

    @Override
    @Cacheable(value = "logisticsRelationshipBinding", key = "'getLogisticsRelationshipBindings_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<LogisticsRelationshipBinding> getLogisticsRelationshipBindings(int status, int pageIndex, int pageSize, String name) {
        return mapper.getLogisticsRelationshipBindings(pageIndex, pageSize, status, name);
    }

    @Override
    @Cacheable(value = "logisticsRelationshipBinding", key = "'getLogisticsRelationshipBindingAll'")
    public List<LogisticsRelationshipBinding> getLogisticsRelationshipBindingAll() {
        return mapper.getLogisticsRelationshipBindingAll();
    }

    @Override
    @Cacheable(value = "logisticsRelationshipBinding", key = "'getLogisticsRelationshipBindingCount_'+#status+'_'+#name")
    public int getLogisticsRelationshipBindingCount(int status, String name) {
        return mapper.getLogisticsRelationshipBindingCount(status, name);
    }

    @Override
    @CacheEvict(value = "logisticsRelationshipBinding", allEntries = true)
    public void changeLogisticsRelationshipBinding(LogisticsRelationshipBinding logisticsRelationshipBinding) {
        mapper.changeLogisticsRelationshipBinding(logisticsRelationshipBinding);
    }

}
