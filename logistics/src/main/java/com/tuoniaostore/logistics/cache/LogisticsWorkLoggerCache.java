package com.tuoniaostore.logistics.cache;
import com.tuoniaostore.datamodel.logistics.LogisticsWorkLogger;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
public interface LogisticsWorkLoggerCache {

    void addLogisticsWorkLogger(LogisticsWorkLogger logisticsWorkLogger);

    LogisticsWorkLogger getLogisticsWorkLogger(String id);

    List<LogisticsWorkLogger> getLogisticsWorkLoggers(int status, int pageIndex, int pageSize, String name);
    List<LogisticsWorkLogger> getLogisticsWorkLoggerAll();
    int getLogisticsWorkLoggerCount(int status, String name);
    void changeLogisticsWorkLogger(LogisticsWorkLogger logisticsWorkLogger);

}
