package com.tuoniaostore.logistics.cache.impl;
        import com.tuoniaostore.datamodel.logistics.LogisticsLocationLogger;
        import com.tuoniaostore.logistics.cache.LogisticsLocationLoggerCache;
        import com.tuoniaostore.logistics.data.LogisticsLocationLoggerMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Component
@CacheConfig(cacheNames = "logisticsLocationLogger")
public class LogisticsLocationLoggerCacheImpl implements LogisticsLocationLoggerCache {

    @Autowired
    LogisticsLocationLoggerMapper mapper;

    @Override
    @CacheEvict(value = "logisticsLocationLogger", allEntries = true)
    public void addLogisticsLocationLogger(LogisticsLocationLogger logisticsLocationLogger) {
        mapper.addLogisticsLocationLogger(logisticsLocationLogger);
    }

    @Override
    @Cacheable(value = "logisticsLocationLogger", key = "'getLogisticsLocationLogger_'+#id")
    public   LogisticsLocationLogger getLogisticsLocationLogger(String id) {
        return mapper.getLogisticsLocationLogger(id);
    }

    @Override
    @Cacheable(value = "logisticsLocationLogger", key = "'getLogisticsLocationLoggers_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<LogisticsLocationLogger> getLogisticsLocationLoggers(int status, int pageIndex, int pageSize, String name) {
        return mapper.getLogisticsLocationLoggers(pageIndex, pageSize, status, name);
    }

    @Override
    @Cacheable(value = "logisticsLocationLogger", key = "'getLogisticsLocationLoggerAll'")
    public List<LogisticsLocationLogger> getLogisticsLocationLoggerAll() {
        return mapper.getLogisticsLocationLoggerAll();
    }

    @Override
    @Cacheable(value = "logisticsLocationLogger", key = "'getLogisticsLocationLoggerCount_'+#status+'_'+#name")
    public int getLogisticsLocationLoggerCount(int status, String name) {
        return mapper.getLogisticsLocationLoggerCount(status, name);
    }

    @Override
    @CacheEvict(value = "logisticsLocationLogger", allEntries = true)
    public void changeLogisticsLocationLogger(LogisticsLocationLogger logisticsLocationLogger) {
        mapper.changeLogisticsLocationLogger(logisticsLocationLogger);
    }

}
