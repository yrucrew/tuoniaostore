package com.tuoniaostore.logistics.cache;

import com.tuoniaostore.datamodel.logistics.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Component
public class LogisticsDataAccessManager {


    @Autowired
    LogisticsCarTypeCache logisticsCarTypeCache;

    public void addLogisticsCarType(LogisticsCarType logisticsCarType) {
        logisticsCarTypeCache.addLogisticsCarType(logisticsCarType);
    }

    public LogisticsCarType getLogisticsCarType(String id) {
        return logisticsCarTypeCache.getLogisticsCarType(id);
    }

    public List<LogisticsCarType> getLogisticsCarTypes(int status, int pageIndex, int pageSize, String name) {
        return logisticsCarTypeCache.getLogisticsCarTypes(status, pageIndex, pageSize, name);
    }

    public List<LogisticsCarType> getLogisticsCarTypeAll() {
        return logisticsCarTypeCache.getLogisticsCarTypeAll();
    }

    public int getLogisticsCarTypeCount(int status, String name) {
        return logisticsCarTypeCache.getLogisticsCarTypeCount(status, name);
    }

    public void changeLogisticsCarType(LogisticsCarType logisticsCarType) {
        logisticsCarTypeCache.changeLogisticsCarType(logisticsCarType);
    }
    @Autowired
    LogisticsWorkLoggerCache logisticsWorkLoggerCache;

    public void addLogisticsWorkLogger(LogisticsWorkLogger logisticsWorkLogger) {
        logisticsWorkLoggerCache.addLogisticsWorkLogger(logisticsWorkLogger);
    }

    public   LogisticsWorkLogger getLogisticsWorkLogger(String id) {
        return logisticsWorkLoggerCache.getLogisticsWorkLogger(id);
    }

    public List<LogisticsWorkLogger> getLogisticsWorkLoggers(int status,int pageIndex, int pageSize, String name) {
        return logisticsWorkLoggerCache.getLogisticsWorkLoggers(status,pageIndex, pageSize, name);
    }

    public List<LogisticsWorkLogger> getLogisticsWorkLoggerAll() {
        return logisticsWorkLoggerCache.getLogisticsWorkLoggerAll();
    }

    public int getLogisticsWorkLoggerCount(int status, String name) {
        return logisticsWorkLoggerCache.getLogisticsWorkLoggerCount(status, name);
    }

    public void changeLogisticsWorkLogger(LogisticsWorkLogger logisticsWorkLogger) {
        logisticsWorkLoggerCache.changeLogisticsWorkLogger(logisticsWorkLogger);
    }
    @Autowired
    LogisticsCourierPropertiesCache logisticsCourierPropertiesCache;

    public void addLogisticsCourierProperties(LogisticsCourierProperties logisticsCourierProperties) {
        logisticsCourierPropertiesCache.addLogisticsCourierProperties(logisticsCourierProperties);
    }

    public   LogisticsCourierProperties getLogisticsCourierProperties(String id) {
        return logisticsCourierPropertiesCache.getLogisticsCourierProperties(id);
    }

    public List<LogisticsCourierProperties> getLogisticsCourierPropertiess(int status,int pageIndex, int pageSize, String name) {
        return logisticsCourierPropertiesCache.getLogisticsCourierPropertiess(status,pageIndex, pageSize, name);
    }

    public List<LogisticsCourierProperties> getLogisticsCourierPropertiesAll() {
        return logisticsCourierPropertiesCache.getLogisticsCourierPropertiesAll();
    }

    public int getLogisticsCourierPropertiesCount(int status, String name) {
        return logisticsCourierPropertiesCache.getLogisticsCourierPropertiesCount(status, name);
    }

    public void changeLogisticsCourierProperties(LogisticsCourierProperties logisticsCourierProperties) {
        logisticsCourierPropertiesCache.changeLogisticsCourierProperties(logisticsCourierProperties);
    }
    @Autowired
    LogisticsLocationLoggerCache logisticsLocationLoggerCache;

    public void addLogisticsLocationLogger(LogisticsLocationLogger logisticsLocationLogger) {
        logisticsLocationLoggerCache.addLogisticsLocationLogger(logisticsLocationLogger);
    }

    public   LogisticsLocationLogger getLogisticsLocationLogger(String id) {
        return logisticsLocationLoggerCache.getLogisticsLocationLogger(id);
    }

    public List<LogisticsLocationLogger> getLogisticsLocationLoggers(int status,int pageIndex, int pageSize, String name) {
        return logisticsLocationLoggerCache.getLogisticsLocationLoggers(status,pageIndex, pageSize, name);
    }

    public List<LogisticsLocationLogger> getLogisticsLocationLoggerAll() {
        return logisticsLocationLoggerCache.getLogisticsLocationLoggerAll();
    }

    public int getLogisticsLocationLoggerCount(int status, String name) {
        return logisticsLocationLoggerCache.getLogisticsLocationLoggerCount(status, name);
    }

    public void changeLogisticsLocationLogger(LogisticsLocationLogger logisticsLocationLogger) {
        logisticsLocationLoggerCache.changeLogisticsLocationLogger(logisticsLocationLogger);
    }
    @Autowired
    LogisticsRelationshipBindingCache logisticsRelationshipBindingCache;

    public void addLogisticsRelationshipBinding(LogisticsRelationshipBinding logisticsRelationshipBinding) {
        logisticsRelationshipBindingCache.addLogisticsRelationshipBinding(logisticsRelationshipBinding);
    }

    public   LogisticsRelationshipBinding getLogisticsRelationshipBinding(String id) {
        return logisticsRelationshipBindingCache.getLogisticsRelationshipBinding(id);
    }

    public List<LogisticsRelationshipBinding> getLogisticsRelationshipBindings(int status,int pageIndex, int pageSize, String name) {
        return logisticsRelationshipBindingCache.getLogisticsRelationshipBindings(status,pageIndex, pageSize, name);
    }

    public List<LogisticsRelationshipBinding> getLogisticsRelationshipBindingAll() {
        return logisticsRelationshipBindingCache.getLogisticsRelationshipBindingAll();
    }

    public int getLogisticsRelationshipBindingCount(int status, String name) {
        return logisticsRelationshipBindingCache.getLogisticsRelationshipBindingCount(status, name);
    }

    public void changeLogisticsRelationshipBinding(LogisticsRelationshipBinding logisticsRelationshipBinding) {
        logisticsRelationshipBindingCache.changeLogisticsRelationshipBinding(logisticsRelationshipBinding);
    }

}
