package com.tuoniaostore.logistics.cache.impl;
        import com.tuoniaostore.datamodel.logistics.LogisticsCourierProperties;
        import com.tuoniaostore.logistics.cache.LogisticsCourierPropertiesCache;
        import com.tuoniaostore.logistics.data.LogisticsCourierPropertiesMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Component
@CacheConfig(cacheNames = "logisticsCourierProperties")
public class LogisticsCourierPropertiesCacheImpl implements LogisticsCourierPropertiesCache {

    @Autowired
    LogisticsCourierPropertiesMapper mapper;

    @Override
    @CacheEvict(value = "logisticsCourierProperties", allEntries = true)
    public void addLogisticsCourierProperties(LogisticsCourierProperties logisticsCourierProperties) {
        mapper.addLogisticsCourierProperties(logisticsCourierProperties);
    }

    @Override
    @Cacheable(value = "logisticsCourierProperties", key = "'getLogisticsCourierProperties_'+#id")
    public   LogisticsCourierProperties getLogisticsCourierProperties(String id) {
        return mapper.getLogisticsCourierProperties(id);
    }

    @Override
    @Cacheable(value = "logisticsCourierProperties", key = "'getLogisticsCourierPropertiess_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<LogisticsCourierProperties> getLogisticsCourierPropertiess(int status, int pageIndex, int pageSize, String name) {
        return mapper.getLogisticsCourierPropertiess(pageIndex, pageSize, status, name);
    }

    @Override
    @Cacheable(value = "logisticsCourierProperties", key = "'getLogisticsCourierPropertiesAll'")
    public List<LogisticsCourierProperties> getLogisticsCourierPropertiesAll() {
        return mapper.getLogisticsCourierPropertiesAll();
    }

    @Override
    @Cacheable(value = "logisticsCourierProperties", key = "'getLogisticsCourierPropertiesCount_'+#status+'_'+#name")
    public int getLogisticsCourierPropertiesCount(int status, String name) {
        return mapper.getLogisticsCourierPropertiesCount(status, name);
    }

    @Override
    @CacheEvict(value = "logisticsCourierProperties", allEntries = true)
    public void changeLogisticsCourierProperties(LogisticsCourierProperties logisticsCourierProperties) {
        mapper.changeLogisticsCourierProperties(logisticsCourierProperties);
    }

}
