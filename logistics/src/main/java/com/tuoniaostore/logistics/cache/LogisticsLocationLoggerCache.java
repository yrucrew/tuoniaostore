package com.tuoniaostore.logistics.cache;
import com.tuoniaostore.datamodel.logistics.LogisticsLocationLogger;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
public interface LogisticsLocationLoggerCache {

    void addLogisticsLocationLogger(LogisticsLocationLogger logisticsLocationLogger);

    LogisticsLocationLogger getLogisticsLocationLogger(String id);

    List<LogisticsLocationLogger> getLogisticsLocationLoggers(int status, int pageIndex, int pageSize, String name);
    List<LogisticsLocationLogger> getLogisticsLocationLoggerAll();
    int getLogisticsLocationLoggerCount(int status, String name);
    void changeLogisticsLocationLogger(LogisticsLocationLogger logisticsLocationLogger);

}
