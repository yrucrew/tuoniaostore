package com.tuoniaostore.logistics.controller;
        import com.tuoniaostore.logistics.service.LogisticsCarTypeService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Controller
@RequestMapping("logisticsCarType")
public class LogisticsCarTypeController {
    @Autowired
    private LogisticsCarTypeService logisticsCarTypeService;


    @RequestMapping("addLogisticsCarType")
    @ResponseBody
    public JSONObject addLogisticsCarType() {
        return logisticsCarTypeService.addLogisticsCarType();
    }

    @RequestMapping("getLogisticsCarType")
    @ResponseBody
    public JSONObject getLogisticsCarType() {
        return logisticsCarTypeService.getLogisticsCarType();
    }


    @RequestMapping("getLogisticsCarTypes")
    @ResponseBody
    public JSONObject getLogisticsCarTypes() {
        return logisticsCarTypeService.getLogisticsCarTypes();
    }

    @RequestMapping("getLogisticsCarTypeAll")
    @ResponseBody
    public JSONObject getLogisticsCarTypeAll() {
        return logisticsCarTypeService.getLogisticsCarTypeAll();
    }

    @RequestMapping("getLogisticsCarTypeCount")
    @ResponseBody
    public JSONObject getLogisticsCarTypeCount() {
        return logisticsCarTypeService.getLogisticsCarTypeCount();
    }

    @RequestMapping("changeLogisticsCarType")
    @ResponseBody
    public JSONObject changeLogisticsCarType() {
        return logisticsCarTypeService.changeLogisticsCarType();
    }

}
