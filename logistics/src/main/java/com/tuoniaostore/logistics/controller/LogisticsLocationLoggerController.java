package com.tuoniaostore.logistics.controller;
        import com.tuoniaostore.logistics.service.LogisticsLocationLoggerService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Controller
@RequestMapping("logisticsLocationLogger")
public class LogisticsLocationLoggerController {
    @Autowired
    private LogisticsLocationLoggerService logisticsLocationLoggerService;


    @RequestMapping("addLogisticsLocationLogger")
    @ResponseBody
    public JSONObject addLogisticsLocationLogger() {
        return logisticsLocationLoggerService.addLogisticsLocationLogger();
    }

    @RequestMapping("getLogisticsLocationLogger")
    @ResponseBody
    public JSONObject getLogisticsLocationLogger() {
        return logisticsLocationLoggerService.getLogisticsLocationLogger();
    }


    @RequestMapping("getLogisticsLocationLoggers")
    @ResponseBody
    public JSONObject getLogisticsLocationLoggers() {
        return logisticsLocationLoggerService.getLogisticsLocationLoggers();
    }

    @RequestMapping("getLogisticsLocationLoggerAll")
    @ResponseBody
    public JSONObject getLogisticsLocationLoggerAll() {
        return logisticsLocationLoggerService.getLogisticsLocationLoggerAll();
    }

    @RequestMapping("getLogisticsLocationLoggerCount")
    @ResponseBody
    public JSONObject getLogisticsLocationLoggerCount() {
        return logisticsLocationLoggerService.getLogisticsLocationLoggerCount();
    }

    @RequestMapping("changeLogisticsLocationLogger")
    @ResponseBody
    public JSONObject changeLogisticsLocationLogger() {
        return logisticsLocationLoggerService.changeLogisticsLocationLogger();
    }

}
