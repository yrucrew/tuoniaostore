package com.tuoniaostore.logistics.controller;
        import com.tuoniaostore.logistics.service.LogisticsWorkLoggerService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Controller
@RequestMapping("logisticsWorkLogger")
public class LogisticsWorkLoggerController {
    @Autowired
    private LogisticsWorkLoggerService logisticsWorkLoggerService;


    @RequestMapping("addLogisticsWorkLogger")
    @ResponseBody
    public JSONObject addLogisticsWorkLogger() {
        return logisticsWorkLoggerService.addLogisticsWorkLogger();
    }

    @RequestMapping("getLogisticsWorkLogger")
    @ResponseBody
    public JSONObject getLogisticsWorkLogger() {
        return logisticsWorkLoggerService.getLogisticsWorkLogger();
    }


    @RequestMapping("getLogisticsWorkLoggers")
    @ResponseBody
    public JSONObject getLogisticsWorkLoggers() {
        return logisticsWorkLoggerService.getLogisticsWorkLoggers();
    }

    @RequestMapping("getLogisticsWorkLoggerAll")
    @ResponseBody
    public JSONObject getLogisticsWorkLoggerAll() {
        return logisticsWorkLoggerService.getLogisticsWorkLoggerAll();
    }

    @RequestMapping("getLogisticsWorkLoggerCount")
    @ResponseBody
    public JSONObject getLogisticsWorkLoggerCount() {
        return logisticsWorkLoggerService.getLogisticsWorkLoggerCount();
    }

    @RequestMapping("changeLogisticsWorkLogger")
    @ResponseBody
    public JSONObject changeLogisticsWorkLogger() {
        return logisticsWorkLoggerService.changeLogisticsWorkLogger();
    }

}
