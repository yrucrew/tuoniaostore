package com.tuoniaostore.logistics.controller;
        import com.tuoniaostore.logistics.service.LogisticsRelationshipBindingService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Controller
@RequestMapping("logisticsRelationshipBinding")
public class LogisticsRelationshipBindingController {
    @Autowired
    private LogisticsRelationshipBindingService logisticsRelationshipBindingService;


    @RequestMapping("addLogisticsRelationshipBinding")
    @ResponseBody
    public JSONObject addLogisticsRelationshipBinding() {
        return logisticsRelationshipBindingService.addLogisticsRelationshipBinding();
    }

    @RequestMapping("getLogisticsRelationshipBinding")
    @ResponseBody
    public JSONObject getLogisticsRelationshipBinding() {
        return logisticsRelationshipBindingService.getLogisticsRelationshipBinding();
    }


    @RequestMapping("getLogisticsRelationshipBindings")
    @ResponseBody
    public JSONObject getLogisticsRelationshipBindings() {
        return logisticsRelationshipBindingService.getLogisticsRelationshipBindings();
    }

    @RequestMapping("getLogisticsRelationshipBindingAll")
    @ResponseBody
    public JSONObject getLogisticsRelationshipBindingAll() {
        return logisticsRelationshipBindingService.getLogisticsRelationshipBindingAll();
    }

    @RequestMapping("getLogisticsRelationshipBindingCount")
    @ResponseBody
    public JSONObject getLogisticsRelationshipBindingCount() {
        return logisticsRelationshipBindingService.getLogisticsRelationshipBindingCount();
    }

    @RequestMapping("changeLogisticsRelationshipBinding")
    @ResponseBody
    public JSONObject changeLogisticsRelationshipBinding() {
        return logisticsRelationshipBindingService.changeLogisticsRelationshipBinding();
    }

}
