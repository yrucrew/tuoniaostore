package com.tuoniaostore.logistics.controller;
        import com.tuoniaostore.logistics.service.LogisticsCourierPropertiesService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Controller
@RequestMapping("logisticsCourierProperties")
public class LogisticsCourierPropertiesController {
    @Autowired
    private LogisticsCourierPropertiesService logisticsCourierPropertiesService;


    @RequestMapping("addLogisticsCourierProperties")
    @ResponseBody
    public JSONObject addLogisticsCourierProperties() {
        return logisticsCourierPropertiesService.addLogisticsCourierProperties();
    }

    @RequestMapping("getLogisticsCourierProperties")
    @ResponseBody
    public JSONObject getLogisticsCourierProperties() {
        return logisticsCourierPropertiesService.getLogisticsCourierProperties();
    }


    @RequestMapping("getLogisticsCourierPropertiess")
    @ResponseBody
    public JSONObject getLogisticsCourierPropertiess() {
        return logisticsCourierPropertiesService.getLogisticsCourierPropertiess();
    }

    @RequestMapping("getLogisticsCourierPropertiesAll")
    @ResponseBody
    public JSONObject getLogisticsCourierPropertiesAll() {
        return logisticsCourierPropertiesService.getLogisticsCourierPropertiesAll();
    }

    @RequestMapping("getLogisticsCourierPropertiesCount")
    @ResponseBody
    public JSONObject getLogisticsCourierPropertiesCount() {
        return logisticsCourierPropertiesService.getLogisticsCourierPropertiesCount();
    }

    @RequestMapping("changeLogisticsCourierProperties")
    @ResponseBody
    public JSONObject changeLogisticsCourierProperties() {
        return logisticsCourierPropertiesService.changeLogisticsCourierProperties();
    }

}
