package com.tuoniaostore.logistics.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.logistics.LogisticsWorkLogger;
import com.tuoniaostore.logistics.cache.LogisticsDataAccessManager;
import com.tuoniaostore.logistics.service.LogisticsWorkLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("logisticsWorkLoggerService")
public class LogisticsWorkLoggerServiceImpl extends BasicWebService implements LogisticsWorkLoggerService {
    @Autowired
    private LogisticsDataAccessManager dataAccessManager;

    @Override
    public JSONObject addLogisticsWorkLogger() {
        LogisticsWorkLogger logisticsWorkLogger = new LogisticsWorkLogger();
        //用户通行证ID
        String userId = getUTF("userId");
        //打卡类型
        Integer clockType = getIntParameter("clockType", 0);
        //备注
        String remark = getUTF("remark", null);
        //打卡位置
        String location = getUTF("location", null);
        //打卡地址
        String address = getUTF("address", null);
        //与正常上班地址的距离，单位：米
        Long clockDistance = getLongParameter("clockDistance", 0);
        logisticsWorkLogger.setUserId(userId);
        logisticsWorkLogger.setClockType(clockType);
        logisticsWorkLogger.setRemark(remark);
        logisticsWorkLogger.setLocation(location);
        logisticsWorkLogger.setAddress(address);
        logisticsWorkLogger.setClockDistance(clockDistance);
        logisticsWorkLogger.setClockTime(new Date());
        dataAccessManager.addLogisticsWorkLogger(logisticsWorkLogger);
        return success();
    }

    @Override
    public JSONObject getLogisticsWorkLogger() {
        LogisticsWorkLogger logisticsWorkLogger = dataAccessManager.getLogisticsWorkLogger(getId());
        return success(logisticsWorkLogger);
    }

    @Override
    public JSONObject getLogisticsWorkLoggers() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<LogisticsWorkLogger> logisticsWorkLoggers = dataAccessManager.getLogisticsWorkLoggers(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getLogisticsWorkLoggerCount(status, name);
        return success(logisticsWorkLoggers, count);
    }

    @Override
    public JSONObject getLogisticsWorkLoggerAll() {
        List<LogisticsWorkLogger> logisticsWorkLoggers = dataAccessManager.getLogisticsWorkLoggerAll();
        return success(logisticsWorkLoggers);
    }

    @Override
    public JSONObject getLogisticsWorkLoggerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getLogisticsWorkLoggerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeLogisticsWorkLogger() {
        LogisticsWorkLogger logisticsWorkLogger = dataAccessManager.getLogisticsWorkLogger(getId());
        //用户通行证ID
        String userId = getUTF("userId");
        //打卡类型
        Integer clockType = getIntParameter("clockType", 0);
        //备注
        String remark = getUTF("remark", null);
        //打卡位置
        String location = getUTF("location", null);
        //打卡地址
        String address = getUTF("address", null);
        //与正常上班地址的距离，单位：米
        Long clockDistance = getLongParameter("clockDistance", 0);
        logisticsWorkLogger.setUserId(userId);
        logisticsWorkLogger.setClockType(clockType);
        logisticsWorkLogger.setRemark(remark);
        logisticsWorkLogger.setLocation(location);
        logisticsWorkLogger.setAddress(address);
        logisticsWorkLogger.setClockDistance(clockDistance);
        logisticsWorkLogger.setClockTime(new Date());
        dataAccessManager.changeLogisticsWorkLogger(logisticsWorkLogger);
        return success();
    }

}
