package com.tuoniaostore.logistics.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.logistics.LogisticsRelationshipBinding;
import com.tuoniaostore.logistics.cache.LogisticsDataAccessManager;
import com.tuoniaostore.logistics.service.LogisticsRelationshipBindingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("logisticsRelationshipBindingService")
public class LogisticsRelationshipBindingServiceImpl extends BasicWebService implements LogisticsRelationshipBindingService {
    @Autowired
    private LogisticsDataAccessManager dataAccessManager;

    @Override
    public JSONObject addLogisticsRelationshipBinding() {
        LogisticsRelationshipBinding logisticsRelationshipBinding = new LogisticsRelationshipBinding();
        //关系主人方标识
        String hostUserId = getUTF("hostUserId");
        //关系客人方标识
        String guestUserId = getUTF("guestUserId");
        //关系类型 逻辑设定
        Integer type = getIntParameter("type", 0);
        //关系状态 0无效 1有效 其他值由逻辑决定
        Integer status = getIntParameter("status", 0);
        logisticsRelationshipBinding.setHostUserId(hostUserId);
        logisticsRelationshipBinding.setGuestUserId(guestUserId);
        logisticsRelationshipBinding.setType(type);
        logisticsRelationshipBinding.setStatus(status);
        dataAccessManager.addLogisticsRelationshipBinding(logisticsRelationshipBinding);
        return success();
    }

    @Override
    public JSONObject getLogisticsRelationshipBinding() {
        LogisticsRelationshipBinding logisticsRelationshipBinding = dataAccessManager.getLogisticsRelationshipBinding(getId());
        return success(logisticsRelationshipBinding);
    }

    @Override
    public JSONObject getLogisticsRelationshipBindings() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<LogisticsRelationshipBinding> logisticsRelationshipBindings = dataAccessManager.getLogisticsRelationshipBindings(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getLogisticsRelationshipBindingCount(status, name);
        return success(logisticsRelationshipBindings, count);
    }

    @Override
    public JSONObject getLogisticsRelationshipBindingAll() {
        List<LogisticsRelationshipBinding> logisticsRelationshipBindings = dataAccessManager.getLogisticsRelationshipBindingAll();
        return success(logisticsRelationshipBindings);
    }

    @Override
    public JSONObject getLogisticsRelationshipBindingCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getLogisticsRelationshipBindingCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeLogisticsRelationshipBinding() {
        LogisticsRelationshipBinding logisticsRelationshipBinding = dataAccessManager.getLogisticsRelationshipBinding(getId());
        //关系主人方标识
        String hostUserId = getUTF("hostUserId");
        //关系客人方标识
        String guestUserId = getUTF("guestUserId");
        //关系类型 逻辑设定
        Integer type = getIntParameter("type", 0);
        //关系状态 0无效 1有效 其他值由逻辑决定
        Integer status = getIntParameter("status", 0);
        logisticsRelationshipBinding.setHostUserId(hostUserId);
        logisticsRelationshipBinding.setGuestUserId(guestUserId);
        logisticsRelationshipBinding.setType(type);
        logisticsRelationshipBinding.setStatus(status);
        dataAccessManager.changeLogisticsRelationshipBinding(logisticsRelationshipBinding);
        return success();
    }

}
