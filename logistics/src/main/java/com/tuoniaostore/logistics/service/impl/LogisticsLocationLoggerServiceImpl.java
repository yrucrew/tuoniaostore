package com.tuoniaostore.logistics.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.logistics.LogisticsLocationLogger;
import com.tuoniaostore.logistics.cache.LogisticsDataAccessManager;
import com.tuoniaostore.logistics.service.LogisticsLocationLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("logisticsLocationLoggerService")
public class LogisticsLocationLoggerServiceImpl extends BasicWebService implements LogisticsLocationLoggerService {
    @Autowired
    private LogisticsDataAccessManager dataAccessManager;

    @Override
    public JSONObject addLogisticsLocationLogger() {
        LogisticsLocationLogger logisticsLocationLogger = new LogisticsLocationLogger();
        //
        Integer actionType = getIntParameter("actionType", 0);
        //
        Double latitude = getDoubleParameter("latitude", 0);
        //
        Double longitude = getDoubleParameter("longitude", 0);
        //
        String userId = getUTF("userId");
        logisticsLocationLogger.setActionType(actionType);
        logisticsLocationLogger.setLatitude(latitude);
        logisticsLocationLogger.setLongitude(longitude);
        logisticsLocationLogger.setUserId(userId);
        dataAccessManager.addLogisticsLocationLogger(logisticsLocationLogger);
        return success();
    }

    @Override
    public JSONObject getLogisticsLocationLogger() {
        LogisticsLocationLogger logisticsLocationLogger = dataAccessManager.getLogisticsLocationLogger(getId());
        return success(logisticsLocationLogger);
    }

    @Override
    public JSONObject getLogisticsLocationLoggers() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<LogisticsLocationLogger> logisticsLocationLoggers = dataAccessManager.getLogisticsLocationLoggers(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getLogisticsLocationLoggerCount(status, name);
        return success(logisticsLocationLoggers, count);
    }

    @Override
    public JSONObject getLogisticsLocationLoggerAll() {
        List<LogisticsLocationLogger> logisticsLocationLoggers = dataAccessManager.getLogisticsLocationLoggerAll();
        return success(logisticsLocationLoggers);
    }

    @Override
    public JSONObject getLogisticsLocationLoggerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getLogisticsLocationLoggerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeLogisticsLocationLogger() {
        LogisticsLocationLogger logisticsLocationLogger = dataAccessManager.getLogisticsLocationLogger(getId());
        //
        Integer actionType = getIntParameter("actionType", 0);
        //
        Double latitude = getDoubleParameter("latitude", 0);
        //
        Double longitude = getDoubleParameter("longitude", 0);
        //
        String userId = getUTF("userId");
        logisticsLocationLogger.setActionType(actionType);
        logisticsLocationLogger.setLatitude(latitude);
        logisticsLocationLogger.setLongitude(longitude);
        logisticsLocationLogger.setUserId(userId);
        dataAccessManager.changeLogisticsLocationLogger(logisticsLocationLogger);
        return success();
    }

}
