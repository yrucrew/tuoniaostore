package com.tuoniaostore.logistics.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.logistics.LogisticsCourierProperties;
import com.tuoniaostore.logistics.cache.LogisticsDataAccessManager;
import com.tuoniaostore.logistics.service.LogisticsCourierPropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("logisticsCourierPropertiesService")
public class LogisticsCourierPropertiesServiceImpl extends BasicWebService implements LogisticsCourierPropertiesService {
    @Autowired
    private LogisticsDataAccessManager dataAccessManager;

    @Override
    public JSONObject addLogisticsCourierProperties() {
        LogisticsCourierProperties logisticsCourierProperties = new LogisticsCourierProperties();
        //归属角色ID
        String userId = getUTF("userId");
        //工作类型 0：全职 1：兼职
        Integer workType = getIntParameter("workType", 0);
        //工作地址中心位置经纬度，格式：longitude,latitude
        String workLocation = getUTF("workLocation", null);
        //车型 -1时表示步行
        Integer carType = getIntParameter("carType", 0);
        //上班时间
        String timeToWork = getUTF("timeToWork", null);
        //下班时间
        String timeOffWork = getUTF("timeOffWork", null);
        //当处于上班时间时 是否接受订单推送 0时不接受
        Integer acceptStatus = getIntParameter("acceptStatus", 0);
        //最后所在位置
        String lastLocation = getUTF("lastLocation", null);
        //配送范围 单位：米
        Integer deliveryRange = getIntParameter("deliveryRange", 0);
        logisticsCourierProperties.setUserId(userId);
        logisticsCourierProperties.setWorkType(workType);
        logisticsCourierProperties.setWorkLocation(workLocation);
        logisticsCourierProperties.setCarType(carType);
        logisticsCourierProperties.setTimeToWork(new Date());
        logisticsCourierProperties.setAcceptStatus(acceptStatus);
        logisticsCourierProperties.setLastLocation(lastLocation);
        logisticsCourierProperties.setDeliveryRange(deliveryRange);
        dataAccessManager.addLogisticsCourierProperties(logisticsCourierProperties);
        return success();
    }

    @Override
    public JSONObject getLogisticsCourierProperties() {
        LogisticsCourierProperties logisticsCourierProperties = dataAccessManager.getLogisticsCourierProperties(getId());
        return success(logisticsCourierProperties);
    }

    @Override
    public JSONObject getLogisticsCourierPropertiess() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<LogisticsCourierProperties> logisticsCourierPropertiess = dataAccessManager.getLogisticsCourierPropertiess(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getLogisticsCourierPropertiesCount(status, name);
        return success(logisticsCourierPropertiess, count);
    }

    @Override
    public JSONObject getLogisticsCourierPropertiesAll() {
        List<LogisticsCourierProperties> logisticsCourierPropertiess = dataAccessManager.getLogisticsCourierPropertiesAll();
        return success(logisticsCourierPropertiess);
    }

    @Override
    public JSONObject getLogisticsCourierPropertiesCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getLogisticsCourierPropertiesCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeLogisticsCourierProperties() {
        LogisticsCourierProperties logisticsCourierProperties = dataAccessManager.getLogisticsCourierProperties(getId());
        //归属角色ID
        String userId = getUTF("userId");
        //工作类型 0：全职 1：兼职
        Integer workType = getIntParameter("workType", 0);
        //工作地址中心位置经纬度，格式：longitude,latitude
        String workLocation = getUTF("workLocation", null);
        //车型 -1时表示步行
        Integer carType = getIntParameter("carType", 0);
        //当处于上班时间时 是否接受订单推送 0时不接受
        Integer acceptStatus = getIntParameter("acceptStatus", 0);
        //最后所在位置
        String lastLocation = getUTF("lastLocation", null);
        //配送范围 单位：米
        Integer deliveryRange = getIntParameter("deliveryRange", 0);
        logisticsCourierProperties.setUserId(userId);
        logisticsCourierProperties.setWorkType(workType);
        logisticsCourierProperties.setWorkLocation(workLocation);
        logisticsCourierProperties.setCarType(carType);
        logisticsCourierProperties.setTimeOffWork(new Date());
        logisticsCourierProperties.setAcceptStatus(acceptStatus);
        logisticsCourierProperties.setLastLocation(lastLocation);
        logisticsCourierProperties.setDeliveryRange(deliveryRange);
        dataAccessManager.changeLogisticsCourierProperties(logisticsCourierProperties);
        return success();
    }

}
