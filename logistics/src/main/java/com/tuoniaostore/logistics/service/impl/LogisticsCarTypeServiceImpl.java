package com.tuoniaostore.logistics.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.logistics.LogisticsCarType;
import com.tuoniaostore.logistics.cache.LogisticsDataAccessManager;
import com.tuoniaostore.logistics.service.LogisticsCarTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("logisticsCarTypeService")
public class LogisticsCarTypeServiceImpl extends BasicWebService implements LogisticsCarTypeService {
    @Autowired
    private LogisticsDataAccessManager dataAccessManager;

    @Override
    public JSONObject addLogisticsCarType() {
        LogisticsCarType logisticsCarType = new LogisticsCarType();
        //车型类型值 0表示单车
        Integer type = getIntParameter("type", 0);
        //标题(用于展示)
        String title = getUTF("title", null);
        //图片链接
        String image = getUTF("image", null);
        //简介
        String introduction = getUTF("introduction", null);
        logisticsCarType.setType(type);
        logisticsCarType.setTitle(title);
        logisticsCarType.setImage(image);
        logisticsCarType.setIntroduction(introduction);
        dataAccessManager.addLogisticsCarType(logisticsCarType);
        return success();
    }

    @Override
    public JSONObject getLogisticsCarType() {
        LogisticsCarType logisticsCarType = dataAccessManager.getLogisticsCarType(getId());
        return success(logisticsCarType);
    }

    @Override
    public JSONObject getLogisticsCarTypes() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<LogisticsCarType> logisticsCarTypes = dataAccessManager.getLogisticsCarTypes(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getLogisticsCarTypeCount(status, name);
        return success(logisticsCarTypes, count);
    }

    @Override
    public JSONObject getLogisticsCarTypeAll() {
        List<LogisticsCarType> logisticsCarTypes = dataAccessManager.getLogisticsCarTypeAll();
        return success(logisticsCarTypes);
    }

    @Override
    public JSONObject getLogisticsCarTypeCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getLogisticsCarTypeCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeLogisticsCarType() {
        LogisticsCarType logisticsCarType = dataAccessManager.getLogisticsCarType(getId());
        //车型类型值 0表示单车
        Integer type = getIntParameter("type", 0);
        //标题(用于展示)
        String title = getUTF("title", null);
        //图片链接
        String image = getUTF("image", null);
        //简介
        String introduction = getUTF("introduction", null);
        logisticsCarType.setType(type);
        logisticsCarType.setTitle(title);
        logisticsCarType.setImage(image);
        logisticsCarType.setIntroduction(introduction);
        dataAccessManager.changeLogisticsCarType(logisticsCarType);
        return success();
    }

}
