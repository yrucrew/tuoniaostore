package com.tuoniaostore.logistics.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
public interface LogisticsWorkLoggerService {

    JSONObject addLogisticsWorkLogger();

    JSONObject getLogisticsWorkLogger();

    JSONObject getLogisticsWorkLoggers();

    JSONObject getLogisticsWorkLoggerCount();
    JSONObject getLogisticsWorkLoggerAll();

    JSONObject changeLogisticsWorkLogger();
}
