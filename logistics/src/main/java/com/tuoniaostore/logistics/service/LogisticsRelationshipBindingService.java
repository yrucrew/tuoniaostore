package com.tuoniaostore.logistics.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
public interface LogisticsRelationshipBindingService {

    JSONObject addLogisticsRelationshipBinding();

    JSONObject getLogisticsRelationshipBinding();

    JSONObject getLogisticsRelationshipBindings();

    JSONObject getLogisticsRelationshipBindingCount();
    JSONObject getLogisticsRelationshipBindingAll();

    JSONObject changeLogisticsRelationshipBinding();
}
