package com.tuoniaostore.logistics.data;

import com.tuoniaostore.datamodel.logistics.LogisticsLocationLogger;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Component
@Mapper
public interface LogisticsLocationLoggerMapper {

    String column = "`action_type`, " +//
            "`create_time`, " +//
            "`latitude`, " +//
            "`longitude`, " +//
            "`user_id`"//
            ;

    @Insert("insert into logistics_location_logger (" +
            " `id`,  " +
            " `action_type`,  " +
            " `latitude`,  " +
            " `longitude`,  " +
            " `user_id` " +
            ")values(" +
            "#{id},  " +
            "#{actionType},  " +
            "#{latitude},  " +
            "#{longitude},  " +
            "#{userId} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addLogisticsLocationLogger(LogisticsLocationLogger logisticsLocationLogger);

    @Results(id = "logisticsLocationLogger", value = {
            @Result(property = "id", column = "id"), @Result(property = "actionType", column = "action_type"),
            @Result(property = "createTime", column = "create_time"), @Result(property = "latitude", column = "latitude"),
            @Result(property = "longitude", column = "longitude"), @Result(property = "userId", column = "user_id")})
    @Select("select " + column + " from logistics_location_logger where id = #{id}")
    LogisticsLocationLogger getLogisticsLocationLogger(@Param("id") String id);

    @Select("<script>select  " + column + "  from logistics_location_logger   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("logisticsLocationLogger")
    List<LogisticsLocationLogger> getLogisticsLocationLoggers(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from logistics_location_logger   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("logisticsLocationLogger")
    List<LogisticsLocationLogger> getLogisticsLocationLoggerAll();

    @Select("<script>select count(1) from logistics_location_logger   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getLogisticsLocationLoggerCount(@Param("status") int status, @Param("name") String name);

    @Update("update logistics_location_logger  " +
            "set " +
            "`action_type` = #{actionType}  , " +
            "`create_time` = #{createTime}  , " +
            "`latitude` = #{latitude}  , " +
            "`longitude` = #{longitude}  , " +
            "`user_id` = #{userId}  " +
            " where id = #{id}")
    void changeLogisticsLocationLogger(LogisticsLocationLogger logisticsLocationLogger);

}
