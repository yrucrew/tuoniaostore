package com.tuoniaostore.logistics.data;

import com.tuoniaostore.datamodel.logistics.LogisticsCarType;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Component
@Mapper
public interface LogisticsCarTypeMapper {

    String column = "`type`, " +//车型类型值 0表示单车
            "`title`, " +//标题(用于展示)
            "`image`, " +//图片链接
            "`introduction`"//简介
            ;

    @Insert("insert into logistics_car_type (" +
            " `id`,  " +
            " `type`,  " +
            " `title`,  " +
            " `image`,  " +
            " `introduction` " +
            ")values(" +
            "#{id},  " +
            "#{type},  " +
            "#{title},  " +
            "#{image},  " +
            "#{introduction} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addLogisticsCarType(LogisticsCarType logisticsCarType);

    @Results(id = "logisticsCarType", value = {
            @Result(property = "id", column = "id"), @Result(property = "type", column = "type"), @Result(property = "title", column = "title"), @Result(property = "image", column = "image"), @Result(property = "introduction", column = "introduction")})
    @Select("select " + column + " from logistics_car_type where id = #{id}")
    LogisticsCarType getLogisticsCarType(@Param("id") String id);

    @Select("<script>select  " + column + "  from logistics_car_type   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("logisticsCarType")
    List<LogisticsCarType> getLogisticsCarTypes(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from logistics_car_type   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("logisticsCarType")
    List<LogisticsCarType> getLogisticsCarTypeAll();

    @Select("<script>select count(1) from logistics_car_type   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getLogisticsCarTypeCount(@Param("status") int status, @Param("name") String name);

    @Update("update logistics_car_type  " +
            "set " +
            "`type` = #{type}  , " +
            "`title` = #{title}  , " +
            "`image` = #{image}  , " +
            "`introduction` = #{introduction}  " +
            " where id = #{id}")
    void changeLogisticsCarType(LogisticsCarType logisticsCarType);

}
