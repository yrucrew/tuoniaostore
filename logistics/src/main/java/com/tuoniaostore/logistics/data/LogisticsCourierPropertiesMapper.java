package com.tuoniaostore.logistics.data;

import com.tuoniaostore.datamodel.logistics.LogisticsCourierProperties;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Component
@Mapper
public interface LogisticsCourierPropertiesMapper {

    String column = "`user_id`, " +//归属角色ID
            "`work_type`, " +//工作类型 0：全职 1：兼职
            "`work_location`, " +//工作地址中心位置经纬度，格式：longitude,latitude
            "`car_type`, " +//车型 -1时表示步行
            "`time_to_work`, " +//上班时间
            "`time_off_work`, " +//下班时间
            "`accept_status`, " +//当处于上班时间时 是否接受订单推送 0时不接受
            "`last_location`, " +//最后所在位置
            "`delivery_range`, " +//配送范围 单位：米
            "`create_time`"//建立时间
            ;

    @Insert("insert into logistics_courier_properties (" +
            " `id`,  " +
            " `user_id`,  " +
            " `work_type`,  " +
            " `work_location`,  " +
            " `car_type`,  " +
            " `time_to_work`,  " +
            " `time_off_work`,  " +
            " `accept_status`,  " +
            " `last_location`,  " +
            " `delivery_range`,  " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{workType},  " +
            "#{workLocation},  " +
            "#{carType},  " +
            "#{timeToWork},  " +
            "#{timeOffWork},  " +
            "#{acceptStatus},  " +
            "#{lastLocation},  " +
            "#{deliveryRange},  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addLogisticsCourierProperties(LogisticsCourierProperties logisticsCourierProperties);

    @Results(id = "logisticsCourierProperties", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "workType", column = "work_type"), @Result(property = "workLocation", column = "work_location"), @Result(property = "carType", column = "car_type"), @Result(property = "timeToWork", column = "time_to_work"), @Result(property = "timeOffWork", column = "time_off_work"), @Result(property = "acceptStatus", column = "accept_status"), @Result(property = "lastLocation", column = "last_location"), @Result(property = "deliveryRange", column = "delivery_range"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from logistics_courier_properties where id = #{id}")
    LogisticsCourierProperties getLogisticsCourierProperties(@Param("id") String id);

    @Select("<script>select  " + column + "  from logistics_courier_properties   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("logisticsCourierProperties")
    List<LogisticsCourierProperties> getLogisticsCourierPropertiess(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from logistics_courier_properties   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("logisticsCourierProperties")
    List<LogisticsCourierProperties> getLogisticsCourierPropertiesAll();

    @Select("<script>select count(1) from logistics_courier_properties   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getLogisticsCourierPropertiesCount(@Param("status") int status, @Param("name") String name);

    @Update("update logistics_courier_properties  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`work_type` = #{workType}  , " +
            "`work_location` = #{workLocation}  , " +
            "`car_type` = #{carType}  , " +
            "`time_to_work` = #{timeToWork}  , " +
            "`time_off_work` = #{timeOffWork}  , " +
            "`accept_status` = #{acceptStatus}  , " +
            "`last_location` = #{lastLocation}  , " +
            "`delivery_range` = #{deliveryRange}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeLogisticsCourierProperties(LogisticsCourierProperties logisticsCourierProperties);

}
