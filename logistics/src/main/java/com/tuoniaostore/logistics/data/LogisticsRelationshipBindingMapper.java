package com.tuoniaostore.logistics.data;

import com.tuoniaostore.datamodel.logistics.LogisticsRelationshipBinding;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Component
@Mapper
public interface LogisticsRelationshipBindingMapper {

    String column = "`host_user_id`, " +//关系主人方标识
            "`guest_user_id`, " +//关系客人方标识
            "`type`, " +//关系类型 逻辑设定
            "`status`, " +//关系状态 0无效 1有效 其他值由逻辑决定
            "`create_time`"//建立时间
            ;

    @Insert("insert into logistics_relationship_binding (" +
            " `id`,  " +
            " `host_user_id`,  " +
            " `guest_user_id`,  " +
            " `type`,  " +
            " `status`,  " +
            ")values(" +
            "#{id},  " +
            "#{hostUserId},  " +
            "#{guestUserId},  " +
            "#{type},  " +
            "#{status},  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addLogisticsRelationshipBinding(LogisticsRelationshipBinding logisticsRelationshipBinding);

    @Results(id = "logisticsRelationshipBinding", value = {
            @Result(property = "id", column = "id"), @Result(property = "hostUserId", column = "host_user_id"),
            @Result(property = "guestUserId", column = "guest_user_id"),
            @Result(property = "type", column = "type"), @Result(property = "status", column = "status"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from logistics_relationship_binding where id = #{id}")
    LogisticsRelationshipBinding getLogisticsRelationshipBinding(@Param("id") String id);

    @Select("<script>select  " + column + "  from logistics_relationship_binding   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("logisticsRelationshipBinding")
    List<LogisticsRelationshipBinding> getLogisticsRelationshipBindings(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from logistics_relationship_binding   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("logisticsRelationshipBinding")
    List<LogisticsRelationshipBinding> getLogisticsRelationshipBindingAll();

    @Select("<script>select count(1) from logistics_relationship_binding   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('', #{name}, '%') " +
            " </when></script>")
    int getLogisticsRelationshipBindingCount(@Param("status") int status, @Param("name") String name);

    @Update("update logistics_relationship_binding  " +
            "set " +
            "`host_user_id` = #{hostUserId}  , " +
            "`guest_user_id` = #{guestUserId}  , " +
            "`type` = #{type}  , " +
            "`status` = #{status}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeLogisticsRelationshipBinding(LogisticsRelationshipBinding logisticsRelationshipBinding);

}
