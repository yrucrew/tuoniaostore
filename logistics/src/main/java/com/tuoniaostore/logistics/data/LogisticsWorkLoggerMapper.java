package com.tuoniaostore.logistics.data;

import com.tuoniaostore.datamodel.logistics.LogisticsWorkLogger;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
@Component
@Mapper
public interface LogisticsWorkLoggerMapper {

    String column = "`user_id`, " +//用户通行证ID
            "`clock_type`, " +//打卡类型
            "`remark`, " +//备注
            "`location`, " +//打卡位置
            "`address`, " +//打卡地址
            "`clock_distance`, " +//与正常上班地址的距离，单位：米
            "`clock_time`"//打卡时间
            ;

    @Insert("insert into logistics_work_logger (" +
            " `id`,  " +
            " `user_id`,  " +
            " `clock_type`,  " +
            " `remark`,  " +
            " `location`,  " +
            " `address`,  " +
            " `clock_distance`,  " +
            " `clock_time` " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{clockType},  " +
            "#{remark},  " +
            "#{location},  " +
            "#{address},  " +
            "#{clockDistance},  " +
            "#{clockTime} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addLogisticsWorkLogger(LogisticsWorkLogger logisticsWorkLogger);

    @Results(id = "logisticsWorkLogger", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "clockType", column = "clock_type"),
            @Result(property = "remark", column = "remark"),
            @Result(property = "location", column = "location"), @Result(property = "address", column = "address"), @Result(property = "clockDistance", column = "clock_distance"), @Result(property = "clockTime", column = "clock_time")})
    @Select("select " + column + " from logistics_work_logger where id = #{id}")
    LogisticsWorkLogger getLogisticsWorkLogger(@Param("id") String id);

    @Select("<script>select  " + column + "  from logistics_work_logger   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("logisticsWorkLogger")
    List<LogisticsWorkLogger> getLogisticsWorkLoggers(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from logistics_work_logger   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("logisticsWorkLogger")
    List<LogisticsWorkLogger> getLogisticsWorkLoggerAll();

    @Select("<script>select count(1) from logistics_work_logger   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getLogisticsWorkLoggerCount(@Param("status") int status, @Param("name") String name);

    @Update("update logistics_work_logger  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`clock_type` = #{clockType}  , " +
            "`remark` = #{remark}  , " +
            "`location` = #{location}  , " +
            "`address` = #{address}  , " +
            "`clock_distance` = #{clockDistance}  , " +
            "`clock_time` = #{clockTime}  " +
            " where id = #{id}")
    void changeLogisticsWorkLogger(LogisticsWorkLogger logisticsWorkLogger);

}
