package com.tuoniaostore.payment.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.EnvironmentEnum;
import com.tuoniaostore.commons.constant.MessageStatus;
import com.tuoniaostore.commons.constant.MessageType;
import com.tuoniaostore.commons.constant.pay.WeiXinTerminalPayTypeEnum;
import com.tuoniaostore.commons.constant.pay.WeixinTemplateIdConstant;
import com.tuoniaostore.commons.constant.payment.CurrencyTypeEnum;
import com.tuoniaostore.commons.constant.payment.PayTypeEnum;
import com.tuoniaostore.commons.constant.payment.TransStatusEnum;
import com.tuoniaostore.commons.constant.payment.TransTypeEnum;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserSmsRemoteService;
import com.tuoniaostore.commons.support.order.OrderEntryRemoteService;
import com.tuoniaostore.commons.support.order.OrderGoodSnapshotRemoteService;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.commons.utils.MessageUtils;
import com.tuoniaostore.commons.utils.WeiXinPayUtils;
import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.payment.*;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.user.SysUserSmsLogger;
import com.tuoniaostore.datamodel.vo.order.PayLogNumbersVo;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.redis.RedisService;
import com.tuoniaostore.payment.service.PaymentTransactionLoggerService;
import com.tuoniaostore.payment.vo.PayLogVo;
import com.tuoniaostore.payment.vo.PayLogVo2;
import com.tuoniaostore.payment.vo.PaymentTransactionLoggerVO;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


@Service("paymentTransactionLoggerService")
public class PaymentTransactionLoggerServiceImpl extends BasicWebService implements PaymentTransactionLoggerService {

    private static final Logger logger = LoggerFactory.getLogger(PaymentTransactionLoggerServiceImpl.class);

    @Autowired
    private PaymentDataAccessManager dataAccessManager;

    @Autowired
    private RedisService redisService;

    @Override
    public JSONObject addPaymentTransactionLogger() {
        PaymentTransactionLogger paymentTransactionLogger = new PaymentTransactionLogger();
        //发生交易的通行证ID
        String userId = getUTF("userId");
        //我方生成的交易序列号
        String transSequenceNumber = getUTF("transSequenceNumber", null);
        //交易状态 使用二进制方式计算 如：客户端成功时为4 回调成功时为8 双方成功后为12 默认1
        Integer transStatus = getIntParameter("transStatus", 1);
        //支付类型 0-余额 1-微信 2-支付宝 3-银联等 如有其他交易方式 由逻辑决定
        String paymentType = getUTF("paymentType", null);
        //交易类型 1-支付 2-充值 3-代付 4-提现
        Integer transType = getIntParameter("transType", 0);
        //我方为合作商分配的商家服务号
        String partnerId = getUTF("partnerId", null);
        //商户的appId
        String appId = getUTF("appId", null);
        //合作商家的用户标志
        String partnerUserId = getUTF("partnerUserId", null);
        //合作商家的交易序号
        String partnerTradeNumber = getUTF("partnerTradeNumber", null);
        //我方为上游合作商分配的渠道号 0-官方渠道 其他值由逻辑决定
        Integer channelId = getIntParameter("channelId", 0);
        //渠道方的交易序列号
        String channelTradeNumber = getUTF("channelTradeNumber", null);
        //渠道方的用户ID(唯一标志)
        String channelUserId = getUTF("channelUserId", null);
        //渠道方用户名
        String channelUserName = getUTF("channelUserName", null);
        //渠道方的备注信息
        String channelRemark = getUTF("channelRemark", null);
        //收款帐号(仅提现时有效)
        String accountNumber = getUTF("accountNumber", null);
        //收款账户名
        String accountName = getUTF("accountName", null);
        //账户类型，如：信用卡、储值卡
        Integer accountType = getIntParameter("accountType", 0);
        //交易货币类型，如RMB
        String currencyType = getUTF("currencyType", null);
        //银行名
        String bankName = getUTF("bankName", null);
        //银行编码，如支行编码
        String bankBranchCode = getUTF("bankBranchCode", null);
        //银行简称代码，如：ICBC
        String bankSimpleName = getUTF("bankSimpleName", null);
        //交易单价 单位：分
        Long transUnitAmount = getLongParameter("transUnitAmount", 0);
        //交易数量
        Integer transNumber = getIntParameter("transNumber", 0);
        //交易总价 单位：分 这里未必等于单价*数量 因为可能存在优惠
        Long transTotalAmount = getLongParameter("transTotalAmount", 0);
        //交易标题
        String transTitle = getUTF("transTitle", null);
        //备注内容
        String remark = getUTF("remark", null);
        //交易时间
        String transCreateTime = getUTF("transCreateTime", null);
        //支付时间
        String paymentTime = getUTF("paymentTime", null);
        //是否使用优惠券 0-未使用 1-使用
        Integer useConpon = getIntParameter("useConpon", 0);
        //优惠额度 单位：分
        Long discountAmount = getLongParameter("discountAmount", 0);
        //退款状态 0-未退款
        Integer refundStatus = getIntParameter("refundStatus", 0);
        //退款时间
        String refundTime = getUTF("refundTime", null);
        //处理完请求后，当前页面跳转到指定页面的路径
        String notifyFrontUrl = getUTF("notifyFrontUrl", null);
        //回调接口
        String notifyUrl = getUTF("notifyUrl", null);
        //其他参数
        String extendParameter = getUTF("extendParameter", null);
        //操作时间
        String extendTime = getUTF("extendTime", null);
        paymentTransactionLogger.setUserId(userId);
        paymentTransactionLogger.setTransSequenceNumber(transSequenceNumber);
        paymentTransactionLogger.setTransStatus(transStatus);
        paymentTransactionLogger.setPaymentType(paymentType);
        paymentTransactionLogger.setTransType(transType);
        paymentTransactionLogger.setPartnerId(partnerId);
        paymentTransactionLogger.setAppId(appId);
        paymentTransactionLogger.setPartnerUserId(partnerUserId);
        paymentTransactionLogger.setPartnerTradeNumber(partnerTradeNumber);
        paymentTransactionLogger.setChannelId(channelId);
        paymentTransactionLogger.setChannelTradeNumber(channelTradeNumber);
        paymentTransactionLogger.setChannelUserId(channelUserId);
        paymentTransactionLogger.setChannelUserName(channelUserName);
        paymentTransactionLogger.setChannelRemark(channelRemark);
        paymentTransactionLogger.setAccountNumber(accountNumber);
        paymentTransactionLogger.setAccountName(accountName);
        paymentTransactionLogger.setAccountType(accountType);
        paymentTransactionLogger.setCurrencyType(currencyType);
        paymentTransactionLogger.setBankName(bankName);
        paymentTransactionLogger.setBankBranchCode(bankBranchCode);
        paymentTransactionLogger.setBankSimpleName(bankSimpleName);
        paymentTransactionLogger.setTransUnitAmount(transUnitAmount);
        paymentTransactionLogger.setTransNumber(transNumber);
        paymentTransactionLogger.setTransTotalAmount(transTotalAmount);
        paymentTransactionLogger.setTransTitle(transTitle);
        paymentTransactionLogger.setRemark(remark);
//        paymentTransactionLogger.setTransCreateTime(transCreateTime);
//        paymentTransactionLogger.setPaymentTime(paymentTime);
        paymentTransactionLogger.setUseConpon(useConpon);
        paymentTransactionLogger.setDiscountAmount(discountAmount);
        paymentTransactionLogger.setRefundStatus(refundStatus);
//        paymentTransactionLogger.setRefundTime(refundTime);
        paymentTransactionLogger.setNotifyFrontUrl(notifyFrontUrl);
        paymentTransactionLogger.setNotifyUrl(notifyUrl);
        paymentTransactionLogger.setExtendParameter(extendParameter);
//        paymentTransactionLogger.setExtendTime(extendTime);
        dataAccessManager.addPaymentTransactionLogger(paymentTransactionLogger);
        return success();
    }

    @Override
    public JSONObject addPaymentTransactionLoggerJson() {
        String str = getUTF("paymentTransactionLogger");
        PaymentTransactionLogger paymentTransactionLogger = JSONObject.parseObject(str, PaymentTransactionLogger.class);
        dataAccessManager.addPaymentTransactionLogger(paymentTransactionLogger);
        return success();
    }
    @Override
    public JSONObject getPaymentTransactionLogger() {
        PaymentTransactionLogger paymentTransactionLogger = dataAccessManager.getPaymentTransactionLogger(getId());
        return success(paymentTransactionLogger);
    }

    @Override
    public JSONObject getPaymentTransactionLoggers() {
        HashMap<String, Object> parameters = new HashMap<>();
        // 交易状态
        parameters.put("transStatus", getUTF("transStatus", null));
        // 交易类型
        String[] transType = getUTF("transType").split(",");
        if (ArrayUtils.isEmpty(transType)) {
            return fail("交易类型不能为空");
        }
        parameters.put("transType", transType);
        // 搜索金额范围
        double transTotalAmountLow = getDoubleParameter("transTotalAmountLow", 0);
        double transTotalAmountHigh = getDoubleParameter("transTotalAmountHigh", 0);
        if (transTotalAmountLow > 0 && transTotalAmountHigh > 0 &&  transTotalAmountLow > transTotalAmountHigh) {
            return fail("金额范围的最大值不能小于最小值");
        }
        if (transTotalAmountLow<0||transTotalAmountHigh<0){
            return fail("金额不能小于0");
        }
        parameters.put("transTotalAmountLow",transTotalAmountLow);
        parameters.put("transTotalAmountHigh",transTotalAmountHigh);
        // 搜索时间
        // 申请时间
        String createTimeBeg = getUTF("createTimeBeg", null);
        String createTimeEnd = getUTF("createTimeEnd", null);
        if (!StringUtils.isEmpty(createTimeBeg)&&!StringUtils.isEmpty(createTimeEnd)){
            parameters.put("createTimeBeg", DateUtils.parse(createTimeBeg, DateUtils.FORMAT_SHORT));
            parameters.put("createTimeEnd", DateUtils.addDay(DateUtils.parse(createTimeEnd, DateUtils.FORMAT_SHORT),1));
        }
        // 处理时间
        String paymentTimeBeg = getUTF("paymentTimeBeg",null);
        String paymentTimeEnd = getUTF("paymentTimeEnd",null);
        if (!StringUtils.isEmpty(paymentTimeBeg)&&!StringUtils.isEmpty(paymentTimeEnd)){
            parameters.put("paymentTimeBeg", DateUtils.parse(paymentTimeBeg, DateUtils.FORMAT_SHORT));
            parameters.put("paymentTimeEnd", DateUtils.addDay(DateUtils.parse(paymentTimeEnd, DateUtils.FORMAT_SHORT),1));
        }

        // 完成时间
        String transCreateTimeBeg = getUTF("transCreateTimeBeg", null);
        String transCreateTimeEnd = getUTF("transCreateTimeEnd", null);
        if (!StringUtils.isEmpty(transCreateTimeBeg)&&!StringUtils.isEmpty(transCreateTimeEnd)){
            parameters.put("transCreateTimeBeg", DateUtils.parse(transCreateTimeBeg, DateUtils.FORMAT_SHORT));
            parameters.put("transCreateTimeEnd", DateUtils.addDay(DateUtils.parse(transCreateTimeEnd, DateUtils.FORMAT_SHORT),1));
        }

        // 商户名称
        String name = getUTF("name", null);
        if (!StringUtils.isEmpty(name)) {
            List<SysUser> sysUserList = SysUserRemoteService.getSysUserByName(name, getHttpServletRequest());
            if (CollectionUtils.isEmpty(sysUserList)){
                return success(new ArrayList(), 0);
            }
            List<String> userIds = sysUserList.stream().map(SysUser::getId).collect(Collectors.toList());
            //parameters.put("userIds", userIds);
        }

        String phoneNum = getUTF("phoneNum", null);
        //根据号码 查找对应的id
        List<String> userIds = null;
        if(phoneNum != null && !phoneNum.equals("")){
            List<SysUser> phoneNum1 = SysUserRemoteService.getSysUserByPhoneNumLike(phoneNum, getHttpServletRequest());
            if(phoneNum1 != null && phoneNum1.size() > 0){
                userIds = phoneNum1.stream().map(SysUser::getId).collect(Collectors.toList());
            }
            parameters.put("userIds", userIds);
        }

        List<PaymentTransactionLogger> paymentTransactionLoggers = dataAccessManager.getPaymentTransactionLoggers(getPageStartIndex(getPageSize()), getPageSize(), parameters);
        int size = paymentTransactionLoggers.size();
        for (int i = 0; i < size; i++) {
            try {
                SysUser sysUser = SysUserRemoteService.getSysUser(paymentTransactionLoggers.get(i).getUserId(), getHttpServletRequest());
                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        paymentTransactionLoggers.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        paymentTransactionLoggers.get(i).setUserName(sysUser.getShowName());
                    } else {
                        paymentTransactionLoggers.get(i).setUserName(sysUser.getDefaultName());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int count = dataAccessManager.getPaymentTransactionLoggerCount(parameters);
        return success(paymentTransactionLoggers, count);
    }

    @Override
    public JSONObject getPaymentTransactionLoggerAll() {
        List<PaymentTransactionLogger> paymentTransactionLoggers = dataAccessManager.getPaymentTransactionLoggerAll();
        return success(paymentTransactionLoggers);
    }

    @Override
    public JSONObject getPaymentTransactionLoggerCount() {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("keyWord", getUTF("keyWord", null));
        int count = dataAccessManager.getPaymentTransactionLoggerCount(parameters);
        return success(count);
    }

    @Override
    public JSONObject changePaymentTransactionLogger() {
        String[] id=getId().split(",");
        dataAccessManager.changePaymentTransactionLogger(id);
        return success();
    }


    /**
     * 获取提现记录
     *
     * @return
     */
    @Override
    public JSONObject getWithdrawalPaymentTransactionLogger() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userId = user.getId();

        int pageNum = getIntParameter("pageNum", 1);//页码
        int pageSize = getIntParameter("pageSize", 10);//页面大小

        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageNum - 1);
        int limitEnd = pageSize;

        //通过用户id 查找对应的提现记录 需要提供页码、每页大小
        List<PaymentTransactionLogger> list = dataAccessManager.getWithdrawalPaymentTransactionLogger(userId, TransTypeEnum.DRAW_CASH.getKey(), limitStart, limitEnd);
        //封装返回结果 PaymentCurrencyOffsetLoggerVO
        List<PaymentTransactionLoggerVO> list2 = new ArrayList<>();
        if (list != null && list.size() > 0) {
            list.forEach(x -> {
                PaymentTransactionLoggerVO vo = new PaymentTransactionLoggerVO();
                vo.setBankName(x.getBankName());
                vo.setOperationTime(x.getPaymentTime());
                String value = TransStatusEnum.getValue(x.getTransStatus());
                vo.setStatus(value);
                //处理一下账号 给出尾号
                String accountNumber = x.getAccountNumber();
                char[] chars = accountNumber.toCharArray();
                int length = chars.length;
                int i = length - 4;
                String substring = accountNumber.substring(i);
                vo.setAccountNumber(substring);//切割最后4位
                vo.setTransTotalAmount(x.getTransTotalAmount());
                list2.add(vo);
            });
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list", list2);
        return success(jsonObject);
    }

    /**
     * 提现 短信验证码
     *
     * @return
     */
    @Override
    public JSONObject getMSGPaymentTransactionLogger() {
        //获取手机号码
        String phoneNum = getUTF("phoneNum");
        if (phoneNum == null || phoneNum.equals("")) {
            return fail("手机号码不能为空！");
        }

        //发送验证码
        SysUserSmsLogger sysUserSmsLogger = new SysUserSmsLogger();
        String code = "";
        try {
            code = MessageUtils.sendMessage(phoneNum);
            sysUserSmsLogger.setStatus(MessageStatus.MESSAGE_STATUS_SUCCESS.getKey());
        } catch (Exception e) {
            sysUserSmsLogger.setStatus(MessageStatus.MESSAGE_STATUS_FAILD.getKey());
            e.printStackTrace();
        } finally {
            sysUserSmsLogger.setContent("提现验证码:" + code);
            sysUserSmsLogger.setPhone(phoneNum);
            sysUserSmsLogger.setCode(code);
            sysUserSmsLogger.setType(MessageType.MESSAGE_TYPE_WITHDRAWAL.getKey());
            //调用community服务 加入短信记录
            try {
                SysUserSmsRemoteService.addSysUserSmsLogger(sysUserSmsLogger, getHttpServletRequest());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return success("验证码发送成功！");
    }

    @Autowired
    ApplicationContext applicationContext;

    /**
     * 提现操作
     *
     * @return
     */
    @Override
    @Transactional
    public JSONObject withdrawalPaymentTransactionLogger() {
        Environment environment = applicationContext.getEnvironment();
        int environmentEnum = 0;
        String[] activeProfiles = environment.getActiveProfiles();
        if(activeProfiles != null && activeProfiles.length > 0){
            String activeProfile = activeProfiles[0];
            logger.info("环境:{}", activeProfile);
            environmentEnum = EnvironmentEnum.getKey(activeProfile);
        }
        //获取结算卡的id
        String cardId = getUTF("cardId");//结算卡
        if (StringUtils.isEmpty(cardId)) {
            return fail("结算卡不能为空!");
        }

        //获取提现金额
        long money = getLongParameter("money");//前端传递过来是分 记得改成分
        if (money <= 0) {
            return fail("金额必须大于0.0!");
        }

        // 获取到账时间规则id 提现规则
        String ruleId = getUTF("ruleId");
        if (StringUtils.isEmpty(ruleId)) {
            return fail("提现规则不能为空!");
        }
        PaymentTakeRule paymentTakeRule = dataAccessManager.getPaymentTakeRule(ruleId);
        if (paymentTakeRule == null) {
            return fail("提现规则不存在!");
        }


        //获取手机号码
        String phoneNum = getUTF("phoneNum");//手机号码
        String validCode = getUTF("validCode");//验证码

        SysUser user = null;
        try {
            // Token获取user
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }

        SysUser sysUser = null;
        try {
            // 根据Token获取的UserId在数据库中获取user
            sysUser = SysUserRemoteService.getSysUser(user.getId(), getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 验证当前用户余额是否有这么多钱 可以提现
        PaymentCurrencyAccount paymentCurrencyAccountByUserId = dataAccessManager.getPaymentCurrencyAccountByUserId(sysUser.getId(), CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());
        Long currentAmount = paymentCurrencyAccountByUserId.getCurrentAmount();//可用额度
        if (money > currentAmount) {
            return fail("账户余额不够，不能提现！");
        }


        if (validCode == null || validCode.equals("")) {
            return fail("验证码不能为空!");
        }
        //获取验证码是否正确
        //验证当前号码 和 接受到的验证码是否一样
        try {
            SysUserSmsLogger sysUserSmsLogger = SysUserSmsRemoteService.getSysUserSmsLogger(phoneNum, MessageType.MESSAGE_TYPE_WITHDRAWAL.getKey(), getHttpServletRequest());//获取到短信记录
            if (sysUserSmsLogger != null) {
                String code1 = sysUserSmsLogger.getCode();
                if (code1 != null && !code1.equals("")) {
                    if (code1.equals(validCode)) {//说明验证码正确
                        //根据结算卡的账号id 和 当前用户id 获取结算卡的账号
                        PaymentBank paymentBank = dataAccessManager.getPaymentBank(cardId);
                        if (paymentBank == null) {//银行卡不对
                            return fail("银行卡id不对!");
                        }
                        // 银行卡号
                        String bankAccount = paymentBank.getBankAccount();//银行账户
                        String bankTemplateId = paymentBank.getBankTemplateId();//银行卡模板信息
                        Integer accountType = paymentBank.getAccountType();

                        //查找银行信息
                        PaymentBankTemplate paymentBankTemplate = dataAccessManager.getPaymentBankTemplate(bankTemplateId);

                        //生成新序列号
                        String tn  = uniquePrimaryKey("TN");
                        // 终端类型：0商家 1供应商
                        Integer loginType = getIntParameter("loginType");
                        if (loginType == null){
                            return fail("终端类型不明确");
                        }

                        //添加提现记录
                        PaymentTransactionLogger paymentTransactionLogger = new PaymentTransactionLogger();
                        paymentTransactionLogger.setTransSequenceNumber(tn);//序列号
                        paymentTransactionLogger.setUserId(user.getId());//用户id
                        paymentTransactionLogger.setTransStatus(TransStatusEnum.TRADE_DEFAULT.getKey());//交易状态 平台审核中
                        paymentTransactionLogger.setPaymentType(PayTypeEnum.TRANS_TYPE_BALANCE.getKey()+"");//支付类型
                        paymentTransactionLogger.setTransType(TransTypeEnum.DRAW_CASH.getKey());//提现类型
                        paymentTransactionLogger.setTransSupplierType(loginType);//商家 供应商提现
                        paymentTransactionLogger.setAccountNumber(bankAccount);//收款账号
                        paymentTransactionLogger.setChannelUserId(user.getId());
                        paymentTransactionLogger.setAccountName(paymentBank.getAccountName());//账户名字
                        paymentTransactionLogger.setBankName(paymentBankTemplate.getName());//银行信息
                        paymentTransactionLogger.setBankBranchCode(paymentBankTemplate.getBankCode());//支行编码
                        paymentTransactionLogger.setBankSimpleName(paymentBankTemplate.getSimpleCode());//支行编码
                        paymentTransactionLogger.setTransTitle(TransTypeEnum.DRAW_CASH.getValue());
                        paymentTransactionLogger.setTransTotalAmount(money);// 分
                        paymentTransactionLogger.setUseConpon(0);//未使用优惠券
//                        paymentTransactionLogger.setPaymentTime(new Date());//支付时间

                        paymentTransactionLogger.setTakeRuleId(ruleId);//提现规则id
                        paymentTransactionLogger.setTakeDesc(paymentTakeRule.getTakeDesc());// 提现规则描述
                        paymentTransactionLogger.setHighCost(paymentTakeRule.getHighCost());// 最低收费费用 单位：分
                        paymentTransactionLogger.setLowCost(paymentTakeRule.getLowCost());// 最高收费费用 单位：分
                        paymentTransactionLogger.setRate(paymentTakeRule.getRate());// 万份比

                        paymentTransactionLogger.setAccountType(accountType);//账户类型1、储值卡 2、信用卡
                        paymentTransactionLogger.setCurrencyType("RMB");
                        String formId1 = getUTF("formId1",null);
                        String formId2 = getUTF("formId2",null);
                        paymentTransactionLogger.setWxFormId(formId2);//微信formId
                        dataAccessManager.addPaymentTransactionLogger(paymentTransactionLogger);//添加记录

                        //财务那边确定之后 才在流水上面添加 流水记录 如果财务那边不确定提现的话 那么余额也要加回来
                        //在余额的基础上 减少余额
                        dataAccessManager.changePaymentCurrencyAccountWithWithdrawl(sysUser.getId(), money, CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());
                        // 微信推送模板消息
                        String openId = sysUser.getOpenId();
                        if (openId == null) {
                            return fail("用户的openId不存在");
                        }
                        if(formId1 != null || formId2 != null){
                            HashMap<String, Object> map = new HashMap<>();
                            // 订单号
                            map.put("keyword1",paymentTransactionLogger.getTransSequenceNumber());
                            // 卡号
                            map.put("keyword2",paymentTransactionLogger.getAccountNumber());
                            // 提现金额
                            map.put("keyword3",paymentTransactionLogger.getTransTotalAmount().doubleValue()/100+" 元");
                            // 提现发起时间
                            map.put("keyword4",DateUtils.format(new Date()));
                            // 温馨提示 规则
                            map.put("keyword5",paymentTakeRule.getTakeDesc());
                            //判断当前终端
                            String templateId = "";
                            if(loginType == 0){
                                templateId = WeixinTemplateIdConstant.WEIXIN_TEMPLATE_APPLY;
                            }else{
                                templateId = WeixinTemplateIdConstant.WEIXIN_TEMPLATE_APPLY_SUPPLIER;
                            }
                            logger.info("environmentEnum:{}", environmentEnum);
                            JSONObject jsonObject = WeiXinPayUtils.templateMessageSend(
                                    loginType,
                                    openId,
                                    templateId,
                                    formId1,
                                    map,
                                    environmentEnum);
                            logger.info(jsonObject.toJSONString());
                        }
                        return success("提现请求发起成功!");
                    } else {
                        return fail("验证码错误!");
                    }
                } else {
                    logger.error("code:{}",code1);
                    return fail("系统错误!");
                }
            } else {
                return fail("请发送短信验证码!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return fail("系统错误!");
        }
    }

    /**
     * 进入提现操作 获取结算卡 和 提现规则
     *
     * @return
     */
    @Override
    public JSONObject getRulePaymentTransactionLogger() {
        //获取当前用户id的结算卡
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userId = user.getId();
        List<PaymentBank> paymentBankByUserId = dataAccessManager.getPaymentBankByUserId(userId);
        if (paymentBankByUserId != null && paymentBankByUserId.size() > 0) {
            paymentBankByUserId.forEach(x -> {//将里面银行卡号码第5-8位加密
                String bankAccount = x.getBankAccount();
                StringBuffer sb = new StringBuffer(bankAccount);
                sb.replace(4, 7, "****");
                x.setBankAccount(sb.toString());
            });
        }
        //获取所有的到账时间规则
        List<PaymentTakeRule> paymentTakeRules = dataAccessManager.getPaymentTakeRuleByMode(getIntParameter("loginType"));
        List<PaymentTakeRule> list = new ArrayList<>();
        if (paymentTakeRules != null && paymentTakeRules.size() > 0) {
            for (PaymentTakeRule paymentTakeRule : paymentTakeRules) {
                PaymentTakeRule paymentTakeRuleEntity = new PaymentTakeRule();
                paymentTakeRuleEntity.setId(paymentTakeRule.getId());
                paymentTakeRuleEntity.setTakeDesc(paymentTakeRule.getTakeDesc());
                paymentTakeRuleEntity.setRate(paymentTakeRule.getRate());
                list.add(paymentTakeRuleEntity);
            }
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("card", paymentBankByUserId);
        jsonObject.put("rule", list);
        return success(jsonObject);
    }

    /**
     * 确认打款
     * @return
     */
    @Override
    public JSONObject changePaymentTransactionLoggerRemit() {
        Environment environment = applicationContext.getEnvironment();
        int environmentEnum = 0;
        String[] activeProfiles = environment.getActiveProfiles();
        if(activeProfiles != null && activeProfiles.length > 0){
            String activeProfile = activeProfiles[0];
            environmentEnum = EnvironmentEnum.getKey(activeProfile);
        }
        String[] ids=getId().split(",");
        if(ArrayUtils.isEmpty(ids)){
            return fail("参数不能为空");
        }
        // 打款-已支付
        dataAccessManager.changePaymentTransactionLoggerStatus(ids, TransStatusEnum.TRADE_SUCCESSED_SERVER.getKey(), new HashMap<>());
        // 微信模板消息
        for (String id : ids) {
            PaymentTransactionLogger paymentTransactionLogger = dataAccessManager.getPaymentTransactionLogger(id);
            if (paymentTransactionLogger == null) {
                logger.error("提现记录不存在，id为{}", id);
                continue;
            }
            SysUser sysUser = SysUserRemoteService.getSysUser(paymentTransactionLogger.getUserId(), getHttpServletRequest());
            if (sysUser == null) {
                logger.error("提现用户关联不存在，订单号为{}",paymentTransactionLogger.getTransSequenceNumber());
                continue;
            }
            PaymentTakeRule paymentTakeRule = dataAccessManager.getPaymentTakeRule(paymentTransactionLogger.getTakeRuleId());
            String openId = sysUser.getOpenId();
            if (openId == null) {
                logger.error("提现用户没有openId，userId为{}", paymentTransactionLogger.getUserId());
                continue;
            }
            String formId = paymentTransactionLogger.getWxFormId();
            if (formId == null) {
                logger.error("该笔订单无formId记录，订单号为{}", paymentTransactionLogger.getTransSequenceNumber());
                continue;
            }
            HashMap<String, Object> map = new HashMap<>();
            // 订单号
            map.put("keyword1",paymentTransactionLogger.getTransSequenceNumber());
            // 卡号
            map.put("keyword2",paymentTransactionLogger.getAccountNumber());
            // 提现金额
            map.put("keyword3",paymentTransactionLogger.getTransTotalAmount().doubleValue()/100+" 元");
            // 提现发起时间
            map.put("keyword4",DateUtils.format(new Date()));
            // 温馨提示 规则
            map.put("keyword5",paymentTakeRule.getTakeDesc());

            Integer transSupplierType = paymentTransactionLogger.getTransSupplierType();
            String weixinTemplateRemit = null;
            if(transSupplierType != null){
                if(transSupplierType == 0){
                    weixinTemplateRemit = WeixinTemplateIdConstant.WEIXIN_TEMPLATE_REMIT;
                }else{
                    weixinTemplateRemit = WeixinTemplateIdConstant.WEIXIN_TEMPLATE_REMIT_SUPPLIER;
                }
            }
            JSONObject jsonObject = WeiXinPayUtils.templateMessageSend(
                    // 20190615 oy修改
                    paymentTakeRule.getTakeMode(),
                    openId,
                    weixinTemplateRemit,
                    formId,
                    map,
                    environmentEnum
            );
            logger.info(jsonObject.toJSONString());
        }
        return success();
    }

    @Override
    public JSONObject changePaymentTransactionLoggerFinish() {
        String[] id=getId().split(",");
        if(ArrayUtils.isEmpty(id)){
            return fail("参数不能为空");
        }
        // 交易成功
        dataAccessManager.changePaymentTransactionLoggerStatus(id,TransStatusEnum.TRADE_FINISHED.getKey(), new HashMap());
        return success();
    }

    /**
     * 拒绝提现
     * @return
     */
    @Override
    public JSONObject changePaymentTransactionLoggerRefuse() {
        Environment environment = applicationContext.getEnvironment();
        int environmentEnum = 0;
        String[] activeProfiles = environment.getActiveProfiles();
        if(activeProfiles != null && activeProfiles.length > 0){
            String activeProfile = activeProfiles[0];
            environmentEnum = EnvironmentEnum.getKey(activeProfile);
        }
        String[] ids=getId().split(",");
        if(ArrayUtils.isEmpty(ids)){
            return fail("参数不能为空");
        }
        String remark = getUTF("remark");
        if (StringUtils.isEmpty(remark.trim())) {
            return fail("备注信息不能为空");
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("remark", remark);
        // 交易类型修改为提现退款
        hashMap.put("transType", TransTypeEnum.REJECT_DRAW_CASH.getKey());
        hashMap.put("transTitle", TransTypeEnum.REJECT_DRAW_CASH.getValue());
        // 更新提现记录->拒绝
        dataAccessManager.changePaymentTransactionLoggerStatus(ids,TransStatusEnum.TRADE_REJECT.getKey(), hashMap);
        // 发送微信模板消息
        for (String id : ids) {
            PaymentTransactionLogger paymentTransactionLogger = dataAccessManager.getPaymentTransactionLogger(id);
            if (paymentTransactionLogger == null) {
                logger.error("提现记录不存在，id为{}", id);
                continue;
            }
            SysUser sysUser = SysUserRemoteService.getSysUser(paymentTransactionLogger.getUserId(), getHttpServletRequest());
            if (sysUser == null) {
                logger.error("提现用户关联不存在，订单号为{}",paymentTransactionLogger.getTransSequenceNumber());
                continue;
            }
            // 回滚余额
            dataAccessManager.changePaymentCurrencyAccountWithWithdrawlAdd(sysUser.getId(), paymentTransactionLogger.getTransTotalAmount(),CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());
            PaymentTakeRule paymentTakeRule = dataAccessManager.getPaymentTakeRule(paymentTransactionLogger.getTakeRuleId());
            String openId = sysUser.getOpenId();
            if (openId == null) {
                logger.error("提现用户没有openId，userId为{}", paymentTransactionLogger.getUserId());
                continue;
            }
            String formId = paymentTransactionLogger.getWxFormId();
            if (formId == null) {
                logger.error("该笔订单无formId记录，订单号为{}", paymentTransactionLogger.getTransSequenceNumber());
                continue;
            }
            HashMap<String, Object> map = new HashMap<>();
            // 订单号
            map.put("keyword1",paymentTransactionLogger.getTransSequenceNumber());
            // 卡号
            map.put("keyword2",paymentTransactionLogger.getAccountNumber());
            // 提现金额
            map.put("keyword3",paymentTransactionLogger.getTransTotalAmount().doubleValue()/100+" 元");
            // 提现发起时间
            map.put("keyword4",DateUtils.format(new Date()));
            // 温馨提示 规则
            map.put("keyword5",remark);

            //1供应商  0商家
            Integer transSupplierType = paymentTransactionLogger.getTransSupplierType();
            String weixinTemplateRemit = null;
            if(transSupplierType != null){
                if(transSupplierType == 0){
                    weixinTemplateRemit = WeixinTemplateIdConstant.WEIXIN_TEMPLATE_REFUSE;
                }else{
                    weixinTemplateRemit = WeixinTemplateIdConstant.WEIXIN_TEMPLATE_REFUSE_SUPPLIER;
                }
            }
            JSONObject jsonObject = WeiXinPayUtils.templateMessageSend(
                    // 20190615 oy 修改
                    paymentTakeRule.getTakeMode(),
                    openId,
                    weixinTemplateRemit,
                    formId,
                    map,
                    environmentEnum
            );
            logger.info(jsonObject.toJSONString());
        }
        return success();
    }
    @Override
    public JSONObject changePaymentTransactionLoggerMap() {
        HashMap<String, Object> map = new HashMap<>();
        String transSequenceNumber = getUTF("transSequenceNumber");
        String refundStatus = getUTF("refundStatus");
        map.put("transStatus",TransStatusEnum.TRADE_SUCCESS_CLIENT.getKey());
        map.put("transSequenceNumber",transSequenceNumber);
        map.put("refundStatus",refundStatus);
        dataAccessManager.changePaymentTransactionLoggerByMap(map);
        return success();
    }

    @Override
    public JSONObject getPaymentTransactionLoggerByMap() {
        String utf = getUTF("");
        HashMap<String, Object> map = new HashMap<>();
        //List<PaymentTransactionLogger> paymentTransactionLoggerByMap = dataAccessManager.getPaymentTransactionLoggerByMap(map);
        return success();
    }

    @Override
    public JSONObject getPaymentTransactionLoggerPos() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user == null){
            return  fail("用户登录失效");
        }
        HashMap<String, Object> map = new HashMap<>();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String paymentTime = getUTF("paymentTime",sf.format(new Date()));
        Integer orderSum = 0;
        long moneySum = 0;
        map.put("paymentTime",paymentTime);
        map.put("userId",user.getId());
        int pageIndex = getIntParameter("page", 1);
        int pageSize = getIntParameter("limit", 50);
        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageIndex - 1);
        int limitEnd = pageSize;
        map.put("pageIndex",limitStart);
        map.put("pageSize",limitEnd);
        List<PayLogNumbersVo> paymentTransactionLoggerNumberDate = dataAccessManager.getPaymentTransactionLoggerNumberDate(map);
        List<PayLogVo> paymentTransactionLogger = dataAccessManager.getPaymentTransactionLoggerByMap(map);
        int count = dataAccessManager.getPaymentTransactionLoggerByMapCount(map);
        if(paymentTransactionLoggerNumberDate != null && paymentTransactionLoggerNumberDate.size() > 0){
            for (PayLogNumbersVo payLogNumbersVo : paymentTransactionLoggerNumberDate) {
                orderSum+=Integer.parseInt(payLogNumbersVo.getNumber());
                moneySum+=payLogNumbersVo.getMoneySum();
            }
        }

        //添加商品概况
        if(paymentTransactionLogger != null && paymentTransactionLogger.size() > 0){
            for (PayLogVo payLogVo : paymentTransactionLogger) {
                String general = "";
                List<OrderEntry> orderEntryByOrderNumber = OrderEntryRemoteService.getOrderEntryPosByOrdenumber(payLogVo.getOrderNumer(), getHttpServletRequest());
                if(orderEntryByOrderNumber != null && orderEntryByOrderNumber.size() > 0){
                    List<OrderGoodSnapshot> orderEntryByOrderNum = OrderGoodSnapshotRemoteService.getOrderGoodSnapshotPosMapByorderId(orderEntryByOrderNumber.get(0).getId(), getHttpServletRequest());
                    if(orderEntryByOrderNum != null && orderEntryByOrderNum.size() > 0){
                        for (OrderGoodSnapshot orderGoodSnapshot : orderEntryByOrderNum) {
                            general += orderGoodSnapshot.getGoodName()+"、";
                        }
                    }
                }
                if(general != null && general.length() > 2 ){
                    general = general.substring(0,general.length()-1);
                }
                payLogVo.setOrderConditions(general);
            }
        }


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("payLog",paymentTransactionLogger);
        jsonObject.put("payLogNumber",paymentTransactionLoggerNumberDate);
        jsonObject.put("moneySum",moneySum);
        jsonObject.put("orderSum",orderSum);
        jsonObject.put("count",count);
        return success(jsonObject);
    }

    @Override
    public JSONObject getPaymentTransactionLoggerMap() {
        String paymentType = getUTF("paymentType");
        String transType = getUTF("transType");
        String transSequenceNumber = getUTF("transSequenceNumber");
        String endPayaTime = getUTF("endPayaTime");
        String startPayaTime = getUTF("startPayaTime");
        HashMap<String, Object> map = new HashMap<>();
        map.put("paymentType",paymentType);
        map.put("transType",transType);
        map.put("transSequenceNumber",transSequenceNumber);
        map.put("endPayaTime",endPayaTime);
        map.put("startPayaTime",startPayaTime);
        List<PaymentTransactionLogger> paymentTransactionLoggerMap = dataAccessManager.getPaymentTransactionLoggerMap(map);
        return success(paymentTransactionLoggerMap);
    }

    @Override
    public JSONObject getPaymentTransactionLoggerById() {
        String id = getUTF("id");
        PaymentTransactionLogger paymentTransactionLogger = dataAccessManager.getPaymentTransactionLogger(id);
        return success(paymentTransactionLogger);
    }

    @Override
    public JSONObject getPaymentTransactionLoggerPosMap() {
        //结束时间+1天
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Format f = new SimpleDateFormat("yyyy-MM-dd");
        String refundNumberOrOrderNumber = getUTF("refundNumberOrOrderNumber",null);
        String endPayaTime = getUTF("endPayaTime",null);
        String startPayaTime = getUTF("startPayaTime",sdf.format(new Date()));

        try {
            if(endPayaTime != null){
                //结束时间加一天
                Date sDate = sdf.parse(endPayaTime);
                Calendar c = Calendar.getInstance();
                c.setTime(sDate);
                c.add(Calendar.DAY_OF_MONTH, 1);
                sDate = c.getTime();
                endPayaTime = f.format(sDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //店主id
        String userId = "";
        if(user.getLeaderUserId() == null || user.getLeaderUserId().equals("")){
            userId= user.getId();
        }else{
            userId = user.getLeaderUserId();
        }


        HashMap<String, Object> map = new HashMap<>();
        map.put("pageIndex",getPageStartIndex(getPageSize()));
        map.put("pageSize",getPageSize());
        map.put("refundNumberOrOrderNumber",refundNumberOrOrderNumber);
        map.put("userId",userId);
        map.put("endPayaTime",endPayaTime);
        map.put("startPayaTime",startPayaTime);
        map.put("transType",TransTypeEnum.REFUND.getKey());
        List<PaymentTransactionLogger> paymentTransactionLoggerMap = dataAccessManager.getPaymentTransactionLoggerMap(map);
        if(paymentTransactionLoggerMap.size() <= 0){
           return fail("没有数据");
        }
        List<String> orderNumbers = paymentTransactionLoggerMap.stream().map(PaymentTransactionLogger::getChannelTradeNumber).collect(Collectors.toList());
        List<String> userIds = paymentTransactionLoggerMap.stream().map(PaymentTransactionLogger::getPartnerUserId).distinct().collect(Collectors.toList());

        String orderNumbersStr = JSONObject.toJSONString(orderNumbers);
        List<OrderEntry> list = OrderEntryRemoteService.inOrderNumber(orderNumbersStr, getHttpServletRequest());
        HashMap<String, Object> mapObj = new HashMap<>();
        if(list != null){
            for (OrderEntry orderEntry : list) {
                mapObj.put(orderEntry.getOrderSequenceNumber(),orderEntry.getActualPrice());
            }
        }

        HashMap<String, Object> mapObjUser = new HashMap<>();
        List<SysUser> userByIds = SysUserRemoteService.getSysUserInIds(userIds, getHttpServletRequest());
        if(userByIds != null){
            for (SysUser userById : userByIds) {
                mapObjUser.put(userById.getId(),userById.getRealName());
            }
        }
        List<PayLogVo2> payLogVo = new ArrayList<>();
        if(paymentTransactionLoggerMap.size()>0){
            for (PaymentTransactionLogger paymentTransactionLogger : paymentTransactionLoggerMap) {
                PayLogVo2 payLogVo2 = new PayLogVo2();
                BeanUtils.copyProperties(paymentTransactionLogger,payLogVo2);
                payLogVo2.setOrderNumberPrice((long)mapObj.get(payLogVo2.getChannelTradeNumber()));
                payLogVo2.setPartnerUserName((String) mapObjUser.get(payLogVo2.getPartnerUserId()));
                payLogVo.add(payLogVo2);
            }
        }
        return success(payLogVo);
    }
}
