package com.tuoniaostore.payment.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentBankService {

    JSONObject addPaymentBank();

    JSONObject getPaymentBank();

    JSONObject getPaymentBanks();

    JSONObject getPaymentBankCount();
    JSONObject getPaymentBankAll();

    JSONObject changePaymentBank();

    /**
     * 获取用户银行卡
     * @return
     */
    JSONObject getPaymentBankByUserId();

    /**
     * 新增用户银行卡
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject addPaymentBankByUserId();

    JSONObject deletePaymentBank();
}
