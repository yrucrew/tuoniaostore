package com.tuoniaostore.payment.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.MessageStatus;
import com.tuoniaostore.commons.constant.MessageType;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.payment.PaymentCurrentPropertiesTypeEnum;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserSmsRemoteService;
import com.tuoniaostore.commons.utils.MessageUtils;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyProperties;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.user.SysUserSmsLogger;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.redis.RedisService;
import com.tuoniaostore.payment.service.PaymentCurrencyPropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("paymentCurrencyPropertiesService")
public class PaymentCurrencyPropertiesServiceImpl extends BasicWebService implements PaymentCurrencyPropertiesService {
    @Autowired
    private PaymentDataAccessManager dataAccessManager;

    @Autowired
    private RedisService redisService;

    @Override
    public JSONObject addPaymentCurrencyProperties() {
        PaymentCurrencyProperties paymentCurrencyProperties = new PaymentCurrencyProperties();
        //账户ID
        String userId = getUTF("userId");
        //属性类型
        Integer type = getIntParameter("type", 0);
        //属性key
        String k = getUTF("k", null);
        //属性值
        String v = getUTF("v", null);
        paymentCurrencyProperties.setUserId(userId);
        paymentCurrencyProperties.setType(type);
        paymentCurrencyProperties.setK(k);
        paymentCurrencyProperties.setV(v);
        dataAccessManager.addPaymentCurrencyProperties(paymentCurrencyProperties);
        return success();
    }

    @Override
    public JSONObject getPaymentCurrencyProperties() {
        PaymentCurrencyProperties paymentCurrencyProperties = dataAccessManager.getPaymentCurrencyProperties(getId());
        return success(paymentCurrencyProperties);
    }

    @Override
    public JSONObject getPaymentCurrencyPropertiess() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<PaymentCurrencyProperties> paymentCurrencyPropertiess = dataAccessManager.getPaymentCurrencyPropertiess(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getPaymentCurrencyPropertiesCount(status, name);
        return success(paymentCurrencyPropertiess, count);
    }

    @Override
    public JSONObject getPaymentCurrencyPropertiesAll() {
        List<PaymentCurrencyProperties> paymentCurrencyPropertiess = dataAccessManager.getPaymentCurrencyPropertiesAll();
        return success(paymentCurrencyPropertiess);
    }

    @Override
    public JSONObject getPaymentCurrencyPropertiesCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getPaymentCurrencyPropertiesCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changePaymentCurrencyProperties() {
        PaymentCurrencyProperties paymentCurrencyProperties = dataAccessManager.getPaymentCurrencyProperties(getId());
        //账户ID
        String userId = getUTF("userId");
        //属性类型
        Integer type = getIntParameter("type", 0);
        //属性key
        String k = getUTF("k", null);
        //属性值
        String v = getUTF("v", null);
        paymentCurrencyProperties.setUserId(userId);
        paymentCurrencyProperties.setType(type);
        paymentCurrencyProperties.setK(k);
        paymentCurrencyProperties.setV(v);
        dataAccessManager.changePaymentCurrencyProperties(paymentCurrencyProperties);
        return success();
    }

    /**
     *  修改支付密码  短信验证码
     * @return
     */
    @Override
    public JSONObject changePaymentPasswordMsg() {
        //获取当前手机号码
        String phoneNum = getUTF("phoneNum");

        if (phoneNum == null || phoneNum.equals("")) {
            return  fail("手机号码不能为空！");
        }
        //发送验证码
        String code = "";
        SysUserSmsLogger sysUserSmsLogger = new SysUserSmsLogger();
        try {
            code = MessageUtils.sendMessage(phoneNum);
            redisService.setMessageCode(MessageType.MESSAGE_TYPE_RESETPAYMENT.getPrefix() + phoneNum, code);//加入redis
            sysUserSmsLogger.setStatus(MessageStatus.MESSAGE_STATUS_SUCCESS.getKey());
        } catch (Exception e) {
            sysUserSmsLogger.setStatus(MessageStatus.MESSAGE_STATUS_FAILD.getKey());
            e.printStackTrace();
        } finally {
            sysUserSmsLogger.setPhone(phoneNum);
            sysUserSmsLogger.setContent("修改支付密码验证码:"+code);
            sysUserSmsLogger.setCode(code);
            sysUserSmsLogger.setType(MessageType.MESSAGE_TYPE_RESETPAYMENT.getKey());
            //调用community服务 加入短信记录
            try {
                SysUserSmsRemoteService.addSysUserSmsLogger(sysUserSmsLogger,getHttpServletRequest());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return success("验证码发送成功！");
    }

    /**
     * 重置支付密码
     * @return
     */
    @Override
    public JSONObject changePaymentPassword() {
        //获取当前手机号码
        String phoneNum = getUTF("phoneNum");//手机号码
        String code = getUTF("validCode");//验证码
        String password = getUTF("password");//密码


        if(phoneNum == null || phoneNum.equals("")){
            return fail("手机号码不能为空");
        }

        if(password == null || password.equals("")){
            return fail("密码不能为空");
        }

        if(code == null || code.equals("")){
            return fail("验证码不能为空");
        }
        //获取当前用户id
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userId = user.getId();//用户id

        //验证当前号码 和 接受到的验证码是否一样
        try {
            //验证码是否正确
            String messageCode = redisService.getMessageCode(MessageType.MESSAGE_TYPE_RESETPAYMENT.getPrefix() + phoneNum);//redis获取验证码
            if (messageCode == null) {
                return fail("验证码无效!");
            }else{
                if(messageCode.equals(code)){//说明验证码正确
                    //修改一下 用户的支付密码
                    //查询是否用户有支付密码记录，如果有 正常修改 如果没有 新增记录

                    //参数 用户id 支付类型 （补充：因为这个表是存放着扩展数据，支付密码只是一个扩展数据，所以要加类型 有枚举）
                    PaymentCurrencyProperties paymentCurrencyProperties =
                            dataAccessManager.getPaymentCurrencyPropertiesByUserId(userId, PaymentCurrentPropertiesTypeEnum.PAYMENT_CURRENT_PROPERTIES_TYPE_PAY.getKey());
                    if (paymentCurrencyProperties == null) {//说明没有设置过支付密码
                        //新增支付密码
                        PaymentCurrencyProperties paymentCurrencyProperties1 = new PaymentCurrencyProperties();
                        paymentCurrencyProperties1.setUserId(userId);//用户名字
                        paymentCurrencyProperties1.setK("paymentPassword");//属性 支付密码
                        paymentCurrencyProperties1.setType(PaymentCurrentPropertiesTypeEnum.PAYMENT_CURRENT_PROPERTIES_TYPE_PAY.getKey());//类型 枚举
                        paymentCurrencyProperties1.setV(encryptionPassword(password));//值 密码具体值  加密
                        dataAccessManager.addPaymentCurrencyProperties(paymentCurrencyProperties1);
                    }else{//说明之前有设置支付密码
                        //更新支付密码
                        paymentCurrencyProperties.setV(encryptionPassword(password));
                        dataAccessManager.updatePaymentCurrencyPropertiesPassword(paymentCurrencyProperties);
                    }
                }else {
                    return fail("验证码错误！");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success("重置支付密码成功!");
    }

    /**
     * 重置密码  根据手机验证码 登录密码 修改新的密码
     * @return
     */
    @Override
    public JSONObject changePaymentPassword2() {
        //获取手机号码
        String phoneNum = getUTF("phoneNum");
        //获取手机验证码
        String validCode = getUTF("validCode");
        //登录密码
        String password = getUTF("password");
        //新的密码
        String payPassword = getUTF("payPassword");

        if(phoneNum == null || phoneNum.equals("")){//验证手机号码
            return fail("号码不能为空!");
        }

        if(validCode == null || validCode.equals("")){//验证手机号码
            return fail("验证码不能为空!");
        }

        if(password == null || password.equals("")){//验证手机号码
            return fail("密码不能为空!");
        }

        if(payPassword == null || payPassword.equals("")){//验证手机号码
            return fail("支付不能为空!");
        }

        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userId = user.getId();
        //验证 验证码是否正确
        try {
            SysUserSmsLogger sysUserSmsLogger = SysUserSmsRemoteService.getSysUserSmsLogger(phoneNum, MessageType.MESSAGE_TYPE_RESETPAYMENT.getKey(), getHttpServletRequest());//获取到短信记录
            if(sysUserSmsLogger != null){
                if(sysUserSmsLogger.getCode().equals(validCode)){//验证码正确
                    //查找当前用户的登录密是否输入正确
                    String s = encryptionPassword(password);
                    try {
                        SysUser sysUser = SysUserRemoteService.getSysUser(userId, getHttpServletRequest());
                        if(sysUser.getPassword().equals(s)){//验证登录密码是否正确
                            //修改支付密码
                            PaymentCurrencyProperties paymentCurrencyProperties =
                                    dataAccessManager.getPaymentCurrencyPropertiesByUserId(userId, PaymentCurrentPropertiesTypeEnum.PAYMENT_CURRENT_PROPERTIES_TYPE_PAY.getKey());
                            if (paymentCurrencyProperties == null) {//说明没有设置过支付密码
                                //新增支付密码
                                PaymentCurrencyProperties paymentCurrencyProperties1 = new PaymentCurrencyProperties();
                                paymentCurrencyProperties1.setUserId(userId);//用户名字
                                paymentCurrencyProperties1.setK("paymentPassword");//属性 支付密码
                                paymentCurrencyProperties1.setType(PaymentCurrentPropertiesTypeEnum.PAYMENT_CURRENT_PROPERTIES_TYPE_PAY.getKey());//类型 枚举
                                paymentCurrencyProperties1.setV(encryptionPassword(password));//值 密码具体值  加密
                                dataAccessManager.addPaymentCurrencyProperties(paymentCurrencyProperties1);
                            }else{//说明之前有设置支付密码
                                //更新支付密码
                                paymentCurrencyProperties.setV(encryptionPassword(password));
                                dataAccessManager.updatePaymentCurrencyPropertiesPassword(paymentCurrencyProperties);
                            }
                        }else{
                            return fail("登录密码错误！");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    return fail("验证码错误！");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success("重置支付密码成功！");
    }

    @Override
    public JSONObject getPaymentPasswordByUserId() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user == null){
            return fail("用户登录失效");
        }
        PaymentCurrencyProperties paymentCurrencyProperties =
                dataAccessManager.getPaymentCurrencyPropertiesByUserId(user.getId(), PaymentCurrentPropertiesTypeEnum.PAYMENT_CURRENT_PROPERTIES_TYPE_PAY.getKey());

        return success(paymentCurrencyProperties);
    }

}
