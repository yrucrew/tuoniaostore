package com.tuoniaostore.payment.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.datamodel.payment.PaymentSettlementSet;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.service.PaymentSettlementSetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;


@Service("paymentSettlementSetService")
public class PaymentSettlementSetServiceImpl extends BasicWebService implements PaymentSettlementSetService {
    @Autowired
    private PaymentDataAccessManager dataAccessManager;

    @Override
    public JSONObject addPaymentSettlementSet() {
        PaymentSettlementSet paymentSettlementSet = new PaymentSettlementSet();

        //扣点(万分比)
        Long cablePoint = getLongParameter("cablePoint", 0);
        String userId = getUTF("userId");
        //结算天数
        Integer settlementDays = getIntParameter("settlementDays", 0);
        //结算金额
        Long settlementMoney = getLongParameter("settlementMoney", 0);
        //结算金额：0.按结算天数，1.按结算额度
        Integer settlementType = getIntParameter("settlementType", 0);
        //1、扣点处理，2返点处理
        Integer type = getIntParameter("type", 0);
        paymentSettlementSet.setUserId(userId);
        paymentSettlementSet.setCablePoint(cablePoint);
        paymentSettlementSet.setSettlementDays(settlementDays);
        paymentSettlementSet.setSettlementMoney(settlementMoney);
        paymentSettlementSet.setSettlementType(settlementType);
        paymentSettlementSet.setType(type);
        dataAccessManager.addPaymentSettlementSet(paymentSettlementSet);
        return success();
    }

    @Override
    public JSONObject getPaymentSettlementSet() {
        PaymentSettlementSet paymentSettlementSet = dataAccessManager.getPaymentSettlementSet(getId());
        fullUserMsg(paymentSettlementSet);
        return success(paymentSettlementSet);
    }

    @Override
    public JSONObject getPaymentSettlementSets() {
        String phoneNum = getUTF("phoneNum", null);
        String userName = getUTF("userName", null);
        Integer settlementType = getIntParameter("settlementType", -1);
        Integer type = getIntParameter("type", -1);

        List<String> ids = null;
        // 用户名搜索用户id
        if (!StringUtils.isEmpty(userName)) {
            List<SysUser> sysUserList = SysUserRemoteService.getSysUserByName(userName, getHttpServletRequest());
            if (!CollectionUtils.isEmpty(sysUserList)) {
                List<String> list = new ArrayList<>(sysUserList.stream().map(SysUser::getId).collect(Collectors.toList()));
                ids = new ArrayList<>();
                ids.addAll(list);
            }
        }
        // 手机号搜索用户id
        if(phoneNum != null && !phoneNum.equals("")){
            List<SysUser> list = new ArrayList<>(SysUserRemoteService.getSysUserByPhoneNumLike(phoneNum,getHttpServletRequest()));
            if(list != null && list.size() > 0){
                if (ids == null) {
                    ids = new ArrayList<>();
                }
                ids.addAll(list.stream().map(SysUser::getId).collect(Collectors.toList()));
            }
        }
        Map<String,Object> map = new HashMap<>();

        // 搜索无果
        if ((!StringUtils.isEmpty(userName)||!StringUtils.isEmpty(phoneNum))&&CollectionUtils.isEmpty(ids)) {
            return success(new ArrayList(), 0);
        }

        map.put("userIds",ids);
        map.put("settlementType",settlementType);
        map.put("type",type);

        List<PaymentSettlementSet> paymentSettlementSets = dataAccessManager.getPaymentSettlementSets(getPageStartIndex(getPageSize()), getPageSize(), map);
        int count = 0;
        if(paymentSettlementSets != null && paymentSettlementSets.size() > 0){
            fullListUserMsg(paymentSettlementSets);
            count = dataAccessManager.getPaymentSettlementSetCount(map);
        }
        return success(paymentSettlementSets, count);
    }

    @Override
    public JSONObject getPaymentSettlementSetAll() {
        List<PaymentSettlementSet> paymentSettlementSets = dataAccessManager.getPaymentSettlementSetAll();
        fullListUserMsg(paymentSettlementSets);
        return success(paymentSettlementSets);
    }


    /**
     * 填充创建人名字
     *
     * @param paymentSettlementSet
     */
    private void fullListUserMsg(List<PaymentSettlementSet> paymentSettlementSet) {
        int size = paymentSettlementSet.size();
        for (int i = 0; i < size; i++) {
            try {
                SysUser sysUser = SysUserRmoteService.getSysUser(paymentSettlementSet.get(i).getUserId(), getHttpServletRequest());
                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        paymentSettlementSet.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        paymentSettlementSet.get(i).setUserName(sysUser.getShowName());
                    } else {
                        paymentSettlementSet.get(i).setUserName(sysUser.getDefaultName());
                    }
                    paymentSettlementSet.get(i).setPhoneNum(sysUser.getPhoneNumber());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 填充创建人名字
     *
     * @param paymentSettlementSet
     */
    private void fullUserMsg(PaymentSettlementSet paymentSettlementSet) {
        try {
            SysUser sysUser = SysUserRmoteService.getSysUser(paymentSettlementSet.getUserId(), getHttpServletRequest());
            if (sysUser != null) {
                if (sysUser.getRealName() != null) {
                    paymentSettlementSet.setUserName(sysUser.getRealName());
                } else if (sysUser.getShowName() != null) {
                    paymentSettlementSet.setUserName(sysUser.getShowName());
                } else {
                    paymentSettlementSet.setUserName(sysUser.getDefaultName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public JSONObject getPaymentSettlementSetCount() {
        String keyWord = getUTF("keyWord", null);
//        int count = dataAccessManager.getPaymentSettlementSetCount(keyWord);
        return success();
    }

    @Override
    public JSONObject changePaymentSettlementSet() {
        PaymentSettlementSet paymentSettlementSet = dataAccessManager.getPaymentSettlementSet(getId());
        if(paymentSettlementSet==null){
            return fail("该条数据不存在！");
        }
        String userId = getUTF("userId");
        //扣点(万分比)
        Long cablePoint = getLongParameter("cablePoint", 0);
        //结算天数
        Integer settlementDays = getIntParameter("settlementDays", 0);
        //结算金额
        Long settlementMoney = getLongParameter("settlementMoney", 0);
        //结算金额：0.按结算天数，1.按结算额度
        Integer settlementType = getIntParameter("settlementType", 0);
        //1、扣点处理，2返点处理
        Integer type = getIntParameter("type", 0);
        paymentSettlementSet.setUserId(userId);
        paymentSettlementSet.setCablePoint(cablePoint);
        paymentSettlementSet.setSettlementDays(settlementDays);
        paymentSettlementSet.setSettlementMoney(settlementMoney);
        paymentSettlementSet.setSettlementType(settlementType);
        paymentSettlementSet.setType(type);
        dataAccessManager.changePaymentSettlementSet(paymentSettlementSet);
        return success();
    }

    @Override
    public JSONObject getPaymentSettlementSetByUserId() {
        String id = getUTF("id");
        PaymentSettlementSet paymentSettlementSetByUserId = dataAccessManager.getPaymentSettlementSetByUserId(id);
        return success(paymentSettlementSetByUserId);
    }

}
