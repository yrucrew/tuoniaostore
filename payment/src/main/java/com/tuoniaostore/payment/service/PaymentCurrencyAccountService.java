package com.tuoniaostore.payment.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentCurrencyAccountService {

    JSONObject addPaymentCurrencyAccount();

    JSONObject getPaymentCurrencyAccount();

    JSONObject getPaymentCurrencyAccounts();

    JSONObject getPaymentCurrencyAccountCount();
    JSONObject getPaymentCurrencyAccountAll();

    JSONObject changePaymentCurrencyAccount();

    /**
     * 根据当前用户id 查找账户余额
     * @return
     */
    JSONObject getPaymentCurrencyAccountByUserId();

    /**
     * 初始化账户信息
     * @return
     */
    void initPaymentCurrencyAccount();

    /**
     * 根据当前用户id 查找账户金额
     * @return
     */
    JSONObject getPaymentCurrencyAccountBalance();
    /**
     * 查询用户余额、会员余额
     * @author sqd
     * @date 2019/4/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getPaymentCurrencyAccountByCurrencyTypeEnum();

    /**
     * 获取用户账户总余额
     * @author sqd
     * @date 2019/4/28
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getPaymentCurrencyAccountBalanceByUserid();

    /**
     * 余额支付订单
     * @author sqd
     * @date 2019/4/28
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject paymentOrderBalance();

    /**
     * 商家端余额充值
     * @author sqd
     * @date 2019/5/2
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject balanceRecharge();

    /**
     * 余额充值回调
     * @author sqd
     * @date 2019/5/8
     * @param
     * @return
     */
    JSONObject recChangeCallback();

    JSONObject queryOrderForWeiPay();
    JSONObject getPaymentCurrencyAccountBalanceByid();
    JSONObject changePaymentCurrencyAccountJson();
}
