package com.tuoniaostore.payment.service;
import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentTransactionLoggerService {

    JSONObject addPaymentTransactionLogger();

    JSONObject addPaymentTransactionLoggerJson();

    JSONObject getPaymentTransactionLogger();

    /**
     * 提现列表
     */
    JSONObject getPaymentTransactionLoggers();

    JSONObject getPaymentTransactionLoggerCount();
    JSONObject getPaymentTransactionLoggerAll();

    JSONObject changePaymentTransactionLogger();

    /**
     * 提现记录
     * @return
     */
    JSONObject getWithdrawalPaymentTransactionLogger();

    /**
     * 提现短信
     * @return
     */
    JSONObject getMSGPaymentTransactionLogger();

    /**
     * 提现操作
     * @return
     */
    JSONObject withdrawalPaymentTransactionLogger();

    /**
     * 提现操作前期获取数据
     * @return
     */
    JSONObject getRulePaymentTransactionLogger();
    /**
     * 提现打款
     */
    JSONObject changePaymentTransactionLoggerRemit();
    /**
     * 提现到账
     */
    JSONObject changePaymentTransactionLoggerFinish();
    /**
     * 拒绝提现
     * @return
     */
    JSONObject changePaymentTransactionLoggerRefuse();

    /**
     * 修改提现记录
     * @return
     */
    JSONObject changePaymentTransactionLoggerMap();


    JSONObject getPaymentTransactionLoggerByMap();

    /**
     * 订单流水记录
     * @author sqd
     * @date 2019/5/8
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getPaymentTransactionLoggerPos();

    JSONObject getPaymentTransactionLoggerMap();
    JSONObject getPaymentTransactionLoggerById();
    JSONObject getPaymentTransactionLoggerPosMap();

}
