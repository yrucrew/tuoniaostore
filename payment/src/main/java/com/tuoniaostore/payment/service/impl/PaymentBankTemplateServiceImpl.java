package com.tuoniaostore.payment.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.payment.PaymentBankTemplate;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.service.PaymentBankTemplateService;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;


@Service("paymentBankTemplateService")
public class PaymentBankTemplateServiceImpl extends BasicWebService implements PaymentBankTemplateService {
    @Autowired
    private PaymentDataAccessManager dataAccessManager;

    @Override
    public JSONObject addPaymentBankTemplate() {
        PaymentBankTemplate paymentBankTemplate = new PaymentBankTemplate();
        //银行名字
        String name = getUTF("name", null);
        //简称
        String simpleCode = getUTF("simpleCode", null);
        //支行号
        String bankCode = getUTF("bankCode", null);
        //图片
        String logo = getUTF("logo", null);
        paymentBankTemplate.setName(name);
        paymentBankTemplate.setSimpleCode(simpleCode);
        paymentBankTemplate.setBankCode(bankCode);
        paymentBankTemplate.setLogo(logo);
        dataAccessManager.addPaymentBankTemplate(paymentBankTemplate);
        return success();
    }

    @Override
    public JSONObject getPaymentBankTemplate() {
        PaymentBankTemplate paymentBankTemplate = dataAccessManager.getPaymentBankTemplate(getId());
        return success(paymentBankTemplate);
    }

    @Override
    public JSONObject getPaymentBankTemplates() {
        String name = getUTF("name", null);                 //银行名称
        String simpleCode = getUTF("simpleCode", null);     //简称
        String bankCode = getUTF("bankCode", null);         //支行号
        Map<String,Object> map = new HashMap<>();
        map.put("name",name);
        map.put("simpleCode",simpleCode);
        map.put("bankCode",bankCode);

        //List<PaymentBankTemplate> paymentBankTemplates = dataAccessManager.getPaymentBankTemplates( getPageStartIndex(getPageSize()), getPageSize(), name);
        List<PaymentBankTemplate> paymentBankTemplates = dataAccessManager.getPaymentBankTemplatesByParam( getPageStartIndex(getPageSize()), getPageSize(), map);
        int count = 0;
        if(paymentBankTemplates != null && paymentBankTemplates.size() > 0) {
            //count = dataAccessManager.getPaymentBankTemplateCount( name);
            count = dataAccessManager.getPaymentBankTemplateCountByParam(map);
        }
        return success(paymentBankTemplates, count);
    }

    @Override
    public JSONObject getPaymentBankTemplateAll() {
        List<PaymentBankTemplate> paymentBankTemplates = dataAccessManager.getPaymentBankTemplateAll();
        return success(paymentBankTemplates);
    }

    @Override
    public JSONObject getPaymentBankTemplateCount() {
        String keyWord = getUTF("keyWord", null);
        int count = dataAccessManager.getPaymentBankTemplateCount( keyWord);
        return success(count);
    }

    @Override
    public JSONObject changePaymentBankTemplate() {
        PaymentBankTemplate paymentBankTemplate = dataAccessManager.getPaymentBankTemplate(getId());
        //银行名字
        String name = getUTF("name", null);
        //简称
        String simpleCode = getUTF("simpleCode", null);
        //支行号
        String bankCode = getUTF("bankCode", null);
        //图片
        String logo = getUTF("logo", null);
        paymentBankTemplate.setName(name);
        paymentBankTemplate.setSimpleCode(simpleCode);
        paymentBankTemplate.setBankCode(bankCode);
        paymentBankTemplate.setLogo(logo);
        dataAccessManager.changePaymentBankTemplate(paymentBankTemplate);
        return success();
    }

    @Override
    public JSONObject deletePaymentBankTemplate() {
        String[] idArr = getUTF("ids").split(",");
        if (ArrayUtils.isEmpty(idArr)) {
            return fail("参数为空");
        }
        dataAccessManager.deletePaymentBankTemplate(idArr);
        return success();
    }

}
