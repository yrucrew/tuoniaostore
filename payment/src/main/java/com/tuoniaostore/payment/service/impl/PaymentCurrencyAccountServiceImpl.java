package com.tuoniaostore.payment.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.pay.WeiXinTerminalPayTypeEnum;
import com.tuoniaostore.commons.constant.pay.WeixinCallcackType;
import com.tuoniaostore.commons.constant.payment.CurrencyTypeEnum;
import com.tuoniaostore.commons.constant.payment.PaymentCurrentPropertiesTypeEnum;
import com.tuoniaostore.commons.constant.payment.RelationTransTypeEnum;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.commons.support.order.OrderEntryRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplychainShopPropertiesRemoteService;
import com.tuoniaostore.commons.utils.WeiXinPayUtils;
import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyAccount;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyProperties;
import com.tuoniaostore.datamodel.payment.PaymentTransactionLogger;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.service.PaymentCurrencyAccountService;
import com.tuoniaostore.payment.service.PaymentCurrencyOffsetLoggerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import org.springframework.transaction.annotation.Transactional;


@Service("paymentCurrencyAccountService")
public class PaymentCurrencyAccountServiceImpl extends BasicWebService implements PaymentCurrencyAccountService {
    @Autowired
    private PaymentDataAccessManager dataAccessManager;

    @Autowired
    private PaymentCurrencyOffsetLoggerService paymentCurrencyOffsetLoggerService;

    private static final Logger logger = LoggerFactory.getLogger(PaymentCurrencyAccountServiceImpl.class);

    @Override
    public void initPaymentCurrencyAccount() {
        //用户ID
        String userId = getUTF("userId");

        //首先判断一下 该用户是否有余额账户信息
        PaymentCurrencyAccount paymentCurrencyAccountByUserId = dataAccessManager.getPaymentCurrencyAccountByUserId(userId, CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());//普通

        if(paymentCurrencyAccountByUserId == null){
            //初始化普通账户信息
            //初始参数
            PaymentCurrencyAccount paymentCurrencyAccount = new PaymentCurrencyAccount();
            //渠道ID(用于计算归属)
            String channelId = getUTF("channelId", "0");
            //货币名字
            String name = getUTF("name", CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getValue());
            //当前可用额度
            Long currentAmount = 0l;
            //冻结额度
            Long freezeAmount = 0l;
            //总的入账额度
            Long totalIntoAmount = 0l;
            //总的出账额度
            Long totalOutputAmount = 0l;
            paymentCurrencyAccount.setUserId(userId);
            paymentCurrencyAccount.setChannelId(channelId);
            paymentCurrencyAccount.setName(name);
            paymentCurrencyAccount.setCurrentAmount(currentAmount);
            paymentCurrencyAccount.setFreezeAmount(freezeAmount);
            paymentCurrencyAccount.setTotalIntoAmount(totalIntoAmount);
            paymentCurrencyAccount.setTotalOutputAmount(totalOutputAmount);

            //货币类型
            Integer currencyType = CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey();
            paymentCurrencyAccount.setCurrencyType(currencyType);
            dataAccessManager.addPaymentCurrencyAccount(paymentCurrencyAccount);
        }
        //再判断一下 该用户是否有会员余额信息
        PaymentCurrencyAccount paymentCurrencyAccountByUserId1 = dataAccessManager.getPaymentCurrencyAccountByUserId(userId, CurrencyTypeEnum.CURRENCY_TYPE_VIP.getKey());//会员

        if(paymentCurrencyAccountByUserId1 == null){
            //初始化会员账户信息
            //初始参数
            PaymentCurrencyAccount paymentCurrencyAccount = new PaymentCurrencyAccount();
            //渠道ID(用于计算归属)
            String channelId = getUTF("channelId", "0");
            //货币名字
            String name = getUTF("name", CurrencyTypeEnum.CURRENCY_TYPE_VIP.getValue());
            //当前可用额度
            Long currentAmount = 0l;
            //冻结额度
            Long freezeAmount = 0l;
            //总的入账额度
            Long totalIntoAmount = 0l;
            //总的出账额度
            Long totalOutputAmount = 0l;
            paymentCurrencyAccount.setUserId(userId);
            paymentCurrencyAccount.setChannelId(channelId);
            paymentCurrencyAccount.setName(name);
            paymentCurrencyAccount.setCurrentAmount(currentAmount);
            paymentCurrencyAccount.setFreezeAmount(freezeAmount);
            paymentCurrencyAccount.setTotalIntoAmount(totalIntoAmount);
            paymentCurrencyAccount.setTotalOutputAmount(totalOutputAmount);

            //货币类型
            Integer currencyType = CurrencyTypeEnum.CURRENCY_TYPE_VIP.getKey();
            paymentCurrencyAccount.setCurrencyType(currencyType);
            dataAccessManager.addPaymentCurrencyAccount(paymentCurrencyAccount);
        }
    }

    @Override
    public JSONObject addPaymentCurrencyAccount() {
        PaymentCurrencyAccount paymentCurrencyAccount = new PaymentCurrencyAccount();
        //用户ID
        String userId = getUTF("userId", "0");
        //渠道ID(用于计算归属)
        String channelId = getUTF("channelId", "0");
        //货币类型
        Integer currencyType = getIntParameter("currencyType", 0);
        //货币名字
        String name = getUTF("name", null);
        //当前可用额度
        Long currentAmount = getLongParameter("currentAmount", 0);
        //冻结额度
        Long freezeAmount = getLongParameter("freezeAmount", 0);
        //总的入账额度
        Long totalIntoAmount = getLongParameter("totalIntoAmount", 0);
        //总的出账额度
        Long totalOutputAmount = getLongParameter("totalOutputAmount", 0);
        paymentCurrencyAccount.setUserId(userId);
        paymentCurrencyAccount.setChannelId(channelId);
        paymentCurrencyAccount.setCurrencyType(currencyType);
        paymentCurrencyAccount.setName(name);
        paymentCurrencyAccount.setCurrentAmount(currentAmount);
        paymentCurrencyAccount.setFreezeAmount(freezeAmount);
        paymentCurrencyAccount.setTotalIntoAmount(totalIntoAmount);
        paymentCurrencyAccount.setTotalOutputAmount(totalOutputAmount);
        dataAccessManager.addPaymentCurrencyAccount(paymentCurrencyAccount);
        return success();
    }

    @Override
    public JSONObject getPaymentCurrencyAccount() {
        PaymentCurrencyAccount paymentCurrencyAccount = dataAccessManager.getPaymentCurrencyAccount(getId());
        fullUserMsg(paymentCurrencyAccount);
        return success(paymentCurrencyAccount);
    }

    @Override
    public JSONObject getPaymentCurrencyAccounts() {
        String phoneNum = getUTF("phoneNum", null);
        Integer currencyType = getIntParameter("currencyType", -1);//默认全部
        Long min = getLongParameter("min",0);
        Long max = getLongParameter("max",0);

        Map<String,Object> map = new HashMap<>();
        if(min != 0 || max != 0){ //默认为0 只要一个不是0 都给去查询
            map.put("min",min);
            map.put("max",max);
        }else{
            map.put("min",null);
            map.put("max",null);
        }

        //根据号码 查找对应的id
        List<String> userIds = null;
        if(phoneNum != null && !phoneNum.equals("")){
            List<SysUser> phoneNum1 = SysUserRemoteService.getSysUserByPhoneNumLike(phoneNum, getHttpServletRequest());
            if(phoneNum1 != null && phoneNum1.size() > 0){
                userIds = phoneNum1.stream().map(SysUser::getId).collect(Collectors.toList());
            }
        }
        map.put("userIds",userIds);
        map.put("currencyType",currencyType);
        List<PaymentCurrencyAccount> paymentCurrencyAccounts = dataAccessManager.getPaymentCurrencyAccounts(getPageStartIndex(getPageSize()), getPageSize(),map);
        int count = 0;
        if(paymentCurrencyAccounts != null && paymentCurrencyAccounts.size() > 0){
            fullListUserMsg(paymentCurrencyAccounts);
            count = dataAccessManager.getPaymentCurrencyAccountCount(map);
        }
        return success(paymentCurrencyAccounts, count);
    }

    @Override
    public JSONObject getPaymentCurrencyAccountAll() {
        List<PaymentCurrencyAccount> paymentCurrencyAccounts = dataAccessManager.getPaymentCurrencyAccountAll();
        fullListUserMsg(paymentCurrencyAccounts);
        return success(paymentCurrencyAccounts);
    }

    @Override
    public JSONObject getPaymentCurrencyAccountCount() {
        String keyWord = getUTF("keyWord", null);
//        int count = dataAccessManager.getPaymentCurrencyAccountCount(keyWord);
        return success();
    }

    /**
     * 填充创建人名字
     * @param paymentCurrencyAccounts
     */
    private void fullUserMsg(PaymentCurrencyAccount paymentCurrencyAccounts){
        try {
            SysUser sysUser = SysUserRmoteService.getSysUser(paymentCurrencyAccounts.getUserId(), getHttpServletRequest());
            if (sysUser != null) {
                if (sysUser.getRealName() != null) {
                    paymentCurrencyAccounts.setUserName(sysUser.getRealName());
                } else if (sysUser.getShowName() != null) {
                    paymentCurrencyAccounts.setUserName(sysUser.getShowName());
                } else {
                    paymentCurrencyAccounts.setUserName(sysUser.getDefaultName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 填充创建人名字
     * @param paymentCurrencyAccounts
     */
    private void fullListUserMsg(List<PaymentCurrencyAccount> paymentCurrencyAccounts){
        int size = paymentCurrencyAccounts.size();
        for (int i = 0; i < size; i++) {
            try {
                SysUser sysUser = SysUserRmoteService.getSysUser(paymentCurrencyAccounts.get(i).getUserId(), getHttpServletRequest());
                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        paymentCurrencyAccounts.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        paymentCurrencyAccounts.get(i).setUserName(sysUser.getShowName());
                    } else {
                        paymentCurrencyAccounts.get(i).setUserName(sysUser.getDefaultName());
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    @Override
    public JSONObject changePaymentCurrencyAccount() {
        PaymentCurrencyAccount paymentCurrencyAccount = dataAccessManager.getPaymentCurrencyAccount(getId());
        //渠道ID(用于计算归属)
        String channelId = getUTF("channelId", "0");
        //货币类型
        Integer currencyType = getIntParameter("currencyType", 0);
        //货币名字
        String name = getUTF("name", null);
        //当前可用额度
        Long currentAmount = getLongParameter("currentAmount", 0);
        //冻结额度
        Long freezeAmount = getLongParameter("freezeAmount", 0);
        //总的入账额度
        Long totalIntoAmount = getLongParameter("totalIntoAmount", 0);
        //总的出账额度
        Long totalOutputAmount = getLongParameter("totalOutputAmount", 0);
        paymentCurrencyAccount.setChannelId(channelId);
        paymentCurrencyAccount.setCurrencyType(currencyType);
        paymentCurrencyAccount.setName(name);
        paymentCurrencyAccount.setCurrentAmount(currentAmount);
        paymentCurrencyAccount.setFreezeAmount(freezeAmount);
        paymentCurrencyAccount.setTotalIntoAmount(totalIntoAmount);
        paymentCurrencyAccount.setTotalOutputAmount(totalOutputAmount);
        dataAccessManager.changePaymentCurrencyAccount(paymentCurrencyAccount);
        return success();
    }

    /**
     * 根据当前用户 获取用户余额
     * @return
     */
    @Override
    public JSONObject getPaymentCurrencyAccountByUserId() {
        SysUser user = null;
        try {
            user = getUser();//获取当前用户
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user == null){
            return fail(1001,"用户不能为空");
        }

        String terminal = getUTF("terminal");
        if(terminal == null || terminal.equals("")){
            return fail("终端不能为空");
        }

        SupplychainShopProperties supplychainShopPropertiesByUserId = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(user.getId(), getHttpServletRequest());


        JSONObject jsonObject = new JSONObject();
        if(supplychainShopPropertiesByUserId != null){
            jsonObject.put("shopName",supplychainShopPropertiesByUserId.getShopName());
        }

        //根据当前用户查找当前的用户余额
        PaymentCurrencyAccount paymentCurrencyAccountByUserId = dataAccessManager.getPaymentCurrencyAccountByUserId(user.getId(), CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());

        //今日收入统计
        Long todayTotalPaymentCurrencyOffsetLoggerByUserId =
        paymentCurrencyOffsetLoggerService.getTodayTotalPaymentCurrencyOffsetLoggerByUserId(user.getId(), terminal ,RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey());

        //本月收入统计
        Long monthTotalPaymentCurrencyOffsetLoggerByUserId =
                paymentCurrencyOffsetLoggerService.getMonthTotalPaymentCurrencyOffsetLoggerByUserId(user.getId(),terminal, RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey());

        if(paymentCurrencyAccountByUserId != null){
            jsonObject.put("paymentCurrencyAccount",paymentCurrencyAccountByUserId.getCurrentAmount());//传入余额
        }else{
            jsonObject.put("paymentCurrencyAccount",0);//传入余额
        }
        if (todayTotalPaymentCurrencyOffsetLoggerByUserId != null) {
            jsonObject.put("todayPaymentCurrencyAccount",todayTotalPaymentCurrencyOffsetLoggerByUserId);//传入今日收入
        }else{
            jsonObject.put("todayPaymentCurrencyAccount",0);//传入今日收入
        }
        if(monthTotalPaymentCurrencyOffsetLoggerByUserId != null){
            jsonObject.put("monthPaymentCurrencyAccount", monthTotalPaymentCurrencyOffsetLoggerByUserId);//传入本月收入
        }else{
            jsonObject.put("monthPaymentCurrencyAccount", 0);//传入本月收入
        }
        return success(jsonObject);
    }


    @Override
    public JSONObject getPaymentCurrencyAccountBalance() {
        SysUser user = null;
        try {
            user = getUser();//获取当前用户
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user == null){
            return fail(1001,"用户不能为空");
        }
        //根据当前用户查找当前的用户余额
        PaymentCurrencyAccount paymentCurrencyAccount = dataAccessManager.getPaymentCurrencyAccountByUserId(user.getId(), CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());
        JSONObject jsonObject = new JSONObject();
        if(paymentCurrencyAccount != null){
            jsonObject.put("currentAmount",paymentCurrencyAccount.getCurrentAmount());//总余额
            jsonObject.put("totalIntoAmount",paymentCurrencyAccount.getTotalIntoAmount());//总入账额度
            jsonObject.put("freezeAmount",paymentCurrencyAccount.getFreezeAmount());//总未到账额度
        }
        return success(jsonObject);
    }

    @Override
    public JSONObject getPaymentCurrencyAccountByCurrencyTypeEnum() {
        SysUser user = null;
        try {
            user = getUser();//获取当前用户
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user == null){
            return fail(1001,"用户不能为空");
        }
        PaymentCurrencyAccount paymentCurrencyAccount = dataAccessManager.getPaymentCurrencyAccountByUserId(user.getId(), CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());//余额
        PaymentCurrencyAccount paymentCurrencyAccount1 = dataAccessManager.getPaymentCurrencyAccountByUserId(user.getId(), CurrencyTypeEnum.CURRENCY_TYPE_VIP.getKey());//会员余额
        JSONObject jsonObject = new JSONObject();
        if(paymentCurrencyAccount != null){
            jsonObject.put("balance",paymentCurrencyAccount.getCurrentAmount());
        }else{
            jsonObject.put("balance",0);

        }
        if(paymentCurrencyAccount1 != null){
            jsonObject.put("vipBalance",paymentCurrencyAccount1.getCurrentAmount());
        }else {
            jsonObject.put("vipBalance",0);

        }
        return success(jsonObject);
    }

    @Override
    public JSONObject getPaymentCurrencyAccountBalanceByUserid() {
        SysUser user = null;
        try {
            user = getUser();//获取当前用户
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user == null){
            return fail(1001,"用户不能为空");
        }
        //根据当前用户查找当前的用户余额
        PaymentCurrencyAccount paymentCurrencyAccount = dataAccessManager.getPaymentCurrencyAccountByUserId(user.getId(), CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());
        JSONObject jsonObject = new JSONObject();
        if(paymentCurrencyAccount != null){
            if(paymentCurrencyAccount.getCurrentAmount() != null){
                jsonObject.put("currentAmount",paymentCurrencyAccount.getCurrentAmount());//总余额
            }else{
                jsonObject.put("currentAmount",0);
            }
        }
        return success(jsonObject);
    }

    /**
     * 订单余额支付
     * @author sqd
     * @date 2019/4/28
     * @param
     * @return
     */
    @Override
    @Transactional
    public JSONObject paymentOrderBalance() {
        String orderNumber = getUTF("orderNumber");
        String payPassword = getUTF("payPassword");
        SysUser user = null;
        try {
            user = getUser();//获取当前用户
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user == null){
            return fail(1001,"用户不能为空");
        }
        //根据当前用户查找当前的用户余额
        PaymentCurrencyAccount paymentCurrencyAccount =
                dataAccessManager.getPaymentCurrencyAccountByUserId(user.getId(), CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());
        //获取用户支付密码
        PaymentCurrencyProperties paymentCurrencyProperties =
                dataAccessManager.getPaymentCurrencyPropertiesByUserId(user.getId(), PaymentCurrentPropertiesTypeEnum.PAYMENT_CURRENT_PROPERTIES_TYPE_PAY.getKey());

        //获取订单
        List<OrderEntry> orderEntry = OrderEntryRemoteService.getOrderEntryByOrderNumber(orderNumber, getHttpServletRequest());
        if(orderEntry == null || orderEntry.size()<= 0){
            return  fail("没有可购买商品");
        }
        if(orderEntry == null){
            return  fail("未找到订单数据");
        }
        if(paymentCurrencyProperties ==null){
            return  fail("用户未设置支付密码");
        }
        if(paymentCurrencyAccount ==null){
            return  fail("用户余额为空");
        }

        //验证密码
        if(encryptionPassword(payPassword).equals(paymentCurrencyProperties.getV())){
            if(orderEntry.get(0).getTotalPrice()<=paymentCurrencyAccount.getCurrentAmount()){
                //支付成功
                Long currentAmount = paymentCurrencyAccount.getCurrentAmount();
                Long actualPrice = orderEntry.get(0).getTotalPrice();
                //Long money =  currentAmount-actualPrice;
                dataAccessManager.changePaymentCurrencyAccountWithWithdrawl(user.getId(),actualPrice,CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());
                //修改订单状态、添加支付记录;
                OrderEntryRemoteService.payCallback(orderNumber,actualPrice+"",getHttpServletRequest());

            }else{
                return  fail("用户余额不足");
            }
        }else{
            return  fail("支付密码错误");
        }
        return success();
    }

    @Value("${appId}")
    private String value;

    /**
     * 商家端余额充值
     * @author sqd
     * @date 2019/5/2
     * @param
     * @return
     */
    @Override
    @Transactional
    public JSONObject balanceRecharge(){
        long money = getLongParameter("money");
        SysUser user = null;
        try {
            user = getUser();//获取当前用户
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user == null){
            return fail(1001,"用户不能为空");
        }
        if(money <= 0){
            return  fail("充值金额有误");
        }

        SysUser sysUser = new SysUser();
        try {
            sysUser = SysUserRemoteService.getSysUser(user.getId(), getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String remoteAddr = getHttpServletRequest().getRemoteAddr();//payOrderEntry.getActualPrice()
        String tp = uniquePrimaryKey("TP");
        Map<String, String> orderBySmallProgram = WeiXinPayUtils.createOrderBySmallProgram(tp, money + "", remoteAddr, sysUser.getOpenId(), "账户充值", WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey(), WeixinCallcackType.BUSINESS_SMALL_PROGRAM_RECHARGE.getKey(),null);

        PaymentTransactionLogger payLogger = new PaymentTransactionLogger();
        payLogger.setUserId(user.getId());
        payLogger.setPaymentType("1");
        payLogger.setTransType(2);
        payLogger.setTransSequenceNumber(tp);
        payLogger.setAccountNumber(user.getPhoneNumber());
        payLogger.setAccountName(user.getShowName());
        payLogger.setCurrencyType("RMB");
        payLogger.setTransCreateTime(new Date());
        payLogger.setTransUnitAmount(money);
        payLogger.setAppId(value);
        payLogger.setTransStatus(4);
        dataAccessManager.addPaymentTransactionLogger(payLogger);//添加记录
        logger.info("充值成功  添加了记录+++++");
        return success(orderBySmallProgram);
    }


    @Transactional
    public  JSONObject recChangeCallback(){
        String mapJson = getUTF("json");

        SortedMap map = JSONObject.parseObject(mapJson,SortedMap.class);
        String cash_fee1 = (String)map.get("cash_fee");
        long cash_fee = Long.parseLong(cash_fee1);//支付金额
        String fee_type = (String) map.get("fee_type");//支付币种
        String out_trade_no = (String) map.get("out_trade_no");//支付单号

        //查找充值记录 修改回调的
        logger.info("充值回调进来 ---------------------------------------------" +out_trade_no);
        PaymentTransactionLogger paymentTransactionLogger = dataAccessManager.getPaymentTransactionLoggerByOutTradeNo(out_trade_no);

        if(paymentTransactionLogger == null){
            logger.error("找不到该充值记录！！！！管理员查看异常！！！！");
            return success();
        }

        String userId = paymentTransactionLogger.getUserId();//用户账号
        Integer transStatus = paymentTransactionLogger.getTransStatus();
        if(transStatus != null && transStatus == 8){//说明微信支付已经成功支付充值余额了
            logger.error("回调进来，余额已经充值好了，不用新增了。");
            return success();
        }
        //修改用户余额
        dataAccessManager.changePaymentCurrencyAccountWithWithdrawlAdd(userId,cash_fee,0);//充值账户余额 会员余额
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("transStatus",8);
        paramMap.put("currencyType",fee_type);
        paramMap.put("transSequenceNumber",paymentTransactionLogger.getTransSequenceNumber());
        dataAccessManager.changePaymentTransactionLoggerByMap(paramMap);//修改充值记录
        return success();
    }

    /**
     * 第一时间查询订单，给用户实时显示金额
     * @author oy
     * @date 2019/6/1
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject queryOrderForWeiPay() {
        String out_order_no = getUTF("out_order_no");
        //查询订单是否有
        Map map = WeiXinPayUtils.queryPayStatus(out_order_no, WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey());
        if(map != null && !map.isEmpty()){
            String trade_state = (String)map.get("trade_state");
            String result_code = (String)map.get("result_code");
            String return_code = (String)map.get("return_code");
            String total_fee = (String)map.get("total_fee");
            String fee_type = (String) map.get("fee_type");//支付币种
            if(trade_state.equals("SUCCESS") && result_code.equals("SUCCESS") && return_code.equals("SUCCESS")){
                //说明支付成功
                PaymentTransactionLogger paymentTransactionLogger = dataAccessManager.getPaymentTransactionLoggerByOutTradeNo(out_order_no);

                if(paymentTransactionLogger == null){
                    logger.error("找不到该充值记录！！！！管理员查看异常！！！！");
                    return success();
                }

                String userId = paymentTransactionLogger.getUserId();//用户账号
                Integer transStatus = paymentTransactionLogger.getTransStatus();
                if(transStatus != null && transStatus == 8){//说明微信支付已经成功支付充值余额了
                    logger.info("回调进来，余额已经充值好了，不用新增了。");
                    return success();
                }
                //修改用户余额
                long money = Long.parseLong(total_fee);
                dataAccessManager.changePaymentCurrencyAccountWithWithdrawlAdd(userId,money,0);//充值账户余额 会员余额
                Map<String,Object> paramMap = new HashMap<>();
                paramMap.put("transStatus",8);
                paramMap.put("currencyType",fee_type);
                paramMap.put("transSequenceNumber",paymentTransactionLogger.getTransSequenceNumber());
                dataAccessManager.changePaymentTransactionLoggerByMap(paramMap);//修改充值记录
                return success();
            }
        }
        return success();
    }

    @Override
    public JSONObject getPaymentCurrencyAccountBalanceByid() {
        String userId = getUTF("userId");
        PaymentCurrencyAccount paymentCurrencyAccountByUserId = dataAccessManager.getPaymentCurrencyAccountByUserId(userId, 0);
        return success(paymentCurrencyAccountByUserId);
    }

    @Override
    public JSONObject changePaymentCurrencyAccountJson() {
        String paymentCurrencyAccount = getUTF("paymentCurrencyAccount");
        PaymentCurrencyAccount paymentCurrencyAccount1 = JSONObject.parseObject(paymentCurrencyAccount, PaymentCurrencyAccount.class);
        dataAccessManager.changePaymentCurrencyAccount(paymentCurrencyAccount1);
        return success();
    }
}
