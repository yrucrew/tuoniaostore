package com.tuoniaostore.payment.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.payment.CurrencyTypeEnum;
import com.tuoniaostore.commons.constant.payment.RelationTransTypeEnum;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyAccount;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyOffsetLogger;
import com.tuoniaostore.datamodel.payment.PaymentEstimateRevenueLogger;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.vo.payment.PaymentCurrencyOffsetLoggerVO;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.service.PaymentCurrencyOffsetLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;


@Service("paymentCurrencyOffsetLoggerService")
public class PaymentCurrencyOffsetLoggerServiceImpl extends BasicWebService implements PaymentCurrencyOffsetLoggerService {
    @Autowired
    private PaymentDataAccessManager dataAccessManager;

    @Override
    public JSONObject addPaymentCurrencyOffsetLogger() {
        PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger = new PaymentCurrencyOffsetLogger();
        //用户ID
        String userId = getUTF("userId", "0");
        //渠道ID(用于计算归属)
        String channelId = getUTF("channelId", "0");
        //货币类型
        Integer currencyType = getIntParameter("currencyType", 0);
        //变化前的额度
        Long beforeAmount = getLongParameter("beforeAmount", 0);
        //偏移的额度
        Long offsetAmount = getLongParameter("offsetAmount", 0);
        //变化后的额度
        Long afterAmount = getLongParameter("afterAmount", 0);
        //交易的标题
        String transTitle = getUTF("transTitle", null);
        //交易的类型 如：支付宝、微信、银联等 具体字段内容由逻辑定义
        String transType = getUTF("transType", null);
        //关联的交易类型 如：提现、收入等
        Integer relationTransType = getIntParameter("relationTransType", 0);
        //关联的交易序号
        String relationTransSequence = getUTF("relationTransSequence", null);
        //1、完成(默认值) 4、逻辑删除
        Integer status = getIntParameter("status", 0);
        paymentCurrencyOffsetLogger.setUserId(userId);
        paymentCurrencyOffsetLogger.setChannelId(channelId);
        paymentCurrencyOffsetLogger.setCurrencyType(currencyType);
        paymentCurrencyOffsetLogger.setBeforeAmount(beforeAmount);
        paymentCurrencyOffsetLogger.setOffsetAmount(offsetAmount);
        paymentCurrencyOffsetLogger.setAfterAmount(afterAmount);
        paymentCurrencyOffsetLogger.setTransTitle(transTitle);
        paymentCurrencyOffsetLogger.setTransType(transType);
        paymentCurrencyOffsetLogger.setRelationTransType(relationTransType);
        paymentCurrencyOffsetLogger.setRelationTransSequence(relationTransSequence);
        paymentCurrencyOffsetLogger.setStatus(status);
        dataAccessManager.addPaymentCurrencyOffsetLogger(paymentCurrencyOffsetLogger);
        return success();
    }

    @Override
    public JSONObject getPaymentCurrencyOffsetLogger() {
        PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger = dataAccessManager.getPaymentCurrencyOffsetLogger(getId());
        fullUserMsg(paymentCurrencyOffsetLogger);
        return success(paymentCurrencyOffsetLogger);
    }

    @Override
    public JSONObject getPaymentCurrencyOffsetLoggers() {
        //String name = getUTF("name", null);

        String phoneNum = getUTF("phoneNum", null);
        Integer currencyType = getIntParameter("currencyType", -1);//默认全部
        Long min = getLongParameter("min",0);
        Long max = getLongParameter("max",0);

        Map<String,Object> map = new HashMap<>();
        if(min != 0 || max != 0){ //默认为0 只要一个不是0 都给去查询
            map.put("min",min);
            map.put("max",max);
        }else{
            map.put("min",null);
            map.put("max",null);
        }
        //根据号码 查找对应的id
        List<String> userIds = null;
        if(phoneNum != null && !phoneNum.equals("")){
            List<SysUser> phoneNum1 = SysUserRemoteService.getSysUserByPhoneNumLike("phoneNum", getHttpServletRequest());
            if(phoneNum1 != null && phoneNum1.size() > 0){
                userIds = phoneNum1.stream().map(SysUser::getId).collect(Collectors.toList());
            }
        }
        map.put("userIds",userIds);
        map.put("currencyType",currencyType);

        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        //List<PaymentCurrencyOffsetLogger> paymentCurrencyOffsetLoggers = dataAccessManager.getPaymentCurrencyOffsetLoggers( getPageStartIndex(getPageSize()), getPageSize(), name);
        List<PaymentCurrencyOffsetLogger> paymentCurrencyOffsetLoggers = dataAccessManager.getPaymentCurrencyOffsetLoggers( getPageStartIndex(getPageSize()), getPageSize(), map);
        fullListUserMsg(paymentCurrencyOffsetLoggers);
        //int count = dataAccessManager.getPaymentCurrencyOffsetLoggerCount( name);
        int count = dataAccessManager.getPaymentCurrencyOffsetLoggerCount( map);
        return success(paymentCurrencyOffsetLoggers, count);
    }

    @Override
    public JSONObject getPaymentCurrencyOffsetLoggerAll() {
        List<PaymentCurrencyOffsetLogger> paymentCurrencyOffsetLoggers = dataAccessManager.getPaymentCurrencyOffsetLoggerAll();
        fullListUserMsg(paymentCurrencyOffsetLoggers);
        return success(paymentCurrencyOffsetLoggers);
    }


    /**
     * 填充创建人名字
     * @param paymentCurrencyOffsetLogger
     */
    private void fullListUserMsg(List<PaymentCurrencyOffsetLogger> paymentCurrencyOffsetLogger){
        int size = paymentCurrencyOffsetLogger.size();
        for (int i = 0; i < size; i++) {
            try {
                SysUser sysUser = SysUserRmoteService.getSysUser(paymentCurrencyOffsetLogger.get(i).getUserId(), getHttpServletRequest());
                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        paymentCurrencyOffsetLogger.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        paymentCurrencyOffsetLogger.get(i).setUserName(sysUser.getShowName());
                    } else {
                        paymentCurrencyOffsetLogger.get(i).setUserName(sysUser.getDefaultName());
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    /**
     * 填充创建人名字
     * @param paymentCurrencyOffsetLogge
     */
    private void fullUserMsg(PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogge){
        try {
            SysUser sysUser = SysUserRmoteService.getSysUser(paymentCurrencyOffsetLogge.getUserId(), getHttpServletRequest());
            if (sysUser != null) {
                if (sysUser.getRealName() != null) {
                    paymentCurrencyOffsetLogge.setUserName(sysUser.getRealName());
                } else if (sysUser.getShowName() != null) {
                    paymentCurrencyOffsetLogge.setUserName(sysUser.getShowName());
                } else {
                    paymentCurrencyOffsetLogge.setUserName(sysUser.getDefaultName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public JSONObject getPaymentCurrencyOffsetLoggerCount() {
        String keyWord = getUTF("keyWord", null);
        int count = dataAccessManager.getPaymentCurrencyOffsetLoggerCountByParam(keyWord);
        return success();
    }

    @Override
    public JSONObject changePaymentCurrencyOffsetLogger() {
        PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger = dataAccessManager.getPaymentCurrencyOffsetLogger(getId());
        //用户ID
        String userId = getUTF("userId", "0");
        //渠道ID(用于计算归属)
        String channelId = getUTF("channelId", "0");
        //货币类型
        Integer currencyType = getIntParameter("currencyType", 0);
        //变化前的额度
        Long beforeAmount = getLongParameter("beforeAmount", 0);
        //偏移的额度
        Long offsetAmount = getLongParameter("offsetAmount", 0);
        //变化后的额度
        Long afterAmount = getLongParameter("afterAmount", 0);
        //交易的标题
        String transTitle = getUTF("transTitle", null);
        //交易的类型 如：支付宝、微信、银联等 具体字段内容由逻辑定义
        String transType = getUTF("transType", null);
        //关联的交易类型 如：提现、收入等
        Integer relationTransType = getIntParameter("relationTransType", 0);
        //关联的交易序号
        String relationTransSequence = getUTF("relationTransSequence", null);
        //1、完成(默认值) 4、逻辑删除
        Integer status = getIntParameter("status", 0);
        paymentCurrencyOffsetLogger.setUserId(userId);
        paymentCurrencyOffsetLogger.setChannelId(channelId);
        paymentCurrencyOffsetLogger.setCurrencyType(currencyType);
        paymentCurrencyOffsetLogger.setBeforeAmount(beforeAmount);
        paymentCurrencyOffsetLogger.setOffsetAmount(offsetAmount);
        paymentCurrencyOffsetLogger.setAfterAmount(afterAmount);
        paymentCurrencyOffsetLogger.setTransTitle(transTitle);
        paymentCurrencyOffsetLogger.setTransType(transType);
        paymentCurrencyOffsetLogger.setRelationTransType(relationTransType);
        paymentCurrencyOffsetLogger.setRelationTransSequence(relationTransSequence);
        paymentCurrencyOffsetLogger.setStatus(status);
        dataAccessManager.changePaymentCurrencyOffsetLogger(paymentCurrencyOffsetLogger);
        return success();
    }

    /**
     * 每日 支出记录 list
     * @return
     */
    @Override
    public JSONObject getDayPaymentCurrencyOffsetLoggers() {
        //获取当前终端
        String terminalId = getUTF("terminal");
        if(terminalId == null){
            return fail("终端id 不能为空！");
        }
        int pageNum = getIntParameter("pageNum",1);//页面大小
        int pageSize = getIntParameter("pageSize",10);//页码

        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageNum - 1);
        int limitEnd = pageSize;

        //获取当前用户id
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userId = user.getId();

        List<PaymentCurrencyOffsetLogger> day = dataAccessManager.getTodayPaymentCurrencyOffsetLoggerByUserId(userId,RelationTransTypeEnum.RelationTrans_TYPE_EXPENDITURE.getKey(), "day",limitStart,limitEnd);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("day",day);
        return success(jsonObject);
    }


    /**
     * 每月 支出记录 list
     * @return
     */
    @Override
    public JSONObject getMonthPaymentCurrencyOffsetLoggers() {
        //获取当前用户id
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userId = user.getId();

        //获取当前终端
        String terminalId = getUTF("terminal");
        if(terminalId == null){
            return fail("终端id 不能为空！");
        }

        int pageNum = getIntParameter("pageNum",1);//页面大小
        int pageSize = getIntParameter("pageSize",10);//页码

        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageNum - 1);
        int limitEnd = pageSize;

        List<PaymentCurrencyOffsetLogger> month = dataAccessManager.getTodayPaymentCurrencyOffsetLoggerByUserId(userId, RelationTransTypeEnum.RelationTrans_TYPE_EXPENDITURE.getKey(), "month",limitStart,limitEnd);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("month",month);
        return success(jsonObject);
    }

    /**
     * 每日 收入记录 list
     * @return
     */
    @Override
    public JSONObject getDayInPaymentCurrencyOffsetLoggers() {
        //获取当前用户id
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userId = user.getId();

        //获取当前终端
        String terminalId = getUTF("terminal");
        if(terminalId == null){
            return fail("终端id 不能为空！");
        }

        int pageNum = getIntParameter("pageNum",1);//页面大小
        int pageSize = getIntParameter("pageSize",10);//页码

        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageNum - 1);
        int limitEnd = pageSize;


        List<PaymentCurrencyOffsetLogger> day = dataAccessManager.getTodayPaymentCurrencyOffsetLoggerByUserId(userId, RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey(), "day",limitStart,limitEnd);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("dayIn",day);
        return success(jsonObject);
    }

    /**
     * 每月 收入记录 list
     * @return
     */
    @Override
    public JSONObject getMonthInPaymentCurrencyOffsetLoggers() {
        //获取当前用户id
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userId = user.getId();

        int pageNum = getIntParameter("pageNum",1);//页面大小
        int pageSize = getIntParameter("pageSize",10);//页码

        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageNum - 1);
        int limitEnd = pageSize;

        //获取当前终端
        String terminalId = getUTF("terminal");
        if(terminalId == null){
            return fail("终端id 不能为空！");
        }

        List<PaymentCurrencyOffsetLogger> month = dataAccessManager.getTodayPaymentCurrencyOffsetLoggerByUserId(userId, RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey(), "month",limitStart,limitEnd);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("monthIn",month);
        return success(jsonObject);
    }


    /**
     * 个人今日提现、收入统计
     * @param id
     * @param key
     * @return
     */
    @Override
    public Long getTodayTotalPaymentCurrencyOffsetLoggerByUserId(String id, String terminalId, int key) {
        List<PaymentCurrencyOffsetLogger> todayPaymentCurrencyOffsetLoggerByUserId = dataAccessManager.getTodayPaymentCurrencyOffsetLoggerByUserId(id, key,"day",null,null);
        long reduce = 0l;
        if(todayPaymentCurrencyOffsetLoggerByUserId != null && todayPaymentCurrencyOffsetLoggerByUserId.size() > 0){
            reduce = todayPaymentCurrencyOffsetLoggerByUserId.stream().mapToLong(PaymentCurrencyOffsetLogger::getOffsetAmount).sum();
        }
        return reduce;
    }


    /**
     * 个人本月提现、收入统计
     * @param id
     * @param key
     * @return
     */
    @Override
    public Long getMonthTotalPaymentCurrencyOffsetLoggerByUserId(String id,String terminalId, int key) {
        List<PaymentCurrencyOffsetLogger> month = dataAccessManager.getTodayPaymentCurrencyOffsetLoggerByUserId(id, key, "month",null,null);
        Long reduce = month.stream().mapToLong(PaymentCurrencyOffsetLogger::getOffsetAmount).sum();
        return reduce;
    }

    /**
     * 获取某一天的流水收入详情
     * @return
     */
    @Override
    public JSONObject getSelectDayIPaymentCurrencyOffsetLoggers() {
        //获取当前用户id
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userId = user.getId();

        int pageNum = getIntParameter("pageNum",1);//页面大小
        int pageSize = getIntParameter("pageSize",10);//页码

        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageNum - 1);
        int limitEnd = pageSize;

        //获取当前终端
        String terminalId = getUTF("terminal");
        if(terminalId == null){
            return fail("终端id 不能为空！");
        }
        String time = getUTF("time");//时间格式 String 2019-03-22
        if(time == null || time.equals("")){
            return fail("时间不能为空");
        }
        List<PaymentCurrencyOffsetLogger> day = dataAccessManager.getSelectTodayPaymentCurrencyOffsetLoggerByUserId(userId, RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey(), time,limitStart,limitEnd);
        //查询一下这一天的 一共多少条记录 多少钱
        Map<String,Object> map = dataAccessManager.getCountTodayPaymentCurrencyOffsetLoggerByUserId(userId, RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey(), time);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("day",day);
        //只有一个数据 count money
        if(map != null){
            Object count = map.get("count");
            if(count != null){
                jsonObject.put("count",count);
            }else{
                jsonObject.put("count",0);
            }
            Object money = map.get("money");
            if(money != null){
                jsonObject.put("money",money);
            }else{
                jsonObject.put("money",0);
            }
        }else{
            jsonObject.put("count",0);
            jsonObject.put("money",0);
        }
        return success(jsonObject);
    }


    /**
     * 余额明细
     * @return
     */
    @Override
    public JSONObject getPaymentCurrencyOffsetLoggersByParam() {
        SysUser user = null;
        //获取用户id
        try {
            user = getUser();
        }catch (Exception e){
            e.printStackTrace();
        }
        String userId = user.getId();
        //获取操作类型
        Integer type = getIntParameter("type");//搜索类型 0全部 1收入 2支出

        Map<String,List<PaymentCurrencyOffsetLogger>> map = new LinkedHashMap<>();//存放返回数据

        List<PaymentCurrencyOffsetLogger> list = new ArrayList<>();

        if(type == 0){
            //根据用户id查询 余额明细
            list = dataAccessManager.getPaymentCurrencyOffsetLoggersByParam(userId,-1);
        } else if (type == 1) {
            list = dataAccessManager.getPaymentCurrencyOffsetLoggersByParam(userId,RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey());
        } else if (type == 2) {
            list = dataAccessManager.getPaymentCurrencyOffsetLoggersByParam(userId,RelationTransTypeEnum.RelationTrans_TYPE_EXPENDITURE.getKey());
        } else {
            return fail("没有该类型!");
        }
        //查出来后 将时间 2019-03 作为键 然后里面放list Map<String,List<Object>>
        if (list != null && list.size() > 0) {//说明里面有数据
            //遍历list 存好返回格式map
            for (int i = 0; i < list.size(); i++) {
                PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger = list.get(i);
                if(paymentCurrencyOffsetLogger.getRelationTransType()!= RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey()){//判断非收入 则是 消费和提现 都是 负号
                    paymentCurrencyOffsetLogger.setOffsetAmount(-1 * paymentCurrencyOffsetLogger.getOffsetAmount());//变成负号
                }

                String format = DateUtils.format(paymentCurrencyOffsetLogger.getCreateTime(), DateUtils.FORMAT_SHORTS);//格式化时间 yyyy-MM-dd
                if(map.get(format) != null && map.get(format).size() > 0){//说明当前有键了 直接放进去就行了
                    map.get(format).add(paymentCurrencyOffsetLogger);//添加到已有的list 中
                }else{
                    List<PaymentCurrencyOffsetLogger> paymentCurrencyOffsetLoggers = new ArrayList<>();
                    paymentCurrencyOffsetLoggers.add(paymentCurrencyOffsetLogger);
                    map.put(format,paymentCurrencyOffsetLoggers);//添加到新的list 中
                }
            }
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("map",map);//Map<String,List<Object>>
        return success(jsonObject);
    }

    /***
     * 统计供应商的收入
     * @author oy
     * @date 2019/5/1
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getPaymentCurrencyOffsetLoggerTotalMoneyByUser() {
        String ids = getUTF("ids");
        Integer type = getIntParameter("type");
        if(ids != null && !ids.equals("")){
            List<String> userList = JSONObject.parseArray(ids, String.class);
            //查询一下当前的记录
            if(userList != null && userList.size() > 0){
                List<PaymentCurrencyOffsetLoggerVO> list = dataAccessManager.getPaymentCurrencyOffsetLoggerTotalMoneyByUser(userList,type);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("list",list);
                return jsonObject;
            }
        }
        return null;
    }

    @Override
    public JSONObject getPaymentCurrentOffsetLoggerByUserPage() {
        String userId = getUTF("userId");
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());

        if(userId == null || userId.equals("")){
            return fail("userId不能为空!");
        }
        List<PaymentCurrencyOffsetLogger> list
                = dataAccessManager.getTodayPaymentCurrencyOffsetLoggerByUserId(userId, RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey(), null, getPageStartIndex(getPageSize()), getPageSize());
        int count = dataAccessManager.getPaymentCurrencyPropertiesCount(status, name);
        return success(list,count);
    }

    @Override
    public JSONObject getPaymentCurrentOffsetLoggerAllByUserPage() {
        String userId = getUTF("userId");
        Integer currencyType = getIntParameter("currencyType");

        //交易类型
        Integer relationTransType = getIntParameter("relationTransType", -1);// RelationTransTypeEnum  提现、收入、消费(支出)
        //支付渠道
        Integer transType = getIntParameter("transType", -1);// PayTypeEnum 0-余额 1-微信 2-支付宝 3-银联 4-现金
        //交易金额 区间
        Long min = getLongParameter("min",0);
        Long max = getLongParameter("max",0);
        //起始时间 结束时间
        String startTime = getUTF("startTime", null);
        String endTime = getUTF("endTime", null);

        //通过用户id 获取对应的记录
        Map<String,Object> map = new HashMap<>();
        if(min != 0 || max != 0){ //默认为0 只要一个不是0 都给去查询
            map.put("min",min);
            map.put("max",max);
        }else{
            map.put("min",null);
            map.put("max",null);
        }
        map.put("transType",transType);
        map.put("relationTransType",relationTransType);
        map.put("startTime",startTime);
        map.put("endTime",endTime);
        map.put("currencyType",currencyType);

        int pageStartIndex = getPageStartIndex(getPageSize());
        int pageSize = getPageSize();

        map.put("pageStartIndex",pageStartIndex);
        map.put("pageSize",pageSize);

        //获取总条数
        List<PaymentCurrencyOffsetLogger> list = dataAccessManager.getPaymentCurrencyOffsetLoggersByUserAndParam(userId,map);
        //获取统计信息 当前余额 总支出 总收入 当前时间支出  当前时间收入
        PaymentCurrencyAccount paymentCurrencyAccountByUserId = dataAccessManager.getPaymentCurrencyAccountByUserId(userId, currencyType);
        long currentAmount = 0l;//0L
        if(paymentCurrencyAccountByUserId != null ){
             currentAmount = paymentCurrencyAccountByUserId.getCurrentAmount();
        }
        //总支出
        Map<String,Object> inOut = dataAccessManager.getPaymentCurrencyOffsetLoggerForInOut(userId,currencyType,null,null);
        BigDecimal totalIn = null;
        BigDecimal totalOut = null;

        if(inOut != null && !inOut.isEmpty()){
            totalIn = (BigDecimal)inOut.get("in");
            if(totalIn == null){
                totalIn = BigDecimal.ZERO;
            }
        }

        if(inOut != null && !inOut.isEmpty()){
            totalOut = (BigDecimal)inOut.get("out");
            if(totalOut == null){
                totalOut = BigDecimal.ZERO;
            }
        }

        //当前时间段
        Map<String,Object> inOutTime = dataAccessManager.getPaymentCurrencyOffsetLoggerForInOut(userId,currencyType,startTime,endTime);
        BigDecimal in = null;
        BigDecimal out = null;

        if(inOutTime != null && !inOutTime.isEmpty()){
            in = (BigDecimal)inOutTime.get("in");
            if(in == null){
                in = BigDecimal.ZERO;
            }
        }

        if(inOutTime != null && !inOutTime.isEmpty()){
            out = (BigDecimal)inOutTime.get("out");
            if(out == null){
                out = BigDecimal.ZERO;
            }
        }

        int count = 0;
        if(list != null && list.size() > 0){
            //计算总数
            fullListUserMsg(list);
            count = dataAccessManager.getCountPaymentCurrencyOffsetLoggersByUserAndParam(userId,map);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",list);
        jsonObject.put("count",count);

        jsonObject.put("totolMoney",currentAmount);

        jsonObject.put("totalIn",totalIn);
        jsonObject.put("totalOut",totalOut);
        jsonObject.put("in",in);
        jsonObject.put("out",out);
        return success(jsonObject);
    }

    @Override
    public JSONObject addPaymentCurrencyOffsetLoggerJson() {
        String paymentEstimateRevenueLogger = getUTF("paymentCurrencyOffsetLogger");
        if( !paymentEstimateRevenueLogger.equals("")){
            PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger = JSONObject.parseObject(paymentEstimateRevenueLogger, PaymentCurrencyOffsetLogger.class);
            dataAccessManager.addPaymentCurrencyOffsetLogger(paymentCurrencyOffsetLogger);
        }
        return success();
    }
}
