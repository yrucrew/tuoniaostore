package com.tuoniaostore.payment.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-08 10:21:40
 */
public interface PaymentEstimateRevenueLoggerService {

    JSONObject addPaymentEstimateRevenueLogger();

    JSONObject getPaymentEstimateRevenueLogger();

    JSONObject getPaymentEstimateRevenueLoggers();

    JSONObject getPaymentEstimateRevenueLoggerCount();
    JSONObject getPaymentEstimateRevenueLoggerAll();

    JSONObject changePaymentEstimateRevenueLogger();
    JSONObject addPaymentEstimateRevenueLoggerJson();

}
