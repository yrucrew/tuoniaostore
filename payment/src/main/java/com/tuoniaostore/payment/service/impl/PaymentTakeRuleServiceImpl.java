package com.tuoniaostore.payment.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.CommonUtils;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.datamodel.payment.PaymentTakeRule;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.service.PaymentTakeRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.Time;
import java.time.LocalTime;
import java.util.List;


@Service("paymentTakeRuleService")
public class PaymentTakeRuleServiceImpl extends BasicWebService implements PaymentTakeRuleService {
    @Autowired
    private PaymentDataAccessManager dataAccessManager;

    @Override
    public JSONObject addPaymentTakeRule() {
        PaymentTakeRule paymentTakeRule = new PaymentTakeRule();
        //提现类型1-商家 2-供应商 默认1
        fillPaymentTakeRule(paymentTakeRule);
        dataAccessManager.addPaymentTakeRule(paymentTakeRule);
        return success();
    }

    @Override
    public JSONObject getPaymentTakeRule() {
        PaymentTakeRule paymentTakeRule = dataAccessManager.getPaymentTakeRule(getId());
        return success(paymentTakeRule);
    }

    @Override
    public JSONObject getPaymentTakeRules() {
        String name = getUTF("name", null);
        List<PaymentTakeRule> paymentTakeRules = dataAccessManager.getPaymentTakeRules( getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getPaymentTakeRuleCount(name);
        return success(paymentTakeRules, count);
    }

    @Override
    public JSONObject getPaymentTakeRuleAll() {
        List<PaymentTakeRule> paymentTakeRules = dataAccessManager.getPaymentTakeRuleAll();
        return success(paymentTakeRules);
    }

    @Override
    public JSONObject getPaymentTakeRuleCount() {
        String keyWord = getUTF("keyWord", null);
        int count = dataAccessManager.getPaymentTakeRuleCount( keyWord);
        return success();
    }

    @Override
    public JSONObject changePaymentTakeRule() {
        PaymentTakeRule paymentTakeRule = dataAccessManager.getPaymentTakeRule(getId());
        //提现类型1-商家 2-供应商 默认1
        fillPaymentTakeRule(paymentTakeRule);
        dataAccessManager.changePaymentTakeRule(paymentTakeRule);
        return success();
    }

    @Override
    public JSONObject changeDefaultOption() {
        String id = getUTF("id");
        int option = getIntParameter("option");
        PaymentTakeRule paymentTakeRule = dataAccessManager.getPaymentTakeRule(id);
        if (paymentTakeRule == null) {
            return fail("记录不存在");
        }
        if (option==1){
            // 将该种提现类型的都设置成非默认
            dataAccessManager.changePaymentTakeRuleUnDefault(paymentTakeRule.getTakeMode(), 0);
            // 将该条规则设置成默认
        }
        paymentTakeRule.setDefaultOption(option);
        dataAccessManager.changePaymentTakeRule(paymentTakeRule);
        return success();
    }

    @Override
    public JSONObject delPaymentTakeRule() {
        String ids = getUTF("ids");
        String[] split = ids.split(",");
        dataAccessManager.delPaymentTakeRule(split);
        return success();
    }


    private void fillPaymentTakeRule(PaymentTakeRule paymentTakeRule) {

        Integer takeMode = getIntParameter("takeMode", 1);
        //展示在客户端的描述内容
        String takeDesc = getUTF("takeDesc", null);
        //应用的目标类型
        Integer targetType = getIntParameter("targetType", 0);
        //最低收费费用 单位：分
        Long lowCost = getLongParameter("lowCost", 0);
        //最高收费费用 单位：分
        Long highCost = getLongParameter("highCost", 0);
        //万份比
        Integer rate = getIntParameter("rate", 0);
        //单笔限制(上限) 单位：分
        Long singleAmountLimit = getLongParameter("singleAmountLimit", 0);
        //当天上限
        Long dayAmountLimit = getLongParameter("dayAmountLimit", 0);
        //每天最多提现次数
        Integer dayCountLimit = getIntParameter("dayCountLimit", 0);
        //遇节假日是否顺延 1 是 0 否
        Integer holidayDelay = getIntParameter("holidayDelay", 0);
        //如：48 为48小时内到账
        Integer delayHour = getIntParameter("delayHour", 0);
        //规则生效时间
        String ruleEffectTime = getUTF("ruleEffectTime", null);
        //规则失效时间
        String ruleInvalidTime = getUTF("ruleInvalidTime", null);
        //展示于前端的图片 不存在时 表示无需展示图片
        String showImage = getUTF("showImage", "");
        //是否为默认选项 1时是 0时否 一个目标类型只能有一种默认选项
        Integer defaultOption = getIntParameter("defaultOption", 0);
        //可提现时间(开始)(为空时不限制)
        String availableTimeBegin = getUTF("availableTimeBegin", null);
        //可提现时间(结束)(为空时不限制)
        String availableEndBegin = getUTF("availableEndBegin", null);
        //可提现星期, 将数字变为二进制形式, 最低位(最右)是星期日, 第七位(从右到左数)是星期一, 对应位为1的星期为可提现日(为空时不限制)
        Integer availableDate = getIntParameter("availableDate", 0);

        paymentTakeRule.setTakeMode(takeMode);
        paymentTakeRule.setTakeDesc(takeDesc);
        paymentTakeRule.setTargetType(targetType);
        paymentTakeRule.setLowCost(lowCost);
        paymentTakeRule.setHighCost(highCost);
        paymentTakeRule.setRate(rate);
        paymentTakeRule.setSingleAmountLimit(singleAmountLimit);
        paymentTakeRule.setDayAmountLimit(dayAmountLimit);
        paymentTakeRule.setDayCountLimit(dayCountLimit);
        paymentTakeRule.setHolidayDelay(holidayDelay);
        paymentTakeRule.setDelayHour(delayHour);
        paymentTakeRule.setRuleEffectTime(CommonUtils.strToDate(ruleEffectTime));
        paymentTakeRule.setRuleInvalidTime(CommonUtils.strToDate(ruleInvalidTime));
        paymentTakeRule.setShowImage(showImage);
        paymentTakeRule.setRetrieveStatus(0);
        if (defaultOption==1){
            dataAccessManager.changePaymentTakeRuleUnDefault(takeMode, 0);
            paymentTakeRule.setDefaultOption(defaultOption);
        }
        if (!StringUtils.isEmpty(availableTimeBegin)){
            paymentTakeRule.setAvailableTimeBegin(Time.valueOf(availableTimeBegin));
        }
        if (!StringUtils.isEmpty(availableEndBegin)) {
            paymentTakeRule.setAvailableEndBegin(Time.valueOf(availableEndBegin));
        }
        paymentTakeRule.setAvailableDate(availableDate);
    }

}
