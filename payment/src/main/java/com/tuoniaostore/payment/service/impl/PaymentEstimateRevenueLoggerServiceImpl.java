package com.tuoniaostore.payment.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.commons.support.order.OrderEntryRemoteService;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.payment.PaymentEstimateRevenueLogger;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.service.PaymentEstimateRevenueLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;

import static com.tuoniaostore.commons.utils.DateUtils.FORMAT_SHORT;


@Service("paymentEstimateRevenueLoggerService")
public class PaymentEstimateRevenueLoggerServiceImpl extends BasicWebService implements PaymentEstimateRevenueLoggerService {
    @Autowired
    private PaymentDataAccessManager dataAccessManager;

    @Override
    public JSONObject addPaymentEstimateRevenueLogger() {
        PaymentEstimateRevenueLogger paymentEstimateRevenueLogger = new PaymentEstimateRevenueLogger();
        //
        String userId = getUTF("userId");
        //订单号
        String orderId = getUTF("orderId");
        //订单金额
        Long orderAmount = getLongParameter("orderAmount", 0);
        //实际收入
        Long actualIncome = getLongParameter("actualIncome", 0);
        //收款账户
        String gatheringAccount = getUTF("gatheringAccount", null);
        paymentEstimateRevenueLogger.setUserId(userId);
        paymentEstimateRevenueLogger.setOrderId(orderId);
        paymentEstimateRevenueLogger.setOrderAmount(orderAmount);
        paymentEstimateRevenueLogger.setActualIncome(actualIncome);
        paymentEstimateRevenueLogger.setGatheringAccount(gatheringAccount);
        dataAccessManager.addPaymentEstimateRevenueLogger(paymentEstimateRevenueLogger);
        return success();
    }

    @Override
    public JSONObject getPaymentEstimateRevenueLogger() {
        PaymentEstimateRevenueLogger paymentEstimateRevenueLogger = dataAccessManager.getPaymentEstimateRevenueLogger(getId());
        return success(paymentEstimateRevenueLogger);
    }

    @SuppressWarnings("Duplicates")
    @Override
    public JSONObject getPaymentEstimateRevenueLoggers() {
//        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());

        String phoneNum = getUTF("phoneNum",null);//电话号码
        String orderNum = getUTF("orderNum",null);//订单号
        Long min = getLongParameter("min",0);//实际收入 小
        Long max = getLongParameter("max",0);//实际收入 大
        String createTime = getUTF("createTime",null);//创建时间

        Map<String,Object> map = new HashMap<>();

        List<String> ids = null;
        if(phoneNum != null && !phoneNum.equals("")){
            List<SysUser> sysUserByPhoneNumLike = SysUserRemoteService.getSysUserByPhoneNumLike(phoneNum, getHttpServletRequest());
            if(sysUserByPhoneNumLike != null && sysUserByPhoneNumLike.size() > 0){
                ids = sysUserByPhoneNumLike.stream().map(SysUser::getId).collect(Collectors.toList());
            }
        }
        map.put("userIds",ids);

        List<String> orderIds = null;
        if(orderNum != null && !orderNum.equals("")){
            List<OrderEntry> orderEntryByOrderNumber = OrderEntryRemoteService.getOrderEntryByOrderNumberAll(orderNum, getHttpServletRequest());
            if(orderEntryByOrderNumber != null && orderEntryByOrderNumber.size() > 0){
                orderIds = orderEntryByOrderNumber.stream().map(OrderEntry::getId).collect(Collectors.toList());
            }
        }
        map.put("orderIds",orderIds);

        //处理一下时间
        String createTimeStart = null;
        String createTimeEnd = null;
        if (createTime != null && !createTime.equals("")) {
            createTimeStart = dateDateFormateHandler(createTime, 0);
            createTimeEnd = dateDateFormateHandler(createTime, 1);
        }

        map.put("createTimeStart",createTimeStart);
        map.put("createTimeEnd",createTimeEnd);

        if(min != 0 || max != 0){
            map.put("min",min);
            map.put("max",max);
        }

        List<PaymentEstimateRevenueLogger> paymentEstimateRevenueLoggers = dataAccessManager.getPaymentEstimateRevenueLoggers(map,getPageStartIndex(getPageSize()), getPageSize());
        int count = 0;
        if(paymentEstimateRevenueLoggers != null && paymentEstimateRevenueLoggers.size() > 0){
            count = dataAccessManager.getPaymentEstimateRevenueLoggerCount(map);
            fillEntity(paymentEstimateRevenueLoggers);
        }
        return success(paymentEstimateRevenueLoggers, count);
    }


    /**
     * @param s, num
     * @return java.lang.String
     * @author oy
     * @date 2019/5/9
     */
    private String dateDateFormateHandler(String s, int num) {
        String[] split = s.split(" - ");
        String[] split1 = split[num].split("-");
        String s1 = split1[0];
        String s2 = split1[1];
        String s3 = split1[2];
        if (s2.length() == 1) {
            s2 = "0" + s2;
        }
        if (s3.length() == 1) {
            s3 = "0" + s3;
        }
        String data = s1 + "-" + s2 + "-" + s3;
        return data;
    }

    /**
     * 填充数据
     * @author oy
     * @date 2019/6/10
     * @param
     * @return java.util.List<com.tuoniaostore.datamodel.payment.PaymentEstimateRevenue>
     */
    public void fillEntity(List<PaymentEstimateRevenueLogger> paymentEstimateRevenueLoggers){
        int size = paymentEstimateRevenueLoggers.size();
        for (int i = 0; i < size; i++) {
            try {
                SysUser sysUser = SysUserRmoteService.getSysUser(paymentEstimateRevenueLoggers.get(i).getUserId(), getHttpServletRequest());
                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        paymentEstimateRevenueLoggers.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        paymentEstimateRevenueLoggers.get(i).setUserName(sysUser.getShowName());
                    } else {
                        paymentEstimateRevenueLoggers.get(i).setUserName(sysUser.getDefaultName());
                    }
                }
                paymentEstimateRevenueLoggers.get(i).setPhoneNum(sysUser.getPhoneNumber());
                //订单号
                String orderId = paymentEstimateRevenueLoggers.get(i).getOrderId();
                if(orderId != null && !orderId.equals("")){
                    OrderEntry orderEntry = OrderEntryRemoteService.getOrderEntryAndPosById(orderId,getHttpServletRequest());
                    if(orderEntry != null){
                        paymentEstimateRevenueLoggers.get(i).setOrderNumber(orderEntry.getOrderSequenceNumber());
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public JSONObject getPaymentEstimateRevenueLoggerAll() {
        List<PaymentEstimateRevenueLogger> paymentEstimateRevenueLoggers = dataAccessManager.getPaymentEstimateRevenueLoggerAll();
        return success(paymentEstimateRevenueLoggers);
    }

    @Override
    public JSONObject getPaymentEstimateRevenueLoggerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getPaymentEstimateRevenueLoggerCount(new HashMap<>());
        return success();
    }

    @Override
    public JSONObject changePaymentEstimateRevenueLogger() {
        PaymentEstimateRevenueLogger paymentEstimateRevenueLogger = dataAccessManager.getPaymentEstimateRevenueLogger(getId());
        //
        String userId = getUTF("userId");
        //订单号
        String orderId = getUTF("orderId");
        //订单金额
        Long orderAmount = getLongParameter("orderAmount", 0);
        //实际收入
        Long actualIncome = getLongParameter("actualIncome", 0);
        //收款账户
        String gatheringAccount = getUTF("gatheringAccount", null);
        paymentEstimateRevenueLogger.setUserId(userId);
        paymentEstimateRevenueLogger.setOrderId(orderId);
        paymentEstimateRevenueLogger.setOrderAmount(orderAmount);
        paymentEstimateRevenueLogger.setActualIncome(actualIncome);
        paymentEstimateRevenueLogger.setGatheringAccount(gatheringAccount);
        dataAccessManager.changePaymentEstimateRevenueLogger(paymentEstimateRevenueLogger);
        return success();
    }

    @Override
    public JSONObject addPaymentEstimateRevenueLoggerJson() {
        String paymentEstimateRevenueLogger = getUTF("paymentEstimateRevenueLogger");
        if( !paymentEstimateRevenueLogger.equals("")){
            PaymentEstimateRevenueLogger paymentEstimateRevenueLogger1 = JSONObject.parseObject(paymentEstimateRevenueLogger, PaymentEstimateRevenueLogger.class);
            dataAccessManager.addPaymentEstimateRevenueLogger(paymentEstimateRevenueLogger1);
        }
        return success();
    }

}
