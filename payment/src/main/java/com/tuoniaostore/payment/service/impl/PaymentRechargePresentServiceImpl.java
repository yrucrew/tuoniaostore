package com.tuoniaostore.payment.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.CommonUtils;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.payment.PaymentRechargePresent;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.service.PaymentRechargePresentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("paymentRechargePresentService")
public class PaymentRechargePresentServiceImpl extends BasicWebService implements PaymentRechargePresentService {
    @Autowired
    private PaymentDataAccessManager dataAccessManager;

    @Override
    public JSONObject addPaymentRechargePresent() {
        PaymentRechargePresent paymentRechargePresent = new PaymentRechargePresent();
        //最低充值额度
        Long minimumRechargeAmount = getLongParameter("minimumRechargeAmount", 0);
        //对应的货币类型
        Integer currencyType = getIntParameter("currencyType", 0);
        //赠送额度
        Long presentAmount = getLongParameter("presentAmount", 0);
        //介绍内容
        String instruction = getUTF("instruction", null);
        //生效时间
        String effectiveTime = getUTF("effectiveTime", null);
        //失效时间
        String failureTime = getUTF("failureTime", null);
        //当前状态 如：1、有效 0、无效
        Integer status = getIntParameter("status", 0);
        //赠送金额限制设置
        Long limits = getLongParameter("limits", 0);
        paymentRechargePresent.setMinimumRechargeAmount(minimumRechargeAmount);
        paymentRechargePresent.setCurrencyType(currencyType);
        paymentRechargePresent.setPresentAmount(presentAmount);
        paymentRechargePresent.setInstruction(instruction);
        paymentRechargePresent.setEffectiveTime(CommonUtils.strToDate(effectiveTime));
        paymentRechargePresent.setFailureTime(CommonUtils.strToDate(failureTime));
        paymentRechargePresent.setStatus(status);
        paymentRechargePresent.setLimits(limits);
        dataAccessManager.addPaymentRechargePresent(paymentRechargePresent);
        return success();
    }

    @Override
    public JSONObject getPaymentRechargePresent() {
        PaymentRechargePresent paymentRechargePresent = dataAccessManager.getPaymentRechargePresent(getId());
        return success(paymentRechargePresent);
    }

    @Override
    public JSONObject getPaymentRechargePresents() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<PaymentRechargePresent> paymentRechargePresents = dataAccessManager.getPaymentRechargePresents(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getPaymentRechargePresentCount(status, name);
        return success(paymentRechargePresents, count);
    }

    @Override
    public JSONObject getPaymentRechargePresentAll() {
        List<PaymentRechargePresent> paymentRechargePresents = dataAccessManager.getPaymentRechargePresentAll();
        return success(paymentRechargePresents);
    }

    @Override
    public JSONObject getPaymentRechargePresentCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getPaymentRechargePresentCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changePaymentRechargePresent() {
        PaymentRechargePresent paymentRechargePresent = dataAccessManager.getPaymentRechargePresent(getId());
        //最低充值额度
        Long minimumRechargeAmount = getLongParameter("minimumRechargeAmount", 0);
        //对应的货币类型
        Integer currencyType = getIntParameter("currencyType", 0);
        //赠送额度
        Long presentAmount = getLongParameter("presentAmount", 0);
        //介绍内容
        String instruction = getUTF("instruction", null);
        //生效时间
        String effectiveTime = getUTF("effectiveTime", null);
        //失效时间
        String failureTime = getUTF("failureTime", null);
        //当前状态 如：1、有效 0、无效
        Integer status = getIntParameter("status", 0);
        //赠送金额限制设置
        Long limits = getLongParameter("limits", 0);
        paymentRechargePresent.setMinimumRechargeAmount(minimumRechargeAmount);
        paymentRechargePresent.setCurrencyType(currencyType);
        paymentRechargePresent.setPresentAmount(presentAmount);
        paymentRechargePresent.setInstruction(instruction);
        paymentRechargePresent.setEffectiveTime(CommonUtils.strToDate(effectiveTime));
        paymentRechargePresent.setFailureTime(CommonUtils.strToDate(failureTime));
        paymentRechargePresent.setStatus(status);
        paymentRechargePresent.setLimits(limits);
        dataAccessManager.changePaymentRechargePresent(paymentRechargePresent);
        return success();
    }

}
