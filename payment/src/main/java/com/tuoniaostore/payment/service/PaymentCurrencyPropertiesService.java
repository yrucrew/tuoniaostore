package com.tuoniaostore.payment.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentCurrencyPropertiesService {

    JSONObject addPaymentCurrencyProperties();

    JSONObject getPaymentCurrencyProperties();

    JSONObject getPaymentCurrencyPropertiess();

    JSONObject getPaymentCurrencyPropertiesCount();
    JSONObject getPaymentCurrencyPropertiesAll();

    JSONObject changePaymentCurrencyProperties();

    /**
     * 修改支付密码 获取短信验证码
     * @return
     */
    JSONObject changePaymentPasswordMsg();

    /**
     * 重置支付密码
     * @return
     */
    JSONObject changePaymentPassword();

    /**
     * 重置付款密码 和上面差不多 但是表单有点不一样
     * @return
     */
    JSONObject changePaymentPassword2();

    /**
     * 获取用户支付密码
     * @author sqd
     * @date 2019/4/28
     * @param 
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getPaymentPasswordByUserId();
}
