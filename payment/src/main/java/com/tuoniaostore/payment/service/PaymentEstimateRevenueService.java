package com.tuoniaostore.payment.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-08 10:21:40
 */
public interface PaymentEstimateRevenueService {

    JSONObject addPaymentEstimateRevenue();
    JSONObject addPaymentEstimateRevenueJson();

    JSONObject getPaymentEstimateRevenue();

    JSONObject getPaymentEstimateRevenues();

    JSONObject getPaymentEstimateRevenueCount();
    JSONObject getPaymentEstimateRevenueAll();
    JSONObject changePaymentEstimateRevenueRequestTime();
    JSONObject changePaymentEstimateRevenueAccountingTime();
    JSONObject changePaymentEstimateRevenue();
    JSONObject addOrdereWithdrawal();

}
