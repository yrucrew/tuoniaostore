package com.tuoniaostore.payment.service;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyOffsetLogger;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentCurrencyOffsetLoggerService {

    JSONObject addPaymentCurrencyOffsetLogger();

    JSONObject getPaymentCurrencyOffsetLogger();

    JSONObject getPaymentCurrencyOffsetLoggers();

    JSONObject getPaymentCurrencyOffsetLoggerCount();
    JSONObject getPaymentCurrencyOffsetLoggerAll();

    JSONObject changePaymentCurrencyOffsetLogger();


    /**
     * 获取日流水记录 key：支出
     * @return
     */
    JSONObject getDayPaymentCurrencyOffsetLoggers();

    /**
     * 获取月流水记录 key：支出
     * @return
     */
    JSONObject getMonthPaymentCurrencyOffsetLoggers();

    /**
     * 获取日流水记录 key：收入
     * @return
     */
    JSONObject getDayInPaymentCurrencyOffsetLoggers();

    /**
     * 获取月流水记录 key：收入
     * @return
     */
    JSONObject getMonthInPaymentCurrencyOffsetLoggers();


    /**
     * 获取日流水统计
     * @return
     */
    Long getTodayTotalPaymentCurrencyOffsetLoggerByUserId(String id,String terminalId, int key);

    /**
     * 获取月流水统计
     * @return
     */
    Long getMonthTotalPaymentCurrencyOffsetLoggerByUserId(String id,String terminalId,  int key);


    /**
     * 获取具体某一天的流水收入
     * @return
     */
    JSONObject getSelectDayIPaymentCurrencyOffsetLoggers();

    /**
     * 余额明细
     * @return
     */
    JSONObject getPaymentCurrencyOffsetLoggersByParam();


    JSONObject getPaymentCurrencyOffsetLoggerTotalMoneyByUser();


    JSONObject getPaymentCurrentOffsetLoggerByUserPage();

    JSONObject getPaymentCurrentOffsetLoggerAllByUserPage();

    JSONObject addPaymentCurrencyOffsetLoggerJson();
}
