package com.tuoniaostore.payment.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentBankTemplateService {

    JSONObject addPaymentBankTemplate();

    JSONObject getPaymentBankTemplate();

    JSONObject getPaymentBankTemplates();

    JSONObject getPaymentBankTemplateCount();
    JSONObject getPaymentBankTemplateAll();

    JSONObject changePaymentBankTemplate();

    JSONObject deletePaymentBankTemplate();
}
