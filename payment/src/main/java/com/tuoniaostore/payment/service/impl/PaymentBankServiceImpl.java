package com.tuoniaostore.payment.service.impl;

import cn.jiguang.common.utils.StringUtils;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.datamodel.payment.PaymentBank;
import com.tuoniaostore.datamodel.payment.PaymentBankTemplate;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.payment.vo.PayBankVO;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.service.PaymentBankService;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.CollectionUtils;


@Service("paymentBankService")
public class PaymentBankServiceImpl extends BasicWebService implements PaymentBankService {
    @Autowired
    private PaymentDataAccessManager dataAccessManager;

    @Override
    public JSONObject addPaymentBank() {
        PaymentBank paymentBank = new PaymentBank();
        //通行证ID
        String userId = getUTF("userId", "0");
        //银行模版ID
        String bankTemplateId = getUTF("bankTemplateId", "0");
        //账户类型1、储值卡 2、信用卡
        Integer accountType = getIntParameter("accountType", 0);
        //银行帐号
        String bankAccount = getUTF("bankAccount", null);
        //持有者姓名
        String accountName = getUTF("accountName", null);
        //预留电话
        String reservePhone = getUTF("reservePhone", null);
        //支行名
        String branchName = getUTF("branchName", null);
        //状态 如：默认 1、失效 -1、普通 0等
        Integer status = getIntParameter("status", 0);
        paymentBank.setUserId(userId);
        paymentBank.setBankTemplateId(bankTemplateId);
        paymentBank.setAccountType(accountType);
        paymentBank.setBankAccount(bankAccount);
        paymentBank.setAccountName(accountName);
        paymentBank.setReservePhone(reservePhone);
        paymentBank.setBranchName(branchName);
        paymentBank.setStatus(status);
        dataAccessManager.addPaymentBank(paymentBank);
        return success();
    }

    @Override
    public JSONObject getPaymentBank() {
        PaymentBank paymentBank = dataAccessManager.getPaymentBank(getId());
        fullUserMsg(paymentBank);
        return success(paymentBank);
    }

    @Override
    public JSONObject getPaymentBanks() {
        //String name = getUTF("name", null);
        String bankTemplateId = getUTF("bankTemplateId", null);     //银行模版ID
        String bankAccount = getUTF("bankAccount", null);           //银行账户
        String accountName = getUTF("accountName", null);           //持有者姓名
        Integer accountType = getIntParameter("accountType",-1 );   //账户类型
        String reservePhone = getUTF("reservePhone", null);         //预留电话
        String branchName = getUTF("branchName", null);             //支行名
        Map<String, Object> map = new HashMap<>();
        map.put("bankTemplateId", bankTemplateId);
        map.put("bankAccount", bankAccount);
        map.put("accountName", accountName);
        map.put("accountType", accountType);
        map.put("reservePhone", reservePhone);
        map.put("branchName", branchName);

        String userName = getUTF("userName", null);
        if (!StringUtils.isEmpty(userName)) {
            List<SysUser> sysUserList = SysUserRemoteService.getSysUserByName(userName, getHttpServletRequest());
            if (CollectionUtils.isEmpty(sysUserList)){
                return success(new ArrayList(), 0);
            }
            List<String> userIds = sysUserList.stream().map(SysUser::getId).collect(Collectors.toList());
            map.put("userIds", userIds);
        }
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());     //状态
        //List<PaymentBank> paymentBanks = dataAccessManager.getPaymentBanks(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        List<PaymentBank> paymentBanks = dataAccessManager.getPaymentBanksByParam(map,status,getPageStartIndex(getPageSize()), getPageSize());
        fullListUserMsg(paymentBanks);
        //int count = dataAccessManager.getPaymentBankCount(status, name);
        int count = dataAccessManager.getPaymentBankCountByParam(status, map);
        return success(paymentBanks, count);
    }

    @Override
    public JSONObject getPaymentBankAll() {
        List<PaymentBank> paymentBanks = dataAccessManager.getPaymentBankAll();
        fullListUserMsg(paymentBanks);
        return success(paymentBanks);
    }

    /**
     * 填充创建人名字
     * @param paymentBanks
     */
    private void fullUserMsg(PaymentBank paymentBanks){
        try {
            SysUser sysUser = SysUserRmoteService.getSysUser(paymentBanks.getUserId(), getHttpServletRequest());
            if (sysUser != null) {
                if (sysUser.getRealName() != null) {
                    paymentBanks.setUserName(sysUser.getRealName());
                } else if (sysUser.getShowName() != null) {
                    paymentBanks.setUserName(sysUser.getShowName());
                } else {
                    paymentBanks.setUserName(sysUser.getDefaultName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 填充创建人名字
     * @param paymentBanks
     */
    private void fullListUserMsg(List<PaymentBank> paymentBanks){
        int size = paymentBanks.size();
        for (int i = 0; i < size; i++) {
            try {
                SysUser sysUser = SysUserRmoteService.getSysUser(paymentBanks.get(i).getUserId(), getHttpServletRequest());
                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        paymentBanks.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        paymentBanks.get(i).setUserName(sysUser.getShowName());
                    } else {
                        paymentBanks.get(i).setUserName(sysUser.getDefaultName());
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public JSONObject getPaymentBankCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getPaymentBankCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changePaymentBank() {
        PaymentBank paymentBank = dataAccessManager.getPaymentBank(getId());
        //通行证ID
        String userId = getUTF("userId", "0");
        //银行模版ID
        String bankTemplateId = getUTF("bankTemplateId", "0");
        //账户类型1、储值卡 2、信用卡
        Integer accountType = getIntParameter("accountType", 0);
        //银行帐号
        String bankAccount = getUTF("bankAccount", null);
        //持有者姓名
        String accountName = getUTF("accountName", null);
        //预留电话
        String reservePhone = getUTF("reservePhone", null);
        //支行名
        String branchName = getUTF("branchName", null);
        //状态 如：默认 1、失效 -1、普通 0等
        Integer status = getIntParameter("status", 0);
        paymentBank.setUserId(userId);
        paymentBank.setBankTemplateId(bankTemplateId);
        paymentBank.setAccountType(accountType);
        paymentBank.setBankAccount(bankAccount);
        paymentBank.setAccountName(accountName);
        paymentBank.setReservePhone(reservePhone);
        paymentBank.setBranchName(branchName);
        paymentBank.setStatus(status);
        dataAccessManager.changePaymentBank(paymentBank);
        return success();
    }

    /**
     * 获取用户银行卡
     * @return
     */
    @Override
    public JSONObject getPaymentBankByUserId() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //银行卡信息
        List<PaymentBank> paymentBank = dataAccessManager.getPaymentBankByUserId(user.getId());
        List<String> collect = paymentBank.stream().map(PaymentBank::getBankTemplateId).collect(Collectors.toList());//银行卡模板id
        List<String> collect1 = null;
        List<PayBankVO> list = new ArrayList<>();
        if (collect != null && collect.size() > 0) {
            collect1 = collect.stream().distinct().collect(Collectors.toList());//去重
            List<PaymentBankTemplate> paymentBankTemplateById = dataAccessManager.getPaymentBankTemplateById(collect1);
            //查询银行卡模板信息
            paymentBank.forEach(x ->{//便利银行卡 找出对应的模板id
                PayBankVO payBankVO = new PayBankVO();
                String bankTemplateId = x.getBankTemplateId();
                Optional<PaymentBankTemplate> first = paymentBankTemplateById.stream().filter(item -> item.getId().equals(bankTemplateId)).findFirst();
                if(first.isPresent()){//存在
                    //银行模板信息
                    PaymentBankTemplate paymentBankTemplate = first.get();
                    payBankVO.setBankCode(paymentBankTemplate.getBankCode());
                    payBankVO.setName(paymentBankTemplate.getName());
                    payBankVO.setSimpleCode(paymentBankTemplate.getSimpleCode());
                    payBankVO.setLogo(paymentBankTemplate.getLogo());
                }
                payBankVO.setUserId(x.getUserId());
                //银行信息
                payBankVO.setAccountName(x.getAccountName());
                payBankVO.setAccountType(x.getAccountType());
                //将中间账号 ****
                String bankAccount1 = x.getBankAccount();
                StringBuffer sb = new StringBuffer(bankAccount1);
                sb.replace(4,7,"****");
                payBankVO.setBankAccount(sb.toString());
                payBankVO.setId(x.getId());
                payBankVO.setBranchName(x.getBranchName());
                payBankVO.setCreateTime(x.getCreateTime());
                list.add(payBankVO);
            });
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("paymentBank",list);
        return success(jsonObject);
    }

    /**
     * 新增用户银行卡
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject addPaymentBankByUserId() {
        SysUser user = getUser();
        String userId = user.getId();//用户id
        //银行卡号
        String bankAccount = getUTF("bankAccount");
        //银行名称
        String branchName = getUTF("branchName");
        //持有者姓名
        String accountName = getUTF("accountName");
        //预留号码
        String reservePhone = getUTF("reservePhone");
        //模板id
        String bankTemplateId = getUTF("bankTemplateId");
        //账户类型1、储值卡 2、信用卡
        Integer accountType = getIntParameter("accountType",1);

        if(bankAccount == null || bankAccount.equals("")){
            return fail("银行卡号不能为空！");
        }
        if(branchName == null || branchName.equals("")){
            return fail("银行名称不能为空！");
        }
        if(accountName == null || accountName.equals("")){
            return fail("持有者姓名不能为空！");
        }
        if(reservePhone == null || reservePhone.equals("")){
            return fail("预留号码不能为空！");
        }
        if(bankTemplateId == null || bankTemplateId.equals("")){
            return fail("模板id不能为空！");
        }
        if(accountType == null || accountType.equals("")){
            return fail("账户类型不能为空！");
        }
        //根据模板id 获取模板信息
        PaymentBank paymentBank = new PaymentBank();
        PaymentBankTemplate paymentBankTemplate = dataAccessManager.getPaymentBankTemplate(bankTemplateId);
        if(paymentBankTemplate != null){
            String name = paymentBankTemplate.getName();
            paymentBank.setBranchName(name);
        }
        paymentBank.setStatus(1);//状态 如：默认 1、失效 -1、普通 0 等
        paymentBank.setUserId(userId);
        paymentBank.setBankTemplateId(bankTemplateId);//模板id
        paymentBank.setAccountType(accountType);//账号类型
        paymentBank.setAccountName(accountName);//持有者姓名
        paymentBank.setReservePhone(reservePhone);//预留号码
        paymentBank.setBankAccount(bankAccount);//账号
        dataAccessManager.addPaymentBank(paymentBank);
        return success("添加结算卡成功！");
    }

    @Override
    public JSONObject deletePaymentBank() {
        String[] idArr = getUTF("ids").split(",");
        if (ArrayUtils.isEmpty(idArr)) {
            return fail("参数为空");
        }
        dataAccessManager.deletePaymentBank(idArr);
        return success();
    }

}
