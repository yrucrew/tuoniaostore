package com.tuoniaostore.payment.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.commons.support.order.OrderEntryRemoteService;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.payment.PaymentEstimateRevenue;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.service.PaymentEstimateRevenueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;

import static com.tuoniaostore.commons.utils.DateUtils.FORMAT_SHORT;


@Service("paymentEstimateRevenueService")
public class PaymentEstimateRevenueServiceImpl extends BasicWebService implements PaymentEstimateRevenueService {
    @Autowired
    private PaymentDataAccessManager dataAccessManager;

    @Override
    public JSONObject addPaymentEstimateRevenue() {
        PaymentEstimateRevenue paymentEstimateRevenue = new PaymentEstimateRevenue();
        //
        String userId = getUTF("userId");
        //订单号
        String orderId = getUTF("orderId");
        //商家名称
        String shopsName = getUTF("shopsName", null);
        //订单金额
        Long orderAmount = getLongParameter("orderAmount", 0);
        //实际收入
        Long actualIncome = getLongParameter("actualIncome", 0);
        //状态 如：默认 1、请求中 2、已取消 3、已到账 4、已提现
        Integer status = getIntParameter("status", 0);
        //收款账户
        String gatheringAccount = getUTF("gatheringAccount", null);
        //结算方式: 0.按结算天数，1.按结算额度
        Long settlementType = getLongParameter("settlementType", 0);
        paymentEstimateRevenue.setUserId(userId);
        paymentEstimateRevenue.setOrderId(orderId);
        paymentEstimateRevenue.setShopsName(shopsName);
        paymentEstimateRevenue.setOrderAmount(orderAmount);
        paymentEstimateRevenue.setActualIncome(actualIncome);
        paymentEstimateRevenue.setStatus(status);
        paymentEstimateRevenue.setGatheringAccount(gatheringAccount);
        paymentEstimateRevenue.setSettlementType(settlementType);
        dataAccessManager.addPaymentEstimateRevenue(paymentEstimateRevenue);
        return success();
    }

    @Override
    public JSONObject getPaymentEstimateRevenue() {
        PaymentEstimateRevenue paymentEstimateRevenue = dataAccessManager.getPaymentEstimateRevenue(getId());
        return success(paymentEstimateRevenue);
    }

    @SuppressWarnings("Duplicates")
    @Override
    public JSONObject getPaymentEstimateRevenues() {
        String shopsName = getUTF("shopsName", null);
        String orderNumber = getUTF("orderNumber", null);
        String requestTime = getUTF("requestTime", null);
        String drawTime = getUTF("drawTime", null);
        String accountingTime = getUTF("accountingTime", null);
        String phoneNum = getUTF("phoneNum", null);

        //用户账号
        List<String> userIds = null;
        if(phoneNum != null && !phoneNum.equals("")){
            List<SysUser> sysUserByPhoneNumLike = SysUserRemoteService.getSysUserByPhoneNumLike(phoneNum, getHttpServletRequest());
            if(sysUserByPhoneNumLike != null && sysUserByPhoneNumLike.size() > 0){
                userIds = sysUserByPhoneNumLike.stream().map(SysUser::getId).collect(Collectors.toList());
            }
        }

        //订单号
        List<String> orderIds = null;
        if(orderNumber != null && !orderNumber.equals("")){
            List<OrderEntry> orderEntryByOrderNumber = OrderEntryRemoteService.getOrderEntryByOrderNumber(orderNumber, getHttpServletRequest());
            if(orderEntryByOrderNumber != null && orderEntryByOrderNumber.size() > 0){
                orderIds = orderEntryByOrderNumber.stream().map(OrderEntry::getId).collect(Collectors.toList());
            }
        }

        //处理一下时间
        String requestTimeStart = null;
        String requestTimeEnd = null;
        if (requestTime != null && !requestTime.equals("")) {
            requestTimeStart = dateDateFormateHandler(requestTime, 0);
            requestTimeEnd = dateDateFormateHandler(requestTime, 1);
            if (requestTimeStart.equals(requestTimeEnd)){
                requestTimeEnd = addOneDay(requestTimeEnd);
            }
        }

        String drawTimeStart = null;
        String drawTimeEnd = null;
        if (drawTime != null && !drawTime.equals("")) {
            drawTimeStart = dateDateFormateHandler(drawTime, 0);
            drawTimeEnd = dateDateFormateHandler(drawTime, 1);
            if(drawTimeStart.equals(drawTimeEnd)){
                drawTimeEnd = addOneDay(drawTimeEnd);
            }
        }

        String accountingTimeStart = null;
        String accountingTimeEnd = null;
        if (accountingTime != null && !accountingTime.equals("")) {
            accountingTimeStart = dateDateFormateHandler(accountingTime, 0);
            accountingTimeEnd = dateDateFormateHandler(accountingTime, 1);
            if (accountingTimeStart.equals(accountingTimeEnd)){
                accountingTimeEnd = addOneDay(accountingTimeEnd);
            }
        }

        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        Map<String,Object> map = new HashMap<>();
        map.put("shopsName",shopsName);
        map.put("orderIds",orderIds);
        map.put("requestTimeStart",requestTimeStart);
        map.put("requestTimeEnd",requestTimeEnd);
        map.put("drawTimeStart",drawTimeStart);
        map.put("drawTimeEnd",drawTimeEnd);
        map.put("accountingTimeStart",accountingTimeStart);
        map.put("accountingTimeEnd",accountingTimeEnd);
        map.put("userIds",userIds);

        List<PaymentEstimateRevenue> paymentEstimateRevenues = dataAccessManager.getPaymentEstimateRevenues(status, getPageStartIndex(getPageSize()), getPageSize(), map);
        int count = 0;
        if(paymentEstimateRevenues != null && paymentEstimateRevenues.size() > 0){
            fillEntity(paymentEstimateRevenues);
            count = dataAccessManager.getPaymentEstimateRevenueCount(status, map);
        }
        return success(paymentEstimateRevenues, count);
    }

    private String addOneDay(String drawTimeEnd) {
        Date parse = DateUtils.parse(drawTimeEnd, FORMAT_SHORT);
        Date date = DateUtils.addDay(parse, 1);
        return DateUtils.format(date, FORMAT_SHORT);
    }

    /**
     * @param s, num
     * @return java.lang.String
     * @author oy
     * @date 2019/5/9
     */
    private String dateDateFormateHandler(String s, int num) {
        String[] split = s.split(" - ");
        String[] split1 = split[num].split("-");
        String s1 = split1[0];
        String s2 = split1[1];
        String s3 = split1[2];
        if (s2.length() == 1) {
            s2 = "0" + s2;
        }
        if (s3.length() == 1) {
            s3 = "0" + s3;
        }
        String data = s1 + "-" + s2 + "-" + s3;
        return data;
    }

    /**
     * 填充数据
     * @author oy
     * @date 2019/6/10
     * @param
     * @return java.util.List<com.tuoniaostore.datamodel.payment.PaymentEstimateRevenue>
     */
    public void fillEntity(List<PaymentEstimateRevenue> paymentEstimateRevenues){
        int size = paymentEstimateRevenues.size();
        for (int i = 0; i < size; i++) {
            try {
                SysUser sysUser = SysUserRmoteService.getSysUser(paymentEstimateRevenues.get(i).getUserId(), getHttpServletRequest());
                if (sysUser != null) {
                    if (sysUser.getRealName() != null) {
                        paymentEstimateRevenues.get(i).setUserName(sysUser.getRealName());
                    } else if (sysUser.getShowName() != null) {
                        paymentEstimateRevenues.get(i).setUserName(sysUser.getShowName());
                    } else {
                        paymentEstimateRevenues.get(i).setUserName(sysUser.getDefaultName());
                    }
                }
                paymentEstimateRevenues.get(i).setPhoneNum(sysUser.getPhoneNumber());
                //订单号
                String orderId = paymentEstimateRevenues.get(i).getOrderId();
                if(orderId != null && !orderId.equals("")){
                    OrderEntry orderEntry = OrderEntryRemoteService.getOrderEntryAndPosById(orderId,getHttpServletRequest());
                    if(orderEntry != null){
                        paymentEstimateRevenues.get(i).setOrderNumber(orderEntry.getOrderSequenceNumber());
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public JSONObject getPaymentEstimateRevenueAll() {
        List<PaymentEstimateRevenue> paymentEstimateRevenues = dataAccessManager.getPaymentEstimateRevenueAll();
        return success(paymentEstimateRevenues);
    }

    @Override
    public JSONObject getPaymentEstimateRevenueCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
//        int count = dataAccessManager.getPaymentEstimateRevenueCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changePaymentEstimateRevenue() {
        PaymentEstimateRevenue paymentEstimateRevenue = dataAccessManager.getPaymentEstimateRevenue(getId());
        //状态 如：默认 1、请求中 2、已取消 3、已到账 4、已提现
        Integer status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        paymentEstimateRevenue.setStatus(status);
        dataAccessManager.changePaymentEstimateRevenue(paymentEstimateRevenue);
        return success();
    }

    @Override
    public JSONObject changePaymentEstimateRevenueRequestTime() {
        String id = getId();
        dataAccessManager.changePaymentEstimateRevenueRequestTime(id);
        return success();
    }

    @Override
    public JSONObject changePaymentEstimateRevenueAccountingTime() {
        String id = getId();
        dataAccessManager.changePaymentEstimateRevenueAccountingTime(id);
        return success();
    }

    @Override
    public JSONObject addPaymentEstimateRevenueJson() {
        String paymentEstimateRevenueJson = getUTF("paymentEstimateRevenue");
        PaymentEstimateRevenue paymentEstimateRevenue = JSONObject.parseObject(paymentEstimateRevenueJson, PaymentEstimateRevenue.class);
        try {
            dataAccessManager.addPaymentEstimateRevenue(paymentEstimateRevenue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success();
    }

    @Override
    public JSONObject addOrdereWithdrawal() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String rule = getUTF("rule"); //提现规则
        String creditedAccount = getUTF("creditedAccount"); //收款账户
        String orderNumber = getUTF("orderNumber"); //收款账户
        String shopName = getUTF("shopName"); //收款账户
        PaymentEstimateRevenue payRevenue = new PaymentEstimateRevenue();
        payRevenue.setUserId(user.getId());
        payRevenue.setOrderId(orderNumber);
        payRevenue.setShopsName(shopName);
        return null;
    }
}
