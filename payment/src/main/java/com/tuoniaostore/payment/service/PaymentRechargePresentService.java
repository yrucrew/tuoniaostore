package com.tuoniaostore.payment.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentRechargePresentService {

    JSONObject addPaymentRechargePresent();

    JSONObject getPaymentRechargePresent();

    JSONObject getPaymentRechargePresents();

    JSONObject getPaymentRechargePresentCount();
    JSONObject getPaymentRechargePresentAll();

    JSONObject changePaymentRechargePresent();
}
