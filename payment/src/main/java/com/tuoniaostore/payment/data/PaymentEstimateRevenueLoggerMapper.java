package com.tuoniaostore.payment.data;

import com.tuoniaostore.datamodel.payment.PaymentEstimateRevenueLogger;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-08 10:21:40
 */
@Component
@Mapper
public interface PaymentEstimateRevenueLoggerMapper {

    String column = "`user_id`, " +//
            "`order_id`, " +//订单号
            "`order_amount`, " +//订单金额
            "`actual_income`, " +//实际收入
            "`gathering_account`, " +//收款账户
            "`create_time`"//创建时间
            ;

    @Insert("insert into payment_estimate_revenue_logger (" +
            " `id`,  " +
            " `user_id`,  " +
            " `order_id`,  " +
            " `order_amount`,  " +
            " `actual_income`,  " +
            " `gathering_account` " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{orderId},  " +
            "#{orderAmount},  " +
            "#{actualIncome},  " +
            "#{gatheringAccount} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addPaymentEstimateRevenueLogger(PaymentEstimateRevenueLogger paymentEstimateRevenueLogger);

    @Results(id = "paymentEstimateRevenueLogger", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "orderId", column = "order_id"), @Result(property = "orderAmount", column = "order_amount"), @Result(property = "actualIncome", column = "actual_income"), @Result(property = "gatheringAccount", column = "gathering_account"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from payment_estimate_revenue_logger where id = #{id}")
    PaymentEstimateRevenueLogger getPaymentEstimateRevenueLogger(@Param("id") String id);

    @Select("<script>select  " + column + "  from payment_estimate_revenue_logger   where   1=1 " +
            "<if test=\" map.userIds != null and map.userIds.size() > 0\">and user_id in " +
            "<foreach collection=\"map.userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            "<if test=\" map.orderIds != null and map.orderIds.size() > 0\">and order_id in " +
            "<foreach collection=\"map.orderIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            "<if test=\"map.requestTimeBeg != null and map.requestTimeEnd != null \">and  create_time between #{map.requestTimeBeg} and #{map.requestTimeEnd}</if> " +
            "<if test=\"map.accountingTimeBeg != null and map.accountingTimeEnd != null \">and  create_time between #{map.accountingTimeBeg} and #{map.accountingTimeEnd}</if> " +
            "<if test=\"map.drawTimeBeg != null and map.drawTimeEnd != null \">and  create_time between #{map.drawTimeBeg} and #{map.drawTimeEnd}</if> " +
            "<if test=\"map.min != null and map.max != null \">and  actual_income between #{map.min} and #{map.max}</if> " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("paymentEstimateRevenueLogger")
    List<PaymentEstimateRevenueLogger> getPaymentEstimateRevenueLoggers(@Param("map") Map<String, Object> map, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize);

    @Select("<script>select  " + column + "  from payment_estimate_revenue_logger   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("paymentEstimateRevenueLogger")
    List<PaymentEstimateRevenueLogger> getPaymentEstimateRevenueLoggerAll();

    @Select("<script>select count(1) from payment_estimate_revenue_logger   where   1=1 " +
            "<if test=\" map.userIds != null and map.userIds.size() > 0\">and user_id in " +
            "   <foreach collection=\"map.userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}" +
            "</foreach></if> " +
            "<if test=\" map.orderIds != null and map.orderIds.size() > 0\">and order_id in " +
            "   <foreach collection=\"map.orderIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach>" +
            "</if> " +
            "<if test=\"map.requestTimeBeg != null and map.requestTimeEnd != null \">and  create_time between #{map.requestTimeBeg} and #{map.requestTimeEnd}</if> " +
            "<if test=\"map.accountingTimeBeg != null and map.accountingTimeEnd != null \">and  create_time between #{map.accountingTimeBeg} and #{map.accountingTimeEnd}</if> " +
            "<if test=\"map.drawTimeBeg != null and map.drawTimeEnd != null \">and  create_time between #{map.drawTimeBeg} and #{map.drawTimeEnd}</if> " +
            "<if test=\"map.min != null and map.max != null \">and  actual_income between #{map.min} and #{map.max}</if> " +
            "</script>")
    int getPaymentEstimateRevenueLoggerCount(@Param("map") Map<String, Object> map);

    @Update("update payment_estimate_revenue_logger  " +
            "set " +
            "`order_amount` = #{orderAmount}  , " +
            "`actual_income` = #{actualIncome}  , " +
            "`gathering_account` = #{gatheringAccount}  , " +
            " where id = #{id}")
    void changePaymentEstimateRevenueLogger(PaymentEstimateRevenueLogger paymentEstimateRevenueLogger);

}
