package com.tuoniaostore.payment.data;

import com.tuoniaostore.datamodel.payment.PaymentEstimateRevenue;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-08 10:21:40
 */
@Component
@Mapper
public interface PaymentEstimateRevenueMapper {

    String column = "`id`, " +
            "`user_id`, " +//
            "`order_id`, " +//订单号
            "`shops_name`, " +//商家名称
            "`order_amount`, " +//订单金额
            "`actual_income`, " +//实际收入
            "`status`, " +//状态 如：默认 1、发起提现 2、已取消 3、打钱中 4、已完提现
            "`request_time`, " +//请求时间
            "`gathering_account`, " +//收款账户
            "`settlement_type`, " +//结算方式: 0.按结算天数，1.按结算额度
            "`accounting_time`, " +//到账时间
            "`draw_time` " //提现时间
            ;

    @Insert("insert into payment_estimate_revenue (" +
            " `id`,  " +
            " `user_id`,  " +
            " `order_id`,  " +
            " `shops_name`,  " +
            " `order_amount`,  " +
            " `actual_income`,  " +
            " `status`,  " +
            " `request_time`,  " +
            " `gathering_account`,  " +
            " `settlement_type`,  " +
            " `accounting_time`,  " +
            " `draw_time` " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{orderId},  " +
            "#{shopsName},  " +
            "#{orderAmount},  " +
            "#{actualIncome},  " +
            "#{status},  " +
            "#{requestTime},  " +
            "#{gatheringAccount},  " +
            "#{settlementType},  " +
            "#{accountingTime},  " +
            "#{drawTime}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addPaymentEstimateRevenue(PaymentEstimateRevenue paymentEstimateRevenue);

    @Results(id = "paymentEstimateRevenue", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "orderId", column = "order_id"),
            @Result(property = "shopsName", column = "shops_name"),
            @Result(property = "orderAmount", column = "order_amount"),
            @Result(property = "actualIncome", column = "actual_income"),
            @Result(property = "status", column = "status"),
            @Result(property = "requestTime", column = "request_time"),
            @Result(property = "gatheringAccount", column = "gathering_account"),
            @Result(property = "settlementType", column = "settlement_type"),
            @Result(property = "accountingTime", column = "accounting_time"),
            @Result(property = "drawTime", column = "draw_time")})
    @Select("select " + column + " from payment_estimate_revenue where id = #{id}")
    PaymentEstimateRevenue getPaymentEstimateRevenue(@Param("id") String id);

    @Select("<script>select  " + column + "  from payment_estimate_revenue   where  1=1 " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when> " +
            "<if test=\"map.shopsName != null and map.shopsName != ''\"> and shops_name like concat('%',#{map.shopsName},'%') </if>" +
            "<if test=\"map.requestTimeStart != null and map.requestTimeStart != '' and map.requestTimeEnd != null and map.requestTimeEnd != ''\"> and  request_time between #{map.requestTimeStart} and #{map.requestTimeEnd}  </if>" +
            "<if test=\"map.drawTimeStart != null and map.drawTimeStart != '' and map.drawTimeEnd != null and map.drawTimeEnd != ''\"> and  draw_time between #{map.drawTimeStart} and #{map.drawTimeEnd}  </if>" +
            "<if test=\"map.accountingTimeStart != null and map.accountingTimeStart != '' and map.accountingTimeEnd != null and map.accountingTimeEnd != ''\"> and  accounting_time between #{map.accountingTimeStart} and #{map.accountingTimeEnd}  </if>" +
            "<if test=\"map.orderIds != null and map.orderIds.size() > 0 \"> and order_id in <foreach collection=\"map.orderIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>" +
            "<if test=\"map.userIds != null and map.userIds.size() > 0 \"> and user_id in <foreach collection=\"map.userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>" +
            " order by draw_time desc,status " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("paymentEstimateRevenue")
    List<PaymentEstimateRevenue> getPaymentEstimateRevenues(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("map") Map<String,Object> map);

    @Select("<script>select  " + column + "  from payment_estimate_revenue   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("paymentEstimateRevenue")
    List<PaymentEstimateRevenue> getPaymentEstimateRevenueAll();

    @Select("<script>select  count(1) from payment_estimate_revenue   where  1=1 " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when> " +
            "<if test=\"map.shopsName != null and map.shopsName != ''\"> and shops_name like concat('%',#{map.shopsName},'%') </if>" +
            "<if test=\"map.requestTimeStart != null and map.requestTimeStart != '' and map.requestTimeEnd != null and map.requestTimeEnd != ''\"> and  request_time between #{map.requestTimeStart} and #{map.requestTimeEnd}  </if>" +
            "<if test=\"map.drawTimeStart != null and map.drawTimeStart != '' and map.drawTimeEnd != null and map.drawTimeEnd != ''\"> and  draw_time between #{map.drawTimeStart} and #{map.drawTimeEnd}  </if>" +
            "<if test=\"map.accountingTimeStart != null and map.accountingTimeStart != '' and map.accountingTimeEnd != null and map.accountingTimeEnd != ''\"> and  accounting_time between #{map.accountingTimeStart} and #{map.accountingTimeEnd}  </if>" +
            "<if test=\"map.orderIds != null and map.orderIds.size() > 0 \"> and order_id in <foreach collection=\"map.orderIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>" +
            "<if test=\"map.userIds != null and map.userIds.size() > 0 \"> and user_id in <foreach collection=\"map.userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>" +
            " </script>")
    int getPaymentEstimateRevenueCount(@Param("status") int status,  @Param("map") Map<String,Object> map);

    @Update("update payment_estimate_revenue  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`order_id` = #{orderId}  , " +
            "`shops_name` = #{shopsName}  , " +
            "`order_amount` = #{orderAmount}  , " +
            "`actual_income` = #{actualIncome}  , " +
            "`status` = #{status}  , " +
            "`request_time` = #{requestTime}  , " +
            "`gathering_account` = #{gatheringAccount}  , " +
            "`settlement_type` = #{settlementType}  , " +
            "`accounting_time` = #{accountingTime}  , " +
            "`draw_time` = #{drawTime}  " +
            " where id = #{id}")
    void changePaymentEstimateRevenue(PaymentEstimateRevenue paymentEstimateRevenue);


    @Update("update payment_estimate_revenue  " +
            "set " +
            "`status` =3 , " +
            "`request_time` = now() " +
            " where id in (${id}) and `status` = 1")
    void changePaymentEstimateRevenueRequestTime(@Param("id") String idStr);

    @Update("update payment_estimate_revenue  " +
            "set " +
            "`status` = 4 , " +
            "`accounting_time` =now()  " +
            " where id in (${id}) and `status` = 3")
    void changePaymentEstimateRevenueAccountingTime(@Param("id") String idStr);

    @Select("<script> select * from payment_estimate_revenue where status in (1,3) </script>")
    List<PaymentEstimateRevenue> getPaymentEstimateRevenueForUnhandle();

    @Update("<script> update payment_estimate_revenue " +
            "<set>" +
            "<if test=\"map.status != null \"> status = #{map.status}, </if> " +
            "</set> where id = #{map.id} " +
            " </script>")
    void changePaymentEstimateRevenueByParam(@Param("map") Map<String, Object> map);
}
