package com.tuoniaostore.payment.data;

import com.tuoniaostore.datamodel.payment.PaymentCurrencyOffsetLogger;
import com.tuoniaostore.datamodel.vo.payment.PaymentCurrencyOffsetLoggerVO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@Mapper
public interface PaymentCurrencyOffsetLoggerMapper {

    String column = "`user_id`, " +//用户ID
            "`channel_id`, " +//渠道ID(用于计算归属)
            "`currency_type`, " +//货币类型
            "`before_amount`, " +//变化前的额度
            "`offset_amount`, " +//偏移的额度
            "`after_amount`, " +//变化后的额度
            "`trans_title`, " +//交易的标题
            "`trans_type`, " +//交易的类型 如：支付宝、微信、银联等 具体字段内容由逻辑定义
            "`relation_trans_type`, " +//关联的交易类型 如：提现、收入等
            "`relation_trans_sequence`, " +//关联的交易序号
            "`create_time`"//记录时间
            ;

    @Insert("insert into payment_currency_offset_logger (" +
            " `id`,  " +
            " `user_id`,  " +
            " `channel_id`,  " +
            " `currency_type`,  " +
            " `before_amount`,  " +
            " `offset_amount`,  " +
            " `after_amount`,  " +
            " `trans_title`,  " +
            " `trans_type`,  " +
            " `relation_trans_type`,  " +
            " `relation_trans_sequence` " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{channelId},  " +
            "#{currencyType},  " +
            "#{beforeAmount},  " +
            "#{offsetAmount},  " +
            "#{afterAmount},  " +
            "#{transTitle},  " +
            "#{transType},  " +
            "#{relationTransType},  " +
            "#{relationTransSequence} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addPaymentCurrencyOffsetLogger(PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger);

    @Results(id = "paymentCurrencyOffsetLogger", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "channelId", column = "channel_id"), @Result(property = "currencyType", column = "currency_type"), @Result(property = "beforeAmount", column = "before_amount"), @Result(property = "offsetAmount", column = "offset_amount"), @Result(property = "afterAmount", column = "after_amount"), @Result(property = "transTitle", column = "trans_title"), @Result(property = "transType", column = "trans_type"), @Result(property = "relationTransType", column = "relation_trans_type"), @Result(property = "relationTransSequence", column = "relation_trans_sequence"),  @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from payment_currency_offset_logger where id = #{id}")
    PaymentCurrencyOffsetLogger getPaymentCurrencyOffsetLogger(@Param("id") String id);

    @Select("<script>select  " + column + "  from payment_currency_offset_logger   where   1=1 " +

            /*" <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +*/
            "<if test=\"map.currencyType != null and map.currencyType != -1 \"> and currency_type = #{map.currencyType} </if> " +
            "<if test=\"map.userIds != null and map.userIds.size() > 0\"> and user_id in <foreach collection=\"map.userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> " +
            "<if test=\"map.min != null and map.max != null \"> and after_amount between #{map.min} and #{map.max} </if> " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("paymentCurrencyOffsetLogger")
    List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggers(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("map") Map<String, Object> map);

    @Select("<script>select  " + column + "  from payment_currency_offset_logger    " +

            "</script>")
    @ResultMap("paymentCurrencyOffsetLogger")
    List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggerAll();

    @Select("<script>select count(1) from payment_currency_offset_logger   where   1=1 " +


            "<if test=\"map.currencyType != null and map.currencyType != -1 \"> and currency_type = #{map.currencyType} </if> " +
            "<if test=\"map.userIds != null and map.userIds.size() > 0\"> and user_id in <foreach collection=\"map.userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> " +
            "<if test=\"map.min != null and map.max != null \"> and after_amount between #{map.min} and #{map.max} </if> " +
            "</script>")
    int getPaymentCurrencyOffsetLoggerCount( @Param("map") Map<String, Object> map);

    @Select("<script>select count(1) from payment_currency_offset_logger   where   1=1 " +

            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "</script>")
    int getPaymentCurrencyOffsetLoggerCountByParam(@Param("name") String name);

    @Update("update payment_currency_offset_logger  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`channel_id` = #{channelId}  , " +
            "`currency_type` = #{currencyType}  , " +
            "`before_amount` = #{beforeAmount}  , " +
            "`offset_amount` = #{offsetAmount}  , " +
            "`after_amount` = #{afterAmount}  , " +
            "`trans_title` = #{transTitle}  , " +
            "`trans_type` = #{transType}  , " +
            "`relation_trans_type` = #{relationTransType}  , " +
            "`relation_trans_sequence` = #{relationTransSequence}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changePaymentCurrencyOffsetLogger(PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger);


    @Select("<script>select  " + column + "  from payment_currency_offset_logger  " +
            "where user_id = #{userId} and relation_trans_type = #{relationTransType}  " +
            "<when test=\"time == 'day' \">and to_days(create_time) = to_days(now())</when>" +
            "<when test=\"time == 'month' \">and month(create_time) = month(now())</when>" +
            "<when test=\"limitStart != null and limitStart != null \">order by  create_time limit #{limitStart}, #{limitEnd}</when></script>")
    @ResultMap("paymentCurrencyOffsetLogger")
    List<PaymentCurrencyOffsetLogger> getTodayPaymentCurrencyOffsetLoggerByUserId(@Param("userId") String userId,@Param("relationTransType")int relationTransType,@Param("time")String time,@Param("limitStart")Integer limitStart,@Param("limitEnd")Integer limitEnd);

    @Select("<script>select  " + column + "  from payment_currency_offset_logger  " +
            "where user_id = #{userId} and relation_trans_type = #{relationTransType}  " +
            " and date_format(create_time,'%Y-%m-%d') = #{time}" +
            "<when test=\"limitStart != null and limitStart != null \">order by create_time limit #{limitStart}, #{limitEnd}</when></script>")
    @ResultMap("paymentCurrencyOffsetLogger")
    List<PaymentCurrencyOffsetLogger> getSelectTodayPaymentCurrencyOffsetLoggerByUserId(@Param("userId")String userId,@Param("relationTransType")int relationTransType, @Param("time")String time,@Param("limitStart")Integer limitStart,@Param("limitEnd")Integer limitEnd);

    @Select("<script>select  " + column + "  from payment_currency_offset_logger  " +
            "where user_id = #{userId} <when test=\"relationTransType != '' and relationTransType != '-1'.toString() \">and relation_trans_type = #{relationTransType}</when> order by create_time desc</script>")
    @ResultMap("paymentCurrencyOffsetLogger")
    List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggersByParam(@Param("userId") String userId, @Param("relationTransType")Integer relationTransType);

    @Select("<script>select  count(*) as count,sum(offset_amount) as money  from payment_currency_offset_logger  " +
            "where user_id = #{userId} and relation_trans_type = #{relationTransType}  " +
            " and date_format(create_time,'%Y-%m-%d') = #{time}" +
            "</script>")
    @ResultType(Map.class)
    Map<String, Object> getCountTodayPaymentCurrencyOffsetLoggerByUserId(String userId, int relationTransType,String time);

    @Results(id = "paymentTransactionLoggerVO", value = {
            @Result(property = "totalMoney", column = "totalMoney"), @Result(property = "userId", column = "userId")})
    @Select("<script> SELECT user_id as userId,sum(offset_amount)as totalMoney FROM `payment_currency_offset_logger` where relation_trans_type = #{type} " +
            " <if test=\"lists != null and lists.size > 0\">and user_id in " +
            "<foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> GROUP BY user_id </script> ")
    List<PaymentCurrencyOffsetLoggerVO> getPaymentCurrencyOffsetLoggerTotalMoneyByUser(@Param("lists") List<String> userList, @Param("type") int type);

    @Select("<script> select * from payment_currency_offset_logger where 1 = 1 and status = 0 " +
            " <if test=\"userId != null\"> and user_id = #{userId}</if> " +
            " <if test=\"map.currencyType != null\"> and currency_type = #{map.currencyType}</if> " +
            " <if test=\"map.min != null and map.max != null\"> and offset_amount between #{map.min} and #{map.max}</if> " +
            " <if test=\"map.transType != null and map.transType != -1 \"> and trans_type = #{map.transType}</if> " +
            " <if test=\"map.relationTransType != null and map.relationTransType != -1 \"> and relation_trans_type = #{map.relationTransType}</if> " +
            " <if test=\"map.startTime != null and map.endTime != null\"> and DATE_FORMAT(create_time,'%Y-%m-%d') between #{map.startTime} and #{map.endTime} </if> " +
            " order by create_time desc "+
            " <when test=\"map.pageStartIndex != null and map.pageSize != null\"> " +
            "  limit #{map.pageStartIndex}, #{map.pageSize} " +
            "</when></script>")
    List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggersByUserAndParam(@Param("userId") String userId, @Param("map")Map<String, Object> map);

    @Select("<script> select count(1) from payment_currency_offset_logger where 1 = 1 and status = 0 " +
            " <if test=\"userId != null\"> and user_id = #{userId}</if> " +
            " <if test=\"map.currencyType != null\"> and currency_type = #{map.currencyType}</if> " +
            " <if test=\"map.min != null and map.max != null\"> and offset_amount between #{map.min} and #{map.max}</if> " +
            " <if test=\"map.transType != null and map.transType != -1 \"> and trans_type = #{map.transType}</if> " +
            " <if test=\"map.relationTransType != null and map.relationTransType != -1 \"> and relation_trans_type = #{map.relationTransType}</if> " +
            " <if test=\"map.startTime != null and map.endTime != null\"> and DATE_FORMAT(create_time,'%Y-%m-%d') between #{map.startTime} and #{map.endTime} </if> " +
            " order by create_time desc "+
            "</script>")
    int getCountPaymentCurrencyOffsetLoggersByUserAndParam(@Param("userId") String userId, @Param("map")Map<String, Object> map);

    @Select("<script> SELECT sum(CASE WHEN relation_trans_type = 1 then offset_amount else 0 END) AS 'in',  " +
            "sum( CASE WHEN relation_trans_type in (0,2) then offset_amount else 0 END ) AS 'out' FROM `payment_currency_offset_logger`  " +
            "where user_id = #{userId} and  currency_type = #{currencyType}" +
            "<if test=\"startTime != null and endTime != null \"> and create_time between #{startTime} and #{endTime}</if> " +
            "</script> ")
    Map<String, Object> getPaymentCurrencyOffsetLoggerForInOut(@Param("userId") String userId,@Param("currencyType") Integer currencyType,@Param("startTime") String startTime,@Param("endTime") String endTime);
}
