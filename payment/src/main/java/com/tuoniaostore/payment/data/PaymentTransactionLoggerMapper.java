package com.tuoniaostore.payment.data;

import com.tuoniaostore.datamodel.payment.PaymentTransactionLogger;
import com.tuoniaostore.datamodel.vo.order.PayLogNumbersVo;
import com.tuoniaostore.payment.vo.PayLogVo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@Mapper
public interface PaymentTransactionLoggerMapper {

    String column = "`id`, "+
            "`user_id`, " +//发生交易的通行证ID
            "`trans_sequence_number`, " +//我方生成的交易序列号
            "`trans_status`, " +//交易状态 使用二进制方式计算 如：客户端成功时为4 回调成功时为8 双方成功后为12 默认1
            "`payment_type`, " +//支付类型 0-余额 1-微信 2-支付宝 3-银联等 如有其他交易方式 由逻辑决定
            "`trans_type`, " +
            "trans_supplier_type, " +//商家端、供应商
            "`partner_id`, " +//我方为合作商分配的商家服务号
            "`app_id`, " +//商户的appId
            "`partner_user_id`, " +//合作商家的用户标志
            "`partner_trade_number`, " +//合作商家的交易序号
            "`channel_id`, " +//我方为上游合作商分配的渠道号 0-官方渠道 其他值由逻辑决定
            "`channel_trade_number`, " +//渠道方的交易序列号
            "`channel_user_id`, " +//渠道方的用户ID(唯一标志)
            "`channel_user_name`, " +//渠道方用户名
            "`channel_remark`, " +//渠道方的备注信息
            "`account_number`, " +//收款帐号(仅提现时有效)
            "`account_name`, " +//收款账户名
            "`account_type`, " +//账户类型，如：信用卡、储值卡
            "`currency_type`, " +//交易货币类型，如RMB
            "`bank_name`, " +//银行名
            "`bank_branch_code`, " +//银行编码，如支行编码
            "`bank_simple_name`, " +//银行简称代码，如：ICBC
            "`trans_unit_amount`, " +//交易单价 单位：分
            "`trans_number`, " +//交易数量
            "`trans_total_amount`, " +//交易总价 单位：分 这里未必等于单价*数量 因为可能存在优惠
            "`trans_title`, " +//交易标题
            "`remark`, " +//备注内容
            "`create_time`, " +//创建时间
            "`trans_create_time`, " +//交易时间
            "`payment_time`, " +//支付时间
            "`use_conpon`, " +//是否使用优惠券 0-未使用 1-使用
            "`discount_amount`, " +//优惠额度 单位：分
            "`refund_status`, " +//退款状态 0-未退款
            "`refund_time`, " +//退款时间
            "`notify_front_url`, " +//处理完请求后，当前页面跳转到指定页面的路径
            "`notify_url`, " +//回调接口
            "`extend_parameter`, " +//其他参数
            "`extend_time`," +//操作时间
            "`take_desc`," +
            "`low_cost`," +
            "`high_cost`," +
            "`rate`,"+
            "`wx_form_id`, "+
            " `take_rule_id`, "+
            " `take_desc` "
            ;

    @Insert("insert into payment_transaction_logger (" +
            " `id`,  " +
            " `user_id`,  " +
            " `trans_sequence_number`,  " +
            " `trans_status`,  " +
            " `payment_type`,  " +
            " `trans_type`,  " +
            "`trans_supplier_type`, " +
            " `partner_id`,  " +
            " `app_id`,  " +
            " `partner_user_id`,  " +
            " `partner_trade_number`,  " +
            " `channel_id`,  " +
            " `channel_trade_number`,  " +
            " `channel_user_id`,  " +
            " `channel_user_name`,  " +
            " `channel_remark`,  " +
            " `account_number`,  " +
            " `account_name`,  " +
            " `account_type`,  " +
            " `currency_type`,  " +
            " `bank_name`,  " +
            " `bank_branch_code`,  " +
            " `bank_simple_name`,  " +
            " `trans_unit_amount`,  " +
            " `trans_number`,  " +
            " `trans_total_amount`,  " +
            " `trans_title`,  " +
            " `remark`,  " +
            " `trans_create_time`,  " +
            " `payment_time`,  " +
            " `use_conpon`,  " +
            " `discount_amount`,  " +
            " `refund_status`,  " +
            " `refund_time`,  " +
            " `notify_front_url`,  " +
            " `notify_url`,  " +
            " `extend_parameter`,  " +
            " `extend_time`," +
            " `take_rule_id`, "+
            " `take_desc`, "+
            " `low_cost`, "+
            " `high_cost`, "+
            " `rate`, " +
            " `wx_form_id` "+
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{transSequenceNumber},  " +
            "#{transStatus},  " +
            "#{paymentType},  " +
            "#{transType},  " +
            "#{transSupplierType}," +
            "#{partnerId},  " +
            "#{appId},  " +
            "#{partnerUserId},  " +
            "#{partnerTradeNumber},  " +
            "#{channelId},  " +
            "#{channelTradeNumber},  " +
            "#{channelUserId},  " +
            "#{channelUserName},  " +
            "#{channelRemark},  " +
            "#{accountNumber},  " +
            "#{accountName},  " +
            "#{accountType},  " +
            "#{currencyType},  " +
            "#{bankName},  " +
            "#{bankBranchCode},  " +
            "#{bankSimpleName},  " +
            "#{transUnitAmount},  " +
            "#{transNumber},  " +
            "#{transTotalAmount},  " +
            "#{transTitle},  " +
            "#{remark},  " +
            "#{transCreateTime},  " +
            "#{paymentTime},  " +
            "#{useConpon},  " +
            "#{discountAmount},  " +
            "#{refundStatus},  " +
            "#{refundTime},  " +
            "#{notifyFrontUrl},  " +
            "#{notifyUrl},  " +
            "#{extendParameter},  " +
            "#{extendTime}," +
            "#{takeRuleId},  "+
            "#{takeDesc}, "+
            "#{lowCost}, "+
            "#{highCost}, "+
            "#{rate}, " +
            "#{wxFormId}"+
            ")")
    @Options(useGeneratedKeys = true)
    void addPaymentTransactionLogger(PaymentTransactionLogger paymentTransactionLogger);

    @Results(id = "paymentTransactionLogger", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"),
            @Result(property = "transSequenceNumber", column = "trans_sequence_number"), @Result(property = "transStatus", column = "trans_status"), @Result(property = "paymentType", column = "payment_type"), @Result(property = "transType", column = "trans_type"), @Result(property = "partnerId", column = "partner_id"), @Result(property = "appId", column = "app_id"), @Result(property = "partnerUserId", column = "partner_user_id"), @Result(property = "partnerTradeNumber", column = "partner_trade_number"), @Result(property = "channelId", column = "channel_id"), @Result(property = "channelTradeNumber", column = "channel_trade_number"), @Result(property = "channelUserId", column = "channel_user_id"), @Result(property = "channelUserName", column = "channel_user_name"), @Result(property = "channelRemark", column = "channel_remark"), @Result(property = "accountNumber", column = "account_number"), @Result(property = "accountName", column = "account_name"), @Result(property = "accountType", column = "account_type"), @Result(property = "currencyType", column = "currency_type"), @Result(property = "bankName", column = "bank_name"), @Result(property = "bankBranchCode", column = "bank_branch_code"), @Result(property = "bankSimpleName", column = "bank_simple_name"), @Result(property = "transUnitAmount", column = "trans_unit_amount"), @Result(property = "transNumber", column = "trans_number"), @Result(property = "transTotalAmount", column = "trans_total_amount"), @Result(property = "transTitle", column = "trans_title"), @Result(property = "remark", column = "remark"), @Result(property = "createTime", column = "create_time"), @Result(property = "transCreateTime", column = "trans_create_time"),
            @Result(property = "paymentTime", column = "payment_time"),
            @Result(property = "useConpon", column = "use_conpon"), @Result(property = "discountAmount", column = "discount_amount"), @Result(property = "refundStatus", column = "refund_status"), @Result(property = "refundTime", column = "refund_time"), @Result(property = "notifyFrontUrl", column = "notify_front_url"), @Result(property = "notifyUrl", column = "notify_url"), @Result(property = "extendParameter", column = "extend_parameter"), @Result(property = "extendTime", column = "extend_time")})
    @Select("select " + column + " from payment_transaction_logger where id = #{id}")
    PaymentTransactionLogger getPaymentTransactionLogger(@Param("id") String id);

    @Select("<script>select  " + column + "  from payment_transaction_logger   where   1=1 " +
            " <when test=\"parameters.transStatus != null and parameters.transStatus != -1\">" +
            "  and trans_status =#{parameters.transStatus}" +
            " </when>" +
            "  and trans_type in " +
            " <foreach collection='parameters.transType' item='type' index='index' open='(' separator=',' close=')'> " +
            " #{type} " +
            " </foreach> " +
            " <when test=\"parameters.transTotalAmountLow != null and parameters.transTotalAmountLow > 0\">" +
            "  and trans_total_amount &gt;= #{parameters.transTotalAmountLow}" +
            " </when>" +
            " <when test=\"parameters.transTotalAmountHigh != null and parameters.transTotalAmountHigh > 0\">" +
            "  and trans_total_amount &lt;= #{parameters.transTotalAmountHigh}" +
            " </when>" +
            " <when test=\"parameters.createTimeBeg != null\">" +
            "  and create_time &gt;= date_format(#{parameters.createTimeBeg},'%Y-%m-%d')" +
            " </when>" +
            " <when test=\"parameters.createTimeEnd != null\">" +
            "  and create_time &lt;= date_format(#{parameters.createTimeEnd},'%Y-%m-%d')" +
            " </when>" +
            " <when test=\"parameters.paymentTimeBeg != null\">" +
            "  and payment_time &gt;= date_format(#{parameters.paymentTimeBeg},'%Y-%m-%d')" +
            " </when>" +
            " <when test=\"parameters.paymentTimeEnd != null\">" +
            "  and payment_time &lt;= date_format(#{parameters.paymentTimeEnd},'%Y-%m-%d')" +
            " </when>" +
            " <when test=\"parameters.transCreateTimeBeg != null\">" +
            "  and trans_create_time &gt;= date_format(#{parameters.transCreateTimeBeg},'%Y-%m-%d')" +
            " </when>" +
            " <when test=\"parameters.transCreateTimeEnd != null\">" +
            "  and trans_create_time &lt;= date_format(#{parameters.transCreateTimeEnd},'%Y-%m-%d')" +
            " </when>" +
            " <when test=\"parameters.userIds != null\">" +
            "  and user_id in " +
            " <foreach collection='parameters.userIds' item='id' index='index' open='(' separator=',' close=')'>" +
            " #{id} " +
            " </foreach> " +
            " </when>" +
            " ORDER BY create_time DESC " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when>" +
            "</script>")
    @ResultMap("paymentTransactionLogger")
    List<PaymentTransactionLogger> getPaymentTransactionLoggers( @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("parameters") HashMap<String, Object> parameters);

    @Select("<script>select  " + column + "  from payment_transaction_logger   where   1=1  " +
            " </script>")
    @ResultMap("paymentTransactionLogger")
    List<PaymentTransactionLogger> getPaymentTransactionLoggerAll();

    @Select("<script>select  count(1)  from payment_transaction_logger   where   1=1 " +
            " <when test=\"parameters.transStatus != null and parameters.transStatus != -1\">" +
            "  and trans_status =#{parameters.transStatus}" +
            " </when>" +
            "  and trans_type in " +
            " <foreach collection='parameters.transType' item='type' index='index' open='(' separator=',' close=')'> " +
            " #{type} " +
            " </foreach> " +
            " <when test=\"parameters.transTotalAmountLow != null\">" +
            "  and trans_total_amount &gt;= #{parameters.transTotalAmountLow}" +
            " </when>" +
            " <when test=\"parameters.transTotalAmountHigh != null and parameters.transTotalAmountHigh != 0\">" +
            "  and trans_total_amount &lt;= #{parameters.transTotalAmountHigh}" +
            " </when>" +
            " <when test=\"parameters.createTimeBeg != null\">" +
            "  and create_time &gt;= #{parameters.createTimeBeg}" +
            " </when>" +
            " <when test=\"parameters.createTimeEnd != null\">" +
            "  and create_time &lt;= #{parameters.createTimeEnd}" +
            " </when>" +
            " <when test=\"parameters.paymentTimeBeg != null\">" +
            "  and payment_time &gt;= #{parameters.paymentTimeBeg}" +
            " </when>" +
            " <when test=\"parameters.paymentTimeEnd != null\">" +
            "  and payment_time &lt;= #{parameters.paymentTimeEnd}" +
            " </when>" +
            " <when test=\"parameters.transCreateTimeBeg != null\">" +
            "  and trans_create_time &gt;= #{parameters.transCreateTimeBeg}" +
            " </when>" +
            " <when test=\"parameters.transCreateTimeEnd != null\">" +
            "  and trans_create_time &lt;= #{parameters.transCreateTimeEnd}" +
            " </when>" +
            " <when test=\"parameters.userIds != null\">" +
            "  and user_id in " +
            " <foreach collection='parameters.userIds' item='id' index='index' open='(' separator=',' close=')'>" +
            " #{id} " +
            " </foreach> " +
            " </when>" +
            "</script>")
    int getPaymentTransactionLoggerCount( @Param("parameters") HashMap<String, Object> parameters);

    @Update("<script>update payment_transaction_logger  " +
            "set " +
            "`trans_status` = 8  , " +
            "`payment_time` =now()   , " +
            " where id in " +
            "<foreach collection='array' item='id' index='index' open='(' separator=',' close=')'>" +
            "#{id}" +
            "</foreach>" +
            " </script>")
    void changePaymentTransactionLogger(String[] id);

    @Update(" <script> " +
            " update payment_transaction_logger  " +
            " set " +
            " `trans_status` = #{transStatus}  , " +
            " <when test=\"map.remark != null and map.remark.trim() != ''\">" +
            "   `remark` = #{map.remark} ," +
            " </when>" +
            " <when test=\"map.transType != null and map.transType != 0\">" +
            "   `trans_type` = #{map.transType} ," +
            " </when>" +
            " <when test=\"map.transTitle != null and map.transTitle.trim() != ''\">" +
            "   `trans_title` = #{map.transTitle} ," +
            " </when>" +
            " <choose>" +
            "   <when test=\"transStatus != 8\">" +
            "       `trans_create_time` =now() " +
            "   </when>" +
            "   <otherwise>" +
            "       `payment_time` =now()  " +
            "   </otherwise>" +
            " </choose>" +
            " where id in " +
            " <foreach collection='id' item='item' index='index' open='(' separator=',' close=')'> " +
            " #{item}" +
            " </foreach> " +
            " </script> ")
    void changePaymentTransactionLoggerStatus(@Param("id") String[] id, @Param("transStatus") int transStatus, @Param("map")HashMap map);

    @Update("<script>update payment_transaction_logger  " +
            "set " +
            "`trans_status` = 12  , " +
            "`trans_create_time` =now()  " +
            " where id in " +
            "<foreach collection='array' item='id' index='index' open='(' separator=',' close=')'>" +
            "#{id}" +
            "</foreach>" +
            " </script>")
    void changePaymentTransactionLoggerFinish(String[] id);

    @Update("<script> update payment_transaction_logger  " +
            "set " +
            "`trans_status` = 8  , " +
            "`trans_create_time` = now()   " +
            " where id in " +
            "<foreach collection='array' item='id' index='index' open='(' separator=',' close=')'>" +
            "#{id}" +
            "</foreach> </script>" )
    void changePaymentTransactionLoggerRefuse(String[] id);

    @Select("<script>select  " + column + "  from payment_transaction_logger   where  user_id = #{userId} " +
            "and trans_type = #{transType}" +
            "  order by trans_create_time desc   " +
            "  limit #{limitStart}, #{limitEnd}" +
            "</script>")
    @ResultMap("paymentTransactionLogger")
    List<PaymentTransactionLogger> getPaymentTransactionLoggerByUserIdAndType(@Param("userId") String userId, @Param("transType") int key,@Param("limitStart") int limitStart,@Param("limitEnd") int limitEnd);

    @Select("<script>select  " + column + "  from payment_transaction_logger   where  to_days(create_time) = to_days(now())" +
            "  order by create_time desc   " +
            "  limit 1" +
            "</script>")
    @ResultMap("paymentTransactionLogger")
    PaymentTransactionLogger getTransSequenceNumber();

    @Update("<script> update payment_transaction_logger  " +
            " <set> " +
            "`payment_time` = now()," +
            " <when test=\"map.transStatus != null and map.transStatus != null\">  `trans_status` = #{map.transStatus} , </when>   " +
            " <when test=\"map.refundStatus != null and map.refundStatus != null\">  `refund_status` = #{map.refundStatus} ,  </when>   " +
            " <when test=\"map.currencyType != null and map.currencyType != null\">   `currency_type` = #{map.currencyType} , </when>   " +
            " </set> where  trans_sequence_number = #{map.transSequenceNumber}" +
            "</script>" )
    void changePaymentTransactionLoggerByMap(@Param("map") Map<String ,Object > map);

    @Results(id = "payLogVo", value = {
            @Result(property = "orderNumer", column = "trans_sequence_number"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "orderPrice", column = "trans_total_amount"), @Result(property = "payTime", column = "payment_time"),
            @Result(property = "payType", column = "payment_type")})
    @Select("<script> select  create_time,trans_sequence_number,trans_total_amount , payment_time,payment_type from payment_transaction_logger   where " +
            " user_id =#{map.userId} and date_format(payment_time,'%Y-%m-%d') = #{map.paymentTime}" +
            "  order by payment_time desc "+
            " <when test=\"map.pageIndex != null and map.pageSize != null\">" +
            "  limit #{map.pageIndex}, #{map.pageSize}" +
            "</when>"+
            "</script>")
    List<PayLogVo> getPaymentTransactionLoggerByMap(@Param("map")  Map<String ,Object > map);

    @Select("<script> select  count(*)  from payment_transaction_logger where " +
            " user_id =#{map.userId} and date_format(payment_time,'%Y-%m-%d') = #{map.paymentTime}" +
            "</script>")
    int getPaymentTransactionLoggerByMapCount(@Param("map")  Map<String ,Object > map);



    @Results(id = "payLogNumbersVo", value = {
            @Result(property = "number", column = "number"), @Result(property = "paymentType", column = "paymentType"),
            @Result(property = "moneySum", column = "moneySum")})
    @Select("<script>  select count(0) as number ,t.payment_type as paymentType,sum(t.trans_total_amount) as moneySum from " +
            " payment_transaction_logger t  where " +
            " user_id = #{map.userId} and date_format(t.payment_time,'%Y-%m-%d') = #{map.paymentTime}" +
            " group by t.payment_type " +
            " </script>")
    List<PayLogNumbersVo> getPaymentTransactionLoggerNumberDate(@Param("map") Map<String ,Object > map);

    @Select("<script> select "+column+" from payment_transaction_logger  where 1 = 1" +
            " <when test=\"map.userId != null\"> and user_id = #{map.userId}</when>" +
            " <when test=\"map.transType != null\"> and trans_type = #{map.transType}</when>" +
            " <when test=\"map.refundNumberOrOrderNumber != null\"> and (trans_sequence_number = #{map.refundNumberOrOrderNumber} or channel_trade_number = #{map.refundNumberOrOrderNumber})   </when>" +
            " <when test=\"map.transSequenceNumber != null\"> and trans_sequence_number = #{map.transSequenceNumber}</when>" +
            " <when test=\"map.paymentType != null\"> and payment_type = #{map.paymentType}</when>" +
            " <when test=\"map.startPayaTime  != null \"> and payment_time <![CDATA[>=]]> #{map.startPayaTime} </when> "+
            " <when test=\"map.endPayaTime  != null \"> and payment_time <![CDATA[<]]> #{map.endPayaTime} </when> "+
            " order by create_time desc "+
            " <when test=\"map.pageIndex != null and map.pageSize != null\">" +
            "  limit #{map.pageIndex}, #{map.pageSize}" +
            "</when>"+
            "</script>")
    @ResultMap("paymentTransactionLogger")
    List<PaymentTransactionLogger> getPaymentTransactionLoggerMap(@Param("map")  Map<String ,Object > map);

    @Select("<script> select count(0) from payment_transaction_logger  where 1 = 1" +
            " <when test=\"map.transType != null\"> and trans_type = #{map.transType}</when>" +
            " <when test=\"map.refundNumberOrOrderNumber != null\"> and (trans_sequence_number = #{map.refundNumberOrOrderNumber} or channel_trade_number = #{map.refundNumberOrOrderNumber})   </when>" +
            " <when test=\"map.transSequenceNumber != null\"> and trans_sequence_number = #{map.transSequenceNumber}</when>" +
            " <when test=\"map.paymentType != null\"> and payment_type = #{map.paymentType}</when>" +
            " <when test=\"map.startPayaTime  != null \"> and payment_time <![CDATA[>=]]> #{map.startPayaTime} </when> "+
            " <when test=\"map.endPayaTime  != null \"> and payment_time <![CDATA[<]]> #{map.endPayaTime} </when> "+
            "</script>")
    @ResultMap("paymentTransactionLogger")
    int getPaymentTransactionLoggerMapCount(@Param("map")  Map<String ,Object > map);

    @Select("select * from payment_transaction_logger where trans_sequence_number = #{outTradeNo} LIMIT 1")
    PaymentTransactionLogger getPaymentTransactionLoggerByOutTradeNo(@Param("outTradeNo") String out_trade_no);

}
