package com.tuoniaostore.payment.data;

import com.sun.tracing.dtrace.ProviderAttributes;
import com.tuoniaostore.datamodel.payment.PaymentTakeRule;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@Mapper
public interface PaymentTakeRuleMapper {

    String column = " `id`,  " +
            "`take_mode`, " +//提现类型 默认值：0
            "`take_desc`, " +//展示在客户端的描述内容
            "`target_type`, " +//应用的目标类型
            "`low_cost`, " +//最低收费费用 单位：分
            "`high_cost`, " +//最高收费费用 单位：分
            "`rate`, " +//万份比
            "`single_amount_limit`, " +//单笔限制(上限) 单位：分
            "`day_amount_limit`, " +//当天上限
            "`day_count_limit`, " +//每天最多提现次数
            "`holiday_delay`, " +//遇节假日是否顺延 1 是 0 否
            "`delay_hour`, " +//如：48 为48小时内到账
            "`rule_effect_time`, " +//规则生效时间
            "`rule_invalid_time`, " +//规则失效时间
            "`show_image`, " +//展示于前端的图片 不存在时 表示无需展示图片
            "`default_option`, " +//是否为默认选项 1时是 0时否 一个目标类型只能有一种默认选项
            "`available_time_begin`, " +//可提现时间(开始)(为空时不限制)
            "`available_end_begin`, " +//可提现时间(结束)(为空时不限制)
            "`available_date`, " +//可提现星期, 将数字变为二进制形式, 最低位(最右)是星期日, 第七位(从右到左数)是星期一, 对应位为1的星期为可提现日(为空时不限制)
            "`retrieve_status`"//回收状态 0-正常 1-删除
            ;

    @Insert("insert into payment_take_rule (" +
            " `id`,  " +
            " `take_mode`,  " +
            " `take_desc`,  " +
            " `target_type`,  " +
            " `low_cost`,  " +
            " `high_cost`,  " +
            " `rate`,  " +
            " `single_amount_limit`,  " +
            " `day_amount_limit`,  " +
            " `day_count_limit`,  " +
            " `holiday_delay`,  " +
            " `delay_hour`,  " +
            " `rule_effect_time`,  " +
            " `rule_invalid_time`,  " +
            " `show_image`,  " +
            " `default_option`,  " +
            " `available_time_begin`,  " +
            " `available_end_begin`,  " +
            " `available_date`," +
            " `retrieve_status`  " +
            ")values(" +
            "#{id},  " +
            "#{takeMode},  " +
            "#{takeDesc},  " +
            "#{targetType},  " +
            "#{lowCost},  " +
            "#{highCost},  " +
            "#{rate},  " +
            "#{singleAmountLimit},  " +
            "#{dayAmountLimit},  " +
            "#{dayCountLimit},  " +
            "#{holidayDelay},  " +
            "#{delayHour},  " +
            "#{ruleEffectTime},  " +
            "#{ruleInvalidTime},  " +
            "#{showImage},  " +
            "#{defaultOption},  " +
            "#{availableTimeBegin},  " +
            "#{availableEndBegin},  " +
            "#{availableDate}," +
            "#{retrieveStatus} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addPaymentTakeRule(PaymentTakeRule paymentTakeRule);

    @Results(id = "paymentTakeRule", value = {
            @Result(property = "id", column = "id"), @Result(property = "takeMode", column = "take_mode"), @Result(property = "takeDesc", column = "take_desc"), @Result(property = "targetType", column = "target_type"), @Result(property = "lowCost", column = "low_cost"), @Result(property = "highCost", column = "high_cost"), @Result(property = "rate", column = "rate"), @Result(property = "singleAmountLimit", column = "single_amount_limit"), @Result(property = "dayAmountLimit", column = "day_amount_limit"), @Result(property = "dayCountLimit", column = "day_count_limit"), @Result(property = "holidayDelay", column = "holiday_delay"), @Result(property = "delayHour", column = "delay_hour"), @Result(property = "ruleEffectTime", column = "rule_effect_time"), @Result(property = "ruleInvalidTime", column = "rule_invalid_time"), @Result(property = "showImage", column = "show_image"), @Result(property = "defaultOption", column = "default_option"), @Result(property = "availableTimeBegin", column = "available_time_begin"), @Result(property = "availableEndBegin", column = "available_end_begin"), @Result(property = "availableDate", column = "available_date")})
    @Select("select " + column + " from payment_take_rule where id = #{id}")
    PaymentTakeRule getPaymentTakeRule(@Param("id") String id);

    @Select("<script>select  " + column + "  from payment_take_rule   where   1=1 and `retrieve_status` = 0 " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("paymentTakeRule")
    List<PaymentTakeRule> getPaymentTakeRules(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from payment_take_rule </script>")
    @ResultMap("paymentTakeRule")
    List<PaymentTakeRule> getPaymentTakeRuleAll();

    @Select("<script>select count(1) from payment_take_rule   where   1=1 and `retrieve_status` = 0 " +

            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getPaymentTakeRuleCount(@Param("name") String name);

    @Update("update payment_take_rule  " +
            "set " +
            "`take_mode` = #{takeMode}  , " +
            "`take_desc` = #{takeDesc}  , " +
            "`target_type` = #{targetType}  , " +
            "`low_cost` = #{lowCost}  , " +
            "`high_cost` = #{highCost}  , " +
            "`rate` = #{rate}  , " +
            "`single_amount_limit` = #{singleAmountLimit}  , " +
            "`day_amount_limit` = #{dayAmountLimit}  , " +
            "`day_count_limit` = #{dayCountLimit}  , " +
            "`holiday_delay` = #{holidayDelay}  , " +
            "`delay_hour` = #{delayHour}  , " +
            "`rule_effect_time` = #{ruleEffectTime}  , " +
            "`rule_invalid_time` = #{ruleInvalidTime}  , " +
            "`show_image` = #{showImage}  , " +
            "`default_option` = #{defaultOption}  , " +
            "`available_time_begin` = #{availableTimeBegin}  , " +
            "`available_end_begin` = #{availableEndBegin}  , " +
            "`available_date` = #{availableDate}  " +
            " where id = #{id}")
    void changePaymentTakeRule(PaymentTakeRule paymentTakeRule);

    @Select("select * from payment_take_rule where `retrieve_status` = 0 and take_mode=#{mode}")
    @ResultMap("paymentTakeRule")
    List<PaymentTakeRule> getPaymentTakeRuleByMode(@Param("mode")Integer mode);

    @Update("update payment_take_rule  " +
            "set" +
            "`default_option` = #{option} " +
            "where `take_mode` = #{mode}")
    void changePaymentTakeRuleUnDefault(@Param("mode")int mode, @Param("option")int option);

    @Update("<script>update payment_take_rule  " +
            "set " +
            "`retrieve_status` = 1 " +
            " where id in " +
            "<foreach collection='ids' item='id' index='index' open='(' separator=',' close=')'>" +
            "#{id}" +
            "</foreach>" +
            " </script>")
    void delPaymentTakeRule(@Param("ids")String[] ids);
}
