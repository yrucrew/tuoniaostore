package com.tuoniaostore.payment.data;

import com.tuoniaostore.datamodel.payment.PaymentCurrencyAccount;
import org.apache.ibatis.annotations.*;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@Mapper
public interface PaymentCurrencyAccountMapper {

    String column = " `id`," +
            "`user_id`, " +//用户ID
            "`channel_id`, " +//渠道ID(用于计算归属)
            "`currency_type`, " +//货币类型
            "`name`, " +//货币名字
            "`current_amount`, " +//当前可用额度
            "`freeze_amount`, " +//冻结额度
            "`total_into_amount`, " +//总的入账额度
            "`total_output_amount`, " +//总的出账额度
            "`create_time`"//建立时间
            ;

    @Insert("insert into payment_currency_account (" +
            " `id`,  " +
            " `user_id`,  " +
            " `channel_id`,  " +
            " `currency_type`,  " +
            " `name`,  " +
            " `current_amount`,  " +
            " `freeze_amount`,  " +
            " `total_into_amount`,  " +
            " `total_output_amount`  " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{channelId},  " +
            "#{currencyType},  " +
            "#{name},  " +
            "#{currentAmount},  " +
            "#{freezeAmount},  " +
            "#{totalIntoAmount},  " +
            "#{totalOutputAmount}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addPaymentCurrencyAccount(PaymentCurrencyAccount paymentCurrencyAccount);

    @Results(id = "paymentCurrencyAccount", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "channelId", column = "channel_id"), @Result(property = "currencyType", column = "currency_type"), @Result(property = "name", column = "name"), @Result(property = "currentAmount", column = "current_amount"), @Result(property = "freezeAmount", column = "freeze_amount"), @Result(property = "totalIntoAmount", column = "total_into_amount"), @Result(property = "totalOutputAmount", column = "total_output_amount"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from payment_currency_account where id = #{id}")
    PaymentCurrencyAccount getPaymentCurrencyAccount(@Param("id") String id);

    @Select("<script>select  " + column + "  from payment_currency_account   where  1=1 " +
            "<if test=\"map.currencyType != null and map.currencyType != -1 \"> and currency_type = #{map.currencyType} </if> " +
            "<if test=\"map.userIds != null and map.userIds.size() > 0\"> and user_id in <foreach collection=\"map.userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> " +
            "<if test=\"map.min != null and map.max != null \"> and current_amount between #{map.min} and #{map.max} </if> " +
            "<when test=\"pageIndex != null and pageSize != null\"> " +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("paymentCurrencyAccount")
    List<PaymentCurrencyAccount> getPaymentCurrencyAccounts(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("map") Map<String,Object> map);

    @Select("<script>select  " + column + "  from payment_currency_account    </script>")
    @ResultMap("paymentCurrencyAccount")
    List<PaymentCurrencyAccount> getPaymentCurrencyAccountAll();

    @Select("<script>select  count(1)  from payment_currency_account   where  1=1 " +
            "<if test=\"map.currencyType != null and map.currencyType != -1 \"> and currency_type = #{map.currencyType} </if> " +
            "<if test=\"map.userIds != null and map.userIds.size() > 0\"> and user_id in <foreach collection=\"map.userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> " +
            "<if test=\"map.min != null and map.max != null \"> and current_amount between #{map.min} and #{map.max} </if> " +
            "</script>")
    int getPaymentCurrencyAccountCount(@Param("map") Map<String,Object> map);

    @Update("update payment_currency_account  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`channel_id` = #{channelId}  , " +
            "`currency_type` = #{currencyType}  , " +
            "`name` = #{name}  , " +
            "`current_amount` = #{currentAmount}  , " +
            "`freeze_amount` = #{freezeAmount}  , " +
            "`total_into_amount` = #{totalIntoAmount}  , " +
            "`total_output_amount` = #{totalOutputAmount}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changePaymentCurrencyAccount(PaymentCurrencyAccount paymentCurrencyAccount);

    @Select("select " + column + " from payment_currency_account where user_id = #{userId} and currency_type = #{currencyType}")
    @ResultMap("paymentCurrencyAccount")
    PaymentCurrencyAccount getPaymentCurrencyAccountByUserId(@Param("userId") String userId,@Param("currencyType") int currencyType);

    @Update("update payment_currency_account  " +
            "set " +
            " current_amount = current_amount - #{currentAmount}" +
            " where user_id = #{userId} and currency_type = #{currencyType}")
    void changePaymentCurrencyAccountWithWithdrawl(@Param("userId") String id, @Param("currentAmount") long money, @Param("currencyType") int type);

    @Update("update payment_currency_account  " +
            "set " +
            " current_amount = current_amount + #{currentAmount}" +
            " where user_id = #{userId} and currency_type = #{currencyType}")
    void changePaymentCurrencyAccountWithWithdrawlAdd(@Param("userId") String id, @Param("currentAmount")long money, @Param("currencyType")int type);
}
