package com.tuoniaostore.payment.data;

import com.tuoniaostore.datamodel.payment.PaymentSettlementSet;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@Mapper
public interface PaymentSettlementSetMapper {

    String column =   " `id`,  " +
            "`user_id`, " +//
            "`cable_point`, " +//扣点(万分比)
            "`settlement_days`, " +//结算天数
            "`settlement_money`, " +//结算金额
            "`settlement_type`, " +//结算金额：0.按结算天数，1.按结算额度
            "`type`"//1、扣点处理，2返点处理
            ;

    @Insert("insert into payment_settlement_set (" +
            " `id`,  " +
            " `user_id`,  " +
            " `cable_point`,  " +
            " `settlement_days`,  " +
            " `settlement_money`,  " +
            " `settlement_type`,  " +
            " `type` " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{cablePoint},  " +
            "#{settlementDays},  " +
            "#{settlementMoney},  " +
            "#{settlementType},  " +
            "#{type} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addPaymentSettlementSet(PaymentSettlementSet paymentSettlementSet);

    @Results(id = "paymentSettlementSet", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "cablePoint", column = "cable_point"), @Result(property = "settlementDays", column = "settlement_days"), @Result(property = "settlementMoney", column = "settlement_money"), @Result(property = "settlementType", column = "settlement_type"), @Result(property = "type", column = "type")})
    @Select("select " + column + " from payment_settlement_set where id = #{id}")
    PaymentSettlementSet getPaymentSettlementSet(@Param("id") String id);

    @Select("<script>select  " + column + "  from payment_settlement_set   where  1=1 " +
            "<if test=\"map.settlementType != null and map.settlementType != -1 \"> and settlement_type = #{map.settlementType} </if> " +
            "<if test=\"map.type != null and map.type != -1 \"> and type = #{map.type} </if> " +
            "<if test=\"map.userIds != null and map.userIds.size() > 0 \"> " +
            " and user_id in <foreach collection=\"map.userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> " +
            "</if> " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("paymentSettlementSet")
    List<PaymentSettlementSet> getPaymentSettlementSets(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("map") Map<String,Object> map);

    @Select("<script>select  " + column + "  from payment_settlement_set   where  1=1  </script>")
    @ResultMap("paymentSettlementSet")
    List<PaymentSettlementSet> getPaymentSettlementSetAll();

    @Select("<script>select count(1)  from payment_settlement_set   where  1=1 " +
            "<if test=\"map.settlementType != null and map.settlementType != -1 \"> and settlement_type = #{map.settlementType} </if> " +
            "<if test=\"map.type != null and map.type != -1 \"> and type = #{map.type} </if> " +
            "<if test=\"map.userIds != null and map.userIds.size() > 0 \"> and user_id in <foreach collection=\"map.userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> " +
            "</script>")
    int getPaymentSettlementSetCount(@Param("map") Map<String,Object> map);

    @Update("update payment_settlement_set  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`cable_point` = #{cablePoint}  , " +
            "`settlement_days` = #{settlementDays}  , " +
            "`settlement_money` = #{settlementMoney}  , " +
            "`settlement_type` = #{settlementType}  , " +
            "`type` = #{type}  " +
            " where id = #{id}")
    void changePaymentSettlementSet(PaymentSettlementSet paymentSettlementSet);

    @Select("select * from payment_settlement_set where user_id = #{userId} limit 1 ")
    PaymentSettlementSet getPaymentSettlementSetByUserId(@Param("userId") String userId);
}
