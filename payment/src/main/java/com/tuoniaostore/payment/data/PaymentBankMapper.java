package com.tuoniaostore.payment.data;

import com.tuoniaostore.datamodel.payment.PaymentBank;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@Mapper
public interface PaymentBankMapper {

    String column = "`id`," +
            "`user_id`, " +//通行证ID
            "`bank_template_id`, " +//银行模版ID
            "`account_type`, " +//账户类型1、储值卡 2、信用卡
            "`bank_account`, " +//银行帐号
            "`account_name`, " +//持有者姓名
            "`reserve_phone`, " +//预留电话
            "`branch_name`, " +//支行名
            "`status`, " +//状态 如：默认 1、失效 -1、普通 0等
            "`create_time`"//建立时间
            ;

    @Insert("insert into payment_bank (" +
            " `id`,  " +
            " `user_id`,  " +
            " `bank_template_id`,  " +
            " `account_type`,  " +
            " `bank_account`,  " +
            " `account_name`,  " +
            " `reserve_phone`,  " +
            " `branch_name`,  " +
            " `status`" +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{bankTemplateId},  " +
            "#{accountType},  " +
            "#{bankAccount},  " +
            "#{accountName},  " +
            "#{reservePhone},  " +
            "#{branchName},  " +
            "#{status} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addPaymentBank(PaymentBank paymentBank);

    @Results(id = "paymentBank", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "bankTemplateId", column = "bank_template_id"), @Result(property = "accountType", column = "account_type"), @Result(property = "bankAccount", column = "bank_account"), @Result(property = "accountName", column = "account_name"), @Result(property = "reservePhone", column = "reserve_phone"), @Result(property = "branchName", column = "branch_name"), @Result(property = "status", column = "status"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from payment_bank where id = #{id}")
    PaymentBank getPaymentBank(@Param("id") String id);

    @Select("<script>select  " + column + "  from payment_bank   where   1=1 " +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when> " +
            " order by create_time desc " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("paymentBank")
    List<PaymentBank> getPaymentBanks(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);


    @Select("<script>select  " + column + "  from payment_bank   where  1=1  and status=0 </script>")
    @ResultMap("paymentBank")
    List<PaymentBank> getPaymentBankAll();

    @Select("<script>select count(1) from payment_bank   where   1=1" +
            " <when test=\"status != null and status != -1\">" +
             " and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getPaymentBankCount(@Param("status") int status, @Param("name") String name);

    @Update("update payment_bank  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`bank_template_id` = #{bankTemplateId}  , " +
            "`account_type` = #{accountType}  , " +
            "`bank_account` = #{bankAccount}  , " +
            "`account_name` = #{accountName}  , " +
            "`reserve_phone` = #{reservePhone}  , " +
            "`branch_name` = #{branchName}  , " +
            "`status` = #{status}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changePaymentBank(PaymentBank paymentBank);

    @Select("<script>select  " + column + "  from payment_bank   where   1=1 " +

            " <when test=\"map.bankTemplateId != null and map.bankTemplateId.trim() != ''\"> and bank_template_id like concat('%', #{map.bankTemplateId}, '%') </when>" +
            " <when test=\"map.bankAccount != null and map.bankAccount.trim() != ''\"> and bank_account like concat('%', #{map.bankAccount}, '%') </when>" +
            " <when test=\"map.userIds != null and map.userIds.size() > 0\"> and user_id in <foreach collection=\"map.userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </when> " +
            " <when test=\"map.accountName != null and map.accountName.trim() != ''\"> and account_name like concat('%', #{map.accountName}, '%') </when>" +
            " <when test=\"map.accountType != null and map.accountType != -1 \"> and account_type = #{map.accountType} </when> " +
            " <when test=\"map.reservePhone != null and map.reservePhone.trim() != ''\"> and reserve_phone like concat('%', #{map.reservePhone}, '%') </when>" +
            " <when test=\"map.branchName != null and map.branchName.trim() != ''\"> and branch_name like concat('%', #{map.branchName}, '%') </when> " +
            " order by create_time desc " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("paymentBank")
    List<PaymentBank> getPaymentBanksByParam(@Param("map") Map<String, Object> map, @Param("status") int status,@Param("pageIndex") int pageIndex,@Param("pageSize") int pageSize);

    @ResultMap("paymentBank")
    @Select("select " + column + " from payment_bank where user_id = #{userId}")
    List<PaymentBank> getPaymentBankByUserId(@Param("userId") String userId);

    @Select("<script>select count(1) from payment_bank   where   1=1" +
            /*" <when test=\"status != null and status != -1\">" +
            " and status=#{status} " +
            " </when>" +*/
            " <if test=\"map.bankTemplateId != null and map.bankTemplateId.trim() != ''\"> and bank_template_id like concat('%', #{map.bankTemplateId}, '%') </if>" +
            " <if test=\"map.bankAccount != null and map.bankAccount.trim() != ''\"> and bank_account like concat('%', #{map.bankAccount}, '%') </if>" +
            "<if test=\"map.userIds != null and map.userIds.size() > 0\"> and user_id in <foreach collection=\"map.userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> " +
            " <if test=\"map.accountName != null and map.accountName.trim() != ''\"> and account_name like concat('%', #{map.accountName}, '%') </if>" +
            " <if test=\"map.accountType != null and map.accountType != -1 \"> and account_type = #{map.accountType} </if> " +
            " <if test=\"map.reservePhone != null and map.reservePhone.trim() != ''\"> and reserve_phone like concat('%', #{map.reservePhone}, '%') </if>" +
            " <if test=\"map.branchName != null and map.branchName.trim() != ''\"> and branch_name like concat('%', #{map.branchName}, '%') </if>" +
            " </script>")
    int getPaymentBankCountByParam(int status, Map<String, Object> map);

    @Delete("<script> delete from payment_bank where id in " +
            "<foreach collection='array' item='id' open='(' separator=',' close=')'>" +
            "#{id}" +
            "</foreach>" +
            "</script>")
    void deletePaymentBank(String[] idArr);
}
