package com.tuoniaostore.payment.data;

import com.tuoniaostore.datamodel.payment.PaymentBankTemplate;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@Mapper
public interface PaymentBankTemplateMapper {

    String column =
            "`id`, " +//id
            "`name`, " +//银行名字
            "`simple_code`, " +//简称
            "`bank_code`, " +//支行号
            "`logo`, " +//图片
            "`create_time`"//建立时间
            ;

    @Insert("insert into payment_bank_template (" +
            " `id`,  " +
            " `name`,  " +
            " `simple_code`,  " +
            " `bank_code`,  " +
            " `logo` " +
            ")values(" +
            "#{id},  " +
            "#{name},  " +
            "#{simpleCode},  " +
            "#{bankCode},  " +
            "#{logo} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addPaymentBankTemplate(PaymentBankTemplate paymentBankTemplate);

    @Results(id = "paymentBankTemplate", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "simpleCode", column = "simple_code"),
            @Result(property = "bankCode", column = "bank_code"),
            @Result(property = "logo", column = "logo"),
            @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from payment_bank_template where id = #{id}")
    PaymentBankTemplate getPaymentBankTemplate(@Param("id") String id);

    @Select("<script>select  " + column + "  from payment_bank_template   where   1=1 " +

            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            " ORDER BY create_time DESC   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("paymentBankTemplate")
    List<PaymentBankTemplate> getPaymentBankTemplates( @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from payment_bank_template     </script>")
    @ResultMap("paymentBankTemplate")
    List<PaymentBankTemplate> getPaymentBankTemplateAll();

    @Select("<script>select count(1) from payment_bank_template   where  1=1" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getPaymentBankTemplateCount(  @Param("name") String name);

    @Update("update payment_bank_template  " +
            "set " +
            "`name` = #{name}  , " +
            "`simple_code` = #{simpleCode}  , " +
            "`bank_code` = #{bankCode}  , " +
            "`logo` = #{logo}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changePaymentBankTemplate(PaymentBankTemplate paymentBankTemplate);

    @Select("<script>select  " + column + "  from payment_bank_template   " +
            "where 1 = 1 <if test=\"list != null\"> and id in <foreach item =\"item\" collection=\"list\" open=\"(\" separator=\",\" close=\")\" >#{item}</foreach></if></script>")
    @ResultMap("paymentBankTemplate")
    List<PaymentBankTemplate> getPaymentBankTemplateById(@Param("list") List<String> list);

    @Select("<script>select  " + column + "  from payment_bank_template   where   1=1 " +

            " <if test=\"map.name != null and map.name.trim() != ''\"> and name like concat('%', #{map.name}, '%') </if>" +
            " <if test=\"map.simpleCode != null and map.simpleCode.trim() != ''\"> and simple_code like concat('%', #{map.simpleCode}, '%') </if>" +
            " <if test=\"map.bankCode != null and map.bankCode.trim() != ''\"> and bank_code like concat('%', #{map.bankCode}, '%') </if>" +
            " ORDER BY create_time DESC " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("paymentBankTemplate")
    List<PaymentBankTemplate> getPaymentBankTemplatesByParam(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("map") Map<String, Object> map);

    @Select("<script>select  count(1)  from payment_bank_template   where   1=1 " +
            " <if test=\"map.name != null and map.name.trim() != ''\"> and name like concat('%', #{map.name}, '%') </if>" +
            " <if test=\"map.simpleCode != null and map.simpleCode.trim() != ''\"> and simple_code like concat('%', #{map.simpleCode}, '%') </if>" +
            " <if test=\"map.bankCode != null and map.bankCode.trim() != ''\"> and bank_code like concat('%', #{map.bankCode}, '%') </if>" +
            " </script>")
    int getPaymentBankTemplateCountByParam(@Param("map") Map<String, Object> map);

    @Delete("<script> " +
            "delete from payment_bank_template where id in " +
            "<foreach collection='array' item='id' open='(' separator=',' close=')'>" +
            "#{id}" +
            "</foreach>" +
            "</script>")
    void deletePaymentBankTemplate(String[] idArr);
}
