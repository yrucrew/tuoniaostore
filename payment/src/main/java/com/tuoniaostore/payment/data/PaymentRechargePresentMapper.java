package com.tuoniaostore.payment.data;

import com.tuoniaostore.datamodel.payment.PaymentRechargePresent;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@Mapper
public interface PaymentRechargePresentMapper {

    String column = "`minimum_recharge_amount`, " +//最低充值额度
            "`currency_type`, " +//对应的货币类型
            "`present_amount`, " +//赠送额度
            "`instruction`, " +//介绍内容
            "`effective_time`, " +//生效时间
            "`failure_time`, " +//失效时间
            "`status`, " +//当前状态 如：1、有效 0、无效
            "`limits`, " +//赠送金额限制设置
            "`create_time`"//建立时间
            ;

    @Insert("insert into payment_recharge_present (" +
            " `id`,  " +
            " `minimum_recharge_amount`,  " +
            " `currency_type`,  " +
            " `present_amount`,  " +
            " `instruction`,  " +
            " `effective_time`,  " +
            " `failure_time`,  " +
            " `status`,  " +
            " `limits`,  " +
            ")values(" +
            "#{id},  " +
            "#{minimumRechargeAmount},  " +
            "#{currencyType},  " +
            "#{presentAmount},  " +
            "#{instruction},  " +
            "#{effectiveTime},  " +
            "#{failureTime},  " +
            "#{status},  " +
            "#{limits},  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addPaymentRechargePresent(PaymentRechargePresent paymentRechargePresent);

    @Results(id = "paymentRechargePresent", value = {
            @Result(property = "id", column = "id"), @Result(property = "minimumRechargeAmount", column = "minimum_recharge_amount"), @Result(property = "currencyType", column = "currency_type"), @Result(property = "presentAmount", column = "present_amount"), @Result(property = "instruction", column = "instruction"), @Result(property = "effectiveTime", column = "effective_time"), @Result(property = "failureTime", column = "failure_time"), @Result(property = "status", column = "status"), @Result(property = "limits", column = "limits"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from payment_recharge_present where id = #{id}")
    PaymentRechargePresent getPaymentRechargePresent(@Param("id") String id);

    @Select("<script>select  " + column + "  from payment_recharge_present   where   1=1" +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("paymentRechargePresent")
    List<PaymentRechargePresent> getPaymentRechargePresents(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from payment_recharge_present   where   1=1  and status=0 </script>")
    @ResultMap("paymentRechargePresent")
    List<PaymentRechargePresent> getPaymentRechargePresentAll();

    @Select("<script>select count(1) from payment_recharge_present   where  1=1" +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getPaymentRechargePresentCount(@Param("status") int status, @Param("name") String name);

    @Update("update payment_recharge_present  " +
            "set " +
            "`minimum_recharge_amount` = #{minimumRechargeAmount}  , " +
            "`currency_type` = #{currencyType}  , " +
            "`present_amount` = #{presentAmount}  , " +
            "`instruction` = #{instruction}  , " +
            "`effective_time` = #{effectiveTime}  , " +
            "`failure_time` = #{failureTime}  , " +
            "`status` = #{status}  , " +
            "`limits` = #{limits}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changePaymentRechargePresent(PaymentRechargePresent paymentRechargePresent);

}
