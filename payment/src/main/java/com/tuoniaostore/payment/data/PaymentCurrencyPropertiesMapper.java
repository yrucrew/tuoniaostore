package com.tuoniaostore.payment.data;

import com.tuoniaostore.datamodel.payment.PaymentCurrencyProperties;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@Mapper
public interface PaymentCurrencyPropertiesMapper {

    String column = "`id`, " +//id
            "`user_id`, " +//账户ID
            "`type`, " +//属性类型
            "`k`, " +//属性key
            "`v`, " +//属性值
            "`create_time`"//建立时间
            ;

    @Insert("insert into payment_currency_properties (" +
            " `id`,  " +
            " `user_id`,  " +
            " `type`,  " +
            " `k`,  " +
            " `v`  " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{type},  " +
            "#{k},  " +
            "#{v}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addPaymentCurrencyProperties(PaymentCurrencyProperties paymentCurrencyProperties);

    @Results(id = "paymentCurrencyProperties", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "type", column = "type"), @Result(property = "k", column = "k"), @Result(property = "v", column = "v"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from payment_currency_properties where id = #{id}")
    PaymentCurrencyProperties getPaymentCurrencyProperties(@Param("id") String id);

    @Select("<script>select  " + column + "  from payment_currency_properties   where   1=1 " +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("paymentCurrencyProperties")
    List<PaymentCurrencyProperties> getPaymentCurrencyPropertiess(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from payment_currency_properties   where   1=1  and status=0 </script>")
    @ResultMap("paymentCurrencyProperties")
    List<PaymentCurrencyProperties> getPaymentCurrencyPropertiesAll();

    @Select("<script>select count(1) from payment_currency_properties   where   1=1" +
            " <when test=\"status != null and status != -1\">" +
            " and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getPaymentCurrencyPropertiesCount(@Param("status") int status, @Param("name") String name);

    @Update("update payment_currency_properties  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`type` = #{type}  , " +
            "`k` = #{k}  , " +
            "`v` = #{v}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changePaymentCurrencyProperties(PaymentCurrencyProperties paymentCurrencyProperties);

    @ResultMap("paymentCurrencyProperties")
    @Select("select " + column + " from payment_currency_properties where user_id = #{userId} and type = #{type}")
    PaymentCurrencyProperties getPaymentCurrencyPropertiesByUserId(@Param("userId")String userId, @Param("type") int type);

    @Update("update payment_currency_properties  " +
            "set " +
            "`v` = #{v} " +
            " where id = #{id}")
    void updatePaymentCurrencyPropertiesPassword(PaymentCurrencyProperties paymentCurrencyProperties);
}
