package com.tuoniaostore.payment;

import com.tuoniaostore.payment.service.PaymentCurrencyAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/21
 */
@Controller
@RequestMapping("payAPI")
public class CommunityDataAccessController {

    @Autowired
    PaymentCurrencyAccountService paymentCurrencyAccountService;

    /**
     * 初始化账户余额
     * @return
     */
    @RequestMapping("initPaymentCurrencyAccount")
    public void initPaymentCurrencyAccount() {
        paymentCurrencyAccountService.initPaymentCurrencyAccount();
    }

}
