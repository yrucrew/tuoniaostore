package com.tuoniaostore.payment.vo;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/26
 */
public class PayBankVO implements Serializable {

    private static final long serialVersionUID = 1L;

    //
    private String id= DefineRandom.getUUID();
    //通行证ID
    private String userId;

    //创建人名字
    private String userName;

    //账户类型1、储值卡 2、信用卡
    private Integer accountType;

    //银行帐号
    private String bankAccount;

    //持有者姓名
    private String accountName;

    //预留电话
    private String reservePhone;

    //支行名
    private String branchName;

    //建立时间
    private Date createTime;

    //银行名字
    private String name;
    //简称
    private String simpleCode;
    //支行号
    private String bankCode;
    //图片
    private String logo;

    public PayBankVO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getReservePhone() {
        return reservePhone;
    }

    public void setReservePhone(String reservePhone) {
        this.reservePhone = reservePhone;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSimpleCode() {
        return simpleCode;
    }

    public void setSimpleCode(String simpleCode) {
        this.simpleCode = simpleCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
