package com.tuoniaostore.payment.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author sqd
 * @description
 * @date 2019/5/9
 */
public class PayLogVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private  String orderConditions;
    private  String orderNumer;
    private  Integer payType ;
    private  Integer orderPrice ;
    private Date payTime ;
    private Date createTime ;
    //json 转换时间格式化 防止时间转换产生误差
    @JsonFormat(pattern="yyyy年MM月dd日 HH:mm:ss",timezone = "GMT+8")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOrderConditions() {
        return orderConditions;
    }

    public void setOrderConditions(String orderConditions) {
        this.orderConditions = orderConditions;
    }

    public String getOrderNumer() {
        return orderNumer;
    }

    public void setOrderNumer(String orderNumer) {
        this.orderNumer = orderNumer;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Integer orderPrice) {
        this.orderPrice = orderPrice;
    }
    @JsonFormat(pattern="yyyy年MM月dd日 HH:mm:ss",timezone = "GMT+8")
    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }
}
