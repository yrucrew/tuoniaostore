package com.tuoniaostore.payment.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author sqd
 * @date 2019/5/15
 */
public class PayLogVo2 implements Serializable {

    private String id;
    private String transSequenceNumber;
    //交易总价 单位：分 这里未必等于单价*数量 因为可能存在优惠	***
    private Long transTotalAmount;
    //创建时间
    private Date createTime;
    //支付时间 ****
    private Date paymentTime;
    //退款时间
    private Date refundTime;
    //合作商家的用户标志
    private String partnerUserId;
    private long orderNumberPrice;
    //合作商家的用户标志
    private String partnerUserName;
    private String channelTradeNumber;
    private Integer refundStatus;

    public Integer getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Integer refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    @JsonFormat(pattern="yyyy年MM月dd日 HH:mm:ss",timezone = "GMT+8")
    public Date getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(Date refundTime) {
        this.refundTime = refundTime;
    }

    @JsonFormat(pattern="yyyy年MM月dd日 HH:mm:ss",timezone = "GMT+8")
    public Date getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getChannelTradeNumber() {
        return channelTradeNumber;
    }

    public void setChannelTradeNumber(String channelTradeNumber) {
        this.channelTradeNumber = channelTradeNumber;
    }

    public String getTransSequenceNumber() {
        return transSequenceNumber;
    }

    public void setTransSequenceNumber(String transSequenceNumber) {
        this.transSequenceNumber = transSequenceNumber;
    }

    public Long getTransTotalAmount() {
        return transTotalAmount;
    }

    public void setTransTotalAmount(Long transTotalAmount) {
        this.transTotalAmount = transTotalAmount;
    }

    @JsonFormat(pattern="yyyy年MM月dd日 HH:mm:ss",timezone = "GMT+8")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getPartnerUserId() {
        return partnerUserId;
    }

    public void setPartnerUserId(String partnerUserId) {
        this.partnerUserId = partnerUserId;
    }

    public long getOrderNumberPrice() {
        return orderNumberPrice;
    }

    public void setOrderNumberPrice(long orderNumberPrice) {
        this.orderNumberPrice = orderNumberPrice;
    }

    public String getPartnerUserName() {
        return partnerUserName;
    }

    public void setPartnerUserName(String partnerUserName) {
        this.partnerUserName = partnerUserName;
    }
}
