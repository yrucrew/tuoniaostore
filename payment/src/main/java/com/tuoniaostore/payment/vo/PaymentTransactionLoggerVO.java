package com.tuoniaostore.payment.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/27
 */
public class PaymentTransactionLoggerVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date operationTime; //操作时间
    private String bankName;    //银行名称
    private String accountNumber;//提现账号 展示尾号就行了
    private long transTotalAmount;//提现金额
    private String status;      //交易状态

    public PaymentTransactionLoggerVO() {
    }

    public Date getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(Date operationTime) {
        this.operationTime = operationTime;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public long getTransTotalAmount() {
        return transTotalAmount;
    }

    public void setTransTotalAmount(long transTotalAmount) {
        this.transTotalAmount = transTotalAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
