package com.tuoniaostore.payment.schedual;

import com.tuoniaostore.commons.constant.payment.PayTypeEnum;
import com.tuoniaostore.commons.constant.payment.RelationTransTypeEnum;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.datamodel.payment.*;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author oy
 * @description 预到账定时
 * @date 2019/6/12
 */
@Component
public class CurrencyService {

    @Autowired
    private PaymentDataAccessManager dataAccessManager;

    /***
     * 每10分钟 执行一次 查看没有入账的预收入  计算是否可以到账 给用户到余额
     * @author oy
     * @date 2019/6/12
     * @param
     * @return void
     */
    @Scheduled(cron = "0 0/10 * * * *")
    @Transactional
    public void handlePayCurrency(){
        //查找所有的未完成的 按照时间排序
        List<PaymentEstimateRevenue> list = dataAccessManager.getPaymentEstimateRevenueForUnhandle();
        if(list != null && list.size() > 0){
            //将没有完成的收集
            Map<String,List<PaymentEstimateRevenue>> map = new HashMap<>();
            list.forEach(x->{
                String userId = x.getUserId();
                List<PaymentEstimateRevenue> paymentEstimateRevenues = map.get(userId);
                if(paymentEstimateRevenues != null){
                    paymentEstimateRevenues.add(x);
                }else{
                    paymentEstimateRevenues = new ArrayList<>();
                    paymentEstimateRevenues.add(x);
                    map.put(userId,paymentEstimateRevenues);
                }
            });

            //去统计每个用户的计算规则
            if(map != null && !map.isEmpty()){
                map.forEach((x,y)->{
                    PaymentSettlementSet paymentSettlementSet = dataAccessManager.getPaymentSettlementSetByUserId(x);
                    if(paymentSettlementSet != null){
                        Integer type = paymentSettlementSet.getType();
                        Integer settlementType = paymentSettlementSet.getSettlementType();//结算类型 0.按结算天数，1.按结算额度
                        Long cablePoint = paymentSettlementSet.getCablePoint();
                        if(settlementType.equals(0)){//天数
                            Integer settlementDays = paymentSettlementSet.getSettlementDays();//结算天数
                            //计算时间 如果之前的时间 和 现在的时间 相差大于 结算天数 就给到账这些
                            y.forEach(z->{
                                Long actualIncome = z.getActualIncome();
                                Date requestTime = z.getRequestTime();
                                int betweenDay = DateUtils.getBetweenDay(new Date(), requestTime);
                                if(betweenDay >= settlementDays){
                                    addLogger(x,type,actualIncome,cablePoint,z);
                                }
                            });
                        }else{//按结算额度
                            Long settlementMoney = paymentSettlementSet.getSettlementMoney();//结算金额
                            //计算什么时候 额度有这个了  就到账
                            if(y != null && y.size() > 0){
                                long sum = y.stream().mapToLong(PaymentEstimateRevenue::getActualIncome).sum();//总共金额
                                if(sum >= settlementMoney){
                                    //到账
                                    y.forEach(z->{
                                        Long actualIncome = z.getActualIncome();
                                        addLogger(x,type,actualIncome,cablePoint,z);
                                    });
                                }
                            }
                        }

                    }else{ // 如果是空的 说明这个用户没有账期
                        //直接加余额 加日志
                        //到账
                        y.forEach(z->{
                            Long actualIncome = z.getActualIncome();
                            addLogger(x,null,actualIncome,null,z);
                        });
                    }
                });
            }
        }
    }

    public void addLogger(String x,Integer type,Long actualIncome,Long cablePoint,PaymentEstimateRevenue z){
        //给当前这个实体 提现 并记录
        //计算到账金额
        if(type == null){
            type = 0;
        }
        long money = 0l;
        if(type.equals(1)){ //扣点
            money = actualIncome * (10000 - cablePoint) / 10000;//向下取整
        }else if(type.equals(2)){//反点
            money = actualIncome * (10000 + cablePoint) / 10000;//向下取整
        } else{
            if(actualIncome == null){
                money = 0;
            }else{
                money = actualIncome;
            }
        }
        //添加预收入记录
        PaymentEstimateRevenueLogger paymentEstimateRevenueLogger = new PaymentEstimateRevenueLogger();
        paymentEstimateRevenueLogger.setActualIncome(money);
        paymentEstimateRevenueLogger.setOrderAmount(actualIncome);
        paymentEstimateRevenueLogger.setUserId(x);
        paymentEstimateRevenueLogger.setOrderId(z.getOrderId());
        paymentEstimateRevenueLogger.setGatheringAccount(z.getGatheringAccount());
        dataAccessManager.addPaymentEstimateRevenueLogger(paymentEstimateRevenueLogger);

        //添加户余额记录
        PaymentCurrencyAccount paymentCurrencyAccount = dataAccessManager.getPaymentCurrencyAccount(x);
        PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger = new PaymentCurrencyOffsetLogger();
        paymentCurrencyOffsetLogger.setUserId(x);
        Long currentAmount = z.getActualIncome();
        if(currentAmount == null){
            currentAmount = 0l;
        }
        long afterAmount = money + currentAmount;
        if(paymentCurrencyAccount != null){
            Long currentAmount1 = paymentCurrencyAccount.getCurrentAmount();
            paymentCurrencyOffsetLogger.setBeforeAmount(currentAmount1);
            paymentCurrencyOffsetLogger.setAfterAmount(currentAmount1 + afterAmount);
        }
        paymentCurrencyOffsetLogger.setOffsetAmount(afterAmount);
        paymentCurrencyOffsetLogger.setTransTitle("预收入到账");
        paymentCurrencyOffsetLogger.setTransType(PayTypeEnum.TRANS_TYPE_BALANCE.getValue());
        paymentCurrencyOffsetLogger.setRelationTransType(RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey());
        dataAccessManager.addPaymentCurrencyOffsetLogger(paymentCurrencyOffsetLogger);

        //给用户加余额
        dataAccessManager.changePaymentCurrencyAccountWithWithdrawlAdd(x,money,0);

        //修改对应的状态 完成到账
        Map<String,Object> map = new HashMap<>();
        map.put("id",z.getId());
        map.put("status",4);
        dataAccessManager.changePaymentEstimateRevenueByParam(map);//已完成提现
    }


    public static void main(String[] args) {
        long a = 134;
        long l = a * (10000 - 3) / 10000;
        System.out.println(l);
    }
}
