package com.tuoniaostore.payment.controller;
        import com.tuoniaostore.payment.service.PaymentRechargePresentService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Controller
@RequestMapping("paymentRechargePresent")
public class PaymentRechargePresentController {
    @Autowired
    private PaymentRechargePresentService paymentRechargePresentService;


    @RequestMapping("addPaymentRechargePresent")
    @ResponseBody
    public JSONObject addPaymentRechargePresent() {
        return paymentRechargePresentService.addPaymentRechargePresent();
    }

    @RequestMapping("getPaymentRechargePresent")
    @ResponseBody
    public JSONObject getPaymentRechargePresent() {
        return paymentRechargePresentService.getPaymentRechargePresent();
    }


    @RequestMapping("getPaymentRechargePresents")
    @ResponseBody
    public JSONObject getPaymentRechargePresents() {
        return paymentRechargePresentService.getPaymentRechargePresents();
    }

    @RequestMapping("getPaymentRechargePresentAll")
    @ResponseBody
    public JSONObject getPaymentRechargePresentAll() {
        return paymentRechargePresentService.getPaymentRechargePresentAll();
    }

    @RequestMapping("getPaymentRechargePresentCount")
    @ResponseBody
    public JSONObject getPaymentRechargePresentCount() {
        return paymentRechargePresentService.getPaymentRechargePresentCount();
    }

    @RequestMapping("changePaymentRechargePresent")
    @ResponseBody
    public JSONObject changePaymentRechargePresent() {
        return paymentRechargePresentService.changePaymentRechargePresent();
    }

}
