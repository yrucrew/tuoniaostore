package com.tuoniaostore.payment.controller;

import com.tuoniaostore.payment.service.PaymentBankTemplateService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Controller
@RequestMapping("paymentBankTemplate")
public class PaymentBankTemplateController {
    @Autowired
    private PaymentBankTemplateService paymentBankTemplateService;


    @RequestMapping("addPaymentBankTemplate")
    @ResponseBody
    public JSONObject addPaymentBankTemplate() {
        return paymentBankTemplateService.addPaymentBankTemplate();
    }

    @RequestMapping("getPaymentBankTemplate")
    @ResponseBody
    public JSONObject getPaymentBankTemplate() {
        return paymentBankTemplateService.getPaymentBankTemplate();
    }


    @RequestMapping("getPaymentBankTemplates")
    @ResponseBody
    public JSONObject getPaymentBankTemplates() {
        return paymentBankTemplateService.getPaymentBankTemplates();
    }

    @RequestMapping("getPaymentBankTemplateAll")
    @ResponseBody
    public JSONObject getPaymentBankTemplateAll() {
        return paymentBankTemplateService.getPaymentBankTemplateAll();
    }

    @RequestMapping("getPaymentBankTemplateCount")
    @ResponseBody
    public JSONObject getPaymentBankTemplateCount() {
        return paymentBankTemplateService.getPaymentBankTemplateCount();
    }

    @RequestMapping("changePaymentBankTemplate")
    @ResponseBody
    public JSONObject changePaymentBankTemplate() {
        return paymentBankTemplateService.changePaymentBankTemplate();
    }

    @ApiOperation("删除银行")
    @RequestMapping("deletePaymentBankTemplate")
    @ResponseBody
    public JSONObject deletePaymentBankTemplate() {
        return paymentBankTemplateService.deletePaymentBankTemplate();
    }

}
