package com.tuoniaostore.payment.controller;

import com.tuoniaostore.payment.service.PaymentBankService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Controller
@RequestMapping("paymentBank")
public class PaymentBankController {
    @Autowired
    private PaymentBankService paymentBankService;


    @RequestMapping("addPaymentBank")
    @ResponseBody
    public JSONObject addPaymentBank() {
        return paymentBankService.addPaymentBank();
    }



    @RequestMapping("getPaymentBank")
    @ResponseBody
    public JSONObject getPaymentBank() {
        return paymentBankService.getPaymentBank();
    }


    @RequestMapping("getPaymentBanks")
    @ResponseBody
    public JSONObject getPaymentBanks() {
        return paymentBankService.getPaymentBanks();
    }

    @RequestMapping("getPaymentBankAll")
    @ResponseBody
    public JSONObject getPaymentBankAll() {
        return paymentBankService.getPaymentBankAll();
    }

    @RequestMapping("getPaymentBankCount")
    @ResponseBody
    public JSONObject getPaymentBankCount() {
        return paymentBankService.getPaymentBankCount();
    }

    @RequestMapping("changePaymentBank")
    @ResponseBody
    public JSONObject changePaymentBank() {
        return paymentBankService.changePaymentBank();
    }

    /**
     * 获取用户银行卡
     * @return
     */
    @RequestMapping("getPaymentBankByUserId")
    @ResponseBody
    public JSONObject getPaymentBankByUserId() {
        return paymentBankService.getPaymentBankByUserId();
    }

    /**
     * 新增用户银行卡
     * @return
     */
    @RequestMapping("addPaymentBankByUserId")
    @ResponseBody
    public JSONObject addPaymentBankByUserId() {
        return paymentBankService.addPaymentBankByUserId();
    }

    @ApiOperation("删除银行卡")
    @RequestMapping("deletePaymentBank")
    @ResponseBody
    public JSONObject deletePaymentBank() {
        return paymentBankService.deletePaymentBank();
    }
}
