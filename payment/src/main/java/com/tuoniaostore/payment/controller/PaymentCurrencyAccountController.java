package com.tuoniaostore.payment.controller;

import com.tuoniaostore.payment.service.PaymentCurrencyAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Controller
@RequestMapping("paymentCurrencyAccount")
public class PaymentCurrencyAccountController {
    @Autowired
    private PaymentCurrencyAccountService paymentCurrencyAccountService;


    @RequestMapping("addPaymentCurrencyAccount")
    @ResponseBody
    public JSONObject addPaymentCurrencyAccount() {
        return paymentCurrencyAccountService.addPaymentCurrencyAccount();
    }

    @RequestMapping("getPaymentCurrencyAccount")
    @ResponseBody
    public JSONObject getPaymentCurrencyAccount() {
        return paymentCurrencyAccountService.getPaymentCurrencyAccount();
    }


    @RequestMapping("getPaymentCurrencyAccounts")
    @ResponseBody
    public JSONObject getPaymentCurrencyAccounts() {
        return paymentCurrencyAccountService.getPaymentCurrencyAccounts();
    }

    @RequestMapping("getPaymentCurrencyAccountAll")
    @ResponseBody
    public JSONObject getPaymentCurrencyAccountAll() {
        return paymentCurrencyAccountService.getPaymentCurrencyAccountAll();
    }

    @RequestMapping("getPaymentCurrencyAccountCount")
    @ResponseBody
    public JSONObject getPaymentCurrencyAccountCount() {
        return paymentCurrencyAccountService.getPaymentCurrencyAccountCount();
    }

    @RequestMapping("changePaymentCurrencyAccount")
    @ResponseBody
    public JSONObject changePaymentCurrencyAccount() {
        return paymentCurrencyAccountService.changePaymentCurrencyAccount();
    }

    /**
     * 获取用户的余额 和 今日收入 本月收入
     * @return
     */
    @RequestMapping("getPaymentCurrencyAccountByUserId")
    @ResponseBody
    public JSONObject getPaymentCurrencyAccountByUserId() {
        return  paymentCurrencyAccountService.getPaymentCurrencyAccountByUserId();
    }
    /**
     * 总余额 总入账额度 总未到账额度
     * @return
     */
    @RequestMapping("getPaymentCurrencyAccountBalance")
    @ResponseBody
    public JSONObject getPaymentCurrencyAccountBalance() {
        return  paymentCurrencyAccountService.getPaymentCurrencyAccountBalance();
    }

    /**
     * 查询用户余额
     * @author sqd
     * @date 2019/4/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getPaymentCurrencyAccountByCurrencyTypeEnum")
    @ResponseBody
    public JSONObject getPaymentCurrencyAccountByCurrencyTypeEnum() {
        return  paymentCurrencyAccountService.getPaymentCurrencyAccountByCurrencyTypeEnum();
    }
    /**
     * 获取用户账户总余额
     * @author sqd
     * @date 2019/4/28
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getPaymentCurrencyAccountBalanceByUserid")
    @ResponseBody
    public JSONObject getPaymentCurrencyAccountBalanceByUserid() {
        return  paymentCurrencyAccountService.getPaymentCurrencyAccountBalanceByUserid();
    }

    @RequestMapping("paymentOrderBalance")
    @ResponseBody
    public JSONObject paymentOrderBalance() {
        return paymentCurrencyAccountService.paymentOrderBalance();
    }

    /**
     * 商家端用户充值
     * @author sqd
     * @date 2019/5/2
     * @param
     * @return
     */
    @RequestMapping("balanceRecharge")
    @ResponseBody
    public JSONObject balanceRecharge() {
        return paymentCurrencyAccountService.balanceRecharge();
    }

    /**
     * 余额充值回调
     * @author sqd
     * @date 2019/5/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("recChangeCallback")
    @ResponseBody
    public JSONObject recChangeCallback() {
        return paymentCurrencyAccountService.recChangeCallback();
    }

    /***
     * 商家端充值结束，完成支付，查询订单，充值金额
     * @author oy
     * @date 2019/6/1
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("queryOrderForWeiPay")
    @ResponseBody
    public JSONObject queryOrderForWeiPay() {
        return paymentCurrencyAccountService.queryOrderForWeiPay();
    }

    @RequestMapping("getPaymentCurrencyAccountBalanceByid")
    @ResponseBody
    public JSONObject getPaymentCurrencyAccountBalanceByid() {
        return paymentCurrencyAccountService.getPaymentCurrencyAccountBalanceByid();
    }

    @RequestMapping("changePaymentCurrencyAccountJson")
    @ResponseBody
    public JSONObject changePaymentCurrencyAccountJson() {
        return paymentCurrencyAccountService.changePaymentCurrencyAccountJson();
    }
}
