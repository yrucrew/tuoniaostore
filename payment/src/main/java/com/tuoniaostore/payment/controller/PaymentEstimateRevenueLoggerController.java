package com.tuoniaostore.payment.controller;
        import com.tuoniaostore.payment.service.PaymentEstimateRevenueLoggerService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-08 10:21:40
 */
@Controller
@RequestMapping("paymentEstimateRevenueLogger")
public class PaymentEstimateRevenueLoggerController {
    @Autowired
    private PaymentEstimateRevenueLoggerService paymentEstimateRevenueLoggerService;


    @RequestMapping("addPaymentEstimateRevenueLogger")
    @ResponseBody
    public JSONObject addPaymentEstimateRevenueLogger() {
        return paymentEstimateRevenueLoggerService.addPaymentEstimateRevenueLogger();
    }

    @RequestMapping("getPaymentEstimateRevenueLogger")
    @ResponseBody
    public JSONObject getPaymentEstimateRevenueLogger() {
        return paymentEstimateRevenueLoggerService.getPaymentEstimateRevenueLogger();
    }

    @RequestMapping("getPaymentEstimateRevenueLoggers")
    @ResponseBody
    public JSONObject getPaymentEstimateRevenueLoggers() {
        return paymentEstimateRevenueLoggerService.getPaymentEstimateRevenueLoggers();
    }

    @RequestMapping("getPaymentEstimateRevenueLoggerAll")
    @ResponseBody
    public JSONObject getPaymentEstimateRevenueLoggerAll() {
        return paymentEstimateRevenueLoggerService.getPaymentEstimateRevenueLoggerAll();
    }

    @RequestMapping("getPaymentEstimateRevenueLoggerCount")
    @ResponseBody
    public JSONObject getPaymentEstimateRevenueLoggerCount() {
        return paymentEstimateRevenueLoggerService.getPaymentEstimateRevenueLoggerCount();
    }

    @RequestMapping("changePaymentEstimateRevenueLogger")
    @ResponseBody
    public JSONObject changePaymentEstimateRevenueLogger() {
        return paymentEstimateRevenueLoggerService.changePaymentEstimateRevenueLogger();
    }

    @RequestMapping("addPaymentEstimateRevenueLoggerJson")
    @ResponseBody
    public JSONObject addPaymentEstimateRevenueLoggerJson() {
        return paymentEstimateRevenueLoggerService.addPaymentEstimateRevenueLoggerJson();
    }

}
