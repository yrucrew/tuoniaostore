package com.tuoniaostore.payment.controller;

import com.tuoniaostore.payment.service.PaymentTransactionLoggerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@RestController
@RequestMapping("paymentTransactionLogger")
public class PaymentTransactionLoggerController {
    @Autowired
    private PaymentTransactionLoggerService paymentTransactionLoggerService;


    @RequestMapping("addPaymentTransactionLogger")
    public JSONObject addPaymentTransactionLogger() {
        return paymentTransactionLoggerService.addPaymentTransactionLogger();
    }

    @RequestMapping("addPaymentTransactionLoggerJson")
    public JSONObject addPaymentTransactionLoggerJson() {
        return paymentTransactionLoggerService.addPaymentTransactionLoggerJson();
    }

    @RequestMapping("getPaymentTransactionLogger")
    public JSONObject getPaymentTransactionLogger() {
        return paymentTransactionLoggerService.getPaymentTransactionLogger();
    }

    @ApiOperation("提现列表")
    @RequestMapping("getPaymentTransactionLoggers")
    public JSONObject getPaymentTransactionLoggers() {
        return paymentTransactionLoggerService.getPaymentTransactionLoggers();
    }

    @RequestMapping("getPaymentTransactionLoggerAll")
    public JSONObject getPaymentTransactionLoggerAll() {
        return paymentTransactionLoggerService.getPaymentTransactionLoggerAll();
    }

    @RequestMapping("getPaymentTransactionLoggerCount")
    public JSONObject getPaymentTransactionLoggerCount() {
        return paymentTransactionLoggerService.getPaymentTransactionLoggerCount();
    }

    @RequestMapping("changePaymentTransactionLogger")
    public JSONObject changePaymentTransactionLogger() {
        return paymentTransactionLoggerService.changePaymentTransactionLogger();
    }

    /**
     * 提现记录
     *
     * @return
     */
    @RequestMapping("getWithdrawalPaymentTransactionLogger")
    public JSONObject getWithdrawalPaymentTransactionLogger() {
        return paymentTransactionLoggerService.getWithdrawalPaymentTransactionLogger();
    }


    /**
     * 提现操作：点进来获取数据 获取结算卡（默认第一张） 然后获取到账时间规则（默认取第一条）
     */
    @RequestMapping("getRulePaymentTransactionLogger")
    public JSONObject getRulePaymentTransactionLogger() {
        return paymentTransactionLoggerService.getRulePaymentTransactionLogger();
    }

    @ApiOperation("提现打款")
    @RequestMapping("changePaymentTransactionLoggerRemit")
    public JSONObject changePaymentTransactionLoggerRemit() {
        return paymentTransactionLoggerService.changePaymentTransactionLoggerRemit();
    }

    @ApiOperation("提现到账")
    @RequestMapping("changePaymentTransactionLoggerFinish")
    public JSONObject changePaymentTransactionLoggerFinish() {
        return paymentTransactionLoggerService.changePaymentTransactionLoggerFinish();
    }

    @ApiOperation("拒绝提现")
    @RequestMapping("changePaymentTransactionLoggerRefuse")
    public JSONObject changePaymentTransactionLoggerRefuse() {
        return paymentTransactionLoggerService.changePaymentTransactionLoggerRefuse();
    }

    /**
     * 提现操作：获取短信验证码
     *
     * @return
     */
    @RequestMapping("getMSGPaymentTransactionLogger")
    public JSONObject getMSGPaymentTransactionLogger() {
        return paymentTransactionLoggerService.getMSGPaymentTransactionLogger();
    }

    /**
     * 提现操作
     *
     * @return
     */
    @RequestMapping("withdrawalPaymentTransactionLogger")
    public JSONObject withdrawalPaymentTransactionLogger() {
        return paymentTransactionLoggerService.withdrawalPaymentTransactionLogger();
    }

    /**
     * 修改支付记录
     *
     * @param
     * @return void
     * @author sqd
     * @date 2019/4/26
     */
    @RequestMapping("changePaymentTransactionLoggerByMap")
    public void changePaymentTransactionLoggerByMap(Map<String, Object> map) {
        paymentTransactionLoggerService.changePaymentTransactionLoggerMap();
    }

    /**
     * 商家端 订单流水记录查询
     *
     * @param
     * @return void
     * @author sqd
     * @date 2019/5/9
     */
    @RequestMapping("getPaymentTransactionLoggerPos")
    public JSONObject getPaymentTransactionLoggerPos(Map<String, Object> map) {
        return paymentTransactionLoggerService.getPaymentTransactionLoggerPos();
    }

    /**
     * 后台收支明细查询
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/13
     */
    @RequestMapping("getPaymentTransactionLoggerMap")
    public JSONObject getPaymentTransactionLoggerMap(Map<String, Object> map) {
        return paymentTransactionLoggerService.getPaymentTransactionLoggerMap();
    }

    @RequestMapping("getPaymentTransactionLoggerById")
    public JSONObject getPaymentTransactionLoggerById(Map<String, Object> map) {
        return paymentTransactionLoggerService.getPaymentTransactionLoggerById();
    }

    @RequestMapping("getPaymentTransactionLoggerPosMap")
    public JSONObject getPaymentTransactionLoggerPosMap(Map<String, Object> map) {
        return paymentTransactionLoggerService.getPaymentTransactionLoggerPosMap();
    }

}
