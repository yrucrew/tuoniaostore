package com.tuoniaostore.payment.controller;

import com.tuoniaostore.payment.service.PaymentEstimateRevenueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-08 10:21:40
 */
@Controller
@RequestMapping("paymentEstimateRevenue")
public class PaymentEstimateRevenueController {
    @Autowired
    private PaymentEstimateRevenueService paymentEstimateRevenueService;


    @RequestMapping("addPaymentEstimateRevenue")
    @ResponseBody
    public JSONObject addPaymentEstimateRevenue() {
        return paymentEstimateRevenueService.addPaymentEstimateRevenue();
    }

    @RequestMapping("addPaymentEstimateRevenueJson")
    @ResponseBody
    public JSONObject addPaymentEstimateRevenueJson() {
        return paymentEstimateRevenueService.addPaymentEstimateRevenueJson();
    }

    @RequestMapping("getPaymentEstimateRevenue")
    @ResponseBody
    public JSONObject getPaymentEstimateRevenue() {
        return paymentEstimateRevenueService.getPaymentEstimateRevenue();
    }


    @RequestMapping("getPaymentEstimateRevenues")
    @ResponseBody
    public JSONObject getPaymentEstimateRevenues() {
        return paymentEstimateRevenueService.getPaymentEstimateRevenues();
    }

    @RequestMapping("getPaymentEstimateRevenueAll")
    @ResponseBody
    public JSONObject getPaymentEstimateRevenueAll() {
        return paymentEstimateRevenueService.getPaymentEstimateRevenueAll();
    }

    @RequestMapping("getPaymentEstimateRevenueCount")
    @ResponseBody
    public JSONObject getPaymentEstimateRevenueCount() {
        return paymentEstimateRevenueService.getPaymentEstimateRevenueCount();
    }

    @RequestMapping("changePaymentEstimateRevenue")
    @ResponseBody
    public JSONObject changePaymentEstimateRevenue() {
        return paymentEstimateRevenueService.changePaymentEstimateRevenue();
    }

    @RequestMapping("changePaymentEstimateRevenueAccountingTime")
    @ResponseBody
    public JSONObject changePaymentEstimateRevenueAccountingTime() {
        return paymentEstimateRevenueService.changePaymentEstimateRevenueAccountingTime();
    }
    @RequestMapping("changePaymentEstimateRevenueRequestTime")
    @ResponseBody
    public JSONObject changePaymentEstimateRevenueRequestTime() {
        return paymentEstimateRevenueService.changePaymentEstimateRevenueRequestTime();
    }

}
