package com.tuoniaostore.payment.controller;
        import com.tuoniaostore.payment.service.PaymentCurrencyOffsetLoggerService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Controller
@RequestMapping("paymentCurrencyOffsetLogger")
public class PaymentCurrencyOffsetLoggerController {
    @Autowired
    private PaymentCurrencyOffsetLoggerService paymentCurrencyOffsetLoggerService;


    @RequestMapping("addPaymentCurrencyOffsetLogger")
    @ResponseBody
    public JSONObject addPaymentCurrencyOffsetLogger() {
        return paymentCurrencyOffsetLoggerService.addPaymentCurrencyOffsetLogger();
    }

    @RequestMapping("getPaymentCurrencyOffsetLogger")
    @ResponseBody
    public JSONObject getPaymentCurrencyOffsetLogger() {
        return paymentCurrencyOffsetLoggerService.getPaymentCurrencyOffsetLogger();
    }


    @RequestMapping("getPaymentCurrencyOffsetLoggers")
    @ResponseBody
    public JSONObject getPaymentCurrencyOffsetLoggers() {
        return paymentCurrencyOffsetLoggerService.getPaymentCurrencyOffsetLoggers();
    }

    @RequestMapping("getPaymentCurrencyOffsetLoggerAll")
    @ResponseBody
    public JSONObject getPaymentCurrencyOffsetLoggerAll() {
        return paymentCurrencyOffsetLoggerService.getPaymentCurrencyOffsetLoggerAll();
    }

    @RequestMapping("getPaymentCurrencyOffsetLoggerCount")
    @ResponseBody
    public JSONObject getPaymentCurrencyOffsetLoggerCount() {
        return paymentCurrencyOffsetLoggerService.getPaymentCurrencyOffsetLoggerCount();
    }

    @RequestMapping("changePaymentCurrencyOffsetLogger")
    @ResponseBody
    public JSONObject changePaymentCurrencyOffsetLogger() {
        return paymentCurrencyOffsetLoggerService.changePaymentCurrencyOffsetLogger();
    }

    /**
     * 获取今日流水记录 支出
     * @return
     */
    @RequestMapping("getDayIncomePaymentCurrencyOffsetLoggers")
    @ResponseBody
    public JSONObject getDayIncomePaymentCurrencyOffsetLoggers() {
        return paymentCurrencyOffsetLoggerService.getDayPaymentCurrencyOffsetLoggers();
    }

    /**
     * 获取今日流水记录 收入
     * @return
     */
    @RequestMapping("getDayInPaymentCurrencyOffsetLoggers")
    @ResponseBody
    public JSONObject getDayInPaymentCurrencyOffsetLoggers() {
        return paymentCurrencyOffsetLoggerService.getDayInPaymentCurrencyOffsetLoggers();
    }

    /**
     * 获取 具体哪天的流水记录 收入
     * @return
     */
    @RequestMapping("getSelectDayIPaymentCurrencyOffsetLoggers")
    @ResponseBody
    public JSONObject getSelectDayIPaymentCurrencyOffsetLoggers() {
        return paymentCurrencyOffsetLoggerService.getSelectDayIPaymentCurrencyOffsetLoggers();
    }


    /**
     * 获取每月流水记录 支出
     * @return
     */
    @RequestMapping("getMonthPaymentCurrencyOffsetLoggers")
    @ResponseBody
    public JSONObject getMonthPaymentCurrencyOffsetLoggers() {
        return paymentCurrencyOffsetLoggerService.getMonthPaymentCurrencyOffsetLoggers();
    }


    /**
     * 获取每月流水记录 收入
     * @return
     */
    @RequestMapping("getMonthInPaymentCurrencyOffsetLoggers")
    @ResponseBody
    public JSONObject getMonthInPaymentCurrencyOffsetLoggers() {
        return paymentCurrencyOffsetLoggerService.getMonthInPaymentCurrencyOffsetLoggers();
    }

    /**
     * 获取流水记录 根据条件查找 收支、类型 统计
     * @return
     */
    @RequestMapping("getPaymentCurrencyOffsetLoggersByParam")
    @ResponseBody
    public JSONObject getPaymentCurrencyOffsetLoggersByParam() {
        return paymentCurrencyOffsetLoggerService.getPaymentCurrencyOffsetLoggersByParam();
    }

    /***
     * 统计供应商的收入
     * @author oy
     * @date 2019/5/1
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getPaymentCurrencyOffsetLoggerTotalMoneyByUser")
    @ResponseBody
    public JSONObject getPaymentCurrencyOffsetLoggerTotalMoneyByUser() {
        return paymentCurrencyOffsetLoggerService.getPaymentCurrencyOffsetLoggerTotalMoneyByUser();
    }

    /**
     * 供应商收入详情列表
     * @author oy
     * @date 2019/5/1
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getPaymentCurrentOffsetLoggerByUserPage")
    @ResponseBody
    public JSONObject getPaymentCurrentOffsetLoggerByUserPage() {
        return paymentCurrencyOffsetLoggerService.getPaymentCurrentOffsetLoggerByUserPage();
    }

    /**
     * 获取当前用户的账单详情
     * @author oy
     * @date 2019/6/11
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getPaymentCurrentOffsetLoggerAllByUserPage")
    @ResponseBody
    public JSONObject getPaymentCurrentOffsetLoggerAllByUserPage() {
        return paymentCurrencyOffsetLoggerService.getPaymentCurrentOffsetLoggerAllByUserPage();
    }

    @RequestMapping("addPaymentCurrencyOffsetLoggerJson")
    @ResponseBody
    public JSONObject addPaymentCurrencyOffsetLoggerJson() {
        return paymentCurrencyOffsetLoggerService.addPaymentCurrencyOffsetLoggerJson();
    }

}
