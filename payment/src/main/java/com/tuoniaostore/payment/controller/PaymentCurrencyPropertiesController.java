package com.tuoniaostore.payment.controller;
        import com.tuoniaostore.payment.service.PaymentCurrencyPropertiesService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;

/**
 * 
 * 用户账号 key-value 表
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Controller
@RequestMapping("paymentCurrencyProperties")
public class PaymentCurrencyPropertiesController {
    @Autowired
    private PaymentCurrencyPropertiesService paymentCurrencyPropertiesService;


    @RequestMapping("addPaymentCurrencyProperties")
    @ResponseBody
    public JSONObject addPaymentCurrencyProperties() {
        return paymentCurrencyPropertiesService.addPaymentCurrencyProperties();
    }

    @RequestMapping("getPaymentCurrencyProperties")
    @ResponseBody
    public JSONObject getPaymentCurrencyProperties() {
        return paymentCurrencyPropertiesService.getPaymentCurrencyProperties();
    }


    @RequestMapping("getPaymentCurrencyPropertiess")
    @ResponseBody
    public JSONObject getPaymentCurrencyPropertiess() {
        return paymentCurrencyPropertiesService.getPaymentCurrencyPropertiess();
    }

    @RequestMapping("getPaymentCurrencyPropertiesAll")
    @ResponseBody
    public JSONObject getPaymentCurrencyPropertiesAll() {
        return paymentCurrencyPropertiesService.getPaymentCurrencyPropertiesAll();
    }

    @RequestMapping("getPaymentCurrencyPropertiesCount")
    @ResponseBody
    public JSONObject getPaymentCurrencyPropertiesCount() {
        return paymentCurrencyPropertiesService.getPaymentCurrencyPropertiesCount();
    }

    @RequestMapping("changePaymentCurrencyProperties")
    @ResponseBody
    public JSONObject changePaymentCurrencyProperties() {
        return paymentCurrencyPropertiesService.changePaymentCurrencyProperties();
    }


    /**
     * 修改支付密码 短信验证码
     * @return
     */
    @RequestMapping("changePaymentPasswordMsg")
    @ResponseBody
    public JSONObject changePaymentPasswordMsg() {
        return paymentCurrencyPropertiesService.changePaymentPasswordMsg();
    }


    /**
     * 修改支付密码
     * @return
     */
    @RequestMapping("changePaymentPassword")
    @ResponseBody
    public JSONObject changePaymentPassword() {
        return paymentCurrencyPropertiesService.changePaymentPassword();
    }

    /**
    * 重置密码  根据手机验证码 登录密码 修改新的密码
    * @return
    */
    @RequestMapping("changePaymentPassword2")
    @ResponseBody
    public JSONObject changePaymentPassword2() {
        return paymentCurrencyPropertiesService.changePaymentPassword2();
    }

    /**
    * 获取当前用户支付密码
    * @return
    */
    @RequestMapping("getPaymentPasswordByUserId")
    @ResponseBody
    public JSONObject getPaymentPasswordByUserId() {
        return paymentCurrencyPropertiesService.getPaymentPasswordByUserId();
    }



}
