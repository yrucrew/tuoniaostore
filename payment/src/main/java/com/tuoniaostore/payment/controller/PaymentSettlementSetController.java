package com.tuoniaostore.payment.controller;
        import com.tuoniaostore.payment.service.PaymentSettlementSetService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Controller
@RequestMapping("paymentSettlementSet")
public class PaymentSettlementSetController {
    @Autowired
    private PaymentSettlementSetService paymentSettlementSetService;


    @RequestMapping("addPaymentSettlementSet")
    @ResponseBody
    public JSONObject addPaymentSettlementSet() {
        return paymentSettlementSetService.addPaymentSettlementSet();
    }

    @RequestMapping("getPaymentSettlementSet")
    @ResponseBody
    public JSONObject getPaymentSettlementSet() {
        return paymentSettlementSetService.getPaymentSettlementSet();
    }


    @RequestMapping("getPaymentSettlementSets")
    @ResponseBody
    public JSONObject getPaymentSettlementSets() {
        return paymentSettlementSetService.getPaymentSettlementSets();
    }

    @RequestMapping("getPaymentSettlementSetAll")
    @ResponseBody
    public JSONObject getPaymentSettlementSetAll() {
        return paymentSettlementSetService.getPaymentSettlementSetAll();
    }

    @RequestMapping("getPaymentSettlementSetCount")
    @ResponseBody
    public JSONObject getPaymentSettlementSetCount() {
        return paymentSettlementSetService.getPaymentSettlementSetCount();
    }

    @RequestMapping("changePaymentSettlementSet")
    @ResponseBody
    public JSONObject changePaymentSettlementSet() {
        return paymentSettlementSetService.changePaymentSettlementSet();
    }

    @RequestMapping("getPaymentSettlementSetByUserId")
    @ResponseBody
    public JSONObject getPaymentSettlementSetByUserId() {
        return paymentSettlementSetService.getPaymentSettlementSetByUserId();
    }

}
