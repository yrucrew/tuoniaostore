package com.tuoniaostore.payment.controller;

import com.tuoniaostore.payment.service.PaymentTakeRuleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@RestController
@RequestMapping("paymentTakeRule")
public class PaymentTakeRuleController {
    @Autowired
    private PaymentTakeRuleService paymentTakeRuleService;


    @RequestMapping("addPaymentTakeRule")
    public JSONObject addPaymentTakeRule() {
        return paymentTakeRuleService.addPaymentTakeRule();
    }

    @RequestMapping("getPaymentTakeRule")
    public JSONObject getPaymentTakeRule() {
        return paymentTakeRuleService.getPaymentTakeRule();
    }


    @RequestMapping("getPaymentTakeRules")
    public JSONObject getPaymentTakeRules() {
        return paymentTakeRuleService.getPaymentTakeRules();
    }

    @RequestMapping("getPaymentTakeRuleAll")
    public JSONObject getPaymentTakeRuleAll() {
        return paymentTakeRuleService.getPaymentTakeRuleAll();
    }

    @RequestMapping("getPaymentTakeRuleCount")
    public JSONObject getPaymentTakeRuleCount() {
        return paymentTakeRuleService.getPaymentTakeRuleCount();
    }

    @RequestMapping("changePaymentTakeRule")
    public JSONObject changePaymentTakeRule() {
        return paymentTakeRuleService.changePaymentTakeRule();
    }

    @ApiOperation("修改默认选项")
    @RequestMapping("changeDefaultOption")
    public JSONObject changeDefaultOption() {
        return paymentTakeRuleService.changeDefaultOption();
    }

    @ApiOperation("批量逻辑删除")
    @RequestMapping("/delPaymentTakeRule")
    public JSONObject delPaymentTakeRule() {
        return paymentTakeRuleService.delPaymentTakeRule();
    }
}
