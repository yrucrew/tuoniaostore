package com.tuoniaostore.payment.cache.impl;

import com.tuoniaostore.datamodel.payment.PaymentBank;
import com.tuoniaostore.payment.cache.PaymentBankCache;
import com.tuoniaostore.payment.data.PaymentBankMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@CacheConfig(cacheNames = "paymentBank")
public class PaymentBankCacheImpl implements PaymentBankCache {

    @Autowired
    PaymentBankMapper mapper;

    @Override
    @CacheEvict(value = "paymentBank", allEntries = true)
    public void addPaymentBank(PaymentBank paymentBank) {
        mapper.addPaymentBank(paymentBank);
    }

    @Override
    @Cacheable(value = "paymentBank", key = "'getPaymentBank_'+#id")
    public PaymentBank getPaymentBank(String id) {
        return mapper.getPaymentBank(id);
    }

    @Override
    @Cacheable(value = "paymentBank", key = "'getPaymentBanks_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<PaymentBank> getPaymentBanks( int status,int pageIndex, int pageSize, String name) {
        return mapper.getPaymentBanks( status, pageIndex, pageSize,name);
    }

    @Override
    @Cacheable(value = "paymentBank", key = "'getPaymentBankAll'")
    public List<PaymentBank> getPaymentBankAll() {
        return mapper.getPaymentBankAll();
    }

    @Override
    @Cacheable(value = "paymentBank", key = "'getPaymentBankCount_'+#status+'_'+#name")
    public int getPaymentBankCount(int status, String name) {
        return mapper.getPaymentBankCount(status, name);
    }

    @Override
    @CacheEvict(value = "paymentBank", allEntries = true)
    public void changePaymentBank(PaymentBank paymentBank) {
        mapper.changePaymentBank(paymentBank);
    }

    @Override
    @Cacheable(value = "paymentBank", key = "'getPaymentBankByUserId_'+#userId")
    public List<PaymentBank> getPaymentBankByUserId(String userId) {
        return mapper.getPaymentBankByUserId(userId);
    }

    @Override
    @Cacheable(value = "paymentBank", key = "'getPaymentBanksByParam_'+#map+'_'+#status+'_'+#pageIndex+'_'+#pageSize")
    public List<PaymentBank> getPaymentBanksByParam(Map<String, Object> map, int status, int pageIndex, int pageSize) {
        return mapper.getPaymentBanksByParam(map, status, pageIndex, pageSize);
    }

    @Override
    @Cacheable(value = "paymentBank", key = "'getPaymentBankCountByParam_'+#status+'_'+#map")
    public int getPaymentBankCountByParam(int status, Map<String, Object> map) {
        return mapper.getPaymentBankCountByParam(status, map);
    }

    @Override
    @CacheEvict(value = "paymentBank", allEntries = true)
    public void deletePaymentBank(String[] idArr) {
        mapper.deletePaymentBank(idArr);
    }

}
