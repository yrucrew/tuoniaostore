package com.tuoniaostore.payment.cache;
import com.tuoniaostore.datamodel.payment.PaymentEstimateRevenueLogger;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-08 10:21:40
 */
public interface PaymentEstimateRevenueLoggerCache {

    void addPaymentEstimateRevenueLogger(PaymentEstimateRevenueLogger paymentEstimateRevenueLogger);

    PaymentEstimateRevenueLogger getPaymentEstimateRevenueLogger(String id);

    List<PaymentEstimateRevenueLogger> getPaymentEstimateRevenueLoggers(Map<String, Object> map, int pageIndex, int pageSize);
    List<PaymentEstimateRevenueLogger> getPaymentEstimateRevenueLoggerAll();
    int getPaymentEstimateRevenueLoggerCount(Map<String, Object> map);
    void changePaymentEstimateRevenueLogger(PaymentEstimateRevenueLogger paymentEstimateRevenueLogger);

}
