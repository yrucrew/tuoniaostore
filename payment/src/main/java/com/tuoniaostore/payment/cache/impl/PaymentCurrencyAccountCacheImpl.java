package com.tuoniaostore.payment.cache.impl;

import com.tuoniaostore.datamodel.payment.PaymentCurrencyAccount;
import com.tuoniaostore.payment.cache.PaymentCurrencyAccountCache;
import com.tuoniaostore.payment.data.PaymentCurrencyAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@CacheConfig(cacheNames = "paymentCurrencyAccount")
public class PaymentCurrencyAccountCacheImpl implements PaymentCurrencyAccountCache {

    @Autowired
    PaymentCurrencyAccountMapper mapper;

    @Override
    @CacheEvict(value = "paymentCurrencyAccount", allEntries = true)
    public void addPaymentCurrencyAccount(PaymentCurrencyAccount paymentCurrencyAccount) {
        mapper.addPaymentCurrencyAccount(paymentCurrencyAccount);
    }

    @Override
    @Cacheable(value = "paymentCurrencyAccount", key = "'getPaymentCurrencyAccount_'+#id")
    public   PaymentCurrencyAccount getPaymentCurrencyAccount(String id) {
        return mapper.getPaymentCurrencyAccount(id);
    }

    @Override
    @Cacheable(value = "paymentCurrencyAccount", key = "'getPaymentCurrencyAccounts_'+#pageIndex+'_'+#pageSize+'_'+#map")
    public List<PaymentCurrencyAccount> getPaymentCurrencyAccounts(int pageIndex, int pageSize, Map<String,Object> map) {
        return mapper.getPaymentCurrencyAccounts(pageIndex, pageSize, map);
    }

    @Override
    @Cacheable(value = "paymentCurrencyAccount", key = "'getPaymentCurrencyAccountAll'")
    public List<PaymentCurrencyAccount> getPaymentCurrencyAccountAll() {
        return mapper.getPaymentCurrencyAccountAll();
    }

    @Override
    @Cacheable(value = "paymentCurrencyAccount", key = "'getPaymentCurrencyAccountCount_'+#map")
    public int getPaymentCurrencyAccountCount(Map<String,Object> map) {
        return mapper.getPaymentCurrencyAccountCount(map);
    }

    @Override
    @CacheEvict(value = "paymentCurrencyAccount", allEntries = true)
    public void changePaymentCurrencyAccount(PaymentCurrencyAccount paymentCurrencyAccount) {
        mapper.changePaymentCurrencyAccount(paymentCurrencyAccount);
    }

    @Override
    @Cacheable(value = "paymentCurrencyAccount", key = "'getPaymentCurrencyAccountByUserId_'+#userId+'_'+#type")
    public PaymentCurrencyAccount getPaymentCurrencyAccountByUserId(String userId,int type) {
        return mapper.getPaymentCurrencyAccountByUserId(userId,type);
    }

    @Override
    @CacheEvict(value = "paymentCurrencyAccount", allEntries = true)
    public void changePaymentCurrencyAccountWithWithdrawl(String id, long money, int type) {
        mapper.changePaymentCurrencyAccountWithWithdrawl(id,money,type);
    }

    @Override
    @CacheEvict(value = "paymentCurrencyAccount", allEntries = true)
    public void changePaymentCurrencyAccountWithWithdrawlAdd(String id, long money, int type) {
        mapper.changePaymentCurrencyAccountWithWithdrawlAdd(id,money,type);
    }

}
