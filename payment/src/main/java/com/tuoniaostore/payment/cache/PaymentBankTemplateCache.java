package com.tuoniaostore.payment.cache;
import com.tuoniaostore.datamodel.payment.PaymentBankTemplate;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentBankTemplateCache {

    void addPaymentBankTemplate(PaymentBankTemplate paymentBankTemplate);

    PaymentBankTemplate getPaymentBankTemplate(String id);

    List<PaymentBankTemplate> getPaymentBankTemplates(int pageIndex, int pageSize, String name);
    List<PaymentBankTemplate> getPaymentBankTemplateAll();
    int getPaymentBankTemplateCount( String name);
    void changePaymentBankTemplate(PaymentBankTemplate paymentBankTemplate);

    /**
     * 根据id 获取list
     * @return
     */
    List<PaymentBankTemplate> getPaymentBankTemplateById(List<String> list);

    List<PaymentBankTemplate> getPaymentBankTemplatesByParam(int pageIndex, int pageSize, Map<String, Object> map);

    int getPaymentBankTemplateCountByParam(Map<String, Object> map);

    void deletePaymentBankTemplate(String[] idArr);
}
