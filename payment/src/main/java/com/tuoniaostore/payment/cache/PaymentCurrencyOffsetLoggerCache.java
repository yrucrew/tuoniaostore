package com.tuoniaostore.payment.cache;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyOffsetLogger;
import com.tuoniaostore.datamodel.vo.payment.PaymentCurrencyOffsetLoggerVO;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentCurrencyOffsetLoggerCache {

    void addPaymentCurrencyOffsetLogger(PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger);

    PaymentCurrencyOffsetLogger getPaymentCurrencyOffsetLogger(String id);

    List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggers(int pageIndex, int pageSize,  Map<String, Object> map);
    List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggerAll();
    int getPaymentCurrencyOffsetLoggerCount( Map<String, Object> map);
    int getPaymentCurrencyOffsetLoggerCountByParam(String name);
    void changePaymentCurrencyOffsetLogger(PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger);

    /**
     * 根据用户，类型 查找流水记录 time：day month 查询当天 或者 当月的
     * @param userId
     * @param
     */
    List<PaymentCurrencyOffsetLogger> getTodayPaymentCurrencyOffsetLoggerByUserId(String userId,int relationTransType,String time,Integer limitStart,Integer limitEnd);


    /**
     * 根据用户，类型 查找流水记录 time：给具体的时间 string 类型 "2019-03-22"
     * @param userId
     * @param
     */
    List<PaymentCurrencyOffsetLogger> getSelectTodayPaymentCurrencyOffsetLoggerByUserId(String userId,int relationTransType,String time,Integer limitStart,Integer limitEnd);


    /**
     * 根据用户，类型 查找流水记录 返回的结果 时间类型的话 2019-03-23
     * @param userId
     * @param
     */
    List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggersByParam(String userId, Integer relationTransType);

    /**
     * 查询具体某天的 记录数 和 金额数
     * @author oy
     * @date 2019/3/31
     * @param userId, relationTransType, time
     * @return java.util.Map<java.lang.String,java.lang.Long>
     */
    Map<String, Object> getCountTodayPaymentCurrencyOffsetLoggerByUserId(String userId, int relationTransType, String time);

    /**
     * 根据用户和支入 支出类型
     * @author oy
     * @date 2019/5/1
     * @param userList, type
     * @return java.util.List<com.tuoniaostore.datamodel.vo.payment.PaymentCurrencyOffsetLoggerVO>
     */
    List<PaymentCurrencyOffsetLoggerVO> getPaymentCurrencyOffsetLoggerTotalMoneyByUser(List<String> userList, int type);

    List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggersByUserAndParam(String userId, Map<String, Object> map);

    int getCountPaymentCurrencyOffsetLoggersByUserAndParam(String userId, Map<String, Object> map);

    Map<String, Object> getPaymentCurrencyOffsetLoggerForInOut(String userId, Integer currencyType, String startTime, String endTime);

}
