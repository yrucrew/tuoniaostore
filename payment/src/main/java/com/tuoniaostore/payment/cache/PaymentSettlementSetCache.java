package com.tuoniaostore.payment.cache;
import com.tuoniaostore.datamodel.payment.PaymentSettlementSet;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentSettlementSetCache {

    void addPaymentSettlementSet(PaymentSettlementSet paymentSettlementSet);

    PaymentSettlementSet getPaymentSettlementSet(String id);

    List<PaymentSettlementSet> getPaymentSettlementSets(int pageIndex, int pageSize, Map<String,Object> map);
    List<PaymentSettlementSet> getPaymentSettlementSetAll();
    int getPaymentSettlementSetCount(Map<String,Object> map);
    void changePaymentSettlementSet(PaymentSettlementSet paymentSettlementSet);

    PaymentSettlementSet getPaymentSettlementSetByUserId(String userId);
}
