package com.tuoniaostore.payment.cache;
import com.tuoniaostore.datamodel.payment.PaymentBank;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentBankCache {

    void addPaymentBank(PaymentBank paymentBank);

    PaymentBank getPaymentBank(String id);

    List<PaymentBank> getPaymentBanks(int pageIndex, int pageSize, int status, String name);
    List<PaymentBank> getPaymentBankAll();
    int getPaymentBankCount(int status, String name);
    void changePaymentBank(PaymentBank paymentBank);

    /**
     * 根据用户 查找银行卡信息
     * @param userId
     * @return
     */
    List<PaymentBank> getPaymentBankByUserId(String userId);

    List<PaymentBank> getPaymentBanksByParam(Map<String, Object> map, int status, int pageIndex, int pageSize);

    int getPaymentBankCountByParam(int status, Map<String, Object> map);

    void deletePaymentBank(String[] idArr);
}
