package com.tuoniaostore.payment.cache.impl;
        import com.tuoniaostore.datamodel.payment.PaymentBankTemplate;
        import com.tuoniaostore.payment.cache.PaymentBankTemplateCache;
        import com.tuoniaostore.payment.data.PaymentBankTemplateMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;
        import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@CacheConfig(cacheNames = "paymentBankTemplate")
public class PaymentBankTemplateCacheImpl implements PaymentBankTemplateCache {

    @Autowired
    PaymentBankTemplateMapper mapper;

    @Override
    @CacheEvict(value = "paymentBankTemplate", allEntries = true)
    public void addPaymentBankTemplate(PaymentBankTemplate paymentBankTemplate) {
        mapper.addPaymentBankTemplate(paymentBankTemplate);
    }

    @Override
    @Cacheable(value = "paymentBankTemplate", key = "'getPaymentBankTemplate_'+#id")
    public   PaymentBankTemplate getPaymentBankTemplate(String id) {
        return mapper.getPaymentBankTemplate(id);
    }

    @Override
    @Cacheable(value = "paymentBankTemplate", key = "'getPaymentBankTemplates_'+#pageIndex+'_'+#pageSize+'_'+#name")
    public List<PaymentBankTemplate> getPaymentBankTemplates(int pageIndex, int pageSize,  String name) {
        return mapper.getPaymentBankTemplates(pageIndex, pageSize,  name);
    }

    @Override
    public int getPaymentBankTemplateCountByParam(Map<String, Object> map) {
        return mapper.getPaymentBankTemplateCountByParam(map);
    }

    @Override
    @CacheEvict(value = "paymentBankTemplate", allEntries = true)
    public void deletePaymentBankTemplate(String[] idArr) {
        mapper.deletePaymentBankTemplate(idArr);
    }

    @Override
    @Cacheable(value = "paymentBankTemplate", key = "'getPaymentBankTemplatesByParam_'+#pageIndex+'_'+#pageSize+'_'+#map")
    public List<PaymentBankTemplate> getPaymentBankTemplatesByParam(int pageIndex, int pageSize, Map<String, Object> map) {
        return mapper.getPaymentBankTemplatesByParam(pageIndex, pageSize,  map);
    }

    @Override
    @Cacheable(value = "paymentBankTemplate", key = "'getPaymentBankTemplateAll'")
    public List<PaymentBankTemplate> getPaymentBankTemplateAll() {
        return mapper.getPaymentBankTemplateAll();
    }

    @Override
    @Cacheable(value = "paymentBankTemplate", key = "'getPaymentBankTemplateCount_'+#name")
    public int getPaymentBankTemplateCount( String name) {
        return mapper.getPaymentBankTemplateCount( name);
    }

    @Override
    @CacheEvict(value = "paymentBankTemplate", allEntries = true)
    public void changePaymentBankTemplate(PaymentBankTemplate paymentBankTemplate) {
        mapper.changePaymentBankTemplate(paymentBankTemplate);
    }

    @Override
    public List<PaymentBankTemplate> getPaymentBankTemplateById(List<String> list) {
        return mapper.getPaymentBankTemplateById(list);
    }

}
