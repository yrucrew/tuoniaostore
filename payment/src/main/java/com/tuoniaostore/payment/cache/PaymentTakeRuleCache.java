package com.tuoniaostore.payment.cache;
import com.tuoniaostore.datamodel.payment.PaymentTakeRule;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentTakeRuleCache {

    void addPaymentTakeRule(PaymentTakeRule paymentTakeRule);

    PaymentTakeRule getPaymentTakeRule(String id);

    List<PaymentTakeRule> getPaymentTakeRules(int pageIndex, int pageSize, String name);
    List<PaymentTakeRule> getPaymentTakeRuleAll();
    int getPaymentTakeRuleCount(String name);
    void changePaymentTakeRule(PaymentTakeRule paymentTakeRule);

    List<PaymentTakeRule> getPaymentTakeRuleByMode(Integer mode);

    void changePaymentTakeRuleUnDefault(int mode, int option);

    void delPaymentTakeRule(String[] ids);
}
