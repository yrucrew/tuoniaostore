package com.tuoniaostore.payment.cache.impl;
        import com.tuoniaostore.datamodel.payment.PaymentRechargePresent;
        import com.tuoniaostore.payment.cache.PaymentRechargePresentCache;
        import com.tuoniaostore.payment.data.PaymentRechargePresentMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@CacheConfig(cacheNames = "paymentRechargePresent")
public class PaymentRechargePresentCacheImpl implements PaymentRechargePresentCache {

    @Autowired
    PaymentRechargePresentMapper mapper;

    @Override
    @CacheEvict(value = "paymentRechargePresent", allEntries = true)
    public void addPaymentRechargePresent(PaymentRechargePresent paymentRechargePresent) {
        mapper.addPaymentRechargePresent(paymentRechargePresent);
    }

    @Override
    @Cacheable(value = "paymentRechargePresent", key = "'getPaymentRechargePresent_'+#id")
    public   PaymentRechargePresent getPaymentRechargePresent(String id) {
        return mapper.getPaymentRechargePresent(id);
    }

    @Override
    @Cacheable(value = "paymentRechargePresent", key = "'getPaymentRechargePresents_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<PaymentRechargePresent> getPaymentRechargePresents(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getPaymentRechargePresents(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "paymentRechargePresent", key = "'getPaymentRechargePresentAll'")
    public List<PaymentRechargePresent> getPaymentRechargePresentAll() {
        return mapper.getPaymentRechargePresentAll();
    }

    @Override
    @Cacheable(value = "paymentRechargePresent", key = "'getPaymentRechargePresentCount_'+#status+'_'+#name")
    public int getPaymentRechargePresentCount(int status, String name) {
        return mapper.getPaymentRechargePresentCount(status, name);
    }

    @Override
    @CacheEvict(value = "paymentRechargePresent", allEntries = true)
    public void changePaymentRechargePresent(PaymentRechargePresent paymentRechargePresent) {
        mapper.changePaymentRechargePresent(paymentRechargePresent);
    }

}
