package com.tuoniaostore.payment.cache.impl;
        import com.tuoniaostore.datamodel.payment.PaymentSettlementSet;
        import com.tuoniaostore.payment.cache.PaymentSettlementSetCache;
        import com.tuoniaostore.payment.data.PaymentSettlementSetMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;
        import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@CacheConfig(cacheNames = "paymentSettlementSet")
public class PaymentSettlementSetCacheImpl implements PaymentSettlementSetCache {

    @Autowired
    PaymentSettlementSetMapper mapper;

    @Override
    @CacheEvict(value = "paymentSettlementSet", allEntries = true)
    public void addPaymentSettlementSet(PaymentSettlementSet paymentSettlementSet) {
        mapper.addPaymentSettlementSet(paymentSettlementSet);
    }

    @Override
    @Cacheable(value = "paymentSettlementSet", key = "'getPaymentSettlementSet_'+#id")
    public   PaymentSettlementSet getPaymentSettlementSet(String id) {
        return mapper.getPaymentSettlementSet(id);
    }

    @Override
    @Cacheable(value = "paymentSettlementSet", key = "'getPaymentSettlementSets_'+#pageIndex+'_'+#pageSize+'_'+#map")
    public List<PaymentSettlementSet> getPaymentSettlementSets(int pageIndex, int pageSize,  Map<String,Object> map) {
        return mapper.getPaymentSettlementSets( pageIndex, pageSize, map);
    }

    @Override
    @Cacheable(value = "paymentSettlementSet", key = "'getPaymentSettlementSetAll'")
    public List<PaymentSettlementSet> getPaymentSettlementSetAll() {
        return mapper.getPaymentSettlementSetAll();
    }

    @Override
    @Cacheable(value = "paymentSettlementSet", key = "'getPaymentSettlementSetCount_'+'_'+#map")
    public int getPaymentSettlementSetCount( Map<String,Object> map) {
        return mapper.getPaymentSettlementSetCount(map);
    }

    @Override
    @CacheEvict(value = "paymentSettlementSet", allEntries = true)
    public void changePaymentSettlementSet(PaymentSettlementSet paymentSettlementSet) {
        mapper.changePaymentSettlementSet(paymentSettlementSet);
    }

    @Override
    @Cacheable(value = "paymentSettlementSet", key = "'getPaymentSettlementSetByUserId_'+'_'+#userId")
    public PaymentSettlementSet getPaymentSettlementSetByUserId(String userId) {
        return mapper.getPaymentSettlementSetByUserId(userId);
    }

}
