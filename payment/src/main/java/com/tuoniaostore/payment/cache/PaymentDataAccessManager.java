package com.tuoniaostore.payment.cache;

import com.tuoniaostore.datamodel.payment.*;
import com.tuoniaostore.datamodel.vo.order.PayLogNumbersVo;
import com.tuoniaostore.datamodel.vo.payment.PaymentCurrencyOffsetLoggerVO;
import com.tuoniaostore.payment.vo.PayLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
public class PaymentDataAccessManager {


    @Autowired
    PaymentBankCache paymentBankCache;

    public void addPaymentBank(PaymentBank paymentBank) {
        paymentBankCache.addPaymentBank(paymentBank);
    }

    public PaymentBank getPaymentBank(String id) {
        return paymentBankCache.getPaymentBank(id);
    }

    public List<PaymentBank> getPaymentBanks(int status, int pageIndex, int pageSize, String name) {
        return paymentBankCache.getPaymentBanks(status, pageIndex, pageSize, name);
    }

    public List<PaymentBank> getPaymentBanksByParam(Map<String, Object> map, int status, int pageIndex, int pageSize) {
        return paymentBankCache.getPaymentBanksByParam(map ,status, pageIndex, pageSize);
    }

    public List<PaymentBank> getPaymentBankAll() {
        return paymentBankCache.getPaymentBankAll();
    }

    public int getPaymentBankCount(int status, String name) {
        return paymentBankCache.getPaymentBankCount(status, name);
    }

    public int getPaymentBankCountByParam(int status, Map<String, Object> map) {
        return paymentBankCache.getPaymentBankCountByParam(status, map);
    }

    public void changePaymentBank(PaymentBank paymentBank) {
        paymentBankCache.changePaymentBank(paymentBank);
    }

    public List<PaymentBank> getPaymentBankByUserId(String userId) {
        return paymentBankCache.getPaymentBankByUserId(userId);
    }

    public void deletePaymentBank(String[] idArr) {
        paymentBankCache.deletePaymentBank(idArr);
    }


    @Autowired
    PaymentBankTemplateCache paymentBankTemplateCache;

    public void addPaymentBankTemplate(PaymentBankTemplate paymentBankTemplate) {
        paymentBankTemplateCache.addPaymentBankTemplate(paymentBankTemplate);
    }

    public   PaymentBankTemplate getPaymentBankTemplate(String id) {
        return paymentBankTemplateCache.getPaymentBankTemplate(id);
    }

    public List<PaymentBankTemplate> getPaymentBankTemplates(int pageIndex, int pageSize, String name) {
        return paymentBankTemplateCache.getPaymentBankTemplates(pageIndex, pageSize, name);
    }

    public List<PaymentBankTemplate> getPaymentBankTemplatesByParam(int pageIndex, int pageSize, Map<String, Object> map) {
        return paymentBankTemplateCache.getPaymentBankTemplatesByParam(pageIndex, pageSize, map);
    }

    public List<PaymentBankTemplate> getPaymentBankTemplateAll() {
        return paymentBankTemplateCache.getPaymentBankTemplateAll();
    }

    public int getPaymentBankTemplateCount( String name) {
        return paymentBankTemplateCache.getPaymentBankTemplateCount( name);
    }

    public int getPaymentBankTemplateCountByParam(Map<String, Object> map) {
        return paymentBankTemplateCache.getPaymentBankTemplateCountByParam( map);
    }

    public void changePaymentBankTemplate(PaymentBankTemplate paymentBankTemplate) {
        paymentBankTemplateCache.changePaymentBankTemplate(paymentBankTemplate);
    }

    public List<PaymentBankTemplate> getPaymentBankTemplateById(List<String> list) {
        return paymentBankTemplateCache.getPaymentBankTemplateById(list);
    }

    public void deletePaymentBankTemplate(String[] idArr) {
        paymentBankTemplateCache.deletePaymentBankTemplate(idArr);
    }

    @Autowired
    PaymentCurrencyAccountCache paymentCurrencyAccountCache;

    public void addPaymentCurrencyAccount(PaymentCurrencyAccount paymentCurrencyAccount) {
        paymentCurrencyAccountCache.addPaymentCurrencyAccount(paymentCurrencyAccount);
    }

    public   PaymentCurrencyAccount getPaymentCurrencyAccount(String id) {
        return paymentCurrencyAccountCache.getPaymentCurrencyAccount(id);
    }

    public List<PaymentCurrencyAccount> getPaymentCurrencyAccounts(int pageIndex, int pageSize,Map<String,Object> map) {
        return paymentCurrencyAccountCache.getPaymentCurrencyAccounts(pageIndex, pageSize, map);
    }

    public List<PaymentCurrencyAccount> getPaymentCurrencyAccountAll() {
        return paymentCurrencyAccountCache.getPaymentCurrencyAccountAll();
    }

    public int getPaymentCurrencyAccountCount(Map<String,Object> map) {
        return paymentCurrencyAccountCache.getPaymentCurrencyAccountCount(map);
    }

    public void changePaymentCurrencyAccount(PaymentCurrencyAccount paymentCurrencyAccount) {
        paymentCurrencyAccountCache.changePaymentCurrencyAccount(paymentCurrencyAccount);
    }

    public PaymentCurrencyAccount getPaymentCurrencyAccountByUserId(String userId,int type){
        return paymentCurrencyAccountCache.getPaymentCurrencyAccountByUserId(userId,type);
    }

    public void changePaymentCurrencyAccountWithWithdrawl(String id, long money,int type) {
        paymentCurrencyAccountCache.changePaymentCurrencyAccountWithWithdrawl(id,money,type);
    }

    public void changePaymentCurrencyAccountWithWithdrawlAdd(String id, long money,int type) {
        paymentCurrencyAccountCache.changePaymentCurrencyAccountWithWithdrawlAdd(id,money,type);
    }

    @Autowired
    PaymentCurrencyOffsetLoggerCache paymentCurrencyOffsetLoggerCache;

    public void addPaymentCurrencyOffsetLogger(PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger) {
        paymentCurrencyOffsetLoggerCache.addPaymentCurrencyOffsetLogger(paymentCurrencyOffsetLogger);
    }

    public   PaymentCurrencyOffsetLogger getPaymentCurrencyOffsetLogger(String id) {
        return paymentCurrencyOffsetLoggerCache.getPaymentCurrencyOffsetLogger(id);
    }

    public List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggers(int pageIndex, int pageSize, Map<String, Object> map) {
        return paymentCurrencyOffsetLoggerCache.getPaymentCurrencyOffsetLoggers(pageIndex, pageSize, map);
    }

    public List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggerAll() {
        return paymentCurrencyOffsetLoggerCache.getPaymentCurrencyOffsetLoggerAll();
    }

    public int getPaymentCurrencyOffsetLoggerCount( Map<String, Object> map) {
        return paymentCurrencyOffsetLoggerCache.getPaymentCurrencyOffsetLoggerCount( map);
    }

    public int getPaymentCurrencyOffsetLoggerCountByParam( String name) {
        return paymentCurrencyOffsetLoggerCache.getPaymentCurrencyOffsetLoggerCountByParam( name);
    }

    public void changePaymentCurrencyOffsetLogger(PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger) {
        paymentCurrencyOffsetLoggerCache.changePaymentCurrencyOffsetLogger(paymentCurrencyOffsetLogger);
    }

    public List<PaymentCurrencyOffsetLogger> getTodayPaymentCurrencyOffsetLoggerByUserId(String userId,int relationTransType,String time,Integer limitStart,Integer limitEnd) {
        return paymentCurrencyOffsetLoggerCache.getTodayPaymentCurrencyOffsetLoggerByUserId(userId,relationTransType,time,limitStart,limitEnd);
    }

    public List<PaymentCurrencyOffsetLogger> getSelectTodayPaymentCurrencyOffsetLoggerByUserId(String userId,int relationTransType,String time,Integer limitStart,Integer limitEnd) {
        return paymentCurrencyOffsetLoggerCache.getSelectTodayPaymentCurrencyOffsetLoggerByUserId(userId,relationTransType,time,limitStart,limitEnd);
    }

    public List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggersByParam(String userId, Integer relationTransType) {
        return paymentCurrencyOffsetLoggerCache.getPaymentCurrencyOffsetLoggersByParam(userId,relationTransType);
    }

    public Map<String, Object> getCountTodayPaymentCurrencyOffsetLoggerByUserId(String userId, int relationTransType, String time) {
        return paymentCurrencyOffsetLoggerCache.getCountTodayPaymentCurrencyOffsetLoggerByUserId(userId,relationTransType,time);
    }

    public List<PaymentCurrencyOffsetLoggerVO> getPaymentCurrencyOffsetLoggerTotalMoneyByUser(List<String> userList, int type) {
        return paymentCurrencyOffsetLoggerCache.getPaymentCurrencyOffsetLoggerTotalMoneyByUser(userList,type);
    }

    public List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggersByUserAndParam(String userId, Map<String, Object> map) {
        return paymentCurrencyOffsetLoggerCache.getPaymentCurrencyOffsetLoggersByUserAndParam(userId,map);
    }

    public int getCountPaymentCurrencyOffsetLoggersByUserAndParam(String userId, Map<String, Object> map) {
        return paymentCurrencyOffsetLoggerCache.getCountPaymentCurrencyOffsetLoggersByUserAndParam(userId,map);
    }

    public Map<String, Object> getPaymentCurrencyOffsetLoggerForInOut(String userId, Integer currencyType, String startTime, String endTime) {
        return paymentCurrencyOffsetLoggerCache.getPaymentCurrencyOffsetLoggerForInOut(userId,currencyType,startTime,endTime);
    }

    @Autowired
    PaymentCurrencyPropertiesCache paymentCurrencyPropertiesCache;

    public void addPaymentCurrencyProperties(PaymentCurrencyProperties paymentCurrencyProperties) {
        paymentCurrencyPropertiesCache.addPaymentCurrencyProperties(paymentCurrencyProperties);
    }

    public   PaymentCurrencyProperties getPaymentCurrencyProperties(String id) {
        return paymentCurrencyPropertiesCache.getPaymentCurrencyProperties(id);
    }

    public List<PaymentCurrencyProperties> getPaymentCurrencyPropertiess(int status,int pageIndex, int pageSize, String name) {
        return paymentCurrencyPropertiesCache.getPaymentCurrencyPropertiess(status,pageIndex, pageSize, name);
    }

    public List<PaymentCurrencyProperties> getPaymentCurrencyPropertiesAll() {
        return paymentCurrencyPropertiesCache.getPaymentCurrencyPropertiesAll();
    }

    public int getPaymentCurrencyPropertiesCount(int status, String name) {
        return paymentCurrencyPropertiesCache.getPaymentCurrencyPropertiesCount(status, name);
    }

    public void changePaymentCurrencyProperties(PaymentCurrencyProperties paymentCurrencyProperties) {
        paymentCurrencyPropertiesCache.changePaymentCurrencyProperties(paymentCurrencyProperties);
    }

    public PaymentCurrencyProperties getPaymentCurrencyPropertiesByUserId(String userId,int type) {
        return paymentCurrencyPropertiesCache.getPaymentCurrencyPropertiesByUserId(userId,type);
    }

    public void updatePaymentCurrencyPropertiesPassword(PaymentCurrencyProperties paymentCurrencyProperties) {
        paymentCurrencyPropertiesCache.updatePaymentCurrencyPropertiesPassword(paymentCurrencyProperties);
    }

    @Autowired
    PaymentRechargePresentCache paymentRechargePresentCache;

    public void addPaymentRechargePresent(PaymentRechargePresent paymentRechargePresent) {
        paymentRechargePresentCache.addPaymentRechargePresent(paymentRechargePresent);
    }

    public   PaymentRechargePresent getPaymentRechargePresent(String id) {
        return paymentRechargePresentCache.getPaymentRechargePresent(id);
    }
    public List<PaymentRechargePresent> getPaymentRechargePresents(int status,int pageIndex, int pageSize, String name) {
        return paymentRechargePresentCache.getPaymentRechargePresents(status,pageIndex, pageSize, name);
    }

    public List<PaymentRechargePresent> getPaymentRechargePresentAll() {
        return paymentRechargePresentCache.getPaymentRechargePresentAll();
    }

    public int getPaymentRechargePresentCount(int status, String name) {
        return paymentRechargePresentCache.getPaymentRechargePresentCount(status, name);
    }

    public void changePaymentRechargePresent(PaymentRechargePresent paymentRechargePresent) {
        paymentRechargePresentCache.changePaymentRechargePresent(paymentRechargePresent);
    }

    @Autowired
    PaymentSettlementSetCache paymentSettlementSetCache;

    public void addPaymentSettlementSet(PaymentSettlementSet paymentSettlementSet) {
        paymentSettlementSetCache.addPaymentSettlementSet(paymentSettlementSet);
    }

    public   PaymentSettlementSet getPaymentSettlementSet(String id) {
        return paymentSettlementSetCache.getPaymentSettlementSet(id);
    }

    public List<PaymentSettlementSet> getPaymentSettlementSets(int pageIndex, int pageSize, Map<String,Object> map) {
        return paymentSettlementSetCache.getPaymentSettlementSets(pageIndex, pageSize, map);
    }

    public List<PaymentSettlementSet> getPaymentSettlementSetAll() {
        return paymentSettlementSetCache.getPaymentSettlementSetAll();
    }

    public int getPaymentSettlementSetCount( Map<String,Object> map) {
        return paymentSettlementSetCache.getPaymentSettlementSetCount( map);
    }

    public void changePaymentSettlementSet(PaymentSettlementSet paymentSettlementSet) {
        paymentSettlementSetCache.changePaymentSettlementSet(paymentSettlementSet);
    }

    public PaymentSettlementSet getPaymentSettlementSetByUserId(String userId) {
        return paymentSettlementSetCache.getPaymentSettlementSetByUserId(userId);
    }

    @Autowired
    PaymentTakeRuleCache paymentTakeRuleCache;

    public void addPaymentTakeRule(PaymentTakeRule paymentTakeRule) {
        paymentTakeRuleCache.addPaymentTakeRule(paymentTakeRule);
    }

    public   PaymentTakeRule getPaymentTakeRule(String id) {
        return paymentTakeRuleCache.getPaymentTakeRule(id);
    }
    public List<PaymentTakeRule> getPaymentTakeRules(int pageIndex, int pageSize, String name) {
        return paymentTakeRuleCache.getPaymentTakeRules(pageIndex, pageSize, name);
    }

    public List<PaymentTakeRule> getPaymentTakeRuleAll() {
        return paymentTakeRuleCache.getPaymentTakeRuleAll();
    }

    public List<PaymentTakeRule> getPaymentTakeRuleByMode(Integer mode) {
        return paymentTakeRuleCache.getPaymentTakeRuleByMode(mode);
    }

    public int getPaymentTakeRuleCount( String name) {
        return paymentTakeRuleCache.getPaymentTakeRuleCount( name);
    }

    public void changePaymentTakeRule(PaymentTakeRule paymentTakeRule) {
        paymentTakeRuleCache.changePaymentTakeRule(paymentTakeRule);
    }

    public void changePaymentTakeRuleUnDefault(int mode, int option) {
        paymentTakeRuleCache.changePaymentTakeRuleUnDefault(mode, option);
    }

    public void delPaymentTakeRule(String[] ids){
        paymentTakeRuleCache.delPaymentTakeRule(ids);
    }

    @Autowired
    PaymentTransactionLoggerCache paymentTransactionLoggerCache;

    public void addPaymentTransactionLogger(PaymentTransactionLogger paymentTransactionLogger) {
        paymentTransactionLoggerCache.addPaymentTransactionLogger(paymentTransactionLogger);
    }

    public   PaymentTransactionLogger getPaymentTransactionLogger(String id) {
        return paymentTransactionLoggerCache.getPaymentTransactionLogger(id);
    }

    public List<PaymentTransactionLogger> getPaymentTransactionLoggers(int pageIndex, int pageSize, HashMap<String, Object> parameters) {
        return paymentTransactionLoggerCache.getPaymentTransactionLoggers(pageIndex, pageSize, parameters);
    }

    /*public List<PaymentTransactionLogger> getPaymentTransactionLoggers(int pageIndex, int pageSize, HashMap<String, Object> parameters, List<String> ids) {
        return paymentTransactionLoggerCache.getPaymentTransactionLoggers(pageIndex, pageSize, parameters, ids);
    }*/

    public List<PaymentTransactionLogger> getPaymentTransactionLoggerAll() {
        return paymentTransactionLoggerCache.getPaymentTransactionLoggerAll();
    }

    public int getPaymentTransactionLoggerCount(HashMap<String, Object> parameters) {
        return paymentTransactionLoggerCache.getPaymentTransactionLoggerCount(parameters);
    }

    public void changePaymentTransactionLogger(String[] id) {
        paymentTransactionLoggerCache.changePaymentTransactionLogger(id);
    }

    public void changePaymentTransactionLoggerFinish(String[] id) {
        paymentTransactionLoggerCache.changePaymentTransactionLoggerFinish(id);
    }

    public void changePaymentTransactionLoggerRefuse(String[] id) {
        paymentTransactionLoggerCache.changePaymentTransactionLoggerRefuse(id);
    }

    public List<PaymentTransactionLogger> getWithdrawalPaymentTransactionLogger(String userId, int key,int limitStart,int limitEnd) {
        return paymentTransactionLoggerCache.getPaymentTransactionLoggerByUserIdAndType(userId,key,limitStart,limitEnd);
    }
    public PaymentTransactionLogger getTransSequenceNumber() {
        return paymentTransactionLoggerCache.getTransSequenceNumber();
    }
    public void changePaymentTransactionLoggerByMap(Map<String ,Object > map){
        paymentTransactionLoggerCache.changePaymentTransactionLoggerByMap(map);
    }
    public List<PayLogVo> getPaymentTransactionLoggerByMap(Map<String ,Object > map){
        return  paymentTransactionLoggerCache.getPaymentTransactionLoggerByMap(map);
    }

    public List<PayLogNumbersVo> getPaymentTransactionLoggerNumberDate(Map<String ,Object > map){
        return  paymentTransactionLoggerCache.getPaymentTransactionLoggerNumberDate(map);
    }

    public List<PaymentTransactionLogger> getPaymentTransactionLoggerMap(Map<String, Object> map) {
        return  paymentTransactionLoggerCache.getPaymentTransactionLoggerMap(map);
    }

    public int getPaymentTransactionLoggerByMapCount(Map<String, Object> map) {
        return paymentTransactionLoggerCache.getPaymentTransactionLoggerByMapCount(map);
    }

    public int getPaymentTransactionLoggerMapCount(Map<String, Object> map) {
        return paymentTransactionLoggerCache.getPaymentTransactionLoggerMapCount(map);
    }

    public PaymentTransactionLogger getPaymentTransactionLoggerByOutTradeNo(String out_trade_no) {
        return paymentTransactionLoggerCache.getPaymentTransactionLoggerByOutTradeNo(out_trade_no);
    }

    public void changePaymentTransactionLoggerStatus(String[] id, int transStatus, HashMap map) {
        paymentTransactionLoggerCache.changePaymentTransactionLoggerStatus(id, transStatus, map);
    }

    @Autowired
    PaymentEstimateRevenueCache paymentEstimateRevenueCache;

    public void addPaymentEstimateRevenue(PaymentEstimateRevenue paymentEstimateRevenue) {
        paymentEstimateRevenueCache.addPaymentEstimateRevenue(paymentEstimateRevenue);
    }

    public   PaymentEstimateRevenue getPaymentEstimateRevenue(String id) {
        return paymentEstimateRevenueCache.getPaymentEstimateRevenue(id);
    }
    public List<PaymentEstimateRevenue> getPaymentEstimateRevenues(int status,int pageIndex, int pageSize, Map<String,Object> map) {
        return paymentEstimateRevenueCache.getPaymentEstimateRevenues(status,pageIndex, pageSize, map);
    }
    public List<PaymentEstimateRevenue> getPaymentEstimateRevenueAll() {
        return paymentEstimateRevenueCache.getPaymentEstimateRevenueAll();
    }

    public int getPaymentEstimateRevenueCount(int status, Map<String,Object> map) {
        return paymentEstimateRevenueCache.getPaymentEstimateRevenueCount(status, map);
    }

    public void changePaymentEstimateRevenue(PaymentEstimateRevenue paymentEstimateRevenue) {
        paymentEstimateRevenueCache.changePaymentEstimateRevenue(paymentEstimateRevenue);
    }

    public void changePaymentEstimateRevenueRequestTime( String idStr) {
        paymentEstimateRevenueCache.changePaymentEstimateRevenueRequestTime(idStr);
    }

    public void changePaymentEstimateRevenueAccountingTime( String idStr) {
        paymentEstimateRevenueCache.changePaymentEstimateRevenueAccountingTime(idStr);
    }

    public List<PaymentEstimateRevenue> getPaymentEstimateRevenueForUnhandle() {
        return paymentEstimateRevenueCache.getPaymentEstimateRevenueForUnhandle();
    }

    public void changePaymentEstimateRevenueByParam(Map<String,Object> map) {
        paymentEstimateRevenueCache.changePaymentEstimateRevenueByParam(map);
    }

    @Autowired
    PaymentEstimateRevenueLoggerCache paymentEstimateRevenueLoggerCache;

    public void addPaymentEstimateRevenueLogger(PaymentEstimateRevenueLogger paymentEstimateRevenueLogger) {
        paymentEstimateRevenueLoggerCache.addPaymentEstimateRevenueLogger(paymentEstimateRevenueLogger);
    }

    public   PaymentEstimateRevenueLogger getPaymentEstimateRevenueLogger(String id) {
        return paymentEstimateRevenueLoggerCache.getPaymentEstimateRevenueLogger(id);
    }

    public List<PaymentEstimateRevenueLogger> getPaymentEstimateRevenueLoggers(Map<String, Object> map, int pageIndex, int pageSize) {
        return paymentEstimateRevenueLoggerCache.getPaymentEstimateRevenueLoggers(map,pageIndex,pageSize);
    }

    public List<PaymentEstimateRevenueLogger> getPaymentEstimateRevenueLoggerAll() {
        return paymentEstimateRevenueLoggerCache.getPaymentEstimateRevenueLoggerAll();
    }

    public int getPaymentEstimateRevenueLoggerCount(Map<String, Object> map) {
        return paymentEstimateRevenueLoggerCache.getPaymentEstimateRevenueLoggerCount(map);
    }

    public void changePaymentEstimateRevenueLogger(PaymentEstimateRevenueLogger paymentEstimateRevenueLogger) {
        paymentEstimateRevenueLoggerCache.changePaymentEstimateRevenueLogger(paymentEstimateRevenueLogger);
    }

}
