package com.tuoniaostore.payment.cache.impl;
        import com.tuoniaostore.datamodel.payment.PaymentTransactionLogger;
        import com.tuoniaostore.datamodel.vo.order.PayLogNumbersVo;
        import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.cache.PaymentTransactionLoggerCache;
import com.tuoniaostore.payment.data.PaymentTransactionLoggerMapper;
import com.tuoniaostore.payment.vo.PayLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@CacheConfig(cacheNames = "paymentTransactionLogger")
public class PaymentTransactionLoggerCacheImpl implements PaymentTransactionLoggerCache {

    @Autowired
    PaymentTransactionLoggerMapper mapper;
    @Autowired
    PaymentDataAccessManager dataAccessManager;

    @Override
    @CacheEvict(value = "paymentTransactionLogger", allEntries = true)
    public void addPaymentTransactionLogger(PaymentTransactionLogger paymentTransactionLogger) {
        mapper.addPaymentTransactionLogger(paymentTransactionLogger);
    }

    @Override
    @Cacheable(value = "paymentTransactionLogger", key = "'getPaymentTransactionLogger_'+#id")
    public   PaymentTransactionLogger getPaymentTransactionLogger(String id) {
        return mapper.getPaymentTransactionLogger(id);
    }

    @Override
    @Cacheable(value = "paymentTransactionLogger", key = "'getPaymentTransactionLoggers_'+#pageIndex+'_'+#pageSize+'_'+#parameters")
    public List<PaymentTransactionLogger> getPaymentTransactionLoggers(int pageIndex, int pageSize, HashMap<String, Object> parameters) {
        return mapper.getPaymentTransactionLoggers(pageIndex, pageSize,  parameters);
    }

    @Override
    @Cacheable(value = "paymentTransactionLogger", key = "'getPaymentTransactionLoggerAll'")
    public List<PaymentTransactionLogger> getPaymentTransactionLoggerAll() {
        return mapper.getPaymentTransactionLoggerAll();
    }

    @Override
    @Cacheable(value = "paymentTransactionLogger", key = "'getPaymentTransactionLoggerCount_'+#parameters")
    public int getPaymentTransactionLoggerCount(HashMap<String, Object> parameters) {
        return mapper.getPaymentTransactionLoggerCount(parameters);
    }

    @Override
    @CacheEvict(value = "paymentTransactionLogger", allEntries = true)
    public void changePaymentTransactionLogger(String[] id) {
        mapper.changePaymentTransactionLogger(id);
    }

    @Override
    @CacheEvict(value = "paymentTransactionLogger", allEntries = true)
    public void changePaymentTransactionLoggerStatus(String[] id, int transStatus, HashMap map) {
        mapper.changePaymentTransactionLoggerStatus(id, transStatus, map);
    }

    @Override
    @CacheEvict(value = "paymentTransactionLogger", allEntries = true)
    public void changePaymentTransactionLoggerFinish(String[] id) {
        mapper.changePaymentTransactionLoggerFinish(id);
    }

    @Override
    @CacheEvict(value = "paymentTransactionLogger", allEntries = true)
    public void changePaymentTransactionLoggerRefuse(String[] id) {
        mapper.changePaymentTransactionLoggerRefuse(id);
    }

    @Override
    @Cacheable(value = "paymentTransactionLogger", key = "'getPaymentTransactionLoggerByUserIdAndType_'+#userId+'_'+#key+'_'+#limitStart+'_'+#limitEnd")
    public List<PaymentTransactionLogger> getPaymentTransactionLoggerByUserIdAndType(String userId, int key,int limitStart,int limitEnd) {
        return mapper.getPaymentTransactionLoggerByUserIdAndType(userId,key,limitStart,limitEnd);
    }

    /**
     * 不需要缓存
     * @return
     */
    @Override
    public PaymentTransactionLogger getTransSequenceNumber() {
        return mapper.getTransSequenceNumber();
    }

    /**
     * 修改支付记录
     * @author sqd
     * @date 2019/4/26
     * @param
     * @return void
     */
    @CacheEvict(value = "paymentTransactionLogger", allEntries = true)
    public void changePaymentTransactionLoggerByMap(Map<String ,Object > map){
        mapper.changePaymentTransactionLoggerByMap(map);
    }

    @Override
    @Cacheable(value = "paymentTransactionLogger", key = "'getPaymentTransactionLoggerByMap_'+#map")
    public List<PayLogVo> getPaymentTransactionLoggerByMap(Map<String ,Object > map){
        return  mapper.getPaymentTransactionLoggerByMap(map);
    }

    @Override
    @Cacheable(value = "paymentTransactionLogger", key = "'getPaymentTransactionLoggerNumberDate_'+#map")
    public List<PayLogNumbersVo> getPaymentTransactionLoggerNumberDate(Map<String ,Object > map){
        return  mapper.getPaymentTransactionLoggerNumberDate(map);
    }

    @Override
    @Cacheable(value = "paymentTransactionLogger", key = "'getPaymentTransactionLoggerMap_'+#map")
    public List<PaymentTransactionLogger> getPaymentTransactionLoggerMap(Map<String, Object> map) {
        return  mapper.getPaymentTransactionLoggerMap(map);
    }

    @Override
    @Cacheable(value = "paymentTransactionLogger", key = "'getPaymentTransactionLoggerByMapCount_'+#map")
    public int getPaymentTransactionLoggerByMapCount(Map<String, Object> map) {
        return mapper.getPaymentTransactionLoggerByMapCount(map);
    }

    @Override
    @Cacheable(value = "paymentTransactionLogger", key = "'getPaymentTransactionLoggerMapCount_'+#map")
    public int getPaymentTransactionLoggerMapCount(Map<String, Object> map) {
        return mapper.getPaymentTransactionLoggerMapCount(map);
    }

    @Override
    public PaymentTransactionLogger getPaymentTransactionLoggerByOutTradeNo(String out_trade_no) {
        return mapper.getPaymentTransactionLoggerByOutTradeNo(out_trade_no);
    }

}
