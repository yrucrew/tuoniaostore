package com.tuoniaostore.payment.cache.impl;
        import com.tuoniaostore.datamodel.payment.PaymentTakeRule;
        import com.tuoniaostore.payment.cache.PaymentTakeRuleCache;
        import com.tuoniaostore.payment.data.PaymentTakeRuleMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@CacheConfig(cacheNames = "paymentTakeRule")
public class PaymentTakeRuleCacheImpl implements PaymentTakeRuleCache {

    @Autowired
    PaymentTakeRuleMapper mapper;

    @Override
    @CacheEvict(value = "paymentTakeRule", allEntries = true)
    public void addPaymentTakeRule(PaymentTakeRule paymentTakeRule) {
        mapper.addPaymentTakeRule(paymentTakeRule);
    }

    @Override
    @Cacheable(value = "paymentTakeRule", key = "'getPaymentTakeRule_'+#id")
    public   PaymentTakeRule getPaymentTakeRule(String id) {
        return mapper.getPaymentTakeRule(id);
    }

    @Override
    @Cacheable(value = "paymentTakeRule", key = "'getPaymentTakeRules_'+#pageIndex+'_'+#pageSize+'_'+#name")
    public List<PaymentTakeRule> getPaymentTakeRules(int pageIndex, int pageSize, String name) {
        return mapper.getPaymentTakeRules(pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "paymentTakeRule", key = "'getPaymentTakeRuleAll'")
    public List<PaymentTakeRule> getPaymentTakeRuleAll() {
        return mapper.getPaymentTakeRuleAll();
    }

    @Override
    @Cacheable(value = "paymentTakeRule", key = "'getPaymentTakeRuleCount_'+#name")
    public int getPaymentTakeRuleCount( String name) {
        return mapper.getPaymentTakeRuleCount( name);
    }

    @Override
    @CacheEvict(value = "paymentTakeRule", allEntries = true)
    public void changePaymentTakeRule(PaymentTakeRule paymentTakeRule) {
        mapper.changePaymentTakeRule(paymentTakeRule);
    }

    @Override
    @Cacheable(value = "paymentTakeRule", key = "'getPaymentTakeRuleByMode_'+#mode")
    public List<PaymentTakeRule> getPaymentTakeRuleByMode(Integer mode) {
        return mapper.getPaymentTakeRuleByMode(mode);
    }

    @Override
    @CacheEvict(value = "paymentTakeRule", allEntries = true)
    public void changePaymentTakeRuleUnDefault(int mode, int option) {
        mapper.changePaymentTakeRuleUnDefault(mode, option);
    }

    @Override
    @CacheEvict(value = "paymentTakeRule", allEntries = true)
    public void delPaymentTakeRule(String[] ids) {
        mapper.delPaymentTakeRule(ids);
    }
}
