package com.tuoniaostore.payment.cache.impl;
        import com.tuoniaostore.datamodel.payment.PaymentCurrencyOffsetLogger;
        import com.tuoniaostore.datamodel.vo.payment.PaymentCurrencyOffsetLoggerVO;
        import com.tuoniaostore.payment.cache.PaymentCurrencyOffsetLoggerCache;
        import com.tuoniaostore.payment.data.PaymentCurrencyOffsetLoggerMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;
        import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@CacheConfig(cacheNames = "paymentCurrencyOffsetLogger")
public class PaymentCurrencyOffsetLoggerCacheImpl implements PaymentCurrencyOffsetLoggerCache {

    @Autowired
    PaymentCurrencyOffsetLoggerMapper mapper;

    @Override
    @CacheEvict(value = "paymentCurrencyOffsetLogger", allEntries = true)
    public void addPaymentCurrencyOffsetLogger(PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger) {
        mapper.addPaymentCurrencyOffsetLogger(paymentCurrencyOffsetLogger);
    }

    @Override
    @Cacheable(value = "paymentCurrencyOffsetLogger", key = "'getPaymentCurrencyOffsetLogger_'+#id")
    public   PaymentCurrencyOffsetLogger getPaymentCurrencyOffsetLogger(String id) {
        return mapper.getPaymentCurrencyOffsetLogger(id);
    }

    @Override
    @Cacheable(value = "paymentCurrencyOffsetLogger", key = "'getPaymentCurrencyOffsetLoggers_'+#pageIndex+'_'+#pageSize+'_'+#map")
    public List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggers(int pageIndex, int pageSize,  Map<String, Object> map) {
        return mapper.getPaymentCurrencyOffsetLoggers(pageIndex, pageSize,  map);
    }

    @Override
    @Cacheable(value = "paymentCurrencyOffsetLogger", key = "'getPaymentCurrencyOffsetLoggerAll'")
    public List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggerAll() {
        return mapper.getPaymentCurrencyOffsetLoggerAll();
    }

    @Override
    @Cacheable(value = "paymentCurrencyOffsetLogger", key = "'getPaymentCurrencyOffsetLoggerCount_'+#map")
    public int getPaymentCurrencyOffsetLoggerCount( Map<String, Object> map) {
        return mapper.getPaymentCurrencyOffsetLoggerCount(map);
    }

    @Override
    @Cacheable(value = "paymentCurrencyOffsetLogger", key = "'getPaymentCurrencyOffsetLoggerCountByParam_'+#name")
    public int getPaymentCurrencyOffsetLoggerCountByParam(String name) {
        return mapper.getPaymentCurrencyOffsetLoggerCountByParam(name);
    }

    @Override
    @CacheEvict(value = "paymentCurrencyOffsetLogger", allEntries = true)
    public void changePaymentCurrencyOffsetLogger(PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger) {
        mapper.changePaymentCurrencyOffsetLogger(paymentCurrencyOffsetLogger);
    }

    @Override
    @Cacheable(value = "paymentCurrencyOffsetLogger", key = "'getTodayPaymentCurrencyOffsetLoggerByUserId'+#userId+'_'+'_'+#relationTransType+'_'+#time+'_'+#limitStart+'_'+#limitEnd")
    public List<PaymentCurrencyOffsetLogger> getTodayPaymentCurrencyOffsetLoggerByUserId(String userId,int relationTransType,String time,Integer limitStart,Integer limitEnd) {
        return mapper.getTodayPaymentCurrencyOffsetLoggerByUserId(userId,relationTransType,time,limitStart,limitEnd);
    }

    @Override
    @Cacheable(value = "paymentCurrencyOffsetLogger", key = "'getSelectTodayPaymentCurrencyOffsetLoggerByUserId'+#userId+'_'+#relationTransType+'_'+#time+'_'+#limitStart+'_'+#limitEnd")
    public List<PaymentCurrencyOffsetLogger> getSelectTodayPaymentCurrencyOffsetLoggerByUserId(String userId, int relationTransType, String time,Integer limitStart,Integer limitEnd ) {
        return mapper.getSelectTodayPaymentCurrencyOffsetLoggerByUserId(userId,relationTransType,time,limitStart,limitEnd);
    }

    @Override
    @Cacheable(value = "paymentCurrencyOffsetLogger", key = "'getPaymentCurrencyOffsetLoggersByParam'+#userId+'_'+#relationTransType")
    public List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggersByParam(String userId, Integer relationTransType){
        return mapper.getPaymentCurrencyOffsetLoggersByParam(userId,relationTransType);
    }

    @Override
    @Cacheable(value = "paymentCurrencyOffsetLogger", key = "'getCountTodayPaymentCurrencyOffsetLoggerByUserId'+#userId+'_'+#relationTransType+'_'+#time")
    public Map<String, Object> getCountTodayPaymentCurrencyOffsetLoggerByUserId(String userId, int relationTransType, String time) {
        return mapper.getCountTodayPaymentCurrencyOffsetLoggerByUserId(userId,relationTransType,time);
    }

    @Override
    @Cacheable(value = "paymentCurrencyOffsetLogger", key = "'getPaymentCurrencyOffsetLoggerTotalMoneyByUser_'+#userList+'_'+#type")
    public List<PaymentCurrencyOffsetLoggerVO> getPaymentCurrencyOffsetLoggerTotalMoneyByUser(List<String> userList, int type) {
        return mapper.getPaymentCurrencyOffsetLoggerTotalMoneyByUser(userList,type);
    }

    @Override
    public List<PaymentCurrencyOffsetLogger> getPaymentCurrencyOffsetLoggersByUserAndParam(String userId, Map<String, Object> map) {
        return mapper.getPaymentCurrencyOffsetLoggersByUserAndParam(userId,map);
    }

    @Override
    public int getCountPaymentCurrencyOffsetLoggersByUserAndParam(String userId, Map<String, Object> map) {
        return mapper.getCountPaymentCurrencyOffsetLoggersByUserAndParam(userId,map);
    }

    @Override
    public Map<String, Object> getPaymentCurrencyOffsetLoggerForInOut(String userId, Integer currencyType, String startTime, String endTime) {
        return mapper.getPaymentCurrencyOffsetLoggerForInOut(userId,currencyType,startTime,endTime);
    }

}
