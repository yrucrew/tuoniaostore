package com.tuoniaostore.payment.cache;
import com.tuoniaostore.datamodel.payment.PaymentEstimateRevenue;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-08 10:21:40
 */
public interface PaymentEstimateRevenueCache {

    void addPaymentEstimateRevenue(PaymentEstimateRevenue paymentEstimateRevenue);

    PaymentEstimateRevenue getPaymentEstimateRevenue(String id);

    List<PaymentEstimateRevenue> getPaymentEstimateRevenues(int status, int pageIndex, int pageSize, Map<String,Object> map);
    List<PaymentEstimateRevenue> getPaymentEstimateRevenueAll();
    int getPaymentEstimateRevenueCount(int status,  Map<String,Object> map);
    void changePaymentEstimateRevenue(PaymentEstimateRevenue paymentEstimateRevenue);
    void changePaymentEstimateRevenueRequestTime( String idStr);
    void changePaymentEstimateRevenueAccountingTime( String idStr);

    List<PaymentEstimateRevenue> getPaymentEstimateRevenueForUnhandle();

    void changePaymentEstimateRevenueByParam(Map<String, Object> map);
}
