package com.tuoniaostore.payment.cache;
import com.tuoniaostore.datamodel.payment.PaymentTransactionLogger;
import com.tuoniaostore.datamodel.vo.order.PayLogNumbersVo;
import com.tuoniaostore.payment.vo.PayLogVo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentTransactionLoggerCache {

    void addPaymentTransactionLogger(PaymentTransactionLogger paymentTransactionLogger);

    PaymentTransactionLogger getPaymentTransactionLogger(String id);

    //List<PaymentTransactionLogger> getPaymentTransactionLoggers(int pageIndex, int pageSize, HashMap<String, Object> parameters, List<String> ids);
    List<PaymentTransactionLogger> getPaymentTransactionLoggers(int pageIndex, int pageSize, HashMap<String, Object> parameters);
    List<PaymentTransactionLogger> getPaymentTransactionLoggerAll();
    int getPaymentTransactionLoggerCount(HashMap<String, Object> parameters);
    void changePaymentTransactionLogger(String[] id);
    void changePaymentTransactionLoggerFinish(String[] id);
    void changePaymentTransactionLoggerRefuse(String[] id);

    /**
     * 根据用户id 和 转钱类型  //1-支付 2-充值 3-代付 4-提现
     * @param userId
     * @param key
     */
    List<PaymentTransactionLogger> getPaymentTransactionLoggerByUserIdAndType(String userId, int key,int limitStart,int limitEnd);

    /**
     * 查询当天的序列号
     * @return
     */
    PaymentTransactionLogger getTransSequenceNumber();

    /**
     * 修改支付记录
     * @author sqd
     * @date 2019/4/26
     * @param
     * @return void
     */
    void changePaymentTransactionLoggerByMap(Map<String ,Object > map);

    List<PayLogVo> getPaymentTransactionLoggerByMap(Map<String ,Object > map);

    List<PayLogNumbersVo> getPaymentTransactionLoggerNumberDate(Map<String ,Object > map);

    List<PaymentTransactionLogger> getPaymentTransactionLoggerMap(Map<String ,Object > map);

    int getPaymentTransactionLoggerByMapCount(Map<String ,Object > map);

    int getPaymentTransactionLoggerMapCount(Map<String ,Object > map);

    PaymentTransactionLogger getPaymentTransactionLoggerByOutTradeNo(String out_trade_no);

    /**
     * 修改交易状态
     * @param id
     * @param transStatus 状态
     */
    void changePaymentTransactionLoggerStatus(String[] id, int transStatus, HashMap map);
}
