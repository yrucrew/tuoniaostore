package com.tuoniaostore.payment.cache.impl;

import com.tuoniaostore.datamodel.payment.PaymentEstimateRevenue;
import com.tuoniaostore.payment.cache.PaymentDataAccessManager;
import com.tuoniaostore.payment.cache.PaymentEstimateRevenueCache;
import com.tuoniaostore.payment.data.PaymentEstimateRevenueMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-08 10:21:40
 */
@Component
@CacheConfig(cacheNames = "paymentEstimateRevenue")
public class PaymentEstimateRevenueCacheImpl implements PaymentEstimateRevenueCache {

    @Autowired
    PaymentEstimateRevenueMapper mapper;
    @Autowired
    PaymentDataAccessManager dataAccessManager;

    @Override
    @CacheEvict(value = "paymentEstimateRevenue", allEntries = true)
    public void addPaymentEstimateRevenue(PaymentEstimateRevenue paymentEstimateRevenue) {
        mapper.addPaymentEstimateRevenue(paymentEstimateRevenue);
    }

    @Override
    @Cacheable(value = "paymentEstimateRevenue", key = "'getPaymentEstimateRevenue_'+#id")
    public PaymentEstimateRevenue getPaymentEstimateRevenue(String id) {
        return mapper.getPaymentEstimateRevenue(id);
    }

    @Override
    @Cacheable(value = "paymentEstimateRevenue", key = "'getPaymentEstimateRevenues_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#map")
    public List<PaymentEstimateRevenue> getPaymentEstimateRevenues(int status, int pageIndex, int pageSize, Map<String,Object> map) {
        List<PaymentEstimateRevenue> paymentEstimateRevenues = mapper.getPaymentEstimateRevenues(status, pageIndex, pageSize,map);
        return paymentEstimateRevenues;
    }

    @Override
    @Cacheable(value = "paymentEstimateRevenue", key = "'getPaymentEstimateRevenueAll'")
    public List<PaymentEstimateRevenue> getPaymentEstimateRevenueAll() {
        return mapper.getPaymentEstimateRevenueAll();
    }

    @Override
    @Cacheable(value = "paymentEstimateRevenue", key = "'getPaymentEstimateRevenueCount_'+#status+'_'+#map")
    public int getPaymentEstimateRevenueCount(int status,  Map<String,Object> map) {
        return mapper.getPaymentEstimateRevenueCount(status, map);
    }

    @Override
    @CacheEvict(value = "paymentEstimateRevenue", allEntries = true)
    public void changePaymentEstimateRevenue(PaymentEstimateRevenue paymentEstimateRevenue) {
        mapper.changePaymentEstimateRevenue(paymentEstimateRevenue);
    }

    @Override
    @CacheEvict(value = "paymentEstimateRevenue", allEntries = true)
    public void changePaymentEstimateRevenueRequestTime(String idStr) {
        mapper.changePaymentEstimateRevenueRequestTime(idStr);
    }

    @Override
    @CacheEvict(value = "paymentEstimateRevenue", allEntries = true)
    public void changePaymentEstimateRevenueAccountingTime(String idStr) {
        mapper.changePaymentEstimateRevenueAccountingTime(idStr);
    }

    @Override
    public List<PaymentEstimateRevenue> getPaymentEstimateRevenueForUnhandle() {
        return mapper.getPaymentEstimateRevenueForUnhandle();
    }

    @Override
    @CacheEvict(value = "paymentEstimateRevenue", allEntries = true)
    public void changePaymentEstimateRevenueByParam(Map<String, Object> map) {
        mapper.changePaymentEstimateRevenueByParam(map);
    }

}
