package com.tuoniaostore.payment.cache;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyProperties;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentCurrencyPropertiesCache {

    void addPaymentCurrencyProperties(PaymentCurrencyProperties paymentCurrencyProperties);

    PaymentCurrencyProperties getPaymentCurrencyProperties(String id);

    List<PaymentCurrencyProperties> getPaymentCurrencyPropertiess(int pageIndex, int pageSize, int status, String name);
    List<PaymentCurrencyProperties> getPaymentCurrencyPropertiesAll();
    int getPaymentCurrencyPropertiesCount(int status, String name);
    void changePaymentCurrencyProperties(PaymentCurrencyProperties paymentCurrencyProperties);

    /**
     * 通过用户id 查找当前支付密码
     * @param userId
     */
    PaymentCurrencyProperties getPaymentCurrencyPropertiesByUserId(String userId,int type);

    /**
     * 修改密码
     * @param paymentCurrencyProperties
     */
    void updatePaymentCurrencyPropertiesPassword(PaymentCurrencyProperties paymentCurrencyProperties);
}
