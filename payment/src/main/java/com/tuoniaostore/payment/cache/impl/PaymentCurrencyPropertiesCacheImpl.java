package com.tuoniaostore.payment.cache.impl;
        import com.tuoniaostore.datamodel.payment.PaymentCurrencyProperties;
        import com.tuoniaostore.payment.cache.PaymentCurrencyPropertiesCache;
        import com.tuoniaostore.payment.data.PaymentCurrencyPropertiesMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
@Component
@CacheConfig(cacheNames = "paymentCurrencyProperties")
public class PaymentCurrencyPropertiesCacheImpl implements PaymentCurrencyPropertiesCache {

    @Autowired
    PaymentCurrencyPropertiesMapper mapper;

    @Override
    @CacheEvict(value = "paymentCurrencyProperties", allEntries = true)
    public void addPaymentCurrencyProperties(PaymentCurrencyProperties paymentCurrencyProperties) {
        mapper.addPaymentCurrencyProperties(paymentCurrencyProperties);
    }

    @Override
    @Cacheable(value = "paymentCurrencyProperties", key = "'getPaymentCurrencyProperties_'+#id")
    public   PaymentCurrencyProperties getPaymentCurrencyProperties(String id) {
        return mapper.getPaymentCurrencyProperties(id);
    }

    @Override
    @Cacheable(value = "paymentCurrencyProperties", key = "'getPaymentCurrencyPropertiess_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<PaymentCurrencyProperties> getPaymentCurrencyPropertiess(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getPaymentCurrencyPropertiess(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "paymentCurrencyProperties", key = "'getPaymentCurrencyPropertiesAll'")
    public List<PaymentCurrencyProperties> getPaymentCurrencyPropertiesAll() {
        return mapper.getPaymentCurrencyPropertiesAll();
    }

    @Override
    @Cacheable(value = "paymentCurrencyProperties", key = "'getPaymentCurrencyPropertiesCount_'+#status+'_'+#name")
    public int getPaymentCurrencyPropertiesCount(int status, String name) {
        return mapper.getPaymentCurrencyPropertiesCount(status, name);
    }

    @Override
    @CacheEvict(value = "paymentCurrencyProperties", allEntries = true)
    public void changePaymentCurrencyProperties(PaymentCurrencyProperties paymentCurrencyProperties) {
        mapper.changePaymentCurrencyProperties(paymentCurrencyProperties);
    }

    @Override
    @Cacheable(value = "paymentCurrencyProperties", key = "'getPaymentCurrencyProperties_'+#userId+'_'+#type")
    public PaymentCurrencyProperties getPaymentCurrencyPropertiesByUserId(String userId, int type) {
        return mapper.getPaymentCurrencyPropertiesByUserId(userId,type);
    }

    @Override
    @CacheEvict(value = "paymentCurrencyProperties", allEntries = true)
    public void updatePaymentCurrencyPropertiesPassword(PaymentCurrencyProperties paymentCurrencyProperties) {
        mapper.updatePaymentCurrencyPropertiesPassword(paymentCurrencyProperties);
    }

}
