package com.tuoniaostore.payment.cache.impl;
        import com.tuoniaostore.datamodel.payment.PaymentEstimateRevenueLogger;
        import com.tuoniaostore.payment.cache.PaymentEstimateRevenueLoggerCache;
        import com.tuoniaostore.payment.data.PaymentEstimateRevenueLoggerMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;
        import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-08 10:21:40
 */
@Component
@CacheConfig(cacheNames = "paymentEstimateRevenueLogger")
public class PaymentEstimateRevenueLoggerCacheImpl implements PaymentEstimateRevenueLoggerCache {

    @Autowired
    PaymentEstimateRevenueLoggerMapper mapper;

    @Override
    @CacheEvict(value = "paymentEstimateRevenueLogger", allEntries = true)
    public void addPaymentEstimateRevenueLogger(PaymentEstimateRevenueLogger paymentEstimateRevenueLogger) {
        mapper.addPaymentEstimateRevenueLogger(paymentEstimateRevenueLogger);
    }

    @Override
    @Cacheable(value = "paymentEstimateRevenueLogger", key = "'getPaymentEstimateRevenueLogger_'+#id")
    public   PaymentEstimateRevenueLogger getPaymentEstimateRevenueLogger(String id) {
        return mapper.getPaymentEstimateRevenueLogger(id);
    }

    @Override
    @Cacheable(value = "paymentEstimateRevenueLogger", key = "'getPaymentEstimateRevenueLoggers_'+#map+'_'+#pageIndex+'_'+#pageSize")
    public List<PaymentEstimateRevenueLogger> getPaymentEstimateRevenueLoggers(Map<String, Object> map, int pageIndex, int pageSize) {
        return mapper.getPaymentEstimateRevenueLoggers(map,pageIndex, pageSize);
    }

    @Override
    @Cacheable(value = "paymentEstimateRevenueLogger", key = "'getPaymentEstimateRevenueLoggerAll'")
    public List<PaymentEstimateRevenueLogger> getPaymentEstimateRevenueLoggerAll() {
        return mapper.getPaymentEstimateRevenueLoggerAll();
    }

    @Override
    @Cacheable(value = "paymentEstimateRevenueLogger", key = "'getPaymentEstimateRevenueLoggerCount_'+#map")
    public int getPaymentEstimateRevenueLoggerCount(Map<String, Object> map) {
        return mapper.getPaymentEstimateRevenueLoggerCount(map);
    }

    @Override
    @CacheEvict(value = "paymentEstimateRevenueLogger", allEntries = true)
    public void changePaymentEstimateRevenueLogger(PaymentEstimateRevenueLogger paymentEstimateRevenueLogger) {
        mapper.changePaymentEstimateRevenueLogger(paymentEstimateRevenueLogger);
    }

}
