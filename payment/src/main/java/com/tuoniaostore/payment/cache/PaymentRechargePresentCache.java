package com.tuoniaostore.payment.cache;
import com.tuoniaostore.datamodel.payment.PaymentRechargePresent;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentRechargePresentCache {

    void addPaymentRechargePresent(PaymentRechargePresent paymentRechargePresent);

    PaymentRechargePresent getPaymentRechargePresent(String id);

    List<PaymentRechargePresent> getPaymentRechargePresents(int pageIndex, int pageSize, int status, String name);
    List<PaymentRechargePresent> getPaymentRechargePresentAll();
    int getPaymentRechargePresentCount(int status, String name);
    void changePaymentRechargePresent(PaymentRechargePresent paymentRechargePresent);

}
