package com.tuoniaostore.payment.cache;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyAccount;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public interface PaymentCurrencyAccountCache {

    void addPaymentCurrencyAccount(PaymentCurrencyAccount paymentCurrencyAccount);

    PaymentCurrencyAccount getPaymentCurrencyAccount(String id);

    List<PaymentCurrencyAccount> getPaymentCurrencyAccounts(int pageIndex, int pageSize, Map<String,Object> map);
    List<PaymentCurrencyAccount> getPaymentCurrencyAccountAll();
    int getPaymentCurrencyAccountCount(Map<String,Object> map);
    void changePaymentCurrencyAccount(PaymentCurrencyAccount paymentCurrencyAccount);

    /**
     * 通过用户id 查找当前的余额
     * @param userId
     * @return
     */
    PaymentCurrencyAccount getPaymentCurrencyAccountByUserId(String userId,int type);

    /**
     * 提现的时候 扣除对应的用户账号中的金额
     * @param id 用户id
     * @param money 金钱
     * @param type 余额类型还是会员类型
     */
    void changePaymentCurrencyAccountWithWithdrawl(String id, long money,int type);

    void changePaymentCurrencyAccountWithWithdrawlAdd(String id, long money, int type);
}
