package com.tuoniaostore.supplychain.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.CommonUtils;
import com.tuoniaostore.commons.constant.MessageType;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.order.OrderReceiptTitel;
import com.tuoniaostore.commons.constant.payment.CurrencyTypeEnum;
import com.tuoniaostore.commons.constant.supplychain.*;
import com.tuoniaostore.commons.constant.user.ClientTypeEnum;
import com.tuoniaostore.commons.lbs.SimpleLocationUtils;
import com.tuoniaostore.commons.support.community.AreaRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.commons.support.community.SysUserTerminalService;
import com.tuoniaostore.commons.support.good.GoodBarcodeRemoteService;
import com.tuoniaostore.commons.support.good.GoodPricePosRemoteService;
import com.tuoniaostore.commons.support.good.GoodTypeRemoteService;
import com.tuoniaostore.commons.support.payment.PaymentCurrencyAccountRemoteService;
import com.tuoniaostore.commons.support.payment.PaymentCurrencyOffsetLoggerRemoteService;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.datamodel.good.GoodBarcode;
import com.tuoniaostore.datamodel.good.GoodType;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyAccount;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopBindRelationship;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseBindRelationship;
import com.tuoniaostore.datamodel.user.SysArea;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.user.SysUserTerminal;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.datamodel.vo.payment.PaymentCurrencyOffsetLoggerVO;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainShopPropertiesService;
import com.tuoniaostore.supplychain.vo.ShopReportMapperVO;
import com.tuoniaostore.supplychain.vo.SupplychainGoodVO;
import com.tuoniaostore.supplychain.vo.SupplychainShopPropertiesForSupplierVO;
import com.tuoniaostore.supplychain.vo.SupplychainShopPropertiesVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@Service("supplychainShopPropertiesService")
public class SupplychainShopPropertiesServiceImpl extends BasicWebService implements SupplychainShopPropertiesService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainShopProperties() {
        SupplychainShopProperties supplychainShopProperties = new SupplychainShopProperties();
        //归属渠道
        String channelId = getUTF("channelId", null);
        //仓库的名字
        String shopName = getUTF("shopName", null);
        //当前仓库状态 0表示关仓 1表示开仓 2不对外开放(理论上只有一个--直营主仓)
        Integer status = getIntParameter("status", 0);
        //仓库类型 如：0-直营仓、1-虚拟仓、2-第三方仓、3-合作仓、4-代理仓等
        Integer type = getIntParameter("type", 0);
        //归属ID
        String ownerId = getUTF("ownerId", null);
        //管理员姓名
        String adminName = getUTF("adminName", null);
        //联系人号码
        String phoneNumber = getUTF("phoneNumber", null);
        //归属的逻辑区域ID
        String logicAreaId = getUTF("logicAreaId", null);
        //所在省份
        String province = getUTF("province", null);
        //所在城市
        String city = getUTF("city", null);
        //所在区域
        String district = getUTF("district", null);
        //具体地址
        String address = getUTF("address", null);
        //所在地址经纬度 格式：longitude,latitude
        String location = getUTF("location", null);
        //父级仓库ID 一级仓时为0
        String parentId = getUTF("parentId", "0");
        //是否同步父级仓库存 0-独立库存 1-本仓+父级仓库存 2-仅读取父级库存
        Integer syncParentStock = getIntParameter("syncParentStock", 0);
        //配送类型 1-我方配送 2-独立配送
        Integer distributionType = getIntParameter("distributionType", 0);
        //覆盖距离 单位：米
        Long coveringDistance = getLongParameter("coveringDistance", 0);
        //配送费用 单位：分
        Long distributionCost = getLongParameter("distributionCost", 0);
        //起订量
        Long moq = getLongParameter("moq", 0);
        //打印类型 1：下单后 2：支付后 3：接单后 可扩展
        Integer printerType = getIntParameter("printerType", 0);
        //绑定的小票打印机编码
        String bindPrinterCode = getUTF("bindPrinterCode", null);
        supplychainShopProperties.setUserId(getUser().getId());
        supplychainShopProperties.setMoq(moq);
        supplychainShopProperties.setChannelId(channelId);
        supplychainShopProperties.setShopName(shopName);
        supplychainShopProperties.setStatus(status);
        supplychainShopProperties.setType(type);
        supplychainShopProperties.setOwnerId(ownerId);
        supplychainShopProperties.setAdminName(adminName);
        supplychainShopProperties.setPhoneNumber(phoneNumber);
        supplychainShopProperties.setLogicAreaId(logicAreaId);
        supplychainShopProperties.setProvince(province);
        supplychainShopProperties.setCity(city);
        supplychainShopProperties.setDistrict(district);
        supplychainShopProperties.setAddress(address);
        supplychainShopProperties.setLocation(location);
        supplychainShopProperties.setParentId(parentId);
        supplychainShopProperties.setSyncParentStock(syncParentStock);
        supplychainShopProperties.setDistributionType(distributionType);
        supplychainShopProperties.setCoveringDistance(coveringDistance);
        supplychainShopProperties.setDistributionCost(distributionCost);
        supplychainShopProperties.setPrinterType(printerType);
        supplychainShopProperties.setBindPrinterCode(bindPrinterCode);
        dataAccessManager.addSupplychainShopProperties(supplychainShopProperties);
        return success();
    }

    /**
     * 添加门店信息
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/9
     */
    @Override
    @Transactional
    public JSONObject addSupplychainShopPropertiesForStore() {
        String entity = getUTF("entity");
        if (entity == null || entity.equals("")) {
            return fail("entity 不能为空！");
        }
        SupplychainShopProperties supplychainShopProperties = JSONObject.parseObject(entity, SupplychainShopProperties.class);
        String userId = supplychainShopProperties.getUserId();
        SysUser sysUser = null;
        if (userId != null) {
            //验证一下 是否有该用户 是否有商店
            try {
                sysUser = SysUserRemoteService.getSysUser(userId, getHttpServletRequest());
                if (sysUser == null) {
                    return fail("该用户不存在！");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(userId);
            if (supplychainShopPropertiesByUserId != null) {//用户有商店
                return fail("该用户已有商店!");
            }
        } else {

            Integer type = supplychainShopProperties.getType();
            if (type == null) {
                return fail("type不能为空!");
            }

            //新增用户 手机号码 密码 终端类型：商家管理员 角色：门店管理员
            sysUser = new SysUser();
            String password = supplychainShopProperties.getPassword();
            String phoneNumber = supplychainShopProperties.getPhoneNumber();
            if (phoneNumber == null || phoneNumber.equals("")) {
                return fail("phoneNumber不能为空！");
            }

            if (password == null || password.equals("")) {
                return fail("password不能为空！");
            }

            sysUser.setDefaultName(phoneNumber);
            sysUser.setRealName(phoneNumber);
            sysUser.setShowName(phoneNumber);
            sysUser.setPhoneNumber(phoneNumber);
            sysUser.setPassword(encryptionPassword(password));
            if (type == 6 || type == 7) {
                sysUser.setRoleId("afc5abf2a49e4b72807497d89e80ccb9JvmOQs");//门店角色  6.普通门店 7.直营店
            } else if (type == 5 || type == 8 || type == 9) {//5代理商8.批发,9.厂商
                sysUser.setRoleId("2a480697c95d4f389a34f272bcf47c9dEKLqDk");//采购角色
            } else if (type == 0 || type == 1 || type == 2 || type == 3 || type == 4) {//0-直营仓、1-虚拟仓、2-第三方仓、3-合作仓、4-代理仓
                sysUser.setRoleId("100000000005");//仓管角色
            }

            sysUser.setRetrieveStatus(0);
            sysUser.setStatus(0);
            sysUser.setType(0);
            sysUser.setUserId(getUser().getId());


            //用户绑定终端 - 商家端终端
            if (type == 6 || type == 7) {//用户绑定终端 - pos端终端
                SysUserTerminal sysUserTerminal = new SysUserTerminal();
                sysUserTerminal.setUserId(sysUser.getId());
                sysUserTerminal.setTerminalId("e2896824c0684d53afd8bf8588f1f3eaTrrSVr");//商家管理员
                addUseTerminal(sysUserTerminal);

                SysUserTerminal sysUserTerminal2 = new SysUserTerminal();
                sysUserTerminal2.setUserId(sysUser.getId());
                sysUserTerminal2.setTerminalId("6901f7e39e9843758a13a166e03d97daGLktxR");//云POS店长
                addUseTerminal(sysUserTerminal2);
            } else if (type == 5 || type == 8 || type == 9) {
                SysUserTerminal sysUserTerminal = new SysUserTerminal();
                sysUserTerminal.setUserId(sysUser.getId());
                sysUserTerminal.setTerminalId("5f3691aa09024e30ad4af74928c7134bUUVnwd");//供应商管理员
                addUseTerminal(sysUserTerminal);
            } else if (type == 0 || type == 1 || type == 2 || type == 3 || type == 4) {
                SysUserTerminal sysUserTerminal = new SysUserTerminal();
                sysUserTerminal.setUserId(sysUser.getId());
                sysUserTerminal.setTerminalId("57f5e9d0a6da43aaac0e112a95a4aa78"); //后台管理
                addUseTerminal(sysUserTerminal);

                SysUserTerminal sysUserTerminal1 = new SysUserTerminal();
                sysUserTerminal1.setUserId(sysUser.getId());
                sysUserTerminal1.setTerminalId("cd0c1d8f4a4146d1971dcbb8269e3121HkZANx"); //云供应链管理员
                addUseTerminal(sysUserTerminal1);
            }
        }

        //接下来保存对应的信息
        supplychainShopProperties.setAdminName(getUser().getDefaultName());
        supplychainShopProperties.setUserId(sysUser.getId());//用户id
        supplychainShopProperties.setOwnerId(sysUser.getId());//用户id
        supplychainShopProperties.setChannelId("2e96e47f626c480b8ad39ac791177a5b");//来源
        supplychainShopProperties.setLocation(supplychainShopProperties.getLongitude() + "," + supplychainShopProperties.getLatitude());//地址
        //异步初始化账户信息
        addPaymentCurrencyAccountSync(sysUser);

        //异步添加用户 和 终端信息绑定
        addUserAsyc(sysUser);

        dataAccessManager.addSupplychainShopProperties(supplychainShopProperties);
        return success("保存成功！");
    }

    /***
     * 异步初始化账户信息
     * @author oy
     * @date 2019/5/27
     * @param sysUser
     * @return void
     */
    @Async
    public void addPaymentCurrencyAccountSync(SysUser sysUser) {
        PaymentCurrencyAccount paymentCurrencyAccount = new PaymentCurrencyAccount();
        paymentCurrencyAccount.setUserId(sysUser.getId());//用户id
        paymentCurrencyAccount.setCurrencyType(CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());//余额
        paymentCurrencyAccount.setName(CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getValue());//余额
        paymentCurrencyAccount.setChannelId(sysUser.getFromChannel());//渠道
        try {
            PaymentCurrencyAccountRemoteService.addPaymentCurrencyAccount(paymentCurrencyAccount, getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public JSONObject getSupplychainShopPropertiesByShopName() {
        String shopName = getUTF("shopName", null);
        ArrayList<Integer> types = new ArrayList<>();
        types.add(SupplyChainShopTypeEnum.ORDINARY_STORES.getKey());
        types.add(SupplyChainShopTypeEnum.DIRECT_SHOP.getKey());
        List<SupplychainShopProperties> supplychainShopPropertiesByShopName = dataAccessManager.getSupplychainShopPropertiesByShopName(shopName, types, null, null);
        return success(supplychainShopPropertiesByShopName);
    }

    /**
     * 获取绑定的仓库
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/26
     */
    @Override
    public JSONObject getSupplychainShopPropertiesForBind() {
        //查找所有的 非虚拟仓 非子仓的实体仓库
        List<SupplychainShopProperties> list = dataAccessManager.getSupplychainShopPropertiesForBindingSelect(null, null);
        return success(list);
    }

    @Override
    public JSONObject bindSupplychainShopProperties() {
        String shopId = getUTF("shopId");
        String parentId = getUTF("parentId");

        //迭代查找当前商店是否可以绑定 这个父仓（只要这个仓库 下面的仓库没有这个父仓就行，如果有 绑定失败，告诉原因）
        boolean isBinding = findIsBinding(shopId, parentId);
        if (isBinding) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", shopId);
            map.put("parentId", parentId);
            dataAccessManager.updateSupplychainShopPropertiesByParam(map);
            return success("绑定仓库成功");
        } else {
            return fail("该父仓是本仓库的子仓,不能绑定！");
        }
    }

    /**
     * 获取门店的统计
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/6/2
     */
    @Override
    public JSONObject getSupplychainShopPropertiesTotal() {
        Integer type = getIntParameter("type");//区分 日：0 周：1 月：2

        JSONObject jsonObject = new JSONObject();
        //统计门店总数
        List<String> list = new ArrayList<>();
        list.add("6");
        list.add("7");
        int totalShopNum = dataAccessManager.getSupplychainShopPropertiesByTypeCount(list);//已存在的门店数

        //统计 从昨天开始数量以内的每天的门店增长记录 门店  默认12天的 可以前端传值
        //根据类型 判断是什么样的报表

        //把每个map 按照顺序给转成两个list
        List<String> dateArry = new ArrayList<>();
        List<Long> numArry = new ArrayList<>();

        //今日新增门店数
        String startTime = getUTF("startTime", null);
        String endTime = getUTF("endTime", null);

        if (type == 0) {//日
            if (startTime == null && endTime == null) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, -1);
                Date time = calendar.getTime();
                endTime = DateUtils.format(time, DateUtils.FORMAT_SHORT); //1天前

                Calendar calendar1 = Calendar.getInstance();
                calendar1.add(Calendar.DATE, -12);
                Date time1 = calendar1.getTime();
                startTime = DateUtils.format(time1, DateUtils.FORMAT_SHORT); //12天前
            }
            //算出当前时间内的每一天的数量
            List<ShopReportMapperVO>  shopDetailNumForDay = dataAccessManager.getSupplychainShopPropertiesByTypeCountForEveryDay(list, startTime, endTime);
            if (shopDetailNumForDay != null && !shopDetailNumForDay.isEmpty()) {
                shopDetailNumForDay.forEach(x -> {
                    dateArry.add(x.getDate());
                    numArry.add(x.getNum());
                });
            }
        } else if (type == 1) {//周
            if (startTime == null && endTime == null) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -3);
                Date time = calendar.getTime();
                startTime = DateUtils.format(time, DateUtils.FORMAT_SHORT); //三个月
                endTime = DateUtils.format(new Date(), DateUtils.FORMAT_SHORT);//今天
            }
            //统计三个月
            List<ShopReportMapperVO> shopDetailNumForWeek = dataAccessManager.getSupplychainShopPropertiesByTypeCountForEveryWeek(list,startTime,endTime);
            if (shopDetailNumForWeek != null && !shopDetailNumForWeek.isEmpty()) {
                shopDetailNumForWeek.forEach(x->{
                    dateArry.add(x.getDate());
                    numArry.add(x.getNum());
                });
            }

        } else if (type == 2) {//月
            if (startTime == null && endTime == null) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -12);
                Date time = calendar.getTime();
                startTime = DateUtils.format(time, DateUtils.FORMAT_SHORTS); //十二个月
                endTime = DateUtils.format(new Date(), DateUtils.FORMAT_SHORTS);//今天
            }
            //统计三个月
            List<ShopReportMapperVO>  shopDetailNumForMonth = dataAccessManager.getSupplychainShopPropertiesByTypeCountForEveryMonth(list,startTime,endTime);
            if (shopDetailNumForMonth != null && !shopDetailNumForMonth.isEmpty()) {
                shopDetailNumForMonth.forEach(x->{
                    dateArry.add(x.getDate());
                    numArry.add(x.getNum());
                });
            }
        }
        //统计当前新增的门店
        Calendar calendar = Calendar.getInstance();
        Date time = calendar.getTime();
        String todayTime = DateUtils.format(time, DateUtils.FORMAT_SHORT); //今天的

        List<ShopReportMapperVO> shopDetailNumForToday = dataAccessManager.getSupplychainShopPropertiesByTypeCountForEveryDay(list, todayTime, todayTime);
        //获取当前的记录
        if(shopDetailNumForToday != null && shopDetailNumForToday.size() > 0){
            Long aLong = shopDetailNumForToday.get(0).getNum();
            jsonObject.put("todayAddNum", aLong);
        }else{
            jsonObject.put("todayAddNum", 0);
        }
        jsonObject.put("day", dateArry);
        jsonObject.put("num", numArry);
        jsonObject.put("allShopNum", totalShopNum);
        return success(jsonObject);
    }


    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -3);
        Date time = calendar.getTime();
        String startTime = DateUtils.format(time, DateUtils.FORMAT_SHORT); //三个月
        String endTime = DateUtils.format(new Date(), DateUtils.FORMAT_SHORT);//今天
        System.out.println(startTime);
        System.out.println(endTime);
    }

    /**
     * 根据用户id获取门店信息
     *
     * @param userId
     * @return
     */
    public SupplychainShopProperties getSupplychainShopPropertiesByUid(String userId) {
        SupplychainShopProperties supplychainShopProperties;
        // 店员
        SupplychainShopBindRelationship supplychainShopBindRelationship =
                dataAccessManager.getSupplychainShopBindRelationshipByUserIdAndType(userId, ShopBindRelationshipTypeConstant.CLERK_BIND_SHOP);
        if (supplychainShopBindRelationship != null) {
            supplychainShopProperties = dataAccessManager.getSupplychainShopPropertiesById(supplychainShopBindRelationship.getShopId());
        } else {
            // 店长
            supplychainShopProperties = dataAccessManager.getSupplychainShopPropertiesByUserId(userId);
        }
        return supplychainShopProperties;
    }

    @Override
    public JSONObject getSupplychainShopPropertiesByUid() {
        String userId = getUTF("userId");
        SupplychainShopProperties supplychainShopProperties = getSupplychainShopPropertiesByUid(userId);
        if (supplychainShopProperties == null) {
            return fail("当前用户无关联门店数据");
        }
        return success(supplychainShopProperties);
    }

    @Override
    public JSONObject UpdateBindPrinterCodeByUserId() {
        String userId = getUTF("userId");
        String bindPrinterCode = getUTF("bindPrinterCode");
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.UpdateBindPrinterCodeByUserId(userId,bindPrinterCode);
        if (supplychainShopProperties == null) {
            return fail("当前用户无关联门店数据");
        }
        return success(supplychainShopProperties);
    }

    /**
     * 是否有绑定
     *
     * @param
     * @return boolean
     * @author oy
     * @date 2019/5/26
     */
    private boolean findIsBinding(String shopId, String parentId) {
        //拿当前要绑定的父仓去找它的父仓 一直遍历往上找 直到父仓id 为空 就行了 没有出现shopId说明可以绑定，出现了就不能绑定的
        //查找当前的
        if (parentId.equals("0")) {
            return true;
        }
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(parentId);
        if (supplychainShopProperties != null) {
            String parentId1 = supplychainShopProperties.getParentId();
            String id = supplychainShopProperties.getId();
            if (parentId1 != null && !parentId1.equals("") && !parentId1.equals(id)) {
                if (!parentId1.equals(shopId)) {
                    boolean isBinding = findIsBinding(id, parentId1);//递归
                    if (isBinding) {
                        return true;
                    }
                } else {
                    return false;//不能绑定
                }
            } else { //父仓等于空的时候 说明没有绑定的
                return true;
            }
        }
        return false;
    }


    /**
     * 异步添加用户
     *
     * @param sysUser
     * @return void
     * @author oy
     * @date 2019/5/10
     */
    @Async
    protected void addUserAsyc(SysUser sysUser) {
        SysUserRemoteService.addSysUser(sysUser, getHttpServletRequest());
    }

    /**
     * 异步添加终端绑定
     *
     * @param sysUserTerminal
     * @return void
     * @author oy
     * @date 2019/5/10
     */
    @Async
    protected void addUseTerminal(SysUserTerminal sysUserTerminal) {
        SysUserTerminalService.addSysUserTerminal(sysUserTerminal, getHttpServletRequest());
    }


    @Override
    public JSONObject getSupplychainShopProperties() {
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(getId());
        return success(supplychainShopProperties);
    }

    @Override
    public JSONObject getSupplychainShopPropertiess() {
        String name = getUTF("name", null);//名店信息  或者是商家号码
        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        String type = getUTF("type", null);
        int auth = getIntParameter("auth", -1);

        if (type != null) {

            List<SupplychainShopProperties> supplychainShopPropertiess = dataAccessManager.getSupplychainShopPropertiess(status, getPageStartIndex(getPageSize()), getPageSize(), name, type, auth);
            fillSupplychainShopProperties(supplychainShopPropertiess);
            Map<String, String> paramMap = new HashMap<>();
            int count = dataAccessManager.getSupplychainShopPropertiesCount(status, name, type, auth, paramMap);
            return success(supplychainShopPropertiess, count);

        }
        return success();
    }

    @Override
    public JSONObject getSupplychainShopPropertiess2() {

        String nameOrPhone = getUTF("nameOrPhone", null);//名店信息  或者是商家号码
        String type = getUTF("type");//类型
        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        int auth = getIntParameter("auth", -1);
        String areaId = getUTF("areaId", null);//地址id
        String province = getUTF("province", null); //省
        String city = getUTF("city", null);//市
        String district = getUTF("district", null);//区
        String createTime = getUTF("createTime", null);//时间 "2019-5-15 - 2019-6-10"

        String startTime = null;
        String endTime = null;

        if (createTime != null && !createTime.equals("")) {
            startTime = dateDateFormateHandler(createTime, 0);
            endTime = dateDateFormateHandler(createTime, 1);
        }
        Map<String, String> paramMap = new HashMap<>();
        if (areaId != null && !areaId.trim().equals("")) {
            paramMap.put("areaId", areaId);
        }
        if (province != null && !province.trim().equals("")) {
            paramMap.put("province", province);
        }
        if (city != null && !city.trim().equals("")) {
            paramMap.put("city", city);
        }
        if (district != null && !district.trim().equals("")) {
            paramMap.put("district", district);
        }
        if (startTime != null && !startTime.trim().equals("")) {
            paramMap.put("startTime", startTime);
        }
        if (endTime != null && !endTime.trim().equals("")) {
            paramMap.put("endTime", endTime);
        }
        List<SupplychainShopProperties> supplychainShopPropertiess = null;
        if (type != null) {
            supplychainShopPropertiess = dataAccessManager.getSupplychainShopPropertiess2(status, getPageStartIndex(getPageSize()), getPageSize(), nameOrPhone, type, auth, paramMap);
            if (supplychainShopPropertiess != null && supplychainShopPropertiess.size() > 0) {
                fillSupplychainShopProperties(supplychainShopPropertiess);
                int count = dataAccessManager.getSupplychainShopPropertiesCount(status, nameOrPhone, type, auth, paramMap);//20190509
                return success(supplychainShopPropertiess, count);
            }
        }
        return success(supplychainShopPropertiess, 0);
    }

    /**
     * @param s, num
     * @return java.lang.String
     * @author oy
     * @date 2019/5/9
     */
    private String dateDateFormateHandler(String s, int num) {
        String[] split = s.split(" - ");
        String[] split1 = split[num].split("-");
        String s1 = split1[0];
        String s2 = split1[1];
        String s3 = split1[2];
        if (s2.length() == 1) {
            s2 = "0" + s2;
        }
        if (s3.length() == 1) {
            s3 = "0" + s3;
        }
        String data = s1 + "-" + s2 + "-" + s3;
        return data;
    }


    private void fillSupplychainShopProperties(List<SupplychainShopProperties> supplychainShopPropertiess) {
        int size = supplychainShopPropertiess.size();
        for (int i = 0; i < size; i++) {
//            Double cost = Double.parseDouble(supplychainShopPropertiess.get(i).getDistributionCost() + "") / 100;
//            supplychainShopPropertiess.get(i).setDistributionCostStr(CommonUtils.formatTwo(cost) + "");

            SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(supplychainShopPropertiess.get(i).getId());
            if (supplychainShopProperties != null) {
                String parentId = supplychainShopPropertiess.get(i).getParentId();
                if (parentId != null && !parentId.equals("") && !parentId.equals("0")) {
                    SupplychainShopProperties supplychainShopProperties1 = dataAccessManager.getSupplychainShopProperties(parentId);
                    if (supplychainShopProperties1 != null) {
                        supplychainShopPropertiess.get(i).setParentName(supplychainShopProperties1.getShopName());
                    }
                }
            }

            try {
                if (supplychainShopPropertiess.get(i).getLogicAreaId() != null) {
                    SysArea sysArea = AreaRemoteService.getSysArea(supplychainShopPropertiess.get(i).getLogicAreaId(), getHttpServletRequest());
                    if (sysArea != null) {
                        supplychainShopPropertiess.get(i).setLogicAreaName(sysArea.getTitle());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public JSONObject getSupplychainShopPropertiesAll() {
        String types = getUTF("type");

        List<SupplychainShopProperties> supplychainShopPropertiess = dataAccessManager.getSupplychainShopPropertiesAll(types);
        fillSupplychainShopProperties(supplychainShopPropertiess);
        return success(supplychainShopPropertiess);
    }

    @Override
    public JSONObject getSupplychainShopPropertiesCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        return success();
    }

    @Override
    public JSONObject changeSupplychainShopProperties() {
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(getId());

        //归属者通行证ID
        String userId = getUTF("userId", "0");
        //归属渠道
        String channelId = getUTF("channelId", null);
        //仓库的名字
        String shopName = getUTF("shopName", null);
        //当前仓库状态 0表示关仓 1表示开仓 2不对外开放(理论上只有一个--直营主仓)
        Integer status = getIntParameter("status", 0);
        //仓库类型 如：0-直营仓、1-虚拟仓、2-第三方仓、3-合作仓、4-代理仓等
        Integer type = getIntParameter("type");
        //归属ID
        String ownerId = getUTF("ownerId", null);
        //管理员姓名
        String adminName = getUTF("adminName", null);
        //联系人号码
        String phoneNumber = getUTF("phoneNumber", null);
        //归属的逻辑区域ID
        String logicAreaId = getUTF("logicAreaId", null);
        //所在省份
        String province = getUTF("province", null);
        //所在城市
        String city = getUTF("city", null);
        //所在区域
        String district = getUTF("district", null);
        //具体地址
        String address = getUTF("address", null);
        //所在地址经纬度 格式：longitude,latitude
        String location = getUTF("location", null);
        //父级仓库ID 一级仓时为0
        String parentId = getUTF("parentId", "0");
        //是否同步父级仓库存 0-独立库存 1-本仓+父级仓库存 2-仅读取父级库存
        Integer syncParentStock = getIntParameter("syncParentStock", 0);
        //配送类型 1-我方配送 2-独立配送
        Integer distributionType = getIntParameter("distributionType", 0);
        //覆盖距离 单位：米
        Long coveringDistance = getLongParameter("coveringDistance", 0);
        //配送费用 单位：分
        Long distributionCost = getLongParameter("distributionCost", 0);
        //打印类型 1：下单后 2：支付后 3：接单后 可扩展
        Integer printerType = getIntParameter("printerType", 0);
        //绑定的小票打印机编码
        String bindPrinterCode = getUTF("bindPrinterCode", null);
        Long moq = getLongParameter("moq", 0);
        supplychainShopProperties.setUserId(userId);
        supplychainShopProperties.setChannelId(channelId);
        supplychainShopProperties.setShopName(shopName);
        supplychainShopProperties.setStatus(status);
        supplychainShopProperties.setType(type);
        supplychainShopProperties.setOwnerId(ownerId);
        supplychainShopProperties.setAdminName(adminName);
        supplychainShopProperties.setPhoneNumber(phoneNumber);
        supplychainShopProperties.setLogicAreaId(logicAreaId);
        supplychainShopProperties.setProvince(province);
        supplychainShopProperties.setCity(city);
        supplychainShopProperties.setDistrict(district);
        supplychainShopProperties.setAddress(address);
        supplychainShopProperties.setLocation(location);
        supplychainShopProperties.setParentId(parentId);
        supplychainShopProperties.setSyncParentStock(syncParentStock);
        supplychainShopProperties.setDistributionType(distributionType);
        supplychainShopProperties.setCoveringDistance(coveringDistance);
        supplychainShopProperties.setDistributionCost(distributionCost);
        supplychainShopProperties.setPrinterType(printerType);
        supplychainShopProperties.setBindPrinterCode(bindPrinterCode);
        supplychainShopProperties.setMoq(moq);
        dataAccessManager.changeSupplychainShopProperties(supplychainShopProperties);
        return success();
    }

    @Override
    @Transactional
    public JSONObject changeSupplychainShopProperties2() {
        String shopPropertiesEntity = getUTF("shopPropertiesEntity");
        if (shopPropertiesEntity != null && !shopPropertiesEntity.equals("")) {
            SupplychainShopProperties supplychainShopProperties = JSONObject.parseObject(shopPropertiesEntity, SupplychainShopProperties.class);
            if (supplychainShopProperties != null) {
                dataAccessManager.changeSupplychainShopProperties(supplychainShopProperties);
                return success("修改成功！");
            }
        }
        return success();
    }

    /**
     * 查找当前用户的商店 通用
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getSupplychainShopPropertiesByUserIdCommon() {
        SysUser user = getUser();
        //通过当前用户 查找用的的商店信息
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopPropertiesByUserId(user.getId());

        return success(supplychainShopProperties);
    }

    /**
     * 获取当前用户的商店信息
     *
     * @return
     */
    @Override
    public JSONObject getSupplychainShopPropertiesByUserId() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //通过当前用户 查找用的的商店信息
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopPropertiesByUserId(user.getId());
        SupplychainShopPropertiesVO vo = new SupplychainShopPropertiesVO();
        if (supplychainShopProperties != null) {
            vo.setShopName(supplychainShopProperties.getShopName());//商店名称
            vo.setPhoneNumber(supplychainShopProperties.getPhoneNumber());//店铺电话
            vo.setAddress(supplychainShopProperties.getAddress());//具体地址
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("supplychainShopProperties", vo);
        return success(jsonObject);
    }

    /**
     * 修改当前用户的商店名称
     *
     * @return
     */
    @Override
    public JSONObject changeSupplychainShopPropertiesById() {
        String name = getUTF("name");//商店名字
        if (name == null || name.equals("")) {
            return fail("商店名字不能为空！");
        }

        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        dataAccessManager.changeSupplychainShopPropertiesById(user.getId(), name);
        return success("修改成功！");
    }

    /**
     * 查询当前的用户的供应店信息
     *
     * @return
     */
    @Override
    public JSONObject selectSupplychainShopPropertiesById() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopPropertiesByUserId(user.getId());
        JSONObject jsonObject = new JSONObject();
        if (supplychainShopProperties != null) {
            jsonObject.put("phoneNumber", supplychainShopProperties.getPhoneNumber().replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2"));
            jsonObject.put("shopName", supplychainShopProperties.getShopName());
        }
        return success(jsonObject);
    }

    /**
     * 供应商端：修改供应商的营业状态
     *
     * @return
     */
    @Override
    public JSONObject changeSupplychainShopPropertiesStatus() {
        SysUser user = getUser();//获取用户
        String userId = user.getId();
        int status = getIntParameter("status");//0表示关仓 1表示开仓 2不对外开放

        List<Integer> types = new ArrayList<>();//SupplyChainShopTypeEnum 因为现在是供应商端的 可能出现的type = 5 8 9
        types.add(SupplyChainShopTypeEnum.SUPPLIER.getKey());
        types.add(SupplyChainShopTypeEnum.WHOLESALE.getKey());
        types.add(SupplyChainShopTypeEnum.MANUFACTURER.getKey());
        //能确定唯一的一个 添加的时候 要注意不能一个用户有多个 5 8 9状态
        dataAccessManager.changeSupplychainShopPropertiesStatusByIdAndTypes(userId, types, status);
        return success("修改营业状态成功！");
    }

    /**
     * 供应商端：获取供应商页面内容
     *
     * @return
     */
    @Override
    public JSONObject getSupplychainShopPropertiesVO() {
        SysUser user = getUser();
        List<Integer> list = new ArrayList<>();
        list.add(SupplyChainShopTypeEnum.WHOLESALE.getKey());
        list.add(SupplyChainShopTypeEnum.SUPPLIER.getKey());
        list.add(SupplyChainShopTypeEnum.MANUFACTURER.getKey());
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopPropertiesByUserIdAndTypeId(user.getId(), list);
        if (supplychainShopProperties == null) {
            return fail("无商店信息！");
        }
        SupplychainShopPropertiesForSupplierVO vo = new SupplychainShopPropertiesForSupplierVO();
        vo.setAddress(supplychainShopProperties.getAddress());
        vo.setBindPrinterCode(supplychainShopProperties.getBindPrinterCode());
        vo.setCoveringDistance(supplychainShopProperties.getCoveringDistance());
        vo.setPhoneNumber(supplychainShopProperties.getPhoneNumber());
        vo.setStatus(supplychainShopProperties.getStatus());
        return success(vo);
    }

    /**
     * 修改配送距离
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/3/30
     */
    @Override
    public JSONObject changeSupplychainShopPropertiesCoveringDistance() {
        SysUser user = getUser();//获取用户
        String userId = user.getId();
        Integer coveringDistance = getIntParameter("coveringDistance");//获取配送的距离

        if (coveringDistance == null) {
            return fail("配送距离不能为空！");
        }

        List<Integer> types = new ArrayList<>();//SupplyChainShopTypeEnum 因为现在是供应商端的 可能出现的type = 5 8 9
        types.add(SupplyChainShopTypeEnum.MANUFACTURER.getKey());
        types.add(SupplyChainShopTypeEnum.SUPPLIER.getKey());
        types.add(SupplyChainShopTypeEnum.WHOLESALE.getKey());

        //能确定唯一的一个 添加的时候 要注意不能一个用户有多个 5 8 9状态
        dataAccessManager.changeSupplychainShopPropertiesCoveringDistanceByIdAndTypes(userId, types, coveringDistance);
        return success("修改配送距离成功！");
    }

    /**
     * 修改打印机设备
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/3/30
     */
    @Override
    public JSONObject changeSupplychainShopPropertiesBindPrinterCode() {
        SysUser user = getUser();//获取用户
        String userId = user.getId();
        String bindPrinterCode = getUTF("bindPrinterCode");//获取配送的距离

        List<Integer> types = new ArrayList<>();//SupplyChainShopTypeEnum 因为现在是供应商端的 可能出现的type = 5 8 9
        types.add(SupplyChainShopTypeEnum.MANUFACTURER.getKey());
        types.add(SupplyChainShopTypeEnum.SUPPLIER.getKey());
        types.add(SupplyChainShopTypeEnum.WHOLESALE.getKey());

        //能确定唯一的一个 添加的时候 要注意不能一个用户有多个 5 8 9状态
        dataAccessManager.changeSupplychainShopPropertiesBindPrinterCodeByIdAndTypes(userId, types, bindPrinterCode);
        return success("修改打印机设备成功！");
    }

    /**
     * 修改联系号码
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/3/30
     */
    @Override
    public JSONObject changeSupplychainShopPropertiesPhoneNumber() {
        SysUser user = getUser();//获取用户
        String userId = user.getId();
        String phoneNumber = getUTF("phoneNumber");//获取联系电话
        if (phoneNumber == null || phoneNumber.equals("")) {
            return fail("联系电话不能为空！");
        }

        List<Integer> types = new ArrayList<>();//SupplyChainShopTypeEnum 因为现在是供应商端的 可能出现的type = 5 8 9
        types.add(SupplyChainShopTypeEnum.MANUFACTURER.getKey());
        types.add(SupplyChainShopTypeEnum.SUPPLIER.getKey());
        types.add(SupplyChainShopTypeEnum.WHOLESALE.getKey());

        //能确定唯一的一个 添加的时候 要注意不能一个用户有多个 5 8 9状态
        dataAccessManager.changeSupplychainShopPropertiesPhoneNumber(userId, types, phoneNumber);
        return success("修改联系电话成功！");
    }

    /***
     * 查找绑定的仓库（如果没有绑定的 就找附近的）
     * @author oy
     * @date 2019/4/12
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject findRelationShopKey() {

        SysUser sysUser = getUser();
        Double longitude = getDoubleParameter("longitude");
        Double latitude = getDoubleParameter("latitude");
        List<SupplychainShopBindRelationship> shopBindRelationships = dataAccessManager.getShopBindRelationships(sysUser.getId(), ClientTypeEnum.CONSUMER.getKey());
        if (!CommonUtils.isEmpty(shopBindRelationships)) {
            for (SupplychainShopBindRelationship shopBindRelationship : shopBindRelationships) {
                if (shopBindRelationship.getShopId() != null && shopBindRelationship.getShopId().length() > 0) {
                    SupplychainShopProperties shopProperties = dataAccessManager.getSupplychainShopProperties(shopBindRelationship.getShopId());
                    if (shopProperties.getStatus() != ShopStatusEnum.OPEN.getKey()) {
                        continue;
                    }
                    return success(shopProperties);
                }
            }
        }
        return success(findRecentSelfShop(longitude, latitude));
    }

    /**
     * <pre>
     *     <b>寻找距离最近的自营仓库</b> 后台用 查询全部
     * </pre>
     *
     * @param longitude double 经度
     * @param latitude  double 纬度
     * @return 返回合适的自营仓库；当没有适合的仓库时，返回null
     */
    public SupplychainShopProperties findRecentSelfShop(double longitude, double latitude) {
        try {
            try {
                List<SupplychainShopProperties> selfShop = dataAccessManager.getSupplychainShopPropertiesAll("");
                SupplychainShopProperties supplychainShopProperties = handleShopPropertiest(selfShop, longitude, latitude);
                return supplychainShopProperties;
            } finally {
            }
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        return null;
    }


    /***
     * 查找绑定的仓库（如果没有绑定的 就找附近的）
     * @author oy
     * @date 2019/4/12
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject findRecentSelfShopForRemote() {
        SysUser sysUser = getUser();
        Double longitude = getDoubleParameter("longitude");
        Double latitude = getDoubleParameter("latitude");

        //通过当前的用户获取当前的商店
        String userId = sysUser.getId();
        SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(userId);
        if (supplychainShopPropertiesByUserId != null) {
            String bindShopId = supplychainShopPropertiesByUserId.getId();//商店id
            //查看绑定的仓库 一个门店只有一个仓库
            int type = 0;
            SupplychainWarehourseBindRelationship bindRelationship = dataAccessManager.getSupplychainWarehourseBindRelationshipsByBindShopId(bindShopId, type);
            if (bindRelationship != null) {
                String shopId1 = bindRelationship.getShopId();
                //获取对应的
                SupplychainShopProperties shopProperties = dataAccessManager.getSupplychainShopProperties(shopId1);
                if (shopProperties.getStatus() == ShopStatusEnum.OPEN.getKey()) {
                    return success(shopProperties);
                }
            }
        }
        return success(findRecentSelfShop2(longitude, latitude));
    }

    /**
     * <pre>
     *     <b>寻找距离最近的自营仓库（开仓状态的）</b> （远程调用服务用）
     * </pre>
     *
     * @param longitude double 经度
     * @param latitude  double 纬度
     * @return 返回合适的自营仓库；当没有适合的仓库时，返回null
     */
    public SupplychainShopProperties findRecentSelfShop2(double longitude, double latitude) {
        try {
            try {
                List<String> list = new ArrayList<>();
                list.add(SupplyChainShopTypeEnum.STRAIGHT_BARN.getKey() + "");
                list.add(SupplyChainShopTypeEnum.VIRTUAL_WAREHOUSE.getKey() + "");
                list.add(SupplyChainShopTypeEnum.THIRD_PARTY_WAREHOUSE.getKey() + "");
                list.add(SupplyChainShopTypeEnum.COOPERATIVE_WAREHOUSE.getKey() + "");
                list.add(SupplyChainShopTypeEnum.AGENCY_WAREHOUSE.getKey() + "");
                String types = list.stream().collect(Collectors.joining(","));
                List<SupplychainShopProperties> selfShop = dataAccessManager.getSupplychainShopPropertiesAll2(types);//查找 0 1 2 3 4的仓库
                SupplychainShopProperties supplychainShopProperties = handleShopPropertiest(selfShop, longitude, latitude);
                return supplychainShopProperties;
            } finally {
            }
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        return null;
    }

    /***
     * 处理查询出来的仓库，判断是否符合 返回符合条件的仓库
     * @param selfShop
     * @param longitude
     * @param latitude
     * @return
     */
    public SupplychainShopProperties handleShopPropertiest(List<SupplychainShopProperties> selfShop, double longitude, double latitude) {
        SupplychainShopProperties shop = null;
        int lastDistance = 0;
        if (selfShop == null) {
            return null;
        }
        int size = selfShop.size();
        for (int i = 0; i < size; i++) {

            SupplychainShopProperties shopProperties = selfShop.get(i);
            if (shopProperties == null) {
                continue;
            }
            if (shopProperties.getStatus() == ShopStatusEnum.CLOSE.getKey()) {
                // 关闭状态
                continue;
            }
            if (shopProperties.getStatus() == ShopStatusEnum.ONLY.getKey()) {
                // 不对外开放 -- 主仓
                continue;
            }
            int distance = SimpleLocationUtils.simpleDistance(latitude, longitude, Double.parseDouble(shopProperties.getLocation().split(",")[1]), Double.parseDouble(shopProperties.getLocation().split(",")[0]));
            if (distance > shopProperties.getCoveringDistance()) {
                // 超出配送距离
                continue;
            }
            if (shop == null || lastDistance > distance) {
                shop = shopProperties;
                lastDistance = distance;
            }
        }
        if (shop != null && shop.getType() == SupplyChainShopTypeEnum.VIRTUAL_WAREHOUSE.getKey()) { // 虚拟仓时 往上匹配物理仓
            if (shop.getParentId() == null) {
                return null;
            }
            shop = dataAccessManager.getSupplychainShopProperties(shop.getParentId());
        }
        return shop;
    }

    @Override
    public JSONObject authSupplychainShopProperties() {
        int result = dataAccessManager.authSupplychainShopProperties(getId(), SupplychainShopAuthEnum.PASS.getKey());
        return result > 1 ? success() : fail();
    }

    @Override
    public JSONObject deleteSupplychainShopProperties() {
        String ids = getUTF("ids");
        if (StringUtils.isEmpty(ids)) {
            return fail("参数不能为空");
        }
        List<String> shopIds = JSONObject.parseArray(ids, String.class);
        List<SupplychainShopProperties> supplychainShopPropertiesList = dataAccessManager.getSupplychainShopPropertiesByIds(shopIds);
        if (CollectionUtils.isEmpty(supplychainShopPropertiesList)) {
            return fail("无匹配数据");
        }
        // 逻辑删除仓库
        dataAccessManager.deleteSupplychainShopProperties(shopIds);

        for (SupplychainShopProperties supplychainShopProperties : supplychainShopPropertiesList) {
            if (supplychainShopProperties != null) {
                deleteRelationshipByShopIdAndType(supplychainShopProperties);
            }
        }
        return success();
    }

    @Async
    void deleteRelationshipByShopIdAndType(SupplychainShopProperties supplychainShopProperties) {

        switch (supplychainShopProperties.getType()) {
            // 仓库
            case SupplyChainShopTypeConstant.STRAIGHT_BARN:
            case SupplyChainShopTypeConstant.VIRTUAL_WAREHOUSE:
            case SupplyChainShopTypeConstant.THIRD_PARTY_WAREHOUSE:
            case SupplyChainShopTypeConstant.COOPERATIVE_WAREHOUSE:
            case SupplyChainShopTypeConstant.AGENCY_WAREHOUSE:
                // 删除仓库绑定供应商关系
                // 删除仓库绑定门店关系
                dataAccessManager.deleteSupplychainWarehourseBindRelationship(supplychainShopProperties.getId(), 0);
                dataAccessManager.deleteSupplychainWarehourseBindRelationship(supplychainShopProperties.getId(), 1);
                break;
            // 供应商
            case SupplyChainShopTypeConstant.SUPPLIER:
            case SupplyChainShopTypeConstant.WHOLESALE:
            case SupplyChainShopTypeConstant.MANUFACTURER:
                // 清除供应商分类绑定关系
                dataAccessManager.clearSupplychainGoodBindRelationships(supplychainShopProperties.getUserId());
                break;
            // 门店
            case SupplyChainShopTypeConstant.ORDINARY_STORES:
            case SupplyChainShopTypeConstant.DIRECT_SHOP:
                // 清除门店绑定商品
                dataAccessManager.clearSupplychainGoodsByShopId(supplychainShopProperties.getId());
                // 清除门店分类绑定关系
                dataAccessManager.clearSupplychainGoodBindRelationships(supplychainShopProperties.getUserId());
                break;
            default:
                break;
        }
    }

    /**
     * POS：获取首页商品分类
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/29
     */
    @Override
    public JSONObject getSupplychainShopPropertiesForPosHome() {
        //获取当前用户
        SysUser user = getUser();
        String userId = user.getId();

        //查看该id 是否有商店
        SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(userId);
        String shopId = null;
        if (supplychainShopPropertiesByUserId != null) {//说明该用户就是店主
            shopId = supplychainShopPropertiesByUserId.getId();
        } else {//再去查看该用户是属于哪个店的
            List<SupplychainShopBindRelationship> shopBindRelationships = dataAccessManager.getShopBindRelationships(userId, ShopBindRelationshipTypeEnum.CLERK_BIND_SHOP.getType());
            if (shopBindRelationships != null && shopBindRelationships.size() > 0) {
                SupplychainShopBindRelationship supplychainShopBindRelationship = shopBindRelationships.get(0);//这里只有一个 取一个就行
                shopId = supplychainShopBindRelationship.getShopId();
            } else {
                return success();
            }
        }
        //通过商店id 获取对应的商品
        List<SupplychainGood> goodList = dataAccessManager.getsupplychaingoodByShopIdAndPriceId(shopId, null, null, null, null, null);
        List<GoodType> goodTypes = null;
        if (goodList != null && goodList.size() > 0) {
            List<String> goodTypeIds = goodList.stream().map(SupplychainGood::getGoodTypeId).distinct().collect(Collectors.toList());
            //远程调用分类
            try {
                goodTypes = GoodTypeRemoteService.getGoodTypes(goodTypeIds, getHttpServletRequest());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("shopId", shopId);
        jsonObject.put("goodTypes", goodTypes);
        return success(jsonObject);
    }

    /**
     * POS：根据商品分类获取首页商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/30
     */
    @Override
    public JSONObject getSupplychainShopPropertiesForPosHomeGood() {
        //分类id
        String typeId = getUTF("typeId");
        String shopId = getUTF("shopId");
        if (typeId == null || typeId.equals("")) {
            return fail("typeId不能为空！");
        }
        if (shopId == null || shopId.equals("")) {
            return fail("shopId不能为空！");
        }
        List<String> typeIds = new ArrayList<>();
        typeIds.add(typeId);
        List<SupplychainGood> supplychainGoodByShopIdAndTypes = dataAccessManager.getSupplychainGoodByShopIdAndTypes(shopId, typeIds, null, null);
        //获取对应的价格id
        if (supplychainGoodByShopIdAndTypes != null && supplychainGoodByShopIdAndTypes.size() > 0) {
            int count = supplychainGoodByShopIdAndTypes.size();
            List<SupplychainGoodVO> goodPriceIdAndUnitId = getGoodPriceIdAndUnitId(supplychainGoodByShopIdAndTypes);
            return success(goodPriceIdAndUnitId, count);
        }
        return success(new ArrayList(), 0);
    }

    /**
     * 获取商品的详细信息 单位 价格标签
     *
     * @param supplychainGoodById
     * @return java.util.List<com.tuoniaostore.supplychain.vo.SupplychainGoodVO>
     * @author oy
     * @date 2019/4/15
     */
    private List<SupplychainGoodVO> getGoodPriceIdAndUnitId(List<SupplychainGood> supplychainGoodById) {
        List<SupplychainGoodVO> list = new ArrayList<>();//返回的结果
        List<String> priceIds = supplychainGoodById.stream().map(SupplychainGood::getPriceId).distinct().collect(Collectors.toList());//将id 拿到 然后去重 成集合
        //通过价格id集合去查找对应的单位和价格标签
        List<GoodPriceCombinationVO> allGoodPricesByPriceIds = GoodPricePosRemoteService.getTemplateByPriceIdByParam(priceIds, null, getHttpServletRequest());
        if (allGoodPricesByPriceIds != null && allGoodPricesByPriceIds.size() > 0) {
            supplychainGoodById.forEach(x -> {
                String priceId = x.getPriceId();
                Optional<GoodPriceCombinationVO> first = allGoodPricesByPriceIds.stream().filter(item -> item.getPriceId().equals(priceId)).findFirst();
                SupplychainGoodVO vo = new SupplychainGoodVO();
                vo.setId(x.getId());//id
                vo.setTemplateId(x.getGoodTemplateId());//模板id
                vo.setDefineImage(x.getDefineImage());
                vo.setDefineName(x.getDefineName());//名称
                vo.setStock(x.getStock());//库存
                vo.setStatus(x.getStatus());//状态
                vo.setDescribed(x.getDescription());//描述
                //20190509新增
                vo.setSalePrice(x.getSalePrice());  //建议售价
                vo.setColleaguePrice(x.getColleaguePrice());    //同行价
                vo.setMemberPrice(x.getMemberPrice());  //会员价
                vo.setSiglePrice(x.getSellPrice());//销售
                vo.setCostPrice(x.getCostPrice());//成本价
                vo.setDiscountPrice(x.getDiscountPrice());//促销价
                vo.setMarketPrice(x.getMarketPrice());//市场价
                vo.setSellPrice(x.getSellPrice());//交易价
                vo.setCreateTime(x.getCreateTime());//创建时间
                if (first.isPresent()) {
                    GoodPriceCombinationVO goodPriceCombinationVO = first.get();
                    List<GoodBarcode> barcodesByPriceId = GoodBarcodeRemoteService.getBarcodesByPriceId(goodPriceCombinationVO.getPriceId(), getHttpServletRequest());
                    //条码需要解析
                    vo.setBarcodeList(barcodesByPriceId);//条码
                    vo.setBarcode(goodPriceCombinationVO.getBarcode());//条码
                    //一二级名字和id
                    vo.setTypeNameOne(goodPriceCombinationVO.getTypeNameOneName());
                    vo.setTypeNameOneId(goodPriceCombinationVO.getTypeNameOneId());
                    vo.setTypeNameTwo(goodPriceCombinationVO.getTypeNameTwoName());
                    vo.setTypeNameTwoId(goodPriceCombinationVO.getTypeNameTwoId());
                    vo.setBrandId(goodPriceCombinationVO.getBrandId());//品牌id
                    vo.setBrandLogo(goodPriceCombinationVO.getBrandId());//品牌logo
                    vo.setBrandName(goodPriceCombinationVO.getBrandName());//品牌名称
                    vo.setPriceId(goodPriceCombinationVO.getPriceId());//价格id
                    vo.setPriceTitle(goodPriceCombinationVO.getPriceTitle());//价格标签
                    vo.setUnit(goodPriceCombinationVO.getUnitTitle());//单位
                    vo.setUnitId(goodPriceCombinationVO.getUnitTitleId());//单位id
                    vo.setTypeIconOne(goodPriceCombinationVO.getTypeIconOne());//一级分类的icon
                    vo.setTypeIconTwo(goodPriceCombinationVO.getTypeIconTwo());//二级分类的icon
                    vo.setTypeImageOne(goodPriceCombinationVO.getTypeImageOne());//一级分类图片
                    vo.setTypeImageTwo(goodPriceCombinationVO.getTypeImageTwo());//二级分类图片
                    vo.setUserful(0);
                    vo.setShelfLife(goodPriceCombinationVO.getShelfLife());//保质期
                } else {
                    vo.setUserful(1);
                }
                list.add(vo);
            });
        }
        return list;
    }


    /**
     * 获取供应商详情和统计总收入
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/1
     */
    @Override
    public JSONObject getSupplychainShopForSupplier() {
        String type = getUTF("type");//5,8,9 类型
        String name = getUTF("name", null);

        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        int auth = getIntParameter("auth", -1);

        if (type != null) {
            List<SupplychainShopProperties> supplychainShopPropertiess = dataAccessManager.getSupplychainShopPropertiess(0, getPageStartIndex(getPageSize()), getPageSize(), name, type, 1);
            //查询总共条数
            Map<String, String> paramMap = new HashMap<>();
            int count = dataAccessManager.getSupplychainShopPropertiesCount(status, name, type, auth, paramMap);

            //远程获取对应的收入总算
            if (supplychainShopPropertiess != null && supplychainShopPropertiess.size() > 0) {
                fillSupplychainShopProperties(supplychainShopPropertiess);
                List<String> userIds = supplychainShopPropertiess.stream().map(SupplychainShopProperties::getUserId).distinct().collect(Collectors.toList());
                List<PaymentCurrencyOffsetLoggerVO> paymentTransactionLoggerTotalMoneyByUser = PaymentCurrencyOffsetLoggerRemoteService.getPaymentTransactionLoggerTotalMoneyByUser(userIds, getHttpServletRequest());
                if (paymentTransactionLoggerTotalMoneyByUser != null && paymentTransactionLoggerTotalMoneyByUser.size() > 0) {
                    supplychainShopPropertiess.forEach(x -> {
                        String userId = x.getUserId();
                        Optional<PaymentCurrencyOffsetLoggerVO> first = paymentTransactionLoggerTotalMoneyByUser.stream().filter(item -> item.getUserId().equals(userId)).findFirst();
                        if (first.isPresent()) {
                            PaymentCurrencyOffsetLoggerVO paymentCurrencyOffsetLoggerVO = first.get();
                            x.setTotalMoney(paymentCurrencyOffsetLoggerVO.getTotalMoney());
                        }
                    });
                }
            }
            return success(supplychainShopPropertiess, count);
        }
        return success();
    }

    /**
     * 根据当前用户查找对应的商店
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/5
     */
    @Override
    public JSONObject getSupplychainShopByLoginUser() {
        SysUser user = getUser();
        if (user != null) {
            SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(user.getId());
            if (supplychainShopPropertiesByUserId != null) {
                return success(supplychainShopPropertiesByUserId);
            } else {
                SysUser sysUser = null;
                try {
                    sysUser = SysUserRmoteService.getSysUser(user.getId(), getHttpServletRequest());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (sysUser != null) {
                    String leaderUserId = sysUser.getLeaderUserId();
                    if (leaderUserId != null && !leaderUserId.equals("")) {
                        SupplychainShopProperties supplychainShopPropertiesByUserId1 = dataAccessManager.getSupplychainShopPropertiesByUserId(leaderUserId);
                        supplychainShopPropertiesByUserId.setDiscount(OrderReceiptTitel.DISCOUNT);//折扣
                        return success(supplychainShopPropertiesByUserId1);
                    } else {
                        return fail("无商店信息，请联系管理员。");
                    }
                }
            }
        }
        return success();
    }

    /**
     * 查找该用户是否有商店
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/9
     */
    @Override
    public JSONObject getExistSupplychainShopByUserId() {
        String phoneNum = getUTF("phoneNum");
        if (phoneNum == null || phoneNum.equals("")) {
            return fail("电话号码不能为空！");
        }

        JSONObject jsonObject = new JSONObject();
        SysUser sysUser = null;
        //查询是否有该用户
        try {
            sysUser = SysUserRemoteService.getSysUserByPhoneNumForRemote(phoneNum, getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //如果有用户 查询是否有门店
        if (sysUser != null) {
            //查询该用户是否有商店
            SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(sysUser.getId());
            if (supplychainShopPropertiesByUserId != null) {
                jsonObject.put("exist", 1);//有用户 有商店
                jsonObject.put("userId", supplychainShopPropertiesByUserId.getId());
                jsonObject.put("code", 0);//状态码
            } else {
                jsonObject.put("exist", 2);//有用户 无商店
                jsonObject.put("userId", sysUser.getId());
                jsonObject.put("code", 0);//状态码
            }
        } else {
            jsonObject.put("exist", 0);//无用户
            jsonObject.put("userId", null);
            jsonObject.put("code", 0);//状态码
        }
        return jsonObject;
    }


}
