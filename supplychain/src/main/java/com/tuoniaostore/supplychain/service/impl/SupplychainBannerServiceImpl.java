package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.support.community.SysUserAddressRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplychainShopPropertiesRemoteService;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.datamodel.supplychain.SupplychainBanner;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.user.SysUserAddress;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;


@Service("supplychainBannerService")
public class SupplychainBannerServiceImpl extends BasicWebService implements SupplychainBannerService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainBanner() {
        SupplychainBanner supplychainBanner = new SupplychainBanner();
        //广告编码
        String adcode = getUTF("adcode", null);
        //自定义区域ID
        String defineAreaId = getUTF("defineAreaId", null);
        //仓库ID
        String shopId = getUTF("shopId");
        //banner类型，类型：分类连接，商品详情，跳转图片，专题活动等等
        Integer type = getIntParameter("type", 0);
        //排序 越小越前
        Integer sort = getIntParameter("sort", 0);
        //图片链接、用于展示
        String imageUrl = getUTF("imageUrl", null);
        //点击的跳转链接，与type结合
        String clickUrl = getUTF("clickUrl", null);
        //当前状态，1、开启中 0、关闭中
        Integer status = getIntParameter("status", 0);
        //开启时间
        String openTimeStr = getUTF("openTime", null);
        //关闭时间
        String closeTimeStr = getUTF("closeTime", null);
        Date openTime = null;
        Date closeTime = null;
        if (!StringUtils.isEmpty(openTimeStr)) {
            openTime = DateUtils.parse(openTimeStr);
        }
        if (!StringUtils.isEmpty(closeTimeStr)) {
            closeTime = DateUtils.parse(closeTimeStr);
        }
        supplychainBanner.setAdcode(adcode);
        supplychainBanner.setDefineAreaId(defineAreaId);
        supplychainBanner.setShopId(shopId);
        supplychainBanner.setType(type);
        supplychainBanner.setSort(sort);
        supplychainBanner.setImageUrl(imageUrl);
        supplychainBanner.setClickUrl(clickUrl);
        supplychainBanner.setStatus(status);
        supplychainBanner.setOpenTime(openTime);
        supplychainBanner.setCloseTime(closeTime);
        dataAccessManager.addSupplychainBanner(supplychainBanner);
        return success();
    }

    @Override
    public JSONObject getSupplychainBanner() {
        SupplychainBanner supplychainBanner = dataAccessManager.getSupplychainBanner(getId());
        fullShopNameMsg(supplychainBanner);
        return success(supplychainBanner);
    }

    @Override
    public JSONObject getSupplychainBanners() {
        String shopId = getUTF("shopId");
        String areaId = getUTF("areaId", null);
        List<SupplychainBanner> supplychainBanners = dataAccessManager.getSupplychainBannersByShopIdAndAreaId(shopId, areaId);
        return success(supplychainBanners);
    }

    @Override
    public JSONObject getSupplychainBannerAll() {
        List<SupplychainBanner> supplychainBanners = dataAccessManager.getSupplychainBannerAll();
        fullListShopNameMsg(supplychainBanners);
        return success(supplychainBanners);
    }

    @Override
    public JSONObject getSupplychainBannerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainBannerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainBanner() {
        String id = getUTF("id");
        SupplychainBanner supplychainBanner = dataAccessManager.getSupplychainBanner(id);
        if (supplychainBanner == null) {
            return fail("数据不存在");
        }
        //城市编码
        String adcode = getUTF("adcode", null);
        //自定义区域ID
        String defineAreaId = getUTF("defineAreaId", null);
        //仓库ID
        String shopId = getUTF("shopId");
        //banner类型，类型：分类连接，商品详情，跳转图片，专题活动等等
        Integer type = getIntParameter("type", 0);
        //排序 越小越前
        Integer sort = getIntParameter("sort", 0);
        //图片链接、用于展示
        String imageUrl = getUTF("imageUrl", null);
        //点击的跳转链接，与type结合
        String clickUrl = getUTF("clickUrl", null);
        //当前状态，1、开启中 0、关闭中
        Integer status = getIntParameter("status", 0);
        //开启时间
        String openTimeStr = getUTF("openTime", null);
        //关闭时间
        String closeTimeStr = getUTF("closeTime", null);
        Date openTime = null;
        Date closeTime = null;
        if (!StringUtils.isEmpty(openTimeStr)) {
            openTime = DateUtils.parse(openTimeStr);
        }
        if (!StringUtils.isEmpty(closeTimeStr)) {
            closeTime = DateUtils.parse(closeTimeStr);
        }
        supplychainBanner.setId(id);
        supplychainBanner.setAdcode(adcode);
        supplychainBanner.setDefineAreaId(defineAreaId);
        supplychainBanner.setShopId(shopId);
        supplychainBanner.setType(type);
        supplychainBanner.setSort(sort);
        supplychainBanner.setImageUrl(imageUrl);
        supplychainBanner.setClickUrl(clickUrl);
        supplychainBanner.setStatus(status);
        supplychainBanner.setOpenTime(openTime);
        supplychainBanner.setCloseTime(closeTime);
        dataAccessManager.changeSupplychainBanner(supplychainBanner);
        return success();
    }


    /***
     * 商家端采购：获取首页的banner
     * @author oy
     * @date 2019/4/12
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getSupplychainBannerFirst() {
        //获取当前地址
        Double longitude = getDoubleParameter("longitude", 999);//没有坐标的时候 传999
        Double latitude = getDoubleParameter("latitude", 999);

        String shopId = null;//商品id
        String areaId = null;//地址id

        SupplychainShopProperties supplychainShopPropertiest = getSupplychainShopPropertiest(longitude, latitude);
        if (supplychainShopPropertiest != null) {
            shopId = supplychainShopPropertiest.getId();
            areaId = supplychainShopPropertiest.getLogicAreaId();
        }
        List<SupplychainBanner> supplychainBanners = null;
        if (shopId != null && areaId != null) {//商店id 和 地址id 去查找对应的bannerId
            supplychainBanners = dataAccessManager.getSupplychainBannerByShopIdAndAreaId(shopId, areaId);
        }
        //通过供应商的id 和 地址id 查找对应的banner信息
        return success(supplychainBanners);
    }

    /**
     * 商家端采购：首页获取仓库地址
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/15
     */
    @Override
    public JSONObject getSupplychainShopFirst() {
        //获取当前地址
        Double longitude = getDoubleParameter("longitude", 999);//没有坐标的时候 传999
        Double latitude = getDoubleParameter("latitude", 999);

        SupplychainShopProperties supplychainShopPropertiest = getSupplychainShopPropertiest(longitude, latitude);
        if (supplychainShopPropertiest != null) {
            return success(supplychainShopPropertiest);
        }
        return success();
    }

    @Override
    public JSONObject batchDeleteSupplychainBanner() {
        String ids = getUTF("ids");
        if (StringUtils.isEmpty(ids)) {
            return fail("无入参数据");
        }
        List<String> list = JSONObject.parseArray(ids, String.class);
        if (!CollectionUtils.isEmpty(list)) {
            dataAccessManager.batchDeleteSupplychainBanner(list);
        }
        return success("删除成功！");
    }

    /**
     * 根据经纬度查找当前的仓库（如果没有坐标 就拿用户的默认坐标）
     *
     * @param longitude, latitude
     * @return com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties
     * @author oy
     * @date 2019/4/15
     */
    public SupplychainShopProperties getSupplychainShopPropertiest(Double longitude, Double latitude) {
        if (longitude == 999 && latitude == 999) {
            //选择默认地址上传过来
            String userId = getUser().getId();//用户id
            SysUserAddress defaultSysUserAddress = SysUserAddressRemoteService.getDefaultSysUserAddress(userId, getHttpServletRequest());
            if (defaultSysUserAddress != null) { //说明有默认地址
                //根据默认地址去查询附近的仓库
                longitude = defaultSysUserAddress.getLatitude();
                latitude = defaultSysUserAddress.getLongitude();
            }
        }
        //根据经纬度查找当前附近的仓库
        SupplychainShopProperties relationShopKey = SupplychainShopPropertiesRemoteService.findRelationShopKey(longitude, latitude, getHttpServletRequest());//查询最近的 或者该用户绑定的仓库
        return relationShopKey;
    }


    /**
     * 填充商店名称(List)
     */
    private void fullListShopNameMsg(List<SupplychainBanner> supplychainBanners) {
        int size = supplychainBanners.size();
        if (supplychainBanners != null && size > 0) {
            for (int i = 0; i < size; i++) {
                SupplychainBanner supplychainBanner = supplychainBanners.get(i);
                if (supplychainBanner != null) {
                    String shopId = supplychainBanner.getShopId();
                    SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(shopId);
                    if (supplychainShopProperties != null) {
                        supplychainBanner.setShopName(supplychainShopProperties.getShopName());
                    }
                }
            }
        }
    }

    /**
     * 填充商店名称
     */
    private void fullShopNameMsg(SupplychainBanner supplychainBanners) {
        if (supplychainBanners != null) {
            String shopId = supplychainBanners.getShopId();
            SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(shopId);
            if (supplychainShopProperties != null) {
                supplychainBanners.setShopName(supplychainShopProperties.getShopName());
            }
        }
    }


}
