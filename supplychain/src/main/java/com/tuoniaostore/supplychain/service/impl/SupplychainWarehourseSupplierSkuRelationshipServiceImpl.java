package com.tuoniaostore.supplychain.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseBindRelationship;
import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseSupplierSkuRelationship;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainWarehourseSupplierSkuRelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/6/3
 */
@Service("supplychainWarehourseSupplierSkuRelationshipService")
public class SupplychainWarehourseSupplierSkuRelationshipServiceImpl  extends BasicWebService implements SupplychainWarehourseSupplierSkuRelationshipService {

    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject getSupplychainWarehourseSupplierSkuRelationships() {
        Map<String,Object> map = new HashMap<>();
        String shopId = getUTF("shopId");
        String priceId = getUTF("priceId");
        map.put("shopId",shopId);
        map.put("priceId",priceId);
        List<SupplychainWarehourseSupplierSkuRelationship> supplychainWarehourseSupplierSkuRelationshipByParam = dataAccessManager.getSupplychainWarehourseSupplierSkuRelationshipByParam(map);
        return success(supplychainWarehourseSupplierSkuRelationshipByParam);
    }

    /***
     * 添加仓库绑定供应商的商品（priceId）
     * @author oy
     * @date 2019/6/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    @Transactional
    public JSONObject addSupplychainWarehourseSupplierSkuRelationships() {
        String shopId = getUTF("shopId");//仓库id
        String supplierId = getUTF("supplierId");//供应商id

        String priceIds = getUTF("priceIds");//价格集合
        //清除当前的绑定 当前当前仓库id 和供应商id
        dataAccessManager.clearSupplychainWarehourseSupplierSkuRelationshipByShopIdAndsupplierId(shopId,supplierId);
        if(priceIds != null && !priceIds.equals("")){
            String userId = getUser().getId();
            List<String> priceIds1 = JSONObject.parseArray(priceIds, String.class);
            if(priceIds1 != null && priceIds1.size() > 0 ){
                List<SupplychainWarehourseSupplierSkuRelationship> addList = new ArrayList<>();
                priceIds1.forEach(x->{
                    SupplychainWarehourseSupplierSkuRelationship addEntity = new SupplychainWarehourseSupplierSkuRelationship();
                    addEntity.setUserId(userId);
                    addEntity.setShopId(shopId);
                    addEntity.setSupplierId(supplierId);
                    addEntity.setPriceId(x);
                    addEntity.setSort(0);
                    addList.add(addEntity);
                });
                dataAccessManager.batchAddSupplychainWarehourseSupplierSkuRelationship(addList);
            }
        }
        return success("修改成功！");
    }

}
