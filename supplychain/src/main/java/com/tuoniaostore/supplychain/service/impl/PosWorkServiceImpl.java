package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.order.OrderStatus;
import com.tuoniaostore.commons.constant.payment.PayTypeEnum;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.commons.support.order.OrderEntryRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplychainShopBindRelationshipRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplychainShopPropertiesRemoteService;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.supplychain.PosWork;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopBindRelationship;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.vo.order.OrderPayTypeSumAndRefundStatisticVo;
import com.tuoniaostore.datamodel.vo.order.OrderPayTypeSumVo;
import com.tuoniaostore.supplychain.SupplychainAop;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.PosWorkService;
import com.tuoniaostore.supplychain.vo.PosWorkVo;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;


@Service("posWorkService")
public class PosWorkServiceImpl extends BasicWebService implements PosWorkService {
    private static final org.slf4j.Logger loggerAop = LoggerFactory.getLogger(SupplychainAop.class);
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addPosWork() {
        PosWork posWork = new PosWork();
        //
        String id = getUTF("id", null);
        //门店ID
        String shopId = getUTF("shopId", null);
        //
        String userId = getUTF("userId", null);
        //总收入
        Long totalIncome = getLongParameter("totalIncome", 0);
        //总订单数
        Integer totalOrder = getIntParameter("totalOrder", 0);
        //支付宝订单数
        Integer alipayCount = getIntParameter("alipayCount", 0);
        //微信订单数
        Integer wxCount = getIntParameter("wxCount", 0);
        //现金总条数
        Integer cashCount = getIntParameter("cashCount", 0);
        //交班输入现金数
        Long inputCashIncome = getLongParameter("inputCashIncome", 0);
        //支付宝收入
        Long alipayIncome = getLongParameter("alipayIncome", 0);
        //微信收入
        Long wxIncome = getLongParameter("wxIncome", 0);
        //现金收入
        Long cashIncome = getLongParameter("cashIncome", 0);
        //上班时间
        Long loginTime = getLongParameter("loginTime", 0);
        //交班时间
        Long logoutTime = getLongParameter("logoutTime", 0);
        if (id != null) {
            posWork.setId(id);
        }
        posWork.setId(id);
        posWork.setShopId(shopId);
        posWork.setUserId(userId);
        posWork.setTotalIncome(totalIncome);
        posWork.setTotalOrder(totalOrder);
        posWork.setAlipayCount(alipayCount);
        posWork.setWxCount(wxCount);
        posWork.setCashCount(cashCount);
        posWork.setInputCashIncome(inputCashIncome);
        posWork.setAlipayIncome(alipayIncome);
        posWork.setWxIncome(wxIncome);
        posWork.setCashIncome(cashIncome);
        dataAccessManager.addPosWork(posWork);
        return success();
    }

    @Override
    public JSONObject getPosWork() {
        PosWork posWork = dataAccessManager.getPosWork(getId());
        return success(posWork);
    }

    @Override
    public JSONObject getPosWorks() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<PosWork> posWorks = dataAccessManager.getPosWorks(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getPosWorkCount(status, name);
        return success(posWorks, count);
    }

    @Override
    public JSONObject getPosWorkAll() {
        List<PosWork> posWorks = dataAccessManager.getPosWorkAll();
        return success(posWorks);
    }

    @Override
    public JSONObject getPosWorkCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getPosWorkCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changePosWork() {
        PosWork posWork = dataAccessManager.getPosWork(getId());
        //
        String id = getUTF("id", null);
        //门店ID
        String shopId = getUTF("shopId", null);
        //
        String userId = getUTF("userId", null);
        //总收入
        Long totalIncome = getLongParameter("totalIncome", 0);
        //总订单数
        Integer totalOrder = getIntParameter("totalOrder", 0);
        //支付宝订单数
        Integer alipayCount = getIntParameter("alipayCount", 0);
        //微信订单数
        Integer wxCount = getIntParameter("wxCount", 0);
        //现金总条数
        Integer cashCount = getIntParameter("cashCount", 0);
        //交班输入现金数
        Long inputCashIncome = getLongParameter("inputCashIncome", 0);
        //支付宝收入
        Long alipayIncome = getLongParameter("alipayIncome", 0);
        //微信收入
        Long wxIncome = getLongParameter("wxIncome", 0);
        //现金收入
        Long cashIncome = getLongParameter("cashIncome", 0);
        //上班时间
        Long loginTime = getLongParameter("loginTime", 0);
        //交班时间
        Long logoutTime = getLongParameter("logoutTime", 0);
        posWork.setId(id);
        posWork.setShopId(shopId);
        posWork.setUserId(userId);
        posWork.setTotalIncome(totalIncome);
        posWork.setTotalOrder(totalOrder);
        posWork.setAlipayCount(alipayCount);
        posWork.setWxCount(wxCount);
        posWork.setCashCount(cashCount);
        posWork.setInputCashIncome(inputCashIncome);
        posWork.setAlipayIncome(alipayIncome);
        posWork.setWxIncome(wxIncome);
        posWork.setCashIncome(cashIncome);
        dataAccessManager.changePosWork(posWork);
        return success();
    }

    /**
     * 新增交班信息
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/6
     */
    @Override
    public JSONObject addBusinessShift() {

        return null;
    }

    /**
     * 结算交班
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/7
     */
    @Override
    public JSONObject endBusinessShift() {
        return null;
    }

    /**
     * 添加PosWork
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/13
     */
    @Override
    public JSONObject addPosWorkModel() {
        String posWork = getUTF("posWork");
        loggerAop.info("=======进入添加方法======");
        loggerAop.info(posWork);
        PosWork posWorks = JSONObject.parseObject(posWork, PosWork.class);
        dataAccessManager.addPosWork(posWorks);
        loggerAop.info("=======进入添加方法完成======");

        return success();
    }

    /**
     * 交班接口
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/13
     */
    @Override
    public JSONObject isExchangeDuty() {
        SysUser user = getUser();
        if (user == null) {
            fail("用户登录失效");
        }
        int isExchangeDuty = getIntParameter("isExchangeDuty");//是否交班
        int practicalCashIncome = getIntParameter("practicalCashIncome", 0);//实际清点金额
        String shopId = "";
        SupplychainShopProperties shops = dataAccessManager.getSupplychainShopPropertiesByUserId(user.getId());
        if (shops == null) {

            List<SupplychainShopBindRelationship> shopBindRelationships = dataAccessManager.getShopBindRelationships(user.getId(), 5);
            if (shopBindRelationships.size() > 0) {
                SupplychainShopProperties supplychainShopPropertiesById = dataAccessManager.getSupplychainShopPropertiesById(shopBindRelationships.get(0).getShopId());
                if (supplychainShopPropertiesById != null) {
                    shopId = supplychainShopPropertiesById.getId();
                }
            } else {
                SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(user.getId());
                if (supplychainShopPropertiesByUserId != null) {
                    shopId = supplychainShopPropertiesByUserId.getId();
                }
            }
            if (shopId.equals("")) {
                return fail("未找到用户商店信息");
            }
        } else {
            shopId = shops.getId();
        }

        if(shopId.equals("")){
            return  fail("未找到该用户商店信息");
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("userId", user.getId());
        map.put("shopId", shopId);
        map.put("workStatus", "0");
        PosWork posWork = dataAccessManager.getPosWrokByMap(map);
         if (posWork == null) {
            return fail("未找到值班信息");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date loginTime = posWork.getLoginTime();//上班时间
        Date loginTimeOut = new Date(); // 交班时间
        List<OrderPayTypeSumAndRefundStatisticVo> orderEntryGruopByPayTtpe = OrderEntryRemoteService.getRefundOrder(sdf.format(loginTime), sdf.format(loginTimeOut), shopId, getHttpServletRequest());
        List<OrderEntry> returnOrderEntry = OrderEntryRemoteService.getOrderEntryReturn(sdf.format(loginTime), sdf.format(loginTimeOut), shopId, getHttpServletRequest());
        int orderNum = 0;
        long money = 0;
        long wxIncome = 0;          //微信
        long zfbIncome = 0;         //支付宝
        long cashIncome = 0;        //现金
        int wxOrder = 0;            //微信订单数
        int zfbOrder = 0;           //支付宝订单数
        int cashOrder = 0;          //现金订单数
        long refundPrice = 0 ;      //退款金额
        Integer refundOrderNumber = 0 ; //退款订单数
        if (orderEntryGruopByPayTtpe != null && orderEntryGruopByPayTtpe.size() > 0) {
            for (OrderPayTypeSumAndRefundStatisticVo orderPayTypeSumVo : orderEntryGruopByPayTtpe) {
                orderNum += Integer.parseInt(orderPayTypeSumVo.getNumber());
                money += orderPayTypeSumVo.getMoneySum();
                if (orderPayTypeSumVo.getPaymentType() == PayTypeEnum.TRANS_TYPE_WEI.getKey()) {
                    wxIncome = orderPayTypeSumVo.getMoneySum();
                    wxOrder = Integer.parseInt((orderPayTypeSumVo.getNumber()==null?"0":orderPayTypeSumVo.getNumber()));
                }
                if (orderPayTypeSumVo.getPaymentType() == PayTypeEnum.TRANS_TYPE_CASH.getKey()) {
                    cashIncome = orderPayTypeSumVo.getMoneySum();
                    cashOrder = Integer.parseInt((orderPayTypeSumVo.getNumber()==null?"0":orderPayTypeSumVo.getNumber()));
                }
                if (orderPayTypeSumVo.getPaymentType() == PayTypeEnum.TRANS_TYPE_ZFB.getKey()) {
                    zfbIncome = orderPayTypeSumVo.getMoneySum();
                    zfbOrder = Integer.parseInt((orderPayTypeSumVo.getNumber()==null?"0":orderPayTypeSumVo.getNumber()));
                }
            }
        }
         if(returnOrderEntry != null && returnOrderEntry.size() > 0 ){
             refundOrderNumber = returnOrderEntry.size();
             for (OrderEntry order : returnOrderEntry) {
                 if (order.getStatus() == OrderStatus.ORDER_TYPE_REFUND.getKey()){
                    if(order.getFindPrice() != null && order.getTotalPrice() != null ){
                        refundPrice+=order.getTotalPrice() - order.getFindPrice();
                    }else{
                        refundPrice+=order.getActualPrice();
                    }
                 }
             }
         }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("orderNum", orderNum);
        jsonObject.put("refundPrice", refundPrice);//退款金额
        jsonObject.put("refundOrderNumber", refundOrderNumber);//退款订单数
        jsonObject.put("money", money);
        jsonObject.put("orderSum", orderEntryGruopByPayTtpe);
        if(user.getRealName()!= null && !user.getRealName().equals("")){
            jsonObject.put("dutyUser",user.getRealName());//值班用户
        }else{
            jsonObject.put("dutyUser",user.getPhoneNumber());//值班用户
        }
        jsonObject.put("loginTime", sdf.format(loginTime));//上班时间
        jsonObject.put("loginTimeOut", sdf.format(loginTimeOut)); // 交班时间
        long l = loginTimeOut.getTime() - loginTime.getTime();
        long day = l / (24 * 60 * 60 * 1000);
        long hour = (l / (60 * 60 * 1000) - day * 24);
        long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        jsonObject.put("dutyTime", "" + day + "天" + hour + "小时" + min + "分" + s + "秒"); // 上班时长

        if (isExchangeDuty == 1) {
            PosWork posWk = new PosWork();
            BeanUtils.copyProperties(posWork, posWk);
            posWk.setTotalIncome(money);
            posWk.setTotalOrder(orderNum);
            posWk.setAlipayCount(zfbOrder);//支付宝订单数
            posWk.setWxCount(wxOrder);//微信订单数
            posWk.setCashCount(cashOrder);//现金订单数
            posWk.setAlipayIncome(zfbIncome);//支付宝收入
            posWk.setWxIncome(wxIncome);//微信收入
            posWk.setCashIncome(cashIncome);//现金收入
            posWk.setLogoutTime(new Date());//下班时间
            posWk.setWorkStatus(1);//现金收入
            posWk.setRedundPrice(refundPrice);
            posWk.setRefundOrder(refundOrderNumber);
            posWk.setPracticalCashIncome(practicalCashIncome);//实际清点金额
            dataAccessManager.changePosWork(posWk);
            return success(jsonObject);
        }
        return success(jsonObject);
    }

    /**
     * 交班记录
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/15
     */
    @Override
    public JSONObject getPosWrokListByMap() {
        HashMap<String, Object> map = new HashMap<>();
        JSONObject jsonObject = new JSONObject();
        String startTime = getUTF("startTime", null);
        String endTime = getUTF("endTime", null);
        String accountOrName = getUTF("accountOrName", null);
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String shopId = "";
        SupplychainShopProperties shops = dataAccessManager.getSupplychainShopPropertiesByUserId(user.getId());
        if (shops == null) {

            List<SupplychainShopBindRelationship> shopBindRelationships = dataAccessManager.getShopBindRelationships(user.getId(), 5);
            if (shopBindRelationships.size() > 0) {
                SupplychainShopProperties supplychainShopPropertiesById = dataAccessManager.getSupplychainShopPropertiesById(shopBindRelationships.get(0).getShopId());
                if (supplychainShopPropertiesById != null) {
                    shopId = supplychainShopPropertiesById.getId();
                }
            } else {
                SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(user.getId());
                if (supplychainShopPropertiesByUserId != null) {
                    shopId = supplychainShopPropertiesByUserId.getId();
                }
            }
            if (shopId.equals("")) {
                return fail("未找到用户商店信息");
            }
        } else {
            shopId = shops.getId();
        }

        if(shopId.equals("")){
            return  fail("未找到该用户商店信息");
        }


        if (accountOrName != null) {
            List<SysUser> sysUserAccountOrName = SysUserRmoteService.getSysUserAccountOrName(accountOrName, getHttpServletRequest());
            if (sysUserAccountOrName != null && sysUserAccountOrName.size() > 0) {
                List<String> userId = sysUserAccountOrName.stream().map(SysUser::getId).collect(Collectors.toList());
                map.put("accountOrName", userId);
            } else {
                //没有查到数据
                return success(null,0);
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Format f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (endTime != null) {
                //结束时间加一天
                Date sDate = sdf.parse(endTime);
                Calendar c = Calendar.getInstance();
                c.setTime(sDate);
                c.add(Calendar.DAY_OF_MONTH, 1);
                sDate = c.getTime();
                endTime = f.format(sDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        map.put("workStatus", 1);
        map.put("shopId", shopId);
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        map.put("pageIndex", getPageStartIndex(getPageSize()));
        map.put("pageSize", getPageSize());
        List<PosWork> posWrokList = dataAccessManager.getPosWrokListByMap(map);
        Integer count = dataAccessManager.getPosWrokListCountByMap(map);

        //查询所有用户
        List<String> userId = posWrokList.stream().map(PosWork::getUserId).distinct().collect(Collectors.toList());
        if(userId != null && userId.size() > 0){
            List<SysUser> sysUserInIds = SysUserRemoteService.getSysUserInIds(userId, getHttpServletRequest());
            HashMap<String, Object> userMap = new HashMap<>();

            if(sysUserInIds != null && sysUserInIds.size() > 0){
                posWrokList.forEach(x ->{
                    String id = x.getUserId();
                    Optional<SysUser> first = sysUserInIds.stream().filter(item -> item.getId().equals(id)).findFirst();
                    if(first.isPresent()){
                        SysUser sysUser = first.get();
                        x.setUserName(sysUser.getRealName());
                        x.setUserAccount(sysUser.getPhoneNumber());
                    }
                    x.setCreateTimeStr(DateUtils.format(x.getCreateTime()));
                });
            }
        }
        return success(posWrokList,count);
    }

    /**
     * 交班记录详情
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/15
     */
    @Override
    public JSONObject getPosWrokDetil() {
        String id = getUTF("id");
        PosWork posWork = dataAccessManager.getPosWork(id);
        if(posWork == null){
            return  fail("未找到交班数据");
        }

        Date loginTime = posWork.getLoginTime();
        Date logoutTime = posWork.getLogoutTime();
        String shopId = posWork.getShopId();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String loginTimestr = sdf.format(loginTime);
        String logoutTimestr = sdf.format(logoutTime);
        //  支付方式统计
        List<OrderPayTypeSumAndRefundStatisticVo> orderEntryGruopByPayTtpeStatistic = OrderEntryRemoteService.getRefundOrder(loginTimestr, logoutTimestr, shopId,getHttpServletRequest());


        List<PosWorkVo> list = new ArrayList<>();
        PosWorkVo posWorkVo = new PosWorkVo();//微信
        PosWorkVo posWorkVo1 = new PosWorkVo();//支付宝
        PosWorkVo posWorkVo2 = new PosWorkVo();//现金支付
        long refundPrice = 0;
        Integer refundOrderNumber = 0;
        //封装退款数据
        loggerAop.info("=============退款数据封装============="+orderEntryGruopByPayTtpeStatistic);
        if(orderEntryGruopByPayTtpeStatistic != null && orderEntryGruopByPayTtpeStatistic.size() > 0){
            loggerAop.info("=============进入退款数据封装=============");
            for (OrderPayTypeSumAndRefundStatisticVo refund : orderEntryGruopByPayTtpeStatistic) {
                if(refund.getRefundNumber() != null){
                    refundOrderNumber+=refund.getRefundNumber();
                }
                refundPrice+=refund.getRefundMoneySum();
                if(refund.getPaymentType() == PayTypeEnum.TRANS_TYPE_WEI.getKey()){
                    posWorkVo.setRefundNumber(refund.getRefundNumber());
                    posWorkVo.setRefundMoneySum(refund.getRefundMoneySum());
                }else if(refund.getPaymentType() == PayTypeEnum.TRANS_TYPE_ZFB.getKey()){
                    posWorkVo1.setRefundNumber(refund.getRefundNumber());
                    posWorkVo1.setRefundMoneySum(refund.getRefundMoneySum());
                }else {
                    posWorkVo2.setRefundNumber(refund.getRefundNumber());
                    posWorkVo2.setRefundMoneySum(refund.getRefundMoneySum());
                }
            }
        }

        posWorkVo.setOrderNumber(posWork.getWxCount());
        posWorkVo.setReceiptMoney(posWork.getWxIncome());
        posWorkVo.setPayType("微信支付");
        list.add(posWorkVo);
        posWorkVo1.setOrderNumber(posWork.getAlipayCount());
        posWorkVo1.setReceiptMoney(posWork.getAlipayIncome());
        posWorkVo1.setPayType("支付宝支付");
        list.add(posWorkVo1);
        posWorkVo2.setOrderNumber(posWork.getCashCount());
        posWorkVo2.setReceiptMoney(posWork.getCashIncome());
        posWorkVo2.setPayType("现金支付");
        list.add(posWorkVo2);



        posWork.setRefundPrice(refundPrice);
        posWork.setRefundOrderNumber(refundOrderNumber);
        String exchangeUser = "";
        if (posWork.getUserId() != null) {
            try {
                SysUser sysUser = SysUserRmoteService.getSysUser(posWork.getUserId(), getHttpServletRequest());
                if (sysUser != null) {
                    posWork.setUserAccount(sysUser.getPhoneNumber());
                    posWork.setUserName(sysUser.getRealName());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        long l = posWork.getLogoutTime().getTime() - posWork.getLoginTime().getTime();
        long day = l / (24 * 60 * 60 * 1000);
        long hour = (l / (60 * 60 * 1000) - day * 24);
        long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s=(l/1000-day*24*60*60-hour*60*60-min*60);

        posWork.setCreateTimeStr(DateUtils.format(posWork.getCreateTime()));
        posWork.setLoginTimeStr(DateUtils.format(posWork.getLoginTime()));
        posWork.setLogoutTimeStr(DateUtils.format(posWork.getLogoutTime()));

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("dutyTime", "" + day + "天" + hour + "小时" + min + "分"+s+"秒"); // 上班时长
        jsonObject.put("list", list);
        jsonObject.put("posWork", posWork);
        return success(jsonObject);
    }

    @Override
    public JSONObject getPosWorkByUserId() {
        String workStatus = getUTF("workStatus");
        String userId = getUTF("userId");
        String shopId = getUTF("shopId");
        HashMap<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        map.put("shopId", shopId);
        map.put("workStatus", workStatus);
        PosWork posWrokByMap = dataAccessManager.getPosWrokByMap(map);
        return success(posWrokByMap);
    }


}
