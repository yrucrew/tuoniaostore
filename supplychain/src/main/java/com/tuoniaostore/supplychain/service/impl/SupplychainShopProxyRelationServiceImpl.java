package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProxyRelation;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainShopProxyRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainShopProxyRelationService")
public class SupplychainShopProxyRelationServiceImpl extends BasicWebService implements SupplychainShopProxyRelationService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainShopProxyRelation() {
        SupplychainShopProxyRelation supplychainShopProxyRelation = new SupplychainShopProxyRelation();
        //
        String id = getUTF("id", null);
        //主要角色ID
        String hostWarehouseId = getUTF("hostWarehouseId", null);
        //客人角色ID
        String guestWarehouseId = getUTF("guestWarehouseId", null);
        //关系类型 如：独代、合作等
        Integer type = getIntParameter("type", 0);
        //关系状态 1--可用 2--不可用
        Integer status = getIntParameter("status", 0);
        supplychainShopProxyRelation.setId(id);
        supplychainShopProxyRelation.setHostWarehouseId(hostWarehouseId);
        supplychainShopProxyRelation.setGuestWarehouseId(guestWarehouseId);
        supplychainShopProxyRelation.setType(type);
        supplychainShopProxyRelation.setStatus(status);
        dataAccessManager.addSupplychainShopProxyRelation(supplychainShopProxyRelation);
        return success();
    }

    @Override
    public JSONObject getSupplychainShopProxyRelation() {
        SupplychainShopProxyRelation supplychainShopProxyRelation = dataAccessManager.getSupplychainShopProxyRelation(getId());
        return success(supplychainShopProxyRelation);
    }

    @Override
    public JSONObject getSupplychainShopProxyRelations() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainShopProxyRelation> supplychainShopProxyRelations = dataAccessManager.getSupplychainShopProxyRelations(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainShopProxyRelationCount(status, name);
        return success(supplychainShopProxyRelations, count);
    }

    @Override
    public JSONObject getSupplychainShopProxyRelationAll() {
        List<SupplychainShopProxyRelation> supplychainShopProxyRelations = dataAccessManager.getSupplychainShopProxyRelationAll();
        return success(supplychainShopProxyRelations);
    }

    @Override
    public JSONObject getSupplychainShopProxyRelationCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainShopProxyRelationCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainShopProxyRelation() {
        SupplychainShopProxyRelation supplychainShopProxyRelation = dataAccessManager.getSupplychainShopProxyRelation(getId());
        //
        String id = getUTF("id", null);
        //主要角色ID
        String hostWarehouseId = getUTF("hostWarehouseId", null);
        //客人角色ID
        String guestWarehouseId = getUTF("guestWarehouseId", null);
        //关系类型 如：独代、合作等
        Integer type = getIntParameter("type", 0);
        //关系状态 1--可用 2--不可用
        Integer status = getIntParameter("status", 0);
        supplychainShopProxyRelation.setId(id);
        supplychainShopProxyRelation.setHostWarehouseId(hostWarehouseId);
        supplychainShopProxyRelation.setGuestWarehouseId(guestWarehouseId);
        supplychainShopProxyRelation.setType(type);
        supplychainShopProxyRelation.setStatus(status);
        dataAccessManager.changeSupplychainShopProxyRelation(supplychainShopProxyRelation);
        return success();
    }

}
