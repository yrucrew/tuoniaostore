package com.tuoniaostore.supplychain.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainBannerService {

    /**
     * 添加
     * @return
     */
    JSONObject addSupplychainBanner();

    JSONObject getSupplychainBanner();

    JSONObject getSupplychainBanners();

    JSONObject getSupplychainBannerCount();
    JSONObject getSupplychainBannerAll();

    /**
     * 修改
     */
    JSONObject changeSupplychainBanner();

    /**
     * 商家端采购：获取首页的banner
     * @author oy
     * @date 2019/4/12
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getSupplychainBannerFirst();


    /**
     * 商家端采购：首页获取仓库地址
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getSupplychainShopFirst();

    /**
     * 根据id批量删除
     */
    JSONObject batchDeleteSupplychainBanner();
}
