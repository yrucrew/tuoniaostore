package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainUserDailyBuyLogger;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainUserDailyBuyLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainUserDailyBuyLoggerService")
public class SupplychainUserDailyBuyLoggerServiceImpl extends BasicWebService implements SupplychainUserDailyBuyLoggerService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainUserDailyBuyLogger() {
        SupplychainUserDailyBuyLogger supplychainUserDailyBuyLogger = new SupplychainUserDailyBuyLogger();
        //
        String id = getUTF("id", null);
        //通行证ID
        String userId = getUTF("userId", null);
        //商品ID
        String itemId = getUTF("itemId", null);
        //对应的商品模版ID
        String itemTemplateId = getUTF("itemTemplateId", null);
        //已购买数量
        Integer hasBuyCount = getIntParameter("hasBuyCount", 0);
        //发生日期
      //  Date happenDate = getUTF("happenDate", null);
        supplychainUserDailyBuyLogger.setId(id);
        supplychainUserDailyBuyLogger.setUserId(userId);
        supplychainUserDailyBuyLogger.setItemId(itemId);
        supplychainUserDailyBuyLogger.setItemTemplateId(itemTemplateId);
        supplychainUserDailyBuyLogger.setHasBuyCount(hasBuyCount);
       // supplychainUserDailyBuyLogger.setHappenDate(happenDate);
        dataAccessManager.addSupplychainUserDailyBuyLogger(supplychainUserDailyBuyLogger);
        return success();
    }

    @Override
    public JSONObject getSupplychainUserDailyBuyLogger() {
        SupplychainUserDailyBuyLogger supplychainUserDailyBuyLogger = dataAccessManager.getSupplychainUserDailyBuyLogger(getId());
        return success(supplychainUserDailyBuyLogger);
    }

    @Override
    public JSONObject getSupplychainUserDailyBuyLoggers() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainUserDailyBuyLogger> supplychainUserDailyBuyLoggers = dataAccessManager.getSupplychainUserDailyBuyLoggers(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainUserDailyBuyLoggerCount(status, name);
        return success(supplychainUserDailyBuyLoggers, count);
    }

    @Override
    public JSONObject getSupplychainUserDailyBuyLoggerAll() {
        List<SupplychainUserDailyBuyLogger> supplychainUserDailyBuyLoggers = dataAccessManager.getSupplychainUserDailyBuyLoggerAll();
        return success(supplychainUserDailyBuyLoggers);
    }

    @Override
    public JSONObject getSupplychainUserDailyBuyLoggerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainUserDailyBuyLoggerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainUserDailyBuyLogger() {
        SupplychainUserDailyBuyLogger supplychainUserDailyBuyLogger = dataAccessManager.getSupplychainUserDailyBuyLogger(getId());
        //
        String id = getUTF("id", null);
        //通行证ID
        String userId = getUTF("userId", null);
        //商品ID
        String itemId = getUTF("itemId", null);
        //对应的商品模版ID
        String itemTemplateId = getUTF("itemTemplateId", null);
        //已购买数量
        Integer hasBuyCount = getIntParameter("hasBuyCount", 0);
        //发生日期
       // Date happenDate = getUTF("happenDate", null);
        supplychainUserDailyBuyLogger.setId(id);
        supplychainUserDailyBuyLogger.setUserId(userId);
        supplychainUserDailyBuyLogger.setItemId(itemId);
        supplychainUserDailyBuyLogger.setItemTemplateId(itemTemplateId);
        supplychainUserDailyBuyLogger.setHasBuyCount(hasBuyCount);
       // supplychainUserDailyBuyLogger.setHappenDate(happenDate);
        dataAccessManager.changeSupplychainUserDailyBuyLogger(supplychainUserDailyBuyLogger);
        return success();
    }

}
