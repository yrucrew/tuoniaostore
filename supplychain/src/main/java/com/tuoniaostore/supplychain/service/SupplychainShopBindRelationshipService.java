package com.tuoniaostore.supplychain.service;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiOperation;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:24
 */
public interface SupplychainShopBindRelationshipService {

    JSONObject addSupplychainShopBindRelationship();

    JSONObject getSupplychainShopBindRelationship();

    JSONObject getSupplychainShopBindRelationships();

    JSONObject getSupplychainShopBindRelationshipCount();

    JSONObject getSupplychainShopBindRelationshipAll();

    JSONObject changeSupplychainShopBindRelationship();

    JSONObject getPurchaseBindShops();

    JSONObject purchaseBindShop();

    JSONObject getSupplychainShopPropertiesList();

    JSONObject getShopBindRelationships();

    JSONObject getSysUsersListByPos();

    JSONObject addSysUserByPos();

    JSONObject editSysUserByPos();

    JSONObject forbidSysUserByPos();

    JSONObject delSysUserByPos();
}
