package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainTimeTaskLock;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainTimeTaskLockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainTimeTaskLockService")
public class SupplychainTimeTaskLockServiceImpl extends BasicWebService implements SupplychainTimeTaskLockService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainTimeTaskLock() {
        SupplychainTimeTaskLock supplychainTimeTaskLock = new SupplychainTimeTaskLock();
        //
        String id = getUTF("id", null);
        //标题
        String title = getUTF("title", null);
        //类型
        Integer type = getIntParameter("type", 0);
        //发生的时间 由逻辑决定
        String happenTime = getUTF("happenTime", null);
        //获得者地址
        String winnerAddress = getUTF("winnerAddress", null);
        supplychainTimeTaskLock.setId(id);
        supplychainTimeTaskLock.setTitle(title);
        supplychainTimeTaskLock.setType(type);
        supplychainTimeTaskLock.setHappenTime(happenTime);
        supplychainTimeTaskLock.setWinnerAddress(winnerAddress);
        dataAccessManager.addSupplychainTimeTaskLock(supplychainTimeTaskLock);
        return success();
    }

    @Override
    public JSONObject getSupplychainTimeTaskLock() {
        SupplychainTimeTaskLock supplychainTimeTaskLock = dataAccessManager.getSupplychainTimeTaskLock(getId());
        return success(supplychainTimeTaskLock);
    }

    @Override
    public JSONObject getSupplychainTimeTaskLocks() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainTimeTaskLock> supplychainTimeTaskLocks = dataAccessManager.getSupplychainTimeTaskLocks(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainTimeTaskLockCount(status, name);
        return success(supplychainTimeTaskLocks, count);
    }

    @Override
    public JSONObject getSupplychainTimeTaskLockAll() {
        List<SupplychainTimeTaskLock> supplychainTimeTaskLocks = dataAccessManager.getSupplychainTimeTaskLockAll();
        return success(supplychainTimeTaskLocks);
    }

    @Override
    public JSONObject getSupplychainTimeTaskLockCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainTimeTaskLockCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainTimeTaskLock() {
        SupplychainTimeTaskLock supplychainTimeTaskLock = dataAccessManager.getSupplychainTimeTaskLock(getId());
        //
        String id = getUTF("id", null);
        //标题
        String title = getUTF("title", null);
        //类型
        Integer type = getIntParameter("type", 0);
        //发生的时间 由逻辑决定
        String happenTime = getUTF("happenTime", null);
        //获得者地址
        String winnerAddress = getUTF("winnerAddress", null);
        supplychainTimeTaskLock.setId(id);
        supplychainTimeTaskLock.setTitle(title);
        supplychainTimeTaskLock.setType(type);
        supplychainTimeTaskLock.setHappenTime(happenTime);
        supplychainTimeTaskLock.setWinnerAddress(winnerAddress);
        dataAccessManager.changeSupplychainTimeTaskLock(supplychainTimeTaskLock);
        return success();
    }

}
