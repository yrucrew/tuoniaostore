package com.tuoniaostore.supplychain.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.CommonUtils;
import com.tuoniaostore.commons.exception.Nearby123shopRuntimeException;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.good.GoodTemplateRemoteService;
import com.tuoniaostore.datamodel.good.GoodTemplate;
import com.tuoniaostore.datamodel.supplychain.SupplychainPurchaseBindGood;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.supplychain.cache.SupplychainPurchaseBindGoodCache;
import com.tuoniaostore.supplychain.data.SupplychainPurchaseBindGoodMapper;
import com.tuoniaostore.supplychain.service.SupplychainPurchaseBindGoodService;
import com.tuoniaostore.supplychain.vo.PurchaseBindGoodVo;
import org.springframework.util.CollectionUtils;

@Service
public class SupplychainPurchaseBindGoodServiceImpl extends BasicWebService implements SupplychainPurchaseBindGoodService {

    @Autowired
    SupplychainPurchaseBindGoodCache purchaseBindGoodCache;

    @Autowired
    SupplychainPurchaseBindGoodMapper purchaseBindGoodMapper;

    @Override
    public JSONObject addPurchaseBindGood() {

        String purchaseUserId = getUTF("purchaseUserId");
        String goodTemplateId = getUTF("goodTemplateIds"); // 多个一起加

        SysUser sysUser;
        try {
            sysUser = SysUserRemoteService.getSysUser(purchaseUserId, getHttpServletRequest());
            if (sysUser == null)
                return fail("采购员账号不存在");
        } catch (Exception e1) {
            e1.printStackTrace();
        }

//		int count = purchaseBindGoodCache.getPurchaseBindGoodCount(purchaseUserId);
//		if (count > 0) {
//			return fail("错误：采购员存在绑定记录");
//		}
        // 删除原有记录
        purchaseBindGoodCache.deletePurchaseBindGoodByUserId(purchaseUserId);

        List<String> goodTemplateIds = JSONArray.parseArray(goodTemplateId, String.class);
        if (CollectionUtils.isEmpty(goodTemplateIds)) {
            return success("没有选择绑定的商品");
        }

        List<SupplychainPurchaseBindGood> purchaseBindGoods = new ArrayList<>();
        for (String gtId : goodTemplateIds) {
            try {
                GoodTemplate goodTemplate = GoodTemplateRemoteService.getGoodTemplate(gtId, getHttpServletRequest());
                if (goodTemplate == null)
                    throw new Nearby123shopRuntimeException("商品模板不存在");
            } catch (Exception e) {
                throw new Nearby123shopRuntimeException("远程服务器连接失败");
            }
            SupplychainPurchaseBindGood purchaseBindGood = new SupplychainPurchaseBindGood();
            purchaseBindGood.setUserId(purchaseUserId);
            purchaseBindGood.setGoodTemplateId(gtId);
            purchaseBindGoods.add(purchaseBindGood);
        }
        purchaseBindGoodCache.addPurchaseBindGoods(purchaseBindGoods);
        return success();
    }

    @Override
    public JSONObject updatePurchaseBindGood() {
        String purchaseUserId = getUTF("purchaseUserId");
        String goodTemplateId = getUTF("goodTemplateId");

        if (!StringUtils.isEmpty(goodTemplateId)) {
            List<SupplychainPurchaseBindGood> purchaseBindGoods = new ArrayList<>();

            String[] gtIds = goodTemplateId.split(",");
            Set<String> gtIdSet = new HashSet<>();
            for (String gtId : gtIds) {
                // 去重
                boolean add = gtIdSet.add(gtId);
                if (!add) {
                    continue;
                }

                try {
                    GoodTemplate goodTemplate = GoodTemplateRemoteService.getGoodTemplate(gtId, getHttpServletRequest());
                    if (goodTemplate == null)
                        throw new Nearby123shopRuntimeException("商品模板不存在");
                } catch (Exception e) {
                    throw new Nearby123shopRuntimeException("远程服务器连接失败");
                }
                SupplychainPurchaseBindGood purchaseBindGood = new SupplychainPurchaseBindGood();
                purchaseBindGood.setUserId(purchaseUserId);
                purchaseBindGood.setGoodTemplateId(gtId);
                purchaseBindGoods.add(purchaseBindGood);
            }
            purchaseBindGoodCache.deletePurchaseBindGoodByUserId(purchaseUserId);
            purchaseBindGoodCache.addPurchaseBindGoods(purchaseBindGoods);
        }
//		
//		try {
//			GoodTemplate goodTemplate = GoodTemplateRemoteService.getGoodTemplate(goodTemplateId, getHttpServletRequest());
//			if (goodTemplate == null)
//				return fail("商品模板不存在");
//		} catch (Exception e) {
//			throw new Nearby123shopRuntimeException("远程服务器连接失败");
//		}
//		int result = purchaseBindGoodCache.updatePurchaseBindGood(purchaseBindGoodId, goodTemplateId);
        return success();
    }

    @Override
    public JSONObject deletePurchaseBindGood() {
        String purchaseUserIds = getUTF("purchaseUserIds");
        String[] userIds = purchaseUserIds.split(",");
        purchaseBindGoodCache.deletePurchaseBindGoodByUserId(userIds);
        return success();
    }

    @Override
    public JSONObject purchaseBindGoodList() {
        PurchaseBindGoodVo param = getParseParamValue(PurchaseBindGoodVo.class);
        int pageSize = getPageSize();
        int pageStartIndex = getPageStartIndex(pageSize);

        List<PurchaseBindGoodVo> vos = purchaseBindGoodCache.getPurchaseBindGoodList(param, pageStartIndex, pageSize);

        if (!CommonUtils.isEmpty(vos)) {
            List<String> userIds = vos.stream().map(PurchaseBindGoodVo::getPurchaseUserId).collect(Collectors.toList());
            List<PurchaseBindGoodVo> goodNames = purchaseBindGoodCache.getPurchaseBindGoodNameList(userIds);

            Map<String, List<PurchaseBindGoodVo>> nameMap = goodNames.stream().collect(Collectors.groupingBy(PurchaseBindGoodVo::getPurchaseUserId));

            vos.forEach(vo -> {
                if (nameMap.containsKey(vo.getPurchaseUserId())) {
                    StringBuilder sb = new StringBuilder();
                    nameMap.get(vo.getPurchaseUserId()).forEach(name -> {
                        sb.append(name.getGoodTemplateName()).append(";");
                    });
                    sb.deleteCharAt(sb.length() - 1);
                    vo.setGoodTemplateName(sb.toString());
                }
            });

        }

        int total = purchaseBindGoodCache.getPurchaseBindGoodListCount(param);
        return success(vos, total);
    }

    @Override
    public JSONObject getGoodTemplates() {
        String purchaseUserId = getUTF("purchaseUserId", null);

        Map<String, Object> map = GoodTemplateRemoteService.getGoodTemplates(getHttpServletRequest());
        List<GoodPriceCombinationVO> vos = (List<GoodPriceCombinationVO>) map.get("rows");
        Integer total = (Integer) map.get("total");

        if (CommonUtils.isEmpty(vos)) {
            return fail("无商品模板数据");
        }

        List<SupplychainPurchaseBindGood> binds = null;
        if (!StringUtils.isEmpty(purchaseUserId)) {
            binds = purchaseBindGoodCache.getPurchaseBindGoodsByUserId(purchaseUserId);
        }

        Map<String, SupplychainPurchaseBindGood> bindMap = new HashMap<>();
        if (!CommonUtils.isEmpty(binds)) {
            bindMap.putAll(binds.stream().collect(Collectors.toMap(SupplychainPurchaseBindGood::getGoodTemplateId, Function.identity())));
        }

        JSONArray arr = vos.stream().map(vo -> {
            JSONObject data = new JSONObject();
            data.put("id", vo.getGoodTemplateId());
            data.put("name", vo.getGoodName());
            data.put("typeName", vo.getTypeNameTwoName());
            data.put("status", vo.getGoodStatus());
            data.put("checkFlag", bindMap.containsKey(vo.getGoodTemplateId()));

            return data;
        }).collect(Collectors.toCollection(JSONArray::new));

        return success(arr, total);
    }

    @Override
    public JSONObject checkPurchaseBindBood() {
        String purchaseUserId = getUTF("purchaseUserId");
        List<SupplychainPurchaseBindGood> supplychainPurchaseBindGoods = purchaseBindGoodCache.getPurchaseBindGoodsByUserId(purchaseUserId);
        return CollectionUtils.isEmpty(supplychainPurchaseBindGoods) ? success(false) : success(true);
    }

}
