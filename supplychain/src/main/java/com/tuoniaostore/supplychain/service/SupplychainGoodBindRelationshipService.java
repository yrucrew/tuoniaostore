package com.tuoniaostore.supplychain.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainGoodBindRelationshipService {

    JSONObject addSupplychainGoodBindRelationship();

    JSONObject getSupplychainGoodBindRelationship();

    JSONObject getSupplychainGoodBindRelationships();

    JSONObject getSupplychainGoodBindRelationshipCount();
    JSONObject getSupplychainGoodBindRelationshipAll();

    JSONObject changeSupplychainGoodBindRelationship();

    /**
     * 获取所有的商品类型 根据用户id
     * @return
     */
    JSONObject getSupplychainGoodBindRelationshipByUserId();

    /**
     * 获取商家端所有的商品类型
     * @author oy
     * @date 2019/3/31
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getSupplychainGoodBindRelationshipByUserId2();

    /**
     * 商家端：添加绑定的用户的类型
     * @author oy
     * @date 2019/4/2
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject addSupplychainGoodBindRelationshipByUser();

    /**
     * 商家端：删除绑定的用户的类型
     * @author oy
     * @date 2019/4/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject delSupplychainGoodBindRelationshipByUser();


    JSONObject getSupplychainGoodBindRelationshipByUserIdForRemote();

    /**
     * 为商店绑定商品分类
     * @author oy
     * @date 2019/5/6
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject bindSupplychainGoodBindRelationships();

    /**
     * 获取商店绑定的分类
     * @author oy
     * @date 2019/5/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getbindSupplychainGoodBindRelationships();

    /**
     * 获取商店绑定的分类详情
     * @author oy
     * @date 2019/5/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getbindSupplychainGoodBindRelationshipsDetails();

    /**
     * 修改商店绑定的分类
     * @author oy
     * @date 2019/5/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject updatebindSupplychainGoodBindRelationships();

    /**
     * 商品绑定分类 筛选已选fenlei
     * @author oy
     * @date 2019/5/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getTypeBySupplychainGoodBindType();
}
