package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainRole;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainRoleService")
public class SupplychainRoleServiceImpl extends BasicWebService implements SupplychainRoleService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainRole() {
        SupplychainRole supplychainRole = new SupplychainRole();
        //
        String id = getUTF("id", null);
        //对应的用户ID
        String userId = getUTF("userId", null);
        //角色名
        String roleName = getUTF("roleName", null);
        //显示的标题 用于交互时
        String title = getUTF("title", null);
        //角色类型
        Integer type = getIntParameter("type", 0);
        //当前状态
        Integer status = getIntParameter("status", 0);
        //联系电话
        String contactPhone = getUTF("contactPhone", null);
        supplychainRole.setId(id);
        supplychainRole.setUserId(userId);
        supplychainRole.setRoleName(roleName);
        supplychainRole.setTitle(title);
        supplychainRole.setType(type);
        supplychainRole.setStatus(status);
        supplychainRole.setContactPhone(contactPhone);
        dataAccessManager.addSupplychainRole(supplychainRole);
        return success();
    }

    @Override
    public JSONObject getSupplychainRole() {
        SupplychainRole supplychainRole = dataAccessManager.getSupplychainRole(getId());
        return success(supplychainRole);
    }

    @Override
    public JSONObject getSupplychainRoles() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainRole> supplychainRoles = dataAccessManager.getSupplychainRoles(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainRoleCount(status, name);
        return success(supplychainRoles, count);
    }

    @Override
    public JSONObject getSupplychainRoleAll() {
        List<SupplychainRole> supplychainRoles = dataAccessManager.getSupplychainRoleAll();
        return success(supplychainRoles);
    }

    @Override
    public JSONObject getSupplychainRoleCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainRoleCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainRole() {
        SupplychainRole supplychainRole = dataAccessManager.getSupplychainRole(getId());
        //
        String id = getUTF("id", null);
        //对应的用户ID
        String userId = getUTF("userId", null);
        //角色名
        String roleName = getUTF("roleName", null);
        //显示的标题 用于交互时
        String title = getUTF("title", null);
        //角色类型
        Integer type = getIntParameter("type", 0);
        //当前状态
        Integer status = getIntParameter("status", 0);
        //联系电话
        String contactPhone = getUTF("contactPhone", null);
        supplychainRole.setId(id);
        supplychainRole.setUserId(userId);
        supplychainRole.setRoleName(roleName);
        supplychainRole.setTitle(title);
        supplychainRole.setType(type);
        supplychainRole.setStatus(status);
        supplychainRole.setContactPhone(contactPhone);
        dataAccessManager.changeSupplychainRole(supplychainRole);
        return success();
    }

}
