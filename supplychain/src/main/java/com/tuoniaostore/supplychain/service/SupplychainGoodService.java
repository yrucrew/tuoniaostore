package com.tuoniaostore.supplychain.service;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainGoodService {

    JSONObject addSupplychainGood();

    JSONObject getSupplychainGood();

    JSONObject getSupplychainGoods();

    JSONObject getSupplychainGoodCount();
    JSONObject getSupplychainGoodAll();

    JSONObject changeSupplychainGood();

    /**
     * 供应商：通过商品类型id 查找具体商品
     * @return
     */
    JSONObject getSupplychainGoodByGoodTypeId();

    /**
     * 商家端：通过商品类型id 查找具体商品
     * @author oy
     * @date 2019/3/31
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getSupplychainGoodByGoodTypeId2();

    /**
     * 编辑商品
     * @return
     */
    JSONObject changeSupplychainGoodByGoodTypeId();

    /**
     * 下架商品
     * @return
     */
    JSONObject downSupplychainGoodByGoodId();

    /**
     * 删除商品
     * @return
     */
    JSONObject deleteSupplychainGoodByGoodId();
    /**
     * 上架商品
     * @author oy
     * @date 2019/3/31
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject upSupplychainGoodByGoodId();

    /**
     * 根据商品ID查询商品
     * @author sqd
     * @date 2019/4/8
     * @param
     * @return
     */
    JSONObject getSupplychainGoodById();
    /**
     * 商家：编辑商品
     * @author oy
     * @date 2019/4/2
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject changeSupplychainGoodByGoodTypeId2();

    /**
     * 商家：添加商品
     * @author oy
     * @date 2019/4/2
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject addSupplychainGoodByUser();

    /**
     * 商家：编辑页面数据获取
     * @author oy
     * @date 2019/4/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getSupplychainGoodByGoodId();


    /**
     * 商家：删除商品信息
     * @author oy
     * @date 2019/4/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject delSupplychainGoodByGoodId();

    /**
     * 商家：搜索商品信息(模糊查询)
     * @author oy
     * @date 2019/4/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject searchSupplychainGoodByGoodId();

    /**
     * 查询当前用户购物车商品
     * @author sqd
     * @date 2019/4/12
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getSupplychainGoodByIdAndShopNumber();


    /**
     * 根据商品ids查询商品
     * @return
     */
    JSONObject getSupplychainGoodByIds();
    JSONObject changSupplychainGoodByStock();


    JSONObject getSupplychainGoodByShopId();

    JSONObject addSupplychainGoodWithShopRelation();

    /**
     * 查询下单商品
     * @author sqd
     * @date 2019/4/19
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
     JSONObject getSupplychainGoodByIdOrder();


    JSONObject searchSupplychainGoodBySHOrNameOrBarcode();

    JSONObject getSupplychainGoodForOrder();

    JSONObject updateStockByList();

    /**
     * 修改商品库存
     * @author sqd
     * @date 2019/5/6
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject changSupplychainGoodByGoodId();

    JSONObject getSupplychainGoodByShopId2();

    JSONObject addSupplychainGoodWithShopRelation2();

    JSONObject batchDeleteSupplychainGoodByGoodId();

    /**
     * 修改绑定商品的一些信息
     * @author oy
     * @date 2019/5/9
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject changSupplychainGoodDetailsByGoodId();


    JSONObject batchDownSupplychainGoodByGoodId();

    JSONObject batchUpSupplychainGoodByGoodId();
    JSONObject getSupplychainGoodPosList();

    JSONObject getSupplychainGoodByShopIdAndPriceId();

    JSONObject getSupplychainGoodByShopIdAndBarcode();

    JSONObject updateSupplychainGoodStatus();

    JSONObject getSupplychainGoodByIdAndStatus();

    JSONObject getSupplychainGoodForBindSupplierWithStore();
}
