package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainLevelTemplate;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainLevelTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainLevelTemplateService")
public class SupplychainLevelTemplateServiceImpl extends BasicWebService implements SupplychainLevelTemplateService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainLevelTemplate() {
        SupplychainLevelTemplate supplychainLevelTemplate = new SupplychainLevelTemplate();
        //
        String id = getUTF("id", null);
        //等级值
        Integer levelValue = getIntParameter("levelValue", 0);
        //其他内容
        String defineParameter = getUTF("defineParameter", null);
        //是否启用 0--无效 1--启动
        Integer status = getIntParameter("status", 0);
        supplychainLevelTemplate.setId(id);
        supplychainLevelTemplate.setLevelValue(levelValue);
        supplychainLevelTemplate.setDefineParameter(defineParameter);
        supplychainLevelTemplate.setStatus(status);
        dataAccessManager.addSupplychainLevelTemplate(supplychainLevelTemplate);
        return success();
    }

    @Override
    public JSONObject getSupplychainLevelTemplate() {
        SupplychainLevelTemplate supplychainLevelTemplate = dataAccessManager.getSupplychainLevelTemplate(getId());
        return success(supplychainLevelTemplate);
    }

    @Override
    public JSONObject getSupplychainLevelTemplates() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainLevelTemplate> supplychainLevelTemplates = dataAccessManager.getSupplychainLevelTemplates(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainLevelTemplateCount(status, name);
        return success(supplychainLevelTemplates, count);
    }

    @Override
    public JSONObject getSupplychainLevelTemplateAll() {
        List<SupplychainLevelTemplate> supplychainLevelTemplates = dataAccessManager.getSupplychainLevelTemplateAll();
        return success(supplychainLevelTemplates);
    }

    @Override
    public JSONObject getSupplychainLevelTemplateCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainLevelTemplateCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainLevelTemplate() {
        SupplychainLevelTemplate supplychainLevelTemplate = dataAccessManager.getSupplychainLevelTemplate(getId());
        //
        String id = getUTF("id", null);
        //等级值
        Integer levelValue = getIntParameter("levelValue", 0);
        //其他内容
        String defineParameter = getUTF("defineParameter", null);
        //是否启用 0--无效 1--启动
        Integer status = getIntParameter("status", 0);
        supplychainLevelTemplate.setId(id);
        supplychainLevelTemplate.setLevelValue(levelValue);
        supplychainLevelTemplate.setDefineParameter(defineParameter);
        supplychainLevelTemplate.setStatus(status);
        dataAccessManager.changeSupplychainLevelTemplate(supplychainLevelTemplate);
        return success();
    }

}
