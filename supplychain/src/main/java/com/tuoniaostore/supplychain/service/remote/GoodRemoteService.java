package com.tuoniaostore.supplychain.service.remote;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.http.HttpRequest;
import com.tuoniaostore.datamodel.good.GoodBrand;
import com.tuoniaostore.datamodel.good.GoodTemplate;
import com.tuoniaostore.datamodel.good.GoodType;

public class GoodRemoteService {
	
	private static String URL = CommonsUrlConfig.goodRemoteURL;
	
	private static <T> T singleResult(String data, Class<T> clazz) {
		String response = JSONObject.parseObject(data).getString("response");
		return JSONObject.parseObject(response, clazz);
	}
	
	/** 查询商品模板 */
	public static GoodTemplate getGoodTemplate(String id) {
		String data = HttpRequest.get(URL + "goodTemplate/getGoodTemplate?id=" + id);
		return singleResult(data, GoodTemplate.class);
	}
	
	/** 查询商品分类 */
	public static GoodType getGoodType(String id) {
		String data = HttpRequest.get(URL + "goodType/getGoodType?id=" + id);
		return singleResult(data, GoodType.class);
	}
	
	/** 查询商品品牌 */
	public static GoodBrand getGoodBrand(String id) {
		String data = HttpRequest.get(URL + "goodBrand/getGoodBrand?id=" + id);
		return singleResult(data, GoodBrand.class);
	}
	
	
}
