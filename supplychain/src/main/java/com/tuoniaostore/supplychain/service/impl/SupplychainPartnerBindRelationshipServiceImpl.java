package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainPartnerBindRelationship;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainPartnerBindRelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainPartnerBindRelationshipService")
public class SupplychainPartnerBindRelationshipServiceImpl extends BasicWebService implements SupplychainPartnerBindRelationshipService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainPartnerBindRelationship() {
        SupplychainPartnerBindRelationship supplychainPartnerBindRelationship = new SupplychainPartnerBindRelationship();
        //
        String id = getUTF("id", null);
        //合作方ID
        String partnerId = getUTF("partnerId", null);
        //绑定我方仓库ID
        String bindShopId = getUTF("bindShopId", null);
        //对应合作方仓库标识
        String partnerShopId = getUTF("partnerShopId", null);
        //关系状态 1、独代
        Integer status = getIntParameter("status", 0);
        supplychainPartnerBindRelationship.setId(id);
        supplychainPartnerBindRelationship.setPartnerId(partnerId);
        supplychainPartnerBindRelationship.setBindShopId(bindShopId);
        supplychainPartnerBindRelationship.setPartnerShopId(partnerShopId);
        supplychainPartnerBindRelationship.setStatus(status);
        dataAccessManager.addSupplychainPartnerBindRelationship(supplychainPartnerBindRelationship);
        return success();
    }

    @Override
    public JSONObject getSupplychainPartnerBindRelationship() {
        SupplychainPartnerBindRelationship supplychainPartnerBindRelationship = dataAccessManager.getSupplychainPartnerBindRelationship(getId());
        return success(supplychainPartnerBindRelationship);
    }

    @Override
    public JSONObject getSupplychainPartnerBindRelationships() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainPartnerBindRelationship> supplychainPartnerBindRelationships = dataAccessManager.getSupplychainPartnerBindRelationships(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainPartnerBindRelationshipCount(status, name);
        return success(supplychainPartnerBindRelationships, count);
    }

    @Override
    public JSONObject getSupplychainPartnerBindRelationshipAll() {
        List<SupplychainPartnerBindRelationship> supplychainPartnerBindRelationships = dataAccessManager.getSupplychainPartnerBindRelationshipAll();
        return success(supplychainPartnerBindRelationships);
    }

    @Override
    public JSONObject getSupplychainPartnerBindRelationshipCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainPartnerBindRelationshipCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainPartnerBindRelationship() {
        SupplychainPartnerBindRelationship supplychainPartnerBindRelationship = dataAccessManager.getSupplychainPartnerBindRelationship(getId());
        //
        String id = getUTF("id", null);
        //合作方ID
        String partnerId = getUTF("partnerId", null);
        //绑定我方仓库ID
        String bindShopId = getUTF("bindShopId", null);
        //对应合作方仓库标识
        String partnerShopId = getUTF("partnerShopId", null);
        //关系状态 1、独代
        Integer status = getIntParameter("status", 0);
        supplychainPartnerBindRelationship.setId(id);
        supplychainPartnerBindRelationship.setPartnerId(partnerId);
        supplychainPartnerBindRelationship.setBindShopId(bindShopId);
        supplychainPartnerBindRelationship.setPartnerShopId(partnerShopId);
        supplychainPartnerBindRelationship.setStatus(status);
        dataAccessManager.changeSupplychainPartnerBindRelationship(supplychainPartnerBindRelationship);
        return success();
    }

}
