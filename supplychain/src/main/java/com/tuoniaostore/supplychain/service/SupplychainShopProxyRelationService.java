package com.tuoniaostore.supplychain.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainShopProxyRelationService {

    JSONObject addSupplychainShopProxyRelation();

    JSONObject getSupplychainShopProxyRelation();

    JSONObject getSupplychainShopProxyRelations();

    JSONObject getSupplychainShopProxyRelationCount();
    JSONObject getSupplychainShopProxyRelationAll();

    JSONObject changeSupplychainShopProxyRelation();
}
