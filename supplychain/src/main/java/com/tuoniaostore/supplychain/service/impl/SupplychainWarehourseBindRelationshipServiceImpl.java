package com.tuoniaostore.supplychain.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseBindRelationship;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainWarehourseBindRelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author oy
 * @description
 * @date 2019/5/28
 */
@Service("SupplychainWarehourseBindRelationshipService")
public class SupplychainWarehourseBindRelationshipServiceImpl extends BasicWebService implements SupplychainWarehourseBindRelationshipService {


    @Autowired
    private SupplychainDataAccessManager dataAccessManager;


    /**
     * 获取所有的绑定关系
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/28
     */
    @Override
    public JSONObject getSupplychainWarehourseBindRelationships() {
        List<SupplychainWarehourseBindRelationship> list = dataAccessManager.getSupplychainWarehourseBindRelationships(getPageStartIndex(getPageSize()), getPageSize());
        return success(list);
    }

    /**
     * 获取绑定的列表展示
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/28
     */
    @Override
    public JSONObject getSupplychainWarehourseBindRelationshipsBySupplier() {
        int type = getIntParameter("type");//0 仓库绑定门店，1仓库绑定供应商
        //获取对应 仓库列表 0 1 2 3 4
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        //添加仓库搜索
        String shopName = getUTF("shopName", null);

        List<SupplychainShopProperties> supplychainShopPropertiesByType = dataAccessManager.getSupplychainShopPropertiesByShopName(shopName, list, getPageStartIndex(getPageSize()), getPageSize());
        int count = 0;
        if (supplychainShopPropertiesByType != null && supplychainShopPropertiesByType.size() > 0) {
            count = dataAccessManager.getSupplychainShopPropertiesByShopNameCount(shopName, list);
            supplychainShopPropertiesByType.forEach(x -> {
                String id = x.getId();
                List<SupplychainWarehourseBindRelationship> supplychainWarehourseBindRelationshipsByShopIds = dataAccessManager.getSupplychainWarehourseBindRelationshipsByShopIdsAndType(id, type, getPageStartIndex(getPageSize()), getPageSize());
                if (supplychainWarehourseBindRelationshipsByShopIds != null && supplychainWarehourseBindRelationshipsByShopIds.size() > 0) {
                    List<String> bindShopIds = supplychainWarehourseBindRelationshipsByShopIds.stream().map(SupplychainWarehourseBindRelationship::getBindShopId).distinct().collect(Collectors.toList());
                    List<SupplychainShopProperties> supplychainShopPropertiesByIds = dataAccessManager.getSupplychainShopPropertiesByIds(bindShopIds);
                    if (supplychainShopPropertiesByIds != null && supplychainShopPropertiesByIds.size() > 0) {
                        String collect = supplychainShopPropertiesByIds.stream().map(SupplychainShopProperties::getShopName).collect(Collectors.joining(","));
                        x.setBindShopName(collect);
                    }
                }
                String userId = x.getUserId();
                if (userId != null && !userId.equals("")) {
                    try {
                        SysUser user = SysUserRemoteService.getSysUser(userId, getHttpServletRequest());
                        if (user != null) {
                            if (user.getRealName() != null) {
                                x.setUserName(user.getRealName());
                            } else if (user.getDefaultName() != null) {
                                x.setUserName(user.getDefaultName());
                            } else {
                                x.setUserName(user.getPhoneNumber());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return success(supplychainShopPropertiesByType, count);
        }
        return success(supplychainShopPropertiesByType, count);
    }

    @Override
    public JSONObject listForBindingWareHourse() {
        String shopId = getUTF("shopId");
        String shopName = getUTF("shopName", null);
        String type = getUTF("type"); //6,7 门店 5,8,9供应商

        int bindType = 0;

        List<Integer> types = new ArrayList<>();
        if (type != null && !type.equals("")) {
            if (type.equals("5,8,9")) {//0 仓库绑定门店，1仓库绑定供应商
                bindType = 1;
            }

            String[] split = type.split(",");
            for (String s : split) {
                int i = Integer.parseInt(s);
                types.add(i);
            }
        }

        //根据仓库id 去查找对应的仓库
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(shopId);

        if (supplychainShopProperties != null) {
            //去查找当前商店绑定的门店信息
            List<SupplychainShopProperties> supplychainShopPropertiesByType = null;
            if (bindType == 1) {
                // 供应商
                supplychainShopPropertiesByType = dataAccessManager.getSupplychainShopPropertiesByShopName(shopName, types, getPageStartIndex(getPageSize()), getPageSize());
            } else {
                // 门店
                supplychainShopPropertiesByType = dataAccessManager.getOwnAndUnbindShopProperties(shopName, shopId, types, getPageStartIndex(getPageSize()), getPageSize());
            }

            int count = 0;

            if (!CollectionUtils.isEmpty(supplychainShopPropertiesByType)) {
                if (bindType == 1) {
                    count = dataAccessManager.getSupplychainShopPropertiesByShopNameCount(shopName, types);
                } else {
                    count = dataAccessManager.getOwnAndUnbindShopPropertiesCount(shopId, types);
                }
                // 已绑定关系
                List<SupplychainWarehourseBindRelationship> supplychainWarehourseBindRelationshipsByShopIdsAndType = dataAccessManager.getSupplychainWarehourseBindRelationshipsByShopIdsAndType(supplychainShopProperties.getId(), bindType, getPageStartIndex(getPageSize()), getPageSize());
                if (!CollectionUtils.isEmpty(supplychainWarehourseBindRelationshipsByShopIdsAndType)) {
                    List<String> bindShopIds = supplychainWarehourseBindRelationshipsByShopIdsAndType.stream()
                            .map(SupplychainWarehourseBindRelationship::getBindShopId)
                            .collect(Collectors.toList());
                    supplychainShopPropertiesByType.forEach(x -> {
                        String id = x.getId();
                        if (bindShopIds.contains(id)) {
                            x.setCheckFlag(true);
                        }
                    });
                    return success(supplychainShopPropertiesByType, count);
                } else {
                    return success(supplychainShopPropertiesByType, count);
                }
            } else {
                return success(supplychainShopPropertiesByType, count);
            }
        } else {
            return fail("无该商店信息！");
        }
    }

    @Override
    public JSONObject listForBindingWareHourseDetails() {
        String shopId = getUTF("shopId");
        String type = getUTF("type"); //6,7 门店 5,8,9供应商

        int bindType = 0;

        List<Integer> list = new ArrayList<>();
        if (type != null && !type.equals("")) {
            if (type.equals("5,8,9")) {//0 仓库绑定门店，1仓库绑定供应商
                bindType = 1;
            }

            String[] split = type.split(",");
            for (String s : split) {
                int i = Integer.parseInt(s);
                list.add(i);
            }
        }

        //根据仓库id 去查找对应的仓库
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(shopId);
        List<SupplychainShopProperties> supplychainShopPropertiesByIdsPage = null;
        int count = 0;
        if (supplychainShopProperties != null) {
            List<SupplychainWarehourseBindRelationship> supplychainWarehourseBindRelationshipsByShopIdsAndType = dataAccessManager.getSupplychainWarehourseBindRelationshipsByShopIdsAndType(supplychainShopProperties.getId(), bindType, getPageStartIndex(getPageSize()), getPageSize());
            if (supplychainWarehourseBindRelationshipsByShopIdsAndType != null && supplychainWarehourseBindRelationshipsByShopIdsAndType.size() > 0) {
                List<String> bindShopIds = supplychainWarehourseBindRelationshipsByShopIdsAndType.stream().map(SupplychainWarehourseBindRelationship::getBindShopId).collect(Collectors.toList());
                if (bindShopIds != null && bindShopIds.size() > 0) {
                    supplychainShopPropertiesByIdsPage = dataAccessManager.getSupplychainShopPropertiesByIdsPage(bindShopIds, getPageStartIndex(getPageSize()), getPageSize());
                    if (supplychainShopPropertiesByIdsPage != null && supplychainShopPropertiesByIdsPage.size() > 0) {
                        count = dataAccessManager.getSupplychainShopPropertiesByIdsCount(bindShopIds);
                    }
                    return success(supplychainShopPropertiesByIdsPage, count);
                } else {
                    return success(supplychainShopPropertiesByIdsPage, count);
                }
            }
        } else {
            return fail("无该商店信息！");
        }
        return success();
    }

    @Override
    public JSONObject getSupplychainWarehourseBindRelationshipsByShopId() {
        String shopid = getUTF("shopId");
        SupplychainWarehourseBindRelationship supplychainWarehourseBindRelationshipsByShopId = dataAccessManager.getSupplychainWarehourseBindRelationshipsByShopId(shopid, 1);
        return  success(supplychainWarehourseBindRelationshipsByShopId);
    }

    @Override
    @Transactional
    public JSONObject bindingWareHourseWithStore() {
        String shopId = getUTF("shopId");//仓库id
        String type = getUTF("type"); //6,7 门店 5,8,9供应商
        String bindingShopIds = getUTF("bindingShopIds"); //绑定的门店或者供应商id 集合

        List<String> stringList = new ArrayList<>();
        if (bindingShopIds != null && !bindingShopIds.equals("")) {
            stringList = JSONObject.parseArray(bindingShopIds, String.class);
        }

        int bindType = 0;

        List<Integer> list = new ArrayList<>();
        if (type != null && !type.equals("")) {
            if (type.equals("5,8,9")) {//0 仓库绑定门店，1仓库绑定供应商
                bindType = 1;
            }
            String[] split = type.split(",");
            for (String s : split) {
                list.add(Integer.parseInt(s));
            }
        }
        dataAccessManager.deleteSupplychainWarehourseBindRelationship(shopId, bindType);
        List<SupplychainWarehourseBindRelationship> batchAdd = new ArrayList<>();
        if (stringList != null && stringList.size() > 0) {
            for (String x : stringList) {
                SupplychainWarehourseBindRelationship add = new SupplychainWarehourseBindRelationship();
                add.setShopId(shopId);
                add.setType(bindType);
                add.setUserId(getUser().getId());
                add.setBindShopId(x);
                batchAdd.add(add);
            }
            dataAccessManager.batchAddSupplychainWarehourseBindRelationships(batchAdd);
        }
        return success("修改成功！");
    }


}
