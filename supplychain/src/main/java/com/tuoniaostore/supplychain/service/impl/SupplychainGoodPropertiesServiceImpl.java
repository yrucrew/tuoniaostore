package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodProperties;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainGoodPropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainGoodPropertiesService")
public class SupplychainGoodPropertiesServiceImpl extends BasicWebService implements SupplychainGoodPropertiesService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainGoodProperties() {
        SupplychainGoodProperties supplychainGoodProperties = new SupplychainGoodProperties();
        //
        String id = getUTF("id", null);
        //商品ID
        String goodId = getUTF("goodId", null);
        //属性类型
        Integer type = getIntParameter("type", 0);
        //属性标志
        String k = getUTF("k", null);
        //具体属性值
        String v = getUTF("v", null);
        supplychainGoodProperties.setId(id);
        supplychainGoodProperties.setGoodId(goodId);
        supplychainGoodProperties.setType(type);
        supplychainGoodProperties.setK(k);
        supplychainGoodProperties.setV(v);
        dataAccessManager.addSupplychainGoodProperties(supplychainGoodProperties);
        return success();
    }

    @Override
    public JSONObject getSupplychainGoodProperties() {
        SupplychainGoodProperties supplychainGoodProperties = dataAccessManager.getSupplychainGoodProperties(getId());
        return success(supplychainGoodProperties);
    }

    @Override
    public JSONObject getSupplychainGoodPropertiess() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainGoodProperties> supplychainGoodPropertiess = dataAccessManager.getSupplychainGoodPropertiess(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainGoodPropertiesCount(status, name);
        return success(supplychainGoodPropertiess, count);
    }

    @Override
    public JSONObject getSupplychainGoodPropertiesAll() {
        List<SupplychainGoodProperties> supplychainGoodPropertiess = dataAccessManager.getSupplychainGoodPropertiesAll();
        return success(supplychainGoodPropertiess);
    }

    @Override
    public JSONObject getSupplychainGoodPropertiesCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainGoodPropertiesCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainGoodProperties() {
        SupplychainGoodProperties supplychainGoodProperties = dataAccessManager.getSupplychainGoodProperties(getId());
        //
        String id = getUTF("id", null);
        //商品ID
        String goodId = getUTF("goodId", null);
        //属性类型
        Integer type = getIntParameter("type", 0);
        //属性标志
        String k = getUTF("k", null);
        //具体属性值
        String v = getUTF("v", null);
        supplychainGoodProperties.setId(id);
        supplychainGoodProperties.setGoodId(goodId);
        supplychainGoodProperties.setType(type);
        supplychainGoodProperties.setK(k);
        supplychainGoodProperties.setV(v);
        dataAccessManager.changeSupplychainGoodProperties(supplychainGoodProperties);
        return success();
    }

}
