package com.tuoniaostore.supplychain.service;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;

/**
 * 仓库的属性
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainShopPropertiesService {

    JSONObject addSupplychainShopProperties();

    JSONObject getSupplychainShopProperties();

    JSONObject getSupplychainShopPropertiess();

    JSONObject getSupplychainShopPropertiesCount();
    JSONObject getSupplychainShopPropertiesAll();

    JSONObject changeSupplychainShopProperties();

    JSONObject changeSupplychainShopProperties2();

    /**
     * 查找当前用户的商店 通用
     * @author oy
     * @date 2019/4/2
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getSupplychainShopPropertiesByUserIdCommon();

    /**
     * 查找当前用户的商店
     * @return
     */
    JSONObject getSupplychainShopPropertiesByUserId();

    /**
     * 修改当前的用户的商店名称
     * @return
     */
    JSONObject changeSupplychainShopPropertiesById();

    /**
     * 查询当前的用户的供应店信息
     * @return
     */
    JSONObject selectSupplychainShopPropertiesById();

    /**
     * 修改供应商的营业状态
     * @return
     */
    JSONObject changeSupplychainShopPropertiesStatus();

    /**
     * 获取供应商页面的界面信息
     * @return
     */
    JSONObject getSupplychainShopPropertiesVO();

    /**
     * 修改配送距离
     * @return
     */
    JSONObject changeSupplychainShopPropertiesCoveringDistance();

    /**
     * 修改打印机设备
     * @author oy
     * @date 2019/3/30
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject changeSupplychainShopPropertiesBindPrinterCode();

    /**
     * 修改电话号码
     * @author oy
     * @date 2019/3/30
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject changeSupplychainShopPropertiesPhoneNumber();
    /**
     * 查找附近的供应链（全部）
     * @author lyb
     * @date 2019/3/30
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject findRelationShopKey();

    /**
     * 查找附近的供应链（根据用户或者坐标查询开仓的仓库）
     * @author lyb
     * @date 2019/3/30
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject findRecentSelfShopForRemote();

    /**
     * 审核供应商
     * @return
     */
	JSONObject authSupplychainShopProperties();

    JSONObject getSupplychainShopPropertiess2();

    JSONObject deleteSupplychainShopProperties();

    JSONObject getSupplychainShopPropertiesForPosHome();

    JSONObject getSupplychainShopPropertiesForPosHomeGood();

    JSONObject getSupplychainShopForSupplier();

    JSONObject getSupplychainShopByLoginUser();

    JSONObject getExistSupplychainShopByUserId();

    JSONObject addSupplychainShopPropertiesForStore();

    JSONObject getSupplychainShopPropertiesByShopName();

    JSONObject getSupplychainShopPropertiesForBind();

    JSONObject bindSupplychainShopProperties();

    JSONObject getSupplychainShopPropertiesTotal();

    SupplychainShopProperties getSupplychainShopPropertiesByUid(String userId);
    /**
     * 获取当前用户的门店信息
     */
    JSONObject getSupplychainShopPropertiesByUid();

    /**
     * 绑定打印机
     * @return
     */
    JSONObject UpdateBindPrinterCodeByUserId();

}
