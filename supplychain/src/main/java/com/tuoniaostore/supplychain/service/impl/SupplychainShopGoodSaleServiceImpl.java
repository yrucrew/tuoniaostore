package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.support.order.OrderEntryPosRemoteService;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopGoodSale;
import com.tuoniaostore.datamodel.vo.order.GoodSaleMsgVO;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainShopGoodSaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainShopGoodSaleService")
public class SupplychainShopGoodSaleServiceImpl extends BasicWebService implements SupplychainShopGoodSaleService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainShopGoodSale() {
        SupplychainShopGoodSale supplychainShopGoodSale = new SupplychainShopGoodSale();
        //id
        String id = getUTF("id", null);
        //商品模板id
        String goodTempalteId = getUTF("goodTempalteId", null);
        //商品名称
        String goodName = getUTF("goodName", null);
        //条码
        String goodBarcode = getUTF("goodBarcode", null);
        //商品单位
        String goodUnit = getUTF("goodUnit", null);
        //商品单位id
        String goodUnitId = getUTF("goodUnitId", null);
        //商品类型id
        String goodTypeId = getUTF("goodTypeId", null);
        //商品类型
        String goodType = getUTF("goodType", null);
        //销售数量
        Long saleNum = getLongParameter("saleNum", 0);
        //销售金额
        Long saleMoney = getLongParameter("saleMoney", 0);

        supplychainShopGoodSale.setId(id);
        supplychainShopGoodSale.setGoodTempalteId(goodTempalteId);
        supplychainShopGoodSale.setGoodName(goodName);
        supplychainShopGoodSale.setGoodBarcode(goodBarcode);
        supplychainShopGoodSale.setGoodUnit(goodUnit);
        supplychainShopGoodSale.setGoodUnitId(goodUnitId);
        supplychainShopGoodSale.setGoodTypeId(goodTypeId);
        supplychainShopGoodSale.setGoodType(goodType);
//        supplychainShopGoodSale.setSaleNum(saleNum);
//        supplychainShopGoodSale.setSaleMoney(saleMoney);
        dataAccessManager.addSupplychainShopGoodSale(supplychainShopGoodSale);
        return success();
    }

    @Override
    public JSONObject getSupplychainShopGoodSale() {
        SupplychainShopGoodSale supplychainShopGoodSale = dataAccessManager.getSupplychainShopGoodSale(getId());
        return success(supplychainShopGoodSale);
    }

    @Override
    public JSONObject getSupplychainShopGoodSales() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainShopGoodSale> supplychainShopGoodSales = dataAccessManager.getSupplychainShopGoodSales(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainShopGoodSaleCount(status, name);
        return success(supplychainShopGoodSales, count);
    }

    @Override
    public JSONObject getSupplychainShopGoodSaleAll() {
        List<SupplychainShopGoodSale> supplychainShopGoodSales = dataAccessManager.getSupplychainShopGoodSaleAll();
        return success(supplychainShopGoodSales);
    }

    @Override
    public JSONObject getSupplychainShopGoodSaleCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainShopGoodSaleCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainShopGoodSale() {
        SupplychainShopGoodSale supplychainShopGoodSale = dataAccessManager.getSupplychainShopGoodSale(getId());
        //id
        String id = getUTF("id", null);
        //商品模板id
        String goodTempalteId = getUTF("goodTempalteId", null);
        //商品名称
        String goodName = getUTF("goodName", null);
        //条码
        String goodBarcode = getUTF("goodBarcode", null);
        //商品单位
        String goodUnit = getUTF("goodUnit", null);
        //商品单位id
        String goodUnitId = getUTF("goodUnitId", null);
        //商品类型id
        String goodTypeId = getUTF("goodTypeId", null);
        //商品类型
        String goodType = getUTF("goodType", null);
        //销售数量
        Long saleNum = getLongParameter("saleNum", 0);
        //销售金额
        Long saleMoney = getLongParameter("saleMoney", 0);

        supplychainShopGoodSale.setId(id);
        supplychainShopGoodSale.setGoodTempalteId(goodTempalteId);
        supplychainShopGoodSale.setGoodName(goodName);
        supplychainShopGoodSale.setGoodBarcode(goodBarcode);
        supplychainShopGoodSale.setGoodUnit(goodUnit);
        supplychainShopGoodSale.setGoodUnitId(goodUnitId);
        supplychainShopGoodSale.setGoodTypeId(goodTypeId);
        supplychainShopGoodSale.setGoodType(goodType);
//        supplychainShopGoodSale.setSaleNum(saleNum);
//        supplychainShopGoodSale.setSaleMoney(saleMoney);
        dataAccessManager.changeSupplychainShopGoodSale(supplychainShopGoodSale);
        return success();
    }

    /**
     * 销售报表首页（包括筛选）
     * @author oy
     * @date 2019/5/25
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listSupplychainShopGoodSale() {
        //门店id
        String shopId = getUTF("shopId");
        //时间起始
        String startTime = getUTF("startTime",null);
        //时间结束
        String endTime = getUTF("endTime",null);
        //分类
        String typeId = getUTF("typeId",null);
        //商品条码   +  商品名字
        String barcode = getUTF("barcode",null);

        if(startTime == null && endTime == null){
            //默认是最近的7天
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE,-7);
            Date time = calendar.getTime();
            startTime = DateUtils.format(time, DateUtils.FORMAT_SHORT); //7天前

            Calendar calendar1 = Calendar.getInstance();
            calendar1.add(Calendar.DATE,-1);
            Date time1 = calendar1.getTime();
            endTime = DateUtils.format(time1, DateUtils.FORMAT_SHORT);//昨天的时间
        }

        int count = 0;
        List<SupplychainShopGoodSale> list = dataAccessManager.getSupplychainShopGoodSaleByList(shopId,typeId,barcode,startTime,endTime,getPageStartIndex(getPageSize()),getPageSize());
        if(list != null && list.size() > 0){
            count = dataAccessManager.getSupplychainShopGoodSaleByListCount(shopId,typeId,barcode,startTime,endTime);
        }
        return success(list,count);
    }

}
