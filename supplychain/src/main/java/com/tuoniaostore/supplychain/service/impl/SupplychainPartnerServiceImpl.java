package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainPartner;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainPartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainPartnerService")
public class SupplychainPartnerServiceImpl extends BasicWebService implements SupplychainPartnerService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainPartner() {
        SupplychainPartner supplychainPartner = new SupplychainPartner();
        //
        String id = getUTF("id", null);
        //合作方标题 如网址
        String title = getUTF("title", null);
        //公司名字
        String companyName = getUTF("companyName", null);
        //合作类型
        Integer type = getIntParameter("type", 0);
        //代理属性类型
        Integer proxyType = getIntParameter("proxyType", 0);
        //合作状态 0-停止合作 1-合作中
        Integer status = getIntParameter("status", 0);
        supplychainPartner.setId(id);
        supplychainPartner.setTitle(title);
        supplychainPartner.setCompanyName(companyName);
        supplychainPartner.setType(type);
        supplychainPartner.setProxyType(proxyType);
        supplychainPartner.setStatus(status);
        dataAccessManager.addSupplychainPartner(supplychainPartner);
        return success();
    }

    @Override
    public JSONObject getSupplychainPartner() {
        SupplychainPartner supplychainPartner = dataAccessManager.getSupplychainPartner(getId());
        return success(supplychainPartner);
    }

    @Override
    public JSONObject getSupplychainPartners() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainPartner> supplychainPartners = dataAccessManager.getSupplychainPartners(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainPartnerCount(status, name);
        return success(supplychainPartners, count);
    }

    @Override
    public JSONObject getSupplychainPartnerAll() {
        List<SupplychainPartner> supplychainPartners = dataAccessManager.getSupplychainPartnerAll();
        return success(supplychainPartners);
    }

    @Override
    public JSONObject getSupplychainPartnerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainPartnerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainPartner() {
        SupplychainPartner supplychainPartner = dataAccessManager.getSupplychainPartner(getId());
        //
        String id = getUTF("id", null);
        //合作方标题 如网址
        String title = getUTF("title", null);
        //公司名字
        String companyName = getUTF("companyName", null);
        //合作类型
        Integer type = getIntParameter("type", 0);
        //代理属性类型
        Integer proxyType = getIntParameter("proxyType", 0);
        //合作状态 0-停止合作 1-合作中
        Integer status = getIntParameter("status", 0);
        supplychainPartner.setId(id);
        supplychainPartner.setTitle(title);
        supplychainPartner.setCompanyName(companyName);
        supplychainPartner.setType(type);
        supplychainPartner.setProxyType(proxyType);
        supplychainPartner.setStatus(status);
        dataAccessManager.changeSupplychainPartner(supplychainPartner);
        return success();
    }

}
