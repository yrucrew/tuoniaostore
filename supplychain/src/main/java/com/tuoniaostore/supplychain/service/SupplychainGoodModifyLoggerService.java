package com.tuoniaostore.supplychain.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 商品库存修改日志
注意：该表主要记录人为介入修改的数据
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainGoodModifyLoggerService {

    JSONObject addSupplychainGoodModifyLogger();

    JSONObject getSupplychainGoodModifyLogger();

    JSONObject getSupplychainGoodModifyLoggers();

    JSONObject getSupplychainGoodModifyLoggerCount();
    JSONObject getSupplychainGoodModifyLoggerAll();

    JSONObject changeSupplychainGoodModifyLogger();
}
