package com.tuoniaostore.supplychain.service.impl;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.supplychain.HomepageGoodsSettingTypeEnum;
import com.tuoniaostore.commons.support.community.AreaRemoteService;
import com.tuoniaostore.commons.support.good.*;
import com.tuoniaostore.datamodel.good.*;
import com.tuoniaostore.datamodel.user.SysArea;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageGoodsSetting;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.vo.supplychain.SupplychainHomePageGood;
import com.tuoniaostore.datamodel.vo.supplychain.SupplychainHomePageGoodDetail;
import com.tuoniaostore.datamodel.vo.supplychain.SupplychainHomePageGoodDetailList;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainHomepageGoodsSettingService;
import com.tuoniaostore.supplychain.vo.SupplychainGoodVO;
import org.jetbrains.annotations.Async;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.alibaba.fastjson.JSONObject;
import org.springframework.transaction.annotation.Transactional;


@Service("supplychainHomepageGoodsSettingService")
public class SupplychainHomepageGoodsSettingServiceImpl extends BasicWebService implements SupplychainHomepageGoodsSettingService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;
    @Override
    public JSONObject addSupplychainHomepageGoodsSetting() {
        SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting = new SupplychainHomepageGoodsSetting();
        //记录类型 0=精选商品; 1=推荐商品; 2=推荐分类 3=推荐品牌
        fillGoodsSetting(supplychainHomepageGoodsSetting);
        dataAccessManager.addSupplychainHomepageGoodsSetting(supplychainHomepageGoodsSetting);
        return success();
    }

    private void fillGoodsSetting(SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting) {
        Integer type = getIntParameter("type", 0);
        //区域id
        String logicAreaId = getUTF("logicAreaId");
        //记录值 当type(记录类型)=0,1时 此字段是商品模板id; type=2时 此字段时分类id; type=3时 此字段为品牌id
        String value = getUTF("value", null);
        //名称(根据value值的类型 记一下分类/品牌名称, 说不定以后有用, value是商品id的话这个值就留空, 没想到有什么用)
        String name = getUTF("name", null);
        //标语
        String description = getUTF("description", null);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //创建人id
        String creatorId = getUTF("creatorId", null);
        supplychainHomepageGoodsSetting.setType(type);
        supplychainHomepageGoodsSetting.setLogicAreaId(logicAreaId);
        supplychainHomepageGoodsSetting.setValue(value);
        supplychainHomepageGoodsSetting.setName(name);
        supplychainHomepageGoodsSetting.setDescription(description);
        supplychainHomepageGoodsSetting.setSort(sort);
        supplychainHomepageGoodsSetting.setCreatorId(creatorId);
    }

    @Override
    public JSONObject getSupplychainHomepageGoodsSetting() {
        SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting = dataAccessManager.getSupplychainHomepageGoodsSetting(getId());
        return success(supplychainHomepageGoodsSetting);
    }

    @Override
    public JSONObject getSupplychainHomepageGoodsSettings() {
        String name = getUTF("name", null);
        List<SupplychainHomepageGoodsSetting> supplychainHomepageGoodsSettings = dataAccessManager.getSupplychainHomepageGoodsSettings( getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainHomepageGoodsSettingCount( name);
        return success(supplychainHomepageGoodsSettings, count);
    }

    @Override
    public JSONObject getSupplychainHomepageGoodsSettingAll() {
        List<SupplychainHomepageGoodsSetting> supplychainHomepageGoodsSettings = dataAccessManager.getSupplychainHomepageGoodsSettingAll();
        return success(supplychainHomepageGoodsSettings);
    }

    @Override
    public JSONObject getSupplychainHomepageGoodsSettingCount() {
        String keyWord = getUTF("keyWord", null);
        int count = dataAccessManager.getSupplychainHomepageGoodsSettingCount(keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainHomepageGoodsSetting() {
        SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting = dataAccessManager.getSupplychainHomepageGoodsSetting(getId());
        //记录类型 0=精选商品; 1=推荐商品; 2=推荐分类 3=推荐品牌
        fillGoodsSetting(supplychainHomepageGoodsSetting);
        dataAccessManager.changeSupplychainHomepageGoodsSetting(supplychainHomepageGoodsSetting);
        return success();
    }


    /**
     * 商家端采购：获取精选商品
     * @author oy
     * @date 2019/4/12
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getSupplychainHomepageGoodsSettingForSelected() {
        //获取当前地址
        String shopId = getUTF("shopId");

        if(shopId == null || shopId.equals("")){
            return fail("商店id不能为空！");
        }

        //分类查询
        String goodTypeId = getUTF("goodTypeId",null);
        //价格排序
        Integer goodPriceSort = getIntParameter("goodPriceSort",0); //价格排序 默认0 1升序 2降序


        //通过商店id 获取所有的商品
        List<SupplychainGood> list = dataAccessManager.getSupplychainGoodForHomePage(shopId,goodTypeId,goodPriceSort,getPageStartIndex(getPageSize()),getPageSize());//根据商店 和 分类 价格排序 分页获取

        List<SupplychainGoodVO> goodDetails = null;
        if(list != null && list.size() > 0){
            //根据当前用户 获取购物车里面的数量
            String id = getUser().getId();
            List<GoodShopCart> goodShopCartList = GoodShopCartRemoteService.getGoodShopCartListByUserId(id,getHttpServletRequest());
            goodDetails = getGoodPriceIdAndUnitId2(list,goodShopCartList);
        }
        //返回
        return success(goodDetails);
    }

    /***
     * 上坡详细信息（包括购物车数量）
     * @author oy
     * @date 2019/5/29
     * @param supplychainGoodById, goodShopCartList
     * @return java.util.List<com.tuoniaostore.supplychain.vo.SupplychainGoodVO>
     */
    private List<SupplychainGoodVO> getGoodPriceIdAndUnitId2(List<SupplychainGood> supplychainGoodById,List<GoodShopCart> goodShopCartList){
        List<SupplychainGoodVO> list = new ArrayList<>();//返回的结果
        List<String> priceIds = supplychainGoodById.stream().map(SupplychainGood::getPriceId).distinct().collect(Collectors.toList());//将id 拿到 然后去重 成集合
        //通过价格id集合去查找对应的单位和价格标签
        List<GoodPriceCombinationVO> allGoodPricesByPriceIds = GoodPricePosRemoteService.getAllGoodPricesByPriceIds(priceIds, getHttpServletRequest());

        if(allGoodPricesByPriceIds != null && allGoodPricesByPriceIds.size() > 0){//拼接一下商品 和商品的价格title 单位title
            supplychainGoodById.forEach(supplychainGood -> {
                SupplychainGoodVO SupplychainGoodVO = new SupplychainGoodVO();
                String id = supplychainGood.getId();
                SupplychainGoodVO.setId(id);
                SupplychainGoodVO.setId(supplychainGood.getId());//商品id
                SupplychainGoodVO.setDefineName(supplychainGood.getDefineName());//商品名称
                SupplychainGoodVO.setDefineImage(supplychainGood.getDefineImage());//商品图片
                SupplychainGoodVO.setSiglePrice(supplychainGood.getSellPrice());//单价
                SupplychainGoodVO.setStock(supplychainGood.getStock());//库存状态
                SupplychainGoodVO.setStatus(supplychainGood.getStatus());//上下架情况
                SupplychainGoodVO.setStartingQuantity(supplychainGood.getStartingQuantity());//起订量
                String priceId = supplychainGood.getPriceId();
                Optional<GoodPriceCombinationVO> first = allGoodPricesByPriceIds.stream().filter(item -> item.getPriceId().equals(priceId)).findFirst();
                if(first.isPresent()){//存在
                    //价格模板信息
                    GoodPriceCombinationVO goodPriceCombinationVO = first.get();
                    String priceTitle = goodPriceCombinationVO.getPriceTitle();
                    if(priceTitle != null){
                        SupplychainGoodVO.setPriceTitle(priceTitle);//价格标签
                    }
                    String priceIcon = goodPriceCombinationVO.getPriceIcon();
                    if(priceIcon != null){
                        SupplychainGoodVO.setPricePic(priceIcon);//价格标签
                    }
                    String unitTitle = goodPriceCombinationVO.getUnitTitle();
                    if(unitTitle != null){
                        SupplychainGoodVO.setUnit(unitTitle);//单位名称
                    }


                }
                if(goodShopCartList != null && goodShopCartList.size() > 0){
                    Optional<GoodShopCart> first1 = goodShopCartList.stream().filter(item -> item.getGoodId().equals(id)).findFirst();
                    if(first1.isPresent()){
                        GoodShopCart goodShopCart = first1.get();
                        Integer goodNumber = goodShopCart.getGoodNumber();
                        SupplychainGoodVO.setCarNum(goodNumber);
                    }else{
                        SupplychainGoodVO.setCarNum(0);
                    }
                }else{
                    SupplychainGoodVO.setCarNum(0);
                }
                list.add(SupplychainGoodVO);
            });
        }
        return list;
    }




    /**
     * 根据chainGood id 去获取商品
     * @author oy
     * @date 2019/4/13
     * @param chainGoodIds：商品id
     * @param goodTypeId：类型id
     * @param goodPriceSort：价格排序 默认0 1升序 2降序
     * @return java.util.List<com.tuoniaostore.supplychain.vo.SupplychainGoodVO>
     */
    private  List<SupplychainGoodVO> getGoodDetails(List<String> chainGoodIds,String goodTypeId,Integer goodPriceSort){
        List<SupplychainGoodVO> list = new ArrayList<>();//返回的结果
        //获取商品的价格title unitTitle
        //去chainGood表中查找对应的商品实体出来
        List<SupplychainGood> supplychainGoodById = dataAccessManager.getSupplychainGoodById(chainGoodIds,goodTypeId,goodPriceSort);
        if (supplychainGoodById != null && supplychainGoodById.size() > 0) { //虽然肯定是有数据的 但是防止数据问题带来的异常
            list = getGoodPriceIdAndUnitId(supplychainGoodById);
        }
        return list;
    }




    /**
     *  获取商品的详细信息 单位 价格标签
     * @author oy
     * @date 2019/4/15
     * @param supplychainGoodById
     * @return java.util.List<com.tuoniaostore.supplychain.vo.SupplychainGoodVO>
     */
    private List<SupplychainGoodVO> getGoodPriceIdAndUnitId(List<SupplychainGood> supplychainGoodById){
        List<SupplychainGoodVO> list = new ArrayList<>();//返回的结果
        List<String> priceIds = supplychainGoodById.stream().map(SupplychainGood::getPriceId).distinct().collect(Collectors.toList());//将id 拿到 然后去重 成集合
        //通过价格id集合去查找对应的单位和价格标签
        List<GoodPriceCombinationVO> allGoodPricesByPriceIds = GoodPricePosRemoteService.getAllGoodPricesByPriceIds(priceIds, getHttpServletRequest());
        if(allGoodPricesByPriceIds != null && allGoodPricesByPriceIds.size() > 0){//拼接一下商品 和商品的价格title 单位title
            supplychainGoodById.forEach(supplychainGood -> {
                SupplychainGoodVO SupplychainGoodVO = new SupplychainGoodVO();
                SupplychainGoodVO.setId(supplychainGood.getId());//商品id
                SupplychainGoodVO.setDefineName(supplychainGood.getDefineName());//商品名称
                SupplychainGoodVO.setDefineImage(supplychainGood.getDefineImage());//商品图片
                SupplychainGoodVO.setSiglePrice(supplychainGood.getSellPrice());//单价
                SupplychainGoodVO.setStock(supplychainGood.getStock());//库存状态
                SupplychainGoodVO.setStartingQuantity(supplychainGood.getStartingQuantity());//起订量
                String priceId = supplychainGood.getPriceId();
                Optional<GoodPriceCombinationVO> first = allGoodPricesByPriceIds.stream().filter(item -> item.getPriceId().equals(priceId)).findFirst();
                if(first.isPresent()){//存在
                    //价格模板信息
                    GoodPriceCombinationVO goodPriceCombinationVO = first.get();
                    String priceTitle = goodPriceCombinationVO.getPriceTitle();
                    if(priceTitle != null){
                        SupplychainGoodVO.setPriceTitle(priceTitle);//价格标签
                    }

                    String priceIcon = goodPriceCombinationVO.getPriceIcon();
                    if(priceIcon != null){
                        SupplychainGoodVO.setPricePic(priceIcon);//价格图片
                    }

                    String unitTitle = goodPriceCombinationVO.getUnitTitle();
                    if(unitTitle != null){
                        SupplychainGoodVO.setUnit(unitTitle);//单位名称
                    }
                }
                list.add(SupplychainGoodVO);
            });
        }
        return list;
    }

    /**
     * 商家端采购：首页数据 获取热门品牌
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getSupplychainHomepageGoodsSettingForHotBrand() {
        //获取当前地址
        String shopId = getUTF("shopId");
        if(shopId == null || shopId.equals("")){
            return fail("商店id不能为空！");
        }
        //获取品牌
        List<SupplychainHomepageGoodsSetting> supplychainHomepageGoodsSettingByAreaIdAndType =
                getsupplychainHomePageGood(HomepageGoodsSettingTypeEnum.HOT_BRANDS.getKey(),shopId,null, null);

        List<SupplychainGoodVO> list = new ArrayList<>();
        if(supplychainHomepageGoodsSettingByAreaIdAndType != null && supplychainHomepageGoodsSettingByAreaIdAndType.size() > 0){
            List<String> brandIds = supplychainHomepageGoodsSettingByAreaIdAndType.stream().map(SupplychainHomepageGoodsSetting::getValue).collect(Collectors.toList());
            if (brandIds != null && brandIds.size() > 0) {
                //远端调用获取品牌名称 品牌title 品牌id
                List<GoodBrand> barcodesByBrandIds = GoodBrandRemoteService.getBarcodesByBrandIds(brandIds, getHttpServletRequest());
                //barcodesByBrandIds 肯定不为空
                barcodesByBrandIds.forEach(x ->{
                    Integer status = x.getStatus();
                    if(status == 0){//筛选掉 不在远程调用 其他地方有调用
                        SupplychainGoodVO vo = new SupplychainGoodVO();
                        vo.setBrandId(x.getId());
                        vo.setBrandLogo(x.getLogo());
                        vo.setBrandName(x.getName());
                        list.add(vo);
                    }
                });
            }
        }
        return success(list);
    }

    /**
     * 商家端采购：首页数据 获取热门品类
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getSupplychainHomepageGoodsSettingForHotCategory() {
        //获取当前地址
        String shopId = getUTF("shopId");
        if(shopId == null || shopId.equals("")){
            return fail("商店id不能为空！");
        }

        //获取品牌
        List<SupplychainHomepageGoodsSetting> supplychainHomepageGoodsSettingByAreaIdAndType =
                getsupplychainHomePageGood(HomepageGoodsSettingTypeEnum.HOT_CATEGORY.getKey(),shopId, null, null);

        List<SupplychainGoodVO> list = new ArrayList<>();//返回结果
        if(supplychainHomepageGoodsSettingByAreaIdAndType != null && supplychainHomepageGoodsSettingByAreaIdAndType.size() > 0){
            List<String> typeIds = supplychainHomepageGoodsSettingByAreaIdAndType.stream().map(SupplychainHomepageGoodsSetting::getValue).collect(Collectors.toList());
            if(typeIds != null && typeIds.size() > 0){
                try {
                    List<GoodType> goodTypes = GoodTypeRemoteService.getGoodTypes(typeIds, getHttpServletRequest());
                    if(goodTypes != null && goodTypes.size() > 0){//类型
                        goodTypes.forEach(x -> {
                            SupplychainGoodVO vo = new SupplychainGoodVO();
                            vo.setTypeId(x.getId());//id
                            vo.setTypeName(x.getTitle());//品名
                            vo.setTypeImage(x.getImage());//图片
                            vo.setTypeRemark(x.getRemark());//备注
                            list.add(vo);
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return success(list);
    }

    /***
     * 商家端采购：首页数据 获取推荐商品
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getSupplychainHomepageGoodsSettingForRecommendingCommodities() {
        //获取当前地址
        String shopId = getUTF("shopId");
        if(shopId == null || shopId.equals("")){
            return fail("商店id不能为空！");
        }
        return getHomePageGoodByShopId(shopId,HomepageGoodsSettingTypeEnum.RECOMMENDING_COMMODITIES.getKey());
    }

    /***
     * 根据商店id 和 商品类型id 获取采购首页的商品数据
     * @author oy
     * @date 2019/4/22
     * @param shopId, type
     * @return com.alibaba.fastjson.JSONObject
     */
    private JSONObject getHomePageGoodByShopId(String shopId,int type){
        String logicAreaId = null;
        //获取当前商店的地址 华南 华北  华中
        if(shopId != null && !shopId.equals("")){
            SupplychainShopProperties supplychainShopPropertiesById = dataAccessManager.getSupplychainShopPropertiesById(shopId);
            if(supplychainShopPropertiesById != null){
                logicAreaId = supplychainShopPropertiesById.getLogicAreaId();
            }
        }
        //获取当前区域的所有该地址下面的所有精选商品
        List<SupplychainHomepageGoodsSetting> supplychainHomepageGoodsSettingByAreaIdAndType = dataAccessManager.getSupplychainHomepageGoodsSettingByAreaIdAndType(logicAreaId, type, null, null);

        //返回值
        List<SupplychainGoodVO> goodDetails = new ArrayList<>();
        //推荐不为空
        if(supplychainHomepageGoodsSettingByAreaIdAndType != null && supplychainHomepageGoodsSettingByAreaIdAndType.size() > 0){
            //获取所有的精选 priceId
            List<String> priceIds = supplychainHomepageGoodsSettingByAreaIdAndType.stream().map(SupplychainHomepageGoodsSetting::getValue).collect(Collectors.toList());

            //获取当前的商店的商品信息 ，有当前的价格标签的
            List<SupplychainGood> goodList = dataAccessManager.getsupplychaingoodByShopIdAndPriceId(shopId,priceIds,null,1,null,null);

            //根据这些商品 获取详细的数据
            if(goodList != null && goodList.size() > 0){
                goodDetails = getGoodPriceIdAndUnitId(goodList);
            }
            if(priceIds.size() == goodList.size() && priceIds.size() != 0){//查看这个仓库是否全都有该价格标签的商品
                return success(goodDetails);
            }
            List<String> goodPriceIds = goodList.stream().map(SupplychainGood::getPriceId).collect(Collectors.toList());//已经有的priceId 的对应商品

            //通过priceId 获取到商品的模板数据
            List<String> outPriceIds = priceIds.stream().filter(t -> !goodPriceIds.contains(t)).collect(Collectors.toList()); //还有没在商店中查到的

            //没有的模板  就去模板库中 查找出对应的记录
            List<GoodPriceCombinationVO> templateByPriceId = GoodPricePosRemoteService.getTemplateByPriceId(outPriceIds, getHttpServletRequest());

            if(templateByPriceId != null && templateByPriceId.size() > 0){
                for (GoodPriceCombinationVO x : templateByPriceId) {
                    SupplychainGoodVO vo = new SupplychainGoodVO();
                    vo.setDefineName(x.getGoodName());
                    vo.setUnit(x.getUnitTitle());
                    vo.setPriceTitle(x.getPriceTitle());
                    vo.setSiglePrice(x.getSellPrice());
                    vo.setTypeImage(x.getImageUrl());
                    vo.setPriceId(x.getPriceId());
                    vo.setPricePic(x.getPriceIcon());
                    goodDetails.add(vo);
                }
            }
            return success(goodDetails);
        }else{
            return success();
        }
    }


    /**
     * 获取首页数据的详情
     * @author oy
     * @date 2019/4/13
     * @param type,shopId,pageIndex,pageSize
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainHomepageGoodsSetting>
     */
    public List<SupplychainHomepageGoodsSetting> getsupplychainHomePageGood(Integer type,String shopId,Integer pageIndex,Integer pageSize){
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(shopId);
        String  areaId = null;
        if(supplychainShopProperties != null){
            areaId = supplychainShopProperties.getLogicAreaId();
        }
        //然后获取areaId
        List<SupplychainHomepageGoodsSetting> supplychainHomepageGoodsSettingByAreaIdAndType = null;
        if(areaId != null){//地址id 华南地区 华中等
            //通过areaId 获取到 首页商品
            supplychainHomepageGoodsSettingByAreaIdAndType
                    = dataAccessManager.getSupplychainHomepageGoodsSettingByAreaIdAndType(areaId, type,pageIndex,pageSize);
        }
        return supplychainHomepageGoodsSettingByAreaIdAndType;
    }


    /***
     * 商家端：采购 获取热门品类下面的商品
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getSupplychainGoodByGoodTypeByStore() {
        //获取仓库id
        String shopId = getUTF("shopId");
        //获取品牌id
        String typeId = getUTF("typeId");//类型id

        Integer goodPriceSort = getIntParameter("goodPriceSort",0); //价格排序 默认0 1升序 2降序

        if(shopId == null || shopId.equals("")){
            return fail("商店id不能为空！");
        }
        if(typeId == null || typeId.equals("")){
            return fail("品类id不能为空！");
        }

        int pageNum = getIntParameter("pageIndex", 1);//页码
        int pageSize = getIntParameter("pageSize", 10);//页面大小
        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageNum - 1);
        int limitEnd = pageSize;

        List<SupplychainGood> supplychainGoodByUserIdAndShopId = dataAccessManager.getSupplychainGoodForHomePage(shopId,typeId,goodPriceSort,limitStart,limitEnd);//getSupplychainGoodByUserIdAndShopId
        if(supplychainGoodByUserIdAndShopId != null && supplychainGoodByUserIdAndShopId.size() >0 ){
            List<SupplychainGoodVO> goodPriceIdAndUnitId = getGoodPriceIdAndUnitId(supplychainGoodByUserIdAndShopId);
            return success(goodPriceIdAndUnitId);
        }
        return success();
    }

    /**
     * 商家端：采购 获取热门品牌品牌下面的商品
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getSupplychainGoodByGoodBrandByStore() {
        //品牌
        String brandId = getUTF("brandId");
        //商店id
        String shopId = getUTF("shopId");
        //分页参数
        int pageSize = getPageSize();//页大小 limit
        int pageStartIndex = getPageStartIndex(getPageSize());//页码page

        if(brandId == null || brandId.equals("")){
            return fail("品牌id不能为空！");
        }

        if(shopId == null || shopId.equals("")){
            return fail("仓库id不能为空！");
        }
        //远程调用 查找当前品牌的是那些模板id
        List<GoodTemplate> goodTemplatesByBrandId = GoodTemplateRemoteService.getGoodTemplatesByBrandId(brandId, getHttpServletRequest());
        List<String> templateIds = null;
        if(goodTemplatesByBrandId != null && goodTemplatesByBrandId.size() > 0){
            //将模板id 弄成集合 去重
            templateIds = goodTemplatesByBrandId.stream().map(GoodTemplate::getId).distinct().collect(Collectors.toList());
        }else{ //说明没有模板 所以没有商品
            return success();
        }
        //通过这些模板id 和 shopId去查找一下商品 需要分页
        List<SupplychainGood> list = dataAccessManager.getSupplychainGoodByShopIdAndTempateIds(shopId,templateIds,pageStartIndex,pageSize);
        //填充商品信息
        if(list != null && list.size() > 0 ){
            List<SupplychainGoodVO> goodPriceIdAndUnitId = getGoodPriceIdAndUnitId(list);
            return success(goodPriceIdAndUnitId);
        }
        return success();
    }


    /**
     * 商家端：采购 页面搜索商品
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getSupplychainGoodByGoodNameSearch() {
        //商店id
        String shopId = getUTF("shopId");
        //商品名称
        String goodNameOrBrandName = getUTF("goodNameOrBrandName");

        if(goodNameOrBrandName == null || goodNameOrBrandName.equals("")){
            return null;
        }

        //分页参数
        int pageSize = getPageSize();//页大小 limit
        int pageStartIndex = getPageStartIndex(getPageSize());//页码page

        //首先搜索商品名称
        int status = 1;
        List<SupplychainGood> list = dataAccessManager.getsupplychainGoodByGoodNameAndStatus(goodNameOrBrandName,shopId,status,pageStartIndex,pageSize);
        if(list != null && list.size() > 0){
            //根据当前用户 获取购物车里面的数量
            String id = getUser().getId();
            List<GoodShopCart> goodShopCartList = GoodShopCartRemoteService.getGoodShopCartListByUserId(id,getHttpServletRequest());
            List<SupplychainGoodVO> goodPriceIdAndUnitId = getGoodPriceIdAndUnitId2(list,goodShopCartList);
            return success(goodPriceIdAndUnitId);
        }
        //搜索品牌
        List<String> goodTemplateId = GoodBrandTemplateRemoteService.getBarcodesByBrandName(goodNameOrBrandName, getHttpServletRequest());
        if(goodTemplateId != null && goodTemplateId.size() > 0){
            //去商品表里面查
            List<SupplychainGood> supplychainGoodByShopIdAndTempateIds = dataAccessManager.getSupplychainGoodByShopIdAndTempateIds(shopId, goodTemplateId, pageStartIndex, pageSize);
            if (supplychainGoodByShopIdAndTempateIds != null && supplychainGoodByShopIdAndTempateIds.size() > 0) {
                List<SupplychainGoodVO> goodVOS = getGoodPriceIdAndUnitId(supplychainGoodByShopIdAndTempateIds);
                return success(goodVOS);
            }
        }
        return success();
    }

    /***
     * 商家端采购：获取首页的 精选商品
     * @author oy
     * @date 2019/4/22
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getHomepageGoodsSettingForSelected() {
        //获取商店id
        String shopId = getUTF("shopId");
        return getHomePageGoodByShopId(shopId,HomepageGoodsSettingTypeEnum.SELECTED_COMMODITIES.getKey());
    }


    /***
     * 精选商品配置首页（扩展推荐商品）
     * @author oy
     * @date 2019/4/25
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listSupplychainGoodForHomeSetting() {
        //SupplychainHomePageGoodDetail
        //获取全部的精选商品
        int homePageType = getIntParameter("homePageType");//0=精选商品; 1=推荐商品;
        //条码 或者 商品名称
        String goodBarcodeOrName = getUTF("goodBarcodeOrName",null);
        //区域名称
        String areaName = getUTF("areaName",null);

        if(homePageType != 0 &&  homePageType != 1){
            return fail("商品类型参数错误！");
        }

        //远程调用查询地址信息
        List<String> areaIds = null;
        List<SysArea> sysAreas = null;
        if(areaName != null && !areaName.equals("")){
            sysAreas = AreaRemoteService.getSysAreaByNameLike(areaName,getHttpServletRequest());
            if(sysAreas != null && sysAreas.size() > 0){
                areaIds = sysAreas.stream().map(SysArea::getId).collect(Collectors.toList());
            }else{//说明以地址查询的模糊查询 没有结果 直接返回
                return success(new ArrayList(),0);
            }
        }
        List<SupplychainHomepageGoodsSetting> supplychainHomepageGoodsSettingByAreaIdAndType = null;
        if(areaIds != null && areaIds.size() > 0){
            supplychainHomepageGoodsSettingByAreaIdAndType
                    = dataAccessManager.getSupplychainHomepageGoodsSettingByAreaIdsAndType(areaIds, homePageType, null,null);
        }else{//进来这一步 是因为 没用到地址查询的
            supplychainHomepageGoodsSettingByAreaIdAndType
                    = dataAccessManager.getSupplychainHomepageGoodsSettingByAreaIdAndType(null, homePageType, null,null);
        }

        if(supplychainHomepageGoodsSettingByAreaIdAndType != null && supplychainHomepageGoodsSettingByAreaIdAndType.size() > 0){
            List<String> priceIds = supplychainHomepageGoodsSettingByAreaIdAndType.stream().map(SupplychainHomepageGoodsSetting::getValue).collect(Collectors.toList());
            //前面进行查询模糊，如果有地址信息，查不到不会进来，查到了 肯定是有地址集合的
            if(areaName == null || areaName.equals("")){//说明没有模糊查询的时候，所以拿出chainHomePage中的所有信息查出来
                areaIds = supplychainHomepageGoodsSettingByAreaIdAndType.stream().map(SupplychainHomepageGoodsSetting::getLogicAreaId).collect(Collectors.toList());
                sysAreas = AreaRemoteService.getSysAreasByIds(areaIds,getHttpServletRequest());
            }
            //通过价格id 去查所有相关联的属性出来
            List<GoodPriceCombinationVO> allGoodPricesByPriceIds = GoodPricePosRemoteService.getTemplateByPriceIdByParam(priceIds,goodBarcodeOrName,getHttpServletRequest());
            if(allGoodPricesByPriceIds == null || allGoodPricesByPriceIds.size() == 0){
                return success(new ArrayList(),0);//这里没有价格标签 就直接返回
            }
            List<SupplychainHomePageGood> supplychainHomePageGoodDetails = fillDetail2(supplychainHomepageGoodsSettingByAreaIdAndType, allGoodPricesByPriceIds, sysAreas);
            return success(supplychainHomePageGoodDetails);
        }
        return success(new ArrayList(),0);
    }

    /**
     * 精选（推荐）商品配置首页 批量删除
     * @author oy
     * @date 2019/4/26
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject bacchDeleteSupplychainGoodForHomeSetting() {
        String deleteList = getUTF("deleteList");
        if(deleteList == null || deleteList.equals("")){
            return fail("集合不能为空!");
        }
        List<String> strings = JSONObject.parseArray(deleteList, String.class);
        if(strings != null && strings.size() > 0){
            dataAccessManager.bacchDeleteHomeSettingBy(strings);
        }
        return success("删除成功!");
    }

    @Override
    public JSONObject addSupplychainGoodForHomeSetting() {
        String homePageGoodList = getUTF("homePageGoodList");
        if(homePageGoodList == null || homePageGoodList.equals("")){
            return fail("参数不能为空");
        }
        SupplychainHomePageGood supplychainHomePageGoodDetails = JSONObject.parseObject(homePageGoodList, SupplychainHomePageGood.class);

        String id = getUser().getId();
        if(supplychainHomePageGoodDetails != null){

            List<SupplychainHomepageGoodsSetting> list = new ArrayList<>();
            List<SupplychainHomePageGoodDetailList> list1 = supplychainHomePageGoodDetails.getList();
            list1.forEach(y ->{
                SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting = new SupplychainHomepageGoodsSetting();
                String areaId = supplychainHomePageGoodDetails.getAreaId();
                if(areaId != null){
                    supplychainHomepageGoodsSetting.setLogicAreaId(areaId);//地址id
                }
                String goodPriceId = y.getGoodPriceId();
                if(goodPriceId != null){
                    supplychainHomepageGoodsSetting.setValue(y.getGoodPriceId());//价格id
                }

                String goodName = y.getGoodName();
                if(goodName != null){
                    supplychainHomepageGoodsSetting.setName(y.getGoodName());//商品名称
                }
                Integer sort = y.getSort();
                if(sort != null){
                    supplychainHomepageGoodsSetting.setSort(y.getSort());//商品排序
                }else{
                    supplychainHomepageGoodsSetting.setSort(0);//商品排序
                }
                supplychainHomepageGoodsSetting.setCreatorId(id);//创建人
                //supplychainHomepageGoodsSetting.setDescription(x.getDescription());//描述
                supplychainHomepageGoodsSetting.setType(supplychainHomePageGoodDetails.getTypeNum());//类型 0  1 精选 推荐
                list.add(supplychainHomepageGoodsSetting);
            });
            if(list != null && list.size() > 0){
                dataAccessManager.addSupplychainHomepageGoodsSettings(list);
            }
            return success("保存成功");
        }
        return success();
    }


    /**
     * 推荐品牌首页显示 （带  区域名称，品牌名称 搜索 ）
     * @author oy
     * @date 2019/4/29
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listForHomeSettingByBrand() {
        //获取两个条件查询的字段
        String areaName = getUTF("areaName",null);
        String brandName = getUTF("brandName",null);

        //根据地址名称 获取当前的地址id
        //远程调用查询地址信息
        List<String> areaIds = null;
        List<SysArea> sysAreas = null;
        if(areaName != null && !areaName.equals("")){
            sysAreas = AreaRemoteService.getSysAreaByNameLike(areaName,getHttpServletRequest());
            if(sysAreas != null && sysAreas.size() > 0){
                areaIds = sysAreas.stream().map(SysArea::getId).collect(Collectors.toList());
            }else{//说明以地址查询的模糊查询 没有结果 直接返回
                return success();
            }
        }

        //查询首页品牌表 查询当前的品牌id
        List<SupplychainHomepageGoodsSetting> supplychainHomepageGoodsSettingByAreaIdsAndType
                = dataAccessManager.getSupplychainHomepageGoodsSettingByAreaIdsAndType(areaIds, HomepageGoodsSettingTypeEnum.HOT_BRANDS.getKey(), null, null);

        List<SupplychainHomePageGood> list = new ArrayList<>();//返回结果

        if(supplychainHomepageGoodsSettingByAreaIdsAndType != null && supplychainHomepageGoodsSettingByAreaIdsAndType.size() > 0){
            //品牌集合
            List<String> brandIds = supplychainHomepageGoodsSettingByAreaIdsAndType.stream().map(SupplychainHomepageGoodsSetting::getValue).distinct().collect(Collectors.toList());
            //是否模糊查询的地址 如果没有就去找所有的地址
            if(areaName == null || areaName.equals("")){
                areaIds = supplychainHomepageGoodsSettingByAreaIdsAndType.stream().map(SupplychainHomepageGoodsSetting::getLogicAreaId).distinct().collect(Collectors.toList());
                sysAreas = AreaRemoteService.getSysAreasByIds(areaIds, getHttpServletRequest());//获取地址信息
            }

            //远程调用品牌的信息集合
            if(brandIds != null && brandIds.size() >0 ){
                List<GoodBrand> barcodesByBrandIds = GoodBrandRemoteService.getBarcodesByBrandIdsAndNameLike(brandIds,brandName,getHttpServletRequest());
                if(barcodesByBrandIds != null && barcodesByBrandIds.size() > 0){
                    for (SupplychainHomepageGoodsSetting x : supplychainHomepageGoodsSettingByAreaIdsAndType) {
                        SupplychainHomePageGood supplychainHomePageGood = null;
                        Optional<SupplychainHomePageGood> first1 = list.stream().filter(item -> item.getAreaId().equals(x.getLogicAreaId())).findFirst();
                        List<GoodBrand> brandList = null;
                        if(first1.isPresent()){//说明已经添加了
                            supplychainHomePageGood = first1.get();
                            brandList = supplychainHomePageGood.getBrandList();
                        }else{
                            supplychainHomePageGood = new SupplychainHomePageGood();
                            brandList = new ArrayList<>();
                            supplychainHomePageGood.setTypeNum(x.getType());
                            String value = HomepageGoodsSettingTypeEnum.getValue(x.getType());
                            supplychainHomePageGood.setType(value);
                            supplychainHomePageGood.setDescription(x.getDescription());
                            String logicAreaId = x.getLogicAreaId();
                            supplychainHomePageGood.setAreaId(logicAreaId);//地址id
                            if(sysAreas != null && sysAreas.size() > 0){
                                Optional<SysArea> first = sysAreas.stream().filter(item -> item.getId().equals(logicAreaId)).findFirst();
                                if(first.isPresent()){
                                    SysArea sysArea = first.get();
                                    supplychainHomePageGood.setAreaName(sysArea.getTitle());
                                }
                            }
                            //拿到对象里面的集合的地址
                            supplychainHomePageGood.setBrandList(brandList);
                            brandList = supplychainHomePageGood.getBrandList();
                            list.add(supplychainHomePageGood);
                        }
                        String brandId = x.getValue();
                        Optional<GoodBrand> first = barcodesByBrandIds.stream().filter(item -> item.getId().equals(brandId)).findFirst();
                        if(first.isPresent()){
                            GoodBrand goodBrand = first.get();
                            brandList.add(goodBrand);
                        }
                    }
                }
            }
        }else{
            return success(list);
        }
        return success(list);
    }

    /**
     * 推荐分类首页显示 （带  区域名称，分类名称 搜索 ）
     * @author oy
     * @date 2019/4/29
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject listForHomeSettingByGoodType() {
        //获取两个条件查询的字段
        String areaName = getUTF("areaName",null);
        String typeName = getUTF("typeName",null);

        //根据地址名称 获取当前的地址id
        //远程调用查询地址信息
        List<String> areaIds = null;
        List<SysArea> sysAreas = null;
        if(areaName != null && !areaName.equals("")){
            sysAreas = AreaRemoteService.getSysAreaByNameLike(areaName,getHttpServletRequest());
            if(sysAreas != null && sysAreas.size() > 0){
                areaIds = sysAreas.stream().map(SysArea::getId).collect(Collectors.toList());
            }else{//说明以地址查询的模糊查询 没有结果 直接返回
                return success();
            }
        }

        //查询首页品牌表 查询当前的品牌id
        List<SupplychainHomepageGoodsSetting> supplychainHomepageGoodsSettingByAreaIdsAndType
                = dataAccessManager.getSupplychainHomepageGoodsSettingByAreaIdsAndType(areaIds, HomepageGoodsSettingTypeEnum.HOT_CATEGORY.getKey(), null, null);

        List<SupplychainHomePageGood> list = new ArrayList<>();//返回结果
        if(supplychainHomepageGoodsSettingByAreaIdsAndType != null && supplychainHomepageGoodsSettingByAreaIdsAndType.size() > 0){
            //品牌集合
            List<String> brandIds = supplychainHomepageGoodsSettingByAreaIdsAndType.stream().map(SupplychainHomepageGoodsSetting::getValue).collect(Collectors.toList());
            //是否模糊查询的地址 如果没有就去找所有的地址
            if(areaName == null || areaName.equals("")){
                areaIds = supplychainHomepageGoodsSettingByAreaIdsAndType.stream().map(SupplychainHomepageGoodsSetting::getLogicAreaId).collect(Collectors.toList());
                sysAreas = AreaRemoteService.getSysAreasByIds(areaIds, getHttpServletRequest());//获取地址信息
            }
            //远程调用品牌的信息集合
            if(brandIds != null && brandIds.size() >0 ){
                List<GoodType> goodTypesAndNameLike = GoodTypeRemoteService.getGoodTypesAndNameLike(brandIds,typeName,getHttpServletRequest());
                if(goodTypesAndNameLike != null && goodTypesAndNameLike.size() > 0){
                    for (SupplychainHomepageGoodsSetting x : supplychainHomepageGoodsSettingByAreaIdsAndType) {
                        SupplychainHomePageGood supplychainHomePageGood = null;
                        Optional<SupplychainHomePageGood> first1 = list.stream().filter(item -> item.getAreaId().equals(x.getLogicAreaId())).findFirst();
                        List<GoodType> brandList = null;
                        if(first1.isPresent()){//说明已经添加了
                            supplychainHomePageGood = first1.get();
                            brandList = supplychainHomePageGood.getTypeList();
                        }else{
                            supplychainHomePageGood = new SupplychainHomePageGood();
                            brandList = new ArrayList<>();
                            supplychainHomePageGood.setTypeNum(x.getType());
                            String value = HomepageGoodsSettingTypeEnum.getValue(x.getType());
                            supplychainHomePageGood.setType(value);
                            supplychainHomePageGood.setDescription(x.getDescription());
                            String logicAreaId = x.getLogicAreaId();
                            supplychainHomePageGood.setAreaId(logicAreaId);//地址id
                            if(sysAreas != null && sysAreas.size() > 0){
                                Optional<SysArea> first = sysAreas.stream().filter(item -> item.getId().equals(logicAreaId)).findFirst();
                                if(first.isPresent()){
                                    SysArea sysArea = first.get();
                                    supplychainHomePageGood.setAreaName(sysArea.getTitle());
                                }
                            }
                            //拿到对象里面的集合的地址
                            supplychainHomePageGood.setTypeList(brandList);
                            brandList = supplychainHomePageGood.getTypeList();
                            list.add(supplychainHomePageGood);
                        }
                        String typeId = x.getValue();
                        Optional<GoodType> first = goodTypesAndNameLike.stream().filter(item -> item.getId().equals(typeId)).findFirst();
                        if(first.isPresent()){
                            GoodType goodType = first.get();
                            brandList.add(goodType);
                        }
                    }
                }
            }
        }else{
            return success(list);
        }
        return success(list);
    }

    @Override
    public JSONObject getHomePageGoodByAreaId() {
        String areaId = getUTF("areaId");
        int type = getIntParameter("type", 0);
        if(areaId != null && !areaId.equals("")){
            List<SupplychainHomepageGoodsSetting> supplychainHomepageGoodsSettingByAreaIdAndType = dataAccessManager.getSupplychainHomepageGoodsSettingByAreaIdAndType(areaId, type, null, null);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("list",supplychainHomepageGoodsSettingByAreaIdAndType);
            return jsonObject;
        }
        return success();
    }

    /**
     * 批量修改推荐首页设置
     * @author oy
     * @date 2019/5/2
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject batchChangeHomeGoods() {
        String homePageGoodList = getUTF("homePageGoodList");
        if(homePageGoodList == null || homePageGoodList.equals("")){
            return fail("参数不能为空");
        }
        SupplychainHomePageGood supplychainHomePageGoodDetails = JSONObject.parseObject(homePageGoodList, SupplychainHomePageGood.class);
        String areaId = supplychainHomePageGoodDetails.getAreaId();
        String id = getUser().getId();
        if(supplychainHomePageGoodDetails != null){
            List<SupplychainHomepageGoodsSetting> list = new ArrayList<>();
            List<SupplychainHomePageGoodDetailList> list1 = supplychainHomePageGoodDetails.getList();
            Integer typeNum = supplychainHomePageGoodDetails.getTypeNum();
            dataAccessManager.DeleteHomeSettingByAreaId(areaId,typeNum);
            if(list1 != null && list1.size() > 0){
                list1.forEach(y ->{
                    SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting = new SupplychainHomepageGoodsSetting();
                    if(areaId != null){
                        supplychainHomepageGoodsSetting.setLogicAreaId(areaId);//地址id
                    }
                    String goodPriceId = y.getGoodPriceId();
                    if(goodPriceId != null){
                        supplychainHomepageGoodsSetting.setValue(y.getGoodPriceId());//价格id
                    }

                    String goodName = y.getGoodName();
                    if(goodName != null){
                        supplychainHomepageGoodsSetting.setName(y.getGoodName());//商品名称
                    }
                    Integer sort = y.getSort();
                    if(sort != null){
                        supplychainHomepageGoodsSetting.setSort(y.getSort());//商品排序
                    }else{
                        supplychainHomepageGoodsSetting.setSort(0);//商品排序
                    }
                    supplychainHomepageGoodsSetting.setCreatorId(id);//创建人
                    supplychainHomepageGoodsSetting.setType(supplychainHomePageGoodDetails.getTypeNum());//类型 0  1 精选 推荐
                    list.add(supplychainHomepageGoodsSetting);
                });
                if(list != null && list.size() > 0){
                    dataAccessManager.addSupplychainHomepageGoodsSettings(list);
                }
                return success("修改成功！");
            }
        }
        return success("修改成功！");
    }

    /**
     * 填充方案二
     * @author oy
     * @date 2019/4/27
     * @param supplychainHomepageGoodsSettingByAreaIdAndType, allGoodPricesByPriceIds, sysAreas
     * @return java.util.List<com.tuoniaostore.datamodel.vo.supplychain.SupplychainHomePageGood>
     */
    private List<SupplychainHomePageGood> fillDetail2(List<SupplychainHomepageGoodsSetting> supplychainHomepageGoodsSettingByAreaIdAndType
            ,List<GoodPriceCombinationVO> allGoodPricesByPriceIds,List<SysArea> sysAreas){
        List<SupplychainHomePageGood> list = new ArrayList<>();//存放返回结果

        if(supplychainHomepageGoodsSettingByAreaIdAndType != null && supplychainHomepageGoodsSettingByAreaIdAndType.size() > 0){
            for (SupplychainHomepageGoodsSetting x : supplychainHomepageGoodsSettingByAreaIdAndType) {
                SupplychainHomePageGood supplychainHomePageGood = null;//一级
                List<SupplychainHomePageGoodDetailList> supplychainHomePageGoodDetailLists = null; //二级list

                Optional<SupplychainHomePageGood> first2 = list.stream().filter(item -> item.getAreaId().equals(x.getLogicAreaId())).findFirst();
                if(first2.isPresent()){//已经有了
                    supplychainHomePageGood = first2.get();
                    supplychainHomePageGoodDetailLists = supplychainHomePageGood.getList();
                }else{//没有 要新建
                    supplychainHomePageGood = new SupplychainHomePageGood();
                    supplychainHomePageGoodDetailLists = new ArrayList<>();
                }
                SupplychainHomePageGoodDetailList supplychainHomePageGoodDetailList = new SupplychainHomePageGoodDetailList();//二级详情
                int type = x.getType();
                supplychainHomePageGood.setType(HomepageGoodsSettingTypeEnum.getValue(type));//分类名称

                Optional<SysArea> first1 = sysAreas.stream().filter(item -> item.getId().equals(x.getLogicAreaId())).findFirst();
                if(first1.isPresent()){
                    SysArea sysArea = first1.get();
                    supplychainHomePageGood.setAreaId(sysArea.getId());//区域id
                    supplychainHomePageGood.setAreaName(sysArea.getTitle());//区域标题
                }
                String priceId = x.getValue();//价格id
                Optional<GoodPriceCombinationVO> first = null;
                if(allGoodPricesByPriceIds != null && allGoodPricesByPriceIds.size() > 0){
                    first = allGoodPricesByPriceIds.stream().filter(item -> item.getPriceId().equals(priceId)).findFirst();
                    if(first.isPresent()){
                        supplychainHomePageGoodDetailList.setHomePageGoodId(x.getId());//id
                        supplychainHomePageGoodDetailList.setSort(x.getSort());//排序
                        GoodPriceCombinationVO goodPriceCombinationVO = first.get();
                        supplychainHomePageGoodDetailList.setGoodTemplateId(goodPriceCombinationVO.getGoodTemplateId());//商品模板id
                        supplychainHomePageGoodDetailList.setGoodTemplatePic(goodPriceCombinationVO.getImageUrl());//图片
                        supplychainHomePageGoodDetailList.setGoodTemplateName(goodPriceCombinationVO.getGoodName());//商品模板名字
                        supplychainHomePageGoodDetailList.setGoodPriceId(goodPriceCombinationVO.getPriceId());//价格标签id
                        supplychainHomePageGoodDetailList.setGoodPriceName(goodPriceCombinationVO.getPriceTitle());//价格标签
                        supplychainHomePageGoodDetailList.setGoodName(goodPriceCombinationVO.getGoodName() + goodPriceCombinationVO.getPriceTitle());//商品名称
                        supplychainHomePageGoodDetailList.setBarcode(goodPriceCombinationVO.getBarcode());//条码
                        supplychainHomePageGoodDetailList.setBarcodeId(goodPriceCombinationVO.getBarcodeId());//条码id
                        supplychainHomePageGoodDetailList.setUnit(goodPriceCombinationVO.getUnitTitle());//商品规格
                        supplychainHomePageGoodDetailList.setUnitId(goodPriceCombinationVO.getUnitTitleId());//商品规格id

                        supplychainHomePageGoodDetailList.setOneTypeId(goodPriceCombinationVO.getTypeNameOneId());//一级分类id
                        supplychainHomePageGoodDetailList.setOneType(goodPriceCombinationVO.getTypeNameOneName());//一级分类
                        supplychainHomePageGoodDetailList.setTwoTypeId(goodPriceCombinationVO.getTypeNameTwoId());//二级分类id
                        supplychainHomePageGoodDetailList.setTwoType(goodPriceCombinationVO.getTypeNameTwoName());//二级分类
                    }
                }

                if(first1.isPresent()){//如果地址有 就加记录
                    if(first2.isPresent()){//已经有了
                        supplychainHomePageGoodDetailLists.add(supplychainHomePageGoodDetailList);
                    }else{//没有 要新建
                        supplychainHomePageGoodDetailLists.add(supplychainHomePageGoodDetailList);
                        supplychainHomePageGood.setList(supplychainHomePageGoodDetailLists);
                        list.add(supplychainHomePageGood);
                    }
                }
            }
        }
        return list;
    }


}
