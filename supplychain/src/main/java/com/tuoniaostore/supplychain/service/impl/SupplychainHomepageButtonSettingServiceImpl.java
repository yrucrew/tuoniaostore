package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageButtonSetting;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainHomepageButtonSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;

import javax.servlet.http.HttpServletRequest;


@Service("supplychainHomepageButtonSettingService")
public class SupplychainHomepageButtonSettingServiceImpl extends BasicWebService implements SupplychainHomepageButtonSettingService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainHomepageButtonSetting() {
        SupplychainHomepageButtonSetting supplychainHomepageButtonSetting = new SupplychainHomepageButtonSetting();
        //按钮类型 0=商品分类ID; 1=特殊按钮(详细请参考value);
        Integer type = getIntParameter("type", 0);
        //按钮名称
        String name = getUTF("name", null);
        //值 当type=0时此字段为商品分类ID 当type=1时此字段为按钮功能的描述
        String value = getUTF("value", null);
        //图片url
        String img = getUTF("img", null);
        //按钮状态 0=有效; 1=无效;
        Integer status = getIntParameter("status", 0);
        //排序
        Integer sort = getIntParameter("sort", 0);
        supplychainHomepageButtonSetting.setType(type);
        supplychainHomepageButtonSetting.setName(name);
        supplychainHomepageButtonSetting.setValue(value);
        supplychainHomepageButtonSetting.setImg(img);
        supplychainHomepageButtonSetting.setStatus(status);
        supplychainHomepageButtonSetting.setSort(sort);
        dataAccessManager.addSupplychainHomepageButtonSetting(supplychainHomepageButtonSetting);
        return success();
    }

    @Override
    public JSONObject getSupplychainHomepageButtonSetting() {
        SupplychainHomepageButtonSetting supplychainHomepageButtonSetting = dataAccessManager.getSupplychainHomepageButtonSetting(getId());
        return success(supplychainHomepageButtonSetting);
    }

    @Override
    public JSONObject getSupplychainHomepageButtonSettings() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainHomepageButtonSetting> supplychainHomepageButtonSettings = dataAccessManager.getSupplychainHomepageButtonSettings(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainHomepageButtonSettingCount(status, name);
        return success(supplychainHomepageButtonSettings, count);
    }

    @Override
    public JSONObject getSupplychainHomepageButtonSettingAll() {
        List<SupplychainHomepageButtonSetting> supplychainHomepageButtonSettings = dataAccessManager.getSupplychainHomepageButtonSettingAll();
        return success(supplychainHomepageButtonSettings);
    }

    @Override
    public JSONObject getSupplychainHomepageButtonSettingCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainHomepageButtonSettingCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainHomepageButtonSetting() {
        SupplychainHomepageButtonSetting supplychainHomepageButtonSetting = dataAccessManager.getSupplychainHomepageButtonSetting(getId());
        //按钮类型 0=商品分类ID; 1=特殊按钮(详细请参考value);
        Integer type = getIntParameter("type", 1);
        //按钮名称
        String name = getUTF("name", null);
        //值 当type=0时此字段为商品分类ID 当type=1时此字段为按钮功能的描述
//        String value = getUTF("value", null);
        HttpServletRequest request = getHttpServletRequest();
        String value = request.getParameter("value");

        //图片url
        String img = getUTF("img", null);
        //按钮状态 0=有效; 1=无效;
        Integer status = getIntParameter("status", 0);
        //排序
        Integer sort = getIntParameter("sort", 0);
        supplychainHomepageButtonSetting.setType(type);
        supplychainHomepageButtonSetting.setName(name);
        supplychainHomepageButtonSetting.setValue(value);
        supplychainHomepageButtonSetting.setImg(img);
        supplychainHomepageButtonSetting.setStatus(status);
        supplychainHomepageButtonSetting.setSort(sort);
        dataAccessManager.changeSupplychainHomepageButtonSetting(supplychainHomepageButtonSetting);
        return success("修改成功!");
    }

    /**
     * 商家端采购：获取采购首页菜单按钮
     * @author oy
     * @date 2019/4/11
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getSupplychainHomepageButton() {
        //直接获取
        List<SupplychainHomepageButtonSetting> list =  dataAccessManager.getSupplychainHomepageButton();
        return success(list);
    }

    /**
     * 验证按钮是否存在
     * @author oy
     * @date 2019/5/30
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject validateSupplychainHomepageButton() {
        String name = getUTF("name");
        Integer type = getIntParameter("type",1);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        SupplychainHomepageButtonSetting supplychainHomepageButtonSetting =  dataAccessManager.validateSupplychainHomepageButton(name,type);
        if(supplychainHomepageButtonSetting != null){
            jsonObject.put("exist",1);//存在
            jsonObject.put("msg","该按钮已经存在!");//存在
        }else{
            jsonObject.put("exist",0);//不存在
            jsonObject.put("msg","不存在，可以添加");//存在
        }
        return jsonObject;
    }

    /**
     * 批量删除按钮
     * @author oy
     * @date 2019/5/30
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject batchDeleteSupplychainHomepageButton() {
        String ids = getUTF("ids");
        if(ids != null && !ids.equals("")){
            List<String> stringList = JSONObject.parseArray(ids, String.class);
            if(stringList != null && stringList.size() > 0){
                dataAccessManager.batchDeleteSupplychainHomepageButton(stringList);
            }
        }
        return success("删除成功！");
    }

}
