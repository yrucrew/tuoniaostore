package com.tuoniaostore.supplychain.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.good.ShopTypeEnum;
import com.tuoniaostore.commons.constant.supplychain.ChainGoodStatus;
import com.tuoniaostore.commons.constant.supplychain.SupplyChainShopTypeEnum;
import com.tuoniaostore.commons.constant.supplychain.WarehouseType;
import com.tuoniaostore.commons.support.good.*;
import com.tuoniaostore.commons.support.order.OrderGoodSnapshotRemoteService;
import com.tuoniaostore.commons.support.payment.PaymentCurrencyAccountService;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseSupplierSkuRelationship;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.datamodel.vo.supplychain.SupplychainGoodAndShopVO;
import com.tuoniaostore.datamodel.vo.supplychain.SupplychainGoodVo;
import com.tuoniaostore.datamodel.good.*;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodBindRelationship;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainGoodService;
import com.tuoniaostore.supplychain.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@Service("supplychainGoodService")
public class SupplychainGoodServiceImpl extends BasicWebService implements SupplychainGoodService {

    private static final Logger logger = LoggerFactory.getLogger(SupplychainGoodServiceImpl.class);

    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainGood() {
        SupplychainGood supplychainGood = new SupplychainGood();
        //
        String id = getUTF("id", null);
        //对应的模版ID
        String goodTemplateId = getUTF("goodTemplateId", null);
        //自定义商品名字 为空时使用模版名字
        String defineName = getUTF("defineName", null);
        //自定义商品图片 为空时使用模版图片
        String defineImage = getUTF("defineImage", null);
        //默认排序 值越小 越靠前
        Integer defaultSort = getIntParameter("defaultSort", 0);
        //归属仓库ID
        String shopId = getUTF("shopId", null);
        //商品批次
        Integer productBatches = getIntParameter("productBatches", 0);
        //批次编码
        String batchesCode = getUTF("batchesCode", null);
        //当前库存(真实库存) 欠货时为负数
        Integer stock = getIntParameter("stock", 0);
        //挂起数量(已支付但未出库) 订单取消时需回滚至库存
        Integer pendingQuantity = getIntParameter("pendingQuantity", 0);
        //预警数量
        Integer warningQuantity = getIntParameter("warningQuantity", 0);
        //保持数量
        Integer keepQuantity = getIntParameter("keepQuantity", 0);
        //用于展示的超卖数量(超卖+库存)
        Integer oversoldQuantity = getIntParameter("oversoldQuantity", 0);
        //最低的销售数量
        Integer minimumSellCount = getIntParameter("minimumSellCount", 0);
        //调拨中的数量
        Integer allocationQuantity = getIntParameter("allocationQuantity", 0);
        //采购中的数量
        Integer purchaseQuantity = getIntParameter("purchaseQuantity", 0);
        //待调拨数量(存在主仓且读取主仓库存时有效)
        Integer waitAllocationQuantity = getIntParameter("waitAllocationQuantity", 0);
        //当前状态 -1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效
        Integer status = getIntParameter("status", 0);
        //成本价
        Long costPrice = getLongParameter("costPrice", 0);
        //一般销售价
        Long sellPrice = getLongParameter("sellPrice", 0);
        //一般市场价
        Long marketPrice = getLongParameter("marketPrice", 0);
        //促销价
        Long discountPrice = getLongParameter("discountPrice", 0);
        //促销类型 0-不参与促销 1-促销类型 2-特价类型
        Integer discountType = getIntParameter("discountType", 0);
        //限购数量 -1-不限购 0-不出售 其他正数表示限购数量
        Integer restrictionQuantity = getIntParameter("restrictionQuantity", 0);
        //控制超量的情况 0：不可超出限制 1：超出时以最高价购买
        Integer beyondControll = getIntParameter("beyondControll", 0);
        //延迟配送天数
        Integer deliveryDelay = getIntParameter("deliveryDelay", 0);
        //起始销量
        Integer initialSales = getIntParameter("initialSales", 0);
        //实际销量(展示时为：起始销量+实际销量) -- 总出库
        Integer actualSales = getIntParameter("actualSales", 0);
        //总入库数量
        Long totalStorage = getLongParameter("totalStorage", 0);
        //总出库数量
        Long totalOutStorage = getLongParameter("totalOutStorage", 0);
        //商品描述
        String description = getUTF("description", null);
        Integer startingQuantity = getIntParameter("startingQuantity", 0);
        //
        Long relationItemId = getLongParameter("relationItemId", 0);
        supplychainGood.setId(id);
        supplychainGood.setGoodTemplateId(goodTemplateId);
        supplychainGood.setDefineName(defineName);
        supplychainGood.setDefineImage(defineImage);
        supplychainGood.setDefaultSort(defaultSort);
        supplychainGood.setShopId(shopId);
        supplychainGood.setProductBatches(productBatches);
        supplychainGood.setBatchesCode(batchesCode);
        supplychainGood.setStock(stock);
        supplychainGood.setPendingQuantity(pendingQuantity);
        supplychainGood.setWarningQuantity(warningQuantity);
        supplychainGood.setKeepQuantity(keepQuantity);
        supplychainGood.setOversoldQuantity(oversoldQuantity);
        supplychainGood.setMinimumSellCount(minimumSellCount);
        supplychainGood.setAllocationQuantity(allocationQuantity);
        supplychainGood.setPurchaseQuantity(purchaseQuantity);
        supplychainGood.setWaitAllocationQuantity(waitAllocationQuantity);
        supplychainGood.setStatus(status);
        supplychainGood.setCostPrice(costPrice);
        supplychainGood.setSellPrice(sellPrice);
        supplychainGood.setMarketPrice(marketPrice);
        supplychainGood.setDiscountPrice(discountPrice);
        supplychainGood.setDiscountType(discountType);
        supplychainGood.setRestrictionQuantity(restrictionQuantity);
        supplychainGood.setBeyondControll(beyondControll);
        supplychainGood.setDeliveryDelay(deliveryDelay);
        supplychainGood.setInitialSales(initialSales);
        supplychainGood.setActualSales(actualSales);
        supplychainGood.setTotalStorage(totalStorage);
        supplychainGood.setTotalOutStorage(totalOutStorage);
        supplychainGood.setDescription(description);
        supplychainGood.setRelationItemId(relationItemId);
        dataAccessManager.addSupplychainGood(supplychainGood);
        return success();
    }

    @Override
    public JSONObject getSupplychainGood() {
        SupplychainGood supplychainGood = dataAccessManager.getSupplychainGood(getId());
        fullTemplateMsg(supplychainGood);
        fullShopNameMsg(supplychainGood);
        return success(supplychainGood);
    }

    @Override
    public JSONObject getSupplychainGoods() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainGood> supplychainGoods = dataAccessManager.getSupplychainGoods(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainGoodCount(status, name);
        fullListTemplateMsg(supplychainGoods);
        fullListShopNameMsg(supplychainGoods);
        return success(supplychainGoods, count);
    }

    @Override
    public JSONObject getSupplychainGoodAll() {
        List<SupplychainGood> supplychainGoods = dataAccessManager.getSupplychainGoodAll();
        fullListTemplateMsg(supplychainGoods);
        fullListShopNameMsg(supplychainGoods);
        return success(supplychainGoods);
    }

    @Override
    public JSONObject getSupplychainGoodCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainGoodCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainGood() {
        SupplychainGood supplychainGood = dataAccessManager.getSupplychainGood(getId());
        //
        String id = getUTF("id", null);
        //对应的模版ID
        String goodTemplateId = getUTF("goodTemplateId", null);
        //自定义商品名字 为空时使用模版名字
        String defineName = getUTF("defineName", null);
        //自定义商品图片 为空时使用模版图片
        String defineImage = getUTF("defineImage", null);
        //默认排序 值越小 越靠前
        Integer defaultSort = getIntParameter("defaultSort", 0);
        //归属仓库ID
        String shopId = getUTF("shopId", null);
        //商品批次
        Integer productBatches = getIntParameter("productBatches", 0);
        //批次编码
        String batchesCode = getUTF("batchesCode", null);
        //当前库存(真实库存) 欠货时为负数
        Integer stock = getIntParameter("stock", 0);
        //挂起数量(已支付但未出库) 订单取消时需回滚至库存
        Integer pendingQuantity = getIntParameter("pendingQuantity", 0);
        //预警数量
        Integer warningQuantity = getIntParameter("warningQuantity", 0);
        //保持数量
        Integer keepQuantity = getIntParameter("keepQuantity", 0);
        //用于展示的超卖数量(超卖+库存)
        Integer oversoldQuantity = getIntParameter("oversoldQuantity", 0);
        //最低的销售数量
        Integer minimumSellCount = getIntParameter("minimumSellCount", 0);
        //调拨中的数量
        Integer allocationQuantity = getIntParameter("allocationQuantity", 0);
        //采购中的数量
        Integer purchaseQuantity = getIntParameter("purchaseQuantity", 0);
        //待调拨数量(存在主仓且读取主仓库存时有效)
        Integer waitAllocationQuantity = getIntParameter("waitAllocationQuantity", 0);
        //当前状态 -1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效
        Integer status = getIntParameter("status", 0);
        //成本价
        Long costPrice = getLongParameter("costPrice", 0);
        //一般销售价
        Long sellPrice = getLongParameter("sellPrice", 0);
        //一般市场价
        Long marketPrice = getLongParameter("marketPrice", 0);
        //促销价
        Long discountPrice = getLongParameter("discountPrice", 0);
        //促销类型 0-不参与促销 1-促销类型 2-特价类型
        Integer discountType = getIntParameter("discountType", 0);
        //限购数量 -1-不限购 0-不出售 其他正数表示限购数量
        Integer restrictionQuantity = getIntParameter("restrictionQuantity", 0);
        //控制超量的情况 0：不可超出限制 1：超出时以最高价购买
        Integer beyondControll = getIntParameter("beyondControll", 0);
        //延迟配送天数
        Integer deliveryDelay = getIntParameter("deliveryDelay", 0);
        //起始销量
        Integer initialSales = getIntParameter("initialSales", 0);
        //实际销量(展示时为：起始销量+实际销量) -- 总出库
        Integer actualSales = getIntParameter("actualSales", 0);
        //总入库数量
        Long totalStorage = getLongParameter("totalStorage", 0);
        //总出库数量
        Long totalOutStorage = getLongParameter("totalOutStorage", 0);
        //商品描述
        String description = getUTF("description", null);
        //
        Long relationItemId = getLongParameter("relationItemId", 0);
        Integer startingQuantity = getIntParameter("startingQuantity", 0);
        supplychainGood.setId(id);
        supplychainGood.setGoodTemplateId(goodTemplateId);
        supplychainGood.setDefineName(defineName);
        supplychainGood.setDefineImage(defineImage);
        supplychainGood.setDefaultSort(defaultSort);
        supplychainGood.setShopId(shopId);
        supplychainGood.setProductBatches(productBatches);
        supplychainGood.setBatchesCode(batchesCode);
        supplychainGood.setStock(stock);
        supplychainGood.setPendingQuantity(pendingQuantity);
        supplychainGood.setWarningQuantity(warningQuantity);
        supplychainGood.setKeepQuantity(keepQuantity);
        supplychainGood.setOversoldQuantity(oversoldQuantity);
        supplychainGood.setMinimumSellCount(minimumSellCount);
        supplychainGood.setAllocationQuantity(allocationQuantity);
        supplychainGood.setPurchaseQuantity(purchaseQuantity);
        supplychainGood.setWaitAllocationQuantity(waitAllocationQuantity);
        supplychainGood.setStatus(status);
        supplychainGood.setCostPrice(costPrice);
        supplychainGood.setSellPrice(sellPrice);
        supplychainGood.setMarketPrice(marketPrice);
        supplychainGood.setDiscountPrice(discountPrice);
        supplychainGood.setDiscountType(discountType);
        supplychainGood.setRestrictionQuantity(restrictionQuantity);
        supplychainGood.setBeyondControll(beyondControll);
        supplychainGood.setDeliveryDelay(deliveryDelay);
        supplychainGood.setInitialSales(initialSales);
        supplychainGood.setActualSales(actualSales);
        supplychainGood.setTotalStorage(totalStorage);
        supplychainGood.setTotalOutStorage(totalOutStorage);
        supplychainGood.setDescription(description);
        supplychainGood.setRelationItemId(relationItemId);
        supplychainGood.setStartingQuantity(startingQuantity);
        dataAccessManager.changeSupplychainGood(supplychainGood);
        return success();
    }

    /**
     * 供应商：根据具体商品类别id 查找商品id
     *
     * @return
     */
    @Override
    public JSONObject getSupplychainGoodByGoodTypeId() {
        String typeId = getUTF("typeId");//类别id
        SysUser user = getUser();//用户
        String userId = user.getId();
        //获取分页内容 默认size 10，num 1
        int pageNum = getIntParameter("pageNum", 1);//页码
        int pageSize = getIntParameter("pageSize", 10);//页面大小

        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageNum - 1);
        int limitEnd = pageSize;

        //通过用户id 查找一下商店的属性 只有type是 5 8 9才是供应链的
        String shopId = "";
        SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(userId);
        if (supplychainShopPropertiesByUserId != null) {
            Integer type = supplychainShopPropertiesByUserId.getType();
            if (type != SupplyChainShopTypeEnum.SUPPLIER.getKey() && type != SupplyChainShopTypeEnum.WHOLESALE.getKey() && type != SupplyChainShopTypeEnum.MANUFACTURER.getKey()) {
                return fail("没有此权限！");
            } else {
                shopId = supplychainShopPropertiesByUserId.getId();
            }
        } else {
            return fail("没有此权限！");
        }
        if (shopId == null || shopId.equals("")) {//是不是有商店
            return fail("该用户没有商店");
        }

        //获取对应的二级分类
        List<GoodType> goodSecondTypeByTypeId = GoodTypeRemoteService.getGoodSecondTypeByTypeId(typeId, getHttpServletRequest());
        List<String> typeIds = goodSecondTypeByTypeId.stream().map(item -> item.getId()).collect(Collectors.toList());
        List<SupplyChainGoodListVO> good2ByTypeId = getGood2ByTypeId(shopId, typeIds, limitStart, limitEnd, ShopTypeEnum.SUPPLY_CHAIN.getKey());//查询商品信息
        return success(good2ByTypeId);
    }


    /**
     * 供应商:内部方法，用过商店id 类型id获取商品
     *
     * @param shopId, typeId, limitStart, limitEnd
     * @return java.util.List<com.tuoniaostore.supplychain.vo.SupplychainGoodVO>
     * @author oy
     * @date 2019/4/2
     */
    private List<SupplychainGoodVO> getGoodByTypeId(String shopId, String typeId, int limitStart, int limitEnd, int type) {
        //type:区分是供应链查询0 还是门店查询1
        //根据商店id，类别id 去查找对应的商品
        List<SupplychainGoodVO> list = new ArrayList<>();
        List<SupplychainGood> supplychainGoodByUserIdAndShopId = dataAccessManager.getSupplychainGoodByUserIdAndShopId(shopId, typeId, limitStart, limitEnd);
        //通过商品信息 查找SuppliychainPrice 查找具体的价格id 价格id 找对应的条码 和 unitId
        supplychainGoodByUserIdAndShopId.forEach(x -> {
            SupplychainGoodVO vo = new SupplychainGoodVO();
            vo.setStatus(x.getStatus());//状态
            vo.setDefineName(x.getDefineName());//名字
            vo.setDefineImage(x.getDefineImage());//图片
            String id = x.getId();//商品id
            vo.setId(id);//id
            vo.setCostPrice(x.getCostPrice());//单价
            vo.setSiglePrice(x.getSellPrice());//成本价
            vo.setStock(x.getStock());//库存
            vo.setDescribed(x.getDescription());//描述
            //获取当前的价格id
            String priceId = x.getPriceId();
            //获取类别id
            String goodTypeId = x.getGoodTypeId();

            //通过价格id 查找对应的 条形码 和 单位
            GoodPrice goodPriceById = null;
            GoodBarcode goodBarcodeByPriceId = null;
            GoodUnit goodGoodUnitById = null;
            GoodType goodType = null;
            try {
                //查找类别名称
                goodType = GoodTypeRemoteService.getGoodType(goodTypeId, getHttpServletRequest());
                if (goodType != null) {
                    vo.setTypeName(goodType.getTitle());
                }
                //查找当前的价格模板
                goodPriceById = GoodPriceRemoteService.getGoodPriceById(priceId, getHttpServletRequest());//价格
                if (goodPriceById == null) {//去pos表查找
                    if (type == ShopTypeEnum.STORE.getKey()) {//是商家端的
                        goodPriceById = GoodPricePosRemoteService.getGoodPricePos(priceId, getHttpServletRequest());
                    }
                }
                if (goodPriceById != null) {
                    //查找一下 对应的单位unitId
                    String unitId = goodPriceById.getUnitId();
                    String title = goodPriceById.getTitle();
                    vo.setDefineName(vo.getDefineName() + " " + title);//名字整理一下
                    if (unitId != null && !unitId.equals("")) {
                        goodGoodUnitById = GoodUnitRemoteService.getGoodUnitById(unitId, getHttpServletRequest());//单位
                        if (goodGoodUnitById != null) {
                            vo.setTitle(goodGoodUnitById.getTitle());
                        }
                    }
                }
                goodBarcodeByPriceId = GoodBarcodeRemoteService.getGoodBarcodeByPriceId(priceId, getHttpServletRequest());//条形码
                if (goodBarcodeByPriceId != null) {
                    vo.setBarcode(goodBarcodeByPriceId.getBarcode());//条形码
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            list.add(vo);
        });
        return list;
    }

    /**
     * 商家端：根据具体商品类别id 查找同类商品或者单个商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/3/31
     */
    @Override
    public JSONObject getSupplychainGoodByGoodTypeId2() {
        SysUser user = getUser();//用户
        String userId = user.getId();
        String typeId = getUTF("typeId");//类别id
        //获取分页内容 默认size 10，num 1
        int pageSize = getIntParameter("pageSize", 10);//页面大小
        int pageNum = getIntParameter("pageNum", 1);//页码

        //计算获取的数据 开始数据 - 结束数据
        int limitEnd = pageSize;
        int limitStart = pageSize * (pageNum - 1);

        //通过用户id 查找一下商店的属性 只有type是 6 才是供应链的
        String shopId = "";
        SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(userId);

        if (supplychainShopPropertiesByUserId != null) {
            Integer type = supplychainShopPropertiesByUserId.getType();
            if (type != SupplyChainShopTypeEnum.ORDINARY_STORES.getKey() && type != SupplyChainShopTypeEnum.DIRECT_SHOP.getKey()) { // 6 7 是商家端
                return fail("没有此权限！");
            } else {
                shopId = supplychainShopPropertiesByUserId.getId();
            }
        } else {
            return fail("该用户没有商店权限！请联系管理员");
        }
        //获取对应的二级分类
        List<GoodType> goodSecondTypeByTypeId = GoodTypeRemoteService.getGoodSecondTypeByTypeId(typeId, getHttpServletRequest());
        List<String> typeIds = goodSecondTypeByTypeId.stream().map(item -> item.getId()).collect(Collectors.toList());
        List<SupplyChainGoodListVO> good2ByTypeId = getGood2ByTypeId(shopId, typeIds, limitStart, limitEnd, ShopTypeEnum.STORE.getKey());//查询商品信息
        return success(good2ByTypeId);
    }


    /**
     * 商家端:内部方法，用过商店id 类型id获取商品
     *
     * @param shopId, typeId, limitStart, limitEnd
     * @return java.util.List<com.tuoniaostore.supplychain.vo.SupplychainGoodVO>
     * @author oy
     * @date 2019/4/9
     */
    private List<SupplyChainGoodListVO> getGood2ByTypeId(String shopId, List<String> typeId, int limitStart, int limitEnd, int type) {
        //type:区分是供应链查询0 还是门店查询1
        //根据商店id，类别id 去查找对应的商品
        Map<String, List<SupplychainGoodVO>> map = new HashMap<>();//存放有一样的模板id的时候
        List<SupplychainGoodVO> listMap = new ArrayList<>();
        List<SupplychainGood> supplychainGoodByUserIdAndShopId = dataAccessManager.getSupplychainGoodByShopIdAndTypes(shopId, typeId, limitStart, limitEnd);
        //通过商品信息 查找SuppliychainPrice 查找具体的价格id 价格id 找对应的条码 和 unitId
        supplychainGoodByUserIdAndShopId.forEach(x -> {
            SupplychainGoodVO vo = new SupplychainGoodVO();
            vo.setStatus(x.getStatus());//状态
            vo.setDefineName(x.getDefineName());//名字
            vo.setDefineImage(x.getDefineImage());//图片
            String id = x.getId();//商品id
            vo.setId(id);//id
            vo.setCostPrice(x.getCostPrice());//单价
            vo.setSiglePrice(x.getSellPrice());//成本价
            vo.setStock(x.getStock());//库存
            vo.setDescribed(x.getDescription());//描述
            //获取当前的价格id
            String priceId = x.getPriceId();
            //获取类别id
            String goodTypeId = x.getGoodTypeId();

            //通过价格id 查找对应的 条形码 和 单位
            GoodPrice goodPriceById = null;
            List<GoodBarcode> barcodesByPriceId = null;
            GoodUnit goodGoodUnitById = null;
            GoodType goodType = null;
            try {
                //查找类别名称
                if (goodTypeId != null && !goodTypeId.equals("")) {
                    goodType = GoodTypeRemoteService.getGoodType(goodTypeId, getHttpServletRequest());
                }
                if (goodType != null) {
                    vo.setTypeName(goodType.getTitle());
                }

                //查找当前的价格模板
                if (priceId != null && !priceId.equals("")) {
                    goodPriceById = GoodPriceRemoteService.getGoodPriceById(priceId, getHttpServletRequest());//价格
                }
                if (goodPriceById == null) {//去pos表查找
                    if (type == ShopTypeEnum.STORE.getKey()) {//是商家端的
                        goodPriceById = GoodPricePosRemoteService.getGoodPricePos(priceId, getHttpServletRequest());
                    }
                }
                if (goodPriceById != null) {
                    //查找一下 对应的单位unitId
                    String unitId = goodPriceById.getUnitId();
                    String title = goodPriceById.getTitle();
                    if (unitId != null && !unitId.equals("")) {
                        goodGoodUnitById = GoodUnitRemoteService.getGoodUnitById(unitId, getHttpServletRequest());//单位
                        if (goodGoodUnitById != null) {
                            vo.setTitle(goodGoodUnitById.getTitle());
                        }
                    }
                }
                if (priceId != null && !priceId.equals("")) {
                    barcodesByPriceId = GoodBarcodeRemoteService.getBarcodesByPriceId(priceId, getHttpServletRequest());//条形码
                }
                if (barcodesByPriceId != null && barcodesByPriceId.size() > 0) {
                    String collect = barcodesByPriceId.stream().map(GoodBarcode::getBarcode).collect(Collectors.joining(","));
                    vo.setBarcode(collect);//条形码
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            String defineName = x.getDefineName();
            if (map.get(defineName) != null) {//说明里面有
                List<SupplychainGoodVO> supplychainGoods = map.get(defineName);
                supplychainGoods.add(vo);
            } else {
                List<SupplychainGoodVO> supplychainGoods = new ArrayList<>();
                supplychainGoods.add(vo);
                map.put(defineName, supplychainGoods);
            }
        });

        List<SupplyChainGoodListVO> list = new ArrayList<>();
        //遍历map
        map.forEach((x, y) -> {
            SupplyChainGoodListVO v = new SupplyChainGoodListVO();
            v.setTitle(x);
            List<SupplychainGoodVO> vl = new ArrayList<>();
            y.forEach(z -> {
                vl.add(z);
            });
            v.setCommodityList(vl);
            list.add(v);
        });
        return list;
    }

    /**
     * 供应商：编辑商品
     *
     * @return
     */
    @Override
    public JSONObject changeSupplychainGoodByGoodTypeId() {
        //商品id
        String id = getUTF("id");
        //获取修改的成本价
        Long costPrice = getLongParameter("costPrice");
        //获取修改的单价
        Long siglePrice = getLongParameter("siglePrice");
        //获取修改的库存
        Integer stock = getIntParameter("stock");
        //获取修改的描述
        String described = getUTF("described", "");

        //上面条件验证一下
        if (id == null || id.equals("")) {
            return fail("id不能为空!");
        }
        if (costPrice == null) {
            return fail("成本价不能为空!");
        }
        if (siglePrice == null) {
            return fail("单价不能为空!");
        }
        if (stock == null) {
            return fail("库存不能为空!");
        }
        //正常的修改chainGood表就行
        SupplychainGood supplychainGood = new SupplychainGood();
        supplychainGood.setId(id);
        supplychainGood.setCostPrice(costPrice);
        supplychainGood.setSellPrice(siglePrice);
        supplychainGood.setStock(stock);
        supplychainGood.setDescription(described);
        dataAccessManager.changeSupplychainGoodById(supplychainGood);
        return success("修改成功！");
    }


    /**
     * 下架商品
     *
     * @return
     */
    @Override
    public JSONObject downSupplychainGoodByGoodId() {
        String id = getUTF("id");//商品id
        if (id == null || id.equals("")) {
            return fail("id不能为空！");
        }
        //更新成下架的
        dataAccessManager.downSupplychainGoodByGoodId(id, ChainGoodStatus.NOT_NO_SHELVES.getKey());
        return success("商品下架成功！");
    }


    /**
     * 上架商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/3/31
     */
    @Override
    public JSONObject upSupplychainGoodByGoodId() {
        String id = getUTF("id");//商品id
        if (id == null || id.equals("")) {
            return fail("id不能为空！");
        }
        //更新成下架的
        dataAccessManager.downSupplychainGoodByGoodId(id, ChainGoodStatus.SALABLE.getKey());
        return success("商品上架成功！");
    }

    /**
     * 商家：编辑商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/2
     */
    @Override
    public JSONObject changeSupplychainGoodByGoodTypeId2() {
        //获取参数
        SysUser user = getUser();
        String userId = user.getId();

        //商品id
        String id = getUTF("id");
        //获取修改的成本价
        Long costPrice = getLongParameter("costPrice");
        //获取修改的单价
        Long siglePrice = getLongParameter("siglePrice");
        //商品图片
        String goodPic = getUTF("goodPic", null);
        //获取修改的描述
        String described = getUTF("described", "");
        //获取修改的库存
        Integer stock = getIntParameter("stock");

        String defineName = getUTF("defineName");//商品名称
        String typeName = getUTF("typeName");//类别
        String unit = getUTF("unit");//单位
        String title = getUTF("title");//价格标签
        Integer status = getIntParameter("status");//上下架状态 0未上架 1上架


        //上面条件验证一下
        if (id == null || id.equals("")) {
            return fail("id不能为空!");
        }
        if (siglePrice == null) {
            return fail("单价不能为空!");
        }
        if (costPrice == null) {
            return fail("成本价不能为空!");
        }
        if (stock == null) {
            return fail("库存不能为空!");
        }
        if (typeName == null || typeName.equals("")) {
            return fail("类别不能为空!");
        }
        if (unit == null || unit.equals("")) {
            return fail("单位不能为空!");
        }
        if (title == null || title.equals("")) {
            return fail("价格标签不能为空!");
        }
        if (status == null) {
            return fail("上下架状态不能为空!");
        }

        SupplychainGoodVO goodByGoodId = getGoodByGoodId(id);//获取商品的详细信息
        String templateId = "";
        String unitId = "";
        String typeId = "";
        String priceId = "";

        //分类id
        String typeName2 = goodByGoodId.getTypeName();
        typeId = goodByGoodId.getTypeId();
        if (typeName2 != null && !typeName2.equals("")) {
            if (!typeName2.equals(typeName)) {
                GoodType goodTypeByTitle = GoodTypeRemoteService.getGoodTypeByTitle(typeName, getHttpServletRequest());
                if (goodTypeByTitle != null) {
                    typeId = goodTypeByTitle.getId();
                } else {//新建一个
                    GoodType goodType = new GoodType();
                    typeId = goodType.getId();
                    goodType.setTitle(typeName);
                    goodType.setStatus(0);
                    goodType.setType(1);
                    try {
                        GoodTypeRemoteService.addGoodTypes(goodType, getHttpServletRequest());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        //模板id
        String defineName1 = goodByGoodId.getDefineName();
        templateId = goodByGoodId.getTemplateId();
        if (!defineName1.equals(defineName)) {//说明不一样
            //查找该新的商品名字是否有模板 没有则新建
            try {
                GoodTemplate goodTemplate = GoodTemplateRemoteService.getGoodTemplateByName(defineName1, getHttpServletRequest());
                if (goodTemplate != null) {
                    templateId = goodTemplate.getId();
                } else {//新建一个
                    GoodTemplate goodTemplate1 = new GoodTemplate();
                    templateId = goodTemplate1.getId();
                    goodTemplate1.setName(defineName);
                    goodTemplate1.setTypeId(typeId);//typeId
                    GoodTemplateRemoteService.addGoodTemplate(goodTemplate1, getHttpServletRequest());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //单位id
        String unit1 = goodByGoodId.getUnit();
        unitId = goodByGoodId.getUnitId();

        if (unit1 != null) {
            if (!unit1.equals(unit)) {//改了单位
                try {
                    GoodUnit goodUnitByTitle = GoodUnitRemoteService.getGoodUnitByTitle(unit, getHttpServletRequest());
                    if (goodUnitByTitle != null) {
                        unitId = goodUnitByTitle.getId();
                    } else {//添加模板
                        GoodUnit goodUnit = new GoodUnit();
                        unitId = goodUnit.getId();
                        goodUnit.setTitle(unit);
                        goodUnit.setUserId(userId);
                        GoodUnitRemoteService.addGoodUnit(goodUnit, getHttpServletRequest());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        priceId = goodByGoodId.getPriceId();//价格id

        //修改
        try {
            GoodPrice goodPriceById = GoodPriceRemoteService.getGoodPriceById(priceId, getHttpServletRequest());//从第一个表查找
            if (goodPriceById != null) {//查询是哪个数据表的
                GoodPriceRemoteService.updateGoodPrice(title, priceId, unitId, templateId, getHttpServletRequest());//更新价格表
            } else {
                GoodPricePosRemoteService.updateGoodPrice(title, priceId, unitId, templateId, getHttpServletRequest());//更新价格表
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //修改一下chain_good表 template_id price_id good_type_id 然后就是其他的一些信息
        //costPrice siglePrice goodPic described stock defineName
        SupplychainGood supplychainGood = new SupplychainGood();
        supplychainGood.setId(id);
        supplychainGood.setGoodTemplateId(templateId);
        supplychainGood.setPriceId(priceId);
        supplychainGood.setGoodTypeId(typeId);

        supplychainGood.setSellPrice(siglePrice);
        supplychainGood.setCostPrice(costPrice);
        supplychainGood.setDefineImage(goodPic);
        supplychainGood.setDescription(described);
        supplychainGood.setStock(stock);
        supplychainGood.setDefineName(defineName);
        supplychainGood.setStatus(status);
        dataAccessManager.updateChainGood(supplychainGood);

        //绑定分类和用户的关系表 先查询 看是否有 如果没有则新增
        //typeId
        SupplychainGoodBindRelationship supplychainGoodBindRelationship = dataAccessManager.getSupplychainGoodBindRelationshipByUserIdAndTypeId(userId, typeId);
        if (supplychainGoodBindRelationship == null) {//说明这个分类之前没有绑定过 要新增的
            SupplychainGoodBindRelationship entity = new SupplychainGoodBindRelationship();
            entity.setUserId(userId);
            entity.setGoodTypeId(typeId);
            dataAccessManager.addSupplychainGoodBindRelationship(entity);
        }
        return success("修改成功!");
    }

    /**
     * 商家：添加商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/2
     */
    @Override
    public JSONObject addSupplychainGoodByUser() {
        /**
         *  逻辑：
         *  添加商品的时候 需要注意
         *     1.是否默认商品名字模板
         *     2.价格模板是否默认
         *     3.商品类型是否默认
         *     4.商品单位是否默认
         */
        //获取用户
        SysUser user = getUser();
        String userId = user.getId();
        //获取商品的详细信息 前端传递一个list过来
        String list = getUTF("list");
        List<GoodMessageAddVO> goodMessageAddVOS = JSONObject.parseArray(list, GoodMessageAddVO.class);
        if (goodMessageAddVOS != null && goodMessageAddVOS.size() > 0) {
            //当前用户的店铺信息
            String[] shopId = new String[1];
            SupplychainShopProperties supplychainGoodBindRelationship = null;
            try {
                supplychainGoodBindRelationship = dataAccessManager.getSupplychainShopPropertiesByUserId(userId);
                shopId[0] = supplychainGoodBindRelationship.getId();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //弄几个list 保存着是否一致的请求内容，防止循环里添加重复的自定义模板
            Map<String, String> map1 = new HashMap<>();
            Map<String, String> map2 = new HashMap<>();
            Map<String, String> map3 = new HashMap<>();
            Map<String, String> map4 = new HashMap<>();

            goodMessageAddVOS.forEach((GoodMessageAddVO vo) -> {
                String goodPic = vo.getGoodPic();//商品图片
                String goodName = vo.getGoodName();//商品名字
                String goodId = vo.getGoodId(); //商品模板id
                String goodType = vo.getGoodType(); //商品分类
                String goodTypeId = vo.getGoodTypeId(); //商品类别id
                Long goodCostPrice = vo.getGoodCostPrice(); //商品进价
                Long goodSalePrice = vo.getGoodSalePrice(); //商品售价
                String goodDescribe = vo.getGoodDescribe(); //商品描述

                String goodBarcode = vo.getGoodBarcode(); //商品条码
                String goodPriceName = vo.getGoodPriceName(); //标签名称
                String priceId = vo.getPriceId(); //标签名称id
                String goodUnit = vo.getGoodUnit(); //商品单位
                String goodUnitId = vo.getGoodUnitId(); //商品单位
                Integer stock = vo.getStock(); //库存
                Integer status = vo.getStatus(); //上下架状态

                //分类
                //判断是否是模板
                if (goodTypeId == null || goodTypeId.equals("")) {//typeId 为空的情况下
                    //首先判断是否已经加入了模板
                    if (map1.get(goodType) == null) {//说明之前的循环还没添加
                        //判断模板库里面有没有这个模板
                        GoodType goodTypeByTitle = GoodTypeRemoteService.getGoodTypeByTitle(goodType, getHttpServletRequest());
                        if (goodTypeByTitle != null) {
                            goodTypeId = goodTypeByTitle.getId();
                            map1.put(goodType, goodTypeId);
                        } else {//说明没有用默认模板，需要远程去添加模板
                            GoodType goodType1 = new GoodType();
                            goodTypeId = goodType1.getId();
                            //为了防止重复添加
                            map1.put(goodType, goodTypeId);
                            goodType1.setStatus(0);
                            goodType1.setTitle(goodType);
                            goodType1.setUserId(userId);
                            goodType1.setType(1);
                            try {
                                GoodTypeRemoteService.addGoodTypes(goodType1, getHttpServletRequest());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        goodTypeId = map1.get(goodType);
                    }
                }

                //先获取条码 是否有 有的话 直接获取对应的价格id unitId 模板id
                GoodBarcode goodBarcodeByBarcode = GoodBarcodeRemoteService.getGoodBarcodeByBarcode(goodBarcode, getHttpServletRequest());
                if (goodBarcodeByBarcode != null && !goodBarcodeByBarcode.equals("")) {
                    //2019-04-11改动 条码必须唯一 说明有商品了 所以价格id unitId goodId都是固定的 只需要往chain_good表中添加数据 一个templateId 一个priceId 一个goodTypeId
                    priceId = goodBarcodeByBarcode.getPriceId();//获取价格id
                    //通过价格id 获取对应的 模板id unitId
                    GoodPrice goodPricePos = null;
                    try {
                        goodPricePos = GoodPricePosRemoteService.getGoodPricePos(priceId, getHttpServletRequest());
                        if (goodPricePos == null) {
                            goodPricePos = GoodPriceRemoteService.getGoodPriceById(priceId, getHttpServletRequest());
                        }
                        if (goodPricePos != null) {
                            goodId = goodPricePos.getGoodId();//商品模板id
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {//自定义添加 说明没有条码
                    if (goodId == null || !goodId.equals("")) {
                        if (map2.get(goodName) == null) {
                            GoodTemplate goodTemplateByName = GoodTemplateRemoteService.getGoodTemplateByName(goodName, getHttpServletRequest());
                            if (goodTemplateByName != null) {//说明有这个模板
                                goodId = goodTemplateByName.getId();
                            } else {
                                //说明不是用的模板
                                GoodTemplate goodTemplate = new GoodTemplate();
                                goodId = goodTemplate.getId();
                                //为了防止重复添加
                                map2.put(goodName, goodId);
                                goodTemplate.setName(goodName);
                                goodTemplate.setTypeId(goodTypeId);
                                goodTemplate.setUserId(userId);
                                try {
                                    GoodTemplateRemoteService.addGoodTemplate(goodTemplate, getHttpServletRequest());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            goodId = map2.get(goodName);
                        }
                    }

                    if (goodUnitId == null || !goodUnitId.equals("")) {
                        if (map3.get(goodType) == null) {
                            try {
                                GoodUnit goodUnitByTitle = GoodUnitRemoteService.getGoodUnitByTitle(goodUnit, getHttpServletRequest());//查找是否有这个单位
                                if (goodUnitByTitle == null) {
                                    //说明不是用的单位模板
                                    GoodUnit goodUnit1 = new GoodUnit();
                                    goodUnitId = goodUnit1.getId();
                                    //为了防止重复添加
                                    map3.put(goodType, goodUnit1.getId());
                                    goodUnit1.setStatus(0);
                                    goodUnit1.setTitle(goodType);
                                    try {
                                        GoodUnitRemoteService.addGoodUnit(goodUnit1, getHttpServletRequest());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    goodUnitId = goodUnitByTitle.getId();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            map3.get(goodType);
                        }
                    }

                    if (priceId == null || !priceId.equals("")) {
                        if (map4.get(goodPriceName) == null) {
                            //查找是否有这个价格模板 通过title和商品模板id 去查找
                            GoodPrice allGoodPriceByTitleAndGoodId = GoodPricePosRemoteService.getAllGoodPriceByTitleAndGoodId(goodPriceName, goodId, getHttpServletRequest());
                            if (allGoodPriceByTitleAndGoodId != null) {//说明有模板了
                                priceId = allGoodPriceByTitleAndGoodId.getId();
                            } else {
                                //说明不是用的价格模板
                                GoodPrice goodPrice = new GoodPrice();
                                priceId = goodPrice.getId();
                                //为了防止重复添加
                                map4.put(goodPriceName, goodPrice.getId());
                                goodPrice.setTitle(goodPriceName);
                                goodPrice.setGoodId(goodId);
                                goodPrice.setCostPrice(goodCostPrice);
                                goodPrice.setSalePrice(goodSalePrice);
                                goodPrice.setUnitId(goodUnitId);
                                try {
                                    GoodPriceRemoteService.addGoodPrice(goodPrice, getHttpServletRequest());//添加价格id
                                    GoodBarcode goodBarcode1 = new GoodBarcode();
                                    goodBarcode1.setBarcode(goodBarcode);//条码
                                    goodBarcode1.setPriceId(goodPrice.getId());
                                    GoodBarcodeRemoteService.addGoodBarcodeByBarCode(goodBarcode1, getHttpServletRequest());//同时需要添加对应的条码
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            priceId = map4.get(goodPriceName);
                        }
                    }
                }

                //添加商品列表
                SupplychainGood supplychainGood = new SupplychainGood();
                supplychainGood.setDefineImage(goodPic);//图片
                supplychainGood.setGoodTemplateId(goodId);//商品模板id
                supplychainGood.setGoodTypeId(goodTypeId);//商品分类
                supplychainGood.setDefineName(goodName);//商品名字
                supplychainGood.setCostPrice(goodCostPrice);//商品进价
                supplychainGood.setSellPrice(goodSalePrice); //商品售价
                supplychainGood.setDescription(goodDescribe);//商品描述
                supplychainGood.setShopId(shopId[0]);
                supplychainGood.setPriceId(priceId);
                supplychainGood.setGoodReturn(0);
                supplychainGood.setStock(stock);
                supplychainGood.setStatus(status);
                dataAccessManager.addSupplychainGood(supplychainGood);
                //绑定分类和用户的关系
                SupplychainGoodBindRelationship supplychainGoodBindRelationship1 = new SupplychainGoodBindRelationship();
                supplychainGoodBindRelationship1.setUserId(userId);
                supplychainGoodBindRelationship1.setGoodTypeId(goodTypeId);
                dataAccessManager.addSupplychainGoodBindRelationship(supplychainGoodBindRelationship1);
            });
        }
        return success("添加成功！");
    }

    /**
     * 商家端：编辑页面数据获取
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/8
     */
    @Override
    public JSONObject getSupplychainGoodByGoodId() {
        String id = getUTF("id");//商品id
        if (id == null || id.equals("")) {
            return fail("id不能为空");
        }
        SupplychainGoodVO goodByGoodId = getGoodByGoodId(id);
        return success(goodByGoodId);
    }

    /**
     * 商家：删除商品信息
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/10
     */
    @Override
    public JSONObject delSupplychainGoodByGoodId() {
        String id = getUTF("id");//商品id
        if (id == null || id.equals("")) {
            return fail("商品id不能为空！");
        }
        //删除商品信息
        dataAccessManager.delSupplychainGoodById(id);
        return success("删除商品成功！");
    }

    /**
     * 商家：搜索商品信息(模糊查询)
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/10
     */
    @Override
    public JSONObject searchSupplychainGoodByGoodId() {
        String goodName = getUTF("goodName");//商品名称 or 条码
        if (goodName == null) {
            return success();
        }
        //分页参数
        int pageNum = getIntParameter("pageNum", 1);//页码
        int pageSize = getIntParameter("pageSize", 10);//页面大小

        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageNum - 1);
        int limitEnd = pageSize;

        SysUser user = getUser();

        //根据用户id 查找对应的商品信息
        SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(user.getId());
        String shopId = null;
        if (supplychainShopPropertiesByUserId != null) {
            shopId = supplychainShopPropertiesByUserId.getId();
        } else {
            return success();
        }
        //通过商店id，商品名称 模糊查询 获取对应的商品
        List<SupplychainGood> supplychainGoods = dataAccessManager.searchSupplychainGoodByShopIdAndGoodName(shopId, goodName, limitStart, limitEnd);
        //通过条形码 查找对应的价格标签 通过相关的价格标签和商店id 查找符合的商品
        if (supplychainGoods != null && supplychainGoods.size() > 0) {
            return success(supplychainGoods);
        }
        //远端调用goodBarcode
        GoodBarcode goodBarcodeByBarcode = GoodBarcodeRemoteService.getGoodBarcodeByBarcode(goodName, getHttpServletRequest());
        //获取当前的 priceId 去获取对应的商品
        if (goodBarcodeByBarcode != null) {
            String priceId = goodBarcodeByBarcode.getPriceId();
            List<String> list = new ArrayList<>();
            list.add(priceId);
            if (list != null) {
                //查询所有的商品
                List<SupplychainGood> supplychainGoods1 = dataAccessManager.searchSupplychainGoodByShopIdAndPriceId(shopId, list, limitStart, limitEnd);
                if (supplychainGoods1 != null && supplychainGoods1.size() > 0) {
                    return success(supplychainGoods1);
                }
            }
        }
        return success();
    }

    /**
     * 根据当前商品id 获取商品的详细信息
     *
     * @param id
     * @return com.tuoniaostore.supplychain.vo.SupplychainGoodVO
     * @author oy
     * @date 2019/4/9
     */
    private SupplychainGoodVO getGoodByGoodId(String id) {
        List<GoodShopCart> goodShopCartListByUserId = GoodShopCartRemoteService.getGoodShopCartListByUserId(getUser().getId(), getHttpServletRequest());
        //通过商品id 获取当前数据
        SupplychainGood supplychainGood = dataAccessManager.getSupplychainGood(id);
        SupplychainGoodVO vo = new SupplychainGoodVO();
        if (supplychainGood != null) {
            String priceId = supplychainGood.getPriceId();//价格id
            String goodTypeId = supplychainGood.getGoodTypeId();//类别id
            GoodType goodType = null;
            GoodPrice goodPriceById = null;
            //获取分类
            try {
                goodType = GoodTypeRemoteService.getGoodType(goodTypeId, getHttpServletRequest());//类别id
                if (goodType != null) {
                    vo.setTypeName(goodType.getTitle());
                    vo.setTypeId(goodType.getId());
                }
                goodPriceById = GoodPriceRemoteService.getGoodPriceById(priceId, getHttpServletRequest());//价格id
                //获取单位
                if (goodPriceById != null) {
                    String unitId = goodPriceById.getUnitId();
                    String id1 = goodPriceById.getId();//价格id
                    vo.setTitle(goodPriceById.getTitle());
                    vo.setPriceId(id1);
                    String icon = goodPriceById.getIcon();
                    vo.setPricePic(icon);
                    try {
                        GoodUnit goodUnitById = GoodUnitRemoteService.getGoodUnitById(unitId, getHttpServletRequest()); //获取一下 unit
                        List<GoodBarcode> goodBarcodeByPriceId = GoodBarcodeRemoteService.getBarcodesByPriceId(id1, getHttpServletRequest());//条码
                        if (goodUnitById != null) {
                            vo.setUnit(goodUnitById.getTitle());
                            vo.setUnitId(goodUnitById.getId());
                        }
                        if (goodBarcodeByPriceId != null) {
                            vo.setBarcode(goodBarcodeByPriceId.stream().map(GoodBarcode::getBarcode).collect(Collectors.joining(",")));//设置条码 以逗号隔开 成字符串
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            vo.setTemplateId(supplychainGood.getGoodTemplateId());//模板id
            vo.setId(supplychainGood.getId());
            vo.setDefineImage(supplychainGood.getDefineImage());//图片
            vo.setDefineName(supplychainGood.getDefineName());//名称
            vo.setCostPrice(supplychainGood.getCostPrice());//进价
            vo.setSiglePrice(supplychainGood.getSellPrice());//销售价
            vo.setStatus(supplychainGood.getStatus());//上下架状态
            vo.setStock(supplychainGood.getStock());//库存
            vo.setDescribed(supplychainGood.getDescription());//描述
            vo.setStartingQuantity(supplychainGood.getStartingQuantity());//起订量
            if(goodShopCartListByUserId != null && goodShopCartListByUserId.size() > 0){
                Optional<GoodShopCart> first = goodShopCartListByUserId.stream().filter(item -> item.getGoodId().equals(supplychainGood.getId())).findFirst();
                if(first.isPresent()){
                    GoodShopCart goodShopCart = first.get();
                    vo.setCarNum(goodShopCart.getGoodNumber());
                }
            }
        }
        return vo;
    }


    /**
     * 删除商品
     *
     * @return
     */
    @Override
    public JSONObject deleteSupplychainGoodByGoodId() {
        String id = getUTF("id");//商品id
        if (id == null || id.equals("")) {
            return fail("id不能为空！");
        }
        //更新成下架的
        dataAccessManager.downSupplychainGoodByGoodId(id, ChainGoodStatus.DELETE.getKey());
        return success("商品删除成功！");
    }

    /**
     * 填充模块信息（list）
     */
    private void fullListTemplateMsg(List<SupplychainGood> supplychainGood) {
        int size = supplychainGood.size();
        if (supplychainGood != null && size > 0) {
            for (int i = 0; i < size; i++) {
                SupplychainGood supplychainBanner = supplychainGood.get(i);
                if (supplychainBanner != null) {
                    String goodTemplateId = supplychainBanner.getGoodTemplateId();
                    try {
                        GoodTemplate goodTemplate = GoodTemplateRemoteService.getGoodTemplate(goodTemplateId, getHttpServletRequest());
                        if (goodTemplate != null) {
                            supplychainBanner.setShopName(goodTemplate.getName());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 填充模块信息
     */
    private void fullTemplateMsg(SupplychainGood supplychainGood) {
        if (supplychainGood != null) {
            String goodTemplateId = supplychainGood.getGoodTemplateId();
            try {
                GoodTemplate goodTemplate = GoodTemplateRemoteService.getGoodTemplate(goodTemplateId, getHttpServletRequest());
                if (goodTemplate != null) {
                    supplychainGood.setShopName(goodTemplate.getName());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 填充商店名称(List)
     */
    private void fullListShopNameMsg(List<SupplychainGood> supplychainGood) {
        int size = supplychainGood.size();
        if (supplychainGood != null && size > 0) {
            for (int i = 0; i < size; i++) {
                SupplychainGood supplychainBanner = supplychainGood.get(i);
                if (supplychainBanner != null) {
                    String shopId = supplychainBanner.getShopId();
                    SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(shopId);
                    if (supplychainShopProperties != null) {
                        supplychainBanner.setShopName(supplychainShopProperties.getShopName());
                    }
                }
            }
        }
    }

    /**
     * 填充商店名称
     */
    private void fullShopNameMsg(SupplychainGood supplychainGood) {
        if (supplychainGood != null) {
            String shopId = supplychainGood.getShopId();
            SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(shopId);
            if (supplychainShopProperties != null) {
                supplychainGood.setShopName(supplychainShopProperties.getShopName());
            }
        }
    }

    /**
     * 根据商品ID查询商品和该商品购物车数量
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/4/8
     */
    @Override
    public JSONObject getSupplychainGoodByIdAndShopNumber() {
        String ids = getUTF("ids");
        String[] split = ids.split(",");
        String goodNumbers = getUTF("jsonString");
        JSONObject jasonObject = JSONObject.parseObject(goodNumbers);
        Map map = (Map) jasonObject;

        String mapTime = getUTF("mapTime");
        Map mapTimeMap = JSONObject.parseObject(mapTime, Map.class);

        List<String> list = Arrays.asList(split);
//        List<SupplychainGood> goodById = dataAccessManager.getSupplychainGoodByIdPage(list, null, null);//要查询所有的 失效的的要表明在购物车中
        List<SupplychainGood> goodById = dataAccessManager.getSupplychainGoodByIdForShopCart(list, null, null);//要查询所有的 失效的的要表明在购物车中

        ArrayList<SupplychainGoodAndShopVO> andShopV = new ArrayList<>();
        /*ArrayList<String> listType = new ArrayList<>();
        listType.add(String.valueOf(WarehouseType.SUPPLIER.getKey()));
        listType.add(String.valueOf(WarehouseType.WHOLESALE.getKey()));
        listType.add(String.valueOf(WarehouseType.MANUFACTURER.getKey()));*/
        //查询所有仓库
        List<SupplychainShopProperties> supplychainShopProperties = dataAccessManager.getSupplychainShopPropertiesByType(null,null,null);
        ArrayList<SupplychainGoodVo> goodVoList = new ArrayList<>();

        //查询商品数据


        for (SupplychainGood good : goodById) {
            try {
                SupplychainGoodVo goodVo = new SupplychainGoodVo();
                //商品价格
                GoodPrice price = GoodPriceRemoteService.getGoodPriceById(good.getPriceId(), getHttpServletRequest());
                //商品规格，单位
                GoodUnit unit = new GoodUnit();
                if (price != null) {
                    unit = GoodUnitRemoteService.getGoodUnitById(price.getUnitId(), getHttpServletRequest());
                    goodVo.setGoodTitle(price.getTitle());
                }
                if (unit != null) {
                    goodVo.setGoodUnit(unit.getTitle());
                }
                //商品在购物车的数量
                Integer goodNumber = (Integer) map.get(good.getId());
                Long date = (Long)mapTimeMap.get(good.getId());
                Date date1 = new Date(date);
                BeanUtils.copyProperties(good, goodVo);
                goodVo.setGoodNumber(goodNumber);
                goodVo.setCreateTime(date1);
                goodVo.setStatus(good.getStatus()); //-1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效
                goodVo.setStartingQuantity(good.getStartingQuantity());//起订金额
                String priceId = good.getPriceId();
                if(priceId != null && !priceId.equals("")){
                    GoodPrice goodPriceById = GoodPriceRemoteService.getGoodPriceById(priceId, getHttpServletRequest());
                    if(goodPriceById != null){
                        String icon = goodPriceById.getIcon();
                        goodVo.setGoodPricePic(icon);//标签图片
                    }
                }
                //先封装好商品的规格和单位 --》》 再查询商品对应供应商
                goodVoList.add(goodVo);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        //封装供应商数据
        supplychainShopProperties.forEach(x -> {
            String id = x.getId();
            List<SupplychainGoodVo> collect = goodVoList.stream().filter(item -> item.getShopId().equals(id)).sorted(Comparator.comparing(SupplychainGoodVo::getCreateTime).reversed()).collect(Collectors.toList());
            if (collect.size() > 0) {
                SupplychainGoodAndShopVO supplychainGoodAndShopVO = new SupplychainGoodAndShopVO();
                supplychainGoodAndShopVO.setList(collect);
                supplychainGoodAndShopVO.setShopName(x.getShopName());
                supplychainGoodAndShopVO.setDistributionCost(x.getDistributionCost());
                supplychainGoodAndShopVO.setShopId(id);
                supplychainGoodAndShopVO.setShopType(x.getType());
                supplychainGoodAndShopVO.setParentId(x.getParentId());
                supplychainGoodAndShopVO.setMoq(x.getMoq());
                andShopV.add(supplychainGoodAndShopVO);
            }
        });
        return success(andShopV);
    }

    @Override
    public JSONObject getSupplychainGoodByIds() {
        //获取到ids
        String ids = getUTF("ids");

        //转成list
        List<String> strings = JSONObject.parseArray(ids, String.class);
        List<SupplychainGood> list = new ArrayList<>();
        if (strings != null && strings.size() > 0) {
            list = dataAccessManager.getSupplychainGoodById(strings, null, null);
        }
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("list", list);
        jsonObject.put("code", 0);
        return jsonObject;
    }


    /**
     * 通过商店id 查找对应的商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/23
     */
    @Override
    public JSONObject getSupplychainGoodByShopId() {
        String shopId = getUTF("shopId");
        if (shopId == null || shopId.equals("")) {
            return fail("商店id不能为空");
        }
        List<SupplychainGood> supplychainGoodByShopIdAndTypes = dataAccessManager.getSupplychainGoodByShopIdAndTypes(shopId, null, null, null);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list", supplychainGoodByShopIdAndTypes);
        return jsonObject;
    }

    /**
     * 添加仓库和商品的绑定关系
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/22
     */
    @Override
    public JSONObject addSupplychainGoodWithShopRelation() {
        //前提 需要选择一个仓库 进行操作
        String shopId = getUTF("shopId");

        if (shopId == null || shopId.equals("")) {
            return fail("商店ID不能为空!");
        }
        //查找商店是否存在
        SupplychainShopProperties supplychainShopPropertiesById = dataAccessManager.getSupplychainShopPropertiesById(shopId);
        if (supplychainShopPropertiesById == null) {
            return fail("无该商店信息,请联系管理员！");
        }

        // 1.首先选择商品的模板  然后有模板id 之后 会下拉出来 该商品的所有价格标签 然后选中一个价格标签
        String goodTemplateIds = getUTF("goodTemplateIds");//前端传数组过来
        List<GoodTemplate> goodTemplateList = JSONObject.parseArray(goodTemplateIds, GoodTemplate.class);
        if (goodTemplateList == null || goodTemplateList.size() == 0) {
            return fail("请选择商品!");
        }

        List<SupplychainGood> supplychainGoodSaveList = new ArrayList<>();
        List<SupplychainGoodBindRelationship> supplychainGoodBindRelationshipSaveList = new ArrayList<>();
        if (goodTemplateList != null && goodTemplateList.size() > 0) {
            goodTemplateList.forEach(x -> {
                List<GoodPrice> list = x.getList();
                if (list != null && list.size() > 0) {
                    list.forEach(y -> {
                        SupplychainGood supplychainGood = new SupplychainGood();
                        supplychainGood.setGoodTemplateId(x.getId());//模板id
                        supplychainGood.setDefineName(x.getName());//名称
                        supplychainGood.setDefineImage(x.getImageUrl());//图片
                        supplychainGood.setShopId(shopId);//商店id

                        supplychainGood.setGoodTypeId(x.getTypeId());//类别id
                        supplychainGood.setDescription(x.getDescription());//描述

                        supplychainGood.setPriceId(y.getId());//价格id
                        supplychainGood.setCostPrice(y.getCostPrice());//成本价
                        supplychainGood.setSellPrice(y.getSellPrice());//销售价
                        supplychainGood.setMarketPrice(y.getMarketPrice());//市场价
                        supplychainGood.setDiscountPrice(y.getDiscountPrice());//折扣价
                        supplychainGoodSaveList.add(supplychainGood);

                        //添加商品分类和商店的分类绑定
                        SupplychainGoodBindRelationship supplychainGoodBindRelationship = new SupplychainGoodBindRelationship();
                        supplychainGoodBindRelationship.setGoodTypeId(x.getTypeId());
                        supplychainGoodBindRelationship.setUserId(supplychainShopPropertiesById.getUserId());
                        supplychainGoodBindRelationshipSaveList.add(supplychainGoodBindRelationship);
                    });
                }
            });
            //分类和用户绑定 需要验证通过
            List<SupplychainGoodBindRelationship> supplychainGoodBindRelationshipByUserId =
                    dataAccessManager.getSupplychainGoodBindRelationshipByUserId(supplychainShopPropertiesById.getUserId(), null, null);
            if (supplychainGoodBindRelationshipByUserId != null && supplychainGoodBindRelationshipByUserId.size() > 0) {
                List<String> collect = supplychainGoodBindRelationshipByUserId.stream().map(SupplychainGoodBindRelationship::getGoodTypeId).collect(Collectors.toList());
                List<SupplychainGoodBindRelationship> collect1 = supplychainGoodBindRelationshipSaveList.stream().filter(item -> !collect.contains(item.getGoodTypeId())).collect(Collectors.toList());
                //保存局部的
                if (collect1 != null && collect1.size() > 0) {
                    dataAccessManager.addSupplychainGoodBindRelationships(collect1);//保存
                }
            } else {
                if (supplychainGoodBindRelationshipSaveList != null && supplychainGoodBindRelationshipSaveList.size() > 0) {
                    //全部保存
                    dataAccessManager.addSupplychainGoodBindRelationships(supplychainGoodBindRelationshipSaveList);//保存
                }
            }
            //保存商品
            if (supplychainGoodSaveList != null && supplychainGoodSaveList.size() > 0) {
                dataAccessManager.addSupplychainGoods(supplychainGoodSaveList);
            }
        }
        return success("保存成功!");
    }


    /**
     * 按仓库名 商品名 或者商品条码搜索仓库商品详情（模糊查询）
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/24
     */
    @Override
    public JSONObject searchSupplychainGoodBySHOrNameOrBarcode() {
        //仓库名
        String storeHourseName = getUTF("storeHourseName", null);
        //商品名或者条码
        String goodNameOrBarcode = getUTF("goodNameOrBarcode", null);//如果没有填写的话 传空过来

        List<Integer> types = new ArrayList<>();
        types.add(SupplyChainShopTypeEnum.STRAIGHT_BARN.getKey());
        types.add(SupplyChainShopTypeEnum.VIRTUAL_WAREHOUSE.getKey());
        types.add(SupplyChainShopTypeEnum.THIRD_PARTY_WAREHOUSE.getKey());
        types.add(SupplyChainShopTypeEnum.COOPERATIVE_WAREHOUSE.getKey());
        types.add(SupplyChainShopTypeEnum.AGENCY_WAREHOUSE.getKey());

        if (storeHourseName == null && goodNameOrBarcode == null) {//两个没都传
            //查询所有的商品
            List<SupplychainGood> supplychainGoodAll = dataAccessManager.getSupplychainGoodAllByPage(getPageStartIndex(getPageSize()), getPageSize());
            //补充一下数据 条码 商品名称 商品id 仓库名称
            List<SupplychainGoodForInDetails> supplychainGoodForInDetails = fillDetailForInOut(supplychainGoodAll, null);
            return success(supplychainGoodForInDetails);//返回
        } else if (storeHourseName == null && goodNameOrBarcode != null) {
            List<GoodBarcode> goodBarcodeByBarcode = GoodBarcodeRemoteService.getGoodBarcodeByBarcodesLike(goodNameOrBarcode, getHttpServletRequest());//查询条码
            if (goodBarcodeByBarcode != null && goodBarcodeByBarcode.size() > 0) {
                List<String> priceIds = goodBarcodeByBarcode.stream().map(GoodBarcode::getPriceId).collect(Collectors.toList());
                //通过priceId 去获取所有的数据
                List<SupplychainGood> supplychainGoods = dataAccessManager.getsupplychaingoodByShopIdAndPriceId(null, priceIds, null, null, getPageStartIndex(getPageSize()), getPageSize());
                //补充一下数据 条码 商品名称 商品id 仓库名称
                List<SupplychainGoodForInDetails> supplychainGoodForInDetails = fillDetailForInOut(supplychainGoods, null);
                return success(supplychainGoodForInDetails);//返回
            } else {//说明是查询商品名称
                List<SupplychainGood> supplychainGoods = dataAccessManager.getsupplychainGoodByGoodName(goodNameOrBarcode, null, getPageStartIndex(getPageSize()), getPageSize());
                List<SupplychainGoodForInDetails> supplychainGoodForInDetails = fillDetailForInOut(supplychainGoods, null);
                return success(supplychainGoodForInDetails);//返回
            }
        } else if (storeHourseName != null && goodNameOrBarcode == null) {
            List<SupplychainShopProperties> supplychainShopProperties = dataAccessManager.getSupplychainShopPropertiesByShopName(storeHourseName, types, null, null);
            if (supplychainShopProperties != null && supplychainShopProperties.size() > 0) {
                List<String> shopIds = supplychainShopProperties.stream().map(SupplychainShopProperties::getId).collect(Collectors.toList());
                //通过shopId去获取对应的商品
                List<SupplychainGood> supplychainGoods = dataAccessManager.getsupplychaingoodByShopIds(shopIds, getPageStartIndex(getPageSize()), getPageSize());
                List<SupplychainGoodForInDetails> supplychainGoodForInDetails = fillDetailForInOut(supplychainGoods, null);
                return success(supplychainGoodForInDetails);//返回
            }
        } else { //两个都不为空
            //商店信息
            List<SupplychainShopProperties> supplychainShopProperties = dataAccessManager.getSupplychainShopPropertiesByShopName(storeHourseName, types, null, null);
            List<String> shopIds = supplychainShopProperties.stream().map(SupplychainShopProperties::getId).collect(Collectors.toList());//商店ids

            List<GoodBarcode> goodBarcodeByBarcode = GoodBarcodeRemoteService.getGoodBarcodeByBarcodesLike(goodNameOrBarcode, getHttpServletRequest());//查询条码
            if (goodBarcodeByBarcode != null && goodBarcodeByBarcode.size() > 0) {
                List<String> priceIds = goodBarcodeByBarcode.stream().map(GoodBarcode::getPriceId).collect(Collectors.toList());
                //通过priceId 去获取所有的数据
                List<SupplychainGood> supplychainGoods = dataAccessManager.getsupplychaingoodByShopIdAndPriceId2(shopIds, priceIds, getPageStartIndex(getPageSize()), getPageSize());
                //补充一下数据 条码 商品名称 商品id 仓库名称
                List<SupplychainGoodForInDetails> supplychainGoodForInDetails = fillDetailForInOut(supplychainGoods, null);
                return success(supplychainGoodForInDetails);//返回
            } else {//说明是查询商品名称
                List<SupplychainGood> supplychainGoods = dataAccessManager.getsupplychainGoodByshopIdAndGoodName(goodNameOrBarcode, shopIds, null, null, getPageStartIndex(getPageSize()), getPageSize());
                List<SupplychainGoodForInDetails> supplychainGoodForInDetails = fillDetailForInOut(supplychainGoods, null);
                return success(supplychainGoodForInDetails);//返回
            }
        }
        return success();
    }

    /**
     * 查找订单中的详情数据
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/24
     */
    @Override
    public JSONObject getSupplychainGoodForOrder() {
        //订单号
        String orderNum = getUTF("orderNum");
        if (orderNum == null || orderNum.equals("")) {
            return success();
        }
        List<OrderGoodSnapshot> orderEntryByOrderNum = OrderGoodSnapshotRemoteService.getOrderEntryByOrderNum(orderNum, getHttpServletRequest());
        if (orderEntryByOrderNum != null && orderEntryByOrderNum.size() > 0) {
            List<String> goodIds = orderEntryByOrderNum.stream().map(OrderGoodSnapshot::getGoodId).collect(Collectors.toList());
            //查找对应的商品信息
            List<SupplychainGood> supplychainGoodByIdPage = dataAccessManager.getSupplychainGoodByIdPage(goodIds, getPageStartIndex(getPageSize()), getPageSize());
            List<SupplychainGoodForInDetails> supplychainGoodForInDetails = fillDetailForInOut(supplychainGoodByIdPage, orderEntryByOrderNum);
            return success(supplychainGoodForInDetails);
        }
        return success();
    }

    @Override
    public JSONObject updateStockByList() {
        String goodList = getUTF("goodList");
        List<SupplychainGood> goodList1 = JSONObject.parseArray(goodList, SupplychainGood.class);
        dataAccessManager.updateChainGoodList(goodList1);
        return success();
    }


    /***
     * 填充数据(填充一下数据，返回格式为)
     * @author oy
     * @date 2019/4/24
     * @param supplychainGoods
     * @return java.util.List<com.tuoniaostore.supplychain.vo.SupplychainGoodForInDetails>
     */
    private List<SupplychainGoodForInDetails> fillDetailForInOut(List<SupplychainGood> supplychainGoods, List<OrderGoodSnapshot> orderGoodSnapshots) {
        List<String> priceId = supplychainGoods.stream().map(SupplychainGood::getPriceId).distinct().collect(Collectors.toList());//价格id
        //远程调用 查看价格标签
        List<GoodPriceCombinationVO> allGoodPricesByPriceIds = GoodPricePosRemoteService.getAllGoodPricesByPriceIds(priceId, getHttpServletRequest());
        List<String> shopId = supplychainGoods.stream().map(SupplychainGood::getShopId).distinct().collect(Collectors.toList());//商店id
        //查找对应的商店
        List<SupplychainShopProperties> supplychainShopPropertiesByIds = new ArrayList<>();
        if (shopId != null && shopId.size() > 0) {
            supplychainShopPropertiesByIds = dataAccessManager.getSupplychainShopPropertiesByIds(shopId);
        }
        List<SupplychainGoodForInDetails> list = new ArrayList<>();//返回结果
        if (supplychainGoods != null && supplychainGoods.size() > 0) {
            for (SupplychainGood x : supplychainGoods) {
                SupplychainGoodForInDetails vo = new SupplychainGoodForInDetails();
                vo.setId(x.getId());//id
                Optional<GoodPriceCombinationVO> first = allGoodPricesByPriceIds.stream().filter(item -> item.getPriceId().equals(x.getPriceId())).findFirst();
                String priceTitle = "";
                String barcode = "";
                String priceIdVo = "";
                if (first.isPresent()) {
                    GoodPriceCombinationVO goodPriceCombinationVO = first.get();
                    priceTitle = goodPriceCombinationVO.getPriceTitle();
                    barcode = goodPriceCombinationVO.getBarcode();
                    priceIdVo = goodPriceCombinationVO.getPriceId();
                }
                vo.setPriceId(priceIdVo);//价格标签id
                vo.setGoodName(x.getDefineName() + priceTitle);//商品名
                vo.setGoodBarcode(barcode);//条码
                Optional<SupplychainShopProperties> first1 = supplychainShopPropertiesByIds.stream().filter(item -> item.getId().equals(x.getShopId())).findFirst();
                String shopName = "";
                if (first1.isPresent()) {
                    SupplychainShopProperties supplychainShopProperties = first1.get();
                    shopName = supplychainShopProperties.getShopName();
                }
                vo.setShopName(shopName);//商店名
                if (orderGoodSnapshots != null && orderGoodSnapshots.size() > 0) {
                    Optional<OrderGoodSnapshot> first2 = orderGoodSnapshots.stream().filter(item -> item.getGoodId().equals(x.getId())).findFirst();
                    if (first2.isPresent()) {
                        OrderGoodSnapshot orderGoodSnapshot = first2.get();
                        Integer receiptQuantity = orderGoodSnapshot.getReceiptQuantity();
                        vo.setInNum(receiptQuantity);//已经进仓的数量
                        vo.setOrderGoodSnapshotId(orderGoodSnapshot.getId());//快照id
                    }
                }
                list.add(vo);
            }
        }
        return list;
    }


    /**
     * 根据商品ID查询商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/4/8
     */
    @Override
    public JSONObject getSupplychainGoodById() {
        String ids = getUTF("ids");
        String[] split = ids.split(",");
        List<String> list = Arrays.asList(split);
        List<SupplychainGood> goodById = dataAccessManager.getSupplychainGoodById(list, null, null);
        ArrayList<SupplychainGoodAndShopVO> andShopV = new ArrayList<>();
        ArrayList<String> listType = new ArrayList<>();
        listType.add(String.valueOf(WarehouseType.SUPPLIER.getKey()));
        listType.add(String.valueOf(WarehouseType.WHOLESALE.getKey()));
        listType.add(String.valueOf(WarehouseType.MANUFACTURER.getKey()));
        listType.add(String.valueOf(WarehouseType.STRAIGHT_BARN.getKey()));
        listType.add(String.valueOf(WarehouseType.THIRD_PARTY_WAREHOUSE.getKey()));
        listType.add(String.valueOf(WarehouseType.COOPERATIVE_WAREHOUSE.getKey()));
        listType.add(String.valueOf(WarehouseType.AGENCY_WAREHOUSE.getKey()));
        //查询所有仓库
        List<SupplychainShopProperties> supplychainShopProperties = dataAccessManager.getSupplychainShopPropertiesByType(listType, null,null);
        ArrayList<SupplychainGoodVo> goodVoList = new ArrayList<>();

        for (SupplychainGood good : goodById) {
            try {
                SupplychainGoodVo goodVo = new SupplychainGoodVo();
                //商品价格
                GoodPrice price = GoodPriceRemoteService.getGoodPriceById(good.getPriceId(), getHttpServletRequest());
                //商品规格，单位
                GoodUnit unit = new GoodUnit();
                if (price != null) {
                    goodVo.setGoodTitle(price.getTitle());
                    unit = GoodUnitRemoteService.getGoodUnitById(price.getUnitId(), getHttpServletRequest());
                }
                if (unit != null) {
                    goodVo.setGoodUnit(unit.getTitle());
                    goodVo.setGoodUnit(unit.getTitle());
                }
                BeanUtils.copyProperties(good, goodVo);
                //先封装好商品的规格和单位 --》》 再查询商品对应供应商
                goodVoList.add(goodVo);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        supplychainShopProperties.forEach(x -> {
            String id = x.getId();
            List<SupplychainGoodVo> collect = goodVoList.stream().filter(item -> item.getShopId().equals(id)).collect(Collectors.toList());
            if (collect.size() > 0) {
                SupplychainGoodAndShopVO supplychainGoodAndShopVO = new SupplychainGoodAndShopVO();
                supplychainGoodAndShopVO.setList(collect);
                supplychainGoodAndShopVO.setShopName(x.getShopName());
                supplychainGoodAndShopVO.setDistributionCost(x.getDistributionCost());
                supplychainGoodAndShopVO.setShopId(x.getId());
                supplychainGoodAndShopVO.setMoq(x.getMoq());
                supplychainGoodAndShopVO.setProvince(x.getProvince());
                supplychainGoodAndShopVO.setCity(x.getCity());
                supplychainGoodAndShopVO.setDistrict(x.getDistrict());
                supplychainGoodAndShopVO.setAddress(x.getAddress());
                supplychainGoodAndShopVO.setDistributionType(x.getDistributionType());
                andShopV.add(supplychainGoodAndShopVO);
            }
        });
        return success(andShopV);
    }


    /**
     * 查询下单商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/4/19
     */
    @Override
    public JSONObject getSupplychainGoodByIdOrder() {
        String ids = getUTF("ids");
        String[] split = ids.split(",");
        List<String> list = Arrays.asList(split);
        List<SupplychainGood> goodById = dataAccessManager.getSupplychainGoodById(list, null, null);
        ArrayList<SupplychainGoodAndShopVO> andShopV = new ArrayList<>();
        ArrayList<String> listType = new ArrayList<>();
        listType.add(String.valueOf(WarehouseType.SUPPLIER.getKey()));
        listType.add(String.valueOf(WarehouseType.WHOLESALE.getKey()));
        listType.add(String.valueOf(WarehouseType.MANUFACTURER.getKey()));
        listType.add(String.valueOf(WarehouseType.VIRTUAL_WAREHOUSE.getKey()));
        listType.add(String.valueOf(WarehouseType.STRAIGHT_BARN.getKey()));
        listType.add(String.valueOf(WarehouseType.THIRD_PARTY_WAREHOUSE.getKey()));
        listType.add(String.valueOf(WarehouseType.COOPERATIVE_WAREHOUSE.getKey()));
        listType.add(String.valueOf(WarehouseType.AGENCY_WAREHOUSE.getKey()));
        //查询所有仓库
        List<SupplychainShopProperties> supplychainShopProperties = dataAccessManager.getSupplychainShopPropertiesByType(listType,null,null);
        ArrayList<SupplychainGoodVo> goodVoList = new ArrayList<>();

        for (SupplychainGood good : goodById) {
            try {
                SupplychainGoodVo goodVo = new SupplychainGoodVo();
                //商品价格
                GoodPrice price = GoodPriceRemoteService.getGoodPriceById(good.getPriceId(), getHttpServletRequest());
                //商品规格，单位
                GoodUnit unit = new GoodUnit();
                if (price != null) {
                    goodVo.setGoodTitle(price.getTitle());
                    unit = GoodUnitRemoteService.getGoodUnitById(price.getUnitId(), getHttpServletRequest());
                }
                if (unit != null) {
                    goodVo.setGoodUnit(unit.getTitle());
                }
                BeanUtils.copyProperties(good, goodVo);
                //先封装好商品的规格和单位 --》》 再查询商品对应供应商
                goodVoList.add(goodVo);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        //封装供应商数据
        supplychainShopProperties.forEach(x -> {
            String id = x.getId();
            List<SupplychainGoodVo> collect = goodVoList.stream().filter(item -> item.getShopId().equals(id)).collect(Collectors.toList());
            if (collect.size() > 0) {
                SupplychainGoodAndShopVO supplychainGoodAndShopVO = new SupplychainGoodAndShopVO();
                supplychainGoodAndShopVO.setList(collect);
                supplychainGoodAndShopVO.setShopName(x.getShopName());
                supplychainGoodAndShopVO.setDistributionCost(x.getDistributionCost());
                supplychainGoodAndShopVO.setShopId(id);
                supplychainGoodAndShopVO.setShopType(x.getType());
                supplychainGoodAndShopVO.setParentId(x.getParentId());
                supplychainGoodAndShopVO.setMoq(x.getMoq());
                supplychainGoodAndShopVO.setPhoneNumber(x.getPhoneNumber());
                supplychainGoodAndShopVO.setProvince(x.getProvince());
                supplychainGoodAndShopVO.setCity(x.getCity());
                supplychainGoodAndShopVO.setDistrict(x.getDistrict());
                supplychainGoodAndShopVO.setAddress(x.getAddress());
                supplychainGoodAndShopVO.setDistributionType(x.getDistributionType());
                supplychainGoodAndShopVO.setShopUserId(x.getUserId());
                if(x.getLongitude() != null){
                    supplychainGoodAndShopVO.setLongitude(Double.parseDouble(x.getLongitude()));
                }
                if(x.getLatitude() != null){
                    supplychainGoodAndShopVO.setLatitude(Double.parseDouble(x.getLatitude()));
                }
                supplychainGoodAndShopVO.setCoveringDistance(x.getCoveringDistance());
                andShopV.add(supplychainGoodAndShopVO);

            }
        });


        andShopV.forEach(x -> {
            //是否是虚拟仓 --》 找实体仓
            if (x.getShopType() == SupplyChainShopTypeEnum.VIRTUAL_WAREHOUSE.getKey()) {
                System.out.println(x.getParentId());
                SupplychainShopProperties shopParentId = dataAccessManager.getSupplychainShopProperties(x.getParentId());
                x.setShopName(shopParentId.getShopName());
                x.setShopId(shopParentId.getId());
                x.setShopType(shopParentId.getType());
                x.setDistributionCost(shopParentId.getDistributionCost());
            }
        });

        //去除重复仓库
        for (int i = 0; i < andShopV.size(); i++) {
            for (int y = 0; y < andShopV.size(); y++) {
                // i != y 除去自身  是否还有重复
                if (andShopV.get(i).getShopId().equals(andShopV.get(y).getShopId()) && i != y) {
                    List<SupplychainGoodVo> goodVos = andShopV.get(y).getList();
                    int size = goodVos.size();
                    for (int j = 0; j < size; j++) {
                        andShopV.get(i).getList().add(goodVos.get(j));
                    }
                    andShopV.remove(y);
                    break;
                }
            }
        }

        return success(andShopV);
    }

    /**
     * 修改商品库存
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/6
     */
    @Override
    public JSONObject changSupplychainGoodByGoodId() {
        String id = getUTF("id");
        String stock = getUTF("stock");
        String isAdd = getUTF("isAdd");
        dataAccessManager.changSupplychainGoodByGoodId(id, Integer.parseInt(stock), Integer.parseInt(isAdd));
        return success();
    }

    @Override
    public JSONObject getSupplychainGoodByShopId2() {
        String shopId = getUTF("shopId");
        String typeName = getUTF("typeName", null);
        String goodNameOrBarcode = getUTF("goodNameOrBarcode", null);
        Integer status = getIntParameter("status", ChainGoodStatus.normal.getKey()); //
        if (status == 3) {//说明要查询全部的 0-未上架 1-可售
            status = null;
        }

        List<String> typeIds = null;
        if (typeName != null && !typeName.equals("")) {
            List<GoodType> goodTypesNameLike = GoodTypeRemoteService.getGoodTypesNameLike(typeName, getHttpServletRequest());
            if (goodTypesNameLike != null) {
                typeIds = goodTypesNameLike.stream().map(GoodType::getId).distinct().collect(Collectors.toList());
            }
        }

        if (shopId == null || shopId.equals("")) {
            return fail("商店id不能为空");
        }
        //typeList 直接查  shopId直接查 status直接查 goodNameOrBarcode：需要去查询一下条码
        List<String> shopList = new ArrayList<>();
        shopList.add(shopId);
        List<SupplychainGood> goodList = dataAccessManager.getsupplychainGoodByshopIdAndGoodName(goodNameOrBarcode, shopList, typeIds, status, getPageStartIndex(getPageSize()), getPageSize());
        if (goodList == null || goodList.size() == 0) {//说明商品库中 没有这个名字的商品
            //查一下当前的条码是不是存在有
            if(goodNameOrBarcode != null && !goodNameOrBarcode.equals("")){
                List<GoodBarcode> goodBarcodeByBarcodesLike = GoodBarcodeRemoteService.getGoodBarcodeByBarcodesLike(goodNameOrBarcode, getHttpServletRequest());
                if (goodBarcodeByBarcodesLike != null && goodBarcodeByBarcodesLike.size() > 0) {
                    List<String> priceIds = goodBarcodeByBarcodesLike.stream().map(GoodBarcode::getPriceId).collect(Collectors.toList());
                    List<SupplychainGood> goodList1 = dataAccessManager.getsupplychaingoodByShopIdAndPriceId(shopId, priceIds, typeIds, status, getPageStartIndex(getPageSize()), getPageSize());
                    if (goodList1 != null && goodList1.size() > 0) {
                        int count1 = dataAccessManager.getCountSupplychainGoodByParam(goodNameOrBarcode, typeIds, shopList, priceIds, status);
                        List<SupplychainGoodVO> goodPriceIdAndUnitId = getGoodPriceIdAndUnitId(goodList1);
                        return success(goodPriceIdAndUnitId, count1); ///分页 count
                    }
                }
            }
        } else {
            int count = dataAccessManager.getCountSupplychainGoodByParam(goodNameOrBarcode, typeIds, shopList, null, status);
            List<SupplychainGoodVO> goodPriceIdAndUnitId = getGoodPriceIdAndUnitId(goodList);
            return success(goodPriceIdAndUnitId, count); ///分页 count
        }
        return success(new ArrayList(), 0);
    }

    /**
     * 添加仓库和商品的绑定关系2
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/7
     */
    @Override
    @Transactional
    public JSONObject addSupplychainGoodWithShopRelation2() {
        //前提 需要选择一个仓库 进行操作
        String shopId = getUTF("shopId");
        String goodListTemplate = getUTF("goodListTemplate");

        List<GoodPriceCombinationVO> goodPriceCombinationVOS = null;
        if (goodListTemplate != null && !goodListTemplate.equals("")) {
            goodPriceCombinationVOS = JSONObject.parseArray(goodListTemplate, GoodPriceCombinationVO.class);
        }

        if (shopId == null || shopId.equals("")) {
            return fail("商店ID不能为空!");
        }
        //查找商店是否存在
        SupplychainShopProperties supplychainShopPropertiesById = dataAccessManager.getSupplychainShopPropertiesById(shopId);
        if (supplychainShopPropertiesById == null) {
            return fail("无该商店信息,请联系管理员！");
        }
        List<SupplychainGood> supplychainGoodSaveList = new ArrayList<>();
        List<SupplychainGoodBindRelationship> supplychainGoodBindRelationshipSaveList = new ArrayList<>();
        if (goodPriceCombinationVOS != null && goodPriceCombinationVOS.size() > 0) {
            goodPriceCombinationVOS.forEach(x -> {
                SupplychainGood supplychainGood = new SupplychainGood();
                supplychainGood.setGoodTemplateId(x.getGoodTemplateId());//模板id
                supplychainGood.setDefineName(x.getGoodName() + x.getPriceTitle());//名称
                supplychainGood.setDefineImage(x.getImageUrl());//图片
                supplychainGood.setShopId(shopId);//商店id

                supplychainGood.setGoodTypeId(x.getTypeNameTwoId());//类别id
                supplychainGood.setDescription(x.getGoodName());//描述

                supplychainGood.setPriceId(x.getPriceId());//价格id
                supplychainGood.setSellPrice(x.getSellPrice());//销售价
                supplychainGood.setCostPrice(x.getCostPrice());//成本价
                supplychainGood.setDiscountType(0);//促销类型
                //0509新增
                supplychainGood.setSalePrice(x.getSalePrice());//建议售价
                supplychainGood.setColleaguePrice(x.getColleaguePrice());//同行价

                supplychainGood.setMemberPrice(x.getMemberPrice());  //会员价
                supplychainGood.setMarketPrice(x.getMarketPrice());//一般市场价
                supplychainGood.setDiscountPrice(x.getDiscountPrice());//促销价

                supplychainGood.setStartingQuantity(1);// 起订量

                supplychainGoodSaveList.add(supplychainGood);

                //添加商品分类和商店的分类绑定
                SupplychainGoodBindRelationship supplychainGoodBindRelationship = new SupplychainGoodBindRelationship();
                supplychainGoodBindRelationship.setGoodTypeId(x.getTypeNameOneId());
                supplychainGoodBindRelationship.setUserId(supplychainShopPropertiesById.getUserId());
                supplychainGoodBindRelationshipSaveList.add(supplychainGoodBindRelationship);
            });

            //分类和用户绑定 需要验证通过
            List<SupplychainGoodBindRelationship> supplychainGoodBindRelationshipByUserId =
                    dataAccessManager.getSupplychainGoodBindRelationshipByUserId(supplychainShopPropertiesById.getUserId(), null, null);
            if (supplychainGoodBindRelationshipByUserId != null && supplychainGoodBindRelationshipByUserId.size() > 0) {
                List<String> collect = supplychainGoodBindRelationshipByUserId.stream().map(SupplychainGoodBindRelationship::getGoodTypeId).collect(Collectors.toList());
                List<SupplychainGoodBindRelationship> collect1 = supplychainGoodBindRelationshipSaveList.stream().filter(item -> !collect.contains(item.getGoodTypeId())).collect(Collectors.toList());
                //去重
                ArrayList<SupplychainGoodBindRelationship> collect2 = collect1.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getUserId() + f.getGoodTypeId()))), ArrayList::new));
                //保存局部的
                if (collect2 != null && collect2.size() > 0) {
                    dataAccessManager.addSupplychainGoodBindRelationships(collect2);//保存
                }
            } else {
                if (supplychainGoodBindRelationshipSaveList != null && supplychainGoodBindRelationshipSaveList.size() > 0) {
                    //去重
                    ArrayList<SupplychainGoodBindRelationship> collect = supplychainGoodBindRelationshipSaveList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getUserId() + f.getGoodTypeId()))), ArrayList::new));
                    //全部保存
                    dataAccessManager.addSupplychainGoodBindRelationships(collect);//保存
                }
            }
            //保存商品
            if (supplychainGoodSaveList != null && supplychainGoodSaveList.size() > 0) {
                //添加之前 根据list的实体 获取是否有重复的（根据goodTemplateId goodPriceId shopId）
                dataAccessManager.addSupplychainGoods(supplychainGoodSaveList);
            }
        }
        return success("添加成功");
    }

    /**
     * 批量删除商品接口
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/8
     */
    @Override
    public JSONObject batchDeleteSupplychainGoodByGoodId() {
        String ids = getUTF("ids");
        if (ids != null && !ids.equals("")) {
            List<String> strings = JSONObject.parseArray(ids, String.class);
            if (strings != null && strings.size() > 0) {
                dataAccessManager.batchDeleteSupplychainGoodByGoodId(strings);
                return success("删除成功！");
            }
        }
        return success();
    }

    /**
     * 修改绑定商品的一些信息
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/9
     */
    @Override
    public JSONObject changSupplychainGoodDetailsByGoodId() {
        String goodId = getUTF("goodId");//商品id
        Integer status = getIntParameter("status", 1);//0-未上架 1-可售
        Integer stock = getIntParameter("stock", 0);//库存
        Integer defaultSort = getIntParameter("defaultSort", 0);//排序
        Long costPrice = getLongParameter("costPrice", 0);//成本价
        Long memberPrice = getLongParameter("memberPrice", 0);//会员价
        Long sellPrice = getLongParameter("sellPrice", 0);//交易价
        Long marketPrice = getLongParameter("marketPrice", 0);//市场价
        Long salePrice = getLongParameter("salePrice", 0);//建议售价
        Long discountPrice = getLongParameter("discountPrice", 0);//促销价
        Long colleaguePrice = getLongParameter("colleaguePrice", 0);//同行价
        Long startingQuantity = getLongParameter("startingQuantity", 1);//起订量

        Map<String, Object> paramMap = new HashMap<>();//参数
        paramMap.put("id", goodId);
        paramMap.put("status", status);
        paramMap.put("stock", stock);
        paramMap.put("defaultSort", defaultSort);
        paramMap.put("costPrice", costPrice);
        paramMap.put("memberPrice", memberPrice);
        paramMap.put("sellPrice", sellPrice);
        paramMap.put("marketPrice", marketPrice);
        paramMap.put("salePrice", salePrice);
        paramMap.put("discountPrice", discountPrice);
        paramMap.put("colleaguePrice", colleaguePrice);
        paramMap.put("startingQuantity", startingQuantity);
        dataAccessManager.updateChainGoods(paramMap);//修改对应的商品信息
        return success("修改成功！");
    }

    @Override
    public JSONObject batchDownSupplychainGoodByGoodId() {
        String ids = getUTF("ids");//商品ids
        if (ids != null && !ids.equals("")) {
            List<String> stringList = JSONObject.parseArray(ids, String.class);
            if (stringList != null && stringList.size() > 0) {
                //更新成下架的
                dataAccessManager.batchDownSupplychainGoodByGoodId(stringList, ChainGoodStatus.NOT_NO_SHELVES.getKey());
            }
        }
        return success("商品下架成功！");
    }

    @Override
    public JSONObject batchUpSupplychainGoodByGoodId() {
        String ids = getUTF("ids");//商品id
        if (ids != null && !ids.equals("")) {
            List<String> stringList = JSONObject.parseArray(ids, String.class);
            if (stringList != null && stringList.size() > 0) {
                //更新成下架的
                dataAccessManager.batchDownSupplychainGoodByGoodId(stringList, ChainGoodStatus.SALABLE.getKey());
            }
        }
        return success("商品上架成功！");
    }

    /**
     * 获取商品的详细信息 单位 价格标签
     *
     * @param supplychainGoodById
     * @return java.util.List<com.tuoniaostore.supplychain.vo.SupplychainGoodVO>
     * @author oy
     * @date 2019/4/15
     */
    private List<SupplychainGoodVO> getGoodPriceIdAndUnitId(List<SupplychainGood> supplychainGoodById) {
        List<SupplychainGoodVO> list = new ArrayList<>();//返回的结果
        List<String> priceIds = supplychainGoodById.stream().map(SupplychainGood::getPriceId).distinct().collect(Collectors.toList());//将id 拿到 然后去重 成集合
        //通过价格id集合去查找对应的单位和价格标签
        //数据太多 远程调用爆炸 循环调用
        List<GoodPriceCombinationVO> allGoodPricesByPriceIds = new ArrayList<>();
        if(priceIds != null && priceIds.size() >= 150){

            int size = priceIds.size();
            for (int i = 0; i < size; i+=149) {
                //第一轮 0 - 149  150 - 299
                int i1 = i + 149;
                if(i1 >= size){
                    i1 = size;
                }
                List<String> stringList = priceIds.subList(i, i1);
                List<GoodPriceCombinationVO> templateByPriceIdByParam = GoodPricePosRemoteService.getTemplateByPriceIdByParam(stringList, null, getHttpServletRequest());
                allGoodPricesByPriceIds.addAll(templateByPriceIdByParam);
            }
        }else{
            allGoodPricesByPriceIds = GoodPricePosRemoteService.getTemplateByPriceIdByParam(priceIds, null, getHttpServletRequest());
        }

        if (allGoodPricesByPriceIds != null && allGoodPricesByPriceIds.size() > 0) {
            for (SupplychainGood x : supplychainGoodById) {
                String priceId = x.getPriceId();
                Optional<GoodPriceCombinationVO> first = allGoodPricesByPriceIds.stream().filter(item -> item.getPriceId().equals(priceId)).findFirst();
                SupplychainGoodVO vo = new SupplychainGoodVO();
                vo.setId(x.getId());//id
                vo.setTemplateId(x.getGoodTemplateId());//模板id
                vo.setDefineImage(x.getDefineImage());
                vo.setDefineName(x.getDefineName());//名称
                vo.setStock(x.getStock());//库存
                vo.setStatus(x.getStatus());//状态
                vo.setDescribed(x.getDescription());//描述
                //20190509新增
                vo.setSalePrice(x.getSalePrice());  //建议售价
                vo.setColleaguePrice(x.getColleaguePrice());    //同行价
                vo.setMemberPrice(x.getMemberPrice());  //会员价
                vo.setSiglePrice(x.getSellPrice());//销售
                vo.setCostPrice(x.getCostPrice());//成本价
                vo.setDiscountPrice(x.getDiscountPrice());//促销价
                vo.setMarketPrice(x.getMarketPrice());//市场价
                vo.setSellPrice(x.getSellPrice());//交易价
                vo.setCreateTime(x.getCreateTime());//创建时间
                vo.setStartingQuantity(x.getStartingQuantity());//起订量
                vo.setDefaultSort(x.getDefaultSort());//排序
                if (first.isPresent()) {
                    GoodPriceCombinationVO goodPriceCombinationVO = first.get();
                    List<GoodBarcode> barcodesByPriceId = GoodBarcodeRemoteService.getBarcodesByPriceId(goodPriceCombinationVO.getPriceId(), getHttpServletRequest());
                    //条码需要解析
                    vo.setBarcodeList(barcodesByPriceId);//条码
                    vo.setBarcode(goodPriceCombinationVO.getBarcode());//条码
                    //一二级名字和id
                    vo.setTypeNameOne(goodPriceCombinationVO.getTypeNameOneName());
                    vo.setTypeNameOneId(goodPriceCombinationVO.getTypeNameOneId());
                    vo.setTypeNameTwo(goodPriceCombinationVO.getTypeNameTwoName());
                    vo.setTypeNameTwoId(goodPriceCombinationVO.getTypeNameTwoId());
                    vo.setBrandId(goodPriceCombinationVO.getBrandId());//品牌id
                    vo.setBrandLogo(goodPriceCombinationVO.getBrandId());//品牌logo
                    vo.setBrandName(goodPriceCombinationVO.getBrandName());//品牌名称
                    vo.setPriceId(goodPriceCombinationVO.getPriceId());//价格id
                    vo.setPriceTitle(goodPriceCombinationVO.getPriceTitle());//价格标签
                    vo.setUnit(goodPriceCombinationVO.getUnitTitle());//单位
                    vo.setUnitId(goodPriceCombinationVO.getUnitTitleId());//单位id
                    vo.setTypeIconOne(goodPriceCombinationVO.getTypeIconOne());//一级分类的icon
                    vo.setTypeIconTwo(goodPriceCombinationVO.getTypeIconTwo());//二级分类的icon
                    vo.setTypeImageOne(goodPriceCombinationVO.getTypeImageOne());//一级分类图片
                    vo.setTypeImageTwo(goodPriceCombinationVO.getTypeImageTwo());//二级分类图片
                    vo.setUserful(0);
                    vo.setPricePic(goodPriceCombinationVO.getPriceIcon());
                    vo.setGoodName(goodPriceCombinationVO.getGoodName());
                } else {
                    vo.setUserful(1);
                }
                list.add(vo);
            }
        }
        return list;
    }



    /***
     * 获取商品的详细信息 单位 价格标签
     * @author oy
     * @date 2019/6/3
     * @param supplychainGoodById, selectPriceIds
     * @return java.util.List<com.tuoniaostore.supplychain.vo.SupplychainGoodVO>
     */
    private List<SupplychainGoodVO> getGoodPriceIdAndUnitId2(List<SupplychainGood> supplychainGoodById,List<String> selectPriceIds) {
        List<SupplychainGoodVO> list = new ArrayList<>();//返回的结果
        List<String> priceIds = supplychainGoodById.stream().map(SupplychainGood::getPriceId).distinct().collect(Collectors.toList());//将id 拿到 然后去重 成集合

        //通过价格id集合去查找对应的单位和价格标签
        //数据太多 远程调用爆炸 循环调用
        List<GoodPriceCombinationVO> allGoodPricesByPriceIds = new ArrayList<>();
        if(priceIds != null && priceIds.size() >= 150){

            int size = priceIds.size();
            for (int i = 0; i < size; i+=149) {
                //第一轮 0 - 149  150 - 299
                int i1 = i + 149;
                if(i1 >= size){
                    i1 = size;
                }
                List<String> stringList = priceIds.subList(i, i1);
                List<GoodPriceCombinationVO> templateByPriceIdByParam = GoodPricePosRemoteService.getTemplateByPriceIdByParam(stringList, null, getHttpServletRequest());
                allGoodPricesByPriceIds.addAll(templateByPriceIdByParam);
            }
        }else{
            allGoodPricesByPriceIds = GoodPricePosRemoteService.getTemplateByPriceIdByParam(priceIds, null, getHttpServletRequest());
        }

        if (allGoodPricesByPriceIds != null && allGoodPricesByPriceIds.size() > 0) {
            for (SupplychainGood x : supplychainGoodById) {
                String priceId = x.getPriceId();
                Optional<GoodPriceCombinationVO> first = allGoodPricesByPriceIds.stream().filter(item -> item.getPriceId().equals(priceId)).findFirst();
                SupplychainGoodVO vo = new SupplychainGoodVO();
                vo.setId(x.getId());//id
                vo.setTemplateId(x.getGoodTemplateId());//模板id
                vo.setDefineImage(x.getDefineImage());
                vo.setDefineName(x.getDefineName());//名称
                vo.setStock(x.getStock());//库存
                vo.setStatus(x.getStatus());//状态
                vo.setDescribed(x.getDescription());//描述
                //20190509新增
                vo.setSalePrice(x.getSalePrice());  //建议售价
                vo.setColleaguePrice(x.getColleaguePrice());    //同行价
                vo.setMemberPrice(x.getMemberPrice());  //会员价
                vo.setSiglePrice(x.getSellPrice());//销售
                vo.setCostPrice(x.getCostPrice());//成本价
                vo.setDiscountPrice(x.getDiscountPrice());//促销价
                vo.setMarketPrice(x.getMarketPrice());//市场价
                vo.setSellPrice(x.getSellPrice());//交易价
                vo.setCreateTime(x.getCreateTime());//创建时间
                vo.setStartingQuantity(x.getStartingQuantity());//起订量
                vo.setDefaultSort(x.getDefaultSort());//排序
                if (first.isPresent()) {
                    GoodPriceCombinationVO goodPriceCombinationVO = first.get();
                    List<GoodBarcode> barcodesByPriceId = GoodBarcodeRemoteService.getBarcodesByPriceId(goodPriceCombinationVO.getPriceId(), getHttpServletRequest());
                    //条码需要解析
                    vo.setBarcodeList(barcodesByPriceId);//条码
                    vo.setBarcode(goodPriceCombinationVO.getBarcode());//条码
                    //一二级名字和id
                    vo.setTypeNameOne(goodPriceCombinationVO.getTypeNameOneName());
                    vo.setTypeNameOneId(goodPriceCombinationVO.getTypeNameOneId());
                    vo.setTypeNameTwo(goodPriceCombinationVO.getTypeNameTwoName());
                    vo.setTypeNameTwoId(goodPriceCombinationVO.getTypeNameTwoId());
                    vo.setBrandId(goodPriceCombinationVO.getBrandId());//品牌id
                    vo.setBrandLogo(goodPriceCombinationVO.getBrandId());//品牌logo
                    vo.setBrandName(goodPriceCombinationVO.getBrandName());//品牌名称
                    vo.setPriceId(priceId);//价格id
                    vo.setPriceTitle(goodPriceCombinationVO.getPriceTitle());//价格标签
                    vo.setUnit(goodPriceCombinationVO.getUnitTitle());//单位
                    vo.setUnitId(goodPriceCombinationVO.getUnitTitleId());//单位id
                    vo.setTypeIconOne(goodPriceCombinationVO.getTypeIconOne());//一级分类的icon
                    vo.setTypeIconTwo(goodPriceCombinationVO.getTypeIconTwo());//二级分类的icon
                    vo.setTypeImageOne(goodPriceCombinationVO.getTypeImageOne());//一级分类图片
                    vo.setTypeImageTwo(goodPriceCombinationVO.getTypeImageTwo());//二级分类图片
                    vo.setUserful(0);
                    vo.setPricePic(goodPriceCombinationVO.getPriceIcon());
                    vo.setGoodName(goodPriceCombinationVO.getGoodName());
                } else {
                    vo.setUserful(1);
                }
                if(selectPriceIds != null && selectPriceIds.size() > 0){
                    if(selectPriceIds.contains(priceId)){
                        vo.setCheckFlag(true);
                    }else{
                        vo.setCheckFlag(false);
                    }
                }
                list.add(vo);
            }
        }
        return list;
    }


    @Override
    public JSONObject getSupplychainGoodPosList() {
        String shopId = getUTF("shopId");
        String typeId = getUTF("typeId");
        List<SupplychainGoodPOSVo> supplychainGoods = dataAccessManager.getSupplychainGoodPosList(shopId, typeId,getPageStartIndex(getPageSize()),getPageSize(),getHttpServletRequest());
        return success(supplychainGoods);
    }

    @Override
    public JSONObject changSupplychainGoodByStock() {
        int stock=getIntParameter("stock");
        dataAccessManager.changSupplychainGoodByStock(getId(),stock);
        return success();
    }

    @Override
    public JSONObject getSupplychainGoodByShopIdAndPriceId() {
        String shopId = getUTF("shopId");
        String priceId = getUTF("priceId");
        SupplychainGood supplychainGood = dataAccessManager.getSupplychainGoodByShopIdAndPriceId(shopId,priceId);
        return success(supplychainGood);
    }

    /**
     * 根据shopId 和 barcode 获取对应的商品
     * @author oy
     * @date 2019/5/25
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getSupplychainGoodByShopIdAndBarcode() {
        String shopId = getUTF("shopId");
        String barcode = getUTF("barcode");

        List<SupplychainGood> goodList = null;
        //根据条码 获取对应的 价格ids
        List<GoodBarcode> goodBarcodeByBarcodesLike = GoodBarcodeRemoteService.getGoodBarcodeByBarcodesLike(barcode, getHttpServletRequest());
        if(goodBarcodeByBarcodesLike != null && goodBarcodeByBarcodesLike.size() > 0){
            List<String> priceIds = new ArrayList<>();
            goodBarcodeByBarcodesLike.forEach(x->{
                String priceId = x.getPriceId();
                if(priceId != null && !priceId.equals("")){
                    priceIds.add(priceId);
                }
            });
            //获取对应的商品实体
            goodList = dataAccessManager.searchSupplychainGoodByShopIdAndPriceId(shopId, priceIds, null, null);
        }else{
            //如果条码不存在 那就找商品
            goodList = dataAccessManager.getsupplychainGoodByGoodName(barcode, shopId, null, null);
        }

        if(goodList != null && goodList.size() > 0){
            goodList.forEach(x->{
                String goodTemplateId = x.getGoodTemplateId();
                //远程调用对应的商品模板名称
                try {
                    GoodTemplate goodTemplate = GoodTemplateRemoteService.getGoodTemplate(goodTemplateId, getHttpServletRequest());
                    if(goodTemplate != null){
                        x.setGoodTemplateName(goodTemplate.getName());
                        x.setGoodTemplateId(goodTemplate.getId());
                    }
                    //远程调用对应的价格id
                    String priceId = x.getPriceId();
                    GoodPrice goodPriceById = GoodPriceRemoteService.getGoodPriceById(priceId, getHttpServletRequest());
                    if(goodPriceById != null){
                        x.setPriceId(goodPriceById.getId());
                        x.setPriceName(goodPriceById.getTitle());
                    }

                    //远程调用查找一级类
                    GoodType goodTypeOneByTwoTypeId = GoodTypeRemoteService.getGoodTypeOneByTwoTypeId(x.getGoodTypeId(), getHttpServletRequest());
                    if(goodTypeOneByTwoTypeId != null){
                        x.setGoodTypeIdOne(goodTypeOneByTwoTypeId.getId());
                        x.setGoodTypeNameOne(goodTypeOneByTwoTypeId.getTitle());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            return success(goodList);
        }else{
            return fail("该商品不存在！");
        }
    }

    @Override
    @Transactional
    public JSONObject updateSupplychainGoodStatus() {
        String map = getUTF("map");
        if(map != null && !map.equals("")){
            Map<String,Object> map1 = JSONObject.parseObject(map, Map.class);
            dataAccessManager.updateSupplychainGoodStatus(map1);
        }
        return success();
    }

    @Override
    public JSONObject getSupplychainGoodByIdAndStatus() {
        String goodsId = getUTF("goodsId");
        Integer status = getIntParameter("status");
        SupplychainGood supplychainGoodByIdAndStatus = dataAccessManager.getSupplychainGoodByIdAndStatus(goodsId, status);
        return success(supplychainGoodByIdAndStatus);
    }

    /**
     * 获取当前供应商所有可绑定商品列表
     * @author oy
     * @date 2019/6/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getSupplychainGoodForBindSupplierWithStore() {
        String storeId = getUTF("storeId");//仓库id
        String shopId = getUTF("shopId");//供应商id
        String typeName = getUTF("typeName", null);
        String goodNameOrBarcode = getUTF("goodNameOrBarcode", null);
        Integer status = getIntParameter("status", ChainGoodStatus.normal.getKey()); //
        if (status == 3) {//说明要查询全部的 0-未上架 1-可售
            status = null;
        }

        List<String> typeIds = null;
        if (typeName != null && !typeName.equals("")) {
            List<GoodType> goodTypesNameLike = GoodTypeRemoteService.getGoodTypesNameLike(typeName, getHttpServletRequest());
            if (goodTypesNameLike != null) {
                typeIds = goodTypesNameLike.stream().map(GoodType::getId).distinct().collect(Collectors.toList());
            }
        }

        if (shopId == null || shopId.equals("")) {
            return fail("商店id不能为空");
        }
        //typeList 直接查  shopId直接查 status直接查 goodNameOrBarcode：需要去查询一下条码
        List<String> shopList = new ArrayList<>();
        shopList.add(shopId);

        //获取当前仓库绑定的价格  id
        Map<String,Object> map = new HashMap<>();
        map.put("shopId",storeId);//仓库id
        map.put("notShopId",shopId);//供应商id
        map.put("supplierId",shopId);//供应商id
        //判断其他供应商的绑定的价格 id
        List<SupplychainWarehourseSupplierSkuRelationship> supplychainWarehourseSupplierSkuRelationshipByShopIdAndNotSupplierId = dataAccessManager.getSupplychainWarehourseSupplierSkuRelationshipByShopIdAndNotSupplierId(map);

        //获取自己绑定的价格id
        List<SupplychainWarehourseSupplierSkuRelationship> supplychainWarehourseSupplierSkuRelationshipByParam = dataAccessManager.getSupplychainWarehourseSupplierSkuRelationshipByParam(map);

        List<String> isNotPriceIn = null;
        if(supplychainWarehourseSupplierSkuRelationshipByShopIdAndNotSupplierId != null && supplychainWarehourseSupplierSkuRelationshipByShopIdAndNotSupplierId.size() > 0){
            isNotPriceIn = supplychainWarehourseSupplierSkuRelationshipByShopIdAndNotSupplierId.stream().map(SupplychainWarehourseSupplierSkuRelationship::getPriceId).distinct().collect(Collectors.toList());
        }

        List<String> isPriceIn = null;
        if(supplychainWarehourseSupplierSkuRelationshipByParam != null && supplychainWarehourseSupplierSkuRelationshipByParam.size() > 0){
            isPriceIn = supplychainWarehourseSupplierSkuRelationshipByParam.stream().map(SupplychainWarehourseSupplierSkuRelationship::getPriceId).distinct().collect(Collectors.toList());
        }

//        List<SupplychainGood> goodList = dataAccessManager.getsupplychainGoodByshopIdAndGoodName(goodNameOrBarcode, shopList, typeIds, status, getPageStartIndex(getPageSize()), getPageSize());
        List<SupplychainGood> goodList = dataAccessManager.getsupplychainGoodByshopIdAndGoodNameAndNotPrice(goodNameOrBarcode, shopList, typeIds,isNotPriceIn, status, getPageStartIndex(getPageSize()), getPageSize());

        if (goodList == null || goodList.size() == 0) {//说明商品库中 没有这个名字的商品
            //查一下当前的条码是不是存在有
            List<GoodBarcode> goodBarcodeByBarcodesLike = GoodBarcodeRemoteService.getGoodBarcodeByBarcodesLike(goodNameOrBarcode, getHttpServletRequest());
            if (goodBarcodeByBarcodesLike != null && goodBarcodeByBarcodesLike.size() > 0) {
                List<String> priceIds = goodBarcodeByBarcodesLike.stream().map(GoodBarcode::getPriceId).collect(Collectors.toList());
                List<SupplychainGood> goodList1 = dataAccessManager.getsupplychaingoodByShopIdAndPriceId(shopId, priceIds, typeIds, status, getPageStartIndex(getPageSize()), getPageSize());
                if (goodList1 != null && goodList1.size() > 0) {
//                    int count1 = dataAccessManager.getCountSupplychainGoodByParam(goodNameOrBarcode, typeIds, shopList, priceIds, status);
                    int count1 = dataAccessManager.getCountSupplychaingoodByShopIdAndPriceId(shopId, priceIds, typeIds, status);
                    List<SupplychainGoodVO> goodPriceIdAndUnitId = getGoodPriceIdAndUnitId2(goodList1,isPriceIn);
                    return success(goodPriceIdAndUnitId, count1); ///分页 count
                }
            }
        } else {
//            int count = dataAccessManager.getCountSupplychainGoodByParam(goodNameOrBarcode, typeIds, shopList, null, status);
            int count = dataAccessManager.getCountSupplychainGoodByshopIdAndGoodNameAndNotPrice(goodNameOrBarcode, shopList, typeIds,isNotPriceIn, status);
            List<SupplychainGoodVO> goodPriceIdAndUnitId = getGoodPriceIdAndUnitId2(goodList,isPriceIn);
            return success(goodPriceIdAndUnitId, count); ///分页 count
        }
        return success(new ArrayList(), 0);
    }
}