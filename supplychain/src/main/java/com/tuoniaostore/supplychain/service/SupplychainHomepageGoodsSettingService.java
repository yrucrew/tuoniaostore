package com.tuoniaostore.supplychain.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 首页设置表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-04 14:40:15
 */
public interface SupplychainHomepageGoodsSettingService {

    JSONObject addSupplychainHomepageGoodsSetting();

    JSONObject getSupplychainHomepageGoodsSetting();

    JSONObject getSupplychainHomepageGoodsSettings();

    JSONObject getSupplychainHomepageGoodsSettingCount();
    JSONObject getSupplychainHomepageGoodsSettingAll();

    JSONObject changeSupplychainHomepageGoodsSetting();

    JSONObject getSupplychainHomepageGoodsSettingForSelected();

    JSONObject getSupplychainHomepageGoodsSettingForHotBrand();

    JSONObject getSupplychainHomepageGoodsSettingForHotCategory();

    JSONObject getSupplychainHomepageGoodsSettingForRecommendingCommodities();

    /**
     * 商家端：采购 获取热门品牌下面的商品
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getSupplychainGoodByGoodTypeByStore();

    /**
     * 商家端：采购 获取热门品牌品牌下面的商品
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getSupplychainGoodByGoodBrandByStore();

    /**
     * 商家端：采购 页面搜索商品
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getSupplychainGoodByGoodNameSearch();

    /**
     * 商家端采购：获取首页的 精选商品
     * @author oy
     * @date 2019/4/22
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getHomepageGoodsSettingForSelected();

    JSONObject listSupplychainGoodForHomeSetting();

    JSONObject bacchDeleteSupplychainGoodForHomeSetting();

    JSONObject addSupplychainGoodForHomeSetting();

    JSONObject listForHomeSettingByBrand();

    JSONObject listForHomeSettingByGoodType();

    JSONObject getHomePageGoodByAreaId();

    JSONObject batchChangeHomeGoods();
}
