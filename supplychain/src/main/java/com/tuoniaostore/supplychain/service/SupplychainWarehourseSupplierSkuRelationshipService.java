package com.tuoniaostore.supplychain.service;


import com.alibaba.fastjson.JSONObject;


public interface SupplychainWarehourseSupplierSkuRelationshipService {

    JSONObject getSupplychainWarehourseSupplierSkuRelationships();

    /***
     * 添加仓库绑定供应商的商品（priceId）
     * @author oy
     * @date 2019/6/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject addSupplychainWarehourseSupplierSkuRelationships();
}
