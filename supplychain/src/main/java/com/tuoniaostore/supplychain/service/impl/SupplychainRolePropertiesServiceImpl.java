package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainRoleProperties;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainRolePropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainRolePropertiesService")
public class SupplychainRolePropertiesServiceImpl extends BasicWebService implements SupplychainRolePropertiesService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainRoleProperties() {
        SupplychainRoleProperties supplychainRoleProperties = new SupplychainRoleProperties();
        //
        String id = getUTF("id", null);
        //角色ID
        String roleId = getUTF("roleId", null);
        //属性类型
        Integer type = getIntParameter("type", 0);
        //属性标志
        String k = getUTF("k", null);
        //具体属性值
        String v = getUTF("v", null);
        supplychainRoleProperties.setId(id);
        supplychainRoleProperties.setRoleId(roleId);
        supplychainRoleProperties.setType(type);
        supplychainRoleProperties.setK(k);
        supplychainRoleProperties.setV(v);
        dataAccessManager.addSupplychainRoleProperties(supplychainRoleProperties);
        return success();
    }

    @Override
    public JSONObject getSupplychainRoleProperties() {
        SupplychainRoleProperties supplychainRoleProperties = dataAccessManager.getSupplychainRoleProperties(getId());
        return success(supplychainRoleProperties);
    }

    @Override
    public JSONObject getSupplychainRolePropertiess() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainRoleProperties> supplychainRolePropertiess = dataAccessManager.getSupplychainRolePropertiess(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainRolePropertiesCount(status, name);
        return success(supplychainRolePropertiess, count);
    }

    @Override
    public JSONObject getSupplychainRolePropertiesAll() {
        List<SupplychainRoleProperties> supplychainRolePropertiess = dataAccessManager.getSupplychainRolePropertiesAll();
        return success(supplychainRolePropertiess);
    }

    @Override
    public JSONObject getSupplychainRolePropertiesCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainRolePropertiesCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainRoleProperties() {
        SupplychainRoleProperties supplychainRoleProperties = dataAccessManager.getSupplychainRoleProperties(getId());
        //
        String id = getUTF("id", null);
        //角色ID
        String roleId = getUTF("roleId", null);
        //属性类型
        Integer type = getIntParameter("type", 0);
        //属性标志
        String k = getUTF("k", null);
        //具体属性值
        String v = getUTF("v", null);
        supplychainRoleProperties.setId(id);
        supplychainRoleProperties.setRoleId(roleId);
        supplychainRoleProperties.setType(type);
        supplychainRoleProperties.setK(k);
        supplychainRoleProperties.setV(v);
        dataAccessManager.changeSupplychainRoleProperties(supplychainRoleProperties);
        return success();
    }

}
