package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainPartnerCallback;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainPartnerCallbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainPartnerCallbackService")
public class SupplychainPartnerCallbackServiceImpl extends BasicWebService implements SupplychainPartnerCallbackService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainPartnerCallback() {
        SupplychainPartnerCallback supplychainPartnerCallback = new SupplychainPartnerCallback();
        //
        String id = getUTF("id", null);
        //合作方ID
        String partnerId = getUTF("partnerId", null);
        //操作类型
        Integer optType = getIntParameter("optType", 0);
        //回调地址
        String callbackUrl = getUTF("callbackUrl", null);
        //回调延迟
        Integer callbackDelayed = getIntParameter("callbackDelayed", 0);
        //状态 0-禁用 1-启用
        Integer status = getIntParameter("status", 0);
        //描述内容
        String describe = getUTF("describe", null);
        supplychainPartnerCallback.setId(id);
        supplychainPartnerCallback.setPartnerId(partnerId);
        supplychainPartnerCallback.setOptType(optType);
        supplychainPartnerCallback.setCallbackUrl(callbackUrl);
        supplychainPartnerCallback.setCallbackDelayed(callbackDelayed);
        supplychainPartnerCallback.setStatus(status);
        supplychainPartnerCallback.setDescribe(describe);
        dataAccessManager.addSupplychainPartnerCallback(supplychainPartnerCallback);
        return success();
    }

    @Override
    public JSONObject getSupplychainPartnerCallback() {
        SupplychainPartnerCallback supplychainPartnerCallback = dataAccessManager.getSupplychainPartnerCallback(getId());
        return success(supplychainPartnerCallback);
    }

    @Override
    public JSONObject getSupplychainPartnerCallbacks() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainPartnerCallback> supplychainPartnerCallbacks = dataAccessManager.getSupplychainPartnerCallbacks(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainPartnerCallbackCount(status, name);
        return success(supplychainPartnerCallbacks, count);
    }

    @Override
    public JSONObject getSupplychainPartnerCallbackAll() {
        List<SupplychainPartnerCallback> supplychainPartnerCallbacks = dataAccessManager.getSupplychainPartnerCallbackAll();
        return success(supplychainPartnerCallbacks);
    }

    @Override
    public JSONObject getSupplychainPartnerCallbackCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainPartnerCallbackCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainPartnerCallback() {
        SupplychainPartnerCallback supplychainPartnerCallback = dataAccessManager.getSupplychainPartnerCallback(getId());
        //
        String id = getUTF("id", null);
        //合作方ID
        String partnerId = getUTF("partnerId", null);
        //操作类型
        Integer optType = getIntParameter("optType", 0);
        //回调地址
        String callbackUrl = getUTF("callbackUrl", null);
        //回调延迟
        Integer callbackDelayed = getIntParameter("callbackDelayed", 0);
        //状态 0-禁用 1-启用
        Integer status = getIntParameter("status", 0);
        //描述内容
        String describe = getUTF("describe", null);
        supplychainPartnerCallback.setId(id);
        supplychainPartnerCallback.setPartnerId(partnerId);
        supplychainPartnerCallback.setOptType(optType);
        supplychainPartnerCallback.setCallbackUrl(callbackUrl);
        supplychainPartnerCallback.setCallbackDelayed(callbackDelayed);
        supplychainPartnerCallback.setStatus(status);
        supplychainPartnerCallback.setDescribe(describe);
        dataAccessManager.changeSupplychainPartnerCallback(supplychainPartnerCallback);
        return success();
    }

}
