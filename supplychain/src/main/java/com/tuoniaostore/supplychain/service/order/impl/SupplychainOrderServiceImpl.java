package com.tuoniaostore.supplychain.service.order.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.order.OrderTypeEnum;
import com.tuoniaostore.commons.support.order.OrderEntryPosRemoteService;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.vo.order.OrderEntryPosVO;
import com.tuoniaostore.supplychain.service.SupplychainShopPropertiesService;
import com.tuoniaostore.supplychain.service.order.SupplychainOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("SupplychainOrderService")
public class SupplychainOrderServiceImpl extends BasicWebService implements SupplychainOrderService {

    @Autowired
    SupplychainShopPropertiesService supplychainShopPropertiesService;

    @Override
    public JSONObject getSaleOrderList() {
        int type = getIntParameter("type", OrderTypeEnum.SUPERMARKETPOS.getKey());
        SupplychainShopProperties shop = supplychainShopPropertiesService.getSupplychainShopPropertiesByUid(getUser().getId());
        if (shop == null) {
            return fail("当前用户无门店数据");
        }
        List<OrderEntryPosVO> orderEntryPosVOS =
                OrderEntryPosRemoteService.getOrderEntryPosListBySaleUserIdAndType(shop.getId(), type, getPageStartIndex(getPageSize()), getPageSize(), getHttpServletRequest());
        int total = OrderEntryPosRemoteService.getOrderEntryPosListCountBySaleUserIdAndType(shop.getId(), type, getPageStartIndex(getPageSize()), getPageSize(), getHttpServletRequest());
        return success(orderEntryPosVOS, total);
    }
}
