package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopDailyGoodSalesLogger;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainShopDailyGoodSalesLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainShopDailyGoodSalesLoggerService")
public class SupplychainShopDailyGoodSalesLoggerServiceImpl extends BasicWebService implements SupplychainShopDailyGoodSalesLoggerService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainShopDailyGoodSalesLogger() {
        SupplychainShopDailyGoodSalesLogger supplychainShopDailyGoodSalesLogger = new SupplychainShopDailyGoodSalesLogger();
        //
        String id = getUTF("id", null);
        //仓库ID
        String shopId = getUTF("shopId", null);
        //商品ID
        String goodId = getUTF("goodId", null);
        //商品模版ID
        String goodTemplateId = getUTF("goodTemplateId", null);
        //商品名字
        String goodName = getUTF("goodName", null);
        //
        Integer totalInStorageQuantity = getIntParameter("totalInStorageQuantity", 0);
        //总销售数量
        Integer totalSalesQuantity = getIntParameter("totalSalesQuantity", 0);
        //应销售总价
        Long totalOriginPrice = getLongParameter("totalOriginPrice", 0);
        //真实销售总价
        Long totalSellPrice = getLongParameter("totalSellPrice", 0);
        //总成本价
        Long totalCostPrice = getLongParameter("totalCostPrice", 0);
        //发生的日期
        String happenDate = getUTF("happenDate", null);
        supplychainShopDailyGoodSalesLogger.setId(id);
        supplychainShopDailyGoodSalesLogger.setShopId(shopId);
        supplychainShopDailyGoodSalesLogger.setGoodId(goodId);
        supplychainShopDailyGoodSalesLogger.setGoodTemplateId(goodTemplateId);
        supplychainShopDailyGoodSalesLogger.setGoodName(goodName);
        supplychainShopDailyGoodSalesLogger.setTotalInStorageQuantity(totalInStorageQuantity);
        supplychainShopDailyGoodSalesLogger.setTotalSalesQuantity(totalSalesQuantity);
        supplychainShopDailyGoodSalesLogger.setTotalOriginPrice(totalOriginPrice);
        supplychainShopDailyGoodSalesLogger.setTotalSellPrice(totalSellPrice);
        supplychainShopDailyGoodSalesLogger.setTotalCostPrice(totalCostPrice);
        supplychainShopDailyGoodSalesLogger.setHappenDate(happenDate);
        dataAccessManager.addSupplychainShopDailyGoodSalesLogger(supplychainShopDailyGoodSalesLogger);
        return success();
    }

    @Override
    public JSONObject getSupplychainShopDailyGoodSalesLogger() {
        SupplychainShopDailyGoodSalesLogger supplychainShopDailyGoodSalesLogger = dataAccessManager.getSupplychainShopDailyGoodSalesLogger(getId());
        return success(supplychainShopDailyGoodSalesLogger);
    }

    @Override
    public JSONObject getSupplychainShopDailyGoodSalesLoggers() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainShopDailyGoodSalesLogger> supplychainShopDailyGoodSalesLoggers = dataAccessManager.getSupplychainShopDailyGoodSalesLoggers(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainShopDailyGoodSalesLoggerCount(status, name);
        return success(supplychainShopDailyGoodSalesLoggers, count);
    }

    @Override
    public JSONObject getSupplychainShopDailyGoodSalesLoggerAll() {
        List<SupplychainShopDailyGoodSalesLogger> supplychainShopDailyGoodSalesLoggers = dataAccessManager.getSupplychainShopDailyGoodSalesLoggerAll();
        return success(supplychainShopDailyGoodSalesLoggers);
    }

    @Override
    public JSONObject getSupplychainShopDailyGoodSalesLoggerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainShopDailyGoodSalesLoggerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainShopDailyGoodSalesLogger() {
        SupplychainShopDailyGoodSalesLogger supplychainShopDailyGoodSalesLogger = dataAccessManager.getSupplychainShopDailyGoodSalesLogger(getId());
        //
        String id = getUTF("id", null);
        //仓库ID
        String shopId = getUTF("shopId", null);
        //商品ID
        String goodId = getUTF("goodId", null);
        //商品模版ID
        String goodTemplateId = getUTF("goodTemplateId", null);
        //商品名字
        String goodName = getUTF("goodName", null);
        //
        Integer totalInStorageQuantity = getIntParameter("totalInStorageQuantity", 0);
        //总销售数量
        Integer totalSalesQuantity = getIntParameter("totalSalesQuantity", 0);
        //应销售总价
        Long totalOriginPrice = getLongParameter("totalOriginPrice", 0);
        //真实销售总价
        Long totalSellPrice = getLongParameter("totalSellPrice", 0);
        //总成本价
        Long totalCostPrice = getLongParameter("totalCostPrice", 0);
        //发生的日期
        String happenDate = getUTF("happenDate", null);
        supplychainShopDailyGoodSalesLogger.setId(id);
        supplychainShopDailyGoodSalesLogger.setShopId(shopId);
        supplychainShopDailyGoodSalesLogger.setGoodId(goodId);
        supplychainShopDailyGoodSalesLogger.setGoodTemplateId(goodTemplateId);
        supplychainShopDailyGoodSalesLogger.setGoodName(goodName);
        supplychainShopDailyGoodSalesLogger.setTotalInStorageQuantity(totalInStorageQuantity);
        supplychainShopDailyGoodSalesLogger.setTotalSalesQuantity(totalSalesQuantity);
        supplychainShopDailyGoodSalesLogger.setTotalOriginPrice(totalOriginPrice);
        supplychainShopDailyGoodSalesLogger.setTotalSellPrice(totalSellPrice);
        supplychainShopDailyGoodSalesLogger.setTotalCostPrice(totalCostPrice);
        supplychainShopDailyGoodSalesLogger.setHappenDate(happenDate);
        dataAccessManager.changeSupplychainShopDailyGoodSalesLogger(supplychainShopDailyGoodSalesLogger);
        return success();
    }

}
