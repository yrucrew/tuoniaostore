package com.tuoniaostore.supplychain.service.order;

import com.alibaba.fastjson.JSONObject;

public interface SupplychainOrderService {

    /**
     * 获取销售订单列表
     */
    JSONObject getSaleOrderList();
}
