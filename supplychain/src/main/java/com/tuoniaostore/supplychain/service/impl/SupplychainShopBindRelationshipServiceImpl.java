package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.CommonUtils;
import com.tuoniaostore.commons.constant.MessageType;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.supplychain.ShopBindRelationshipStatusEnum;
import com.tuoniaostore.commons.constant.supplychain.ShopBindRelationshipTypeConstant;
import com.tuoniaostore.commons.constant.supplychain.ShopBindRelationshipTypeEnum;
import com.tuoniaostore.commons.constant.supplychain.SupplyChainShopTypeEnum;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplychainShopPropertiesRemoteService;
import com.tuoniaostore.datamodel.DefineRandom;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopBindRelationship;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.redis.RedisService;
import com.tuoniaostore.supplychain.service.SupplychainShopBindRelationshipService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;


@Service("supplychainShopBindRelationshipService")
public class SupplychainShopBindRelationshipServiceImpl extends BasicWebService implements SupplychainShopBindRelationshipService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Autowired
    private RedisService redisService;

    @Override
    public JSONObject addSupplychainShopBindRelationship() {
        SupplychainShopBindRelationship supplychainShopBindRelationship = new SupplychainShopBindRelationship();
        //
        String id = getUTF("id", null);
        //仓库ID
        String shopId = getUTF("shopId", null);
        //角色ID
        String roleId = getUTF("roleId", null);
        //绑定类型，如：1-仓管 2-拣货员 3-配送员
        Integer type = getIntParameter("type", 0);
        //当前状态，0-不可用 1可用
        Integer status = getIntParameter("status", 0);
        if (id != null) {
            supplychainShopBindRelationship.setId(id);
        }
        supplychainShopBindRelationship.setShopId(shopId);
        supplychainShopBindRelationship.setRoleId(roleId);
        supplychainShopBindRelationship.setType(type);
        supplychainShopBindRelationship.setStatus(status);
        dataAccessManager.addSupplychainShopBindRelationship(supplychainShopBindRelationship);
        return success();
    }

    @Override
    public JSONObject getSupplychainShopBindRelationship() {
        SupplychainShopBindRelationship supplychainShopBindRelationship = dataAccessManager.getSupplychainShopBindRelationship(getId());
        return success(supplychainShopBindRelationship);
    }

    @Override
    public JSONObject getSupplychainShopBindRelationships() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainShopBindRelationship> supplychainShopBindRelationships = dataAccessManager.getSupplychainShopBindRelationships(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainShopBindRelationshipCount(status, name);
        return success(supplychainShopBindRelationships, count);
    }

    @Override
    public JSONObject getSupplychainShopBindRelationshipAll() {
        List<SupplychainShopBindRelationship> supplychainShopBindRelationships = dataAccessManager.getSupplychainShopBindRelationshipAll();
        return success(supplychainShopBindRelationships);
    }

    @Override
    public JSONObject getSupplychainShopBindRelationshipCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainShopBindRelationshipCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainShopBindRelationship() {
        SupplychainShopBindRelationship supplychainShopBindRelationship = dataAccessManager.getSupplychainShopBindRelationship(getId());
        //
        String id = getUTF("id", null);
        //仓库ID
        String shopId = getUTF("shopId", null);
        //角色ID
        String roleId = getUTF("roleId", null);
        //绑定类型，如：1-仓管 2-拣货员 3-配送员
        Integer type = getIntParameter("type", 0);
        //当前状态，0-不可用 1可用
        Integer status = getIntParameter("status", 0);
        supplychainShopBindRelationship.setId(id);
        supplychainShopBindRelationship.setShopId(shopId);
        supplychainShopBindRelationship.setRoleId(roleId);
        supplychainShopBindRelationship.setType(type);
        supplychainShopBindRelationship.setStatus(status);
        dataAccessManager.changeSupplychainShopBindRelationship(supplychainShopBindRelationship);
        return success();
    }

    public SupplychainDataAccessManager getDataAccessManager() {
        return dataAccessManager;
    }

    public void setDataAccessManager(SupplychainDataAccessManager dataAccessManager) {
        this.dataAccessManager = dataAccessManager;
    }

    @Override
    public JSONObject getPurchaseBindShops() {
        String purchaseIds = getUTF("purchaseIds");

        // 结果
        JSONArray responseArr = new JSONArray();
        String[] purchaseIdArr = purchaseIds.split(",");
        for (String purchaseId : purchaseIdArr) {
            JSONObject data = new JSONObject();
            data.put("purchaseId", purchaseId);
            responseArr.add(data);
        }

        // 供应商
        List<SupplychainShopBindRelationship> bindSuppliers = dataAccessManager.getShopBindRelationshipsByRoleIds(purchaseIdArr, ShopBindRelationshipTypeEnum.PURCHASE_BIND_SUPPLIER_SHOP.getType());

        if (!CommonUtils.isEmpty(bindSuppliers)) {

            Map<String, String> bindSupplierShopNamesMap = getBindShopNames(bindSuppliers);
            for (String purchaseId : bindSupplierShopNamesMap.keySet()) {

                for (int i = 0; i < responseArr.size(); i++) {
                    JSONObject data = responseArr.getJSONObject(i);
                    if (data.getString("purchaseId").equals(purchaseId)) {
                        data.put("bindSupplierShopNames", bindSupplierShopNamesMap.get(purchaseId));
                        break;
                    }
                }
            }

        }


        // 仓库
        List<SupplychainShopBindRelationship> bindShops = dataAccessManager.getShopBindRelationshipsByRoleIds(purchaseIdArr, ShopBindRelationshipTypeEnum.PURCHASE_BIND_SHOP.getType());

        if (!CommonUtils.isEmpty(bindShops)) {

            Map<String, String> bindShopNameMap = getBindShopNames(bindShops);
            for (String purchaseId : bindShopNameMap.keySet()) {

                for (int i = 0; i < responseArr.size(); i++) {
                    JSONObject data = responseArr.getJSONObject(i);
                    if (data.getString("purchaseId").equals(purchaseId)) {
                        data.put("bindShopName", bindShopNameMap.get(purchaseId));
                        break;
                    }
                }
            }

        }

        return success(responseArr);
    }

    /**
     * 获取绑定名字
     */
    private Map<String, String> getBindShopNames(List<SupplychainShopBindRelationship> bindShops) {
        Map<String, String> dataMap = new HashMap<>();

        Map<String, List<SupplychainShopBindRelationship>> bindMap = bindShops.stream().collect(Collectors.groupingBy(SupplychainShopBindRelationship::getRoleId, Collectors.toList()));

        List<String> shopIds = bindShops.stream().map(SupplychainShopBindRelationship::getShopId).collect(Collectors.toList());

        List<SupplychainShopProperties> shops = dataAccessManager.getSupplychainShopPropertiesByIds(shopIds);

        Map<String, SupplychainShopProperties> shopMap = shops.stream().collect(Collectors.toMap(SupplychainShopProperties::getId, Function.identity()));

        for (String purchaseId : bindMap.keySet()) {

            List<SupplychainShopBindRelationship> list = bindMap.get(purchaseId);

            StringBuilder sb = new StringBuilder();
            list.forEach(data -> {
                String shopId = data.getShopId();
                SupplychainShopProperties shop = shopMap.get(shopId);
                if (shop != null) {
                    sb.append(shop.getShopName()).append(",");
                }
            });
            if (sb.length() > 0)
                sb.deleteCharAt(sb.length() - 1);

            dataMap.put(purchaseId, sb.toString());

        }
        return dataMap;
    }

    /**
     * @see ShopBindRelationshipTypeConstant
     */
    @Override
    public JSONObject purchaseBindShop() {
        String purchaseUserId = getUTF("purchaseUserId");
        String shopIds = getUTF("shopIds", "");
        int type = getIntParameter("type");
        switch (type) {
            // 仓库
            case ShopBindRelationshipTypeConstant.PURCHASE_BIND_SHOP:
                purchaseBindShop(type, purchaseUserId, shopIds);
                break;
            // 供应商
            case ShopBindRelationshipTypeConstant.PURCHASE_BIND_SUPPLIER_SHOP:
                purchaseBindShop(type, purchaseUserId, shopIds);
                break;
            default:
                return fail("类型错误");
        }
        return success("绑定成功");
    }

    private void purchaseBindShop(int type, String roleId, String shopIdsStr) {
        dataAccessManager.delSupplychainShopBindRelationships(type, roleId);
        if (StringUtils.isEmpty(shopIdsStr)) {
            return;
        }
        String[] shopIds = shopIdsStr.split(",");

        dataAccessManager.delSupplychainShopBindRelationships(type, roleId);

        SupplychainShopBindRelationship shopBindRelationship = new SupplychainShopBindRelationship();
        shopBindRelationship.setStatus(1);
        shopBindRelationship.setRoleId(roleId);
        for (String shopId : shopIds) {
            shopBindRelationship.setId(DefineRandom.getUUID());
            shopBindRelationship.setType(type);
            shopBindRelationship.setShopId(shopId);
            dataAccessManager.addSupplychainShopBindRelationship(shopBindRelationship);
        }
    }

    /**
     * @return
     * @see ShopBindRelationshipTypeConstant 类型常量
     */
    @Override
    public JSONObject getSupplychainShopPropertiesList() {
        String purchaseUserId = getUTF("purchaseUserId");
        int type = getIntParameter("type");
        String types;// 类型
        ShopBindRelationshipTypeEnum shopBindRelationshipTypeEnum;
        switch (type) {
            // 仓库-6
            case ShopBindRelationshipTypeConstant.PURCHASE_BIND_SHOP:
                types = SupplyChainShopTypeEnum.STRAIGHT_BARN.getKey() + ","
                        + SupplyChainShopTypeEnum.VIRTUAL_WAREHOUSE.getKey() + ","
                        + SupplyChainShopTypeEnum.THIRD_PARTY_WAREHOUSE.getKey() + ","
                        + SupplyChainShopTypeEnum.COOPERATIVE_WAREHOUSE.getKey() + ","
                        + SupplyChainShopTypeEnum.AGENCY_WAREHOUSE.getKey();
                shopBindRelationshipTypeEnum = ShopBindRelationshipTypeEnum.PURCHASE_BIND_SHOP;
                break;
            // 供应商-4
            case ShopBindRelationshipTypeConstant.PURCHASE_BIND_SUPPLIER_SHOP:
                types = SupplyChainShopTypeEnum.SUPPLIER.getKey() + ","
                        + SupplyChainShopTypeEnum.WHOLESALE.getKey() + ","
                        + SupplyChainShopTypeEnum.MANUFACTURER.getKey();
                shopBindRelationshipTypeEnum = ShopBindRelationshipTypeEnum.PURCHASE_BIND_SUPPLIER_SHOP;
                break;
            default:
                return fail("类型有误");
        }

        List<SupplychainShopProperties> shops = dataAccessManager.getSupplychainShopPropertiesAll(types);

        List<SupplychainShopBindRelationship> binds = dataAccessManager.getShopBindRelationships(purchaseUserId, shopBindRelationshipTypeEnum.getType());
        Map<String, SupplychainShopBindRelationship> bindMap = new HashMap<>();
        if (!CommonUtils.isEmpty(binds)) {
            bindMap.putAll(binds.stream().collect(Collectors.toMap(SupplychainShopBindRelationship::getShopId, Function.identity())));
        }

        JSONArray arr = shops.stream().map(shop -> {
            JSONObject data = new JSONObject();
            data.put("id", shop.getId());
            data.put("type", shop.getType());
            data.put("shopName", shop.getShopName());
            data.put("adminName", shop.getAdminName());
            data.put("phoneNumber", shop.getPhoneNumber());

            if (StringUtils.isEmpty(shop.getParentId()) || !"0".equals(shop.getParentId())) {
                SupplychainShopProperties parentShop = dataAccessManager.getSupplychainShopProperties(shop.getParentId());
                if (parentShop != null)
                    data.put("parentName", parentShop.getShopName());
            }

            data.put("checkFlag", bindMap.containsKey(shop.getId()));

            return data;
        }).collect(Collectors.toCollection(JSONArray::new));

        return success(arr, arr.size());
    }

    @Override
    public JSONObject getShopBindRelationships() {
        Integer type = getIntParameter("type");
        String userId = getUTF("userId");
        List<SupplychainShopBindRelationship> shopBindRelationships = dataAccessManager.getShopBindRelationships(userId, type);
        return success(shopBindRelationships);
    }

    @Override
    public JSONObject getSysUsersListByPos() {
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopPropertiesByUserId(getUser().getUserId());
        if (supplychainShopProperties == null) {
            return fail("当前用户没有门店或已删除");
        }
        // 门店id
        String shopId = supplychainShopProperties.getId();
        // 获取门店员工的绑定关系
        List<SupplychainShopBindRelationship> ShopBindRelationship =
                dataAccessManager.getRelationshipsByShopIdAndType(shopId, ShopBindRelationshipTypeEnum.CLERK_BIND_SHOP.getType(), getPageStartIndex(getPageSize()), getPageSize());

        List<String> userIds = ShopBindRelationship.stream().map(SupplychainShopBindRelationship::getRoleId).collect(Collectors.toList());
        // 无员工
        if (CollectionUtils.isEmpty(userIds)) {
            return success(new ArrayList<>());
        }
        List<SysUser> sysUserInIds = SysUserRemoteService.getSysUserInIds(userIds, getHttpServletRequest());
        return success(sysUserInIds);
    }

    @Override
    @Transactional
    public JSONObject addSysUserByPos() {
        // 入参
        String realName = getUTF("realName");
        String phoneNumber = getUTF("phoneNumber");
        String code = getUTF("code");
        String terminal = getUTF("terminal");//获取终端类型
        String password = getUTF("password");
        String posRoleId = getUTF("posRoleId", "10001");

        //验证手机号码和验证码是否正确
        SysUser sysUserByPhoneNumber = null;
        try {
            sysUserByPhoneNumber = SysUserRemoteService.getSysUserByPhoneNumForRemote(phoneNumber, getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sysUserByPhoneNumber != null) {//说明有该用户了 不能添加
            return fail("该号码已存在! 若有问题，联系管理员。");
        }

        //验证当前手机号码和短信验证码是否正确
        String messageCode = redisService.getMessageCode(MessageType.MESSAGE_TYPE_REGISTER.getPrefix() + phoneNumber);
        if (messageCode == null || !messageCode.equals(code)) {
            return fail("验证码错误或超时！");
        }

        // 添加用户
        SysUser sysUser = new SysUser();
        String id = getUser().getId();
        //用户的基本信息
        sysUser.setPassword(encryptionPassword(password));//密码
        sysUser.setRealName(realName);//真实名字
        sysUser.setStatus(StatusEnum.NORMAL.getKey());//状态
        sysUser.setFromChannel("2e96e47f626c480b8ad39ac791177a5b");//便利店渠道 写死
        sysUser.setShowName(realName);//展示的名字
        sysUser.setDefaultName(phoneNumber);//默认用户名
        sysUser.setLeaderUserId(id);//上级用户
        sysUser.setPosRoleId(posRoleId);//pos机角色id
        sysUser.setUserId(id);
        sysUser.setIdNumber(phoneNumber);
        sysUser.setPhoneNumber(phoneNumber);
        SysUserRemoteService.addSysUserByPos(sysUser, terminal, getHttpServletRequest());

        //将用户和门店绑定
        SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(id);
        if (supplychainShopPropertiesByUserId != null) {
            String shopId = supplychainShopPropertiesByUserId.getId();//店主的商店id
            SupplychainShopBindRelationship supplychainShopBindRelationship = new SupplychainShopBindRelationship();
            supplychainShopBindRelationship.setRoleId(sysUser.getId());
            supplychainShopBindRelationship.setStatus(ShopBindRelationshipStatusEnum.NORMAL.getType());
            supplychainShopBindRelationship.setShopId(shopId);
            supplychainShopBindRelationship.setType(ShopBindRelationshipTypeEnum.CLERK_BIND_SHOP.getType());
            dataAccessManager.addSupplychainShopBindRelationship(supplychainShopBindRelationship);
        }
        return success("添加用户成功！");
    }

    @Override
    public JSONObject editSysUserByPos() {
        String userId = getUTF("userId");//用户id
        String realName = getUTF("realName");
        String password = getUTF("password");

        SysUser sysUser = new SysUser();
        sysUser.setId(userId);
        sysUser.setRealName(realName);
        sysUser.setPassword(encryptionPassword(password));

        SysUserRemoteService.updateSysUserByPos(sysUser, getHttpServletRequest());
        return success("保存成功！");
    }

    @Override
    public JSONObject forbidSysUserByPos() {
        String userId = getUTF("userId");
        int status = ShopBindRelationshipStatusEnum.DISABLE.getType();
        // 更改状态-不可用
        int type = ShopBindRelationshipTypeEnum.CLERK_BIND_SHOP.getType();
        return dataAccessManager.updateRelationshipByUserIdAndType(userId, type, status) > 0 ? success("冻结成功！") : fail("失败！");
    }

    @Override
    public JSONObject delSysUserByPos() {
        int type = ShopBindRelationshipTypeEnum.CLERK_BIND_SHOP.getType();
        String userId = getUTF("userId");
        return dataAccessManager.delSupplychainShopBindRelationships(type, userId) > 0 ? success("删除成功！") : fail("删除失败！");
    }

}
