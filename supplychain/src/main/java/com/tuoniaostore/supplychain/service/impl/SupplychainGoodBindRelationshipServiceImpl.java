package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.supplychain.SupplyChainShopTypeEnum;
import com.tuoniaostore.commons.support.good.GoodTypeRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplychainGoodBindRelationshipRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplychainShopPropertiesRemoteService;
import com.tuoniaostore.datamodel.good.GoodType;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodBindRelationship;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.vo.good.GoodTypeOne;
import com.tuoniaostore.datamodel.vo.good.GoodTypeTwo;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.remote.GoodsRemoteService;
import com.tuoniaostore.supplychain.service.SupplychainGoodBindRelationshipService;
import com.tuoniaostore.supplychain.vo.SupplychainGoodBindRelationShipVO;
import com.tuoniaostore.supplychain.vo.SupplychainTypeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import org.springframework.transaction.annotation.Transactional;


@Service("supplychainGoodBindRelationshipService")
public class SupplychainGoodBindRelationshipServiceImpl extends BasicWebService implements SupplychainGoodBindRelationshipService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;
    @Autowired
    GoodsRemoteService goodsRemoteService;

    @Override
    public JSONObject addSupplychainGoodBindRelationship() {
        SupplychainGoodBindRelationship supplychainGoodBindRelationship = new SupplychainGoodBindRelationship();
        //
        String id = getUTF("id", null);
        //通行证ID
        String useId = getUTF("useId", null);
        //角色类型，如：供应商、采购员等
        Integer roleType = getIntParameter("roleType", 0);
        //商品类型ID
        String goodTypeId = getUTF("goodTypeId", null);
        supplychainGoodBindRelationship.setId(id);
        supplychainGoodBindRelationship.setUserId(useId);
        supplychainGoodBindRelationship.setRoleType(roleType);
        supplychainGoodBindRelationship.setGoodTypeId(goodTypeId);
        dataAccessManager.addSupplychainGoodBindRelationship(supplychainGoodBindRelationship);
        return success();
    }

    @Override
    public JSONObject getSupplychainGoodBindRelationship() {
        SupplychainGoodBindRelationship supplychainGoodBindRelationship = dataAccessManager.getSupplychainGoodBindRelationship(getId());
        return success(supplychainGoodBindRelationship);
    }

    @Override
    public JSONObject getSupplychainGoodBindRelationships() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainGoodBindRelationship> supplychainGoodBindRelationships = dataAccessManager.getSupplychainGoodBindRelationships(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainGoodBindRelationshipCount(status, name);
        return success(supplychainGoodBindRelationships, count);
    }

    @Override
    public JSONObject getSupplychainGoodBindRelationshipAll() {
        List<SupplychainGoodBindRelationship> supplychainGoodBindRelationships = dataAccessManager.getSupplychainGoodBindRelationshipAll();
        return success(supplychainGoodBindRelationships);
    }

    @Override
    public JSONObject getSupplychainGoodBindRelationshipCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainGoodBindRelationshipCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainGoodBindRelationship() {
        SupplychainGoodBindRelationship supplychainGoodBindRelationship = dataAccessManager.getSupplychainGoodBindRelationship(getId());
        //
        String id = getUTF("id", null);
        //通行证ID
        String useId = getUTF("useId", null);
        //角色类型，如：供应商、采购员等
        Integer roleType = getIntParameter("roleType", 0);
        //商品类型ID
        String goodTypeId = getUTF("goodTypeId", null);
        supplychainGoodBindRelationship.setId(id);
        supplychainGoodBindRelationship.setUserId(useId);
        supplychainGoodBindRelationship.setRoleType(roleType);
        supplychainGoodBindRelationship.setGoodTypeId(goodTypeId);
        dataAccessManager.changeSupplychainGoodBindRelationship(supplychainGoodBindRelationship);
        return success();
    }

    /**
     * 获取所有的商品类型 根据用户id
     *
     * @return
     */
    @Override
    public JSONObject getSupplychainGoodBindRelationshipByUserId() {
        //获取用户id
        SysUser user = getUser();
        String userId = user.getId();

        //通过用户id 查找一下商店的属性 只有type是 5 8 9才是供应链的
        SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(userId);
        String shopId = "";
        if (supplychainShopPropertiesByUserId != null) {
            shopId = supplychainShopPropertiesByUserId.getId();
        }
        if (supplychainShopPropertiesByUserId != null) {
            int type = supplychainShopPropertiesByUserId.getType();
            if (type != SupplyChainShopTypeEnum.SUPPLIER.getKey() && type != SupplyChainShopTypeEnum.WHOLESALE.getKey() && type != SupplyChainShopTypeEnum.MANUFACTURER.getKey()) {
                return fail("没有此权限！");
            }
        } else {
            return fail("该用户没有商店，请联系管理员！");
        }
        JSONObject jsonObject = getGoodTypeByUserId2(user, shopId);
        return success(jsonObject);
    }

    /**
     * 获取商家端所有的商品类型
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/3/31
     */
    @Override
    public JSONObject getSupplychainGoodBindRelationshipByUserId2() {
        //获取用户id
        SysUser user = getUser();

        //通过用户id 查找一下商店的属性 只有type是 5 8 9才是供应链的
        SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(user.getId());
        if (supplychainShopPropertiesByUserId == null) {
            return fail("该用户无商店信息，请联系管理员！");
        }
        String shopId = supplychainShopPropertiesByUserId.getId();

        if (supplychainShopPropertiesByUserId != null) {
            int type = supplychainShopPropertiesByUserId.getType();
            if (type != SupplyChainShopTypeEnum.ORDINARY_STORES.getKey() && type != SupplyChainShopTypeEnum.DIRECT_SHOP.getKey()) { //判断是否是普通门店的 6 7
                return fail("没有此权限！");
            }
        } else {
            return fail("该用户没有商店，请联系管理员！");
        }
        JSONObject jsonObject = getGoodTypeByUserId2(user, shopId);
        return success(jsonObject);
    }


    /**
     * 商家：添加商品类型
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/2
     */
    @Override
    public JSONObject addSupplychainGoodBindRelationshipByUser() {
        //user
        SysUser user = getUser();
        String userId = user.getId();
        String goodType = getUTF("goodType");//商品分类

        if (goodType == null || goodType.equals("")) {
            return fail("商品分类不能为空！");
        }

        SupplychainShopProperties supplychainGoodBindRelationship = null;
        //去查询该用户的商店 类型为商家 6 7
        try {
            supplychainGoodBindRelationship = dataAccessManager.getSupplychainShopPropertiesByUserId(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (supplychainGoodBindRelationship == null) {//说明用户没有商店
            return fail("该用户没有商店！");
        } else {
            Integer type = supplychainGoodBindRelationship.getType();
            if (type != SupplyChainShopTypeEnum.ORDINARY_STORES.getKey() && type != SupplyChainShopTypeEnum.DIRECT_SHOP.getKey()) {//说明该商店权限不对
                return fail("该用户没有权限！");
            }
        }
        //去获取 有没有该类别 有的话 则返回已经存在
        GoodType goodTypeByTitle = GoodTypeRemoteService.getGoodTypeByTitle(goodType, getHttpServletRequest());
        if (goodTypeByTitle != null) {
            return fail("该分类已经存在！");
        } else {
            //去goodType 表 添加对应的数据 远程
            GoodType goodType1 = new GoodType();
            goodType1.setTitle(goodType);
            goodType1.setStatus(1);
            goodType1.setType(1);//门店分类
            try {
                GoodTypeRemoteService.addGoodTypes(goodType1, getHttpServletRequest());//远程添加
            } catch (Exception e) {
                e.printStackTrace();
            }
            //添加绑定用户和商品类别
            SupplychainGoodBindRelationship entity = new SupplychainGoodBindRelationship();
            entity.setUserId(userId);
            entity.setGoodTypeId(goodType1.getId());
            dataAccessManager.addSupplychainGoodBindRelationship(entity);
        }
        return success("添加商品类型成功！");
    }

    /**
     * 商家端：删除商品分类
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/10
     */
    @Override
    public JSONObject delSupplychainGoodBindRelationshipByUser() {
        //获取分类id
        String typeId = getUTF("typeId");
        if (typeId == null || typeId.equals("")) {
            return fail("分类id不能为空！");
        }
        SysUser user = getUser();
        String userId = user.getId();
        //获取当前用户的商店
        String shopId = null;
        SupplychainShopProperties supplychainShopPropertiesByUserId = dataAccessManager.getSupplychainShopPropertiesByUserId(userId);
        if (supplychainShopPropertiesByUserId != null) {
            shopId = supplychainShopPropertiesByUserId.getId();
        }
        //删除下面的所有商品（逻辑删除）
        if (shopId != null) {
            //需要拿二级分类的
            List<GoodType> goodSecondTypeByTypeId = GoodTypeRemoteService.getGoodSecondTypeByTypeId(typeId, getHttpServletRequest());
            if(goodSecondTypeByTypeId != null && goodSecondTypeByTypeId.size() > 0){
                List<String> secondTypeId = goodSecondTypeByTypeId.stream().map(GoodType::getId).collect(Collectors.toList());
                dataAccessManager.delSupplychainGoodByShopIdAndTypeId(shopId, secondTypeId);//逻辑删除商品chain good
            }
            dataAccessManager.delSupplychainGoodBindRelationshipByUserIdAndTypeId(userId, typeId);//删除绑定的商品和分类的关系（物理删除）
        } else {
            return fail("该用户商店不存在！");
        }
        return success("删除成功！");
    }

    @Override
    public JSONObject getSupplychainGoodBindRelationshipByUserIdForRemote() {
        String userId = getUTF("userId");
        if (userId == null || userId.equals("")) {
            return fail(1001,"用户不能为空！");
        }
        List<SupplychainGoodBindRelationship> supplychainGoodBindRelationshipByUserId = dataAccessManager.getSupplychainGoodBindRelationshipByUserId(userId, null, null);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list", supplychainGoodBindRelationshipByUserId);
        return jsonObject;
    }

    /**
     * 为商店绑定商品分类
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/6
     */
    @Override
    public JSONObject bindSupplychainGoodBindRelationships() {
        String typeList = getUTF("typeEntity");
        if (typeList != null && !typeList.equals("")) {
            SupplychainGoodBindRelationShipVO supplychainGoodBindRelationShipVO = JSONObject.parseObject(typeList, SupplychainGoodBindRelationShipVO.class);
            if (supplychainGoodBindRelationShipVO != null) {
                List<String> typeList1 = supplychainGoodBindRelationShipVO.getTypeList();
                if (typeList1 != null && typeList1.size() > 0) {
                    List<SupplychainGoodBindRelationship> list = new ArrayList<>();
                    typeList1.forEach(x -> {
                        SupplychainGoodBindRelationship supplychainGoodBindRelationship = new SupplychainGoodBindRelationship();
                        supplychainGoodBindRelationship.setUserId(getUser().getId());
                        supplychainGoodBindRelationship.setGoodTypeId(x);
                        list.add(supplychainGoodBindRelationship);
                    });
                    dataAccessManager.addSupplychainGoodBindRelationships(list);
                    return success("绑定成功！");
                }
            }
        }
        return success();
    }

    /**
     * 获取每个商店绑定的分类
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/8
     */
    @Override
    public JSONObject getbindSupplychainGoodBindRelationships() {//
        String shopName1 = getUTF("shopName", null);
        String typeName1 = getUTF("typeName", null);
        String type = getUTF("type");//仓库的属性 5 8 9 供应商  6 7 门店

        List<String> typeIds = null;//分类id
        List<GoodType> goodTypesNameLike = null;//分类实体
        List<String> userId = null;//用户id
        List<SupplychainGoodBindRelationship> supplychainGoodBindRelationShipsByUseridsAndTypeIds = null;//绑定列表

        if (typeName1 != null && !typeName1.equals("")) {
            goodTypesNameLike = GoodTypeRemoteService.getGoodTypesOneNameLike(typeName1, getHttpServletRequest());

            if(goodTypesNameLike  == null || goodTypesNameLike.size() == 0){
                return success(new ArrayList(),0);
            }
            typeIds = goodTypesNameLike.stream().map(GoodType::getId).collect(Collectors.toList());
            //根据typeid去绑定表中 查找对应的信息
            supplychainGoodBindRelationShipsByUseridsAndTypeIds = dataAccessManager.getSupplychainGoodBindRelationShipsByUseridsAndTypeIds(null, typeIds);
            if(supplychainGoodBindRelationShipsByUseridsAndTypeIds == null || supplychainGoodBindRelationShipsByUseridsAndTypeIds.size() == 0){
                return success(new ArrayList(),0);
            }
            userId = supplychainGoodBindRelationShipsByUseridsAndTypeIds.stream().map(SupplychainGoodBindRelationship::getUserId).distinct().collect(Collectors.toList());
        }

        //查找所有的绑定分类
        List<SupplychainGoodBindRelationShipVO> list = new ArrayList<>();//返回结果
        List<Integer> shopTypeIds = new ArrayList<>();//商店类型
        String[] split = type.split(",");
        if(split != null && split.length > 0){
            for (int i = 0; i < split.length; i++) {
                String s = split[i];
                int i1 = Integer.parseInt(s);
                shopTypeIds.add(i1);
            }
        }
//        shopTypeIds.add(6);
//        shopTypeIds.add(7);
        List<SupplychainShopProperties> supplychainShopPropertiesAll = dataAccessManager.getSupplychainShopPropertiesByShopNameAndUserId(shopName1, userId,shopTypeIds, getPageStartIndex(getPageSize()), getPageSize());
        int count = 0;//查询所有的条数
        List<String> userIds = null;//商店店主id
        if (supplychainShopPropertiesAll != null && supplychainShopPropertiesAll.size() > 0) {
            userIds = supplychainShopPropertiesAll.stream().map(SupplychainShopProperties::getUserId).collect(Collectors.toList());
            count = dataAccessManager.getCountSupplychainShopPropertiesByShopNameAndUserId(shopName1,shopTypeIds, userId);//查询所有的条数
        }
        //根据用户id  获取对应的绑定关系
        List<SupplychainGoodBindRelationship> supplychainGoodBindRelationshipAll2 = dataAccessManager.getSupplychainGoodBindRelationshipByUserIds(userIds);
        //用户+类别去重  去重
        List<SupplychainGoodBindRelationship> supplychainGoodBindRelationshipAll = supplychainGoodBindRelationshipAll2.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getUserId() + f.getGoodTypeId()))), ArrayList::new));
        if (supplychainShopPropertiesAll != null && supplychainShopPropertiesAll.size() > 0) {
            try {
                //将里面的分类进行 user:typeName
                Map<String, String> map = new HashMap<>();
//                List<GoodType> goodTypes = GoodTypeRemoteService.getGoodTypeOne(getHttpServletRequest());
//              //  Map<String, GoodType> goodTypeMap = new HashMap<>();//一级分类
//                for (int i = 0; i < goodTypes.size(); i++) {
//                    goodTypeMap.put(goodTypes.get(i).getId(), goodTypes.get(i));
//                }
                //用户和分类名拼接
                if(supplychainGoodBindRelationshipAll2 != null && supplychainGoodBindRelationshipAll2.size() > 0){
                    for (SupplychainGoodBindRelationship x : supplychainGoodBindRelationshipAll) {
                        String userId2 = x.getUserId();
                        if(x.getGoodTypeId()!=null){
                            GoodType goodType = goodsRemoteService.getGoodType(x.getGoodTypeId()); //goodTypeMap.get();
                            if (goodType != null) {
                                String title = goodType.getTitle();
                                if (title == null) {
                                    title = "";
                                }
                                String s = map.get(userId2);
                                if (s != null && !s.equals("")) {
                                    s += "," + title;
                                    map.remove(userId2);
                                    map.put(userId2, s);
                                } else {
                                    map.put(userId2, title);
                                }
                            }
                        }
                    }
                }

                //遍历商店
                supplychainShopPropertiesAll.forEach(x -> {
                    String shopName = x.getShopName();
                    String shopId = x.getId();
                    String userId2 = x.getUserId();
                    SupplychainGoodBindRelationShipVO vo = new SupplychainGoodBindRelationShipVO();
                    vo.setShopId(shopId);
                    vo.setShopName(shopName);
                    if(map != null && map.entrySet().size() > 0){
                        String s = map.get(userId2);
                        vo.setTypeName(s);
                    }
                    list.add(vo);
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return success(list, count);
    }


    @Override
    public JSONObject getTypeBySupplychainGoodBindType() {
        String shopId = getUTF("shopId");

        if (shopId == null || shopId.equals("")) {
            return fail("shopId不能为空");
        }
        String userId = null;
        SupplychainShopProperties supplychainShopPropertiesById = dataAccessManager.getSupplychainShopPropertiesById(shopId);
        if (supplychainShopPropertiesById != null) {
            userId = supplychainShopPropertiesById.getUserId();
        } else {
            return fail("商店信息错误！");
        }
        //
        List<GoodType> goodTypes = GoodTypeRemoteService.getGoodTypeOne(getHttpServletRequest());

        if (goodTypes != null && goodTypes.size() > 0) {
            List<SupplychainGoodBindRelationship> supplychainGoodBindRelationshipByUserId = dataAccessManager.getSupplychainGoodBindRelationshipByUserId(userId, null, null);
            if (supplychainGoodBindRelationshipByUserId != null && supplychainGoodBindRelationshipByUserId.size() > 0) {
                List<String> goodTypeIds = supplychainGoodBindRelationshipByUserId.stream().map(SupplychainGoodBindRelationship::getGoodTypeId).distinct().collect(Collectors.toList());
                if (goodTypeIds != null && goodTypeIds.size() > 0) {
                    List<GoodType> collect = goodTypes.stream().map(item -> {
                        String id = item.getId();
                        if (goodTypeIds.contains(id)) {
                            item.setCheakFlag(true);
                        } else {
                            item.setCheakFlag(false);
                        }
                        return item;
                    }).collect(Collectors.toList());
                    return success(collect);
                } else {//其实不会出现这种情况的
                    return success(goodTypes);
                }
            } else {
                if (goodTypes != null && goodTypes.size() > 0) {
                    List<GoodType> collect = goodTypes.stream().map(x -> {
                        x.setCheakFlag(false);
                        return x;
                    }).collect(Collectors.toList());
                    return success(collect);
                }
                return success();
            }
        } else {
            return success();
        }
    }

    /**
     * 获取商店绑定的分类详情
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/8
     */
    @Override
    public JSONObject getbindSupplychainGoodBindRelationshipsDetails() {
        String shopId = getUTF("shopId");
        if (shopId == null || shopId.equals("")) {
            return fail("商店id不能为空！");
        }
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(shopId);
        List<GoodType> goodTypes=null;
        int count =0;
        if (supplychainShopProperties != null) {
            String userId = supplychainShopProperties.getUserId();
            List<SupplychainGoodBindRelationship> supplychainGoodBindRelationshipByUserId = dataAccessManager.getSupplychainGoodBindRelationshipByUserId(userId, null, null);
            if (supplychainGoodBindRelationshipByUserId != null && supplychainGoodBindRelationshipByUserId.size() > 0) {
                List<String> collect = supplychainGoodBindRelationshipByUserId.stream().map(SupplychainGoodBindRelationship::getGoodTypeId).distinct().collect(Collectors.toList());
                try {
                    List<GoodType> goodTypes1 = GoodTypeRemoteService.getGoodTypes(collect, getHttpServletRequest());

                    if(goodTypes1!=null){
                          count = goodTypes1.size();
                    }
                    goodTypes = GoodTypeRemoteService.getGoodTypesByPage(collect, getPageStartIndex(getPageSize()), getPageSize(), getHttpServletRequest());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            return fail("无该商店信息！");
        }
        return success(goodTypes, count);

    }

    /**
     * 修改商店绑定的分类
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/8
     */
    @Override
    @Transactional
    public JSONObject updatebindSupplychainGoodBindRelationships() {
        String shopId = getUTF("shopId");
        String typeList = getUTF("typeList");

        if (shopId == null || shopId.equals("")) {
            return fail("商店id不能为空！");
        }
        //用过shopId获取对应的用户id
        SupplychainShopProperties supplychainShopProperties = dataAccessManager.getSupplychainShopProperties(shopId);
        if (supplychainShopProperties != null) {
            String userId = supplychainShopProperties.getUserId();
            if (typeList != null && !typeList.equals("")) {
                List<String> typeIds = JSONObject.parseArray(typeList, String.class);
                List<String> typeIdsDistinct = typeIds.stream().distinct().collect(Collectors.toList());//去重后
                if (typeIdsDistinct.size() == 0) {//说明是删除
                    dataAccessManager.clearSupplychainGoodBindRelationships(userId);
                    return success("保存成功!");
                }
                List<SupplychainGoodBindRelationship> batchList = new ArrayList<>();
                //添加对应的分类信息
                typeIdsDistinct.forEach(x -> {
                    SupplychainGoodBindRelationship o = new SupplychainGoodBindRelationship();
                    o.setGoodTypeId(x);
                    o.setUserId(userId);
                    batchList.add(o);
                });
                dataAccessManager.clearSupplychainGoodBindRelationships(userId);
                dataAccessManager.batchAddSupplychainGoodBindRelationships(batchList);
                return success("保存成功！");
            } else {
                return fail("类型id不能为空");
            }
        } else {
            return fail("无商店信息");
        }
    }

    /**
     * 根据用户id 查找当前的所有商品的类型2
     *
     * @param user, shopId
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/6
     */
    private JSONObject getGoodTypeByUserId2(SysUser user, String shopId) {
        //根据用户id 查找当前的所有商品的类型
        JSONObject jsonObject = new JSONObject();
        List<SupplychainGoodBindRelationship> supplychainGoodBindRelationshipByUserId = dataAccessManager.getSupplychainGoodBindRelationshipByUserId(user.getId(), null, null);
        if (supplychainGoodBindRelationshipByUserId != null && supplychainGoodBindRelationshipByUserId.size() > 0) {
            //获取到所有的goodTypeId 一级分类id
            List<String> typeIds = supplychainGoodBindRelationshipByUserId.stream().map(SupplychainGoodBindRelationship::getGoodTypeId).distinct().collect(Collectors.toList());
            //调用远端服务 去查找商品模块的所有类别
            List<SupplychainTypeVO> list = new ArrayList<>();//存放结果
            Map<String,SupplychainTypeVO> map = new HashMap<>();

            if (typeIds != null && typeIds.size() > 0) {
                try {
                    //一二级的标题结构d
                    List<GoodTypeOne> goodTypes = GoodTypeRemoteService.getGoodTypesAndSecondTypeByTypeId(typeIds, getHttpServletRequest());//获取商品类别 一级二级
                    if (goodTypes != null && goodTypes.size() > 0) {
                        Map<String, List<GoodTypeTwo>> collect = goodTypes.stream().collect(Collectors.toMap(item -> item.getOneLeaveId(), GoodTypeOne::getList));
                        List<String> goodTypeList = new ArrayList<>();//type 二级分类的集合
                        Map<String,String> goodTypeMap = new HashMap<>(); // map

                        collect.forEach((x, y) -> {
                            if (y != null && y.size() > 0) {
                                y.forEach(z -> {
                                    goodTypeList.add(z.getTwoLeaveId());//商品分类 取二级的
                                    goodTypeMap.put(z.getTwoLeaveId(),x);//二级分类：一级分类
                                });
                            }
                        });

                        //查看该商家的所有商品
                        List<SupplychainGood> supplychainGoodByShopIdAndTypes = dataAccessManager.getSupplychainGoodByShopIdAndTypes(shopId, goodTypeList, null, null);
                        //遍历所有的商品
                        if(supplychainGoodByShopIdAndTypes != null && supplychainGoodByShopIdAndTypes.size() > 0){
                            supplychainGoodByShopIdAndTypes.forEach(x->{
                                SupplychainTypeVO vo = new SupplychainTypeVO();
                                String goodTypeId = x.getGoodTypeId();
                                if(goodTypeId != null && !goodTypeId.equals("")){
                                    String oneTypeId = goodTypeMap.get(goodTypeId);
                                    vo.setId(oneTypeId);
                                    SupplychainTypeVO mapVo = map.get(oneTypeId);
                                    Optional<GoodTypeOne> first = goodTypes.stream().filter(item -> item.getOneLeaveId().equals(oneTypeId)).findFirst();
                                    if(first.isPresent()){
                                        GoodTypeOne goodTypeOne = first.get();
                                        String oneLeaveName = goodTypeOne.getOneLeaveName();
                                        vo.setTitle(oneLeaveName);
                                    }
                                    if (mapVo != null) {
                                        vo.setNumber(mapVo.getNumber() + 1);
                                        map.put(oneTypeId,vo);
                                    }else {
                                        vo.setNumber(1);
                                        map.put(oneTypeId,vo);
                                    }
                                }
                            });
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(map != null && map.entrySet().size() > 0){
                map.forEach((x,y)->{
                    list.add(y);
                });
            }
            jsonObject.put("list", list);
        }
        return jsonObject;
    }

}
