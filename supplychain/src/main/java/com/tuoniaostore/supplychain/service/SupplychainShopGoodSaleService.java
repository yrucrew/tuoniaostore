package com.tuoniaostore.supplychain.service;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.datamodel.vo.order.GoodSaleMsgVO;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-20 20:05:41
 */
public interface SupplychainShopGoodSaleService {

    JSONObject addSupplychainShopGoodSale();

    JSONObject getSupplychainShopGoodSale();

    JSONObject getSupplychainShopGoodSales();

    JSONObject getSupplychainShopGoodSaleCount();
    JSONObject getSupplychainShopGoodSaleAll();

    JSONObject changeSupplychainShopGoodSale();

    JSONObject listSupplychainShopGoodSale();
}
