package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodModifyLogger;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainGoodModifyLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainGoodModifyLoggerService")
public class SupplychainGoodModifyLoggerServiceImpl extends BasicWebService implements SupplychainGoodModifyLoggerService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainGoodModifyLogger() {
        SupplychainGoodModifyLogger supplychainGoodModifyLogger = new SupplychainGoodModifyLogger();
        //
        String id = getUTF("id", null);
        //修改者角色ID 系统修改时为0
        String roleId = getUTF("roleId", null);
        //修改者角色名
        String roleName = getUTF("roleName", null);
        //商品ID
        String goodId = getUTF("goodId", null);
        //商品模版ID
        String goodTemplateId = getUTF("goodTemplateId", null);
        //修改前商品数量
        Integer beforeStock = getIntParameter("beforeStock", 0);
        //修改后商品数量
        Integer afterStock = getIntParameter("afterStock", 0);
        //操作类型 0-出库 1-入库 3-盘点修复
        Integer type = getIntParameter("type", 0);
        supplychainGoodModifyLogger.setId(id);
        supplychainGoodModifyLogger.setRoleId(roleId);
        supplychainGoodModifyLogger.setRoleName(roleName);
        supplychainGoodModifyLogger.setGoodId(goodId);
        supplychainGoodModifyLogger.setGoodTemplateId(goodTemplateId);
        supplychainGoodModifyLogger.setBeforeStock(beforeStock);
        supplychainGoodModifyLogger.setAfterStock(afterStock);
        supplychainGoodModifyLogger.setType(type);
//        supplychainGoodModifyLogger.setModifyTime(modifyTime);
        dataAccessManager.addSupplychainGoodModifyLogger(supplychainGoodModifyLogger);
        return success();
    }

    @Override
    public JSONObject getSupplychainGoodModifyLogger() {
        SupplychainGoodModifyLogger supplychainGoodModifyLogger = dataAccessManager.getSupplychainGoodModifyLogger(getId());
        return success(supplychainGoodModifyLogger);
    }

    @Override
    public JSONObject getSupplychainGoodModifyLoggers() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainGoodModifyLogger> supplychainGoodModifyLoggers = dataAccessManager.getSupplychainGoodModifyLoggers(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainGoodModifyLoggerCount(status, name);
        return success(supplychainGoodModifyLoggers, count);
    }

    @Override
    public JSONObject getSupplychainGoodModifyLoggerAll() {
        List<SupplychainGoodModifyLogger> supplychainGoodModifyLoggers = dataAccessManager.getSupplychainGoodModifyLoggerAll();
        return success(supplychainGoodModifyLoggers);
    }

    @Override
    public JSONObject getSupplychainGoodModifyLoggerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainGoodModifyLoggerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainGoodModifyLogger() {
        SupplychainGoodModifyLogger supplychainGoodModifyLogger = dataAccessManager.getSupplychainGoodModifyLogger(getId());
        //
        String id = getUTF("id", null);
        //修改者角色ID 系统修改时为0
        String roleId = getUTF("roleId", null);
        //修改者角色名
        String roleName = getUTF("roleName", null);
        //商品ID
        String goodId = getUTF("goodId", null);
        //商品模版ID
        String goodTemplateId = getUTF("goodTemplateId", null);
        //修改前商品数量
        Integer beforeStock = getIntParameter("beforeStock", 0);
        //修改后商品数量
        Integer afterStock = getIntParameter("afterStock", 0);
        //操作类型 0-出库 1-入库 3-盘点修复
        Integer type = getIntParameter("type", 0);
        //修改时间
//        Date modifyTime = getUTF("modifyTime", null);
        supplychainGoodModifyLogger.setId(id);
        supplychainGoodModifyLogger.setRoleId(roleId);
        supplychainGoodModifyLogger.setRoleName(roleName);
        supplychainGoodModifyLogger.setGoodId(goodId);
        supplychainGoodModifyLogger.setGoodTemplateId(goodTemplateId);
        supplychainGoodModifyLogger.setBeforeStock(beforeStock);
        supplychainGoodModifyLogger.setAfterStock(afterStock);
        supplychainGoodModifyLogger.setType(type);
//        supplychainGoodModifyLogger.setModifyTime(modifyTime);
        dataAccessManager.changeSupplychainGoodModifyLogger(supplychainGoodModifyLogger);
        return success();
    }

}
