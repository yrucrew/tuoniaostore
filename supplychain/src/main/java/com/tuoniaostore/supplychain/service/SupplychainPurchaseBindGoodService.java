package com.tuoniaostore.supplychain.service;

import com.alibaba.fastjson.JSONObject;

public interface SupplychainPurchaseBindGoodService {

	JSONObject addPurchaseBindGood();

	JSONObject updatePurchaseBindGood();

	JSONObject deletePurchaseBindGood();

	JSONObject purchaseBindGoodList();

	JSONObject getGoodTemplates();

	JSONObject checkPurchaseBindBood();
}
