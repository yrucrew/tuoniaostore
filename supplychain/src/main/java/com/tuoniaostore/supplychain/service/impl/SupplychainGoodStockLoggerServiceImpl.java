package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.CommonUtils;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodStockLogger;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainGoodStockLoggerService;
import com.tuoniaostore.supplychain.vo.SupplychainGoodStockLoggerListVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainGoodStockLoggerService")
public class SupplychainGoodStockLoggerServiceImpl extends BasicWebService implements SupplychainGoodStockLoggerService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainGoodStockLogger() {
        SupplychainGoodStockLogger supplychainGoodStockLogger = new SupplychainGoodStockLogger();
        //库存偏移值
        Integer offsetStock = getIntParameter("offsetStock", 0);
        //修改之前的数量
        Integer beforeStock = getIntParameter("beforeStock", 0);
        //修改之后的数量
        Integer afterStock = getIntParameter("afterStock", 0);
        //操作者角色ID
        String operatorRoleId = getUTF("operatorRoleId", null);
        //操作者角色名
        String operatorRoleName = getUTF("operatorRoleName", null);
        //操作类型 1-出库 2-入库(采购) 3-退货 4-调拨
        Integer operationType = getIntParameter("operationType", 0);
        //操作的时间
       // Date operationTime = getUTF("operationTime", null);
        supplychainGoodStockLogger.setOffsetStock(offsetStock);
        supplychainGoodStockLogger.setBeforeStock(beforeStock);
        supplychainGoodStockLogger.setAfterStock(afterStock);
        supplychainGoodStockLogger.setOperatorRoleId(operatorRoleId);
        supplychainGoodStockLogger.setOperatorRoleName(operatorRoleName);
        supplychainGoodStockLogger.setOperationType(operationType);
        dataAccessManager.addSupplychainGoodStockLogger(supplychainGoodStockLogger);
        return success();
    }

    @Override
    public JSONObject getSupplychainGoodStockLogger() {
        SupplychainGoodStockLogger supplychainGoodStockLogger = dataAccessManager.getSupplychainGoodStockLogger(getId());
        return success(supplychainGoodStockLogger);
    }

    @Override
    public JSONObject getSupplychainGoodStockLoggers() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainGoodStockLogger> supplychainGoodStockLoggers = dataAccessManager.getSupplychainGoodStockLoggers(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainGoodStockLoggerCount(status, name);
        return success(supplychainGoodStockLoggers, count);
    }

    @Override
    public JSONObject getSupplychainGoodStockLoggerAll() {
        List<SupplychainGoodStockLogger> supplychainGoodStockLoggers = dataAccessManager.getSupplychainGoodStockLoggerAll();
        return success(supplychainGoodStockLoggers);
    }

    @Override
    public JSONObject getSupplychainGoodStockLoggerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainGoodStockLoggerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainGoodStockLogger() {
        SupplychainGoodStockLogger supplychainGoodStockLogger = dataAccessManager.getSupplychainGoodStockLogger(getId());
        //商品ID
        String goodId = getUTF("goodId");
        //库存偏移值
        Integer offsetStock = getIntParameter("offsetStock", 0);
        //修改之前的数量
        Integer beforeStock = getIntParameter("beforeStock", 0);
        //修改之后的数量
        Integer afterStock = getIntParameter("afterStock", 0);
        //操作者角色ID
        String operatorRoleId = getUTF("operatorRoleId", null);
        //操作者角色名
        String operatorRoleName = getUTF("operatorRoleName", null);
        //操作类型 1-出库 2-入库(采购) 3-退货 4-调拨
        Integer operationType = getIntParameter("operationType", 0);
        supplychainGoodStockLogger.setGoodId(goodId);
        supplychainGoodStockLogger.setOffsetStock(offsetStock);
        supplychainGoodStockLogger.setBeforeStock(beforeStock);
        supplychainGoodStockLogger.setAfterStock(afterStock);
        supplychainGoodStockLogger.setOperatorRoleId(operatorRoleId);
        supplychainGoodStockLogger.setOperatorRoleName(operatorRoleName);
        supplychainGoodStockLogger.setOperationType(operationType);
        dataAccessManager.changeSupplychainGoodStockLogger(supplychainGoodStockLogger);
        return success();
    }
    
	@Override
	public JSONObject getGoodStockLoggerList() {
		int type = getIntParameter("type", -1);
		int operationType = getIntParameter("operationType", -1);
		String orderSequenceNumber = getUTF("orderSequenceNumber", null);
		String goodName = getUTF("goodName", null);
		String shopName = getUTF("shopName", null);
		long startTimeLong = CommonUtils.dateFormatToLong(getUTF("startTime", ""));
		long endTimeLong = CommonUtils.dateFormatToLong(getUTF("endTime", ""));
		
		Date startTime = startTimeLong == 0L ? null : new Date(startTimeLong);
		Date endTime = endTimeLong == 0L ? null : new Date(endTimeLong);
		
		
		int pageSize = getPageSize();
		int pageStartIndex = getPageStartIndex(pageSize);
		
		List<SupplychainGoodStockLoggerListVo> list = dataAccessManager.getGoodStockLoggerList(type, operationType,
				orderSequenceNumber, goodName, shopName, startTime, endTime, pageStartIndex,
				pageSize);
		
		int total = dataAccessManager.getGoodStockLoggerListCount(type, operationType,
				orderSequenceNumber, goodName, shopName, startTime, endTime);
		
		return success(list, total);
	}

	/**
	 * 批量插入
	 * @author oy
	 * @date 2019/4/25
	 * @param
	 * @return com.alibaba.fastjson.JSONObject
	 */
    @Override
    public JSONObject addSupplychainGoodStockLoggers() {
        String list = getUTF("list");
        List<SupplychainGoodStockLogger> supplychainGoodStockLoggers = JSONObject.parseArray(list, SupplychainGoodStockLogger.class);
        dataAccessManager.addSupplychainGoodStockLoggers(supplychainGoodStockLoggers);
        return success();
    }

}
