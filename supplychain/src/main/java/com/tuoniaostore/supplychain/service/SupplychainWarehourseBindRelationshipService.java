package com.tuoniaostore.supplychain.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @author oy
 * @description
 * @date 2019/5/28
 */
public interface SupplychainWarehourseBindRelationshipService {

    JSONObject getSupplychainWarehourseBindRelationships();

    JSONObject getSupplychainWarehourseBindRelationshipsBySupplier();

    JSONObject listForBindingWareHourse();

    JSONObject bindingWareHourseWithStore();

    JSONObject listForBindingWareHourseDetails();

    JSONObject getSupplychainWarehourseBindRelationshipsByShopId();
}
