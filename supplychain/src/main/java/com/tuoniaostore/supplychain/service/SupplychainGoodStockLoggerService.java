package com.tuoniaostore.supplychain.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 商品的出入库日志
主要是针对商品的库存改变
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainGoodStockLoggerService {

    JSONObject addSupplychainGoodStockLogger();

    JSONObject getSupplychainGoodStockLogger();

    JSONObject getSupplychainGoodStockLoggers();

    JSONObject getSupplychainGoodStockLoggerCount();
    JSONObject getSupplychainGoodStockLoggerAll();

    JSONObject changeSupplychainGoodStockLogger();

	JSONObject getGoodStockLoggerList();

    JSONObject addSupplychainGoodStockLoggers();
}
