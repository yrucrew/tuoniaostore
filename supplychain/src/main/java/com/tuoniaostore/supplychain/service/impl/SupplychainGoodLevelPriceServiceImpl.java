package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodLevelPrice;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainGoodLevelPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainGoodLevelPriceService")
public class SupplychainGoodLevelPriceServiceImpl extends BasicWebService implements SupplychainGoodLevelPriceService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainGoodLevelPrice() {
        SupplychainGoodLevelPrice supplychainGoodLevelPrice = new SupplychainGoodLevelPrice();
        //
        String id = getUTF("id", null);
        //角色ID(仓库ID)
        String roleId = getUTF("roleId", null);
        //关联类型 如：0--商品 1--商品模版
        Integer relationType = getIntParameter("relationType", 0);
        //关联实体ID
        String relationId = getUTF("relationId", null);
        //优惠类型，如：0--设定价格 1--折扣 2--按原价减免
        Integer discountType = getIntParameter("discountType", 0);
        //对应优惠类型的具体值
        Long levelPrice = getLongParameter("levelPrice", 0);
        //匹配的等级模版
        Integer levelTemplateId = getIntParameter("levelTemplateId", 0);
        supplychainGoodLevelPrice.setId(id);
        supplychainGoodLevelPrice.setRoleId(roleId);
        supplychainGoodLevelPrice.setRelationType(relationType);
        supplychainGoodLevelPrice.setRelationId(relationId);
        supplychainGoodLevelPrice.setDiscountType(discountType);
        supplychainGoodLevelPrice.setLevelPrice(levelPrice);
        supplychainGoodLevelPrice.setLevelTemplateId(levelTemplateId);
        dataAccessManager.addSupplychainGoodLevelPrice(supplychainGoodLevelPrice);
        return success();
    }

    @Override
    public JSONObject getSupplychainGoodLevelPrice() {
        SupplychainGoodLevelPrice supplychainGoodLevelPrice = dataAccessManager.getSupplychainGoodLevelPrice(getId());
        return success(supplychainGoodLevelPrice);
    }

    @Override
    public JSONObject getSupplychainGoodLevelPrices() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainGoodLevelPrice> supplychainGoodLevelPrices = dataAccessManager.getSupplychainGoodLevelPrices(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainGoodLevelPriceCount(status, name);
        return success(supplychainGoodLevelPrices, count);
    }

    @Override
    public JSONObject getSupplychainGoodLevelPriceAll() {
        List<SupplychainGoodLevelPrice> supplychainGoodLevelPrices = dataAccessManager.getSupplychainGoodLevelPriceAll();
        return success(supplychainGoodLevelPrices);
    }

    @Override
    public JSONObject getSupplychainGoodLevelPriceCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainGoodLevelPriceCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainGoodLevelPrice() {
        SupplychainGoodLevelPrice supplychainGoodLevelPrice = dataAccessManager.getSupplychainGoodLevelPrice(getId());
        //
        String id = getUTF("id", null);
        //角色ID(仓库ID)
        String roleId = getUTF("roleId", null);
        //关联类型 如：0--商品 1--商品模版
        Integer relationType = getIntParameter("relationType", 0);
        //关联实体ID
        String relationId = getUTF("relationId", null);
        //优惠类型，如：0--设定价格 1--折扣 2--按原价减免
        Integer discountType = getIntParameter("discountType", 0);
        //对应优惠类型的具体值
        Long levelPrice = getLongParameter("levelPrice", 0);
        //匹配的等级模版
        Integer levelTemplateId = getIntParameter("levelTemplateId", 0);
        supplychainGoodLevelPrice.setId(id);
        supplychainGoodLevelPrice.setRoleId(roleId);
        supplychainGoodLevelPrice.setRelationType(relationType);
        supplychainGoodLevelPrice.setRelationId(relationId);
        supplychainGoodLevelPrice.setDiscountType(discountType);
        supplychainGoodLevelPrice.setLevelPrice(levelPrice);
        supplychainGoodLevelPrice.setLevelTemplateId(levelTemplateId);
        dataAccessManager.changeSupplychainGoodLevelPrice(supplychainGoodLevelPrice);
        return success();
    }

}
