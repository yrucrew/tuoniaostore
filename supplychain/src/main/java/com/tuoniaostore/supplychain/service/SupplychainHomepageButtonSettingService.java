package com.tuoniaostore.supplychain.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 首页按钮配置
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-04 14:40:15
 */
public interface SupplychainHomepageButtonSettingService {

    JSONObject addSupplychainHomepageButtonSetting();

    JSONObject getSupplychainHomepageButtonSetting();

    JSONObject getSupplychainHomepageButtonSettings();

    JSONObject getSupplychainHomepageButtonSettingCount();

    JSONObject getSupplychainHomepageButtonSettingAll();

    JSONObject changeSupplychainHomepageButtonSetting();

    JSONObject getSupplychainHomepageButton();

    JSONObject validateSupplychainHomepageButton();

    JSONObject batchDeleteSupplychainHomepageButton();
}
