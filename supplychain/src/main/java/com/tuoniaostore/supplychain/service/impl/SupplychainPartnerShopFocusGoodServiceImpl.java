package com.tuoniaostore.supplychain.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.supplychain.SupplychainPartnerShopFocusGood;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainPartnerShopFocusGoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("supplychainPartnerShopFocusGoodService")
public class SupplychainPartnerShopFocusGoodServiceImpl extends BasicWebService implements SupplychainPartnerShopFocusGoodService {
    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSupplychainPartnerShopFocusGood() {
        SupplychainPartnerShopFocusGood supplychainPartnerShopFocusGood = new SupplychainPartnerShopFocusGood();
        //
        String id = getUTF("id", null);
        //合作方ID
        String partnerId = getUTF("partnerId", null);
        //我方实体ID
        String shopId = getUTF("shopId", null);
        //关注的类型 如：0-商品 1-商品类型
        Integer focusType = getIntParameter("focusType", 0);
        //关注的实体数据ID 当focus_type为0时 表示商品ID 当focus_type为1时 表示商品模版ID
        String focusEntryId = getUTF("focusEntryId", null);
        //状态 0-无效 1-有效
        Integer statuc = getIntParameter("statuc", 0);
        supplychainPartnerShopFocusGood.setId(id);
        supplychainPartnerShopFocusGood.setPartnerId(partnerId);
        supplychainPartnerShopFocusGood.setShopId(shopId);
        supplychainPartnerShopFocusGood.setFocusType(focusType);
        supplychainPartnerShopFocusGood.setFocusEntryId(focusEntryId);
        supplychainPartnerShopFocusGood.setStatuc(statuc);
        dataAccessManager.addSupplychainPartnerShopFocusGood(supplychainPartnerShopFocusGood);
        return success();
    }

    @Override
    public JSONObject getSupplychainPartnerShopFocusGood() {
        SupplychainPartnerShopFocusGood supplychainPartnerShopFocusGood = dataAccessManager.getSupplychainPartnerShopFocusGood(getId());
        return success(supplychainPartnerShopFocusGood);
    }

    @Override
    public JSONObject getSupplychainPartnerShopFocusGoods() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SupplychainPartnerShopFocusGood> supplychainPartnerShopFocusGoods = dataAccessManager.getSupplychainPartnerShopFocusGoods(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSupplychainPartnerShopFocusGoodCount(status, name);
        return success(supplychainPartnerShopFocusGoods, count);
    }

    @Override
    public JSONObject getSupplychainPartnerShopFocusGoodAll() {
        List<SupplychainPartnerShopFocusGood> supplychainPartnerShopFocusGoods = dataAccessManager.getSupplychainPartnerShopFocusGoodAll();
        return success(supplychainPartnerShopFocusGoods);
    }

    @Override
    public JSONObject getSupplychainPartnerShopFocusGoodCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSupplychainPartnerShopFocusGoodCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSupplychainPartnerShopFocusGood() {
        SupplychainPartnerShopFocusGood supplychainPartnerShopFocusGood = dataAccessManager.getSupplychainPartnerShopFocusGood(getId());
        //
        String id = getUTF("id", null);
        //合作方ID
        String partnerId = getUTF("partnerId", null);
        //我方实体ID
        String shopId = getUTF("shopId", null);
        //关注的类型 如：0-商品 1-商品类型
        Integer focusType = getIntParameter("focusType", 0);
        //关注的实体数据ID 当focus_type为0时 表示商品ID 当focus_type为1时 表示商品模版ID
        String focusEntryId = getUTF("focusEntryId", null);
        //状态 0-无效 1-有效
        Integer statuc = getIntParameter("statuc", 0);
        supplychainPartnerShopFocusGood.setId(id);
        supplychainPartnerShopFocusGood.setPartnerId(partnerId);
        supplychainPartnerShopFocusGood.setShopId(shopId);
        supplychainPartnerShopFocusGood.setFocusType(focusType);
        supplychainPartnerShopFocusGood.setFocusEntryId(focusEntryId);
        supplychainPartnerShopFocusGood.setStatuc(statuc);
        dataAccessManager.changeSupplychainPartnerShopFocusGood(supplychainPartnerShopFocusGood);
        return success();
    }

}
