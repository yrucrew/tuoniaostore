package com.tuoniaostore.supplychain.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainShopDailyGoodSalesLoggerService {

    JSONObject addSupplychainShopDailyGoodSalesLogger();

    JSONObject getSupplychainShopDailyGoodSalesLogger();

    JSONObject getSupplychainShopDailyGoodSalesLoggers();

    JSONObject getSupplychainShopDailyGoodSalesLoggerCount();
    JSONObject getSupplychainShopDailyGoodSalesLoggerAll();

    JSONObject changeSupplychainShopDailyGoodSalesLogger();
}
