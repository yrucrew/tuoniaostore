package com.tuoniaostore.supplychain.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-06 10:47:35
 */
public interface PosWorkService {

    JSONObject addPosWork();

    JSONObject getPosWork();

    JSONObject getPosWorks();

    JSONObject getPosWorkCount();
    JSONObject getPosWorkAll();

    JSONObject changePosWork();

    JSONObject addBusinessShift();

    JSONObject endBusinessShift();

    JSONObject addPosWorkModel();

    JSONObject isExchangeDuty();
    JSONObject getPosWrokListByMap();

    JSONObject getPosWrokDetil();
    JSONObject getPosWorkByUserId();
}
