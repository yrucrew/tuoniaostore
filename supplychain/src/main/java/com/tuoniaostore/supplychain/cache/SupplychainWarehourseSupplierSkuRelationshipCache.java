package com.tuoniaostore.supplychain.cache;

import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseSupplierSkuRelationship;

import java.util.List;
import java.util.Map;

public interface SupplychainWarehourseSupplierSkuRelationshipCache {

    List<SupplychainWarehourseSupplierSkuRelationship> getSupplychainWarehourseSupplierSkuRelationshipByParam(Map<String,Object> map);
    List<SupplychainWarehourseSupplierSkuRelationship> getSupplychainWarehourseSupplierSkuRelationshipById(Map<String,Object> map);

    List<SupplychainWarehourseSupplierSkuRelationship> getSupplychainWarehourseSupplierSkuRelationshipByShopIdAndNotSupplierId(Map<String, Object> map);

    void batchAddSupplychainWarehourseSupplierSkuRelationship(List<SupplychainWarehourseSupplierSkuRelationship> addList);

    void clearSupplychainWarehourseSupplierSkuRelationshipByShopIdAndsupplierId(String shopId, String supplierId);
}
