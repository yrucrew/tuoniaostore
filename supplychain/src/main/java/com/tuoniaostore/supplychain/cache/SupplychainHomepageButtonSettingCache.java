package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageButtonSetting;

import java.util.List;
/**
 * 首页按钮配置
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-04 14:40:15
 */
public interface SupplychainHomepageButtonSettingCache {

    void addSupplychainHomepageButtonSetting(SupplychainHomepageButtonSetting supplychainHomepageButtonSetting);

    SupplychainHomepageButtonSetting getSupplychainHomepageButtonSetting(String id);

    List<SupplychainHomepageButtonSetting> getSupplychainHomepageButtonSettings(int status, int pageIndex, int pageSize, String name, int retrieveStatus);

    List<SupplychainHomepageButtonSetting> getSupplychainHomepageButtonSettingAll();

    int getSupplychainHomepageButtonSettingCount(int status, String name, int retrieveStatus);

    void changeSupplychainHomepageButtonSetting(SupplychainHomepageButtonSetting supplychainHomepageButtonSetting);

    List<SupplychainHomepageButtonSetting> getSupplychainHomepageButton();

    SupplychainHomepageButtonSetting validateSupplychainHomepageButton(String name, Integer type);

    void batchDeleteSupplychainHomepageButton(List<String> stringList);
}
