package com.tuoniaostore.supplychain.cache.impl;

import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseSupplierSkuRelationship;
import com.tuoniaostore.supplychain.cache.SupplychainWarehourseSupplierSkuRelationshipCache;
import com.tuoniaostore.supplychain.data.SupplychainWarehourseSupplierSkuRelationshipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/6/3
 */
@Component
@CacheConfig(cacheNames = "SupplychainWarehourseSupplierSkuRelationship")
public class SupplychainWarehourseSupplierSkuRelationshipCacheImpl implements SupplychainWarehourseSupplierSkuRelationshipCache {

    @Autowired
    SupplychainWarehourseSupplierSkuRelationshipMapper mapper;

    @Override
    @Cacheable(value = "SupplychainWarehourseSupplierSkuRelationship", key = "'getSupplychainWarehourseSupplierSkuRelationshipByParam_'+#map")
    public List<SupplychainWarehourseSupplierSkuRelationship> getSupplychainWarehourseSupplierSkuRelationshipByParam(Map<String, Object> map) {
        return mapper.getSupplychainWarehourseSupplierSkuRelationshipByParam(map);
    }

    @Override
    @Cacheable(value = "SupplychainWarehourseSupplierSkuRelationship", key = "'getSupplychainWarehourseSupplierSkuRelationshipById_'+#map")
    public List<SupplychainWarehourseSupplierSkuRelationship> getSupplychainWarehourseSupplierSkuRelationshipById(Map<String, Object> map) {
        return mapper.getSupplychainWarehourseSupplierSkuRelationshipById(map);
    }

    @Override
    @Cacheable(value = "SupplychainWarehourseSupplierSkuRelationship", key = "'getSupplychainWarehourseSupplierSkuRelationshipByShopIdAndNotSupplierId_'+#map")
    public List<SupplychainWarehourseSupplierSkuRelationship> getSupplychainWarehourseSupplierSkuRelationshipByShopIdAndNotSupplierId(Map<String, Object> map) {
        return mapper.getSupplychainWarehourseSupplierSkuRelationshipByShopIdAndNotSupplierId(map);
    }

    @Override
    @CacheEvict(value = "SupplychainWarehourseSupplierSkuRelationship", allEntries = true)
    public void batchAddSupplychainWarehourseSupplierSkuRelationship(List<SupplychainWarehourseSupplierSkuRelationship> addList) {
        mapper.batchAddSupplychainWarehourseSupplierSkuRelationship(addList);
    }

    @Override
    @CacheEvict(value = "SupplychainWarehourseSupplierSkuRelationship", allEntries = true)
    public void clearSupplychainWarehourseSupplierSkuRelationshipByShopIdAndsupplierId(String shopId, String supplierId) {
        mapper.clearSupplychainWarehourseSupplierSkuRelationshipByShopIdAndsupplierId(shopId,supplierId);
    }
}
