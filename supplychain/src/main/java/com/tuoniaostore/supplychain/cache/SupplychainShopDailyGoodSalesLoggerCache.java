package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopDailyGoodSalesLogger;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainShopDailyGoodSalesLoggerCache {

    void addSupplychainShopDailyGoodSalesLogger(SupplychainShopDailyGoodSalesLogger supplychainShopDailyGoodSalesLogger);

    SupplychainShopDailyGoodSalesLogger getSupplychainShopDailyGoodSalesLogger(String id);

    List<SupplychainShopDailyGoodSalesLogger> getSupplychainShopDailyGoodSalesLoggers(int status, int pageIndex, int pageSize, String name);
    List<SupplychainShopDailyGoodSalesLogger> getSupplychainShopDailyGoodSalesLoggerAll();
    int getSupplychainShopDailyGoodSalesLoggerCount(int status, String name);
    void changeSupplychainShopDailyGoodSalesLogger(SupplychainShopDailyGoodSalesLogger supplychainShopDailyGoodSalesLogger);

}
