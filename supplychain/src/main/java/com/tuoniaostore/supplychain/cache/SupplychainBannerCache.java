package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainBanner;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainBannerCache {

    /**
     * 添加
     */
    void addSupplychainBanner(SupplychainBanner supplychainBanner);

    SupplychainBanner getSupplychainBanner(String id);

    List<SupplychainBanner> getSupplychainBanners(int status, int pageIndex, int pageSize, String name);

    List<SupplychainBanner> getSupplychainBannerAll();

    int getSupplychainBannerCount(int status, String name);

    /**
     * 修改
     */
    void changeSupplychainBanner(SupplychainBanner supplychainBanner);

    List<SupplychainBanner> getSupplychainBannerByShopIdAndAreaId(String shopId, String areaId);

    /**
     * 根据id批量删除
     */
    void batchDeleteSupplychainBanner(List<String> list);

    List<SupplychainBanner> getSupplychainBannersByShopIdAndAreaId(String shopId, String areaId);
}
