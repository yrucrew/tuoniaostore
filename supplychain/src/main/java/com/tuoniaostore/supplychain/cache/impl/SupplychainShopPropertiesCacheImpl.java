package com.tuoniaostore.supplychain.cache.impl;

import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.supplychain.cache.SupplychainShopPropertiesCache;
import com.tuoniaostore.supplychain.data.SupplychainShopPropertiesMapper;
import com.tuoniaostore.supplychain.vo.ShopReportMapperVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 仓库的属性
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainShopProperties")
public class SupplychainShopPropertiesCacheImpl implements SupplychainShopPropertiesCache {

    @Autowired
    SupplychainShopPropertiesMapper mapper;

    @Override
    @CacheEvict(value = "supplychainShopProperties", allEntries = true)
    public void addSupplychainShopProperties(SupplychainShopProperties supplychainShopProperties) {
        mapper.addSupplychainShopProperties(supplychainShopProperties);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopProperties_'+#id")
    public SupplychainShopProperties getSupplychainShopProperties(String id) {
        return mapper.getSupplychainShopProperties(id);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiess_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name+'_'+#type+'_'+#auth")
    public List<SupplychainShopProperties> getSupplychainShopPropertiess(int status, int pageIndex, int pageSize, String name,String type,int auth) {
        String[] str = type.split(",");
        int len = str.length;
        Integer[] types = new Integer[len];
        for (int i = 0; i < len; i++) {
            if (str[i] != null && str[i].length() > 0) {
                types[i] = Integer.parseInt(str[i]);
            }
        }
        return mapper.getSupplychainShopPropertiess(status, pageIndex, pageSize, name,types, auth);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiess2_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#nameOrPhone+'_'+#type+'_'+#auth+'_'+#map")
    public List<SupplychainShopProperties> getSupplychainShopPropertiess2(int status, Integer pageStartIndex, Integer pageSize, String nameOrPhone, String type, int auth, Map<String,String> map) {
        String[] str = type.split(",");
        int len = str.length;
        Integer[] types = new Integer[len];
        for (int i = 0; i < len; i++) {
            if (str[i] != null && str[i].length() > 0) {
                types[i] = Integer.parseInt(str[i]);
            }
        }
        return mapper.getSupplychainShopPropertiess2(status, pageStartIndex, pageSize, nameOrPhone,types, auth,map);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesAll_'+#status+'_'+#type")
    public List<SupplychainShopProperties> getSupplychainShopPropertiesAll(int status, String type) {
        String[] str = type.split(",");
        int len = str.length;
        Integer[] types = new Integer[len];
        for (int i = 0; i < len; i++) {
            if (str[i] != null && str[i].length() > 0) {
                types[i] = Integer.parseInt(str[i]);
            }
        }
        return mapper.getSupplychainShopPropertiesAll(status, types);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesCount_'+#status+'_'+#name+'_'+#type+'_'+#map")
    public int getSupplychainShopPropertiesCount(int status, String name,String type, int auth,Map<String,String> map) {
        String[] str = type.split(",");
        int len = str.length;
        Integer[] types = new Integer[len];
        for (int i = 0; i < len; i++) {
            if (str[i] != null && str[i].length() > 0) {
                types[i] = Integer.parseInt(str[i]);
            }
        }
        return mapper.getSupplychainShopPropertiesCount(status, name,types, auth,map);
    }

    @Override
    @CacheEvict(value = "supplychainShopProperties", allEntries = true)
    public void changeSupplychainShopProperties(SupplychainShopProperties supplychainShopProperties) {
        mapper.changeSupplychainShopProperties(supplychainShopProperties);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesByUserId_'+#userId")
    public SupplychainShopProperties getSupplychainShopPropertiesByUserId(String userId) {
        return mapper.getSupplychainShopPropertiesByUserId(userId);
    }

    @Override
    @CacheEvict(value = "supplychainShopProperties", allEntries = true)
    public void changeSupplychainShopPropertiesById(String userId, String name) {
        mapper.changeSupplychainShopPropertiesById(userId,name);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'selectSupplychainShopPropertiesById_'+#userId+'_'+#type")
    public SupplychainShopProperties selectSupplychainShopPropertiesById(String userId, int type) {
        return mapper.selectSupplychainShopPropertiesById(userId, type);
    }

    @Override
    @CacheEvict(value = "supplychainShopProperties", allEntries = true)
    public void changeSupplychainShopPropertiesStatusByIdAndTypes(String userId, List<Integer> types,int status) {
         mapper.selectSupplychainShopPropertiesByIdAndTypes(userId,types,status);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesByUserIdAndTypeId'+#userId+'_'+#list")
    public SupplychainShopProperties getSupplychainShopPropertiesByUserIdAndTypeId(String userId, List<Integer> list) {
        return mapper.getSupplychainShopPropertiesByUserIdAndTypeId(userId,list);
    }

    @Override
    @CacheEvict(value = "supplychainShopProperties", allEntries = true)
    public void changeSupplychainShopPropertiesCoveringDistanceByIdAndTypes(String userId, List<Integer> list, int coveringDistance) {
        mapper.changeSupplychainShopPropertiesCoveringDistanceByIdAndTypes(userId,list,coveringDistance);
    }

    @Override
    @CacheEvict(value = "supplychainShopProperties", allEntries = true)
    public void changeSupplychainShopPropertiesBindPrinterCodeByIdAndTypes(String userId, List<Integer> list, String bindPrinterCode) {
        mapper.changeSupplychainShopPropertiesBindPrinterCodeByIdAndTypes(userId,list,bindPrinterCode);
    }

    @Override
    @CacheEvict(value = "supplychainShopProperties", allEntries = true)
    public void changeSupplychainShopPropertiesPhoneNumber(String userId, List<Integer> list, String phoneNumber) {
        mapper.changeSupplychainShopPropertiesPhoneNumber(userId,list,phoneNumber);
    }

    @Override
    public List<SupplychainShopProperties> supplychainShopPropertiesAll() {
        return mapper.supplychainShopPropertiesAll();
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesByType'+#types")
    public List<SupplychainShopProperties> getSupplychainShopPropertiesByType(List<String> types, Integer pageStartIndex, Integer pageSize) {
        return mapper.getSupplychainShopPropertiesByType(types,pageStartIndex,pageSize);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesByTypeCount_'+'_'+#list")
    public int getSupplychainShopPropertiesByTypeCount(List<String> list) {
        return mapper.getSupplychainShopPropertiesByTypeCount(list);
    }

    @Override
    public List<SupplychainShopProperties> getOwnAndUnbindShopProperties(String shopName, String shopId, List<Integer> types, int pageStartIndex, int pageSize) {
        return mapper.getOwnAndUnbindShopProperties(shopName, shopId, types, pageStartIndex, pageSize);
    }

    @Override
    public int getOwnAndUnbindShopPropertiesCount(String shopId, List<Integer> types) {
        return mapper.getOwnAndUnbindShopPropertiesCount(shopId, types);
    }

    @Override
    public List<ShopReportMapperVO>  getSupplychainShopPropertiesByTypeCountForEveryDay(List<String> list, String startTime, String endTime) {
        return mapper.getSupplychainShopPropertiesByTypeCountForEveryDay(list, startTime,endTime);
    }

    @Override
    public List<ShopReportMapperVO>  getSupplychainShopPropertiesByTypeCountForEveryWeek(List<String> list, String startTime, String endTime) {
        return mapper.getSupplychainShopPropertiesByTypeCountForEveryWeek(list, startTime,endTime);
    }

    @Override
    public List<ShopReportMapperVO>  getSupplychainShopPropertiesByTypeCountForEveryMonth(List<String> list, String startTime, String endTime) {
        return mapper.getSupplychainShopPropertiesByTypeCountForEveryMonth(list, startTime,endTime);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesByIds'+#id")
    public SupplychainShopProperties getSupplychainShopPropertiesById(String id) {
        return mapper.getSupplychainShopPropertiesById(id);
    }

	@Override
	@CacheEvict(value = "supplychainShopProperties", allEntries = true)
	public int authSupplychainShopProperties(String id, int auth) {
		return mapper.authSupplychainShopProperties(id, auth);
	}

	@Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesByIds_'+#shopIds")
	public List<SupplychainShopProperties> getSupplychainShopPropertiesByIds(List<String> shopIds) {
		return mapper.getSupplychainShopPropertiesByIds(shopIds);
	}

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesByIdsCount_'+#shopIds")
    public int getSupplychainShopPropertiesByIdsCount(List<String> shopIds) {
        return mapper.getSupplychainShopPropertiesByIdsCount(shopIds);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesByIdsPage_'+#shopIds+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainShopProperties> getSupplychainShopPropertiesByIdsPage(List<String> shopIds, Integer pageStartIndex, Integer pageSize) {
        return mapper.getSupplychainShopPropertiesByIdsPage(shopIds,pageStartIndex,pageSize);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesByShopName_'+#shopName+'_'+#types+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainShopProperties> getSupplychainShopPropertiesByShopName(String shopName, List<Integer> types, Integer pageStartIndex, Integer pageSize) {
        return mapper.getSupplychainShopPropertiesByShopName(shopName,types,pageStartIndex,pageSize);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesByShopNameCount_'+#shopName+'_'+#types")
    public int getSupplychainShopPropertiesByShopNameCount(String shopName, List<Integer> types) {
        return mapper.getSupplychainShopPropertiesByShopNameCount(shopName,types);
    }

    @Override
    @CacheEvict(value = "supplychainShopProperties", allEntries = true)
    public void deleteSupplychainShopProperties(List<String> id) {
        mapper.deleteSupplychainShopProperties(id);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesByShopNameAndUserId_'+#shopName+'_'+#userId+'_'+#typeIds+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainShopProperties> getSupplychainShopPropertiesByShopNameAndUserId(String shopName, List<String> userId,List<Integer>typeIds, Integer pageStartIndex, Integer pageSize) {
        return mapper.getSupplychainShopPropertiesByShopNameAndUserId(shopName,userId,typeIds,pageStartIndex,pageSize);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getCountSupplychainShopPropertiesByShopNameAndUserId_'+#typeIds+'_'+#shopName+'_'+#userId")
    public int getCountSupplychainShopPropertiesByShopNameAndUserId(String shopName,List<Integer>typeIds, List<String> userId) {
        return mapper.getCountSupplychainShopPropertiesByShopNameAndUserId(shopName,typeIds,userId);
    }

    @Override
    @Cacheable(value = "supplychainShopProperties", key = "'getSupplychainShopPropertiesForBindingSelect_'+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainShopProperties> getSupplychainShopPropertiesForBindingSelect(Integer pageStartIndex, Integer pageSize) {
        return mapper.getSupplychainShopPropertiesForBindingSelect(pageStartIndex,pageSize);
    }

    @Override
    @CacheEvict(value = "supplychainShopProperties", allEntries = true)
    public void updateSupplychainShopPropertiesByParam(Map<String, Object> map) {
        mapper.updateSupplychainShopPropertiesByParam(map);
    }

    @Override
    public SupplychainShopProperties UpdateBindPrinterCodeByUserId(String userId, String bindPrinterCode) {
        return mapper.UpdateBindPrinterCodeByUserId(userId, bindPrinterCode);
    }


}
