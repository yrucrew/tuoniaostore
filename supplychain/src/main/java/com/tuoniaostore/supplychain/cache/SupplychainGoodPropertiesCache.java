package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodProperties;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainGoodPropertiesCache {

    void addSupplychainGoodProperties(SupplychainGoodProperties supplychainGoodProperties);

    SupplychainGoodProperties getSupplychainGoodProperties(String id);

    List<SupplychainGoodProperties> getSupplychainGoodPropertiess(int status, int pageIndex, int pageSize, String name);
    List<SupplychainGoodProperties> getSupplychainGoodPropertiesAll();
    int getSupplychainGoodPropertiesCount(int status, String name );
    void changeSupplychainGoodProperties(SupplychainGoodProperties supplychainGoodProperties);

}
