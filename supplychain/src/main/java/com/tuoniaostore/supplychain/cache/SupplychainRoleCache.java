package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainRole;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainRoleCache {

    void addSupplychainRole(SupplychainRole supplychainRole);

    SupplychainRole getSupplychainRole(String id);

    List<SupplychainRole> getSupplychainRoles(int status, int pageIndex, int pageSize, String name);
    List<SupplychainRole> getSupplychainRoleAll();
    int getSupplychainRoleCount(int status, String name);
    void changeSupplychainRole(SupplychainRole supplychainRole);

}
