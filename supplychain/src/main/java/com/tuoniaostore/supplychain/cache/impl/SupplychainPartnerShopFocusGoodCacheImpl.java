package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainPartnerShopFocusGood;
        import com.tuoniaostore.supplychain.cache.SupplychainPartnerShopFocusGoodCache;
        import com.tuoniaostore.supplychain.data.SupplychainPartnerShopFocusGoodMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainPartnerShopFocusGood")
public class SupplychainPartnerShopFocusGoodCacheImpl implements SupplychainPartnerShopFocusGoodCache {

    @Autowired
    SupplychainPartnerShopFocusGoodMapper mapper;

    @Override
    @CacheEvict(value = "supplychainPartnerShopFocusGood", allEntries = true)
    public void addSupplychainPartnerShopFocusGood(SupplychainPartnerShopFocusGood supplychainPartnerShopFocusGood) {
        mapper.addSupplychainPartnerShopFocusGood(supplychainPartnerShopFocusGood);
    }

    @Override
    @Cacheable(value = "supplychainPartnerShopFocusGood", key = "'getSupplychainPartnerShopFocusGood_'+#id")
    public   SupplychainPartnerShopFocusGood getSupplychainPartnerShopFocusGood(String id) {
        return mapper.getSupplychainPartnerShopFocusGood(id);
    }

    @Override
    @Cacheable(value = "supplychainPartnerShopFocusGood", key = "'getSupplychainPartnerShopFocusGoods_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainPartnerShopFocusGood> getSupplychainPartnerShopFocusGoods(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainPartnerShopFocusGoods(status, pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "supplychainPartnerShopFocusGood", key = "'getSupplychainPartnerShopFocusGoodAll'")
    public List<SupplychainPartnerShopFocusGood> getSupplychainPartnerShopFocusGoodAll() {
        return mapper.getSupplychainPartnerShopFocusGoodAll();
    }

    @Override
    @Cacheable(value = "supplychainPartnerShopFocusGood", key = "'getSupplychainPartnerShopFocusGoodCount_'+#status+'_'+#name")
    public int getSupplychainPartnerShopFocusGoodCount(int status, String name) {
        return mapper.getSupplychainPartnerShopFocusGoodCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainPartnerShopFocusGood", allEntries = true)
    public void changeSupplychainPartnerShopFocusGood(SupplychainPartnerShopFocusGood supplychainPartnerShopFocusGood) {
        mapper.changeSupplychainPartnerShopFocusGood(supplychainPartnerShopFocusGood);
    }

}
