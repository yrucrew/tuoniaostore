package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainPartnerCallback;
        import com.tuoniaostore.supplychain.cache.SupplychainPartnerCallbackCache;
        import com.tuoniaostore.supplychain.data.SupplychainPartnerCallbackMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainPartnerCallback")
public class SupplychainPartnerCallbackCacheImpl implements SupplychainPartnerCallbackCache {

    @Autowired
    SupplychainPartnerCallbackMapper mapper;

    @Override
    @CacheEvict(value = "supplychainPartnerCallback", allEntries = true)
    public void addSupplychainPartnerCallback(SupplychainPartnerCallback supplychainPartnerCallback) {
        mapper.addSupplychainPartnerCallback(supplychainPartnerCallback);
    }

    @Override
    @Cacheable(value = "supplychainPartnerCallback", key = "'getSupplychainPartnerCallback_'+#id")
    public   SupplychainPartnerCallback getSupplychainPartnerCallback(String id) {
        return mapper.getSupplychainPartnerCallback(id);
    }

    @Override
    @Cacheable(value = "supplychainPartnerCallback", key = "'getSupplychainPartnerCallbacks_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainPartnerCallback> getSupplychainPartnerCallbacks(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainPartnerCallbacks( status,pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "supplychainPartnerCallback", key = "'getSupplychainPartnerCallbackAll'")
    public List<SupplychainPartnerCallback> getSupplychainPartnerCallbackAll() {
        return mapper.getSupplychainPartnerCallbackAll();
    }

    @Override
    @Cacheable(value = "supplychainPartnerCallback", key = "'getSupplychainPartnerCallbackCount_'+#status+'_'+#name")
    public int getSupplychainPartnerCallbackCount(int status, String name) {
        return mapper.getSupplychainPartnerCallbackCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainPartnerCallback", allEntries = true)
    public void changeSupplychainPartnerCallback(SupplychainPartnerCallback supplychainPartnerCallback) {
        mapper.changeSupplychainPartnerCallback(supplychainPartnerCallback);
    }

}
