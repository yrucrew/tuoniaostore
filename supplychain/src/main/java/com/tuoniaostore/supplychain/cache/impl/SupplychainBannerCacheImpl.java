package com.tuoniaostore.supplychain.cache.impl;

import com.tuoniaostore.datamodel.supplychain.SupplychainBanner;
import com.tuoniaostore.supplychain.cache.SupplychainBannerCache;
import com.tuoniaostore.supplychain.data.SupplychainBannerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainBanner")
public class SupplychainBannerCacheImpl implements SupplychainBannerCache {

    @Autowired
    SupplychainBannerMapper mapper;

    @Override
    @CacheEvict(value = "supplychainBanner", allEntries = true)
    public void addSupplychainBanner(SupplychainBanner supplychainBanner) {
        mapper.addSupplychainBanner(supplychainBanner);
    }

    @Override
    @Cacheable(value = "supplychainBanner", key = "'getSupplychainBanner_'+#id")
    public SupplychainBanner getSupplychainBanner(String id) {
        return mapper.getSupplychainBanner(id);
    }

    @Override
    @Cacheable(value = "supplychainBanner", key = "'getSupplychainBanners_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainBanner> getSupplychainBanners(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainBanners(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "supplychainBanner", key = "'getSupplychainBannerAll'")
    public List<SupplychainBanner> getSupplychainBannerAll() {
        return mapper.getSupplychainBannerAll();
    }

    @Override
    @Cacheable(value = "supplychainBanner", key = "'getSupplychainBannerCount_'+#status+'_'+#name")
    public int getSupplychainBannerCount(int status, String name) {

        return mapper.getSupplychainBannerCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainBanner", allEntries = true)
    public void changeSupplychainBanner(SupplychainBanner supplychainBanner) {
        mapper.changeSupplychainBanner(supplychainBanner);
    }

    @Override
    @Cacheable(value = "supplychainBanner", key = "'getSupplychainBannerByShopIdAndAreaId_'+#shopId+'_'+#areaId")
    public List<SupplychainBanner> getSupplychainBannerByShopIdAndAreaId(String shopId, String areaId) {
        return mapper.getSupplychainBannerByShopIdAndAreaId(shopId,areaId);
    }


    @Override
    @CacheEvict(value = "supplychainBanner", allEntries = true)
    public void batchDeleteSupplychainBanner(List<String> list) {
        mapper.batchDeleteSupplychainBanner(list);
    }

    @Override
    public List<SupplychainBanner> getSupplychainBannersByShopIdAndAreaId(String shopId, String areaId) {
        return mapper.getSupplychainBannersByShopIdAndAreaId(shopId, areaId);
    }

}
