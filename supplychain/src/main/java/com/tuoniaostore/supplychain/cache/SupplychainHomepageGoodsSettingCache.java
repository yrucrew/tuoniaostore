package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageGoodsSetting;
import com.tuoniaostore.datamodel.vo.supplychain.SupplychainHomePageGoodDetail;

import java.util.List;
/**
 * 首页设置表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-04 14:40:15
 */
public interface SupplychainHomepageGoodsSettingCache {

    void addSupplychainHomepageGoodsSetting(SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting);

    SupplychainHomepageGoodsSetting getSupplychainHomepageGoodsSetting(String id);

    List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettings( int pageIndex, int pageSize, String name);
    List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettingAll();
    int getSupplychainHomepageGoodsSettingCount( String name);
    void changeSupplychainHomepageGoodsSetting(SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting);
    List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettingByAreaIdAndType(String areaId, Integer type,Integer pageIndex,Integer pageSize);

    List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettingByAreaIdsAndType(List<String> areaId, Integer type, Integer pageIndex, Integer pageSize);

    void bacchDeleteHomeSettingBy(List<String> ids);

    void addSupplychainHomepageGoodsSettings(List<SupplychainHomepageGoodsSetting> list);

    void batchChangeHomeGoods(List<SupplychainHomepageGoodsSetting> list);

    void DeleteHomeSettingByAreaId(String areaId,Integer typeNum);
}
