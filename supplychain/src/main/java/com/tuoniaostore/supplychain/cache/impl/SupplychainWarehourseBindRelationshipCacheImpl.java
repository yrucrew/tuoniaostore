package com.tuoniaostore.supplychain.cache.impl;

import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseBindRelationship;
import com.tuoniaostore.supplychain.cache.SupplychainWarehourseBindRelationshipCache;
import com.tuoniaostore.supplychain.data.SupplychainWarehourseBindRelationshipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author oy
 * @description
 * @date 2019/5/28
 */
@Component
@CacheConfig(cacheNames = "supplychainWarehourseBindRelationship")
public class SupplychainWarehourseBindRelationshipCacheImpl  implements SupplychainWarehourseBindRelationshipCache {

    @Autowired
    SupplychainWarehourseBindRelationshipMapper mapper;

    @Override
    @Cacheable(value = "supplychainWarehourseBindRelationship", key = "'getSupplychainWarehourseBindRelationships_'+#pageIndex+'_'+#pageSize")
    public List<SupplychainWarehourseBindRelationship> getSupplychainWarehourseBindRelationships(Integer pageIndex, Integer pageSize) {
        return mapper.getSupplychainWarehourseBindRelationships(pageIndex,pageSize);
    }

    @Override
    @Cacheable(value = "supplychainWarehourseBindRelationship", key = "'getSupplychainWarehourseBindRelationshipsByShopIdsAndType_'+#shopId+'_'+#type+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainWarehourseBindRelationship> getSupplychainWarehourseBindRelationshipsByShopIdsAndType(String shopId, int type, Integer pageStartIndex, Integer pageSize) {
        return mapper.getSupplychainWarehourseBindRelationshipsByShopIdsAndType(shopId,type,pageStartIndex,pageSize);
    }

    @Override
    @CacheEvict(value = "supplychainWarehourseBindRelationship", allEntries = true)
    public void deleteSupplychainWarehourseBindRelationship(String shopId ,int type) {
        mapper.deleteSupplychainWarehourseBindRelationship(shopId,type);
    }

    @Override
    @CacheEvict(value = "supplychainWarehourseBindRelationship", allEntries = true)
    public void batchAddSupplychainWarehourseBindRelationships(List<SupplychainWarehourseBindRelationship> entity) {
        mapper.batchAddSupplychainWarehourseBindRelationships(entity);
    }

    @Override
    @Cacheable(value = "supplychainWarehourseBindRelationship", key = "'getSupplychainWarehourseBindRelationshipsByBindShopId_'+#bindShopId+'_'+#type")
    public SupplychainWarehourseBindRelationship getSupplychainWarehourseBindRelationshipsByBindShopId(String bindShopId,Integer type) {
        return mapper.getSupplychainWarehourseBindRelationshipsByBindShopId(bindShopId,type);
    }

    @Override
    @Cacheable(value = "supplychainWarehourseBindRelationship", key = "'getSupplychainWarehourseBindRelationshipsByShopId_'+#shopId+'_'+#type")
    public SupplychainWarehourseBindRelationship getSupplychainWarehourseBindRelationshipsByShopId(String shopId, Integer type) {
        return mapper.getSupplychainWarehourseBindRelationshipsByBindShopId(shopId,type);
    }


}
