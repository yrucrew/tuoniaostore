package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainGoodBindRelationship;
        import com.tuoniaostore.supplychain.cache.SupplychainGoodBindRelationshipCache;
        import com.tuoniaostore.supplychain.data.SupplychainGoodBindRelationshipMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainGoodBindRelationship")
public class SupplychainGoodBindRelationshipCacheImpl implements SupplychainGoodBindRelationshipCache {

    @Autowired
    SupplychainGoodBindRelationshipMapper mapper;

    @Override
    @CacheEvict(value = "supplychainGoodBindRelationship", allEntries = true)
    public void addSupplychainGoodBindRelationship(SupplychainGoodBindRelationship supplychainGoodBindRelationship) {
        mapper.addSupplychainGoodBindRelationship(supplychainGoodBindRelationship);
    }

    @Override
    @Cacheable(value = "supplychainGoodBindRelationship", key = "'getSupplychainGoodBindRelationship_'+#id")
    public   SupplychainGoodBindRelationship getSupplychainGoodBindRelationship(String id) {
        return mapper.getSupplychainGoodBindRelationship(id);
    }

    @Override
    @Cacheable(value = "supplychainGoodBindRelationship", key = "'getSupplychainGoodBindRelationships_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationships(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainGoodBindRelationships( status,pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "supplychainGoodBindRelationship", key = "'getSupplychainGoodBindRelationshipAll'")
    public List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipAll() {
        return mapper.getSupplychainGoodBindRelationshipAll();
    }

    @Override
    @Cacheable(value = "supplychainGoodBindRelationship", key = "'getSupplychainGoodBindRelationshipCount_'+#status+'_'+#name")
    public int getSupplychainGoodBindRelationshipCount(int status, String name) {
        return mapper.getSupplychainGoodBindRelationshipCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainGoodBindRelationship", allEntries = true)
    public void changeSupplychainGoodBindRelationship(SupplychainGoodBindRelationship supplychainGoodBindRelationship) {
        mapper.changeSupplychainGoodBindRelationship(supplychainGoodBindRelationship);
    }

    @Override
    @Cacheable(value = "supplychainGoodBindRelationship", key = "'getSupplychainGoodBindRelationshipByUserId_'+#userId+'_'+#pageIndex+'_'+#pageSize")
    public List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipByUserId(String userId,Integer pageIndex, Integer pageSize) {
        return mapper.getSupplychainGoodBindRelationshipByUserId(userId,pageIndex,pageSize);
    }

    @Override
    @Cacheable(value = "supplychainGoodBindRelationship", key = "'getSupplychainGoodBindRelationshipByUserIdAndTypeId_'+#userId+'_'+#typeId")
    public SupplychainGoodBindRelationship getSupplychainGoodBindRelationshipByUserIdAndTypeId(String userId, String typeId) {
        return mapper.getSupplychainGoodBindRelationshipByUserIdAndTypeId(userId,typeId);
    }

    @Override
    @CacheEvict(value = "supplychainGoodBindRelationship", allEntries = true)
    public void delSupplychainGoodBindRelationshipByUserIdAndTypeId(String userId, String typeId) {
        mapper.delSupplychainGoodBindRelationshipByUserIdAndTypeId(userId,typeId);
    }

    @Override
    @CacheEvict(value = "supplychainGoodBindRelationship", allEntries = true)
    public void addSupplychainGoodBindRelationships(List<SupplychainGoodBindRelationship> supplychainGoodBindRelationship) {
        mapper.addSupplychainGoodBindRelationships(supplychainGoodBindRelationship);
    }

    @Override
    @Cacheable(value = "supplychainGoodBindRelationship", key = "'getSupplychainGoodBindRelationshipByUserIds_'+#userIds")
    public List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipByUserIds(List<String> userIds) {
        return mapper.getSupplychainGoodBindRelationshipByUserIds(userIds);
    }

    @Override
    @Cacheable(value = "supplychainGoodBindRelationship", key = "'getSupplychainGoodBindRelationShipsByUseridsAndTypeIds_'+#userIds+'_'+#typeIds")
    public List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationShipsByUseridsAndTypeIds(List<String> userIds, List<String> typeIds) {
        return mapper.getSupplychainGoodBindRelationShipsByUseridsAndTypeIds(userIds,typeIds);
    }

    @Override
    @CacheEvict(value = "supplychainGoodBindRelationship", allEntries = true)
    public void clearSupplychainGoodBindRelationships(String userId) {
        mapper.clearSupplychainGoodBindRelationships(userId);
    }

    @Override
    @CacheEvict(value = "supplychainGoodBindRelationship", allEntries = true)
    public void batchAddSupplychainGoodBindRelationships(List<SupplychainGoodBindRelationship> entitys) {
        mapper.batchAddSupplychainGoodBindRelationships(entitys);
    }

}
