package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainTimeTaskLock;
        import com.tuoniaostore.supplychain.cache.SupplychainTimeTaskLockCache;
        import com.tuoniaostore.supplychain.data.SupplychainTimeTaskLockMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainTimeTaskLock")
public class SupplychainTimeTaskLockCacheImpl implements SupplychainTimeTaskLockCache {

    @Autowired
    SupplychainTimeTaskLockMapper mapper;

    @Override
    @CacheEvict(value = "supplychainTimeTaskLock", allEntries = true)
    public void addSupplychainTimeTaskLock(SupplychainTimeTaskLock supplychainTimeTaskLock) {
        mapper.addSupplychainTimeTaskLock(supplychainTimeTaskLock);
    }

    @Override
    @Cacheable(value = "supplychainTimeTaskLock", key = "'getSupplychainTimeTaskLock_'+#id")
    public   SupplychainTimeTaskLock getSupplychainTimeTaskLock(String id) {
        return mapper.getSupplychainTimeTaskLock(id);
    }

    @Override
    @Cacheable(value = "supplychainTimeTaskLock", key = "'getSupplychainTimeTaskLocks_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainTimeTaskLock> getSupplychainTimeTaskLocks(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainTimeTaskLocks(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "supplychainTimeTaskLock", key = "'getSupplychainTimeTaskLockAll'")
    public List<SupplychainTimeTaskLock> getSupplychainTimeTaskLockAll() {
        return mapper.getSupplychainTimeTaskLockAll();
    }

    @Override
    @Cacheable(value = "supplychainTimeTaskLock", key = "'getSupplychainTimeTaskLockCount_'+#status+'_'+#name")
    public int getSupplychainTimeTaskLockCount(int status, String name) {
        return mapper.getSupplychainTimeTaskLockCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainTimeTaskLock", allEntries = true)
    public void changeSupplychainTimeTaskLock(SupplychainTimeTaskLock supplychainTimeTaskLock) {
        mapper.changeSupplychainTimeTaskLock(supplychainTimeTaskLock);
    }

}
