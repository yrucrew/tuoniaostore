package com.tuoniaostore.supplychain.cache;

import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import com.tuoniaostore.supplychain.vo.SupplychainGoodPOSVo;
import com.tuoniaostore.supplychain.vo.SupplychainGoodVO;
import org.apache.ibatis.annotations.Param;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainGoodCache {

    void addSupplychainGood(SupplychainGood supplychainGood);

    SupplychainGood getSupplychainGood(String id);

    List<SupplychainGood> getSupplychainGoods(int status, int pageIndex, int pageSize, String name);

    List<SupplychainGood> getSupplychainGoodAllByPage(int pageIndex, int pageSize);

    List<SupplychainGood> getSupplychainGoodAll();

    int getSupplychainGoodCount(int status, String name);

    void changeSupplychainGood(SupplychainGood supplychainGood);

    List<SupplychainGoodPOSVo> getSupplychainGoodPosList(String shopId, String typeId, int pageIndex, int pageSize, HttpServletRequest request);

    /**
     * 通过用户id 商店id 商品类别id 分页条件 查找商品
     *
     * @param shopId
     * @param limitStart
     * @param limitEnd
     * @return
     */
    List<SupplychainGood> getSupplychainGoodByUserIdAndShopId(String shopId, String typeId, int limitStart, int limitEnd);

    /**
     * 通过商店id 类别id 查找对应的商品
     *
     * @param shopId
     * @param typeIds
     * @return
     */
    List<SupplychainGood> getSupplychainGoodByShopIdAndTypes(String shopId, List<String> typeIds, Integer limitStart, Integer limitEnd);

    /**
     * 修改商品详情
     *
     * @param supplychainGood
     */
    void changeSupplychainGoodById(SupplychainGood supplychainGood);

    /**
     * 修改商品状态
     *
     * @param id
     * @param status
     */
    void downSupplychainGoodByGoodId(String id, int status);

    void batchDownSupplychainGoodByGoodId(List<String> stringList, int status);

    void updateChainGood(SupplychainGood supplychainGood);


    /**
     * 删除商品
     *
     * @param shopId, typeId
     * @return void
     * @author oy
     * @date 2019/4/10
     */
    void delSupplychainGoodByShopIdAndTypeId(String shopId, List<String> typeId);

    /***
     * 根据id 删除商品 （逻辑删除）
     * @author oy
     * @date 2019/4/10
     * @param id
     * @return void
     */
    void delSupplychainGoodById(String id);

    /**
     * 根据商店 和 商品名称 搜索商品
     *
     * @param shopId, goodName
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainGood>
     * @author oy
     * @date 2019/4/10
     */
    List<SupplychainGood> searchSupplychainGoodByShopIdAndGoodName(String shopId, String goodName, Integer limitStart, Integer limitEnd);

    /**
     * 根据商店 价格id 搜索商品
     *
     * @param shopId, priceId, limitStart, limitEnd
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainGood>
     * @author oy
     * @date 2019/4/11
     */
    List<SupplychainGood> searchSupplychainGoodByShopIdAndPriceId(String shopId, List<String> priceId, Integer limitStart, Integer limitEnd);

    /**
     * 根据商品ID查询商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/4/8
     */
    List<SupplychainGood> getSupplychainGoodById(List<String> ids, String goodType, Integer goodPriceSort);

    /**
     * 查询商品带分页
     *
     * @param
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainGood>
     * @author sqd
     * @date 2019/4/12
     */
    List<SupplychainGood> getSupplychainGoodByIdPage(List<String> ids, Integer pageIndex, Integer pageSize);

    /**
     * 首页：采购 热门品类 筛选
     *
     * @param shopId, typeId, priceId, limitStart, limitEnd
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainGood>
     * @author oy
     * @date 2019/4/15
     */
    List<SupplychainGood> getSupplychainGoodForHomePage(String shopId, String typeId, Integer goodPriceSort, Integer limitStart, Integer limitEnd);

    /**
     * 根据模板id shopId获取对应的商品
     *
     * @param shopId, templateIds, pageStartIndex, pageSize
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainGood>
     * @author oy
     * @date 2019/4/15
     */
    List<SupplychainGood> getSupplychainGoodByShopIdAndTempateIds(String shopId, List<String> templateIds, Integer pageStartIndex, Integer pageSize);

    /**
     * 根据商品名称 商店id 模糊查询商品
     *
     * @param goodNameOrBrandName, shopId, pageSize, pageStartIndex
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainGood>
     * @author oy
     * @date 2019/4/15
     */
    List<SupplychainGood> getsupplychainGoodByGoodName(String goodNameOrBrandName, String shopId, Integer pageStartIndex, Integer pageSize);

    List<SupplychainGood> getsupplychainGoodByGoodNameAndStatus(String goodNameOrBrandName, String shopId, int status, int pageStartIndex, int pageSize);

    /**
     * 通过商店id  和价格标签ID 查找当前商店符合这些价格标签的商品
     *
     * @param shopId
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainGood>
     * @author oy
     * @date 2019/4/22
     */
    List<SupplychainGood> getsupplychaingoodByShopIdAndPriceId(String shopId, List<String> priceId, List<String> typeIds, Integer status, Integer pageStartIndex, Integer pageSize);

    /**
     * 通过商店id  和价格标签ID 查找当前商店符合这些价格标签的商品2
     *
     * @param shopId
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainGood>
     * @author oy
     * @date 2019/4/24
     */
    List<SupplychainGood> getsupplychaingoodByShopIdAndPriceId2(List<String> shopId, List<String> priceId, Integer pageStartIndex, Integer pageSize);

    /**
     * 批量插入
     *
     * @param supplychainGoodSaveList
     * @return void
     * @author oy
     * @date 2019/4/23
     */
    void addSupplychainGoods(List<SupplychainGood> supplychainGoodSaveList);

    /***
     * 根据shopIds去查找商品名称
     * @author oy
     * @date 2019/4/24
     * @param shopIds, pageStartIndex, pageSize
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainGood>
     */
    List<SupplychainGood> getsupplychaingoodByShopIds(List<String> shopIds, Integer pageStartIndex, Integer pageSize);

    /***
     * 根据商店id 和商品查找商品
     * @author oy
     * @date 2019/4/24
     * @param goodNameOrBarcode, shopIds, pageStartIndex, pageSize
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainGood>
     */
    List<SupplychainGood> getsupplychainGoodByshopIdAndGoodName(String goodNameOrBarcode, List<String> shopIds, List<String> goodType, Integer status, Integer pageStartIndex, Integer pageSize);

    List<SupplychainGood> getsupplychainGoodByshopIdAndGoodNameAndNotPrice(String goodNameOrBarcode, List<String> shopIds, List<String> goodType, List<String> notPriceIn, Integer status, Integer pageStartIndex, Integer pageSize);

    /***
     * 更新商品信息
     * @author oy
     * @date 2019/4/25
     * @param goodList1
     * @return void
     */
    void updateChainGoodList(List<SupplychainGood> goodList1);

    /**
     * 修改商品库存
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/6
     */
    void changSupplychainGoodByGoodId(String id, int stock, int isAdd);
    /**
     * POS修改库存
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/6
     */
    void changSupplychainGoodByStock(String id, int stock);

    /***
     * 根据条件获取对应的条数
     * @author oy
     * @date 2019/5/7
     * @param goodNameOrBarcode, typeIds, shopList, priceIds, status
     * @return java.lang.Integer
     */
    Integer getCountSupplychainGoodByParam(String goodNameOrBarcode, List<String> typeIds, List<String> shopList, List<String> priceIds, Integer status);

    /**
     * 批量删除商品
     *
     * @param strings
     * @return void
     * @author oy
     * @date 2019/5/8
     */
    void batchDeleteSupplychainGoodByGoodId(List<String> strings);

    /**
     * 灵活修改对应商品的信息
     *
     * @param paramMap
     * @return void
     * @author oy
     * @date 2019/5/9
     */
    void updateChainGoods(Map<String, Object> paramMap);

    SupplychainGood getSupplychainGoodByShopIdAndPriceId(String shopId, String priceId);

    void updateSupplychainGoodStatus(Map<String, Object> map1);

    void clearSupplychainGoodsByShopId(String shopId);

    SupplychainGood getSupplychainGoodByIdAndStatus(String id, Integer status);

    int getCountSupplychainGoodByshopIdAndGoodNameAndNotPrice(String goodNameOrBarcode, List<String> shopIds, List<String> goodType, List<String> notPriceIn, Integer status);

    List<SupplychainGood> getSupplychainGoodByIdForShopCart(List<String> list, Integer pageIndex, Integer pageSize);

    int getCountSupplychaingoodByShopIdAndPriceId(String shopId, List<String> priceId, List<String> typeIds, Integer status);
}
