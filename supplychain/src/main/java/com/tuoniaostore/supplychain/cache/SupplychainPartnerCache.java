package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainPartner;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainPartnerCache {

    void addSupplychainPartner(SupplychainPartner supplychainPartner);

    SupplychainPartner getSupplychainPartner(String id);

    List<SupplychainPartner> getSupplychainPartners(int status, int pageIndex, int pageSize, String name);
    List<SupplychainPartner> getSupplychainPartnerAll();
    int getSupplychainPartnerCount(int status, String name);
    void changeSupplychainPartner(SupplychainPartner supplychainPartner);

}
