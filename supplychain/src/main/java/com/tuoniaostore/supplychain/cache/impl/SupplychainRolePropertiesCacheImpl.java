package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainRoleProperties;
        import com.tuoniaostore.supplychain.cache.SupplychainRolePropertiesCache;
        import com.tuoniaostore.supplychain.data.SupplychainRolePropertiesMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainRoleProperties")
public class SupplychainRolePropertiesCacheImpl implements SupplychainRolePropertiesCache {

    @Autowired
    SupplychainRolePropertiesMapper mapper;

    @Override
    @CacheEvict(value = "supplychainRoleProperties", allEntries = true)
    public void addSupplychainRoleProperties(SupplychainRoleProperties supplychainRoleProperties) {
        mapper.addSupplychainRoleProperties(supplychainRoleProperties);
    }

    @Override
    @Cacheable(value = "supplychainRoleProperties", key = "'getSupplychainRoleProperties_'+#id")
    public   SupplychainRoleProperties getSupplychainRoleProperties(String id) {
        return mapper.getSupplychainRoleProperties(id);
    }

    @Override
    @Cacheable(value = "supplychainRoleProperties", key = "'getSupplychainRolePropertiess_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainRoleProperties> getSupplychainRolePropertiess(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainRolePropertiess( status, pageIndex, pageSize,name);
    }

    @Override
    @Cacheable(value = "supplychainRoleProperties", key = "'getSupplychainRolePropertiesAll'")
    public List<SupplychainRoleProperties> getSupplychainRolePropertiesAll() {
        return mapper.getSupplychainRolePropertiesAll();
    }

    @Override
    @Cacheable(value = "supplychainRoleProperties", key = "'getSupplychainRolePropertiesCount_'+#status+'_'+#name")
    public int getSupplychainRolePropertiesCount(int status, String name) {
        return mapper.getSupplychainRolePropertiesCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainRoleProperties", allEntries = true)
    public void changeSupplychainRoleProperties(SupplychainRoleProperties supplychainRoleProperties) {
        mapper.changeSupplychainRoleProperties(supplychainRoleProperties);
    }

}
