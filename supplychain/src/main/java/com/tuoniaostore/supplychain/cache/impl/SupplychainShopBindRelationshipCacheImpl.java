package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainShopBindRelationship;
        import com.tuoniaostore.supplychain.cache.SupplychainShopBindRelationshipCache;
        import com.tuoniaostore.supplychain.data.SupplychainShopBindRelationshipMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:24
 */
@Component
@CacheConfig(cacheNames = "supplychainShopBindRelationship")
public class SupplychainShopBindRelationshipCacheImpl implements SupplychainShopBindRelationshipCache {

    @Autowired
    SupplychainShopBindRelationshipMapper mapper;

    @Override
    @CacheEvict(value = "supplychainShopBindRelationship", allEntries = true)
    public void addSupplychainShopBindRelationship(SupplychainShopBindRelationship supplychainShopBindRelationship) {
        mapper.addSupplychainShopBindRelationship(supplychainShopBindRelationship);
    }

    @Override
    @Cacheable(value = "supplychainShopBindRelationship", key = "'getSupplychainShopBindRelationship_'+#id")
    public   SupplychainShopBindRelationship getSupplychainShopBindRelationship(String id) {
        return mapper.getSupplychainShopBindRelationship(id);
    }

    @Override
    @Cacheable(value = "supplychainShopBindRelationship", key = "'getSupplychainShopBindRelationships_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainShopBindRelationship> getSupplychainShopBindRelationships(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainShopBindRelationships(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "supplychainShopBindRelationship", key = "'getSupplychainShopBindRelationshipAll'")
    public List<SupplychainShopBindRelationship> getSupplychainShopBindRelationshipAll() {
        return mapper.getSupplychainShopBindRelationshipAll();
    }

    @Override
    @Cacheable(value = "supplychainShopBindRelationship", key = "'getSupplychainShopBindRelationshipCount_'+#status+'_'+#name")
    public int getSupplychainShopBindRelationshipCount(int status, String name) {
        return mapper.getSupplychainShopBindRelationshipCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainShopBindRelationship", allEntries = true)
    public void changeSupplychainShopBindRelationship(SupplychainShopBindRelationship supplychainShopBindRelationship) {
        mapper.changeSupplychainShopBindRelationship(supplychainShopBindRelationship);
    }

    @Override
    @Cacheable(value = "supplychainShopBindRelationship", key = "'getShopBindRelationships_'+#roleId+'_'+#type")
    public List<SupplychainShopBindRelationship> getShopBindRelationships(String roleId, int type) {
        return mapper.getShopBindRelationships(roleId,type);
    }

    @Override
    @Cacheable(value = "supplychainShopBindRelationship", key = "'getShopBindRelationship_'+#roleId+'_'+#type+'_'+#shopId")
    public SupplychainShopBindRelationship getShopBindRelationship(String roleId, int type, String shopId) {
        return mapper.getShopBindRelationship(roleId,type,shopId);
    }

	@Override
	public List<SupplychainShopBindRelationship> getShopBindRelationshipsByRoleIds(String[] roleIds, int type) {
		return mapper.getShopBindRelationshipsByRoleIds(roleIds, type);
	}

	@Override
	@CacheEvict(value = "supplychainShopBindRelationship", allEntries = true)
	public int delSupplychainShopBindRelationships(int type, String roleId) {
		return mapper.delSupplychainShopBindRelationships(type, roleId);
	}

    @Override
    @Cacheable(value = "supplychainShopBindRelationship", key = "'getRelationshipsByShopIdAndType_'+#shopId+'_'+#type+'_'+#pageIndex+'_'+#pageSize")
    public List<SupplychainShopBindRelationship> getRelationshipsByShopIdAndType(String shopId, int type, Integer pageIndex, Integer pageSize) {
        return mapper.getRelationshipsByShopIdAndType(shopId, type,pageIndex,pageSize);
    }

    @Override
    @CacheEvict(value = "supplychainShopBindRelationship", allEntries = true)
    public int updateRelationshipByUserIdAndType(String roleId,int type, int status) {
        return mapper.updateRelationshipByUserIdAndType(roleId,type, status);
    }

    @Override
    public SupplychainShopBindRelationship getSupplychainShopBindRelationshipByUserIdAndType(String userId, int type) {
        return mapper.getSupplychainShopBindRelationshipByUserIdAndType(userId, type);
    }
}
