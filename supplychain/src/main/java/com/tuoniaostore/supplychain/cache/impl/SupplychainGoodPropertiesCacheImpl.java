package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainGoodProperties;
        import com.tuoniaostore.supplychain.cache.SupplychainGoodPropertiesCache;
        import com.tuoniaostore.supplychain.data.SupplychainGoodPropertiesMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainGoodProperties")
public class SupplychainGoodPropertiesCacheImpl implements SupplychainGoodPropertiesCache {

    @Autowired
    SupplychainGoodPropertiesMapper mapper;

    @Override
    @CacheEvict(value = "supplychainGoodProperties", allEntries = true)
    public void addSupplychainGoodProperties(SupplychainGoodProperties supplychainGoodProperties) {
        mapper.addSupplychainGoodProperties(supplychainGoodProperties);
    }

    @Override
    @Cacheable(value = "supplychainGoodProperties", key = "'getSupplychainGoodProperties_'+#id")
    public   SupplychainGoodProperties getSupplychainGoodProperties(String id) {
        return mapper.getSupplychainGoodProperties(id);
    }

    @Override
    @Cacheable(value = "supplychainGoodProperties", key = "'getSupplychainGoodPropertiess_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainGoodProperties> getSupplychainGoodPropertiess(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainGoodPropertiess( status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "supplychainGoodProperties", key = "'getSupplychainGoodPropertiesAll'")
    public List<SupplychainGoodProperties> getSupplychainGoodPropertiesAll() {
        return mapper.getSupplychainGoodPropertiesAll();
    }

    @Override
    @Cacheable(value = "supplychainGoodProperties", key = "'getSupplychainGoodPropertiesCount_'+#status+'_'+#name")
    public int getSupplychainGoodPropertiesCount(int status, String name) {
        return mapper.getSupplychainGoodPropertiesCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainGoodProperties", allEntries = true)
    public void changeSupplychainGoodProperties(SupplychainGoodProperties supplychainGoodProperties) {
        mapper.changeSupplychainGoodProperties(supplychainGoodProperties);
    }

}
