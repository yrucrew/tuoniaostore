package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainPartnerBindRelationship;
        import com.tuoniaostore.supplychain.cache.SupplychainPartnerBindRelationshipCache;
        import com.tuoniaostore.supplychain.data.SupplychainPartnerBindRelationshipMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainPartnerBindRelationship")
public class SupplychainPartnerBindRelationshipCacheImpl implements SupplychainPartnerBindRelationshipCache {

    @Autowired
    SupplychainPartnerBindRelationshipMapper mapper;

    @Override
    @CacheEvict(value = "supplychainPartnerBindRelationship", allEntries = true)
    public void addSupplychainPartnerBindRelationship(SupplychainPartnerBindRelationship supplychainPartnerBindRelationship) {
        mapper.addSupplychainPartnerBindRelationship(supplychainPartnerBindRelationship);
    }

    @Override
    @Cacheable(value = "supplychainPartnerBindRelationship", key = "'getSupplychainPartnerBindRelationship_'+#id")
    public   SupplychainPartnerBindRelationship getSupplychainPartnerBindRelationship(String id) {
        return mapper.getSupplychainPartnerBindRelationship(id);
    }

    @Override
    @Cacheable(value = "supplychainPartnerBindRelationship", key = "'getSupplychainPartnerBindRelationships_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainPartnerBindRelationship> getSupplychainPartnerBindRelationships(int status, int pageIndex, int pageSize, String name ) {
        return mapper.getSupplychainPartnerBindRelationships( status,pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "supplychainPartnerBindRelationship", key = "'getSupplychainPartnerBindRelationshipAll'")
    public List<SupplychainPartnerBindRelationship> getSupplychainPartnerBindRelationshipAll() {
        return mapper.getSupplychainPartnerBindRelationshipAll();
    }

    @Override
    @Cacheable(value = "supplychainPartnerBindRelationship", key = "'getSupplychainPartnerBindRelationshipCount_'+#status+'_'+#name")
    public int getSupplychainPartnerBindRelationshipCount(int status, String name) {
        return mapper.getSupplychainPartnerBindRelationshipCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainPartnerBindRelationship", allEntries = true)
    public void changeSupplychainPartnerBindRelationship(SupplychainPartnerBindRelationship supplychainPartnerBindRelationship) {
        mapper.changeSupplychainPartnerBindRelationship(supplychainPartnerBindRelationship);
    }

}
