package com.tuoniaostore.supplychain.cache.impl;

import com.tuoniaostore.datamodel.supplychain.SupplychainGoodLevelPrice;
import com.tuoniaostore.supplychain.cache.SupplychainGoodLevelPriceCache;
import com.tuoniaostore.supplychain.data.SupplychainGoodLevelPriceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainGoodLevelPrice")
public class SupplychainGoodLevelPriceCacheImpl implements SupplychainGoodLevelPriceCache {

    @Autowired
    SupplychainGoodLevelPriceMapper mapper;

    @Override
    @CacheEvict(value = "supplychainGoodLevelPrice", allEntries = true)
    public void addSupplychainGoodLevelPrice(SupplychainGoodLevelPrice supplychainGoodLevelPrice) {
        mapper.addSupplychainGoodLevelPrice(supplychainGoodLevelPrice);
    }

    @Override
    @Cacheable(value = "supplychainGoodLevelPrice", key = "'getSupplychainGoodLevelPrice_'+#id")
    public SupplychainGoodLevelPrice getSupplychainGoodLevelPrice(String id) {
        return mapper.getSupplychainGoodLevelPrice(id);
    }

    @Override
    @Cacheable(value = "supplychainGoodLevelPrice", key = "'getSupplychainGoodLevelPrices_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainGoodLevelPrice> getSupplychainGoodLevelPrices(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainGoodLevelPrices( status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "supplychainGoodLevelPrice", key = "'getSupplychainGoodLevelPriceAll'")
    public List<SupplychainGoodLevelPrice> getSupplychainGoodLevelPriceAll() {
        return mapper.getSupplychainGoodLevelPriceAll();
    }


    @Cacheable(value = "supplychainGoodLevelPrice", key = "'getSupplychainGoodLevelPriceCount_'+#status+'_'+#name")
    public int getSupplychainGoodLevelPriceCount(int status, String name) {
        return mapper.getSupplychainGoodLevelPriceCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainGoodLevelPrice", allEntries = true)
    public void changeSupplychainGoodLevelPrice(SupplychainGoodLevelPrice supplychainGoodLevelPrice) {
        mapper.changeSupplychainGoodLevelPrice(supplychainGoodLevelPrice);
    }

}
