package com.tuoniaostore.supplychain.cache;

import java.util.List;

import com.tuoniaostore.datamodel.supplychain.SupplychainPurchaseBindGood;
import com.tuoniaostore.supplychain.vo.PurchaseBindGoodVo;

public interface SupplychainPurchaseBindGoodCache {

	int addPurchaseBindGood(SupplychainPurchaseBindGood purchaseBindGood);

	int updatePurchaseBindGood(String id, String goodTemplateId);

	int deletePurchaseBindGood(String... ids);

	List<PurchaseBindGoodVo> getPurchaseBindGoodList(PurchaseBindGoodVo param, int pageStartIndex, int pageSize);

	int getPurchaseBindGoodListCount(PurchaseBindGoodVo param);

	SupplychainPurchaseBindGood getPurchaseBindGood(String id);

	PurchaseBindGoodVo getPurchaseBindGoodVo(String purchaseBindGoodId);

	List<PurchaseBindGoodVo> getPurchaseBindGoodNameList(List<String> userIds);

	int getPurchaseBindGoodCount(String userId);

	int addPurchaseBindGoods(List<SupplychainPurchaseBindGood> purchaseBindGoods);

	int deletePurchaseBindGoodByUserId(String... userIds);

	List<SupplychainPurchaseBindGood> getPurchaseBindGoodsByUserId(String purchaseUserId);

}
