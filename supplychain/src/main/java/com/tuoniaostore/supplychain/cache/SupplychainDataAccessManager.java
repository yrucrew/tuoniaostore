package com.tuoniaostore.supplychain.cache;

import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.user.RetrieveStatusEnum;
import com.tuoniaostore.datamodel.supplychain.*;
import com.tuoniaostore.supplychain.vo.ShopReportMapperVO;
import com.tuoniaostore.supplychain.vo.SupplychainGoodPOSVo;
import com.tuoniaostore.supplychain.vo.SupplychainGoodStockLoggerListVo;

import okhttp3.internal.Internal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by liyunbiao on 2019/3/7.
 */
@Component
public class SupplychainDataAccessManager {
    @Autowired
    SupplychainBannerCache supplychainBannerCache;

    public void addSupplychainBanner(SupplychainBanner supplychainBanner) {
        supplychainBannerCache.addSupplychainBanner(supplychainBanner);
    }

    public SupplychainBanner getSupplychainBanner(String id) {
        return supplychainBannerCache.getSupplychainBanner(id);
    }

    public List<SupplychainBanner> getSupplychainBanners(int status, int pageIndex, int pageSize, String name) {
        return supplychainBannerCache.getSupplychainBanners(status, pageIndex, pageSize, name);
    }

    public List<SupplychainBanner> getSupplychainBannerAll() {
        return supplychainBannerCache.getSupplychainBannerAll();
    }

    public int getSupplychainBannerCount(int status, String name) {
        return supplychainBannerCache.getSupplychainBannerCount(status, name);
    }

    public void changeSupplychainBanner(SupplychainBanner supplychainBanner) {
        supplychainBannerCache.changeSupplychainBanner(supplychainBanner);
    }

    public List<SupplychainBanner> getSupplychainBannerByShopIdAndAreaId(String shopId, String areaId) {
        return supplychainBannerCache.getSupplychainBannerByShopIdAndAreaId(shopId, areaId);
    }

    public void batchDeleteSupplychainBanner(List<String> list) {
        supplychainBannerCache.batchDeleteSupplychainBanner(list);
    }

    public List<SupplychainBanner> getSupplychainBannersByShopIdAndAreaId(String shopId, String areaId) {
        return supplychainBannerCache.getSupplychainBannersByShopIdAndAreaId(shopId, areaId);
    }

    @Autowired
    SupplychainGoodBindRelationshipCache supplychainGoodBindRelationshipCache;

    public void addSupplychainGoodBindRelationship(SupplychainGoodBindRelationship supplychainGoodBindRelationship) {
        supplychainGoodBindRelationshipCache.addSupplychainGoodBindRelationship(supplychainGoodBindRelationship);
    }

    public List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationships(int status, int pageIndex, int pageSize, String name) {
        return supplychainGoodBindRelationshipCache.getSupplychainGoodBindRelationships(status, pageIndex, pageSize, name);
    }

    public SupplychainGoodBindRelationship getSupplychainGoodBindRelationship(String id) {
        return supplychainGoodBindRelationshipCache.getSupplychainGoodBindRelationship(id);
    }

    public List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipAll() {
        return supplychainGoodBindRelationshipCache.getSupplychainGoodBindRelationshipAll();
    }

    public int getSupplychainGoodBindRelationshipCount(int status, String name) {
        return supplychainGoodBindRelationshipCache.getSupplychainGoodBindRelationshipCount(status, name);
    }

    public void changeSupplychainGoodBindRelationship(SupplychainGoodBindRelationship supplychainGoodBindRelationship) {
        supplychainGoodBindRelationshipCache.changeSupplychainGoodBindRelationship(supplychainGoodBindRelationship);
    }

    public List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipByUserId(String userId, Integer pageIndex, Integer pageSize) {
        return supplychainGoodBindRelationshipCache.getSupplychainGoodBindRelationshipByUserId(userId, pageIndex, pageSize);
    }

    public List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipByUserIds(List<String> userIds) {
        return supplychainGoodBindRelationshipCache.getSupplychainGoodBindRelationshipByUserIds(userIds);
    }

    public SupplychainGoodBindRelationship getSupplychainGoodBindRelationshipByUserIdAndTypeId(String userId, String typeId) {
        return supplychainGoodBindRelationshipCache.getSupplychainGoodBindRelationshipByUserIdAndTypeId(userId, typeId);
    }

    public void delSupplychainGoodBindRelationshipByUserIdAndTypeId(String userId, String typeId) {
        supplychainGoodBindRelationshipCache.delSupplychainGoodBindRelationshipByUserIdAndTypeId(userId, typeId);
    }

    public void addSupplychainGoodBindRelationships(List<SupplychainGoodBindRelationship> supplychainGoodBindRelationship) {
        supplychainGoodBindRelationshipCache.addSupplychainGoodBindRelationships(supplychainGoodBindRelationship);
    }

    public List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationShipsByUseridsAndTypeIds(List<String> userIds, List<String> typeIds) {
        return supplychainGoodBindRelationshipCache.getSupplychainGoodBindRelationShipsByUseridsAndTypeIds(userIds, typeIds);
    }

    public void clearSupplychainGoodBindRelationships(String userId) {
        supplychainGoodBindRelationshipCache.clearSupplychainGoodBindRelationships(userId);
    }

    public void batchAddSupplychainGoodBindRelationships(List<SupplychainGoodBindRelationship> entitys) {
        supplychainGoodBindRelationshipCache.batchAddSupplychainGoodBindRelationships(entitys);
    }

    @Autowired
    SupplychainGoodCache supplychainGoodCache;

    public void addSupplychainGood(SupplychainGood supplychainGood) {
        supplychainGoodCache.addSupplychainGood(supplychainGood);
    }

    public SupplychainGood getSupplychainGood(String id) {
        return supplychainGoodCache.getSupplychainGood(id);
    }

    public SupplychainGood getSupplychainGoodByIdAndStatus(String id, Integer status) {
        return supplychainGoodCache.getSupplychainGoodByIdAndStatus(id,status);
    }


    public List<SupplychainGood> getSupplychainGoods(int status, int pageIndex, int pageSize, String name) {
        return supplychainGoodCache.getSupplychainGoods(status, pageIndex, pageSize, name);
    }

    public List<SupplychainGood> getSupplychainGoodAll() {
        return supplychainGoodCache.getSupplychainGoodAll();
    }

    public List<SupplychainGood> getSupplychainGoodAllByPage(int pageIndex, int pageSize) {
        return supplychainGoodCache.getSupplychainGoodAllByPage(pageIndex, pageSize);
    }

    public int getSupplychainGoodCount(int status, String name) {
        return supplychainGoodCache.getSupplychainGoodCount(status, name);
    }

    public void changeSupplychainGood(SupplychainGood supplychainGood) {
        supplychainGoodCache.changeSupplychainGood(supplychainGood);
    }

    public List<SupplychainGoodPOSVo> getSupplychainGoodPosList(String shopId, String typeId, int pageIndex, int pageSize, HttpServletRequest request) {

        return supplychainGoodCache.getSupplychainGoodPosList(shopId, typeId, pageIndex, pageSize, request);
    }

    public List<SupplychainGood> getSupplychainGoodByUserIdAndShopId(String shopId, String typeId, int limitStart, int limitEnd) {
        return supplychainGoodCache.getSupplychainGoodByUserIdAndShopId(shopId, typeId, limitStart, limitEnd);
    }

    public List<SupplychainGood> getSupplychainGoodByShopIdAndTypes(String shopId, List<String> typeIds, Integer limitStart, Integer limitEnd) {
        return supplychainGoodCache.getSupplychainGoodByShopIdAndTypes(shopId, typeIds, limitStart, limitEnd);
    }

    public void changeSupplychainGoodById(SupplychainGood supplychainGood) {
        supplychainGoodCache.changeSupplychainGoodById(supplychainGood);
    }

    public void downSupplychainGoodByGoodId(String id, int status) {
        supplychainGoodCache.downSupplychainGoodByGoodId(id, status);
    }

    public void changSupplychainGoodByStock(String id, int stock) {
        supplychainGoodCache.changSupplychainGoodByStock(id, stock);
    }

    public void batchDownSupplychainGoodByGoodId(List<String> stringList, int status) {
        supplychainGoodCache.batchDownSupplychainGoodByGoodId(stringList, status);
    }

    public void updateChainGood(SupplychainGood supplychainGood) {
        supplychainGoodCache.updateChainGood(supplychainGood);
    }

    public void delSupplychainGoodByShopIdAndTypeId(String shopId, List<String> typeId) {
        supplychainGoodCache.delSupplychainGoodByShopIdAndTypeId(shopId, typeId);
    }

    public List<SupplychainGood> searchSupplychainGoodByShopIdAndGoodName(String shopId, String goodName, Integer limitStart, Integer limitEnd) {
        return supplychainGoodCache.searchSupplychainGoodByShopIdAndGoodName(shopId, goodName, limitStart, limitEnd);
    }

    public List<SupplychainGood> searchSupplychainGoodByShopIdAndPriceId(String shopId, List<String> priceId, Integer limitStart, Integer limitEnd) {
        return supplychainGoodCache.searchSupplychainGoodByShopIdAndPriceId(shopId, priceId, limitStart, limitEnd);
    }

    public void delSupplychainGoodById(String id) {
        supplychainGoodCache.delSupplychainGoodById(id);
    }

    public void clearSupplychainGoodsByShopId(String shopId) {
        supplychainGoodCache.clearSupplychainGoodsByShopId(shopId);
    }

    public List<SupplychainGood> getSupplychainGoodById(List<String> list, String goodType, Integer goodPriceSort) {
        return supplychainGoodCache.getSupplychainGoodById(list, goodType, goodPriceSort);
    }

    public List<SupplychainGood> getSupplychainGoodByIdPage(List<String> list, Integer pageIndex, Integer pageSize) {
        return supplychainGoodCache.getSupplychainGoodByIdPage(list, pageIndex, pageSize);
    }

    public List<SupplychainGood> getSupplychainGoodByIdForShopCart(List<String> list, Integer pageIndex, Integer pageSize) {
        return supplychainGoodCache.getSupplychainGoodByIdForShopCart(list, pageIndex, pageSize);
    }

    public List<SupplychainGood> getSupplychainGoodForHomePage(String shopId, String typeId, Integer goodPriceSort, Integer limitStart, Integer limitEnd) {
        return supplychainGoodCache.getSupplychainGoodForHomePage(shopId, typeId, goodPriceSort, limitStart, limitEnd);
    }

    public List<SupplychainGood> getSupplychainGoodByShopIdAndTempateIds(String shopId, List<String> templateIds, Integer pageStartIndex, Integer pageSize) {
        return supplychainGoodCache.getSupplychainGoodByShopIdAndTempateIds(shopId, templateIds, pageStartIndex, pageSize);
    }

    public List<SupplychainGood> getsupplychainGoodByGoodName(String goodNameOrBrandName, String shopId, Integer pageStartIndex, Integer pageSize) {
        return supplychainGoodCache.getsupplychainGoodByGoodName(goodNameOrBrandName, shopId, pageStartIndex, pageSize);
    }

    public List<SupplychainGood> getsupplychainGoodByGoodNameAndStatus(String goodNameOrBrandName, String shopId, int status, int pageStartIndex, int pageSize) {
        return supplychainGoodCache.getsupplychainGoodByGoodNameAndStatus(goodNameOrBrandName, shopId,status, pageStartIndex, pageSize);
    }

    public List<SupplychainGood> getsupplychaingoodByShopIdAndPriceId(String shopId, List<String> priceId, List<String> typeIds, Integer status, Integer pageStartIndex, Integer pageSize) {
        return supplychainGoodCache.getsupplychaingoodByShopIdAndPriceId(shopId, priceId, typeIds, status, pageStartIndex, pageSize);
    }

    public int getCountSupplychaingoodByShopIdAndPriceId(String shopId, List<String> priceId, List<String> typeIds, Integer status) {
        return supplychainGoodCache.getCountSupplychaingoodByShopIdAndPriceId(shopId, priceId, typeIds, status);
    }

    public List<SupplychainGood> getsupplychaingoodByShopIdAndPriceId2(List<String> shopId, List<String> priceId, Integer pageStartIndex, Integer pageSize) {
        return supplychainGoodCache.getsupplychaingoodByShopIdAndPriceId2(shopId, priceId, pageStartIndex, pageSize);
    }

    public void addSupplychainGoods(List<SupplychainGood> supplychainGoodSaveList) {
        supplychainGoodCache.addSupplychainGoods(supplychainGoodSaveList);
    }

    public List<SupplychainGood> getsupplychaingoodByShopIds(List<String> shopIds, Integer pageStartIndex, Integer pageSize) {
        return supplychainGoodCache.getsupplychaingoodByShopIds(shopIds, pageStartIndex, pageSize);
    }

    public List<SupplychainGood> getsupplychainGoodByshopIdAndGoodName(String goodNameOrBarcode, List<String> shopIds, List<String> goodType, Integer status, Integer pageStartIndex, Integer pageSize) {
        return supplychainGoodCache.getsupplychainGoodByshopIdAndGoodName(goodNameOrBarcode, shopIds, goodType, status, pageStartIndex, pageSize);
    }

    public List<SupplychainGood> getsupplychainGoodByshopIdAndGoodNameAndNotPrice(String goodNameOrBarcode, List<String> shopIds, List<String> goodType, List<String> notPriceIn, Integer status, Integer pageStartIndex, Integer pageSize) {
        return supplychainGoodCache.getsupplychainGoodByshopIdAndGoodNameAndNotPrice(goodNameOrBarcode, shopIds, goodType,notPriceIn, status, pageStartIndex, pageSize);
    }

    public int getCountSupplychainGoodByshopIdAndGoodNameAndNotPrice(String goodNameOrBarcode, List<String> shopIds, List<String> goodType, List<String> notPriceIn, Integer status) {
        return supplychainGoodCache.getCountSupplychainGoodByshopIdAndGoodNameAndNotPrice(goodNameOrBarcode, shopIds, goodType,notPriceIn, status);
    }

    public void updateChainGoodList(List<SupplychainGood> goodList1) {
        supplychainGoodCache.updateChainGoodList(goodList1);
    }

    public void changSupplychainGoodByGoodId(String id, int stock, int isAdd) {
        supplychainGoodCache.changSupplychainGoodByGoodId(id, stock, isAdd);
    }

    public int getCountSupplychainGoodByParam(String goodNameOrBarcode, List<String> typeIds, List<String> shopList, List<String> priceIds, Integer status) {
        return supplychainGoodCache.getCountSupplychainGoodByParam(goodNameOrBarcode, typeIds, shopList, priceIds, status);
    }

    public void batchDeleteSupplychainGoodByGoodId(List<String> strings) {
        supplychainGoodCache.batchDeleteSupplychainGoodByGoodId(strings);
    }

    public void updateChainGoods(Map<String, Object> paramMap) {
        supplychainGoodCache.updateChainGoods(paramMap);
    }

    public SupplychainGood getSupplychainGoodByShopIdAndPriceId(String shopId, String priceId) {
        return supplychainGoodCache.getSupplychainGoodByShopIdAndPriceId(shopId, priceId);
    }

    public void updateSupplychainGoodStatus(Map<String, Object> map1) {
        supplychainGoodCache.updateSupplychainGoodStatus(map1);
    }

    @Autowired
    SupplychainGoodLevelPriceCache supplychainGoodLevelPriceCache;

    public void addSupplychainGoodLevelPrice(SupplychainGoodLevelPrice supplychainGoodLevelPrice) {
        supplychainGoodLevelPriceCache.addSupplychainGoodLevelPrice(supplychainGoodLevelPrice);
    }

    public SupplychainGoodLevelPrice getSupplychainGoodLevelPrice(String id) {
        return supplychainGoodLevelPriceCache.getSupplychainGoodLevelPrice(id);
    }

    public List<SupplychainGoodLevelPrice> getSupplychainGoodLevelPrices(int status, int pageIndex, int pageSize, String name) {
        return supplychainGoodLevelPriceCache.getSupplychainGoodLevelPrices(status, pageIndex, pageSize, name);
    }

    public List<SupplychainGoodLevelPrice> getSupplychainGoodLevelPriceAll() {
        return supplychainGoodLevelPriceCache.getSupplychainGoodLevelPriceAll();
    }

    public int getSupplychainGoodLevelPriceCount(int status, String name) {
        return supplychainGoodLevelPriceCache.getSupplychainGoodLevelPriceCount(status, name);
    }

    public void changeSupplychainGoodLevelPrice(SupplychainGoodLevelPrice supplychainGoodLevelPrice) {
        supplychainGoodLevelPriceCache.changeSupplychainGoodLevelPrice(supplychainGoodLevelPrice);
    }


    @Autowired
    SupplychainGoodModifyLoggerCache supplychainGoodModifyLoggerCache;

    public void addSupplychainGoodModifyLogger(SupplychainGoodModifyLogger supplychainGoodModifyLogger) {
        supplychainGoodModifyLoggerCache.addSupplychainGoodModifyLogger(supplychainGoodModifyLogger);
    }

    public SupplychainGoodModifyLogger getSupplychainGoodModifyLogger(String id) {
        return supplychainGoodModifyLoggerCache.getSupplychainGoodModifyLogger(id);
    }

    public List<SupplychainGoodModifyLogger> getSupplychainGoodModifyLoggers(int status, int pageIndex, int pageSize, String name) {
        return supplychainGoodModifyLoggerCache.getSupplychainGoodModifyLoggers(status, pageIndex, pageSize, name);
    }

    public List<SupplychainGoodModifyLogger> getSupplychainGoodModifyLoggerAll() {
        return supplychainGoodModifyLoggerCache.getSupplychainGoodModifyLoggerAll();
    }

    public int getSupplychainGoodModifyLoggerCount(int status, String name) {
        return supplychainGoodModifyLoggerCache.getSupplychainGoodModifyLoggerCount(status, name);
    }

    public void changeSupplychainGoodModifyLogger(SupplychainGoodModifyLogger supplychainGoodModifyLogger) {
        supplychainGoodModifyLoggerCache.changeSupplychainGoodModifyLogger(supplychainGoodModifyLogger);
    }

    @Autowired
    SupplychainGoodPropertiesCache supplychainGoodPropertiesCache;

    public void addSupplychainGoodProperties(SupplychainGoodProperties supplychainGoodProperties) {
        supplychainGoodPropertiesCache.addSupplychainGoodProperties(supplychainGoodProperties);
    }

    public SupplychainGoodProperties getSupplychainGoodProperties(String id) {
        return supplychainGoodPropertiesCache.getSupplychainGoodProperties(id);
    }

    public List<SupplychainGoodProperties> getSupplychainGoodPropertiess(int status, int pageIndex, int pageSize, String name) {
        return supplychainGoodPropertiesCache.getSupplychainGoodPropertiess(status, pageIndex, pageSize, name);
    }

    public List<SupplychainGoodProperties> getSupplychainGoodPropertiesAll() {
        return supplychainGoodPropertiesCache.getSupplychainGoodPropertiesAll();
    }

    public int getSupplychainGoodPropertiesCount(int status, String name) {
        return supplychainGoodPropertiesCache.getSupplychainGoodPropertiesCount(status, name);
    }

    public void changeSupplychainGoodProperties(SupplychainGoodProperties supplychainGoodProperties) {
        supplychainGoodPropertiesCache.changeSupplychainGoodProperties(supplychainGoodProperties);
    }


    @Autowired
    SupplychainGoodStockLoggerCache supplychainGoodStockLoggerCache;

    public void addSupplychainGoodStockLogger(SupplychainGoodStockLogger supplychainGoodStockLogger) {
        supplychainGoodStockLoggerCache.addSupplychainGoodStockLogger(supplychainGoodStockLogger);
    }

    public SupplychainGoodStockLogger getSupplychainGoodStockLogger(String id) {
        return supplychainGoodStockLoggerCache.getSupplychainGoodStockLogger(id);
    }

    public List<SupplychainGoodStockLogger> getSupplychainGoodStockLoggers(int status, int pageIndex, int pageSize, String name) {
        return supplychainGoodStockLoggerCache.getSupplychainGoodStockLoggers(status, pageIndex, pageSize, name);
    }

    public List<SupplychainGoodStockLogger> getSupplychainGoodStockLoggerAll() {
        return supplychainGoodStockLoggerCache.getSupplychainGoodStockLoggerAll();
    }

    public int getSupplychainGoodStockLoggerCount(int status, String name) {
        return supplychainGoodStockLoggerCache.getSupplychainGoodStockLoggerCount(status, name);
    }

    public void changeSupplychainGoodStockLogger(SupplychainGoodStockLogger supplychainGoodStockLogger) {
        supplychainGoodStockLoggerCache.changeSupplychainGoodStockLogger(supplychainGoodStockLogger);
    }

    public List<SupplychainGoodStockLoggerListVo> getGoodStockLoggerList(int type, int operationType, String orderSequenceNumber,
                                                                         String goodName, String shopName, Date startTime, Date endTime, int pageStartIndex,
                                                                         int pageSize) {
        return supplychainGoodStockLoggerCache.getGoodStockLoggerList(type, operationType, orderSequenceNumber,
                goodName, shopName, startTime, endTime, pageStartIndex, pageSize);
    }

    public int getGoodStockLoggerListCount(int type, int operationType, String orderSequenceNumber,
                                           String goodName, String shopName, Date startTime, Date endTime) {
        return supplychainGoodStockLoggerCache.getGoodStockLoggerListCount(type, operationType, orderSequenceNumber,
                goodName, shopName, startTime, endTime);
    }

    public void addSupplychainGoodStockLoggers(List<SupplychainGoodStockLogger> supplychainGoodStockLoggers) {
        supplychainGoodStockLoggerCache.addSupplychainGoodStockLoggers(supplychainGoodStockLoggers);
    }

    @Autowired
    SupplychainLevelTemplateCache supplychainLevelTemplateCache;

    public void addSupplychainLevelTemplate(SupplychainLevelTemplate supplychainLevelTemplate) {
        supplychainLevelTemplateCache.addSupplychainLevelTemplate(supplychainLevelTemplate);
    }

    public SupplychainLevelTemplate getSupplychainLevelTemplate(String id) {
        return supplychainLevelTemplateCache.getSupplychainLevelTemplate(id);
    }

    public List<SupplychainLevelTemplate> getSupplychainLevelTemplates(int status, int pageIndex, int pageSize, String name) {
        return supplychainLevelTemplateCache.getSupplychainLevelTemplates(status, pageIndex, pageSize, name);
    }

    public List<SupplychainLevelTemplate> getSupplychainLevelTemplateAll() {
        return supplychainLevelTemplateCache.getSupplychainLevelTemplateAll();
    }

    public int getSupplychainLevelTemplateCount(int status, String name) {
        return supplychainLevelTemplateCache.getSupplychainLevelTemplateCount(status, name);
    }

    public void changeSupplychainLevelTemplate(SupplychainLevelTemplate supplychainLevelTemplate) {
        supplychainLevelTemplateCache.changeSupplychainLevelTemplate(supplychainLevelTemplate);
    }

    @Autowired
    SupplychainPartnerBindRelationshipCache supplychainPartnerBindRelationshipCache;

    public void addSupplychainPartnerBindRelationship(SupplychainPartnerBindRelationship supplychainPartnerBindRelationship) {
        supplychainPartnerBindRelationshipCache.addSupplychainPartnerBindRelationship(supplychainPartnerBindRelationship);
    }

    public SupplychainPartnerBindRelationship getSupplychainPartnerBindRelationship(String id) {
        return supplychainPartnerBindRelationshipCache.getSupplychainPartnerBindRelationship(id);
    }

    public List<SupplychainPartnerBindRelationship> getSupplychainPartnerBindRelationships(int status, int pageIndex, int pageSize, String name) {
        return supplychainPartnerBindRelationshipCache.getSupplychainPartnerBindRelationships(status, pageIndex, pageSize, name);
    }

    public List<SupplychainPartnerBindRelationship> getSupplychainPartnerBindRelationshipAll() {
        return supplychainPartnerBindRelationshipCache.getSupplychainPartnerBindRelationshipAll();
    }

    public int getSupplychainPartnerBindRelationshipCount(int status, String name) {
        return supplychainPartnerBindRelationshipCache.getSupplychainPartnerBindRelationshipCount(status, name);
    }

    public void changeSupplychainPartnerBindRelationship(SupplychainPartnerBindRelationship supplychainPartnerBindRelationship) {
        supplychainPartnerBindRelationshipCache.changeSupplychainPartnerBindRelationship(supplychainPartnerBindRelationship);
    }

    @Autowired
    SupplychainPartnerCallbackCache supplychainPartnerCallbackCache;

    public void addSupplychainPartnerCallback(SupplychainPartnerCallback supplychainPartnerCallback) {
        supplychainPartnerCallbackCache.addSupplychainPartnerCallback(supplychainPartnerCallback);
    }

    public SupplychainPartnerCallback getSupplychainPartnerCallback(String id) {
        return supplychainPartnerCallbackCache.getSupplychainPartnerCallback(id);
    }

    public List<SupplychainPartnerCallback> getSupplychainPartnerCallbacks(int status, int pageIndex, int pageSize, String name) {
        return supplychainPartnerCallbackCache.getSupplychainPartnerCallbacks(status, pageIndex, pageSize, name);
    }

    public List<SupplychainPartnerCallback> getSupplychainPartnerCallbackAll() {
        return supplychainPartnerCallbackCache.getSupplychainPartnerCallbackAll();
    }

    public int getSupplychainPartnerCallbackCount(int status, String name) {
        return supplychainPartnerCallbackCache.getSupplychainPartnerCallbackCount(status, name);
    }

    public void changeSupplychainPartnerCallback(SupplychainPartnerCallback supplychainPartnerCallback) {
        supplychainPartnerCallbackCache.changeSupplychainPartnerCallback(supplychainPartnerCallback);
    }

    @Autowired
    SupplychainPartnerCache supplychainPartnerCache;

    public void addSupplychainPartner(SupplychainPartner supplychainPartner) {
        supplychainPartnerCache.addSupplychainPartner(supplychainPartner);
    }

    public SupplychainPartner getSupplychainPartner(String id) {
        return supplychainPartnerCache.getSupplychainPartner(id);
    }

    public List<SupplychainPartner> getSupplychainPartners(int status, int pageIndex, int pageSize, String name) {
        return supplychainPartnerCache.getSupplychainPartners(status, pageIndex, pageSize, name);
    }

    public List<SupplychainPartner> getSupplychainPartnerAll() {
        return supplychainPartnerCache.getSupplychainPartnerAll();
    }

    public int getSupplychainPartnerCount(int status, String name) {
        return supplychainPartnerCache.getSupplychainPartnerCount(status, name);
    }

    public void changeSupplychainPartner(SupplychainPartner supplychainPartner) {
        supplychainPartnerCache.changeSupplychainPartner(supplychainPartner);
    }

    @Autowired
    SupplychainPartnerShopFocusGoodCache supplychainPartnerShopFocusGoodCache;

    public void addSupplychainPartnerShopFocusGood(SupplychainPartnerShopFocusGood supplychainPartnerShopFocusGood) {
        supplychainPartnerShopFocusGoodCache.addSupplychainPartnerShopFocusGood(supplychainPartnerShopFocusGood);
    }

    public SupplychainPartnerShopFocusGood getSupplychainPartnerShopFocusGood(String id) {
        return supplychainPartnerShopFocusGoodCache.getSupplychainPartnerShopFocusGood(id);
    }

    public List<SupplychainPartnerShopFocusGood> getSupplychainPartnerShopFocusGoods(int status, int pageIndex, int pageSize, String name) {
        return supplychainPartnerShopFocusGoodCache.getSupplychainPartnerShopFocusGoods(status, pageIndex, pageSize, name);
    }

    public List<SupplychainPartnerShopFocusGood> getSupplychainPartnerShopFocusGoodAll() {
        return supplychainPartnerShopFocusGoodCache.getSupplychainPartnerShopFocusGoodAll();
    }

    public int getSupplychainPartnerShopFocusGoodCount(int status, String name) {
        return supplychainPartnerShopFocusGoodCache.getSupplychainPartnerShopFocusGoodCount(status, name);
    }

    public void changeSupplychainPartnerShopFocusGood(SupplychainPartnerShopFocusGood supplychainPartnerShopFocusGood) {
        supplychainPartnerShopFocusGoodCache.changeSupplychainPartnerShopFocusGood(supplychainPartnerShopFocusGood);
    }

    @Autowired
    SupplychainUserDailyBuyLoggerCache supplychainUserDailyBuyLoggerCache;

    public void addSupplychainUserDailyBuyLogger(SupplychainUserDailyBuyLogger supplychainUserDailyBuyLogger) {
        supplychainUserDailyBuyLoggerCache.addSupplychainUserDailyBuyLogger(supplychainUserDailyBuyLogger);
    }

    public SupplychainUserDailyBuyLogger getSupplychainUserDailyBuyLogger(String id) {
        return supplychainUserDailyBuyLoggerCache.getSupplychainUserDailyBuyLogger(id);
    }

    public List<SupplychainUserDailyBuyLogger> getSupplychainUserDailyBuyLoggers(int status, int pageIndex, int pageSize, String name) {
        return supplychainUserDailyBuyLoggerCache.getSupplychainUserDailyBuyLoggers(status, pageIndex, pageSize, name);
    }

    public List<SupplychainUserDailyBuyLogger> getSupplychainUserDailyBuyLoggerAll() {
        return supplychainUserDailyBuyLoggerCache.getSupplychainUserDailyBuyLoggerAll();
    }

    public int getSupplychainUserDailyBuyLoggerCount(int status, String name) {
        return supplychainUserDailyBuyLoggerCache.getSupplychainUserDailyBuyLoggerCount(status, name);
    }

    public void changeSupplychainUserDailyBuyLogger(SupplychainUserDailyBuyLogger supplychainUserDailyBuyLogger) {
        supplychainUserDailyBuyLoggerCache.changeSupplychainUserDailyBuyLogger(supplychainUserDailyBuyLogger);
    }


    @Autowired
    SupplychainRoleCache supplychainRoleCache;

    public void addSupplychainRole(SupplychainRole supplychainRole) {
        supplychainRoleCache.addSupplychainRole(supplychainRole);
    }

    public SupplychainRole getSupplychainRole(String id) {
        return supplychainRoleCache.getSupplychainRole(id);
    }

    public List<SupplychainRole> getSupplychainRoles(int status, int pageIndex, int pageSize, String name) {
        return supplychainRoleCache.getSupplychainRoles(status, pageIndex, pageSize, name);
    }

    public List<SupplychainRole> getSupplychainRoleAll() {
        return supplychainRoleCache.getSupplychainRoleAll();
    }

    public int getSupplychainRoleCount(int status, String name) {
        return supplychainRoleCache.getSupplychainRoleCount(status, name);
    }

    public void changeSupplychainRole(SupplychainRole supplychainRole) {
        supplychainRoleCache.changeSupplychainRole(supplychainRole);
    }

    @Autowired
    SupplychainRolePropertiesCache supplychainRolePropertiesCache;

    public void addSupplychainRoleProperties(SupplychainRoleProperties supplychainRoleProperties) {
        supplychainRolePropertiesCache.addSupplychainRoleProperties(supplychainRoleProperties);
    }

    public SupplychainRoleProperties getSupplychainRoleProperties(String id) {
        return supplychainRolePropertiesCache.getSupplychainRoleProperties(id);
    }

    public List<SupplychainRoleProperties> getSupplychainRolePropertiess(int status, int pageIndex, int pageSize, String name) {
        return supplychainRolePropertiesCache.getSupplychainRolePropertiess(status, pageIndex, pageSize, name);
    }

    public List<SupplychainRoleProperties> getSupplychainRolePropertiesAll() {
        return supplychainRolePropertiesCache.getSupplychainRolePropertiesAll();
    }

    public int getSupplychainRolePropertiesCount(int status, String name) {
        return supplychainRolePropertiesCache.getSupplychainRolePropertiesCount(status, name);
    }

    public void changeSupplychainRoleProperties(SupplychainRoleProperties supplychainRoleProperties) {
        supplychainRolePropertiesCache.changeSupplychainRoleProperties(supplychainRoleProperties);
    }

    @Autowired
    SupplychainTimeTaskLockCache supplychainTimeTaskLockCache;

    public void addSupplychainTimeTaskLock(SupplychainTimeTaskLock supplychainTimeTaskLock) {
        supplychainTimeTaskLockCache.addSupplychainTimeTaskLock(supplychainTimeTaskLock);
    }

    public SupplychainTimeTaskLock getSupplychainTimeTaskLock(String id) {
        return supplychainTimeTaskLockCache.getSupplychainTimeTaskLock(id);
    }

    public List<SupplychainTimeTaskLock> getSupplychainTimeTaskLocks(int status, int pageIndex, int pageSize, String name) {
        return supplychainTimeTaskLockCache.getSupplychainTimeTaskLocks(status, pageIndex, pageSize, name);
    }

    public List<SupplychainTimeTaskLock> getSupplychainTimeTaskLockAll() {
        return supplychainTimeTaskLockCache.getSupplychainTimeTaskLockAll();
    }

    public int getSupplychainTimeTaskLockCount(int status, String name) {
        return supplychainTimeTaskLockCache.getSupplychainTimeTaskLockCount(status, name);
    }

    public void changeSupplychainTimeTaskLock(SupplychainTimeTaskLock supplychainTimeTaskLock) {
        supplychainTimeTaskLockCache.changeSupplychainTimeTaskLock(supplychainTimeTaskLock);
    }

    @Autowired
    SupplychainShopBindRelationshipCache supplychainShopBindRelationshipCache;

    /**
     * 根据用户id和绑定类型获取shop绑定关系
     *
     * @param userId 用户id
     * @param type   绑定类型
     */
    public SupplychainShopBindRelationship getSupplychainShopBindRelationshipByUserIdAndType(String userId, int type) {
        return supplychainShopBindRelationshipCache.getSupplychainShopBindRelationshipByUserIdAndType(userId, type);
    }

    public void addSupplychainShopBindRelationship(SupplychainShopBindRelationship supplychainShopBindRelationship) {
        supplychainShopBindRelationshipCache.addSupplychainShopBindRelationship(supplychainShopBindRelationship);
    }

    public SupplychainShopBindRelationship getSupplychainShopBindRelationship(String id) {
        return supplychainShopBindRelationshipCache.getSupplychainShopBindRelationship(id);
    }

    public List<SupplychainShopBindRelationship> getSupplychainShopBindRelationships(int status, int pageIndex, int pageSize, String name) {
        return supplychainShopBindRelationshipCache.getSupplychainShopBindRelationships(status, pageIndex, pageSize, name);
    }

    public List<SupplychainShopBindRelationship> getSupplychainShopBindRelationshipAll() {
        return supplychainShopBindRelationshipCache.getSupplychainShopBindRelationshipAll();
    }

    public int getSupplychainShopBindRelationshipCount(int status, String name) {
        return supplychainShopBindRelationshipCache.getSupplychainShopBindRelationshipCount(status, name);
    }

    public void changeSupplychainShopBindRelationship(SupplychainShopBindRelationship supplychainShopBindRelationship) {
        supplychainShopBindRelationshipCache.changeSupplychainShopBindRelationship(supplychainShopBindRelationship);
    }

    public List<SupplychainShopBindRelationship> getShopBindRelationships(String roleId, int type) {
        return supplychainShopBindRelationshipCache.getShopBindRelationships(roleId, type);
    }

    public SupplychainShopBindRelationship getShopBindRelationship(String roleId, int type, String shopId) {
        return supplychainShopBindRelationshipCache.getShopBindRelationship(roleId, type, shopId);
    }

    public List<SupplychainShopBindRelationship> getRelationshipsByShopIdAndType(String shopId, int type, Integer pageIndex, Integer pageSize) {
        return supplychainShopBindRelationshipCache.getRelationshipsByShopIdAndType(shopId, type, pageIndex, pageSize);
    }

    public List<SupplychainShopBindRelationship> getShopBindRelationshipsByRoleIds(String[] roleIds, int type) {
        return supplychainShopBindRelationshipCache.getShopBindRelationshipsByRoleIds(roleIds, type);
    }

    public int delSupplychainShopBindRelationships(int type, String roleId) {
        return supplychainShopBindRelationshipCache.delSupplychainShopBindRelationships(type, roleId);
    }

    public int updateRelationshipByUserIdAndType(String roleId, int type, int status) {
        return supplychainShopBindRelationshipCache.updateRelationshipByUserIdAndType(roleId, type, status);
    }

    @Autowired
    SupplychainShopDailyGoodSalesLoggerCache supplychainShopDailyGoodSalesLoggerCache;

    public void addSupplychainShopDailyGoodSalesLogger(SupplychainShopDailyGoodSalesLogger supplychainShopDailyGoodSalesLogger) {
        supplychainShopDailyGoodSalesLoggerCache.addSupplychainShopDailyGoodSalesLogger(supplychainShopDailyGoodSalesLogger);
    }

    public SupplychainShopDailyGoodSalesLogger getSupplychainShopDailyGoodSalesLogger(String id) {
        return supplychainShopDailyGoodSalesLoggerCache.getSupplychainShopDailyGoodSalesLogger(id);
    }

    public List<SupplychainShopDailyGoodSalesLogger> getSupplychainShopDailyGoodSalesLoggers(int status, int pageIndex, int pageSize, String name) {
        return supplychainShopDailyGoodSalesLoggerCache.getSupplychainShopDailyGoodSalesLoggers(status, pageIndex, pageSize, name);
    }

    public List<SupplychainShopDailyGoodSalesLogger> getSupplychainShopDailyGoodSalesLoggerAll() {
        return supplychainShopDailyGoodSalesLoggerCache.getSupplychainShopDailyGoodSalesLoggerAll();
    }

    public int getSupplychainShopDailyGoodSalesLoggerCount(int status, String name) {
        return supplychainShopDailyGoodSalesLoggerCache.getSupplychainShopDailyGoodSalesLoggerCount(status, name);
    }

    public void changeSupplychainShopDailyGoodSalesLogger(SupplychainShopDailyGoodSalesLogger supplychainShopDailyGoodSalesLogger) {
        supplychainShopDailyGoodSalesLoggerCache.changeSupplychainShopDailyGoodSalesLogger(supplychainShopDailyGoodSalesLogger);
    }

    @Autowired
    SupplychainShopPropertiesCache supplychainShopPropertiesCache;

    public void addSupplychainShopProperties(SupplychainShopProperties supplychainShopProperties) {
        supplychainShopPropertiesCache.addSupplychainShopProperties(supplychainShopProperties);
    }

    public SupplychainShopProperties getSupplychainShopProperties(String id) {
        return supplychainShopPropertiesCache.getSupplychainShopProperties(id);
    }

    public List<SupplychainShopProperties> getSupplychainShopPropertiess(int status, int pageIndex, int pageSize, String name, String types, int auth) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiess(status, pageIndex, pageSize, name, types, auth);
    }

    public List<SupplychainShopProperties> getSupplychainShopPropertiess2(int status, Integer pageStartIndex, Integer pageSize, String nameOrPhone, String type, int auth, Map<String, String> map) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiess2(status, pageStartIndex, pageSize, nameOrPhone, type, auth, map);
    }

    public List<SupplychainShopProperties> getSupplychainShopPropertiesAll(String types) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesAll(StatusEnum.ALL.getKey(), types);
    }

    public List<SupplychainShopProperties> getSupplychainShopPropertiesAll2(String types) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesAll(1, types);//开仓的
    }

    public int getSupplychainShopPropertiesCount(int status, String name, String type, int auth, Map<String, String> map) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesCount(status, name, type, auth, map);
    }

    public void changeSupplychainShopProperties(SupplychainShopProperties supplychainShopProperties) {
        supplychainShopPropertiesCache.changeSupplychainShopProperties(supplychainShopProperties);
    }

    public int getOwnAndUnbindShopPropertiesCount(String shopId, List<Integer> types) {
        return supplychainShopPropertiesCache.getOwnAndUnbindShopPropertiesCount(shopId, types);
    }

    public SupplychainShopProperties getSupplychainShopPropertiesByUserId(String userId) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByUserId(userId);
    }

    public void changeSupplychainShopPropertiesById(String userId, String name) {
        supplychainShopPropertiesCache.changeSupplychainShopPropertiesById(userId, name);
    }

    public SupplychainShopProperties UpdateBindPrinterCodeByUserId(String userId ,String bindPrinterCode) {
        return supplychainShopPropertiesCache.UpdateBindPrinterCodeByUserId(userId, bindPrinterCode);
    }

    public SupplychainShopProperties selectSupplychainShopPropertiesById(String userId, int type) {
        return supplychainShopPropertiesCache.selectSupplychainShopPropertiesById(userId, type);
    }

    public void changeSupplychainShopPropertiesStatusByIdAndTypes(String userId, List<Integer> types, int status) {
        supplychainShopPropertiesCache.changeSupplychainShopPropertiesStatusByIdAndTypes(userId, types, status);
    }

    public SupplychainShopProperties getSupplychainShopPropertiesByUserIdAndTypeId(String userId, List<Integer> list) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByUserIdAndTypeId(userId, list);
    }

    public void changeSupplychainShopPropertiesCoveringDistanceByIdAndTypes(String userId, List<Integer> list, int coveringDistance) {
        supplychainShopPropertiesCache.changeSupplychainShopPropertiesCoveringDistanceByIdAndTypes(userId, list, coveringDistance);
    }

    public void changeSupplychainShopPropertiesBindPrinterCodeByIdAndTypes(String userId, List<Integer> list, String bindPrinterCode) {
        supplychainShopPropertiesCache.changeSupplychainShopPropertiesBindPrinterCodeByIdAndTypes(userId, list, bindPrinterCode);
    }

    public void changeSupplychainShopPropertiesPhoneNumber(String userId, List<Integer> list, String phoneNumber) {
        supplychainShopPropertiesCache.changeSupplychainShopPropertiesPhoneNumber(userId, list, phoneNumber);
    }

    public List<SupplychainShopProperties> getSupplychainShopPropertiesByType(List<String> listType, Integer pageStartIndex, Integer pageSize) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByType(listType, pageStartIndex, pageSize);
    }

    public int getSupplychainShopPropertiesByTypeCount(List<String> list) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByTypeCount(list);
    }

    public SupplychainShopProperties getSupplychainShopPropertiesById(String id) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesById(id);
    }

    public int authSupplychainShopProperties(String id, int auth) {
        return supplychainShopPropertiesCache.authSupplychainShopProperties(id, auth);
    }

    public List<SupplychainShopProperties> getSupplychainShopPropertiesByIds(List<String> shopIds) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByIds(shopIds);
    }

    public int getSupplychainShopPropertiesByIdsCount(List<String> shopIds) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByIdsCount(shopIds);
    }

    public List<SupplychainShopProperties> getSupplychainShopPropertiesByIdsPage(List<String> shopIds, Integer pageStartIndex, Integer pageSize) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByIdsPage(shopIds, pageStartIndex, pageSize);
    }

    public List<SupplychainShopProperties> getSupplychainShopPropertiesByShopName(String shopName, List<Integer> types, Integer pageStartIndex, Integer pageSize) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByShopName(shopName, types, pageStartIndex, pageSize);
    }

    public int getSupplychainShopPropertiesByShopNameCount(String shopName, List<Integer> types) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByShopNameCount(shopName, types);
    }

    public List<SupplychainShopProperties> getSupplychainShopPropertiesByShopNameAndUserId(String shopName, List<String> userId, List<Integer> typeIds, Integer pageStartIndex, Integer pageSize) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByShopNameAndUserId(shopName, userId, typeIds, pageStartIndex, pageSize);
    }

    public List<SupplychainShopProperties> getOwnAndUnbindShopProperties(String shopName, String shopId, List<Integer> types, int pageStartIndex, int pageSize) {
        return supplychainShopPropertiesCache.getOwnAndUnbindShopProperties(shopName,shopId, types, pageStartIndex, pageSize);
    }

    public int getCountSupplychainShopPropertiesByShopNameAndUserId(String shopName, List<Integer> typeIds, List<String> userId) {
        return supplychainShopPropertiesCache.getCountSupplychainShopPropertiesByShopNameAndUserId(shopName, typeIds, userId);
    }

    public void deleteSupplychainShopProperties(List<String> id) {
        supplychainShopPropertiesCache.deleteSupplychainShopProperties(id);
    }

    public List<SupplychainShopProperties> getSupplychainShopPropertiesForBindingSelect(Integer pageStartIndex, Integer pageSize) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesForBindingSelect(pageStartIndex, pageSize);
    }

    public void updateSupplychainShopPropertiesByParam(Map<String, Object> map) {
        supplychainShopPropertiesCache.updateSupplychainShopPropertiesByParam(map);
    }

    public List<ShopReportMapperVO>  getSupplychainShopPropertiesByTypeCountForEveryDay(List<String> list, String startTime, String endTime) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByTypeCountForEveryDay(list,startTime,endTime);
    }

    public List<ShopReportMapperVO>  getSupplychainShopPropertiesByTypeCountForEveryWeek(List<String> list, String startTime, String endTime) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByTypeCountForEveryWeek(list,startTime,endTime);
    }

    public List<ShopReportMapperVO>  getSupplychainShopPropertiesByTypeCountForEveryMonth(List<String> list, String startTime, String endTime) {
        return supplychainShopPropertiesCache.getSupplychainShopPropertiesByTypeCountForEveryMonth(list,startTime,endTime);
    }

    @Autowired
    SupplychainShopProxyRelationCache supplychainShopProxyRelationCache;

    public void addSupplychainShopProxyRelation(SupplychainShopProxyRelation supplychainShopProxyRelation) {
        supplychainShopProxyRelationCache.addSupplychainShopProxyRelation(supplychainShopProxyRelation);
    }

    public SupplychainShopProxyRelation getSupplychainShopProxyRelation(String id) {
        return supplychainShopProxyRelationCache.getSupplychainShopProxyRelation(id);
    }

    public List<SupplychainShopProxyRelation> getSupplychainShopProxyRelations(int status, int pageIndex, int pageSize, String name) {
        return supplychainShopProxyRelationCache.getSupplychainShopProxyRelations(status, pageIndex, pageSize, name);
    }

    public List<SupplychainShopProxyRelation> getSupplychainShopProxyRelationAll() {
        return supplychainShopProxyRelationCache.getSupplychainShopProxyRelationAll();
    }

    public int getSupplychainShopProxyRelationCount(int status, String name) {
        return supplychainShopProxyRelationCache.getSupplychainShopProxyRelationCount(status, name);
    }

    public void changeSupplychainShopProxyRelation(SupplychainShopProxyRelation supplychainShopProxyRelation) {
        supplychainShopProxyRelationCache.changeSupplychainShopProxyRelation(supplychainShopProxyRelation);
    }

    @Autowired
    SupplychainHomepageButtonSettingCache supplychainHomepageButtonSettingCache;

    public void addSupplychainHomepageButtonSetting(SupplychainHomepageButtonSetting supplychainHomepageButtonSetting) {
        supplychainHomepageButtonSettingCache.addSupplychainHomepageButtonSetting(supplychainHomepageButtonSetting);
    }

    public SupplychainHomepageButtonSetting getSupplychainHomepageButtonSetting(String id) {
        return supplychainHomepageButtonSettingCache.getSupplychainHomepageButtonSetting(id);
    }

    public List<SupplychainHomepageButtonSetting> getSupplychainHomepageButtonSettings(int status, int pageIndex, int pageSize, String name) {
        return supplychainHomepageButtonSettingCache.getSupplychainHomepageButtonSettings(status, pageIndex, pageSize, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public List<SupplychainHomepageButtonSetting> getSupplychainHomepageButtonSettingAll() {
        return supplychainHomepageButtonSettingCache.getSupplychainHomepageButtonSettingAll();
    }

    public int getSupplychainHomepageButtonSettingCount(int status, String name) {
        return supplychainHomepageButtonSettingCache.getSupplychainHomepageButtonSettingCount(status, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeSupplychainHomepageButtonSetting(SupplychainHomepageButtonSetting supplychainHomepageButtonSetting) {
        supplychainHomepageButtonSettingCache.changeSupplychainHomepageButtonSetting(supplychainHomepageButtonSetting);
    }

    public List<SupplychainHomepageButtonSetting> getSupplychainHomepageButton() {
        return supplychainHomepageButtonSettingCache.getSupplychainHomepageButton();
    }

    public SupplychainHomepageButtonSetting validateSupplychainHomepageButton(String name, Integer type) {
        return supplychainHomepageButtonSettingCache.validateSupplychainHomepageButton(name, type);
    }

    public void batchDeleteSupplychainHomepageButton(List<String> stringList) {
        supplychainHomepageButtonSettingCache.batchDeleteSupplychainHomepageButton(stringList);
    }

    @Autowired
    SupplychainHomepageGoodsSettingCache supplychainHomepageGoodsSettingCache;

    public void addSupplychainHomepageGoodsSetting(SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting) {
        supplychainHomepageGoodsSettingCache.addSupplychainHomepageGoodsSetting(supplychainHomepageGoodsSetting);
    }

    public SupplychainHomepageGoodsSetting getSupplychainHomepageGoodsSetting(String id) {
        return supplychainHomepageGoodsSettingCache.getSupplychainHomepageGoodsSetting(id);
    }

    public List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettings(int pageIndex, int pageSize, String name) {
        return supplychainHomepageGoodsSettingCache.getSupplychainHomepageGoodsSettings(pageIndex, pageSize, name);
    }

    public List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettingAll() {
        return supplychainHomepageGoodsSettingCache.getSupplychainHomepageGoodsSettingAll();
    }

    public int getSupplychainHomepageGoodsSettingCount(String name) {
        return supplychainHomepageGoodsSettingCache.getSupplychainHomepageGoodsSettingCount(name);
    }

    public void changeSupplychainHomepageGoodsSetting(SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting) {
        supplychainHomepageGoodsSettingCache.changeSupplychainHomepageGoodsSetting(supplychainHomepageGoodsSetting);
    }

    public List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettingByAreaIdAndType(String areaId, Integer type, Integer pageIndex, Integer pageSize) {
        return supplychainHomepageGoodsSettingCache.getSupplychainHomepageGoodsSettingByAreaIdAndType(areaId, type, pageIndex, pageSize);
    }

    public List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettingByAreaIdsAndType(List<String> areaIds, Integer type, Integer pageIndex, Integer pageSize) {
        return supplychainHomepageGoodsSettingCache.getSupplychainHomepageGoodsSettingByAreaIdsAndType(areaIds, type, pageIndex, pageSize);
    }

    public void bacchDeleteHomeSettingBy(List<String> ids) {
        supplychainHomepageGoodsSettingCache.bacchDeleteHomeSettingBy(ids);
    }

    public void addSupplychainHomepageGoodsSettings(List<SupplychainHomepageGoodsSetting> list) {
        supplychainHomepageGoodsSettingCache.addSupplychainHomepageGoodsSettings(list);
    }

    public void batchChangeHomeGoods(List<SupplychainHomepageGoodsSetting> list) {
        supplychainHomepageGoodsSettingCache.batchChangeHomeGoods(list);
    }

    public void DeleteHomeSettingByAreaId(String areaId, Integer typeNum) {
        supplychainHomepageGoodsSettingCache.DeleteHomeSettingByAreaId(areaId, typeNum);
    }

    @Autowired
    PosWorkCache posWorkCache;

    /**
     * 清空分类销售表
     */
    public void clearSupplyShopGoodSale() {
        posWorkCache.clearSupplyShopGoodSale();
    }

    public void addPosWork(PosWork posWork) {
        posWorkCache.addPosWork(posWork);
    }

    public PosWork getPosWork(String id) {
        return posWorkCache.getPosWork(id);
    }

    public List<PosWork> getPosWorks(int status, int pageIndex, int pageSize, String name) {
        return posWorkCache.getPosWorks(status, pageIndex, pageSize, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public List<PosWork> getPosWorkAll() {
        return posWorkCache.getPosWorkAll();
    }

    public int getPosWorkCount(int status, String name) {
        return posWorkCache.getPosWorkCount(status, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changePosWork(PosWork posWork) {
        posWorkCache.changePosWork(posWork);
    }

    public PosWork getPosWrokByMap(Map map) {
        return posWorkCache.getPosWrokByMap(map);
    }

    public List<PosWork> getPosWrokListByMap(Map map) {
        return posWorkCache.getPosWrokListByMap(map);
    }

    public int getPosWrokListCountByMap(Map map) {
        return posWorkCache.getPosWrokListCountByMap(map);
    }

    @Autowired
    SupplychainShopGoodSaleCache supplychainShopGoodSaleCache;

    public void addSupplychainShopGoodSale(SupplychainShopGoodSale supplychainShopGoodSale) {
        supplychainShopGoodSaleCache.addSupplychainShopGoodSale(supplychainShopGoodSale);
    }

    public SupplychainShopGoodSale getSupplychainShopGoodSale(String id) {
        return supplychainShopGoodSaleCache.getSupplychainShopGoodSale(id);
    }

    public List<SupplychainShopGoodSale> getSupplychainShopGoodSales(int status, int pageIndex, int pageSize, String name) {
        return supplychainShopGoodSaleCache.getSupplychainShopGoodSales(status, pageIndex, pageSize, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public List<SupplychainShopGoodSale> getSupplychainShopGoodSaleAll() {
        return supplychainShopGoodSaleCache.getSupplychainShopGoodSaleAll();
    }

    public int getSupplychainShopGoodSaleCount(int status, String name) {
        return supplychainShopGoodSaleCache.getSupplychainShopGoodSaleCount(status, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeSupplychainShopGoodSale(SupplychainShopGoodSale supplychainShopGoodSale) {
        supplychainShopGoodSaleCache.changeSupplychainShopGoodSale(supplychainShopGoodSale);
    }

    public void batchAddSupplychainShopGoodSales(List<SupplychainShopGoodSale> addList) {
        supplychainShopGoodSaleCache.batchAddSupplychainShopGoodSales(addList);
    }

    public List<SupplychainShopGoodSale> getSupplychainShopGoodSaleByList(String shopId, String typeId, String barcode, String startTime, String endTime, Integer pageStartIndex, Integer pageSize) {
        return supplychainShopGoodSaleCache.getSupplychainShopGoodSaleByList(shopId, typeId, barcode, startTime, endTime, pageStartIndex, pageSize);
    }

    public int getSupplychainShopGoodSaleByListCount(String shopId, String typeId, String barcode, String startTime, String endTime) {
        return supplychainShopGoodSaleCache.getSupplychainShopGoodSaleByListCount(shopId, typeId, barcode, startTime, endTime);
    }

    @Autowired
    SupplychainWarehourseBindRelationshipCache supplychainWarehourseBindRelationshipCache;

    public List<SupplychainWarehourseBindRelationship> getSupplychainWarehourseBindRelationships(Integer pageStartIndex, Integer pageSize) {
        return supplychainWarehourseBindRelationshipCache.getSupplychainWarehourseBindRelationships(pageStartIndex, pageSize);
    }

    public List<SupplychainWarehourseBindRelationship> getSupplychainWarehourseBindRelationshipsByShopIdsAndType(String id, int type, Integer pageStartIndex, Integer pageSize) {
        return supplychainWarehourseBindRelationshipCache.getSupplychainWarehourseBindRelationshipsByShopIdsAndType(id, type, pageStartIndex, pageSize);
    }

    public void deleteSupplychainWarehourseBindRelationship(String shopId, int type) {
        supplychainWarehourseBindRelationshipCache.deleteSupplychainWarehourseBindRelationship(shopId, type);
    }

    public void batchAddSupplychainWarehourseBindRelationships(List<SupplychainWarehourseBindRelationship> entity) {
        supplychainWarehourseBindRelationshipCache.batchAddSupplychainWarehourseBindRelationships(entity);
    }
    public SupplychainWarehourseBindRelationship getSupplychainWarehourseBindRelationshipsByBindShopId(String bindShopId, Integer type) {
        return supplychainWarehourseBindRelationshipCache.getSupplychainWarehourseBindRelationshipsByBindShopId(bindShopId, type);
    }

    public SupplychainWarehourseBindRelationship getSupplychainWarehourseBindRelationshipsByShopId(String shopId, Integer type) {
        return supplychainWarehourseBindRelationshipCache.getSupplychainWarehourseBindRelationshipsByBindShopId(shopId,type);
    }

    @Autowired
    SupplychainWarehourseSupplierSkuRelationshipCache supplychainWarehourseSupplierSkuRelationshipCache;
    public List<SupplychainWarehourseSupplierSkuRelationship> getSupplychainWarehourseSupplierSkuRelationshipByParam(Map<String,Object> map){
        return supplychainWarehourseSupplierSkuRelationshipCache.getSupplychainWarehourseSupplierSkuRelationshipByParam(map);
    }

    public List<SupplychainWarehourseSupplierSkuRelationship> getSupplychainWarehourseSupplierSkuRelationshipById(Map<String, Object> map) {
        return supplychainWarehourseSupplierSkuRelationshipCache.getSupplychainWarehourseSupplierSkuRelationshipById(map);
    }

    public List<SupplychainWarehourseSupplierSkuRelationship> getSupplychainWarehourseSupplierSkuRelationshipByShopIdAndNotSupplierId(Map<String, Object> map) {
        return supplychainWarehourseSupplierSkuRelationshipCache.getSupplychainWarehourseSupplierSkuRelationshipByShopIdAndNotSupplierId(map);
    }

    public void batchAddSupplychainWarehourseSupplierSkuRelationship(List<SupplychainWarehourseSupplierSkuRelationship> addList) {
        supplychainWarehourseSupplierSkuRelationshipCache.batchAddSupplychainWarehourseSupplierSkuRelationship(addList);
    }

    public void clearSupplychainWarehourseSupplierSkuRelationshipByShopIdAndsupplierId(String shopId, String supplierId) {
        supplychainWarehourseSupplierSkuRelationshipCache.clearSupplychainWarehourseSupplierSkuRelationshipByShopIdAndsupplierId(shopId,supplierId);
    }
}
