package com.tuoniaostore.supplychain.cache.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.tuoniaostore.datamodel.supplychain.SupplychainPurchaseBindGood;
import com.tuoniaostore.supplychain.cache.SupplychainPurchaseBindGoodCache;
import com.tuoniaostore.supplychain.data.SupplychainPurchaseBindGoodMapper;
import com.tuoniaostore.supplychain.vo.PurchaseBindGoodVo;

@Component
@CacheConfig(cacheNames = "supplychainPurchaseBindGood")
public class SupplychainPurchaseBindGoodCacheImpl implements SupplychainPurchaseBindGoodCache {
	
	@Autowired
	SupplychainPurchaseBindGoodMapper mapper;

	@Override
	@CacheEvict(value = "supplychainPurchaseBindGood", allEntries = true)
	public int addPurchaseBindGood(SupplychainPurchaseBindGood purchaseBindGood) {
		return mapper.addPurchaseBindGood(purchaseBindGood);
	}
	
	@Override
	@CacheEvict(value = "supplychainPurchaseBindGood", allEntries = true)
	public int addPurchaseBindGoods(List<SupplychainPurchaseBindGood> purchaseBindGoods) {
		return mapper.addPurchaseBindGoods(purchaseBindGoods);
	}

	@Override
	@CacheEvict(value = "supplychainPurchaseBindGood", allEntries = true)
	public int updatePurchaseBindGood(String id, String goodTemplateId) {
		return mapper.updatePurchaseBindGood(id, goodTemplateId);
	}

	@Override
	@CacheEvict(value = "supplychainPurchaseBindGood", allEntries = true)
	public int deletePurchaseBindGood(String... ids) {
		return mapper.deletePurchaseBindGood(ids);
	}

	@Override
//	@Cacheable(value = "supplychainPurchaseBindGood", 
//	key = "'getPurchaseBindGoodList_'"
//			+ "+ '_' + #param.purchaseRealName "
//			+ "+ '_' + #param.goodTemplateName "
//			+ "+ '_' + #pageStartIndex "
//			+ "+ '_' + #pageSize")
	public List<PurchaseBindGoodVo> getPurchaseBindGoodList(PurchaseBindGoodVo param, int pageStartIndex, int pageSize) {
		return mapper.getPurchaseBindGoodList(param, pageStartIndex, pageSize);
	}

	@Override
//	@Cacheable(value = "supplychainPurchaseBindGood", 
//	key = "'getPurchaseBindGoodListCount_'"
//			+ "+ '_' + #param.purchaseRealName "
//			+ "+ '_' + #param.goodTemplateName ")
	public int getPurchaseBindGoodListCount(PurchaseBindGoodVo param) {
		return mapper.getPurchaseBindGoodListCount(param);
	}

	@Override
	@Cacheable(value = "supplychainPurchaseBindGood", key = "'getPurchaseBindGood_'+#id")
	public SupplychainPurchaseBindGood getPurchaseBindGood(String id) {
		return mapper.getPurchaseBindGood(id);
	}

	@Override
//	@Cacheable(value = "supplychainPurchaseBindGood", key = "'getPurchaseBindGoodVo_' + #purchaseBindGoodId")
	public PurchaseBindGoodVo getPurchaseBindGoodVo(String purchaseBindGoodId) {
		return mapper.getPurchaseBindGoodVo(purchaseBindGoodId);
	}

	@Override
	public List<PurchaseBindGoodVo> getPurchaseBindGoodNameList(List<String> userIds) {
		return mapper.getPurchaseBindGoodNameList(userIds);
	}

	@Override
	public int getPurchaseBindGoodCount(String userId) {
		return mapper.getPurchaseBindGoodCount(userId);
	}

	@Override
	@CacheEvict(value = "supplychainPurchaseBindGood", allEntries = true)
	public int deletePurchaseBindGoodByUserId(String... userIds) {
		return mapper.deletePurchaseBindGoodByUserId(userIds);
	}

	@Override
	@Cacheable(value = "supplychainPurchaseBindGood", key = "'getPurchaseBindGoodsByUserId_' + #userId")
	public List<SupplychainPurchaseBindGood> getPurchaseBindGoodsByUserId(String userId) {
		return mapper.getPurchaseBindGoodsByUserId(userId);
	}
	
}
