package com.tuoniaostore.supplychain.cache.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.good.GoodPriceType;
import com.tuoniaostore.commons.support.good.*;
import com.tuoniaostore.datamodel.good.*;
import com.tuoniaostore.datamodel.supplychain.SuppliychainPrice;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import com.tuoniaostore.supplychain.cache.SupplychainGoodCache;
import com.tuoniaostore.supplychain.data.SupplychainGoodMapper;
import com.tuoniaostore.supplychain.remote.GoodsRemoteService;
import com.tuoniaostore.supplychain.vo.SupplychainGoodPOSVo;
import com.tuoniaostore.supplychain.vo.SupplychainGoodVO;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainGood")
public class SupplychainGoodCacheImpl implements SupplychainGoodCache {

    @Autowired
    SupplychainGoodMapper mapper;
    @Autowired
    GoodsRemoteService goodsRemoteService;

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void addSupplychainGood(SupplychainGood supplychainGood) {
        mapper.addSupplychainGood(supplychainGood);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGood_'+#id")
    public SupplychainGood getSupplychainGood(String id) {
        return mapper.getSupplychainGood(id);
    }


    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodByIdAndStatus_'+#id+'_'+#status")
    public SupplychainGood getSupplychainGoodByIdAndStatus(String id, Integer status) {
        return mapper.getSupplychainGoodByIdAndStatus(id, status);
    }


    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoods_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainGood> getSupplychainGoods(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainGoods(status, pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodAll'")
    public List<SupplychainGood> getSupplychainGoodAll() {
        return mapper.getSupplychainGoodAll();
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodAllByPage_'+#pageIndex+'_'+#pageSize")
    public List<SupplychainGood> getSupplychainGoodAllByPage(int pageIndex, int pageSize) {
        return mapper.getSupplychainGoodAllByPage(pageIndex, pageSize);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodCount_'+#status+'_'+#name")
    public int getSupplychainGoodCount(int status, String name) {
        return mapper.getSupplychainGoodCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void changeSupplychainGood(SupplychainGood supplychainGood) {
        mapper.changeSupplychainGood(supplychainGood);
    }

    /**
     * 商品信息返回
     *
     * @param shopId     商店id
     * @param typeId     类型id
     * @param limitStart 分页
     * @param limitEnd   分页
     * @return
     */
    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodByUserIdAndShopId_'+'_'+#shopId+'_'+#typeId+'_'+#limitStart+'_'+#limitEnd")
    public List<SupplychainGood> getSupplychainGoodByUserIdAndShopId(String shopId, String typeId, int limitStart, int limitEnd) {
        //商品信息
        return mapper.getSupplychainGoodByShopIdAndTypeId(shopId, typeId, limitStart, limitEnd);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodPosList_'+'_'+#shopId+'_'+#typeId+'_'+#pageIndex+'_'+#pageSize")
    public List<SupplychainGoodPOSVo> getSupplychainGoodPosList(String shopId, String typeId, int pageIndex, int pageSize, HttpServletRequest request) {
        List<SupplychainGood> supplychainGoods = mapper.getSupplychainGoodPosList(shopId, typeId, pageIndex, pageSize);
        List<SupplychainGoodPOSVo> supplychainGoodPOSVos = new ArrayList<>();
        int count = supplychainGoods.size();
        for (int i = 0; i < count; i++) {
            SupplychainGoodPOSVo supplychainGoodPOSVo = new SupplychainGoodPOSVo();
            try {
                if (supplychainGoods.get(i).getPriceId() != null) {
                    GoodPrice goodPrice = goodsRemoteService.getGoodPrice(supplychainGoods.get(i).getPriceId());
                    if (goodPrice != null) {
                        if (goodPrice.getId() != null) {
                            List<GoodBarcode> codes = goodsRemoteService.getGoodBarcode(goodPrice.getId());
                            supplychainGoodPOSVo.setBarCode(codes);
                        }
                        if (supplychainGoods.get(i).getGoodTemplateId() != null) {
                            GoodTemplate goodTemplate = goodsRemoteService.getGoodTemplate(supplychainGoods.get(i).getGoodTemplateId());// GoodTemplateRemoteService.getGoodTemplate(supplychainGoods.get(i).getGoodTemplateId(), request);
                            if (goodTemplate != null) {
                                supplychainGoodPOSVo.setGoodTemplateName(goodTemplate.getName());
                                supplychainGoodPOSVo.setShelfLife(goodTemplate.getShelfLife());
                            }
                        }
                        if (goodPrice.getUnitId() != null) {
                            GoodUnit goodUnit = goodsRemoteService.getGoodsUnit(goodPrice.getUnitId());
                            supplychainGoodPOSVo.setUnitNmae(goodUnit.getTitle());
                        }


                        supplychainGoodPOSVo.setPriceName(goodPrice.getTitle());
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (supplychainGoods.get(i).getGoodTypeId() != null) {
                GoodType goodType = goodsRemoteService.getGoodType(supplychainGoods.get(i).getGoodTypeId());
                if (goodType != null) {
                    supplychainGoodPOSVo.setGoodTypeId(goodType.getParentId());
                }
            }

            supplychainGoodPOSVo.setId(supplychainGoods.get(i).getId());
            supplychainGoodPOSVo.setCostPrice(supplychainGoods.get(i).getCostPrice());
            supplychainGoodPOSVo.setGoodTemplateId(supplychainGoods.get(i).getGoodTemplateId());
            supplychainGoodPOSVo.setGoodId(supplychainGoods.get(i).getId());
            supplychainGoodPOSVo.setShopId(supplychainGoods.get(i).getShopId());
            supplychainGoodPOSVo.setShopName(supplychainGoods.get(i).getShopName());
            supplychainGoodPOSVo.setStock(supplychainGoods.get(i).getStock());
            supplychainGoodPOSVo.setStatus(supplychainGoods.get(i).getStatus());
            supplychainGoodPOSVo.setSellPrice(supplychainGoods.get(i).getSellPrice());
            supplychainGoodPOSVo.setMarketPrice(supplychainGoods.get(i).getMarketPrice());
            supplychainGoodPOSVo.setCreateTime(supplychainGoods.get(i).getCreateTime().toString());
            supplychainGoodPOSVo.setPriceId(supplychainGoods.get(i).getPriceId());
            supplychainGoodPOSVo.setPrice(supplychainGoods.get(i).getSellPrice());
            supplychainGoodPOSVos.add(supplychainGoodPOSVo);
        }


        return supplychainGoodPOSVos;
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodByUserIdAndShopId_'+#shopId+'_'+#typeIds+'_'+#limitStart+'_'+#limitEnd")
    public List<SupplychainGood> getSupplychainGoodByShopIdAndTypes(String shopId, List<String> typeIds, Integer limitStart, Integer limitEnd) {
        return mapper.getSupplychainGoodByShopIdAndTypes(shopId, typeIds, limitStart, limitEnd);
    }

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void changeSupplychainGoodById(SupplychainGood supplychainGood) {
        mapper.changeSupplychainGoodById(supplychainGood);
    }

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void downSupplychainGoodByGoodId(String id, int status) {
        mapper.downSupplychainGoodByGoodId(id, status);
    }

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void batchDownSupplychainGoodByGoodId(List<String> ids, int status) {
        mapper.batchDownSupplychainGoodByGoodId(ids, status);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodById_'+#ids+'_'+#goodType+'_'+#goodPriceSort")
    public List<SupplychainGood> getSupplychainGoodById(List<String> ids, String goodType, Integer goodPriceSort) {
        return mapper.getSupplychainGoodById(ids, goodType, goodPriceSort);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodById'+#ids+'_'+#pageIndex+'_'+#pageSize")
    public List<SupplychainGood> getSupplychainGoodByIdPage(List<String> ids, Integer pageIndex, Integer pageSize) {
        return mapper.getSupplychainGoodByIdPage(ids, pageIndex, pageSize);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodForHomePage_'+#shopId+'_'+#typeId+'_'+#goodPriceSort+'_'+#limitStart+'_'+#limitEnd")
    public List<SupplychainGood> getSupplychainGoodForHomePage(String shopId, String typeId, Integer goodPriceSort, Integer limitStart, Integer limitEnd) {
        return mapper.getSupplychainGoodForHomePage(shopId, typeId, goodPriceSort, limitStart, limitEnd);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodByShopIdAndTempateIds_'+#shopId+'_'+#templateIds+'_'+#pageStartIndex+'_'+#limitStpageSizeart")
    public List<SupplychainGood> getSupplychainGoodByShopIdAndTempateIds(String shopId, List<String> templateIds, Integer pageStartIndex, Integer pageSize) {
        return mapper.getSupplychainGoodByShopIdAndTempateIds(shopId, templateIds, pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getsupplychainGoodByGoodName_'+#goodNameOrBrandName+'_'+#shopId+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainGood> getsupplychainGoodByGoodName(String goodNameOrBrandName, String shopId, Integer pageStartIndex, Integer pageSize) {
        return mapper.getsupplychainGoodByGoodName(goodNameOrBrandName, shopId, pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getsupplychainGoodByGoodName_'+#goodNameOrBrandName+'_'+#shopId+'_'+#status+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainGood> getsupplychainGoodByGoodNameAndStatus(String goodNameOrBrandName, String shopId, int status, int pageStartIndex, int pageSize) {
        return mapper.getsupplychainGoodByGoodNameAndStatus(goodNameOrBrandName, shopId, status, pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getsupplychaingoodByShopIdAndPriceId_'+#shopId+'_'+#priceId+'_'+#typeIds+'_'+#status+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainGood> getsupplychaingoodByShopIdAndPriceId(String shopId, List<String> priceId, List<String> typeIds, Integer status, Integer pageStartIndex, Integer pageSize) {
        return mapper.getsupplychaingoodByShopIdAndPriceId(shopId, priceId, typeIds, status, pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getsupplychaingoodByShopIdAndPriceId2_'+#shopId+'_'+#priceId+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainGood> getsupplychaingoodByShopIdAndPriceId2(List<String> shopId, List<String> priceId, Integer pageStartIndex, Integer pageSize) {
        return mapper.getsupplychaingoodByShopIdAndPriceId2(shopId, priceId, pageStartIndex, pageSize);
    }

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void addSupplychainGoods(List<SupplychainGood> supplychainGoodSaveList) {
        mapper.addSupplychainGoods(supplychainGoodSaveList);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getsupplychaingoodByShopIds_'+#shopId+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainGood> getsupplychaingoodByShopIds(List<String> shopIds, Integer pageStartIndex, Integer pageSize) {
        return mapper.getsupplychaingoodByShopIds(shopIds, pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getsupplychainGoodByshopIdAndGoodName_'+'_'+#goodName+'_'+#shopIds+'_'+#goodType+'_'+#status+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainGood> getsupplychainGoodByshopIdAndGoodName(String goodName, List<String> shopIds, List<String> goodType, Integer status, Integer pageStartIndex, Integer pageSize) {
        return mapper.getsupplychainGoodByshopIdAndGoodName(goodName, shopIds, goodType, status, pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getsupplychainGoodByshopIdAndGoodNameAndNotPrice_'+'_'+#goodNameOrBarcode+'_'+#shopIds+'_'+#goodType+'_'+#notPriceIn+'_'+#status+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainGood> getsupplychainGoodByshopIdAndGoodNameAndNotPrice(String goodNameOrBarcode, List<String> shopIds, List<String> goodType, List<String> notPriceIn, Integer status, Integer pageStartIndex, Integer pageSize) {
        return mapper.getsupplychainGoodByshopIdAndGoodNameAndNotPrice(goodNameOrBarcode, shopIds, goodType, notPriceIn, status, pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getCountSupplychainGoodByshopIdAndGoodNameAndNotPrice_'+'_'+#goodName+'_'+#shopIds+'_'+#goodType+'_'+#notPriceIn+'_'+#status")
    public int getCountSupplychainGoodByshopIdAndGoodNameAndNotPrice(String goodNameOrBarcode, List<String> shopIds, List<String> goodType, List<String> notPriceIn, Integer status) {
        return mapper.getCountSupplychainGoodByshopIdAndGoodNameAndNotPrice(goodNameOrBarcode, shopIds, goodType, notPriceIn, status);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodByIdForShopCart_'+#list+'_'+#pageIndex+'_'+#pageSize")
    public List<SupplychainGood> getSupplychainGoodByIdForShopCart(List<String> list, Integer pageIndex, Integer pageSize) {
        return mapper.getSupplychainGoodByIdForShopCart(list, pageIndex, pageSize);
    }


    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void updateChainGoodList(List<SupplychainGood> goodList1) {
        mapper.updateChainGoodList(goodList1);
    }


    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void updateChainGood(SupplychainGood supplychainGood) {
        mapper.updateChainGood(supplychainGood);
    }

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void delSupplychainGoodByShopIdAndTypeId(String shopId, List<String> typeId) {
        mapper.delSupplychainGoodByShopIdAndTypeId(shopId, typeId);
    }

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void delSupplychainGoodById(String id) {
        mapper.delSupplychainGoodById(id);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'searchSupplychainGoodByShopIdAndGoodName_'+#shopId+'_'+#goodName+'_'+#limitStart+'_'+#limitEnd")
    public List<SupplychainGood> searchSupplychainGoodByShopIdAndGoodName(String shopId, String goodName, Integer limitStart, Integer limitEnd) {
        return mapper.searchSupplychainGoodByShopIdAndGoodName(shopId, goodName, limitStart, limitEnd);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'searchSupplychainGoodByShopIdAndPriceId_'+#shopId+'_'+#priceId+'_'+#limitStart+'_'+#limitEnd")
    public List<SupplychainGood> searchSupplychainGoodByShopIdAndPriceId(String shopId, List<String> priceId, Integer limitStart, Integer limitEnd) {
        return mapper.searchSupplychainGoodByShopIdAndPriceId(shopId, priceId, limitStart, limitEnd);
    }

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void changSupplychainGoodByGoodId(String id, int stock, int isAdd) {
        mapper.changSupplychainGoodByGoodId(id, stock, isAdd);
    }

    @Override
    public void changSupplychainGoodByStock(String id, int stock) {
        mapper.changSupplychainGoodByStock(id, stock);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getCountSupplychainGoodByParam_'+#goodNameOrBarcode+'_'+#typeIds+'_'+#shopList+'_'+#priceIds+'_'+#status")
    public Integer getCountSupplychainGoodByParam(String goodNameOrBarcode, List<String> typeIds, List<String> shopList, List<String> priceIds, Integer status) {
        return mapper.getCountSupplychainGoodByParam(goodNameOrBarcode, typeIds, shopList, priceIds, status);
    }

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void batchDeleteSupplychainGoodByGoodId(List<String> strings) {
        mapper.batchDeleteSupplychainGoodByGoodId(strings);
    }

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void updateChainGoods(Map<String, Object> paramMap) {
        mapper.updateChainGoods(paramMap);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getSupplychainGoodByShopIdAndPriceId_'+#shopId+'_'+#priceId")
    public SupplychainGood getSupplychainGoodByShopIdAndPriceId(String shopId, String priceId) {
        return mapper.getSupplychainGoodByShopIdAndPriceId(shopId, priceId);
    }

    @Override
    @Cacheable(value = "supplychainGood", key = "'getCountSupplychaingoodByShopIdAndPriceId'+#shopId+'_'+#priceId+'_'+#typeIds+'_'+#status")
    public int getCountSupplychaingoodByShopIdAndPriceId(String shopId, List<String> priceId, List<String> typeIds, Integer status) {
        return mapper.getCountSupplychaingoodByShopIdAndPriceId(shopId, priceId, typeIds, status);
    }

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void updateSupplychainGoodStatus(Map<String, Object> map1) {
        mapper.updateSupplychainGoodStatus(map1);
    }

    @Override
    @CacheEvict(value = "supplychainGood", allEntries = true)
    public void clearSupplychainGoodsByShopId(String shopId) {
        mapper.clearSupplychainGoodsByShopId(shopId);
    }


}
