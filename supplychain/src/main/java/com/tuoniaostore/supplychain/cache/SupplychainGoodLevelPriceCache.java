package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodLevelPrice;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainGoodLevelPriceCache {

    void addSupplychainGoodLevelPrice(SupplychainGoodLevelPrice supplychainGoodLevelPrice);

    SupplychainGoodLevelPrice getSupplychainGoodLevelPrice(String id);

    List<SupplychainGoodLevelPrice> getSupplychainGoodLevelPrices(int status, int pageIndex, int pageSize, String name);
    List<SupplychainGoodLevelPrice> getSupplychainGoodLevelPriceAll();
    int getSupplychainGoodLevelPriceCount(int status, String name);
    void changeSupplychainGoodLevelPrice(SupplychainGoodLevelPrice supplychainGoodLevelPrice);

}
