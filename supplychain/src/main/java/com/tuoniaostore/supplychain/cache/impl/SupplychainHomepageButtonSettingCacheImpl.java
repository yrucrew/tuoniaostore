package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageButtonSetting;
        import com.tuoniaostore.supplychain.cache.SupplychainHomepageButtonSettingCache;
        import com.tuoniaostore.supplychain.data.SupplychainHomepageButtonSettingMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 首页按钮配置
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-04 14:40:15
 */
@Component
@CacheConfig(cacheNames = "supplychainHomepageButtonSetting")
public class SupplychainHomepageButtonSettingCacheImpl implements SupplychainHomepageButtonSettingCache {

    @Autowired
    SupplychainHomepageButtonSettingMapper mapper;

    @Override
    @CacheEvict(value = "supplychainHomepageButtonSetting", allEntries = true)
    public void addSupplychainHomepageButtonSetting(SupplychainHomepageButtonSetting supplychainHomepageButtonSetting) {
        mapper.addSupplychainHomepageButtonSetting(supplychainHomepageButtonSetting);
    }

    @Override
    @Cacheable(value = "supplychainHomepageButtonSetting", key = "'getSupplychainHomepageButtonSetting_'+#id")
    public   SupplychainHomepageButtonSetting getSupplychainHomepageButtonSetting(String id) {
        return mapper.getSupplychainHomepageButtonSetting(id);
    }

    @Override
    @Cacheable(value = "supplychainHomepageButtonSetting", key = "'getSupplychainHomepageButtonSettings_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name+'_'+#retrieveStatus")
    public List<SupplychainHomepageButtonSetting> getSupplychainHomepageButtonSettings(int status, int pageIndex, int pageSize, String name,int retrieveStatus) {
        return mapper.getSupplychainHomepageButtonSettings(status,pageIndex, pageSize, name,retrieveStatus);
    }

    @Override
    @Cacheable(value = "supplychainHomepageButtonSetting", key = "'getSupplychainHomepageButtonSettingAll'")
    public List<SupplychainHomepageButtonSetting> getSupplychainHomepageButtonSettingAll() {
        return mapper.getSupplychainHomepageButtonSettingAll();
    }

    @Override
    @Cacheable(value = "supplychainHomepageButtonSetting", key = "'getSupplychainHomepageButtonSettingCount_'+#status+'_'+#name+'_'+#retrieveStatus")
    public int getSupplychainHomepageButtonSettingCount(int status, String name,int retrieveStatus) {
        return mapper.getSupplychainHomepageButtonSettingCount(status, name,retrieveStatus);
    }

    @Override
    @CacheEvict(value = "supplychainHomepageButtonSetting", allEntries = true)
    public void changeSupplychainHomepageButtonSetting(SupplychainHomepageButtonSetting supplychainHomepageButtonSetting) {
        mapper.changeSupplychainHomepageButtonSetting(supplychainHomepageButtonSetting);
    }

    @Override
    @Cacheable(value = "supplychainHomepageButtonSetting", key = "'getSupplychainHomepageButton'")
    public List<SupplychainHomepageButtonSetting> getSupplychainHomepageButton() {
        return mapper.getSupplychainHomepageButton();
    }

    @Override
    @Cacheable(value = "supplychainHomepageButtonSetting", key = "'validateSupplychainHomepageButton_'+#name+'_'+#type")
    public SupplychainHomepageButtonSetting validateSupplychainHomepageButton(String name, Integer type) {
        return mapper.validateSupplychainHomepageButton(name,type);
    }

    @Override
    @CacheEvict(value = "supplychainHomepageButtonSetting", allEntries = true)
    public void batchDeleteSupplychainHomepageButton(List<String> stringList) {
        mapper.batchDeleteSupplychainHomepageButton(stringList);
    }

}
