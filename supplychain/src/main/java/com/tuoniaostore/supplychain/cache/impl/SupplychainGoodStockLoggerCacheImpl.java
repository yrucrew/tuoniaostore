package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainGoodStockLogger;
        import com.tuoniaostore.supplychain.cache.SupplychainGoodStockLoggerCache;
        import com.tuoniaostore.supplychain.data.SupplychainGoodStockLoggerMapper;
import com.tuoniaostore.supplychain.vo.SupplychainGoodStockLoggerListVo;

import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 商品的出入库日志
主要是针对商品的库存改变
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainGoodStockLogger")
public class SupplychainGoodStockLoggerCacheImpl implements SupplychainGoodStockLoggerCache {

    @Autowired
    SupplychainGoodStockLoggerMapper mapper;

    @Override
    @CacheEvict(value = "supplychainGoodStockLogger", allEntries = true)
    public void addSupplychainGoodStockLogger(SupplychainGoodStockLogger supplychainGoodStockLogger) {
        mapper.addSupplychainGoodStockLogger(supplychainGoodStockLogger);
    }

    @Override
    @Cacheable(value = "supplychainGoodStockLogger", key = "'getSupplychainGoodStockLogger_'+#id")
    public   SupplychainGoodStockLogger getSupplychainGoodStockLogger(String id) {
        return mapper.getSupplychainGoodStockLogger(id);
    }

    @Override
    @Cacheable(value = "supplychainGoodStockLogger", key = "'getSupplychainGoodStockLoggers_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainGoodStockLogger> getSupplychainGoodStockLoggers(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainGoodStockLoggers( status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "supplychainGoodStockLogger", key = "'getSupplychainGoodStockLoggerAll'")
    public List<SupplychainGoodStockLogger> getSupplychainGoodStockLoggerAll() {
        return mapper.getSupplychainGoodStockLoggerAll();
    }

    @Override
    @Cacheable(value = "supplychainGoodStockLogger", key = "'getSupplychainGoodStockLoggerCount_'+#status+'_'+#name")
    public int getSupplychainGoodStockLoggerCount(int status, String name) {
        return mapper.getSupplychainGoodStockLoggerCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainGoodStockLogger", allEntries = true)
    public void changeSupplychainGoodStockLogger(SupplychainGoodStockLogger supplychainGoodStockLogger) {
        mapper.changeSupplychainGoodStockLogger(supplychainGoodStockLogger);
    }

	@Override
	public List<SupplychainGoodStockLoggerListVo> getGoodStockLoggerList(int type, int operationType,
			String orderSequenceNumber, String goodName, String shopName, Date startTime, Date endTime,
			int pageStartIndex, int pageSize) {
		return mapper.getGoodStockLoggerList(type, operationType, orderSequenceNumber,
				goodName, shopName, startTime, endTime, pageStartIndex, pageSize);
	}

	@Override
	public int getGoodStockLoggerListCount(int type, int operationType,
			String orderSequenceNumber, String goodName, String shopName, Date startTime, Date endTime) {
		return mapper.getGoodStockLoggerListCount(type, operationType, orderSequenceNumber, goodName, shopName, startTime, endTime);
	}

    @Override
    @CacheEvict(value = "supplychainGoodStockLogger", allEntries = true)
    public void addSupplychainGoodStockLoggers(List<SupplychainGoodStockLogger> supplychainGoodStockLoggers) {
        mapper.addSupplychainGoodStockLoggers(supplychainGoodStockLoggers);
    }

}
