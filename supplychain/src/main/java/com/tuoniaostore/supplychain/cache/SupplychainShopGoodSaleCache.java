package com.tuoniaostore.supplychain.cache;

import com.tuoniaostore.datamodel.supplychain.SupplychainShopGoodSale;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-20 20:05:41
 */
public interface SupplychainShopGoodSaleCache {

    void addSupplychainShopGoodSale(SupplychainShopGoodSale supplychainShopGoodSale);

    SupplychainShopGoodSale getSupplychainShopGoodSale(String id);

    List<SupplychainShopGoodSale> getSupplychainShopGoodSales(int status, int pageIndex, int pageSize, String name, int retrieveStatus);

    List<SupplychainShopGoodSale> getSupplychainShopGoodSaleAll();

    int getSupplychainShopGoodSaleCount(int status, String name, int retrieveStatus);

    void changeSupplychainShopGoodSale(SupplychainShopGoodSale supplychainShopGoodSale);

    void batchAddSupplychainShopGoodSales(List<SupplychainShopGoodSale> supplychainShopGoodSales);

    List<SupplychainShopGoodSale> getSupplychainShopGoodSaleByList(String shopId, String typeId,String barcode, String startTime, String endTime, Integer pageStartIndex, Integer pageSize);

    int getSupplychainShopGoodSaleByListCount(String shopId, String typeId,String barcode, String startTime, String endTime);
}
