package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainPartnerBindRelationship;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainPartnerBindRelationshipCache {

    void addSupplychainPartnerBindRelationship(SupplychainPartnerBindRelationship supplychainPartnerBindRelationship);

    SupplychainPartnerBindRelationship getSupplychainPartnerBindRelationship(String id);

    List<SupplychainPartnerBindRelationship> getSupplychainPartnerBindRelationships(int status, int pageIndex, int pageSize, String name);
    List<SupplychainPartnerBindRelationship> getSupplychainPartnerBindRelationshipAll();
    int getSupplychainPartnerBindRelationshipCount(int status, String name);
    void changeSupplychainPartnerBindRelationship(SupplychainPartnerBindRelationship supplychainPartnerBindRelationship);

}
