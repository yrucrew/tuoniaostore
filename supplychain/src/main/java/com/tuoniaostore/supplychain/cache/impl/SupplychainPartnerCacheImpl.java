package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainPartner;
        import com.tuoniaostore.supplychain.cache.SupplychainPartnerCache;
        import com.tuoniaostore.supplychain.data.SupplychainPartnerMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainPartner")
public class SupplychainPartnerCacheImpl implements SupplychainPartnerCache {

    @Autowired
    SupplychainPartnerMapper mapper;

    @Override
    @CacheEvict(value = "supplychainPartner", allEntries = true)
    public void addSupplychainPartner(SupplychainPartner supplychainPartner) {
        mapper.addSupplychainPartner(supplychainPartner);
    }

    @Override
    @Cacheable(value = "supplychainPartner", key = "'getSupplychainPartner_'+#id")
    public   SupplychainPartner getSupplychainPartner(String id) {
        return mapper.getSupplychainPartner(id);
    }

    @Override
    @Cacheable(value = "supplychainPartner", key = "'getSupplychainPartners_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainPartner> getSupplychainPartners(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainPartners(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "supplychainPartner", key = "'getSupplychainPartnerAll'")
    public List<SupplychainPartner> getSupplychainPartnerAll() {
        return mapper.getSupplychainPartnerAll();
    }

    @Override
    @Cacheable(value = "supplychainPartner", key = "'getSupplychainPartnerCount_'+#status+'_'+#name")
    public int getSupplychainPartnerCount(int status, String name) {
        return mapper.getSupplychainPartnerCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainPartner", allEntries = true)
    public void changeSupplychainPartner(SupplychainPartner supplychainPartner) {
        mapper.changeSupplychainPartner(supplychainPartner);
    }

}
