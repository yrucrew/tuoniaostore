package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainLevelTemplate;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainLevelTemplateCache {

    void addSupplychainLevelTemplate(SupplychainLevelTemplate supplychainLevelTemplate);

    SupplychainLevelTemplate getSupplychainLevelTemplate(String id);

    List<SupplychainLevelTemplate> getSupplychainLevelTemplates(int status, int pageIndex, int pageSize, String name);
    List<SupplychainLevelTemplate> getSupplychainLevelTemplateAll();
    int getSupplychainLevelTemplateCount(int status, String name);
    void changeSupplychainLevelTemplate(SupplychainLevelTemplate supplychainLevelTemplate);

}
