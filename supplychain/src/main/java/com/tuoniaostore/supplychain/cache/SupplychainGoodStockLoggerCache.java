package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodStockLogger;
import com.tuoniaostore.supplychain.vo.SupplychainGoodStockLoggerListVo;

import java.util.Date;
import java.util.List;
/**
 * 商品的出入库日志
主要是针对商品的库存改变
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainGoodStockLoggerCache {

    void addSupplychainGoodStockLogger(SupplychainGoodStockLogger supplychainGoodStockLogger);

    SupplychainGoodStockLogger getSupplychainGoodStockLogger(String id);

    List<SupplychainGoodStockLogger> getSupplychainGoodStockLoggers(int status, int pageIndex, int pageSize, String name);
    List<SupplychainGoodStockLogger> getSupplychainGoodStockLoggerAll();
    int getSupplychainGoodStockLoggerCount(int status, String name );
    void changeSupplychainGoodStockLogger(SupplychainGoodStockLogger supplychainGoodStockLogger);

	List<SupplychainGoodStockLoggerListVo> getGoodStockLoggerList(int type, int operationType,
			String orderSequenceNumber, String goodName, String shopName, Date startTime, Date endTime,
			int pageStartIndex, int pageSize);

	int getGoodStockLoggerListCount(int type, int operationType,
			String orderSequenceNumber, String goodName, String shopName, Date startTime, Date endTime);

    void addSupplychainGoodStockLoggers(List<SupplychainGoodStockLogger> supplychainGoodStockLoggers);
}
