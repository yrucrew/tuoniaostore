package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainTimeTaskLock;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainTimeTaskLockCache {

    void addSupplychainTimeTaskLock(SupplychainTimeTaskLock supplychainTimeTaskLock);

    SupplychainTimeTaskLock getSupplychainTimeTaskLock(String id);

    List<SupplychainTimeTaskLock> getSupplychainTimeTaskLocks(int status, int pageIndex, int pageSize, String name);
    List<SupplychainTimeTaskLock> getSupplychainTimeTaskLockAll();
    int getSupplychainTimeTaskLockCount(int status, String name);
    void changeSupplychainTimeTaskLock(SupplychainTimeTaskLock supplychainTimeTaskLock);

}
