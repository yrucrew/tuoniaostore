package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.supplychain.vo.ShopReportMapperVO;

import java.util.List;
import java.util.Map;

/**
 * 仓库的属性
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainShopPropertiesCache {

    void addSupplychainShopProperties(SupplychainShopProperties supplychainShopProperties);

    SupplychainShopProperties getSupplychainShopProperties(String id);

    List<SupplychainShopProperties> getSupplychainShopPropertiess(int status, int pageIndex, int pageSize, String name,String type,int auth);

    List<SupplychainShopProperties> getSupplychainShopPropertiess2(int status, Integer pageStartIndex, Integer pageSize, String nameOrPhone, String type, int auth,Map<String,String>map);

    List<SupplychainShopProperties> getSupplychainShopPropertiesAll(int status,String types);
    int getSupplychainShopPropertiesCount(int status, String name,String type, int auth ,Map<String,String> map);
    void changeSupplychainShopProperties(SupplychainShopProperties supplychainShopProperties);

    /**
     * 获取当前用户的商店信息
     * @param userId
     */
    SupplychainShopProperties getSupplychainShopPropertiesByUserId(String userId);

    /**
     * 修改店铺名称
     * @param userId
     */
    void changeSupplychainShopPropertiesById(String userId,String name);

    /**
     * 查询当前的用户的供应店信息
     * @param userId
     * @param type
     * @return
     */
    SupplychainShopProperties selectSupplychainShopPropertiesById(String userId,int type);

    /**
     * 查找供应商端的shop
     * @param userId
     * @param types
     * @return
     */
    void changeSupplychainShopPropertiesStatusByIdAndTypes(String userId, List<Integer> types,int status);

    /**
     * 获取供应商端的信息
     * @param userId
     * @param list
     */
    SupplychainShopProperties getSupplychainShopPropertiesByUserIdAndTypeId(String userId, List<Integer> list);

    /**
     * 供应商端：修改供应商配送距离
     * @author oy
     * @date 2019/3/30
     * @param userId, list, coveringDistance
     * @return void
     */
    void changeSupplychainShopPropertiesCoveringDistanceByIdAndTypes(String userId, List<Integer> list, int coveringDistance);

    /**
     * 供应商端：修改打印机设备
     * @author oy
     * @date 2019/3/30
     * @param userId, list, bindPrinterCode
     * @return void
     */
    void changeSupplychainShopPropertiesBindPrinterCodeByIdAndTypes(String userId, List<Integer> list, String bindPrinterCode);

    /**
     * 供应商端：修改联系方式
     * @author oy
     * @date 2019/3/30
     * @param userId, list, phoneNumber
     * @return void
     */
    void changeSupplychainShopPropertiesPhoneNumber(String userId, List<Integer> list, String phoneNumber);
    /**
     * 查询所有仓库
     * @author sqd
     * @date 2019/4/8
     * @param id, type
     * @return com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties
     */
    List<SupplychainShopProperties> supplychainShopPropertiesAll();

    List<SupplychainShopProperties> getSupplychainShopPropertiesByType(List<String> types, Integer pageStartIndex, Integer pageSize);

    /**
     * 根据id查询仓库
     * @author sqd
     * @date 2019/4/18
     * @param
     * @return
     */
    SupplychainShopProperties getSupplychainShopPropertiesById( String id);

	int authSupplychainShopProperties(String id, int auth);

	List<SupplychainShopProperties> getSupplychainShopPropertiesByIds(List<String> shopIds);

    int getSupplychainShopPropertiesByIdsCount(List<String> shopIds);

    List<SupplychainShopProperties> getSupplychainShopPropertiesByIdsPage(List<String> shopIds, Integer pageStartIndex, Integer pageSize);

	/**
	 * 根据商品名称 模糊查询
	 * @author oy
	 * @date 2019/4/24
	 * @param shopName, types, pageStartIndex, pageSize
	 * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties>
	 */
    List<SupplychainShopProperties> getSupplychainShopPropertiesByShopName(String shopName, List<Integer> types, Integer pageStartIndex, Integer pageSize);


    int getSupplychainShopPropertiesByShopNameCount(String shopName, List<Integer> types);

    /**
     * 删除
     * @author oy
     * @date 2019/4/27
     * @param id
     * @return void
     */
    void deleteSupplychainShopProperties(List<String> id);

    /**
     * 根据用户id 和商店名称 获取对应的商店信息
     * @author oy
     * @date 2019/5/8
     * @param shopName, userId, pageStartIndex, pageSize
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties>
     */
    List<SupplychainShopProperties> getSupplychainShopPropertiesByShopNameAndUserId(String shopName, List<String> userId,List<Integer>typeIds, Integer pageStartIndex, Integer pageSize);

    /**
     * 根据用户id 和商店名称 获取对应的商店 条数
     * @author oy
     * @date 2019/5/9
     * @param shopName, userId
     * @return int
     */
    int getCountSupplychainShopPropertiesByShopNameAndUserId(String shopName,List<Integer>typeIds, List<String> userId);

    /**
     * 获取可以绑定的仓库
     * @author oy
     * @date 2019/5/26
     * @param pageStartIndex, pageSize
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties>
     */
    List<SupplychainShopProperties> getSupplychainShopPropertiesForBindingSelect(Integer pageStartIndex, Integer pageSize);

    /**
     * 条件更新仓库信息
     * @author oy
     * @date 2019/5/26
     * @param map
     * @return void
     */
    void updateSupplychainShopPropertiesByParam(Map<String, Object> map);

    SupplychainShopProperties UpdateBindPrinterCodeByUserId(String userId, String bindPrinterCode);

    int getSupplychainShopPropertiesByTypeCount(List<String> list);

    List<SupplychainShopProperties> getOwnAndUnbindShopProperties(String shopName, String shopId, List<Integer> types, int pageStartIndex, int pageSize);

    int getOwnAndUnbindShopPropertiesCount(String shopId, List<Integer> types);

    List<ShopReportMapperVO>  getSupplychainShopPropertiesByTypeCountForEveryDay(List<String> list, String startTime, String endTime);

    List<ShopReportMapperVO>  getSupplychainShopPropertiesByTypeCountForEveryWeek(List<String> list, String startTime, String endTime);

    List<ShopReportMapperVO>  getSupplychainShopPropertiesByTypeCountForEveryMonth(List<String> list, String startTime, String endTime);
}
