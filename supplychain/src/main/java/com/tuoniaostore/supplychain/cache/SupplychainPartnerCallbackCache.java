package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainPartnerCallback;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainPartnerCallbackCache {

    void addSupplychainPartnerCallback(SupplychainPartnerCallback supplychainPartnerCallback);

    SupplychainPartnerCallback getSupplychainPartnerCallback(String id);

    List<SupplychainPartnerCallback> getSupplychainPartnerCallbacks(int status, int pageIndex, int pageSize, String name);
    List<SupplychainPartnerCallback> getSupplychainPartnerCallbackAll();
    int getSupplychainPartnerCallbackCount(int status, String name);
    void changeSupplychainPartnerCallback(SupplychainPartnerCallback supplychainPartnerCallback);

}
