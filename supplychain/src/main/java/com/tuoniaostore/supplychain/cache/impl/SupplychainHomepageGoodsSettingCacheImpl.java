package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageButtonSetting;
        import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageGoodsSetting;
        import com.tuoniaostore.datamodel.vo.supplychain.SupplychainHomePageGoodDetail;
        import com.tuoniaostore.supplychain.cache.SupplychainHomepageGoodsSettingCache;
        import com.tuoniaostore.supplychain.data.SupplychainHomepageGoodsSettingMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 首页设置表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-04 14:40:15
 */
@Component
@CacheConfig(cacheNames = "supplychainHomepageGoodsSetting")
public class SupplychainHomepageGoodsSettingCacheImpl implements SupplychainHomepageGoodsSettingCache {

    @Autowired
    SupplychainHomepageGoodsSettingMapper mapper;

    @Override
    @CacheEvict(value = "supplychainHomepageGoodsSetting", allEntries = true)
    public void addSupplychainHomepageGoodsSetting(SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting) {
        mapper.addSupplychainHomepageGoodsSetting(supplychainHomepageGoodsSetting);
    }

    @Override
    @Cacheable(value = "supplychainHomepageGoodsSetting", key = "'getSupplychainHomepageGoodsSetting_'+#id")
    public   SupplychainHomepageGoodsSetting getSupplychainHomepageGoodsSetting(String id) {
        return mapper.getSupplychainHomepageGoodsSetting(id);
    }

    @Override
    @Cacheable(value = "supplychainHomepageGoodsSetting", key = "'getSupplychainHomepageGoodsSettings_'+#pageIndex+'_'+#pageSize+'_'+'_'+#name")
    public List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettings(int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainHomepageGoodsSettings(pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "supplychainHomepageGoodsSetting", key = "'getSupplychainHomepageGoodsSettingAll'")
    public List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettingAll() {
        return mapper.getSupplychainHomepageGoodsSettingAll();
    }

    @Override
    @Cacheable(value = "supplychainHomepageGoodsSetting", key = "'getSupplychainHomepageGoodsSettingCount_'+#name")
    public int getSupplychainHomepageGoodsSettingCount(String name) {
        return mapper.getSupplychainHomepageGoodsSettingCount( name);
    }

    @Override
    @CacheEvict(value = "supplychainHomepageGoodsSetting", allEntries = true)
    public void changeSupplychainHomepageGoodsSetting(SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting) {
        mapper.changeSupplychainHomepageGoodsSetting(supplychainHomepageGoodsSetting);
    }

    @Override
    @Cacheable(value = "supplychainHomepageGoodsSetting", key = "'getSupplychainHomepageGoodsSettingByAreaIdAndType_'+#areaId+'_'+#type+'_'+#pageIndex+'_'+#pageSize")
    public List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettingByAreaIdAndType(String areaId, Integer type,Integer pageIndex,Integer pageSize) {
        return mapper.getSupplychainHomepageGoodsSettingByAreaIdAndType(areaId,type,pageIndex,pageSize);
    }

    @Override
    @Cacheable(value = "supplychainHomepageGoodsSetting", key = "'getSupplychainHomepageGoodsSettingByAreaIdsAndType_'+#areaIds+'_'+#type+'_'+#pageIndex+'_'+#pageSize")
    public List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettingByAreaIdsAndType(List<String> areaIds, Integer type, Integer pageIndex, Integer pageSize) {
        return mapper.getSupplychainHomepageGoodsSettingByAreaIdsAndType(areaIds,type,pageIndex,pageSize);
    }

    @Override
    @CacheEvict(value = "supplychainHomepageGoodsSetting", allEntries = true)
    public void bacchDeleteHomeSettingBy(List<String> ids) {
        mapper.bacchDeleteHomeSettingBy(ids);
    }

    @Override
    @CacheEvict(value = "supplychainHomepageGoodsSetting", allEntries = true)
    public void addSupplychainHomepageGoodsSettings(List<SupplychainHomepageGoodsSetting> list) {
        mapper.addSupplychainHomepageGoodsSettings(list);
    }

    @Override
    @CacheEvict(value = "supplychainHomepageGoodsSetting", allEntries = true)
    public void batchChangeHomeGoods(List<SupplychainHomepageGoodsSetting> list) {
        mapper.batchChangeHomeGoods(list);
    }

    @Override
    @CacheEvict(value = "supplychainHomepageGoodsSetting", allEntries = true)
    public void DeleteHomeSettingByAreaId(String areaId,Integer typeNum) {
        mapper.DeleteHomeSettingByAreaId(areaId,typeNum);
    }

}
