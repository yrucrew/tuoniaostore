package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodBindRelationship;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopBindRelationship;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainGoodBindRelationshipCache {

    void addSupplychainGoodBindRelationship(SupplychainGoodBindRelationship supplychainGoodBindRelationship);

    SupplychainGoodBindRelationship getSupplychainGoodBindRelationship(String id);

    List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationships(int status, int pageIndex, int pageSize, String name);
    List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipAll();
    int getSupplychainGoodBindRelationshipCount(int status, String name);
    void changeSupplychainGoodBindRelationship(SupplychainGoodBindRelationship supplychainGoodBindRelationship);

    /**
     * 根据用户id 查找当前所有的类型
     * @param userId
     * @return
     */
    List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipByUserId(String userId,Integer pageIndex, Integer pageSize);

    SupplychainGoodBindRelationship getSupplychainGoodBindRelationshipByUserIdAndTypeId(String userId, String typeId);

    /**
     * 删除分类
     * @author oy
     * @date 2019/4/10
     * @param userId, typeId
     * @return com.tuoniaostore.datamodel.supplychain.SupplychainGoodBindRelationship
     */
    void delSupplychainGoodBindRelationshipByUserIdAndTypeId(String userId, String typeId);

    /**
     * 添加绑定集合
     * @author oy
     * @date 2019/4/23
     * @param supplychainGoodBindRelationship
     * @return void
     */
    void addSupplychainGoodBindRelationships(List<SupplychainGoodBindRelationship> supplychainGoodBindRelationship);

    /**
     * 根据ids获取对应的数据
     * @author oy
     * @date 2019/5/8
     * @param userIds
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainGoodBindRelationship>
     */
    List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipByUserIds(List<String> userIds);

    /**
     * 根据用户ids 和types 获取对应的记录
     * @author oy
     * @date 2019/5/8
     * @param userIds, typeIds
     * @return void
     */
    List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationShipsByUseridsAndTypeIds(List<String> userIds, List<String> typeIds);

    /**
     * 清除该用户的所有分类
     * @author oy
     * @date 2019/5/8
     * @param userId
     * @return void
     */
    void clearSupplychainGoodBindRelationships(String userId);

    /**
     * 批量添加分类
     * @author oy
     * @date 2019/5/8
     * @param entitys
     * @return void
     */
    void batchAddSupplychainGoodBindRelationships(List<SupplychainGoodBindRelationship> entitys);
}
