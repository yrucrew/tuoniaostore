package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainUserDailyBuyLogger;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainUserDailyBuyLoggerCache {

    void addSupplychainUserDailyBuyLogger(SupplychainUserDailyBuyLogger supplychainUserDailyBuyLogger);

    SupplychainUserDailyBuyLogger getSupplychainUserDailyBuyLogger(String id);

    List<SupplychainUserDailyBuyLogger> getSupplychainUserDailyBuyLoggers(int status, int pageIndex, int pageSize, String name);
    List<SupplychainUserDailyBuyLogger> getSupplychainUserDailyBuyLoggerAll();
    int getSupplychainUserDailyBuyLoggerCount(int status, String name);
    void changeSupplychainUserDailyBuyLogger(SupplychainUserDailyBuyLogger supplychainUserDailyBuyLogger);

}
