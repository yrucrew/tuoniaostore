package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainShopProxyRelation;
        import com.tuoniaostore.supplychain.cache.SupplychainShopProxyRelationCache;
        import com.tuoniaostore.supplychain.data.SupplychainShopProxyRelationMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainShopProxyRelation")
public class SupplychainShopProxyRelationCacheImpl implements SupplychainShopProxyRelationCache {

    @Autowired
    SupplychainShopProxyRelationMapper mapper;

    @Override
    @CacheEvict(value = "supplychainShopProxyRelation", allEntries = true)
    public void addSupplychainShopProxyRelation(SupplychainShopProxyRelation supplychainShopProxyRelation) {
        mapper.addSupplychainShopProxyRelation(supplychainShopProxyRelation);
    }

    @Override
    @Cacheable(value = "supplychainShopProxyRelation", key = "'getSupplychainShopProxyRelation_'+#id")
    public   SupplychainShopProxyRelation getSupplychainShopProxyRelation(String id) {
        return mapper.getSupplychainShopProxyRelation(id);
    }

    @Override
    @Cacheable(value = "supplychainShopProxyRelation", key = "'getSupplychainShopProxyRelations_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainShopProxyRelation> getSupplychainShopProxyRelations(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainShopProxyRelations( status,pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "supplychainShopProxyRelation", key = "'getSupplychainShopProxyRelationAll'")
    public List<SupplychainShopProxyRelation> getSupplychainShopProxyRelationAll() {
        return mapper.getSupplychainShopProxyRelationAll();
    }

    @Override
    @Cacheable(value = "supplychainShopProxyRelation", key = "'getSupplychainShopProxyRelationCount_'+#status+'_'+#name")
    public int getSupplychainShopProxyRelationCount(int status, String name) {
        return mapper.getSupplychainShopProxyRelationCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainShopProxyRelation", allEntries = true)
    public void changeSupplychainShopProxyRelation(SupplychainShopProxyRelation supplychainShopProxyRelation) {
        mapper.changeSupplychainShopProxyRelation(supplychainShopProxyRelation);
    }

}
