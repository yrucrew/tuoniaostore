package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainRoleProperties;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainRolePropertiesCache {

    void addSupplychainRoleProperties(SupplychainRoleProperties supplychainRoleProperties);

    SupplychainRoleProperties getSupplychainRoleProperties(String id);

    List<SupplychainRoleProperties> getSupplychainRolePropertiess(int status, int pageIndex, int pageSize, String name);
    List<SupplychainRoleProperties> getSupplychainRolePropertiesAll();
    int getSupplychainRolePropertiesCount(int status, String name);
    void changeSupplychainRoleProperties(SupplychainRoleProperties supplychainRoleProperties);

}
