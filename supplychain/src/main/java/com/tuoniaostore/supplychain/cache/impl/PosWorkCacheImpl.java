package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.PosWork;
        import com.tuoniaostore.supplychain.cache.PosWorkCache;
        import com.tuoniaostore.supplychain.data.PosWorkMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;
        import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-06 10:47:35
 */
@Component
@CacheConfig(cacheNames = "posWork")
public class PosWorkCacheImpl implements PosWorkCache {

    @Autowired
    PosWorkMapper mapper;

    @Override
    @CacheEvict(value = "posWork", allEntries = true)
    public void addPosWork(PosWork posWork) {
        mapper.addPosWork(posWork);
    }

    @Override
    @Cacheable(value = "posWork", key = "'getPosWork_'+#id")
    public PosWork getPosWork(String id) {
        return mapper.getPosWork(id);
    }

    @Override
    @Cacheable(value = "posWork", key = "'getPosWorks_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name+'_'+#retrieveStatus")
    public List<PosWork> getPosWorks(int status, int pageIndex, int pageSize, String name,int retrieveStatus) {
        return mapper.getPosWorks(pageIndex, pageSize, status, name,retrieveStatus);
    }

    @Override
    @Cacheable(value = "posWork", key = "'getPosWorkAll'")
    public List<PosWork> getPosWorkAll() {
        return mapper.getPosWorkAll();
    }

    @Override
    @Cacheable(value = "posWork", key = "'getPosWorkCount_'+#status+'_'+#name+'_'+#retrieveStatus")
    public int getPosWorkCount(int status, String name,int retrieveStatus) {
        return mapper.getPosWorkCount(status, name,retrieveStatus);
    }

    @Override
    @CacheEvict(value = "posWork", allEntries = true)
    public void changePosWork(PosWork posWork) {
        mapper.changePosWork(posWork);
    }

    @Override
    @Cacheable(value = "posWork", key = "'getPosWrokByMap_'+#map")
    public PosWork getPosWrokByMap(Map map) {
        return mapper.getPosWrokByMap(map);
    }

    @Override
    @Cacheable(value = "posWork", key = "'getPosWrokListByMap_'+#map")
    public List<PosWork> getPosWrokListByMap(Map map) {
        return  mapper.getPosWrokListByMap(map);
    }

    @Override
    @Cacheable(value = "posWork", key = "'getPosWrokListCountByMap_'+#map")
    public int getPosWrokListCountByMap(Map map) {
        return mapper.getPosWrokListCountByMap(map);
    }

    @Override
    public void clearSupplyShopGoodSale() {
        mapper.clearSupplyShopGoodSale();
    }

}
