package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainUserDailyBuyLogger;
        import com.tuoniaostore.supplychain.cache.SupplychainUserDailyBuyLoggerCache;
        import com.tuoniaostore.supplychain.data.SupplychainUserDailyBuyLoggerMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainUserDailyBuyLogger")
public class SupplychainUserDailyBuyLoggerCacheImpl implements SupplychainUserDailyBuyLoggerCache {

    @Autowired
    SupplychainUserDailyBuyLoggerMapper mapper;

    @Override
    @CacheEvict(value = "supplychainUserDailyBuyLogger", allEntries = true)
    public void addSupplychainUserDailyBuyLogger(SupplychainUserDailyBuyLogger supplychainUserDailyBuyLogger) {
        mapper.addSupplychainUserDailyBuyLogger(supplychainUserDailyBuyLogger);
    }

    @Override
    @Cacheable(value = "supplychainUserDailyBuyLogger", key = "'getSupplychainUserDailyBuyLogger_'+#id")
    public   SupplychainUserDailyBuyLogger getSupplychainUserDailyBuyLogger(String id) {
        return mapper.getSupplychainUserDailyBuyLogger(id);
    }

    @Override
    @Cacheable(value = "supplychainUserDailyBuyLogger", key = "'getSupplychainUserDailyBuyLoggers_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainUserDailyBuyLogger> getSupplychainUserDailyBuyLoggers(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainUserDailyBuyLoggers(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "supplychainUserDailyBuyLogger", key = "'getSupplychainUserDailyBuyLoggerAll'")
    public List<SupplychainUserDailyBuyLogger> getSupplychainUserDailyBuyLoggerAll() {
        return mapper.getSupplychainUserDailyBuyLoggerAll();
    }

    @Override
    @Cacheable(value = "supplychainUserDailyBuyLogger", key = "'getSupplychainUserDailyBuyLoggerCount_'+#status+'_'+#name")
    public int getSupplychainUserDailyBuyLoggerCount(int status, String name) {
        return mapper.getSupplychainUserDailyBuyLoggerCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainUserDailyBuyLogger", allEntries = true)
    public void changeSupplychainUserDailyBuyLogger(SupplychainUserDailyBuyLogger supplychainUserDailyBuyLogger) {
        mapper.changeSupplychainUserDailyBuyLogger(supplychainUserDailyBuyLogger);
    }

}
