package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainLevelTemplate;
        import com.tuoniaostore.supplychain.cache.SupplychainLevelTemplateCache;
        import com.tuoniaostore.supplychain.data.SupplychainLevelTemplateMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainLevelTemplate")
public class SupplychainLevelTemplateCacheImpl implements SupplychainLevelTemplateCache {

    @Autowired
    SupplychainLevelTemplateMapper mapper;

    @Override
    @CacheEvict(value = "supplychainLevelTemplate", allEntries = true)
    public void addSupplychainLevelTemplate(SupplychainLevelTemplate supplychainLevelTemplate) {
        mapper.addSupplychainLevelTemplate(supplychainLevelTemplate);
    }

    @Override
    @Cacheable(value = "supplychainLevelTemplate", key = "'getSupplychainLevelTemplate_'+#id")
    public   SupplychainLevelTemplate getSupplychainLevelTemplate(String id) {
        return mapper.getSupplychainLevelTemplate(id);
    }

    @Override
    @Cacheable(value = "supplychainLevelTemplate", key = "'getSupplychainLevelTemplates_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainLevelTemplate> getSupplychainLevelTemplates(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainLevelTemplates( status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "supplychainLevelTemplate", key = "'getSupplychainLevelTemplateAll'")
    public List<SupplychainLevelTemplate> getSupplychainLevelTemplateAll() {
        return mapper.getSupplychainLevelTemplateAll();
    }

    @Override
    @Cacheable(value = "supplychainLevelTemplate", key = "'getSupplychainLevelTemplateCount_'+#status+'_'+#name")
    public int getSupplychainLevelTemplateCount(int status, String name) {
        return mapper.getSupplychainLevelTemplateCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainLevelTemplate", allEntries = true)
    public void changeSupplychainLevelTemplate(SupplychainLevelTemplate supplychainLevelTemplate) {
        mapper.changeSupplychainLevelTemplate(supplychainLevelTemplate);
    }

}
