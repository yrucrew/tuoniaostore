package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainShopDailyGoodSalesLogger;
        import com.tuoniaostore.supplychain.cache.SupplychainShopDailyGoodSalesLoggerCache;
        import com.tuoniaostore.supplychain.data.SupplychainShopDailyGoodSalesLoggerMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainShopDailyGoodSalesLogger")
public class SupplychainShopDailyGoodSalesLoggerCacheImpl implements SupplychainShopDailyGoodSalesLoggerCache {

    @Autowired
    SupplychainShopDailyGoodSalesLoggerMapper mapper;

    @Override
    @CacheEvict(value = "supplychainShopDailyGoodSalesLogger", allEntries = true)
    public void addSupplychainShopDailyGoodSalesLogger(SupplychainShopDailyGoodSalesLogger supplychainShopDailyGoodSalesLogger) {
        mapper.addSupplychainShopDailyGoodSalesLogger(supplychainShopDailyGoodSalesLogger);
    }

    @Override
    @Cacheable(value = "supplychainShopDailyGoodSalesLogger", key = "'getSupplychainShopDailyGoodSalesLogger_'+#id")
    public   SupplychainShopDailyGoodSalesLogger getSupplychainShopDailyGoodSalesLogger(String id) {
        return mapper.getSupplychainShopDailyGoodSalesLogger(id);
    }

    @Override
    @Cacheable(value = "supplychainShopDailyGoodSalesLogger", key = "'getSupplychainShopDailyGoodSalesLoggers_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainShopDailyGoodSalesLogger> getSupplychainShopDailyGoodSalesLoggers(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainShopDailyGoodSalesLoggers(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "supplychainShopDailyGoodSalesLogger", key = "'getSupplychainShopDailyGoodSalesLoggerAll'")
    public List<SupplychainShopDailyGoodSalesLogger> getSupplychainShopDailyGoodSalesLoggerAll() {
        return mapper.getSupplychainShopDailyGoodSalesLoggerAll();
    }

    @Override
    @Cacheable(value = "supplychainShopDailyGoodSalesLogger", key = "'getSupplychainShopDailyGoodSalesLoggerCount_'+#status+'_'+#name")
    public int getSupplychainShopDailyGoodSalesLoggerCount(int status, String name) {
        return mapper.getSupplychainShopDailyGoodSalesLoggerCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainShopDailyGoodSalesLogger", allEntries = true)
    public void changeSupplychainShopDailyGoodSalesLogger(SupplychainShopDailyGoodSalesLogger supplychainShopDailyGoodSalesLogger) {
        mapper.changeSupplychainShopDailyGoodSalesLogger(supplychainShopDailyGoodSalesLogger);
    }

}
