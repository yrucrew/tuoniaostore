package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainGoodModifyLogger;
        import com.tuoniaostore.supplychain.cache.SupplychainGoodModifyLoggerCache;
        import com.tuoniaostore.supplychain.data.SupplychainGoodModifyLoggerMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 商品库存修改日志
注意：该表主要记录人为介入修改的数据
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainGoodModifyLogger")
public class SupplychainGoodModifyLoggerCacheImpl implements SupplychainGoodModifyLoggerCache {

    @Autowired
    SupplychainGoodModifyLoggerMapper mapper;

    @Override
    @CacheEvict(value = "supplychainGoodModifyLogger", allEntries = true)
    public void addSupplychainGoodModifyLogger(SupplychainGoodModifyLogger supplychainGoodModifyLogger) {
        mapper.addSupplychainGoodModifyLogger(supplychainGoodModifyLogger);
    }

    @Override
    @Cacheable(value = "supplychainGoodModifyLogger", key = "'getSupplychainGoodModifyLogger_'+#id")
    public   SupplychainGoodModifyLogger getSupplychainGoodModifyLogger(String id) {
        return mapper.getSupplychainGoodModifyLogger(id);
    }

    @Override
    @Cacheable(value = "supplychainGoodModifyLogger", key = "'getSupplychainGoodModifyLoggers_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainGoodModifyLogger> getSupplychainGoodModifyLoggers(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainGoodModifyLoggers( status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "supplychainGoodModifyLogger", key = "'getSupplychainGoodModifyLoggerAll'")
    public List<SupplychainGoodModifyLogger> getSupplychainGoodModifyLoggerAll() {
        return mapper.getSupplychainGoodModifyLoggerAll();
    }

    @Override
    @Cacheable(value = "supplychainGoodModifyLogger", key = "'getSupplychainGoodModifyLoggerCount_'+#status+'_'+#name")
    public int getSupplychainGoodModifyLoggerCount(int status, String name) {
        return mapper.getSupplychainGoodModifyLoggerCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainGoodModifyLogger", allEntries = true)
    public void changeSupplychainGoodModifyLogger(SupplychainGoodModifyLogger supplychainGoodModifyLogger) {
        mapper.changeSupplychainGoodModifyLogger(supplychainGoodModifyLogger);
    }

}
