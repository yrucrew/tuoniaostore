package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainRole;
        import com.tuoniaostore.supplychain.cache.SupplychainRoleCache;
        import com.tuoniaostore.supplychain.data.SupplychainRoleMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@CacheConfig(cacheNames = "supplychainRole")
public class SupplychainRoleCacheImpl implements SupplychainRoleCache {

    @Autowired
    SupplychainRoleMapper mapper;

    @Override
    @CacheEvict(value = "supplychainRole", allEntries = true)
    public void addSupplychainRole(SupplychainRole supplychainRole) {
        mapper.addSupplychainRole(supplychainRole);
    }

    @Override
    @Cacheable(value = "supplychainRole", key = "'getSupplychainRole_'+#id")
    public   SupplychainRole getSupplychainRole(String id) {
        return mapper.getSupplychainRole(id);
    }

    @Override
    @Cacheable(value = "supplychainRole", key = "'getSupplychainRoles_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SupplychainRole> getSupplychainRoles(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSupplychainRoles(status, pageIndex, pageSize,   name);
    }

    @Override
    @Cacheable(value = "supplychainRole", key = "'getSupplychainRoleAll'")
    public List<SupplychainRole> getSupplychainRoleAll() {
        return mapper.getSupplychainRoleAll();
    }

    @Override
    @Cacheable(value = "supplychainRole", key = "'getSupplychainRoleCount_'+#status+'_'+#name")
    public int getSupplychainRoleCount(int status, String name) {
        return mapper.getSupplychainRoleCount(status, name);
    }

    @Override
    @CacheEvict(value = "supplychainRole", allEntries = true)
    public void changeSupplychainRole(SupplychainRole supplychainRole) {
        mapper.changeSupplychainRole(supplychainRole);
    }

}
