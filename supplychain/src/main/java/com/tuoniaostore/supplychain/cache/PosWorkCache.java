package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.PosWork;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-06 10:47:35
 */
public interface PosWorkCache {

    void addPosWork(PosWork posWork);

    PosWork getPosWork(String id);

    List<PosWork> getPosWorks(int status, int pageIndex, int pageSize, String name, int retrieveStatus);
    List<PosWork> getPosWorkAll();
    int getPosWorkCount(int status, String name, int retrieveStatus);
    void changePosWork(PosWork posWork);
    PosWork getPosWrokByMap(Map map);
    List<PosWork> getPosWrokListByMap(Map map);
    int getPosWrokListCountByMap(Map map);

    void clearSupplyShopGoodSale();
}
