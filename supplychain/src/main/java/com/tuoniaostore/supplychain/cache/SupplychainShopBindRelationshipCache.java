package com.tuoniaostore.supplychain.cache;

import com.tuoniaostore.datamodel.supplychain.SupplychainShopBindRelationship;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:24
 */
public interface SupplychainShopBindRelationshipCache {

    void addSupplychainShopBindRelationship(SupplychainShopBindRelationship supplychainShopBindRelationship);

    SupplychainShopBindRelationship getSupplychainShopBindRelationship(String id);

    List<SupplychainShopBindRelationship> getSupplychainShopBindRelationships(int status, int pageIndex, int pageSize, String name);

    List<SupplychainShopBindRelationship> getSupplychainShopBindRelationshipAll();

    int getSupplychainShopBindRelationshipCount(int status, String name);

    void changeSupplychainShopBindRelationship(SupplychainShopBindRelationship supplychainShopBindRelationship);

    List<SupplychainShopBindRelationship> getShopBindRelationships(String roleId, int type);

    SupplychainShopBindRelationship getShopBindRelationship(String roleId, int type, String shopId);

    List<SupplychainShopBindRelationship> getShopBindRelationshipsByRoleIds(String[] roleIds, int type);

    int delSupplychainShopBindRelationships(int type, String roleId);

    List<SupplychainShopBindRelationship> getRelationshipsByShopIdAndType(String shopId, int type, Integer pageIndex, Integer pageSize);

    int updateRelationshipByUserIdAndType(String roleId, int type, int status);

    /**
     * @param userId 用户id
     * @param type   绑定类型
     * @return SupplychainShopBindRelationship
     */
    SupplychainShopBindRelationship getSupplychainShopBindRelationshipByUserIdAndType(String userId, int type);
}
