package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodModifyLogger;

import java.util.List;
/**
 * 商品库存修改日志
注意：该表主要记录人为介入修改的数据
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainGoodModifyLoggerCache {

    void addSupplychainGoodModifyLogger(SupplychainGoodModifyLogger supplychainGoodModifyLogger);

    SupplychainGoodModifyLogger getSupplychainGoodModifyLogger(String id);

    List<SupplychainGoodModifyLogger> getSupplychainGoodModifyLoggers(int status, int pageIndex, int pageSize, String name);
    List<SupplychainGoodModifyLogger> getSupplychainGoodModifyLoggerAll();
    int getSupplychainGoodModifyLoggerCount(int status, String name );
    void changeSupplychainGoodModifyLogger(SupplychainGoodModifyLogger supplychainGoodModifyLogger);

}
