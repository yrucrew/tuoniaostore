package com.tuoniaostore.supplychain.cache;

import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseBindRelationship;

import java.util.List;

public interface SupplychainWarehourseBindRelationshipCache {

    List<SupplychainWarehourseBindRelationship> getSupplychainWarehourseBindRelationships(Integer pageIndex, Integer pageSize);

    List<SupplychainWarehourseBindRelationship> getSupplychainWarehourseBindRelationshipsByShopIdsAndType(String shopId, int type, Integer pageStartIndex, Integer pageSize);

    void deleteSupplychainWarehourseBindRelationship(String shopId,int type);

    void batchAddSupplychainWarehourseBindRelationships(List<SupplychainWarehourseBindRelationship> entity);

    SupplychainWarehourseBindRelationship getSupplychainWarehourseBindRelationshipsByBindShopId(String bindShopId,Integer type);

    SupplychainWarehourseBindRelationship getSupplychainWarehourseBindRelationshipsByShopId(String shopId,Integer type);
}
