package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainPartnerShopFocusGood;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainPartnerShopFocusGoodCache {

    void addSupplychainPartnerShopFocusGood(SupplychainPartnerShopFocusGood supplychainPartnerShopFocusGood);

    SupplychainPartnerShopFocusGood getSupplychainPartnerShopFocusGood(String id);

    List<SupplychainPartnerShopFocusGood> getSupplychainPartnerShopFocusGoods(int status, int pageIndex, int pageSize, String name);
    List<SupplychainPartnerShopFocusGood> getSupplychainPartnerShopFocusGoodAll();
    int getSupplychainPartnerShopFocusGoodCount(int status, String name);
    void changeSupplychainPartnerShopFocusGood(SupplychainPartnerShopFocusGood supplychainPartnerShopFocusGood);

}
