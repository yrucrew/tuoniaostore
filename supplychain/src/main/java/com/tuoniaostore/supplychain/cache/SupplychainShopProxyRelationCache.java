package com.tuoniaostore.supplychain.cache;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProxyRelation;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
public interface SupplychainShopProxyRelationCache {

    void addSupplychainShopProxyRelation(SupplychainShopProxyRelation supplychainShopProxyRelation);

    SupplychainShopProxyRelation getSupplychainShopProxyRelation(String id);

    List<SupplychainShopProxyRelation> getSupplychainShopProxyRelations(int status, int pageIndex, int pageSize, String name);
    List<SupplychainShopProxyRelation> getSupplychainShopProxyRelationAll();
    int getSupplychainShopProxyRelationCount(int status, String name);
    void changeSupplychainShopProxyRelation(SupplychainShopProxyRelation supplychainShopProxyRelation);

}
