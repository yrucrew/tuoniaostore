package com.tuoniaostore.supplychain.cache.impl;
        import com.tuoniaostore.datamodel.supplychain.SupplychainShopGoodSale;
        import com.tuoniaostore.supplychain.cache.SupplychainShopGoodSaleCache;
        import com.tuoniaostore.supplychain.data.SupplychainShopGoodSaleMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-20 20:05:41
 */
@Component
@CacheConfig(cacheNames = "supplychainShopGoodSale")
public class SupplychainShopGoodSaleCacheImpl implements SupplychainShopGoodSaleCache {

    @Autowired
    SupplychainShopGoodSaleMapper mapper;

    @Override
    @CacheEvict(value = "supplychainShopGoodSale", allEntries = true)
    public void addSupplychainShopGoodSale(SupplychainShopGoodSale supplychainShopGoodSale) {
        mapper.addSupplychainShopGoodSale(supplychainShopGoodSale);
    }

    @Override
    @Cacheable(value = "supplychainShopGoodSale", key = "'getSupplychainShopGoodSale_'+#id")
    public   SupplychainShopGoodSale getSupplychainShopGoodSale(String id) {
        return mapper.getSupplychainShopGoodSale(id);
    }

    @Override
    @Cacheable(value = "supplychainShopGoodSale", key = "'getSupplychainShopGoodSales_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name+'_'+#retrieveStatus")
    public List<SupplychainShopGoodSale> getSupplychainShopGoodSales(int status, int pageIndex, int pageSize, String name,int retrieveStatus) {
        return mapper.getSupplychainShopGoodSales(pageIndex, pageSize, status, name,retrieveStatus);
    }

    @Override
    @Cacheable(value = "supplychainShopGoodSale", key = "'getSupplychainShopGoodSaleAll'")
    public List<SupplychainShopGoodSale> getSupplychainShopGoodSaleAll() {
        return mapper.getSupplychainShopGoodSaleAll();
    }

    @Override
    @Cacheable(value = "supplychainShopGoodSale", key = "'getSupplychainShopGoodSaleCount_'+#status+'_'+#name+'_'+#retrieveStatus")
    public int getSupplychainShopGoodSaleCount(int status, String name,int retrieveStatus) {
        return mapper.getSupplychainShopGoodSaleCount(status, name,retrieveStatus);
    }

    @Override
    @CacheEvict(value = "supplychainShopGoodSale", allEntries = true)
    public void changeSupplychainShopGoodSale(SupplychainShopGoodSale supplychainShopGoodSale) {
        mapper.changeSupplychainShopGoodSale(supplychainShopGoodSale);
    }

    @Override
    @CacheEvict(value = "supplychainShopGoodSale", allEntries = true)
    public void batchAddSupplychainShopGoodSales(List<SupplychainShopGoodSale> supplychainShopGoodSales) {
        mapper.batchAddSupplychainShopGoodSales(supplychainShopGoodSales);
    }

    @Override
    @Cacheable(value = "supplychainShopGoodSale", key = "'getSupplychainShopGoodSaleByList_'+#shopId+'_'+#typeId+'_'+#barcode+'_'+#startTime+'_'+#endTime+'_'+#pageStartIndex+'_'+#pageSize")
    public List<SupplychainShopGoodSale> getSupplychainShopGoodSaleByList(String shopId, String typeId,String barcode, String startTime, String endTime, Integer pageStartIndex, Integer pageSize) {
        return mapper.getSupplychainShopGoodSaleByList(shopId,typeId,barcode,startTime,endTime,pageStartIndex,pageSize);
    }

    @Override
    @Cacheable(value = "supplychainShopGoodSale", key = "'getSupplychainShopGoodSaleByListCount_'+#shopId+'_'+#barcode+'_'+#typeId+'_'+#startTime+'_'+#endTime")
    public int getSupplychainShopGoodSaleByListCount(String shopId, String typeId, String barcode,String startTime, String endTime) {
        return mapper.getSupplychainShopGoodSaleByListCount(shopId,typeId,barcode,startTime,endTime);
    }

}
