package com.tuoniaostore.supplychain.vo;

import com.tuoniaostore.datamodel.good.*;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;

import java.io.Serializable;
import java.util.List;

/**
 * Created by liyunbiao on 2019/5/18.
 */
public class SupplychainGoodPOSVo implements Serializable {
    private String id;
    private String goodId;
    //对应的模版ID
    private String goodTemplateId;
    //对应的模板NAME
    private String goodTemplateName;
    private String unitNmae;
    private List<GoodBarcode> barCode;
    private Integer defaultSort = 0;
    //归属仓库ID
    private String shopId;
    //仓库名称
    private String shopName;
    //当前库存(真实库存) 欠货时为负数
    private Integer stock;
    //当前状态 -1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效
    private Integer status;
    private Integer shelfLife;
    //成本价
    private Long costPrice;
    //一般销售价
    private Long sellPrice;
    private Long price;
    //一般市场价
    private Long marketPrice;
    //入库时间
    private String createTime;
    //价格id
    private String priceId;
    private String priceName;
    //商品类别id
    private String goodTypeId;

    public String getUnitNmae() {
        return unitNmae;
    }

    public void setUnitNmae(String unitNmae) {
        this.unitNmae = unitNmae;
    }

    public String getPriceName() {
        return priceName;
    }

    public void setPriceName(String priceName) {
        this.priceName = priceName;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }



    public List<GoodBarcode> getBarCode() {
        return barCode;
    }

    public void setBarCode(List<GoodBarcode> barCode) {
        this.barCode = barCode;
    }

    public String getGoodTypeId() {
        return goodTypeId;
    }

    public void setGoodTypeId(String goodTypeId) {
        this.goodTypeId = goodTypeId;
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getGoodTemplateName() {
        return goodTemplateName;
    }

    public void setGoodTemplateName(String goodTemplateName) {
        this.goodTemplateName = goodTemplateName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(Integer shelfLife) {
        this.shelfLife = shelfLife;
    }

    /**
     * 设置：
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public String getId() {
        return id;
    }

    /**
     * 设置：对应的模版ID
     */
    public void setGoodTemplateId(String goodTemplateId) {
        this.goodTemplateId = goodTemplateId;
    }

    /**
     * 获取：对应的模版ID
     */
    public String getGoodTemplateId() {
        return goodTemplateId;
    }


    /**
     * 设置：默认排序 值越小 越靠前
     */
    public void setDefaultSort(Integer defaultSort) {
        this.defaultSort = defaultSort;
    }

    /**
     * 获取：默认排序 值越小 越靠前
     */
    public Integer getDefaultSort() {
        return defaultSort;
    }

    /**
     * 设置：归属仓库ID
     */
    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    /**
     * 获取：归属仓库ID
     */
    public String getShopId() {
        return shopId;
    }

    /**
     * 设置：当前库存(真实库存) 欠货时为负数
     */
    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * 获取：当前库存(真实库存) 欠货时为负数
     */
    public Integer getStock() {
        return stock;
    }



    /**
     * 设置：当前状态 -1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取：当前状态 -1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置：成本价
     */
    public void setCostPrice(Long costPrice) {
        this.costPrice = costPrice;
    }

    /**
     * 获取：成本价
     */
    public Long getCostPrice() {
        return costPrice;
    }

    /**
     * 设置：一般销售价
     */
    public void setSellPrice(Long sellPrice) {
        this.sellPrice = sellPrice;
    }

    /**
     * 获取：一般销售价
     */
    public Long getSellPrice() {
        return sellPrice;
    }

    /**
     * 设置：一般市场价
     */
    public void setMarketPrice(Long marketPrice) {
        this.marketPrice = marketPrice;
    }

    /**
     * 获取：一般市场价
     */
    public Long getMarketPrice() {
        return marketPrice;
    }



    /**
     * 设置：限购数量 -1-不限购 0-不出售 其他正数表示限购数量
     */

    /**
     * 设置：入库时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取：入库时间
     */
    public String getCreateTime() {
        return createTime;
    }


}
