package com.tuoniaostore.supplychain.vo;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class PurchaseBindGoodVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String purchaseUserId; // 采购员ID
	private String purchaseRealName; // 采购员名字
	private String roleName; // 角色名称
	private String goodTemplateName; // 商品名
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime; // 创建时间

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getPurchaseUserId() {
		return purchaseUserId;
	}
	public void setPurchaseUserId(String purchaseUserId) {
		this.purchaseUserId = purchaseUserId;
	}
	public String getPurchaseRealName() {
		return purchaseRealName;
	}
	public void setPurchaseRealName(String purchaseRealName) {
		this.purchaseRealName = purchaseRealName;
	}
	public String getGoodTemplateName() {
		return goodTemplateName;
	}
	public void setGoodTemplateName(String goodTemplateName) {
		this.goodTemplateName = goodTemplateName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
