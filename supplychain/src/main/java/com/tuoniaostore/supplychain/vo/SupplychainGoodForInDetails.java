package com.tuoniaostore.supplychain.vo;

import java.io.Serializable;

/**
 * @author oy
 * @description
 * @date 2019/4/24
 */
public class SupplychainGoodForInDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;      //商品id
    private String orderGoodSnapshotId;//快照id
    private String goodName;    //商品名称
    private String goodBarcode; //商品条码
    private String shopName;    //仓库名称
    private Integer inNum; //已经有的数量
    private Integer operationNum; //操作数量
    private String priceId;//价格标签id

    public SupplychainGoodForInDetails() {
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getOrderGoodSnapshotId() {
        return orderGoodSnapshotId;
    }

    public void setOrderGoodSnapshotId(String orderGoodSnapshotId) {
        this.orderGoodSnapshotId = orderGoodSnapshotId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodBarcode() {
        return goodBarcode;
    }

    public void setGoodBarcode(String goodBarcode) {
        this.goodBarcode = goodBarcode;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getInNum() {
        return inNum;
    }

    public void setInNum(Integer inNum) {
        this.inNum = inNum;
    }

    public Integer getOperationNum() {
        return operationNum;
    }

    public void setOperationNum(Integer operationNum) {
        this.operationNum = operationNum;
    }
}
