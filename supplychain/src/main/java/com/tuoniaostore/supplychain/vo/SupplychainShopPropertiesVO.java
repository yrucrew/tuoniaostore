package com.tuoniaostore.supplychain.vo;

import java.io.Serializable;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/27
 */
public class SupplychainShopPropertiesVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String shopName;
    private String phoneNumber;
    private String address;

    public SupplychainShopPropertiesVO() {
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
