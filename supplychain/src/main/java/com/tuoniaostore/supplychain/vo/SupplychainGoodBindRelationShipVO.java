package com.tuoniaostore.supplychain.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author oy
 * @description 商品绑定商品分类list
 * @date 2019/5/6
 */
public class SupplychainGoodBindRelationShipVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String shopId;
    private String shopName;
    private String typeName;
    private List<String> typeList;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public List<String> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<String> typeList) {
        this.typeList = typeList;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
