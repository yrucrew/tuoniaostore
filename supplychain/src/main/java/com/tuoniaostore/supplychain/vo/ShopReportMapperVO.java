package com.tuoniaostore.supplychain.vo;

import java.io.Serializable;

/**
 * @author oy
 * @description 门店报表查询返回VO
 * @date 2019/6/5
 */
public class ShopReportMapperVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String date;
    private Long num;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }
}
