package com.tuoniaostore.supplychain.vo;

import com.tuoniaostore.datamodel.good.GoodBarcode;
import io.swagger.models.auth.In;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 商品返回实体
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/28
 */
public class SupplychainGoodVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;  //id *
    private String templateId;  //模板id
    private String defineImage; //图片*
    private String barcode;//条形码*
    private String typeName;//类别*
    private String typeId;  //类别id
    private String typeImage; //品类Image
    private String typeRemark; //品类备注
    private String brandId; //品牌id
    private String brandLogo; //品牌logo
    private String brandName; //品牌名称

    private String defineName;  //商品名称*
    private String goodName;//商品名称

    private String priceTitle;  //商品名称*
    private String title;   //规格*
    private String priceId;   //规格*
    private String pricePic;   //规格图片
    private String unit;    //单位
    private String unitId;  //单位id
    private Long costPrice; //成本价

    private Long siglePrice;   //单价
    private Long discountPrice;   //折扣价
    private Long marketPrice;   //市场价

    private Integer defaultSort;//排序

    //建议售价
    private Long salePrice;
    //同行价
    private Long colleaguePrice;
    //会员价
    private Long memberPrice;
    //交易价
    private Long sellPrice;

    private Integer stock;  //库存*
    private String described; //描述
    private Integer status; //商品状态 -1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效

    private String typeNameOne;//一级分类
    private String typeNameOneId;//一级分类id
    private String typeNameTwo;//二级分类
    private String typeNameTwoId;//二级分类id

    private String typeIconOne;//一级分类icon
    private String typeImageOne;//一级分类image
    private String typeIconTwo;//二级分类icon
    private String typeImageTwo;//二级分类image

    private Integer shelfLife;//保质期

    private Date createTime;//创建时间

    private int userful;//0可用 1不可用

    private Integer carNum;

    //起订量
    private Integer startingQuantity;

    //是否已选
    private boolean checkFlag;


    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public boolean getCheckFlag() {
        return checkFlag;
    }

    public void setCheckFlag(boolean checkFlag) {
        this.checkFlag = checkFlag;
    }

    public Integer getDefaultSort() {
        return defaultSort;
    }

    public void setDefaultSort(Integer defaultSort) {
        this.defaultSort = defaultSort;
    }

    public Integer getStartingQuantity() {
        return startingQuantity;
    }

    public void setStartingQuantity(Integer startingQuantity) {
        this.startingQuantity = startingQuantity;
    }

    public String getPricePic() {
        return pricePic;
    }

    public void setPricePic(String pricePic) {
        this.pricePic = pricePic;
    }

    public Integer getCarNum() {
        return carNum;
    }

    public void setCarNum(Integer carNum) {
        this.carNum = carNum;
    }

    private List<GoodBarcode> barcodeList;

    public List<GoodBarcode> getBarcodeList() {
        return barcodeList;
    }

    public void setBarcodeList(List<GoodBarcode> barcodeList) {
        this.barcodeList = barcodeList;
    }

    public SupplychainGoodVO() {
    }

    public Integer getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(Integer shelfLife) {
        this.shelfLife = shelfLife;
    }

    public int getUserful() {
        return userful;
    }

    public void setUserful(int userful) {
        this.userful = userful;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Long sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Long getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Long salePrice) {
        this.salePrice = salePrice;
    }

    public Long getColleaguePrice() {
        return colleaguePrice;
    }

    public void setColleaguePrice(Long colleaguePrice) {
        this.colleaguePrice = colleaguePrice;
    }

    public Long getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(Long memberPrice) {
        this.memberPrice = memberPrice;
    }

    public Long getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Long marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getTypeNameOne() {
        return typeNameOne;
    }

    public void setTypeNameOne(String typeNameOne) {
        this.typeNameOne = typeNameOne;
    }

    public String getTypeNameOneId() {
        return typeNameOneId;
    }

    public void setTypeNameOneId(String typeNameOneId) {
        this.typeNameOneId = typeNameOneId;
    }

    public String getTypeNameTwo() {
        return typeNameTwo;
    }

    public void setTypeNameTwo(String typeNameTwo) {
        this.typeNameTwo = typeNameTwo;
    }

    public String getTypeNameTwoId() {
        return typeNameTwoId;
    }

    public void setTypeNameTwoId(String typeNameTwoId) {
        this.typeNameTwoId = typeNameTwoId;
    }

    public String getTypeIconOne() {
        return typeIconOne;
    }

    public void setTypeIconOne(String typeIconOne) {
        this.typeIconOne = typeIconOne;
    }

    public String getTypeImageOne() {
        return typeImageOne;
    }

    public void setTypeImageOne(String typeImageOne) {
        this.typeImageOne = typeImageOne;
    }

    public String getTypeIconTwo() {
        return typeIconTwo;
    }

    public void setTypeIconTwo(String typeIconTwo) {
        this.typeIconTwo = typeIconTwo;
    }

    public String getTypeImageTwo() {
        return typeImageTwo;
    }

    public void setTypeImageTwo(String typeImageTwo) {
        this.typeImageTwo = typeImageTwo;
    }

    public Long getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Long discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getTypeImage() {
        return typeImage;
    }

    public void setTypeImage(String typeImage) {
        this.typeImage = typeImage;
    }

    public String getTypeRemark() {
        return typeRemark;
    }

    public void setTypeRemark(String typeRemark) {
        this.typeRemark = typeRemark;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getPriceTitle() {
        return priceTitle;
    }

    public void setPriceTitle(String priceTitle) {
        this.priceTitle = priceTitle;
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDefineImage() {
        return defineImage;
    }

    public void setDefineImage(String defineImage) {
        this.defineImage = defineImage;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getDefineName() {
        return defineName;
    }

    public void setDefineName(String defineName) {
        this.defineName = defineName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Long costPrice) {
        this.costPrice = costPrice;
    }

    public Long getSiglePrice() {
        return siglePrice;
    }

    public void setSiglePrice(Long siglePrice) {
        this.siglePrice = siglePrice;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getDescribed() {
        return described;
    }

    public void setDescribed(String described) {
        this.described = described;
    }
}
