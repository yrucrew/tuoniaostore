package com.tuoniaostore.supplychain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author oy
 * @description
 * @date 2019/4/9
 */
public class SupplyChainGoodListVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String title;
    private List<SupplychainGoodVO> commodityList = new ArrayList<>();

    public SupplyChainGoodListVO() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SupplychainGoodVO> getCommodityList() {
        return commodityList;
    }

    public void setCommodityList(List<SupplychainGoodVO> commodityList) {
        this.commodityList = commodityList;
    }
}
