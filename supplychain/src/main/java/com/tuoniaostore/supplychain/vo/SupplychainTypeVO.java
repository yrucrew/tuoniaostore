package com.tuoniaostore.supplychain.vo;

import java.io.Serializable;

/**
 * 供应商 商品首页数据
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/28
 */
public class SupplychainTypeVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String title;//标题
    private String secondId;//二级标题
    //商品详情
    private Integer number;//数量

    public SupplychainTypeVO() {
    }

    public String getSecondId() {
        return secondId;
    }

    public void setSecondId(String secondId) {
        this.secondId = secondId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
