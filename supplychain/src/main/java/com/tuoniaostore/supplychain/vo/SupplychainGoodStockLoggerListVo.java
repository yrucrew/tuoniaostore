package com.tuoniaostore.supplychain.vo;

import com.tuoniaostore.datamodel.supplychain.SupplychainGoodStockLogger;

public class SupplychainGoodStockLoggerListVo extends SupplychainGoodStockLogger {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String orderSequenceNumber;
	private String shopName;
	private String goodTypeName;
	private String goodName;
	private String goodBarcode;
	private String normalPrice;
	public String getOrderSequenceNumber() {
		return orderSequenceNumber;
	}
	public void setOrderSequenceNumber(String orderSequenceNumber) {
		this.orderSequenceNumber = orderSequenceNumber;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getGoodTypeName() {
		return goodTypeName;
	}
	public void setGoodTypeName(String goodTypeName) {
		this.goodTypeName = goodTypeName;
	}
	public String getGoodName() {
		return goodName;
	}
	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}
	public String getGoodBarcode() {
		return goodBarcode;
	}
	public void setGoodBarcode(String goodBarcode) {
		this.goodBarcode = goodBarcode;
	}
	public String getNormalPrice() {
		return normalPrice;
	}
	public void setNormalPrice(String normalPrice) {
		this.normalPrice = normalPrice;
	}
}
