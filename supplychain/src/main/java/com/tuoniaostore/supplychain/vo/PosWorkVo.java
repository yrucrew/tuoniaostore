package com.tuoniaostore.supplychain.vo;


import java.io.Serializable;

public class PosWorkVo implements Serializable{
	
		private  String payType;
		private  Integer orderNumber;
		private  Integer refundNumber = 0;
		private  long refundMoneySum;
		private  long receiptMoney;

	public Integer getRefundNumber() {
		return refundNumber;
	}

	public void setRefundNumber(Integer refundNumber) {
		this.refundNumber = refundNumber;
	}

	public long getRefundMoneySum() {
		return refundMoneySum;
	}

	public void setRefundMoneySum(long refundMoneySum) {
		this.refundMoneySum = refundMoneySum;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public long getReceiptMoney() {
		return receiptMoney;
	}

	public void setReceiptMoney(long receiptMoney) {
		this.receiptMoney = receiptMoney;
	}
}
