package com.tuoniaostore.supplychain.vo;

import java.io.Serializable;

/**
 * 供应商端：供应商首页信息
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/27
 */
public class SupplychainShopPropertiesForSupplierVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer status;//商店状态 0表示不营业 1表示正常营业
    private String phoneNumber;//电话号码
    private String address;//地址
    private Long coveringDistance;//覆盖距离 单位：米
    private String bindPrinterCode; //绑定的小票打印机编码

    public SupplychainShopPropertiesForSupplierVO() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCoveringDistance() {
        return coveringDistance;
    }

    public void setCoveringDistance(Long coveringDistance) {
        this.coveringDistance = coveringDistance;
    }

    public String getBindPrinterCode() {
        return bindPrinterCode;
    }

    public void setBindPrinterCode(String bindPrinterCode) {
        this.bindPrinterCode = bindPrinterCode;
    }
}
