package com.tuoniaostore.supplychain.vo;


import java.io.Serializable;

/**
 * @author oy
 * @description
 * @date 2019/5/9
 */
public class SupplychainShopPropertiesForAddVO implements Serializable {

    private static final long serialVersionUID = 1L;

    //id
    private String id;

    //归属者通行证ID
    private String userId;

    //归属渠道
    private String channelId;

    //商店的名字
    private String shopName;

    //当前仓库状态 0表示关仓 1表示开仓 2不对外开放(理论上只有一个--直营主仓)
    private Integer status;

    //仓库类型 如：0-直营仓、1-虚拟仓、2-第三方仓、3-合作仓、4-代理仓,5供应商 6.普通门店 7.直营店,8.批发,9.厂商（供应商小程序：一个user_id 只能有一个 5 8 9）
    private Integer type;

    //联系人号码
    private String phoneNumber;

    //归属的逻辑区域ID
    private String logicAreaId;

    //所在省份
    private String province;
    //所在城市
    private String city;
    //所在区域
    private String district;
    //所在地址经纬度 格式：longitude,latitude
    private String location;
    private String longitude;
    private String latitude;
    //具体地址
    private String address;
    //配送类型 1-我方配送 2-独立配送
    private Integer distributionType;
    //测试
    private Long coveringDistance;
    //配送费用 单位：分
    private Long distributionCost;
    //打印类型 1：下单后 2：支付后 3：接单后 可扩展
    private Integer printerType;
    //绑定的小票打印机编码
    private String bindPrinterCode;



    public SupplychainShopPropertiesForAddVO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLogicAreaId() {
        return logicAreaId;
    }

    public void setLogicAreaId(String logicAreaId) {
        this.logicAreaId = logicAreaId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getDistributionType() {
        return distributionType;
    }

    public void setDistributionType(Integer distributionType) {
        this.distributionType = distributionType;
    }

    public Long getCoveringDistance() {
        return coveringDistance;
    }

    public void setCoveringDistance(Long coveringDistance) {
        this.coveringDistance = coveringDistance;
    }

    public Long getDistributionCost() {
        return distributionCost;
    }

    public void setDistributionCost(Long distributionCost) {
        this.distributionCost = distributionCost;
    }

    public Integer getPrinterType() {
        return printerType;
    }

    public void setPrinterType(Integer printerType) {
        this.printerType = printerType;
    }

    public String getBindPrinterCode() {
        return bindPrinterCode;
    }

    public void setBindPrinterCode(String bindPrinterCode) {
        this.bindPrinterCode = bindPrinterCode;
    }
}
