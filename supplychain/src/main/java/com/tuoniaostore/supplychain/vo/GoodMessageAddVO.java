package com.tuoniaostore.supplychain.vo;

import io.swagger.models.auth.In;

import java.io.Serializable;

/**
 * @author oy
 * @description 商家端：前端传递过来的商品信息（添加商品信息）
 * @date 2019/4/8
 */
public class GoodMessageAddVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String goodPic;
    //商品名字
    private String goodName;
    //商品名字id 如果没有选择 就传空
    private String goodId;
    //商品分类
    private String goodType;
    //商品类别id 如果没有选择 就传空
    private String goodTypeId;
    //商品进价
    private Long goodCostPrice;
    //商品售价
    private Long goodSalePrice;
    //商品描述
    private String goodDescribe;
    //商品条码
    private String goodBarcode;


    //标签名称 比如 怡宝 500ml 这里500ml就是标签名称
    private String goodPriceName;
    //标签名称id  如果没有选择 就传空
    private String priceId;
    //商品单位
    private String goodUnit;
    //商品单位 id 如果没有选择 就传空
    private String goodUnitId;
    //库存
    private Integer stock;
    //上下架状态
    private Integer status;

    public GoodMessageAddVO() {
    }

    public String getGoodPic() {
        return goodPic;
    }

    public void setGoodPic(String goodPic) {
        this.goodPic = goodPic;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getGoodType() {
        return goodType;
    }

    public void setGoodType(String goodType) {
        this.goodType = goodType;
    }

    public String getGoodTypeId() {
        return goodTypeId;
    }

    public void setGoodTypeId(String goodTypeId) {
        this.goodTypeId = goodTypeId;
    }

    public Long getGoodCostPrice() {
        return goodCostPrice;
    }

    public void setGoodCostPrice(Long goodCostPrice) {
        this.goodCostPrice = goodCostPrice;
    }

    public Long getGoodSalePrice() {
        return goodSalePrice;
    }

    public void setGoodSalePrice(Long goodSalePrice) {
        this.goodSalePrice = goodSalePrice;
    }

    public String getGoodDescribe() {
        return goodDescribe;
    }

    public void setGoodDescribe(String goodDescribe) {
        this.goodDescribe = goodDescribe;
    }

    public String getGoodBarcode() {
        return goodBarcode;
    }

    public void setGoodBarcode(String goodBarcode) {
        this.goodBarcode = goodBarcode;
    }

    public String getGoodPriceName() {
        return goodPriceName;
    }

    public void setGoodPriceName(String goodPriceName) {
        this.goodPriceName = goodPriceName;
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getGoodUnit() {
        return goodUnit;
    }

    public void setGoodUnit(String goodUnit) {
        this.goodUnit = goodUnit;
    }

    public String getGoodUnitId() {
        return goodUnitId;
    }

    public void setGoodUnitId(String goodUnitId) {
        this.goodUnitId = goodUnitId;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
