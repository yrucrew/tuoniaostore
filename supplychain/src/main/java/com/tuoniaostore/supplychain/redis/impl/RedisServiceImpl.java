package com.tuoniaostore.supplychain.redis.impl;

import com.tuoniaostore.supplychain.redis.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisServiceImpl implements RedisService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public String getMessageCode(String phoneNum) {
        return stringRedisTemplate.opsForValue().get(phoneNum);
    }

}
