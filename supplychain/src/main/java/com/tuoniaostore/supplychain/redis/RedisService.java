package com.tuoniaostore.supplychain.redis;

public interface RedisService {

    /**
     * 取出对应手机号码的验证码
     * @param phoneNum
     * @return
     */
    String getMessageCode(String phoneNum);

}
