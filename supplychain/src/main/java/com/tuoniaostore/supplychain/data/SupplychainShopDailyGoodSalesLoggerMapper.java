package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainShopDailyGoodSalesLogger;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainShopDailyGoodSalesLoggerMapper {

    String column = "`id`, " +//
            "`shop_id`, " +//仓库ID
            "`good_id`, " +//商品ID
            "`good_template_id`, " +//商品模版ID
            "`good_name`, " +//商品名字
            "`total_in_storage_quantity`, " +//
            "`total_sales_quantity`, " +//总销售数量
            "`total_origin_price`, " +//应销售总价
            "`total_sell_price`, " +//真实销售总价
            "`total_cost_price`, " +//总成本价
            "`happen_date`"//发生的日期
            ;

    @Insert("insert into supplychain_shop_daily_good_sales_logger (" +
            " `id`,  " +
            " `shop_id`,  " +
            " `good_id`,  " +
            " `good_template_id`,  " +
            " `good_name`,  " +
            " `total_in_storage_quantity`,  " +
            " `total_sales_quantity`,  " +
            " `total_origin_price`,  " +
            " `total_sell_price`,  " +
            " `total_cost_price`,  " +
            " `happen_date` " +
            ")values(" +
            "#{id},  " +
            "#{shopId},  " +
            "#{goodId},  " +
            "#{goodTemplateId},  " +
            "#{goodName},  " +
            "#{totalInStorageQuantity},  " +
            "#{totalSalesQuantity},  " +
            "#{totalOriginPrice},  " +
            "#{totalSellPrice},  " +
            "#{totalCostPrice},  " +
            "#{happenDate} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainShopDailyGoodSalesLogger(SupplychainShopDailyGoodSalesLogger supplychainShopDailyGoodSalesLogger);

    @Results(id = "supplychainShopDailyGoodSalesLogger", value = {
            @Result(property = "id", column = "id"), @Result(property = "shopId", column = "shop_id"), @Result(property = "goodId", column = "good_id"), @Result(property = "goodTemplateId", column = "good_template_id"), @Result(property = "goodName", column = "good_name"), @Result(property = "totalInStorageQuantity", column = "total_in_storage_quantity"), @Result(property = "totalSalesQuantity", column = "total_sales_quantity"), @Result(property = "totalOriginPrice", column = "total_origin_price"), @Result(property = "totalSellPrice", column = "total_sell_price"), @Result(property = "totalCostPrice", column = "total_cost_price"), @Result(property = "happenDate", column = "happen_date")})
    @Select("select " + column + " from supplychain_shop_daily_good_sales_logger where id = #{id}")
    SupplychainShopDailyGoodSalesLogger getSupplychainShopDailyGoodSalesLogger(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_shop_daily_good_sales_logger   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainShopDailyGoodSalesLogger")
    List<SupplychainShopDailyGoodSalesLogger> getSupplychainShopDailyGoodSalesLoggers(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_shop_daily_good_sales_logger   where    1=1  and status=0 </script>")
    @ResultMap("supplychainShopDailyGoodSalesLogger")
    List<SupplychainShopDailyGoodSalesLogger> getSupplychainShopDailyGoodSalesLoggerAll();

    @Select("<script>select count(1) from supplychain_shop_daily_good_sales_logger   where     1=1 " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainShopDailyGoodSalesLoggerCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_shop_daily_good_sales_logger  " +
            "set " +
            "`shop_id` = #{shopId}  , " +
            "`good_id` = #{goodId}  , " +
            "`good_template_id` = #{goodTemplateId}  , " +
            "`good_name` = #{goodName}  , " +
            "`total_in_storage_quantity` = #{totalInStorageQuantity}  , " +
            "`total_sales_quantity` = #{totalSalesQuantity}  , " +
            "`total_origin_price` = #{totalOriginPrice}  , " +
            "`total_sell_price` = #{totalSellPrice}  , " +
            "`total_cost_price` = #{totalCostPrice}  , " +
            "`happen_date` = #{happenDate}  " +
            " where id = #{id}")
    void changeSupplychainShopDailyGoodSalesLogger(SupplychainShopDailyGoodSalesLogger supplychainShopDailyGoodSalesLogger);

}
