package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.supplychain.vo.ShopReportMapperVO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import java.util.List;
import java.util.Map;

/**
 * 仓库的属性
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainShopPropertiesMapper {

    String column = "`id`, " +//
            "`user_id`, " +//归属者通行证ID
            "`channel_id`, " +//归属渠道
            "`shop_name`, " +//仓库的名字
            "`status`, " +//当前仓库状态 0表示关仓 1表示开仓 2不对外开放(理论上只有一个--直营主仓)
            "`type`, " +//仓库类型 如：0-直营仓、1-虚拟仓、2-第三方仓、3-合作仓、4-代理仓等
            "`owner_id`, " +//归属ID
            "`admin_name`, " +//管理员姓名
            "`phone_number`, " +//联系人号码
            "`logic_area_id`, " +//归属的逻辑区域ID
            "`province`, " +//所在省份
            "`city`, " +//所在城市
            "`district`, " +//所在区域
            "`street`, " +//所在区域
            "`address`, " +//具体地址
            "`location`, " +//所在地址经纬度 格式：longitude,latitude
            "`longitude`, " +//所在地址经纬度
            "`latitude`, " +//所在地址经纬度
            "`parent_id`, " +//父级仓库ID 一级仓时为0
            "`sync_parent_stock`, " +//是否同步父级仓库存 0-独立库存 1-本仓+父级仓库存 2-仅读取父级库存
            "`distribution_type`, " +//配送类型 1-我方配送 2-独立配送
            "`covering_distance`, " +//覆盖距离 单位：米
            "`min_price`, " +//起订金额
            "`distribution_cost`, " +//配送费用 单位：分
            "`printer_type`, " +//打印类型 1：下单后 2：支付后 3：接单后 可扩展
            "`bind_printer_code`, " +//绑定的小票打印机编码
            "`create_time`, " +//建立时间
            "`moq`, " +//起订值
            "`retrieve_status`, " +//回收站
            "`business_license_number`, " +//营业编号
            "`sort`, " +//排序
            "auth"//审核
            ;

    @Insert("insert into supplychain_shop_properties (" +
            " `id`,  " +
            " `user_id`,  " +
            " `channel_id`,  " +
            " `shop_name`,  " +
            " `status`,  " +
            " `type`,  " +
            " `owner_id`,  " +
            " `admin_name`,  " +
            " `phone_number`,  " +
            " `logic_area_id`,  " +
            " `province`,  " +
            " `city`,  " +
            " `district`,  " +
            " `street`,  " +
            " `address`,  " +
            " `location`,  " +//longitude,latitude
            " `longitude`,  " +
            " `latitude`,  " +
            " `parent_id`,  " +
            " `sync_parent_stock`,  " +
            " `distribution_type`,  " +
            " `covering_distance`,  " +
            " `min_price`,  " +
            " `distribution_cost`,  " +
            " `printer_type`,  " +
            " `moq`,  " +
            " `business_license_number`,  " +
            " `sort`,  " +
            " `bind_printer_code`  " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{channelId},  " +
            "#{shopName},  " +
            "#{status},  " +
            "#{type},  " +
            "#{ownerId},  " +
            "#{adminName},  " +
            "#{phoneNumber},  " +
            "#{logicAreaId},  " +
            "#{province},  " +
            "#{city},  " +
            "#{district},  " +
            "#{street},  " +
            "#{address},  " +
            "#{location},  " +
            "#{longitude},  " +
            "#{latitude},  " +
            "#{parentId},  " +
            "#{syncParentStock},  " +
            "#{distributionType},  " +
            "#{coveringDistance},  " +
            "#{minPrice},  " +
            "#{distributionCost},  " +
            "#{printerType},  " +
            "#{moq},  " +
            "#{businessLicenseNumber},  " +
            "#{sort},  " +
            "#{bindPrinterCode}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainShopProperties(SupplychainShopProperties supplychainShopProperties);

    @Results(id = "supplychainShopProperties", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "channelId", column = "channel_id"), @Result(property = "shopName", column = "shop_name"), @Result(property = "status", column = "status"), @Result(property = "type", column = "type"), @Result(property = "ownerId", column = "owner_id"), @Result(property = "adminName", column = "admin_name"), @Result(property = "phoneNumber", column = "phone_number"), @Result(property = "logicAreaId", column = "logic_area_id"), @Result(property = "province", column = "province"), @Result(property = "city", column = "city"), @Result(property = "district", column = "district"), @Result(property = "address", column = "address"), @Result(property = "location", column = "location"), @Result(property = "parentId", column = "parent_id"), @Result(property = "syncParentStock", column = "sync_parent_stock"), @Result(property = "distributionType", column = "distribution_type"), @Result(property = "coveringDistance", column = "covering_distance"), @Result(property = "distributionCost", column = "distribution_cost"), @Result(property = "printerType", column = "printer_type"), @Result(property = "bindPrinterCode", column = "bind_printer_code"), @Result(property = "createTime", column = "create_time"),
            @Result(property = "auth", column = "auth"), @Result(property = "retrieveStatus", column = "retrieve_status"), @Result(property = "businessLicenseNumber", column = "business_license_number")})
    @Select("select " + column + " from supplychain_shop_properties where id = #{id} and retrieve_status = 0")
    SupplychainShopProperties getSupplychainShopProperties(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_shop_properties   where    1=1 and retrieve_status = 0 " +
            " <when test=\"status != null and status != -1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and shop_name like concat('%', #{name}, '%') " +
            " </when>" +
            " <when test=\"auth != -1\">" +
            "  and auth=#{auth} " +
            " </when>" +
            " and type in " +
            "<foreach collection='types' item='type' index='index' open='(' separator=',' close=')'>" +
            "#{type}" +
            "</foreach>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainShopProperties")
    List<SupplychainShopProperties> getSupplychainShopPropertiess(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name, @Param("types") Integer[] types, @Param("auth") int auth);

    @Select("<script>select  " + column + "  from supplychain_shop_properties   where   1 = 1  " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and (shop_name like concat('%', #{name}, '%') or phone_number like concat('%', #{name}, '%')) " +
            " </when>" +
            " and type in " +
            "<foreach collection='types' item='type' index='index' open='(' separator=',' close=')'>" +
            "#{type}" +
            "</foreach>" +
            " <when test=\"status != null and status != -1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"map.areaId != null and map.areaId !='' \">" +
            "  and logic_area_id=#{map.areaId} " +
            " </when>" +
            " <when test=\"map.province != null and map.province !='' \">" +
            "  and province=#{map.province} " +
            " </when>" +
            " <when test=\"map.city != null and map.city !='' \">" +
            "  and city=#{map.city} " +
            " </when>" +
            " <when test=\"map.district != null and map.district !='' \">" +
            "  and district=#{map.district} " +
            " </when>" +
            " <when test=\"auth != -1\">" +
            "  and auth = #{auth} " +
            " </when>" +
            "<when test=\"map.startTime != null and  map.endTime != null\">" + //date_format(create_time,'%Y-%m-%d')
            "and date_format(create_time,'%Y-%m-%d') BETWEEN #{map.startTime} AND #{map.endTime}  " +
            " </when>" +
            " and retrieve_status = 0 " +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainShopProperties")
    List<SupplychainShopProperties> getSupplychainShopPropertiess2(@Param("status") int status, @Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("name") String name, @Param("types") Integer[] types, @Param("auth") int auth, @Param("map") Map<String, String> map);


    @Select("<script>select  " + column + "  from supplychain_shop_properties   where  1=1 and retrieve_status = 0 " +
            " <when test=\"status != null and status != -1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"types != null\">" +
            " and type in " +
            "<foreach collection='types' item='type' index='index' open='(' separator=',' close=')'>" +
            "#{type}" +
            "</foreach>" +
            " </when>" +
            "   </script>")
    @ResultMap("supplychainShopProperties")
    List<SupplychainShopProperties> getSupplychainShopPropertiesAll(@Param("status") int status, @Param("types") Integer[] types);


    @Select("<script>select  count(*) from supplychain_shop_properties   where   1 = 1  " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and shop_name like concat('%', #{name}, '%') or phone_number like concat('%', #{name}, '%') " +
            " </when>" +
            " and type in " +
            "<foreach collection='types' item='type' index='index' open='(' separator=',' close=')'>" +
            "#{type}" +
            "</foreach>" +
            " <when test=\"status != null and status != -1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"map.areaId != null and map.areaId !='' \">" +
            "  and logic_area_id=#{map.areaId} " +
            " </when>" +
            " <when test=\"map.province != null and map.province !='' \">" +
            "  and province=#{map.province} " +
            " </when>" +
            " <when test=\"map.city != null and map.city !='' \">" +
            "  and city=#{map.city} " +
            " </when>" +
            " <when test=\"map.district != null and map.district !='' \">" +
            "  and district=#{map.district} " +
            " </when>" +
            " <when test=\"auth != -1\">" +
            "  and auth = #{auth} " +
            " </when>" +
            "<when test=\"map.startTime != null and  map.endTime != null\">" + //date_format(create_time,'%Y-%m-%d')
            "and date_format(create_time,'%Y-%m-%d') BETWEEN #{map.startTime} AND #{map.endTime}  " +
            " </when>" +
            " and retrieve_status = 0 " +
            "  order by sort desc   " +
            " </script>")
    int getSupplychainShopPropertiesCount(@Param("status") int status, @Param("name") String name, @Param("types") Integer[] types, @Param("auth") int auth, @Param("map") Map<String, String> map);

    @Update("<script>update supplychain_shop_properties  " +
            "<set> " +
            "<when test=\"userId != null and userId.trim() != ''\">user_id = #{userId} , </when>" +
            "<when test=\"channelId != null and channelId.trim() != ''\">channel_id = #{channelId} , </when>" +
            "<when test=\"shopName != null and shopName.trim() != ''\">shop_name = #{shopName} , </when>" +
            "<when test=\"status != null \">status = #{status} , </when>" +
            "<when test=\"type != null\">type = #{type} , </when>" +
            "<when test=\"ownerId != null and ownerId.trim() != ''\">owner_id = #{ownerId} , </when>" +
            "<when test=\"adminName != null and adminName.trim() != ''\">admin_name = #{adminName} , </when>" +
            "<when test=\"phoneNumber != null and phoneNumber.trim() != ''\">phone_number = #{phoneNumber} , </when>" +
            "<when test=\"logicAreaId != null and logicAreaId.trim() != ''\">logic_area_id = #{logicAreaId} , </when>" +
            "<when test=\"province != null and province.trim() != ''\">province = #{province} , </when>" +
            "<when test=\"city != null and city.trim() != ''\">city = #{city} , </when>" +
            "<when test=\"district != null and district.trim() != ''\">district = #{district} , </when>" +
            "<when test=\"street != null and street.trim() != ''\">street = #{street} , </when>" +
            "<when test=\"address != null and address.trim() != ''\">address = #{address} , </when>" +
            "<when test=\"location != null and location.trim() != ''\">location = #{location} , </when>" +
            "<when test=\"parentId != null and parentId.trim() != ''\">parent_id = #{parentId} , </when>" +
            "<when test=\"syncParentStock != null\">sync_parent_stock = #{syncParentStock} , </when>" +
            "<when test=\"distributionType != null\">distribution_type = #{distributionType} , </when>" +
            "<when test=\"distributionCost != null\">distribution_cost = #{distributionCost} , </when>" +
            "<when test=\"minPrice != null\">min_price = #{minPrice} , </when>" +
            "<when test=\"coveringDistance != null\">covering_distance = #{coveringDistance} , </when>" +
            "<when test=\"printerType != null\">printer_type = #{printerType} , </when>" +
            "<when test=\"sort != null\">sort = #{sort} , </when>" +
            "<when test=\"bindPrinterCode != null\">bind_printer_code = #{bindPrinterCode} , </when>" +
            "<when test=\"createTime != null\">create_time = #{createTime} , </when>" +
            "<when test=\"moq != null\">moq = #{moq} , </when>" +
            "</set> where id = #{id}</script>")
    void changeSupplychainShopProperties(SupplychainShopProperties supplychainShopProperties);

    @ResultMap("supplychainShopProperties")
    @Select("select " + column + " from supplychain_shop_properties where user_id = #{userId}  and retrieve_status = 0 limit 1")
    SupplychainShopProperties getSupplychainShopPropertiesByUserId(@Param("userId") String userId);

    @Update("update supplychain_shop_properties  " +
            "set " +
            "`shop_name` = #{name}" +
            " where user_id = #{userId}")
    void changeSupplychainShopPropertiesById(@Param("userId") String userId, @Param("name") String name);

    @ResultMap("supplychainShopProperties")
    @Select("select " + column + " from supplychain_shop_properties where user_id = #{userId} and type = #{type} and retrieve_status = 0")
    SupplychainShopProperties selectSupplychainShopPropertiesById(@Param("userId") String userId, @Param("type") int type);


    @Update("<script> update supplychain_shop_properties set status = #{status} where user_id = #{userId}" +
            "<if test=\"lists != null\">and type in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if></script>")
    void selectSupplychainShopPropertiesByIdAndTypes(@Param("userId") String userId, @Param("lists") List<Integer> types, @Param("status") int status);

    @ResultMap("supplychainShopProperties")
    @Select("<script> select " + column + " from supplychain_shop_properties where user_id = #{userId}  and retrieve_status = 0" +
            "<if test=\"lists != null\">and type in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>  limit 1 " +
            "</script>")
    SupplychainShopProperties getSupplychainShopPropertiesByUserIdAndTypeId(@Param("userId") String userId, @Param("lists") List<Integer> list);

    @Update("<script> update supplychain_shop_properties set covering_distance = #{coveringDistance} where user_id = #{userId}  and retrieve_status = 0" +
            "<if test=\"lists != null\">and type in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if></script>")
    void changeSupplychainShopPropertiesCoveringDistanceByIdAndTypes(@Param("userId") String userId, @Param("lists") List<Integer> list, @Param("coveringDistance") int coveringDistance);

    @Update("<script> update supplychain_shop_properties set bind_printer_code = #{bindPrinterCode} where user_id = #{userId} " +
            "<if test=\"lists != null\">and type in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if></script>")
    void changeSupplychainShopPropertiesBindPrinterCodeByIdAndTypes(@Param("userId") String userId, @Param("lists") List<Integer> list, @Param("bindPrinterCode") String bindPrinterCode);

    @Update("<script> update supplychain_shop_properties set phone_number = #{phoneNumber} where user_id = #{userId} " +
            "<if test=\"lists != null\">and type in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if></script>")
    void changeSupplychainShopPropertiesPhoneNumber(@Param("userId") String userId, @Param("lists") List<Integer> list, @Param("phoneNumber") String phoneNumber);

    @ResultMap("supplychainShopProperties")
    @Select("select " + column + " from supplychain_shop_properties")
    List<SupplychainShopProperties> supplychainShopPropertiesAll();


    @Select("<script>select " + column + " from supplychain_shop_properties where 1 = 1  and retrieve_status = 0 " +
            " <if  test=\"types!= null and types.size > 0 \" > and type in <foreach collection=\"types\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>" +
            " <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when> " +
            "</script>")
    @ResultMap("supplychainShopProperties")
    List<SupplychainShopProperties> getSupplychainShopPropertiesByType(@Param("types") List<String> types, @Param("pageStartIndex") Integer pageStartIndex, @Param("pageSize") Integer pageSize);

    @Select("<script>select count(*) from supplychain_shop_properties where  retrieve_status = 0 " +
            " <if  test=\"types!= null and types.size > 0 \" > and type in <foreach collection=\"types\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if></script>")
    int getSupplychainShopPropertiesByTypeCount(@Param("types") List<String> list);


    @Select("select " + column + " from supplychain_shop_properties where id = #{id} and retrieve_status = 0 ")
    @ResultMap("supplychainShopProperties")
    SupplychainShopProperties getSupplychainShopPropertiesById(@Param("id") String id);

    @Update("update supplychain_shop_properties set auth = #{auth} where id = #{id} ")
    int authSupplychainShopProperties(@Param("id") String id, @Param("auth") int auth);

    @Select({
            "<script>",
            "select * from supplychain_shop_properties where retrieve_status = 0 and id in ",
            "<foreach collection=\"shopIds\" item=\"shopId\" open=\"(\" close=\")\" separator=\",\">#{shopId}</foreach>",
            "</script>"
    })
    List<SupplychainShopProperties> getSupplychainShopPropertiesByIds(@Param("shopIds") List<String> shopIds);

    @Select({
            "<script>",
            "select count(1) from supplychain_shop_properties where retrieve_status = 0 and id in ",
            "<foreach collection=\"shopIds\" item=\"shopId\" open=\"(\" close=\")\" separator=\",\">#{shopId}</foreach>",
            "</script>"
    })
    int getSupplychainShopPropertiesByIdsCount(@Param("shopIds") List<String> shopIds);

    @Select({
            "<script>",
            "select * from supplychain_shop_properties where retrieve_status = 0 and id in ",
            "<foreach collection=\"shopIds\" item=\"shopId\" open=\"(\" close=\")\" separator=\",\">#{shopId}</foreach>",
            " <when test=\"pageStartIndex != null and pageSize != null\"> ",
            "  limit #{pageStartIndex}, #{pageSize} ",
            "</when> ",
            "</script>"
    })
    List<SupplychainShopProperties> getSupplychainShopPropertiesByIdsPage(List<String> shopIds, @Param("pageStartIndex") Integer pageStartIndex, @Param("pageSize") Integer pageSize);

    @Select("<script>select " + column + " from supplychain_shop_properties where 1 = 1 and retrieve_status = 0  " +
            "<if test=\"shopName != null\">and shop_name like concat('%', #{shopName}, '%')</if> " +
            "<if test=\"types != null\">and type in <foreach collection=\"types\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            " <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when> " +
            "</script>")
    @ResultMap("supplychainShopProperties")
    List<SupplychainShopProperties> getSupplychainShopPropertiesByShopName(@Param("shopName") String shopName, @Param("types") List<Integer> types, @Param("pageStartIndex") Integer pageStartIndex, @Param("pageSize") Integer pageSize);

    @Select("<script>select count(*) from supplychain_shop_properties where 1 = 1 and retrieve_status = 0  " +
            "<if test=\"shopName != null\">and shop_name like concat('%', #{shopName}, '%')</if> " +
            "<if test=\"types != null\">and type in <foreach collection=\"types\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            "</script>")
    int getSupplychainShopPropertiesByShopNameCount(@Param("shopName") String shopName, @Param("types") List<Integer> types);

    @Update("<script> update supplychain_shop_properties set retrieve_status = 1 where id in <foreach collection=\"ids\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></script>")
    void deleteSupplychainShopProperties(@Param("ids") List<String> id);

    @Select("<script>select " + column + " from supplychain_shop_properties where 1 = 1 and retrieve_status = 0  " +
            "<if test=\"shopName != null\">and shop_name like concat('%', #{shopName}, '%')</if> " +
            "<if test=\"typeIds != null and typeIds.size > 0 \">and type in <foreach collection=\"typeIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            "<if test=\"userIds != null and userIds.size > 0\">and user_id in <foreach collection=\"userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            " <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when> " +
            "</script>")
    @ResultMap("supplychainShopProperties")
    List<SupplychainShopProperties> getSupplychainShopPropertiesByShopNameAndUserId(@Param("shopName") String shopName, @Param("userIds") List<String> userId, @Param("typeIds") List<Integer> typeIds, @Param("pageStartIndex") Integer pageStartIndex, @Param("pageSize") Integer pageSize);

    @Select("<script>select count(*) from supplychain_shop_properties where 1 = 1 and retrieve_status = 0  " +
            "<if test=\"shopName != null \">and shop_name like concat('%', #{shopName}, '%')</if> " +
            "<if test=\"typeIds != null and typeIds.size > 0 \">and type in <foreach collection=\"typeIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            "<if test=\"userIds != null and userIds.size > 0 \">and user_id in <foreach collection=\"userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            "</script>")
    int getCountSupplychainShopPropertiesByShopNameAndUserId(@Param("shopName") String shopName, @Param("typeIds") List<Integer> typeIds, @Param("userIds") List<String> userId);

    @Select("<script>select " + column + " from supplychain_shop_properties where retrieve_status = 0  and type in (0,2,3)" +
            " <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when> " +
            " </script>")
    List<SupplychainShopProperties> getSupplychainShopPropertiesForBindingSelect(@Param("pageStartIndex") Integer pageStartIndex, @Param("pageSize") Integer pageSize);

    @Update("<script> update supplychain_shop_properties " +
            "<set>" +
            "<if test=\"map.parentId != null and map.parentId != ''\">parent_id = #{map.parentId},</if>" +
            "</set> where id = #{map.id} </script>")
    void updateSupplychainShopPropertiesByParam(@Param("map") Map<String, Object> map);

    @Update("<script> update supplychain_shop_properties " +
            "<set>" +
            "<if test=\"bindPrinterCode != null and bindPrinterCode != ''\">bind_printer_code = #{bindPrinterCode},</if>" +
            "</set> where user_id = #{userId}  </script>")
    SupplychainShopProperties UpdateBindPrinterCodeByUserId(@Param("userId") String userId,@Param("bindPrinterCode") String bindPrinterCode);

    String bindColumn = "" +
            "  s.`id`, " +
            "  s.`user_id`, " +
            "  s.`channel_id`, " +
            "  s.`shop_name`, " +
            "  s.`status`, " +
            "  s.`type`, " +
            "  s.`owner_id`, " +
            "  s.`admin_name`, " +
            "  s.`phone_number`, " +
            "  s.`logic_area_id`, " +
            "  s.`province`, " +
            "  s.`city`, " +
            "  s.`district`, " +
            "  s.`street`, " +
            "  s.`address`, " +
            "  s.`location`, " +
            "  s.`longitude`, " +
            "  s.`latitude`, " +
            "  s.`parent_id`, " +
            "  s.`sync_parent_stock`, " +
            "  s.`distribution_type`, " +
            "  s.`covering_distance`, " +
            "  s.`min_price`, " +
            "  s.`distribution_cost`, " +
            "  s.`printer_type`, " +
            "  s.`bind_printer_code`, " +
            "  s.`create_time`, " +
            "  s.`moq`, " +
            "  s.`retrieve_status`, " +
            "  s.`business_license_number`, " +
            "  s.`sort`, " +
            "  s.auth, " +
            "  r.bind_shop_id ";

    @Select("<script>" +
            "SELECT " +
            bindColumn +
            "FROM " +
            " supplychain_shop_properties AS s " +
            "LEFT JOIN supplychain_warehourse_bind_relationship AS r ON s.id = r.bind_shop_id " +
            "WHERE " +
            " 1 = 1 " +
            "<if test=\"shopName != null \">and shop_name like concat('%', #{shopName}, '%')</if> " +
            "AND s.retrieve_status = 0 " +
            "<if test=\"types != null\">and s.type in <foreach collection=\"types\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            "AND r.bind_shop_id IS NULL " +
            "UNION " +
            " SELECT " +
            bindColumn +
            " FROM " +
            "  supplychain_shop_properties AS s " +
            " LEFT JOIN supplychain_warehourse_bind_relationship AS r ON s.id = r.bind_shop_id " +
            " WHERE " +
            "  1 = 1 " +
            "<if test=\"shopName != null \">and shop_name like concat('%', #{shopName}, '%')</if> " +
            " AND s.retrieve_status = 0 " +
            "<if test=\"types != null\">and s.type in <foreach collection=\"types\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            " AND r.bind_shop_id IS NOT NULL " +
            "AND r.shop_id = #{shopId}" +
            "<when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when> " +
            "</script>")
    @ResultMap("supplychainShopProperties")
    List<SupplychainShopProperties> getOwnAndUnbindShopProperties(@Param("shopName") String shopName, @Param("shopId") String shopId, @Param("types") List<Integer> types, @Param("pageStartIndex") int pageStartIndex, @Param("pageSize") int pageSize);

    @Select("<script>SELECT COUNT(1) FROM " +
            "(select s.id " +
            " " +
            "FROM " +
            " supplychain_shop_properties s " +
            "WHERE " +
            " 1 = 1  " +
            " AND retrieve_status = 0 " +
            " and s.type in " +
            "<foreach collection='types' item='type' index='index' open='(' separator=',' close=')'>" +
            "#{type}" +
            "</foreach>" +
            " AND s.id NOT IN ( SELECT bind_shop_id FROM supplychain_warehourse_bind_relationship ) union ALL " +
            "SELECT " +
            " s.id " +
            " " +
            "FROM " +
            " supplychain_shop_properties s " +
            "WHERE " +
            " 1 = 1  " +
            " AND retrieve_status = 0  " +
            " and s.type in " +
            "<foreach collection='types' item='type' index='index' open='(' separator=',' close=')'>" +
            "#{type}" +
            "</foreach>" +
            " AND s.id IN ( SELECT bind_shop_id FROM supplychain_warehourse_bind_relationship WHERE shop_id = #{shopId} ) ) as total</script>")
    int getOwnAndUnbindShopPropertiesCount(@Param("shopId") String shopId, @Param("types") List<Integer> types);

    @Select("<script> " +
            "select DATE_FORMAT(t1.date, '%Y-%m-%d') as date,COALESCE(t2.num,0) as num from calendar t1 LEFT JOIN ( " +
            "SELECT DATE_FORMAT(create_time,'%Y-%m-%d') as 'date' ,count(1) as num  " +
            "FROM `supplychain_shop_properties` where 1 = 1 <if test=\"types != null and types.size() > 0\"> and type in " +
            "<foreach collection='types' item='type' index='index' open='(' separator=',' close=')'> " +
            "#{type} " +
            "</foreach></if> AND DATE_FORMAT(create_time, '%Y-%m-%d') BETWEEN #{startTime} and #{endTime} " +
            "GROUP BY DATE_FORMAT(create_time,'%Y-%m-%d')  " +
            ")  t2 on DATE_FORMAT(t1.date, '%Y-%m-%d') = t2.date where t1.date BETWEEN #{startTime} and #{endTime} GROUP BY date ORDER BY date ASC  " +
            "</script>")
    @Results(id = "shopReportMapperVO", value = {
            @Result(property = "date", column = "date"), @Result(property = "num", column = "num")})
    List<ShopReportMapperVO> getSupplychainShopPropertiesByTypeCountForEveryDay(@Param("types") List<String> list, @Param("startTime") String startTime, @Param("endTime")String endTime);

    @Select("<script>" +
            "select DATE_FORMAT(t1.date, '%x-%v') as date,COALESCE(t2.num,0) as num from calendar t1 LEFT JOIN ( " +
            "SELECT DATE_FORMAT(create_time, '%x-%v') as 'date', count(1) AS num  " +
            "FROM `supplychain_shop_properties` where 1 = 1 <if test=\"types != null and types.size() > 0\"> and type in " +
            "<foreach collection='types' item='type' index='index' open='(' separator=',' close=')'> " +
            "#{type} " +
            "</foreach></if> AND DATE_FORMAT(create_time, '%Y-%m-%d') BETWEEN #{startTime} and #{endTime} " +
            "GROUP BY DATE_FORMAT(create_time, '%x-%v')  " +
            ")  t2 on DATE_FORMAT(t1.date, '%x-%v') = t2.date where t1.date BETWEEN #{startTime} and #{endTime} GROUP BY date ORDER BY date ASC " +
            "</script>")
    @ResultMap("shopReportMapperVO")
    List<ShopReportMapperVO> getSupplychainShopPropertiesByTypeCountForEveryWeek(@Param("types")List<String> list,@Param("startTime") String startTime, @Param("endTime")String endTime);

    @Select("<script> " +
            "select DATE_FORMAT(t1.date, '%x-%m') as date,COALESCE(t2.num,0) as num from calendar t1 LEFT JOIN ( " +
            "SELECT DATE_FORMAT(create_time, '%x-%m') as 'date', count(1) AS num  " +
            "FROM `supplychain_shop_properties` where 1 = 1 <if test=\"types != null and types.size() > 0\"> and type in " +
            "<foreach collection='types' item='type' index='index' open='(' separator=',' close=')'> " +
            "#{type} " +
            "</foreach></if> AND DATE_FORMAT(create_time, '%x-%m') BETWEEN #{startTime} and #{endTime} " +
            "GROUP BY DATE_FORMAT(create_time, '%x-%m') " +
            ") t2 on DATE_FORMAT(t1.date, '%x-%m') = t2.date where DATE_FORMAT(t1.date, '%x-%m') BETWEEN #{startTime} and #{endTime} GROUP BY date ORDER BY date ASC " +
            "</script>")
    @ResultMap("shopReportMapperVO")
    List<ShopReportMapperVO> getSupplychainShopPropertiesByTypeCountForEveryMonth(@Param("types")List<String> list,@Param("startTime") String startTime, @Param("endTime")String endTime);

}
