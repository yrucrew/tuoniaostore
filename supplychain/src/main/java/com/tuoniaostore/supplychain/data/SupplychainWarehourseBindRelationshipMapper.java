package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseBindRelationship;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import java.util.List;

@Component
@Mapper
public interface SupplychainWarehourseBindRelationshipMapper {

    @Select("<script> select * from supplychain_warehourse_bind_relationship order by create_time" +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    List<SupplychainWarehourseBindRelationship> getSupplychainWarehourseBindRelationships(@Param("pageIndex") Integer pageIndex,@Param("pageSize")Integer pageSize);

    @Select("<script> select * from supplychain_warehourse_bind_relationship where shop_id = #{shopId} and type = #{type}" +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    List<SupplychainWarehourseBindRelationship> getSupplychainWarehourseBindRelationshipsByShopIdsAndType(@Param("shopId") String shopId,@Param("type") int type, @Param("pageIndex") Integer pageStartIndex, @Param("pageSize") Integer pageSize);

    @Update("<script> delete from supplychain_warehourse_bind_relationship where shop_id = #{shopId} and type = #{type}</script>")
    void deleteSupplychainWarehourseBindRelationship(@Param("shopId")String shopId,@Param("type") int type);

    @Insert("<script> insert into supplychain_warehourse_bind_relationship " +
            " (`id`,  " +
            " `shop_id`,  " +
            " `bind_shop_id`,  " +
            " `type`,  " +
            " `user_id`  " +
            ") values " +
            "<foreach collection=\"lists\" item=\"item\" separator=\",\"> " +
            "(" +
            "#{item.id},  " +
            "#{item.shopId},  " +
            "#{item.bindShopId},  " +
            "#{item.type},  " +
            "#{item.userId}  " +
            ")</foreach></script>")
    void batchAddSupplychainWarehourseBindRelationships(@Param("lists") List<SupplychainWarehourseBindRelationship> entity);

    @Select("<script> select * from  supplychain_warehourse_bind_relationship where bind_shop_id = #{bindShopId} and type = #{type} limit 1</script>")
    SupplychainWarehourseBindRelationship getSupplychainWarehourseBindRelationshipsByBindShopId(@Param("bindShopId") String bindShopId,@Param("type") Integer type);

    @Select("<script> select * from  supplychain_warehourse_bind_relationship where shop_id = #{shopId} and type = #{type} limit 1</script>")
    SupplychainWarehourseBindRelationship getSupplychainWarehourseBindRelationshipsByShopId(@Param("shopId") String shopId,@Param("type") Integer type);
}
