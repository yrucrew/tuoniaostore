package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainPartnerShopFocusGood;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainPartnerShopFocusGoodMapper {

    String column = "`id`, " +//
            "`partner_id`, " +//合作方ID
            "`shop_id`, " +//我方实体ID
            "`focus_type`, " +//关注的类型 如：0-商品 1-商品类型
            "`focus_entry_id`, " +//关注的实体数据ID 当focus_type为0时 表示商品ID 当focus_type为1时 表示商品模版ID
            "`statuc`, " +//状态 0-无效 1-有效
            "`create_time`"//建立时间
            ;

    @Insert("insert into supplychain_partner_shop_focus_good (" +
            " `id`,  " +
            " `partner_id`,  " +
            " `shop_id`,  " +
            " `focus_type`,  " +
            " `focus_entry_id`,  " +
            " `statuc`,  " +
            ")values(" +
            "#{id},  " +
            "#{partnerId},  " +
            "#{shopId},  " +
            "#{focusType},  " +
            "#{focusEntryId},  " +
            "#{statuc},  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainPartnerShopFocusGood(SupplychainPartnerShopFocusGood supplychainPartnerShopFocusGood);

    @Results(id = "supplychainPartnerShopFocusGood", value = {
            @Result(property = "id", column = "id"), @Result(property = "partnerId", column = "partner_id"), @Result(property = "shopId", column = "shop_id"), @Result(property = "focusType", column = "focus_type"), @Result(property = "focusEntryId", column = "focus_entry_id"), @Result(property = "statuc", column = "statuc"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from supplychain_partner_shop_focus_good where id = #{id}")
    SupplychainPartnerShopFocusGood getSupplychainPartnerShopFocusGood(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_partner_shop_focus_good   where    1=1 " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainPartnerShopFocusGood")
    List<SupplychainPartnerShopFocusGood> getSupplychainPartnerShopFocusGoods(@Param("status") int status, @Param("pageIndex") int pageIndex,
                                                                              @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_partner_shop_focus_good   where  1=1 and status=0 </script>")
    @ResultMap("supplychainPartnerShopFocusGood")
    List<SupplychainPartnerShopFocusGood> getSupplychainPartnerShopFocusGoodAll();

    @Select("<script>select count(1) from supplychain_partner_shop_focus_good   where  1=1 " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainPartnerShopFocusGoodCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_partner_shop_focus_good  " +
            "set " +
            "`partner_id` = #{partnerId}  , " +
            "`shop_id` = #{shopId}  , " +
            "`focus_type` = #{focusType}  , " +
            "`focus_entry_id` = #{focusEntryId}  , " +
            "`statuc` = #{statuc}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeSupplychainPartnerShopFocusGood(SupplychainPartnerShopFocusGood supplychainPartnerShopFocusGood);

}
