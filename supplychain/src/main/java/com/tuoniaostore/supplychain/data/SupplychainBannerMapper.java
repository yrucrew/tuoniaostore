package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainBanner;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainBannerMapper {

    String column = "`id`, " +//
            "`adcode`, " +//城市编码
            "`define_area_id`, " +//自定义区域ID
            "`shop_id`, " +//仓库ID
            "`type`, " +//banner类型，类型：分类连接，商品详情，跳转图片，专题活动等等
            "`sort`, " +//排序 越小越前
            "`image_url`, " +//图片链接、用于展示
            "`click_url`, " +//点击的跳转链接，与type结合
            "`status`, " +//当前状态，1、开启中 0、关闭中
            "`open_time`, " +//开启时间
            "`close_time`, " +//关闭时间
            "`create_time`"//建立时间
            ;

    @Insert("insert into supplychain_banner (" +
            " `id`,  " +
            " `adcode`,  " +
            " `define_area_id`,  " +
            " `shop_id`,  " +
            " `type`,  " +
            " `sort`,  " +
            " `image_url`,  " +
            " `click_url`,  " +
            " `status`,  " +
            " `open_time`,  " +
            " `close_time`  " +
            ")values(" +
            "#{id},  " +
            "#{adcode},  " +
            "#{defineAreaId},  " +
            "#{shopId},  " +
            "#{type},  " +
            "#{sort},  " +
            "#{imageUrl},  " +
            "#{clickUrl},  " +
            "#{status},  " +
            "#{openTime},  " +
            "#{closeTime}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainBanner(SupplychainBanner supplychainBanner);

    @Results(id = "supplychainBanner", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "adcode", column = "adcode"),
            @Result(property = "defineAreaId", column = "define_area_id"),
            @Result(property = "shopId", column = "shop_id"),
            @Result(property = "type", column = "type"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "imageUrl", column = "image_url"),
            @Result(property = "clickUrl", column = "click_url"),
            @Result(property = "status", column = "status"),
            @Result(property = "openTime", column = "open_time"),
            @Result(property = "closeTime", column = "close_time"),
            @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from supplychain_banner where id = #{id}")
    SupplychainBanner getSupplychainBanner(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_banner   where   1=1 " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainBanner")
    List<SupplychainBanner> getSupplychainBanners(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_banner   where    status=0 </script>")
    @ResultMap("supplychainBanner")
    List<SupplychainBanner> getSupplychainBannerAll();

    @Select("<script>select count(1) from supplychain_banner   where   1=1 " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainBannerCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_banner  " +
            "set " +
            "`adcode` = #{adcode}  , " +
            "`define_area_id` = #{defineAreaId}  , " +
            "`shop_id` = #{shopId}  , " +
            "`type` = #{type}  , " +
            "`sort` = #{sort}  , " +
            "`image_url` = #{imageUrl}  , " +
            "`click_url` = #{clickUrl}  , " +
            "`status` = #{status}  , " +
            "`open_time` = #{openTime}  , " +
            "`close_time` = #{closeTime}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeSupplychainBanner(SupplychainBanner supplychainBanner);

    @Select("<script>select " + column + " from supplychain_banner  where status=1 and shop_id = #{shopId} and define_area_id = #{areaId} and SYSDATE() between open_time and close_time </script>")
    @ResultMap("supplychainBanner")
    List<SupplychainBanner> getSupplychainBannerByShopIdAndAreaId(@Param("shopId") String shopId, @Param("areaId") String areaId);

    @Delete("<script> " +
            "delete from  supplychain_banner where id in  " +
            "<foreach collection=\"list\" item=\"item\" open=\"(\" close=\")\" separator=\",\">" +
            "#{item}" +
            "</foreach>" +
            "</script>"
    )
    void batchDeleteSupplychainBanner(@Param("list") List<String> list);

    @Select("<script>select " + column + " from supplychain_banner  " +
            "where 1=1 " +
            "and shop_id = #{shopId} " +
            " <when test=\"areaId != null\">" +
            " and define_area_id = #{areaId} " +
            " </when>" +
            "</script>"
    )
    @ResultMap("supplychainBanner")
    List<SupplychainBanner> getSupplychainBannersByShopIdAndAreaId(@Param("shopId") String shopId,@Param("areaId") String areaId);
}
