package com.tuoniaostore.supplychain.data;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import com.tuoniaostore.datamodel.supplychain.SupplychainPurchaseBindGood;
import com.tuoniaostore.supplychain.vo.PurchaseBindGoodVo;

@Component
@Mapper
public interface SupplychainPurchaseBindGoodMapper {

	@Insert({
		"INSERT INTO supplychain_purchase_bind_good(id, user_id, good_template_id)",
		"VALUES (#{id}, #{userId}, #{goodTemplateId})"
	})
	int addPurchaseBindGood(SupplychainPurchaseBindGood purchaseBindGood);
	
	@Insert({
		"<script>",
		"INSERT INTO supplychain_purchase_bind_good(id, user_id, good_template_id) VALUES",
		"<foreach collection='purchaseBindGoods' item='b' separator=','>",
    	"(#{b.id}, #{b.userId}, #{b.goodTemplateId})",
    	"</foreach>",
		"</script>"
	})
	int addPurchaseBindGoods(@Param("purchaseBindGoods") List<SupplychainPurchaseBindGood> purchaseBindGoods);

	@Update("UPDATE supplychain_purchase_bind_good SET good_template_id = #{goodTemplateId} WHERE id = #{id}")
	int updatePurchaseBindGood(@Param("id") String id, @Param("goodTemplateId") String goodTemplateId);

	@Delete({
		"<script>",
    	"DELETE FROM supplychain_purchase_bind_good WHERE id IN ",
    	"<foreach collection='ids' index='idx' item='id' open='(' separator=',' close=')'>",
    	"#{id}",
    	"</foreach>",
    	"</script>"
	})
	int deletePurchaseBindGood(@Param("ids") String [] ids);
	
//	String listSql = "from supplychain_purchase_bind_good bind " +
//			"left join good.good_template tem on bind.good_template_id = tem.id " +
//			"left join good.good_type type on tem.type_id = type.id " +
//			"left join good.good_brand b on tem.brand_id = b.id " +
//			"left join community.sys_user u on bind.user_id = u.id " +
//			"where 1 = 1" +
//			"<if test = \" param.purchaseRealName != null and param.purchaseRealName != '' \"> AND u.real_name like concat('%', #{param.purchaseRealName} ,'%') </if>" +
//			"<if test = \" param.goodTypeTitle != null and param.goodTypeTitle != '' \"> AND type.title like concat('%', #{param.goodTypeTitle} ,'%') </if>" +
//			"<if test = \" param.goodTemplateName != null and param.goodTemplateName != '' \"> AND tem.name like concat('%', #{param.goodTemplateName} ,'%') </if>" +
//			"<if test = \" param.brandName != null and param.brandName != '' \"> AND b.name like concat('%', #{param.brandName} ,'%') </if>";
	
	String listSql = "from supplychain_purchase_bind_good b inner join community.sys_user u " +
			" on b.user_id = u.id " +
			" LEFT JOIN community.sys_role r ON r.id = u.role_id" +
			"<if test = \" param.purchaseRealName != null and param.purchaseRealName != '' \"> AND u.real_name like concat('%', #{param.purchaseRealName} ,'%') </if>" +
			"group by b.user_id";

	@Select({
		"<script> select b.user_id purchaseUserId, u.real_name purchaseRealName, b.create_time, r.role_name ",
		listSql,
		"limit #{pageStartIndex}, #{pageSize}",
		"</script>"
	})
	List<PurchaseBindGoodVo> getPurchaseBindGoodList(@Param("param") PurchaseBindGoodVo param, @Param("pageStartIndex") int pageStartIndex, @Param("pageSize") int pageSize);
	
	@Select({
		"<script> select count(1) from ( select 1",
		listSql,
		") t",
		"</script>"
	})
	int getPurchaseBindGoodListCount(@Param("param") PurchaseBindGoodVo param);

	@Select("select * from supplychain_purchase_bind_good where id = #{id}")
	SupplychainPurchaseBindGood getPurchaseBindGood(@Param("id") String id);

	@Select({
		"<script> select bind.id purchaseBindGoodId, u.real_name purchaseBindGoodId, type.title goodTypeTitle, tem.name goodTemplateName, b.name brandName, bind.create_time",
		"from supplychain_purchase_bind_good bind ",
		"left join good.good_template tem on bind.good_template_id = tem.id ",
		"left join good.good_type type on tem.type_id = type.id ",
		"left join good.good_brand b on tem.brand_id = b.id ",
		"left join community.sys_user u on bind.user_id = u.id ",
		"where bind.id = #{id}",
		"</script>"
	})
	PurchaseBindGoodVo getPurchaseBindGoodVo(@Param("id") String id);

	@Select({
		"<script>",
		"select b.user_id purchaseUserId, t.name goodTemplateName from supplychain_purchase_bind_good b",
		"left join good.good_template t on b.good_template_id = t.id where b.user_id in",
		"<foreach collection='userIds' item='userId' open='(' separator=',' close=')'>",
    	"#{userId}",
    	"</foreach>",
    	"</script>"
	})
	List<PurchaseBindGoodVo> getPurchaseBindGoodNameList(@Param("userIds") List<String> userIds);

	@Select("select count(1) from supplychain_purchase_bind_good where user_id = #{userId}")
	int getPurchaseBindGoodCount(@Param("userId") String userId);

	@Delete({
		"<script>",
    	"DELETE FROM supplychain_purchase_bind_good WHERE user_id IN ",
    	"<foreach collection='userIds' item='userId' open='(' separator=',' close=')'>",
    	"#{userId}",
    	"</foreach>",
    	"</script>"
	})
	int deletePurchaseBindGoodByUserId(@Param("userIds") String[] userIds);

	@Select("select * from supplychain_purchase_bind_good where user_id = #{userId}")
	List<SupplychainPurchaseBindGood> getPurchaseBindGoodsByUserId(@Param("userId") String userId);

}
