package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainGoodStockLogger;
import com.tuoniaostore.supplychain.vo.SupplychainGoodStockLoggerListVo;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 商品的出入库日志
 * 主要是针对商品的库存改变
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainGoodStockLoggerMapper {

    String column = "`order_id`," +//订单ID
    		"`good_id`, " +//商品ID
            "`offset_stock`, " +//库存偏移值
            "`before_stock`, " +//修改之前的数量
            "`after_stock`, " +//修改之后的数量
            "`operator_role_id`, " +//操作者角色ID
            "`operator_role_name`, " +//操作者角色名
            "`type`, " +//类型
            "`operation_type`, " +//操作类型
            "`operation_time`"//操作的时间
            ;

    @Insert("insert into supplychain_good_stock_logger (" +
            " `id`,  " +
            " `order_id`,  " +
            " `good_id`,  " +
            " `offset_stock`,  " +
            " `before_stock`,  " +
            " `after_stock`,  " +
            " `operator_role_id`,  " +
            " `operator_role_name`,  " +
            " `type`," +
            " `operation_type`,  " +
            " `operation_time` " +
            ")values(" +
            "#{id},  " +
            "#{orderId},  " +
            "#{goodId},  " +
            "#{offsetStock},  " +
            "#{beforeStock},  " +
            "#{afterStock},  " +
            "#{operatorRoleId},  " +
            "#{operatorRoleName},  " +
            "#{type}, " +
            "#{operationType},  " +
            "#{operationTime} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainGoodStockLogger(SupplychainGoodStockLogger supplychainGoodStockLogger);

    @Results(id = "supplychainGoodStockLogger", value = {
            @Result(property = "id", column = "id"), @Result(property = "orderId", column = "order_id"), @Result(property = "goodId", column = "good_id"), @Result(property = "offsetStock", column = "offset_stock"), @Result(property = "beforeStock", column = "before_stock"), @Result(property = "afterStock", column = "after_stock"), @Result(property = "operatorRoleId", column = "operator_role_id"), @Result(property = "operatorRoleName", column = "operator_role_name"), @Result(property = "type", column = "type"), @Result(property = "operationType", column = "operation_type"), @Result(property = "operationTime", column = "operation_time")})
    @Select("select " + column + " from supplychain_good_stock_logger where id = #{id}")
    SupplychainGoodStockLogger getSupplychainGoodStockLogger(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_good_stock_logger   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainGoodStockLogger")
    List<SupplychainGoodStockLogger> getSupplychainGoodStockLoggers(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_good_stock_logger   where    1=1  and status=0 </script>")
    @ResultMap("supplychainGoodStockLogger")
    List<SupplychainGoodStockLogger> getSupplychainGoodStockLoggerAll();

    @Select("<script>select count(1) from supplychain_good_stock_logger   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainGoodStockLoggerCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_good_stock_logger  " +
            "set " +
            "`good_id` = #{goodId}  , " +
            "`order_id` = #{order_id}  , " +
            "`offset_stock` = #{offsetStock}  , " +
            "`before_stock` = #{beforeStock}  , " +
            "`after_stock` = #{afterStock}  , " +
            "`operator_role_id` = #{operatorRoleId}  , " +
            "`operator_role_name` = #{operatorRoleName}  , " +
            "`type` = #{type}  , " +
            "`operation_type` = #{operationType}  , " +
            "`operation_time` = #{operationTime}  " +
            " where id = #{id}")
    void changeSupplychainGoodStockLogger(SupplychainGoodStockLogger supplychainGoodStockLogger);

    String listSql = 
    		"from supplychain_good_stock_logger s " +
    		"left join `order`.order_entry o on s.order_id = o.id " +
    		"left join `order`.order_good_snapshot og on o.id = og.order_id and s.good_id = og.good_id " +
    		"where 1=1 " +
    		"<if test = 'startTime != null' > and s.operation_time &gt;= #{startTime} </if>" +
    		"<if test = 'endTime != null' > and s.operation_time &gt;= #{endTime} </if>" +
//    		"where s.operation_time &gt;= #{startTime} and s.operation_time &lt;= #{endTime} " +
    		"<if test = 'type != -1'>" +
    		"and s.type = #{type} " +
    		"</if>" +
    		"<if test = 'operationType != -1'>" +
    		"and s.operation_type = #{operationType} " +
    		"</if>" +
    		"<if test = 'orderSequenceNumber != null'>" +
    		"and o.order_sequence_number like concat('%', #{orderSequenceNumber} , '%') " +
    		"</if>" +
    		"<if test = 'shopName != null'>" +
    		"and o.shipping_nick_name like concat('%', #{shopName} , '%') " +
    		"</if>" +
    		"<if test = 'goodName != null'>" +
    		"and og.good_name like concat('%', #{goodName} , '%') " +
    		"</if>"
    		;
    
    @Select({
    	"<script>",
    	"select s.*, o.order_sequence_number, o.shipping_nick_name shopName, og.good_type_name, og.good_name, og.good_barcode, og.normal_price",
    	listSql,
    	"limit #{pageStartIndex}, #{pageSize}",
    	"</script>"
    })
	List<SupplychainGoodStockLoggerListVo> getGoodStockLoggerList(@Param("type") int type,
			@Param("operationType") int operationType, @Param("orderSequenceNumber") String orderSequenceNumber,
			@Param("goodName") String goodName, @Param("shopName") String shopName, @Param("startTime") Date startTime,
			@Param("endTime") Date endTime, @Param("pageStartIndex") int pageStartIndex,
			@Param("pageSize") int pageSize);
    
    @Select({
    	"<script>",
    	"select count(1)",
    	listSql,
    	"</script>"
    })
	int getGoodStockLoggerListCount(@Param("type") int type, @Param("operationType") int operationType,
			@Param("orderSequenceNumber") String orderSequenceNumber, @Param("goodName") String goodName,
			@Param("shopName") String shopName, @Param("startTime") Date startTime, @Param("endTime") Date endTime);

	@Insert("<script> insert into supplychain_good_stock_logger (" +
			" `id`,  " +
			" `order_id`,  " +
			" `good_id`,  " +
			" `offset_stock`,  " +
			" `before_stock`,  " +
			" `after_stock`,  " +
			" `operator_role_id`,  " +
			" `operator_role_name`,  " +
			" `type`," +
			" `operation_type`,  " +
			" `operation_time` " +
			")values" +
			"<foreach collection=\"lists\" item=\"item\"  separator=\",\"> " +
			"(#{item.id}, " +
			"#{item.orderId}, " +
			"#{item.goodId}, " +
			"#{item.offsetStock}, " +
			"#{item.beforeStock}, " +
			"#{item.afterStock}, " +
			"#{item.operatorRoleId}, " +
			"#{item.operatorRoleName}," +
			"#{item.type}," +
			"#{item.operationType}," +
			"#{item.operationTime}) " +
			"</foreach> </script>")
    void addSupplychainGoodStockLoggers(@Param("lists") List<SupplychainGoodStockLogger> supplychainGoodStockLoggers);
}
