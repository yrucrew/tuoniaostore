package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainPartnerCallback;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainPartnerCallbackMapper {

    String column = "`id`, " +//
            "`partner_id`, " +//合作方ID
            "`opt_type`, " +//操作类型
            "`callback_url`, " +//回调地址
            "`callback_delayed`, " +//回调延迟
            "`status`, " +//状态 0-禁用 1-启用
            "`describe`"//描述内容
            ;

    @Insert("insert into supplychain_partner_callback (" +
            " `id`,  " +
            " `partner_id`,  " +
            " `opt_type`,  " +
            " `callback_url`,  " +
            " `callback_delayed`,  " +
            " `status`,  " +
            " `describe` " +
            ")values(" +
            "#{id},  " +
            "#{partnerId},  " +
            "#{optType},  " +
            "#{callbackUrl},  " +
            "#{callbackDelayed},  " +
            "#{status},  " +
            "#{describe} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainPartnerCallback(SupplychainPartnerCallback supplychainPartnerCallback);

    @Results(id = "supplychainPartnerCallback", value = {
            @Result(property = "id", column = "id"), @Result(property = "partnerId", column = "partner_id"), @Result(property = "optType", column = "opt_type"), @Result(property = "callbackUrl", column = "callback_url"), @Result(property = "callbackDelayed", column = "callback_delayed"), @Result(property = "status", column = "status"), @Result(property = "describe", column = "describe")})
    @Select("select " + column + " from supplychain_partner_callback where id = #{id}")
    SupplychainPartnerCallback getSupplychainPartnerCallback(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_partner_callback   where   1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainPartnerCallback")
    List<SupplychainPartnerCallback> getSupplychainPartnerCallbacks(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_partner_callback   where    1=1    and status=0 </script>")
    @ResultMap("supplychainPartnerCallback")
    List<SupplychainPartnerCallback> getSupplychainPartnerCallbackAll();

    @Select("<script>select count(1) from supplychain_partner_callback   where      1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainPartnerCallbackCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_partner_callback  " +
            "set " +
            "`partner_id` = #{partnerId}  , " +
            "`opt_type` = #{optType}  , " +
            "`callback_url` = #{callbackUrl}  , " +
            "`callback_delayed` = #{callbackDelayed}  , " +
            "`status` = #{status}  , " +
            "`describe` = #{describe}  " +
            " where id = #{id}")
    void changeSupplychainPartnerCallback(SupplychainPartnerCallback supplychainPartnerCallback);

}
