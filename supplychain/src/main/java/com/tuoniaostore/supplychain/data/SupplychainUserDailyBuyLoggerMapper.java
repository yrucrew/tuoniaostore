package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainUserDailyBuyLogger;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainUserDailyBuyLoggerMapper {

    String column = "`id`, " +//
            "`user_id`, " +//通行证ID
            "`item_id`, " +//商品ID
            "`item_template_id`, " +//对应的商品模版ID
            "`has_buy_count`, " +//已购买数量
            "`happen_date`"//发生日期
            ;

    @Insert("insert into supplychain_user_daily_buy_logger (" +
            " `id`,  " +
            " `user_id`,  " +
            " `item_id`,  " +
            " `item_template_id`,  " +
            " `has_buy_count`,  " +
            " `happen_date` " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{itemId},  " +
            "#{itemTemplateId},  " +
            "#{hasBuyCount},  " +
            "#{happenDate} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainUserDailyBuyLogger(SupplychainUserDailyBuyLogger supplychainUserDailyBuyLogger);

    @Results(id = "supplychainUserDailyBuyLogger", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "itemId", column = "item_id"), @Result(property = "itemTemplateId", column = "item_template_id"), @Result(property = "hasBuyCount", column = "has_buy_count"), @Result(property = "happenDate", column = "happen_date")})
    @Select("select " + column + " from supplychain_user_daily_buy_logger where id = #{id}")
    SupplychainUserDailyBuyLogger getSupplychainUserDailyBuyLogger(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_user_daily_buy_logger   where   1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainUserDailyBuyLogger")
    List<SupplychainUserDailyBuyLogger> getSupplychainUserDailyBuyLoggers(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_user_daily_buy_logger   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("supplychainUserDailyBuyLogger")
    List<SupplychainUserDailyBuyLogger> getSupplychainUserDailyBuyLoggerAll();

    @Select("<script>select count(1) from supplychain_user_daily_buy_logger   where   1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainUserDailyBuyLoggerCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_user_daily_buy_logger  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`item_id` = #{itemId}  , " +
            "`item_template_id` = #{itemTemplateId}  , " +
            "`has_buy_count` = #{hasBuyCount}  , " +
            "`happen_date` = #{happenDate}  " +
            " where id = #{id}")
    void changeSupplychainUserDailyBuyLogger(SupplychainUserDailyBuyLogger supplychainUserDailyBuyLogger);

}
