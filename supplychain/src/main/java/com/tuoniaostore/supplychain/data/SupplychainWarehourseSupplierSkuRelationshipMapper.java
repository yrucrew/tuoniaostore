package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseSupplierSkuRelationship;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Mapper
public interface SupplychainWarehourseSupplierSkuRelationshipMapper {


    @Select("<script> select * from supplychain_warehourse_supplier_sku_relationship where 1 = 1 " +
            "<if test=\"map.shopId != null and map.shopId.trim() != ''\"> and shop_id = #{map.shopId} </if>" +
            "<if test=\"map.priceId != null and map.priceId.trim() != ''\"> and price_id = #{map.priceId} </if>" +
            "<if test=\"map.supplierId != null and map.supplierId.trim() != ''\"> and supplier_id = #{map.supplierId} </if>" +
            " </script>")
    List<SupplychainWarehourseSupplierSkuRelationship> getSupplychainWarehourseSupplierSkuRelationshipByParam(@Param("map") Map<String, Object> map);

    @Select("<script> select * from supplychain_warehourse_supplier_sku_relationship where shop_id = #{map.shopId} " +
            "<if test=\"map.priceId != null\"> and map.priceId in <foreach collection=\"map.priceId\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if>" +
            " </script>")
    List<SupplychainWarehourseSupplierSkuRelationship> getSupplychainWarehourseSupplierSkuRelationshipById(@Param("map") Map<String, Object> map);


    @Select("<script> select * from supplychain_warehourse_supplier_sku_relationship where 1 = 1 " +
            "<if test=\"map.shopId != null and map.shopId.trim() != ''\"> and shop_id = #{map.shopId} </if>" +
            "<if test=\"map.notShopId != null and map.notShopId.trim() != ''\"> and supplier_id != #{map.notShopId} </if>" +
            " </script>")
    List<SupplychainWarehourseSupplierSkuRelationship> getSupplychainWarehourseSupplierSkuRelationshipByShopIdAndNotSupplierId(@Param("map")Map<String, Object> map);

    @Insert("<script> insert into supplychain_warehourse_supplier_sku_relationship" +
            "(`id`,`shop_id`,`price_id`,`supplier_id`,`user_id`,`sort`) " +
            "<foreach collection=\"lists\" item=\"item\" separator=\"UNION ALL\"> " +
            " (SELECT #{item.id},#{item.shopId},#{item.priceId},#{item.supplierId}, #{item.userId}, #{item.sort} " +
            " FROM DUAL WHERE (SELECT COUNT(*) FROM  supplychain_warehourse_supplier_sku_relationship " +
            " WHERE shop_id = #{item.shopId} and price_id = #{item.priceId}) <![CDATA[<]]> 1 ) " +
            "</foreach> </script>")
    void batchAddSupplychainWarehourseSupplierSkuRelationship(@Param("lists") List<SupplychainWarehourseSupplierSkuRelationship> addList);

    @Delete(" delete from supplychain_warehourse_supplier_sku_relationship where shop_id = #{shopId} and supplier_id = #{supplierId} ")
    void clearSupplychainWarehourseSupplierSkuRelationshipByShopIdAndsupplierId(@Param("shopId") String shopId,@Param("supplierId") String supplierId);

}
