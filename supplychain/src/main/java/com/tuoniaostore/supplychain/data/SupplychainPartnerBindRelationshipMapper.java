package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainPartnerBindRelationship;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainPartnerBindRelationshipMapper {

    String column = "`id`, " +//
            "`partner_id`, " +//合作方ID
            "`bind_shop_id`, " +//绑定我方仓库ID
            "`partner_shop_id`, " +//对应合作方仓库标识
            "`status`, " +//关系状态 1、独代
            "`create_time`"//建立时间
            ;

    @Insert("insert into supplychain_partner_bind_relationship (" +
            " `id`,  " +
            " `partner_id`,  " +
            " `bind_shop_id`,  " +
            " `partner_shop_id`,  " +
            " `status`,  " +
            ")values(" +
            "#{id},  " +
            "#{partnerId},  " +
            "#{bindShopId},  " +
            "#{partnerShopId},  " +
            "#{status},  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainPartnerBindRelationship(SupplychainPartnerBindRelationship supplychainPartnerBindRelationship);

    @Results(id = "supplychainPartnerBindRelationship", value = {
            @Result(property = "id", column = "id"), @Result(property = "partnerId", column = "partner_id"), @Result(property = "bindShopId", column = "bind_shop_id"), @Result(property = "partnerShopId", column = "partner_shop_id"), @Result(property = "status", column = "status"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from supplychain_partner_bind_relationship where id = #{id}")
    SupplychainPartnerBindRelationship getSupplychainPartnerBindRelationship(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_partner_bind_relationship   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainPartnerBindRelationship")
    List<SupplychainPartnerBindRelationship> getSupplychainPartnerBindRelationships(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_partner_bind_relationship   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("supplychainPartnerBindRelationship")
    List<SupplychainPartnerBindRelationship> getSupplychainPartnerBindRelationshipAll();

    @Select("<script>select count(1) from supplychain_partner_bind_relationship   where     1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainPartnerBindRelationshipCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_partner_bind_relationship  " +
            "set " +
            "`partner_id` = #{partnerId}  , " +
            "`bind_shop_id` = #{bindShopId}  , " +
            "`partner_shop_id` = #{partnerShopId}  , " +
            "`status` = #{status}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeSupplychainPartnerBindRelationship(SupplychainPartnerBindRelationship supplychainPartnerBindRelationship);

}
