package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainGoodModifyLogger;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 商品库存修改日志
 * 注意：该表主要记录人为介入修改的数据
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainGoodModifyLoggerMapper {

    String column = "`id`, " +//
            "`role_id`, " +//修改者角色ID 系统修改时为0
            "`role_name`, " +//修改者角色名
            "`good_id`, " +//商品ID
            "`good_template_id`, " +//商品模版ID
            "`before_stock`, " +//修改前商品数量
            "`after_stock`, " +//修改后商品数量
            "`type`, " +//操作类型 0-出库 1-入库 3-盘点修复
            "`modify_time`"//修改时间
            ;

    @Insert("insert into supplychain_good_modify_logger (" +
            " `id`,  " +
            " `role_id`,  " +
            " `role_name`,  " +
            " `good_id`,  " +
            " `good_template_id`,  " +
            " `before_stock`,  " +
            " `after_stock`,  " +
            " `type`,  " +
            " `modify_time` " +
            ")values(" +
            "#{id},  " +
            "#{roleId},  " +
            "#{roleName},  " +
            "#{goodId},  " +
            "#{goodTemplateId},  " +
            "#{beforeStock},  " +
            "#{afterStock},  " +
            "#{type},  " +
            "#{modifyTime} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainGoodModifyLogger(SupplychainGoodModifyLogger supplychainGoodModifyLogger);

    @Results(id = "supplychainGoodModifyLogger", value = {
            @Result(property = "id", column = "id"), @Result(property = "roleId", column = "role_id"), @Result(property = "roleName", column = "role_name"), @Result(property = "goodId", column = "good_id"), @Result(property = "goodTemplateId", column = "good_template_id"), @Result(property = "beforeStock", column = "before_stock"), @Result(property = "afterStock", column = "after_stock"), @Result(property = "type", column = "type"), @Result(property = "modifyTime", column = "modify_time")})
    @Select("select " + column + " from supplychain_good_modify_logger where id = #{id}")
    SupplychainGoodModifyLogger getSupplychainGoodModifyLogger(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_good_modify_logger   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainGoodModifyLogger")
    List<SupplychainGoodModifyLogger> getSupplychainGoodModifyLoggers(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_good_modify_logger   where    1=1  and status=0 </script>")
    @ResultMap("supplychainGoodModifyLogger")
    List<SupplychainGoodModifyLogger> getSupplychainGoodModifyLoggerAll();

    @Select("<script>select count(1) from supplychain_good_modify_logger   where      1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainGoodModifyLoggerCount(@Param("status") int status, @Param("name") String name );

    @Update("update supplychain_good_modify_logger  " +
            "set " +
            "`role_id` = #{roleId}  , " +
            "`role_name` = #{roleName}  , " +
            "`good_id` = #{goodId}  , " +
            "`good_template_id` = #{goodTemplateId}  , " +
            "`before_stock` = #{beforeStock}  , " +
            "`after_stock` = #{afterStock}  , " +
            "`type` = #{type}  , " +
            "`modify_time` = #{modifyTime}  " +
            " where id = #{id}")
    void changeSupplychainGoodModifyLogger(SupplychainGoodModifyLogger supplychainGoodModifyLogger);

}
