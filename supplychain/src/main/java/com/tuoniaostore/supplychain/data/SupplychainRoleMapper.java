package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainRole;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainRoleMapper {

    String column = "`id`, " +//
            "`user_id`, " +//对应的用户ID
            "`role_name`, " +//角色名
            "`title`, " +//显示的标题 用于交互时
            "`type`, " +//角色类型
            "`status`, " +//当前状态
            "`contact_phone`, " +//联系电话
            "`create_time`"//建立时间
            ;

    @Insert("insert into supplychain_role (" +
            " `id`,  " +
            " `user_id`,  " +
            " `role_name`,  " +
            " `title`,  " +
            " `type`,  " +
            " `status`,  " +
            " `contact_phone`,  " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{roleName},  " +
            "#{title},  " +
            "#{type},  " +
            "#{status},  " +
            "#{contactPhone},  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainRole(SupplychainRole supplychainRole);

    @Results(id = "supplychainRole", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "roleName", column = "role_name"), @Result(property = "title", column = "title"), @Result(property = "type", column = "type"), @Result(property = "status", column = "status"), @Result(property = "contactPhone", column = "contact_phone"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from supplychain_role where id = #{id}")
    SupplychainRole getSupplychainRole(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_role   where   1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainRole")
    List<SupplychainRole> getSupplychainRoles(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_role   where    1=1  and status=0 </script>")
    @ResultMap("supplychainRole")
    List<SupplychainRole> getSupplychainRoleAll();

    @Select("<script>select count(1) from supplychain_role   where    1=1 " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainRoleCount(@Param("status") int status, @Param("name") String name );

    @Update("update supplychain_role  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`role_name` = #{roleName}  , " +
            "`title` = #{title}  , " +
            "`type` = #{type}  , " +
            "`status` = #{status}  , " +
            "`contact_phone` = #{contactPhone}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeSupplychainRole(SupplychainRole supplychainRole);

}
