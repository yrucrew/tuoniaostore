package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.PosWork;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-06 10:47:35
 */
@Component
@Mapper
public interface PosWorkMapper {

    String column = "`id`, " +//
            "`shop_id`, " +//门店ID
            "`user_id`, " +//
            "`total_income`, " +//总收入
            "`total_order`, " +//总订单数
            "`alipay_count`, " +//支付宝订单数
            "`wx_count`, " +//微信订单数
            "`cash_count`, " +//现金总条数
            "`input_cash_income`, " +//交班输入现金数
            "`alipay_income`, " +//支付宝收入
            "`wx_income`, " +//微信收入
            "`cash_income`, " +//现金收入
            "`login_time`, " +//上班时间
            "`logout_time`, " +//交班时间
            "`create_time`,"+//创建时间
            "`practical_cash_income`,"+//实际清点现金收入
            "`refund_order`,"+//实际清点现金收入
            "`redund_price`,"+//实际清点现金收入
            "`work_status`"//工作状态
            ;

    @Results(id = "posWork", value = {
            @Result(property = "id", column = "id"),@Result(property = "shopId", column = "shop_id"),
            @Result(property = "userId", column = "user_id"), @Result(property = "totalIncome", column = "total_income"),
            @Result(property = "totalOrder", column = "total_order"), @Result(property = "alipayCount", column = "alipay_count"),
            @Result(property = "wxCount", column = "wx_count"), @Result(property = "cashCount", column = "cash_count"),
            @Result(property = "inputCashIncome", column = "input_cash_income"), @Result(property = "alipayIncome", column = "alipay_income"),
            @Result(property = "wxIncome", column = "wx_income"), @Result(property = "cashIncome", column = "cash_income"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "refundOrder", column = "refund_order"),
            @Result(property = "redundPrice", column = "redund_price"),
            @Result(property = "practicalCashIncome", column = "practical_cash_income"),
            @Result(property = "logoutTime", column = "logout_time"),@Result(property = "loginTime", column = "login_time"),
            @Result(property = "workStatus", column = "work_status")})
    @Select("select " + column + " from pos_work where id = #{id}")
    PosWork getPosWork(@Param("id") String id);

    @Insert("insert into pos_work (" +
            " `id`,  " +
            " `shop_id`,  " +
            " `user_id`,  " +
            " `total_income`,  " +
            " `total_order`,  " +
            " `alipay_count`,  " +
            " `wx_count`,  " +
            " `cash_count`,  " +
            " `input_cash_income`,  " +
            " `alipay_income`,  " +
            " `wx_income`,  " +
            " `cash_income`,  " +
            " `login_time`,  " +
            " `logout_time`,  " +
            " `create_time`,  " +
            " `practical_cash_income`,  " +
            " `redund_price`,  " +
            " `refund_order`,  " +
            " `work_status`  " +
            ")values(" +
            "#{id},  " +
            "#{shopId},  " +
            "#{userId},  " +
            "#{totalIncome},  " +
            "#{totalOrder},  " +
            "#{alipayCount},  " +
            "#{wxCount},  " +
            "#{cashCount},  " +
            "#{inputCashIncome},  " +
            "#{alipayIncome},  " +
            "#{wxIncome},  " +
            "#{cashIncome},  " +
            "#{loginTime},  " +
            "#{logoutTime},  " +
            "#{createTime},  " +
            "#{practicalCashIncome},  " +
            "#{redundPrice},  " +
            "#{refundOrder},  " +
            "#{workStatus}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addPosWork(PosWork posWork);

    @Select("<script>select  " + column + "  from pos_work   where   retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("posWork")
    List<PosWork> getPosWorks(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from pos_work   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("posWork")
    List<PosWork> getPosWorkAll();

    @Update("update pos_work  " +
            "set " +
            "`shop_id` = #{shopId}  , " +
            "`user_id` = #{userId}  , " +
            "`total_income` = #{totalIncome}  , " +
            "`total_order` = #{totalOrder}  , " +
            "`alipay_count` = #{alipayCount}  , " +
            "`wx_count` = #{wxCount}  , " +
            "`cash_count` = #{cashCount}  , " +
            "`input_cash_income` = #{inputCashIncome}  , " +
            "`alipay_income` = #{alipayIncome}  , " +
            "`wx_income` = #{wxIncome}  , " +
            "`cash_income` = #{cashIncome} , " +
            "`create_time` = #{createTime}," +
            "`login_time` = #{loginTime}, " +
            "`logout_time` = #{logoutTime}," +
            "`redund_price` = #{redundPrice}," +
            "`practical_cash_income` = #{practicalCashIncome}," +
            "`refund_order` = #{refundOrder}," +
            "`work_status` = #{workStatus}" +
            " where id = #{id}")
    void changePosWork(PosWork posWork);

    @Select("<script> select "+column+" from pos_work where 1=1  " +

            " <when test=\"map.userId != null and map.userId.trim() != '' \">" +
            "  and user_id= #{map.userId}</when>" +
            " <when test=\"map.workStatus != null\">" +
            "  and work_status= #{map.workStatus}</when>" +
            " <when test=\"map.shopId != null and map.shopId != '' \">  and shop_id=#{map.shopId}</when>" +
            " <when test=\"map.startTime  != null and map.startTime != ''\"> and logout_time <![CDATA[>=]]> #{map.startTime} </when> "+
            " <when test=\"map.endTime  != null and map.endTime != ''\"> and logout_time <![CDATA[<]]> #{map.endTime} </when> "+
            " <when test=\"map.accountOrName != null\">and (user_id in <foreach collection=\"map.accountOrName\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> "+
            " order by create_time desc "+
            " <when test=\"map.pageIndex != null and map.pageSize != null\">" +
            " limit #{map.pageIndex}, #{map.pageSize}</when> " +
            "</script>")
    @ResultMap("posWork")
    List<PosWork> getPosWrokListByMap(@Param("map") Map map);

    @Select("<script>select count(1) from pos_work   where     retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getPosWorkCount(@Param("status") int status, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);


    @Select("<script> select "+column+" from pos_work where 1=1  " +
            " <when test=\"map.userId != null and map.userId != '' \">  and user_id = #{map.userId} </when>" +
            " <when test=\"map.shopId != null and map.shopId != '' \">  and shop_id = #{map.shopId} </when>" +
            " <when test=\"map.workStatus != null and map.workStatus != ''\">  and work_status = #{map.workStatus}</when>" +
            " LIMIT 1"+
            "</script>")
    @ResultMap("posWork")
    PosWork getPosWrokByMap(@Param("map") Map map);

    @Select("<script> select count(0) from pos_work where 1=1  " +
            " <when test=\"map.workStatus != null\">" +
            "  and work_status= #{map.workStatus}</when>" +
            " <when test=\"map.userId != null and map.userId.trim() != '' \">" +
            "  and user_id= #{map.userId}</when>" +
            " <when test=\"map.shopId != null and map.shopId != '' \">  and shop_id=#{map.shopId}</when>" +
            " <when test=\"map.startTime  != null and map.startTime != ''\"> and logout_time <![CDATA[>=]]> #{map.startTime} </when> "+
            " <when test=\"map.endTime  != null and map.endTime != ''\"> and logout_time <![CDATA[<]]> #{map.endTime} </when> "+
            " <when test=\"map.accountOrName != null \">and (user_id in <foreach collection=\"map.accountOrName\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> "+
            " <when test=\"map.pageIndex != null and map.pageSize != null\">" +
            "  limit #{map.pageIndex}, #{map.pageSize}" +
            "</when>"+
            "</script>")
    int getPosWrokListCountByMap(@Param("map") Map map);

    @Select("TRUNCATE TABLE supplychain_shop_good_sale")
    void clearSupplyShopGoodSale();
}
