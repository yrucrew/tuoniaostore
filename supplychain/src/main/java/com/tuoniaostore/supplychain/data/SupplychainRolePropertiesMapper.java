package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainRoleProperties;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainRolePropertiesMapper {

    String column = "`id`, " +//
            "`role_id`, " +//角色ID
            "`type`, " +//属性类型
            "`k`, " +//属性标志
            "`v`"//具体属性值
            ;

    @Insert("insert into supplychain_role_properties (" +
            " `id`,  " +
            " `role_id`,  " +
            " `type`,  " +
            " `k`,  " +
            " `v` " +
            ")values(" +
            "#{id},  " +
            "#{roleId},  " +
            "#{type},  " +
            "#{k},  " +
            "#{v} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainRoleProperties(SupplychainRoleProperties supplychainRoleProperties);

    @Results(id = "supplychainRoleProperties", value = {
            @Result(property = "id", column = "id"), @Result(property = "roleId", column = "role_id"), @Result(property = "type", column = "type"), @Result(property = "k", column = "k"), @Result(property = "v", column = "v")})
    @Select("select " + column + " from supplychain_role_properties where id = #{id}")
    SupplychainRoleProperties getSupplychainRoleProperties(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_role_properties   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainRoleProperties")
    List<SupplychainRoleProperties> getSupplychainRolePropertiess(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_role_properties   where    1=1  and status=0 </script>")
    @ResultMap("supplychainRoleProperties")
    List<SupplychainRoleProperties> getSupplychainRolePropertiesAll();

    @Select("<script>select count(1) from supplychain_role_properties   where   1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainRolePropertiesCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_role_properties  " +
            "set " +
            "`role_id` = #{roleId}  , " +
            "`type` = #{type}  , " +
            "`k` = #{k}  , " +
            "`v` = #{v}  " +
            " where id = #{id}")
    void changeSupplychainRoleProperties(SupplychainRoleProperties supplychainRoleProperties);

}
