package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainShopBindRelationship;
import com.tuoniaostore.datamodel.user.SysUser;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:24
 */
@Component
@Mapper
public interface SupplychainShopBindRelationshipMapper {

    String column = "`id`, " +//
            "`shop_id`, " +//仓库ID
            "`role_id`, " +//角色ID
            "`type`, " +//绑定类型，如：1-仓管 2-拣货员 3-配送员
            "`status`, " +//当前状态，0-不可用 1可用
            "`create_time`"//绑定时间
            ;

    @Insert("insert into supplychain_shop_bind_relationship (" +
            " `id`,  " +
            " `shop_id`,  " +
            " `role_id`,  " +
            " `type`,  " +
            " `status` " +
            ")values(" +
            "#{id},  " +
            "#{shopId},  " +
            "#{roleId},  " +
            "#{type},  " +
            "#{status} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainShopBindRelationship(SupplychainShopBindRelationship supplychainShopBindRelationship);


    @Results(id = "supplychainShopBindRelationship", value = {
            @Result(property = "id", column = "id"), @Result(property = "shopId", column = "shop_id"), @Result(property = "roleId", column = "role_id"), @Result(property = "type", column = "type"), @Result(property = "status", column = "status"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from supplychain_shop_bind_relationship where id = #{id}")
    SupplychainShopBindRelationship getSupplychainShopBindRelationship(@Param("id") String id);

    @Select("  select " + column + " from supplychain_shop_bind_relationship where role_id = #{roleId} and type = #{type} and status = 1")
    @ResultMap("supplychainShopBindRelationship")
    List<SupplychainShopBindRelationship> getShopBindRelationships(@Param("roleId") String roleId, @Param("type") int type);

    @Select(" select * from supplychain_shop_bind_relationship where role_id = #{roleId} and type = #{type} and shop_id = #{shopId} and status = 1 LIMIT 1")
    @ResultMap("supplychainShopBindRelationship")
    SupplychainShopBindRelationship getShopBindRelationship(@Param("roleId") String roleId, @Param("type") int type, @Param("shopId") String shopId);


    @Select("<script>select  " + column + "  from supplychain_shop_bind_relationship   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainShopBindRelationship")
    List<SupplychainShopBindRelationship> getSupplychainShopBindRelationships(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_shop_bind_relationship   where   1=1  and status=0 </script>")
    @ResultMap("supplychainShopBindRelationship")
    List<SupplychainShopBindRelationship> getSupplychainShopBindRelationshipAll();

    @Select("<script>select count(1) from supplychain_shop_bind_relationship   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainShopBindRelationshipCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_shop_bind_relationship  " +
            "set " +
            "`shop_id` = #{shopId}  , " +
            "`role_id` = #{roleId}  , " +
            "`type` = #{type}  , " +
            "`status` = #{status}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeSupplychainShopBindRelationship(SupplychainShopBindRelationship supplychainShopBindRelationship);

    @Select({
            "<script>",
            "select * from supplychain_shop_bind_relationship where status = 1 and type = #{type} and role_id in",
            "<foreach collection=\"roleIds\" item=\"roleId\" open=\"(\" separator=\",\" close=\")\"> #{roleId} </foreach>",
            "</script>"
    })
    List<SupplychainShopBindRelationship> getShopBindRelationshipsByRoleIds(@Param("roleIds") String[] roleIds, @Param("type") int type);

    //    @Delete({
//    	"<script>",
//    	"delete from supplychain_shop_bind_relationship where type = #{type} and role_id = #{roleId} and shop_id in",
//    	"<foreach collection=\"shopIds\" item=\"shopId\" open=\"(\" separator=\",\" close=\")\"> #{shopId} </foreach>",
//    	"</script>"
//    })
    @Delete("delete from supplychain_shop_bind_relationship where type = #{type} and role_id = #{roleId}")
    int delSupplychainShopBindRelationships(@Param("type") int type, @Param("roleId") String roleId);

    @Select("<script>SELECT " + column + " FROM `supplychain_shop_bind_relationship` AS r WHERE r.`shop_id` = #{shopId} " +
            "AND r.`type`= #{type} AND r.`status` = 1" +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainShopBindRelationship")
    List<SupplychainShopBindRelationship> getRelationshipsByShopIdAndType(@Param("shopId") String shopId, @Param("type") int type, @Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    @Update("UPDATE `supplychain_shop_bind_relationship` AS s SET s.`status` = #{status} WHERE s.`role_id` = #{roleId}AND s.`type` = #{type} ")
    int updateRelationshipByUserIdAndType(String roleId, int type, int status);

    @Select("<script>" +
            "SELECT " + column + " FROM supplychain_shop_bind_relationship as sbr WHERE sbr.role_id = #{userId} AND sbr.type = #{type}" +
            "</script>")
    @ResultMap("supplychainShopBindRelationship")
    SupplychainShopBindRelationship getSupplychainShopBindRelationshipByUserIdAndType(@Param("userId") String userId, @Param("type") int type);
}
