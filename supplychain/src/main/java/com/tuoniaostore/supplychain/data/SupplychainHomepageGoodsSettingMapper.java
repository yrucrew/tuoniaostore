package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageButtonSetting;
import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageGoodsSetting;
import com.tuoniaostore.datamodel.vo.supplychain.SupplychainHomePageGoodDetail;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 首页设置表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-04 14:40:15
 */
@Component
@Mapper
public interface SupplychainHomepageGoodsSettingMapper {

    String column = "`id`, " +
            "`type`, " +//记录类型 0=精选商品; 1=推荐商品; 2=推荐分类 3=推荐品牌
            "`logic_area_id`, " +//区域id
            "`value`, " +//记录值 当type(记录类型)=0,1时 此字段是商品模板id; type=2时 此字段时分类id; type=3时 此字段为品牌id
            "`name`, " +//名称(根据value值的类型 记一下分类/品牌名称, 说不定以后有用, value是商品id的话这个值就留空, 没想到有什么用)
            "`description`, " +//标语
            "`sort`, " +//排序
            "`create_time`, " +//创建时间
            "`creator_id`"//创建人id
            ;

    @Insert("insert into supplychain_homepage_goods_setting (" +
            " `id`,  " +
            " `type`,  " +
            " `logic_area_id`,  " +
            " `value`,  " +
            " `name`,  " +
            " `description`,  " +
            " `sort`,  " +
            " `creator_id` " +
            ")values(" +
            "#{id},  " +
            "#{type},  " +
            "#{logicAreaId},  " +
            "#{value},  " +
            "#{name},  " +
            "#{description},  " +
            "#{sort},  " +
            "#{creatorId} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainHomepageGoodsSetting(SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting);

    @Results(id = "supplychainHomepageGoodsSetting", value = {
            @Result(property = "id", column = "id"), @Result(property = "type", column = "type"), @Result(property = "logicAreaId", column = "logic_area_id"), @Result(property = "value", column = "value"), @Result(property = "name", column = "name"), @Result(property = "description", column = "description"), @Result(property = "sort", column = "sort"), @Result(property = "createTime", column = "create_time"), @Result(property = "creatorId", column = "creator_id")})
    @Select("select " + column + " from supplychain_homepage_goods_setting where id = #{id}")
    SupplychainHomepageGoodsSetting getSupplychainHomepageGoodsSetting(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_homepage_goods_setting   where  1=1  " +
            " <when test=\"type != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainHomepageGoodsSetting")
    List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettings( @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name );

    @Select("<script>select  " + column + "  from supplychain_homepage_goods_setting  </script>")
    @ResultMap("supplychainHomepageGoodsSetting")
    List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettingAll();

    @Select("<script>select count(1) from supplychain_homepage_goods_setting   where   1=1   " +

            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainHomepageGoodsSettingCount(  @Param("name") String name );

    @Update("update supplychain_homepage_goods_setting  " +
            "set " +
            "`type` = #{type}  , " +
            "`logic_area_id` = #{logicAreaId}  , " +
            "`value` = #{value}  , " +
            "`name` = #{name}  , " +
            "`description` = #{description}  , " +
            "`sort` = #{sort}  , " +
            "`create_time` = #{createTime}  , " +
            "`creator_id` = #{creatorId}  " +
            " where id = #{id}")
    void changeSupplychainHomepageGoodsSetting(SupplychainHomepageGoodsSetting supplychainHomepageGoodsSetting);

    @Select("<script> select " + column + " from supplychain_homepage_goods_setting where 1 = 1  " +
            "<if test=\"areaId != null\"> and logic_area_id = #{areaId}</if> " +
            "<if test=\"type != null\"> and type = #{type}</if> " +
            "order by sort desc " +
            "<if test=\"pageIndex != null and pageSize != null\">limit #{pageIndex},#{pageSize}</if> " +
            "</script>")
    @ResultMap("supplychainHomepageGoodsSetting")
    List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettingByAreaIdAndType(@Param("areaId")String areaId, @Param("type")Integer type, @Param("pageIndex")Integer pageIndex, @Param("pageSize")Integer pageSize);

    @Select("<script> select " + column + " from supplychain_homepage_goods_setting where 1 = 1  " +
            "<if test=\"areaIds != null\">and logic_area_id in <foreach collection=\"areaIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </if> " +
            "<if test=\"type != null\"> and type = #{type}</if> " +
            "order by sort desc " +
            "<if test=\"pageIndex != null and pageSize != null\">limit #{pageIndex},#{pageSize}</if> " +
            "</script>")
    @ResultMap("supplychainHomepageGoodsSetting")
    List<SupplychainHomepageGoodsSetting> getSupplychainHomepageGoodsSettingByAreaIdsAndType(@Param("areaIds")List<String> areaIds,@Param("type") Integer type, @Param("pageIndex")Integer pageIndex, @Param("pageSize")Integer pageSize);

    @Delete("<script> delete from supplychain_homepage_goods_setting where 1 = 1 and logic_area_id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script> ")
    void bacchDeleteHomeSettingBy(@Param("lists") List<String> ids);

    @Delete("<script> delete from supplychain_homepage_goods_setting where logic_area_id = #{areaId} and type = #{type}</script> ")
    void DeleteHomeSettingByAreaId(@Param("areaId") String areaId,@Param("type") Integer typeNum);


    @Insert("<script> insert into supplychain_homepage_goods_setting (" +
            " `id`,  " +
            " `type`,  " +
            " `logic_area_id`,  " +
            " `value`,  " +
            " `name`,  " +
            " `description`,  " +
            " `sort`,  " +
            " `creator_id` " +
            ") " +
            "<foreach collection=\"lists\" item=\"item\" separator=\"UNION ALL\"> " +
            "( select #{item.id},  " +
            "#{item.type},  " +
            "#{item.logicAreaId},  " +
            "#{item.value},  " +
            "#{item.name},  " +
            "#{item.description},  " +
            "#{item.sort},  " +
            "#{item.creatorId} FROM DUAL WHERE (SELECT COUNT(*) FROM  supplychain_homepage_goods_setting  " +
            "WHERE  logic_area_id = #{item.logicAreaId} and value = #{item.value} and type = #{item.type} ) <![CDATA[<]]> 1)" +
            "</foreach> </script>")
    void addSupplychainHomepageGoodsSettings(@Param("lists") List<SupplychainHomepageGoodsSetting> list);

    @Update("<script> INSERT INTO  supplychain_homepage_goods_setting" +
            "<foreach collection=\"lists\" item=\"item\" separator=\"UNION ALL\" > " +
            "(SELECT  #{item.id},#{item.type},#{item.logicAreaId},#{item.value},#{item.name}," +
            " #{item.description},#{item.sort}, #{item.createTime}, #{item.creatorId} FROM DUAL " +
            " WHERE (SELECT COUNT(*) FROM supplychain_homepage_goods_setting  WHERE logic_area_id = #{item.logicAreaId} and " +
            " value = #{item.value} and type = #{item.type}  ) <![CDATA[<]]> 1)" +
            " </foreach> "+
            "</script> ")
    void batchChangeHomeGoods(@Param("lists") List<SupplychainHomepageGoodsSetting> list);
}
