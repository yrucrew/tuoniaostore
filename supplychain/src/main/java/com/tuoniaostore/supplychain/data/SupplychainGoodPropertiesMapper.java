package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainGoodProperties;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainGoodPropertiesMapper {

    String column = "`id`, " +//
            "`good_id`, " +//商品ID
            "`type`, " +//属性类型
            "`k`, " +//属性标志
            "`v`"//具体属性值
            ;

    @Insert("insert into supplychain_good_properties (" +
            " `id`,  " +
            " `good_id`,  " +
            " `type`,  " +
            " `k`,  " +
            " `v` " +
            ")values(" +
            "#{id},  " +
            "#{goodId},  " +
            "#{type},  " +
            "#{k},  " +
            "#{v} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainGoodProperties(SupplychainGoodProperties supplychainGoodProperties);

    @Results(id = "supplychainGoodProperties", value = {
            @Result(property = "id", column = "id"), @Result(property = "goodId", column = "good_id"), @Result(property = "type", column = "type"), @Result(property = "k", column = "k"), @Result(property = "v", column = "v")})
    @Select("select " + column + " from supplychain_good_properties where id = #{id}")
    SupplychainGoodProperties getSupplychainGoodProperties(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_good_properties   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainGoodProperties")
    List<SupplychainGoodProperties> getSupplychainGoodPropertiess(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_good_properties   where    1=1 and status=0 </script>")
    @ResultMap("supplychainGoodProperties")
    List<SupplychainGoodProperties> getSupplychainGoodPropertiesAll();

    @Select("<script>select count(1) from supplychain_good_properties   where      1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainGoodPropertiesCount(@Param("status") int status, @Param("name") String name );

    @Update("update supplychain_good_properties  " +
            "set " +
            "`good_id` = #{goodId}  , " +
            "`type` = #{type}  , " +
            "`k` = #{k}  , " +
            "`v` = #{v}  " +
            " where id = #{id}")
    void changeSupplychainGoodProperties(SupplychainGoodProperties supplychainGoodProperties);

}
