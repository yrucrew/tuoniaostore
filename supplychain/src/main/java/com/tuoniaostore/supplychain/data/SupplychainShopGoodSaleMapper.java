package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainShopGoodSale;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-20 20:05:41
 */
@Component
@Mapper
public interface SupplychainShopGoodSaleMapper {

    String column = "`id`, " +//id
            "`good_tempalte_id`, " +//商品模板id
            "`shop_id`, " +//商店id
            "`good_name`, " +//商品名称
            "`good_barcode`, " +//条码
            "`good_unit`, " +//商品单位
            "`good_unit_id`, " +//商品单位id
            "`good_type_id`, " +//商品类型id
            "`good_type`, " +//商品类型
            "`sale_num`, " +//销售数量
            "`sale_money`, " +//销售金额
            "`sale_time`, " +//销售的时间
            "`create_time`"//
            ;

    @Insert("insert into supplychain_shop_good_sale (" +
            " `id`,  " +
            " `shop_id`,  " +
            " `good_tempalte_id`,  " +
            " `good_name`,  " +
            " `good_barcode`,  " +
            " `good_unit`,  " +
            " `good_unit_id`,  " +
            " `good_type_id`,  " +
            " `good_type`,  " +
            " `sale_num`,  " +
            " `sale_money`,  " +
            " `sale_time`  " +
            ")values(" +
            "#{id},  " +
            "#{shopId},  " +
            "#{goodTempalteId},  " +
            "#{goodName},  " +
            "#{goodBarcode},  " +
            "#{goodUnit},  " +
            "#{goodUnitId},  " +
            "#{goodTypeId},  " +
            "#{goodType},  " +
            "#{saleNum},  " +
            "#{saleMoney},  " +
            "#{saleTime}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainShopGoodSale(SupplychainShopGoodSale supplychainShopGoodSale);

    @Results(id = "supplychainShopGoodSale", value = {
            @Result(property = "id", column = "id"), @Result(property = "goodTempalteId", column = "good_tempalte_id"), @Result(property = "goodName", column = "good_name"), @Result(property = "goodBarcode", column = "good_barcode"), @Result(property = "goodUnit", column = "good_unit"), @Result(property = "goodUnitId", column = "good_unit_id"), @Result(property = "goodTypeId", column = "good_type_id"), @Result(property = "goodType", column = "good_type"), @Result(property = "saleNum", column = "sale_num"), @Result(property = "saleMoney", column = "sale_money"), @Result(property = "saleTime", column = "sale_time"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from supplychain_shop_good_sale where id = #{id}")
    SupplychainShopGoodSale getSupplychainShopGoodSale(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_shop_good_sale   where   retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainShopGoodSale")
    List<SupplychainShopGoodSale> getSupplychainShopGoodSales(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from supplychain_shop_good_sale   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("supplychainShopGoodSale")
    List<SupplychainShopGoodSale> getSupplychainShopGoodSaleAll();

    @Select("<script>select count(1) from supplychain_shop_good_sale   where     retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainShopGoodSaleCount(@Param("status") int status, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Update("<script>update supplychain_shop_good_sale  " +
            "set " +
            "`good_tempalte_id` = #{goodTempalteId}  , " +
            "`shop_id` = #{shopId}  , " +
            "`good_name` = #{goodName}  , " +
            "`good_barcode` = #{goodBarcode}  , " +
            "`good_unit` = #{goodUnit}  , " +
            "`good_unit_id` = #{goodUnitId}  , " +
            "`good_type_id` = #{goodTypeId}  , " +
            "`good_type` = #{goodType}  , " +
            "`sale_num` = #{saleNum}  , " +
            "`sale_money` = #{saleMoney}  , " +
            "`sale_time` = #{saleTime}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}</script>")
    void changeSupplychainShopGoodSale(SupplychainShopGoodSale supplychainShopGoodSale);

    @Insert("<script>insert into supplychain_shop_good_sale (" +
            " `id`,  " +
            " `good_tempalte_id`,  " +
            " `shop_id`,  " +
            " `good_name`,  " +
            " `good_barcode`,  " +
            " `good_unit`,  " +
            " `good_unit_id`,  " +
            " `good_type_id`,  " +
            " `good_type`,  " +
            " `sale_num`,  " +
            " `sale_money`,  " +
            " `sale_time`  " +
            ")values" +
            "<foreach collection=\"lists\" item=\"item\" separator=\",\"> " +
            "( #{item.id},  " +
            "#{item.goodTempalteId},  " +
            "#{item.shopId},  " +
            "#{item.goodName},  " +
            "#{item.goodBarcode},  " +
            "#{item.goodUnit},  " +
            "#{item.goodUnitId},  " +
            "#{item.goodTypeId},  " +
            "#{item.goodType},  " +
            "#{item.saleNum},  " +
            "#{item.saleMoney},  " +
            "#{item.saleTime} )  " +
            "</foreach>" +
            "</script>")
    @Options(useGeneratedKeys = true)
    void batchAddSupplychainShopGoodSales(@Param("lists") List<SupplychainShopGoodSale> supplychainShopGoodSales);

    @Select("<script> select  " + column + "  from supplychain_shop_good_sale  where shop_id = #{shopId} " +
            "<if test=\"typeId != null and typeId != ''\"> and good_type_id = #{typeId} </if>  " +
            "<if test=\"barcode != null and barcode != ''\"> and ( good_barcode like concat('%',#{barcode},'%') or good_name like concat('%',#{barcode},'%') ) </if>  " +
            "<if test=\"startTime != null and startTime != '' and endTime != null and endTime != '' \"> and  date_format(sale_time,'%Y-%m-%d') BETWEEN #{startTime} AND  #{endTime}</if>  " +
            " GROUP BY good_tempalte_id " +
            " <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when> " +
            " </script>")
    @ResultMap("supplychainShopGoodSale")
    List<SupplychainShopGoodSale> getSupplychainShopGoodSaleByList(@Param("shopId") String shopId, @Param("typeId")String typeId,@Param("barcode") String barcode,@Param("startTime") String startTime, @Param("endTime")String endTime,
                                                                   @Param("pageStartIndex")Integer pageStartIndex, @Param("pageSize")Integer pageSize);

    @Select("<script> select  count(*) from supplychain_shop_good_sale  where shop_id = #{shopId} " +
            "<if test=\"typeId != null and typeId != ''\"> and good_type_id = #{typeId} </if>  " +
            "<if test=\"barcode != null and barcode != ''\"> and good_barcode like concat('%',#{barcode},'%')  </if>  " +
            "<if test=\"startTime != null and startTime != '' and endTime != null and endTime != '' \"> and  date_format(sale_time,'%Y-%m-%d') BETWEEN #{startTime} AND  #{endTime}</if>  " +
            " </script>")
    int getSupplychainShopGoodSaleByListCount(@Param("shopId") String shopId, @Param("typeId")String typeId,@Param("barcode") String barcode,@Param("startTime") String startTime, @Param("endTime")String endTime);
}
