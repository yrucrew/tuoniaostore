package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainGoodBindRelationship;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainGoodBindRelationshipMapper {

    String column = "`id`, " +//
            "`user_id`, " +//通行证ID
            "`role_type`, " +//角色类型，如：供应商、采购员等
            "`good_type_id`, " +//商品类型ID
            "`create_time`"//创建时间
            ;

    @Insert("insert into supplychain_good_bind_relationship (" +
            " `id`,  " +
            " `user_id`,  " +
            " `role_type`,  " +
            " `good_type_id`  " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{roleType},  " +
            "#{goodTypeId}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainGoodBindRelationship(SupplychainGoodBindRelationship supplychainGoodBindRelationship);

    @Results(id = "supplychainGoodBindRelationship", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "roleType", column = "role_type"), @Result(property = "goodTypeId", column = "good_type_id"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from supplychain_good_bind_relationship where id = #{id}")
    SupplychainGoodBindRelationship getSupplychainGoodBindRelationship(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_good_bind_relationship   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainGoodBindRelationship")
    List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationships(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_good_bind_relationship   where    1=1   and status=0 </script>")
    @ResultMap("supplychainGoodBindRelationship")
    List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipAll();

    @Select("<script>select count(1) from supplychain_good_bind_relationship   where      1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainGoodBindRelationshipCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_good_bind_relationship  " +
            "set " +
            "`use_id` = #{useId}  , " +
            "`role_type` = #{roleType}  , " +
            "`item_type_id` = #{itemTypeId}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeSupplychainGoodBindRelationship(SupplychainGoodBindRelationship supplychainGoodBindRelationship);

    @Select("<script>select  " + column + "  from supplychain_good_bind_relationship   where user_id = #{userId}" +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when> </script>")
    @ResultMap("supplychainGoodBindRelationship")
    List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipByUserId(@Param("userId") String userId,@Param("pageIndex") Integer pageIndex, @Param("pageSize")Integer pageSize);

    @Select("<script>select  " + column + "  from supplychain_good_bind_relationship where user_id = #{userId} and good_type_id = #{typeId} limit 1</script>")
    @ResultMap("supplychainGoodBindRelationship")
    SupplychainGoodBindRelationship getSupplychainGoodBindRelationshipByUserIdAndTypeId(@Param("userId")String userId, @Param("typeId")String typeId);

    @Delete("delete from supplychain_good_bind_relationship where user_id = #{userId} and good_type_id = #{typeId} ")
    void delSupplychainGoodBindRelationshipByUserIdAndTypeId(@Param("userId")String userId, @Param("typeId")String typeId);

    @Insert("<script> insert into supplychain_good_bind_relationship (" +
            " `id`,  " +
            " `user_id`,  " +
            " `role_type`,  " +
            " `good_type_id`  " +
            ")values" +
            "<foreach collection=\"lists\" item=\"item\"  separator=\",\">(#{item.id}, #{item.userId},#{item.roleType},#{item.goodTypeId})</foreach> </script>")
    void addSupplychainGoodBindRelationships(@Param("lists") List<SupplychainGoodBindRelationship> supplychainGoodBindRelationship);

    @Select("<script>select  " + column + "  from supplychain_good_bind_relationship   where 1= 1  " +
            "<if test=\"lists != null\">and user_id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> </script>")
    @ResultMap("supplychainGoodBindRelationship")
    List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipByUserIds(@Param("lists") List<String> userIds);

    @Select("<script>select  " + column + "  from supplychain_good_bind_relationship   where 1= 1  " +
            "<if test=\"userIds != null and userIds.size > 0\">and user_id in <foreach collection=\"userIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            "<if test=\"typeIds != null and typeIds.size > 0\">and good_type_id in <foreach collection=\"typeIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            " </script>")
    @ResultMap("supplychainGoodBindRelationship")
    List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationShipsByUseridsAndTypeIds(@Param("userIds") List<String> userIds, @Param("typeIds") List<String> typeIds);

    @Delete("<script> delete from  supplychain_good_bind_relationship where user_id = #{userId} </script>")
    void clearSupplychainGoodBindRelationships(@Param("userId") String userId);

    @Insert("<script>insert into supplychain_good_bind_relationship (" +
            " `id`,  " +
            " `user_id`,  " +
            " `role_type`,  " +
            " `good_type_id`  " +
            ")values " +
            "<foreach collection=\"lists\" item=\"item\" separator=\",\">" +
            "(#{item.id},  " +
            "#{item.userId},  " +
            "#{item.roleType},  " +
            "#{item.goodTypeId})" +
            "</foreach></script>")
    @Options(useGeneratedKeys = true)
    void batchAddSupplychainGoodBindRelationships(@Param("lists") List<SupplychainGoodBindRelationship> entitys);
}
