package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageButtonSetting;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 首页按钮配置
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-04 14:40:15
 */
@Component
@Mapper
public interface SupplychainHomepageButtonSettingMapper {

    String column = "`id`," +
            "`type`, " +//按钮类型 0=商品分类ID; 1=特殊按钮(详细请参考value);
            "`name`, " +//按钮名称
            "`value`, " +//值 当type=0时此字段为商品分类ID 当type=1时此字段为按钮功能的描述
            "`img`, " +//图片url
            "`status`, " +//按钮状态 0=有效; 1=无效;
            "`sort`, " +//排序
            "`create_time`"//创建时间
            ;

    @Insert("insert into supplychain_homepage_button_setting (" +
            " `id`,  " +
            " `type`,  " +
            " `name`,  " +
            " `value`,  " +
            " `img`,  " +
            " `status`,  " +
            " `sort` " +
            ")values(" +
            "#{id},  " +
            "#{type},  " +
            "#{name},  " +
            "#{value},  " +
            "#{img},  " +
            "#{status},  " +
            "#{sort}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainHomepageButtonSetting(SupplychainHomepageButtonSetting supplychainHomepageButtonSetting);

    @Results(id = "supplychainHomepageButtonSetting", value = {
            @Result(property = "id", column = "id"), @Result(property = "type", column = "type"), @Result(property = "name", column = "name"), @Result(property = "value", column = "value"), @Result(property = "img", column = "img"), @Result(property = "status", column = "status"), @Result(property = "sort", column = "sort"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from supplychain_homepage_button_setting where id = #{id}")
    SupplychainHomepageButtonSetting getSupplychainHomepageButtonSetting(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_homepage_button_setting   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainHomepageButtonSetting")
    List<SupplychainHomepageButtonSetting> getSupplychainHomepageButtonSettings(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from supplychain_homepage_button_setting   where  1=1  and status=0 </script>")
    @ResultMap("supplychainHomepageButtonSetting")
    List<SupplychainHomepageButtonSetting> getSupplychainHomepageButtonSettingAll();

    @Select("<script>select count(1) from supplychain_homepage_button_setting   where   1 = 1   " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainHomepageButtonSettingCount(@Param("status") int status, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Update("<script>update supplychain_homepage_button_setting  " +
            "<set> " +
            "<if test=\"type != null\">type = #{type}  ,</if> " +
            "<if test=\"name != null and name != ''\">name = #{name}  , </if> " +
            "<if test=\"value != null\">value = #{value}  ,</if> " +
            "<if test=\"img != null and img != ''\">img = #{img}  , </if> " +
            "<if test=\"status != null\">status = #{status}  , </if> " +
            "<if test=\"sort != null\">sort = #{sort}  , </if> " +
            "</set> where id = #{id}</script>")
    void changeSupplychainHomepageButtonSetting(SupplychainHomepageButtonSetting supplychainHomepageButtonSetting);

    @Select("<script>select  " + column + "  from supplychain_homepage_button_setting where status = 0 and type = 1 order by sort desc </script>")
    @ResultMap("supplychainHomepageButtonSetting")
    List<SupplychainHomepageButtonSetting> getSupplychainHomepageButton();

    @Select("<script>select  " + column + "  from supplychain_homepage_button_setting where status = 0 and type = #{type} and  name = #{name} limit 1 </script>")
    @ResultMap("supplychainHomepageButtonSetting")
    SupplychainHomepageButtonSetting validateSupplychainHomepageButton(@Param("name") String name, @Param("type")Integer type);

    @Delete("<script> delete from  supplychain_homepage_button_setting where id in  <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></script>")
    void batchDeleteSupplychainHomepageButton(@Param("lists") List<String> stringList);
}
