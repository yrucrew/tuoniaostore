package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainGoodMapper {

    String column = "`id`, " +//
            "`good_template_id`, " +//对应的模版ID
            "`define_name`, " +//自定义商品名字 为空时使用模版名字
            "`define_image`, " +//自定义商品图片 为空时使用模版图片
            "`default_sort`, " +//默认排序 值越小 越靠前
            "`shop_id`, " +//归属仓库ID
            "`product_batches`, " +//商品批次
            "`batches_code`, " +//批次编码
            "`stock`, " +//当前库存(真实库存) 欠货时为负数
            "`pending_quantity`, " +//挂起数量(已支付但未出库) 订单取消时需回滚至库存
            "`warning_quantity`, " +//预警数量
            "`keep_quantity`, " +//保持数量
            "`oversold_quantity`, " +//用于展示的超卖数量(超卖+库存)
            "`minimum_sell_count`, " +//最低的销售数量
            "`allocation_quantity`, " +//调拨中的数量
            "`purchase_quantity`, " +//采购中的数量
            "`wait_allocation_quantity`, " +//待调拨数量(存在主仓且读取主仓库存时有效)
            "`status`, " +//当前状态 -1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效
            "`cost_price`, " +//成本价
            "`sell_price`, " +//一般销售价
            "`market_price`, " +//一般市场价
            "`discount_price`, " +//促销价
            "`sale_price`, " +//建议售价
            "`colleague_price`, " +//同行价
            "`member_price`, " +//会员价
            "`discount_type`, " +//促销类型 0-不参与促销 1-促销类型 2-特价类型
            "`restriction_quantity`, " +//限购数量 -1-不限购 0-不出售 其他正数表示限购数量
            "`beyond_controll`, " +//控制超量的情况 0：不可超出限制 1：超出时以最高价购买
            "`delivery_delay`, " +//延迟配送天数
            "`initial_sales`, " +//起始销量
            "`actual_sales`, " +//实际销量(展示时为：起始销量+实际销量) -- 总出库
            "`total_storage`, " +//总入库数量
            "`total_out_storage`, " +//总出库数量
            "`description`, " +//商品描述
            "`create_time`, " +//入库时间
            "`price_id`, " +//价格id
            "`good_return`, " +//是否可退
            "`starting_quantity`, " +//起订量
            "`good_type_id`" //类别id
            ;

    @Insert("insert into supplychain_good (" +
            " `id`,  " +
            " `good_template_id`,  " +
            " `define_name`,  " +
            " `define_image`,  " +
            " `default_sort`,  " +
            " `shop_id`,  " +
            " `product_batches`,  " +
            " `batches_code`,  " +
            " `stock`,  " +
            " `pending_quantity`,  " +
            " `warning_quantity`,  " +
            " `keep_quantity`,  " +
            " `oversold_quantity`,  " +
            " `minimum_sell_count`,  " +
            " `allocation_quantity`,  " +
            " `purchase_quantity`,  " +
            " `wait_allocation_quantity`,  " +
            " `status`,  " +
            " `cost_price`,  " +
            " `sell_price`,  " +
            " `market_price`,  " +
            " `discount_price`,  " +
            " `sale_price`,  " +
            " `colleague_price`,  " +
            " `member_price`,  " +
            " `discount_type`,  " +
            " `restriction_quantity`,  " +
            " `beyond_controll`,  " +
            " `delivery_delay`,  " +
            " `initial_sales`,  " +
            " `actual_sales`,  " +
            " `total_storage`,  " +
            " `total_out_storage`,  " +
            " `description`,  " +
            " `price_id`,  " +
            " `good_return`,  " +
            " `starting_quantity`,  " +
            " `good_type_id`" +
            ")values(" +
            "#{id},  " +
            "#{goodTemplateId},  " +
            "#{defineName},  " +
            "#{defineImage},  " +
            "#{defaultSort},  " +
            "#{shopId},  " +
            "#{productBatches},  " +
            "#{batchesCode},  " +
            "#{stock},  " +
            "#{pendingQuantity},  " +
            "#{warningQuantity},  " +
            "#{keepQuantity},  " +
            "#{oversoldQuantity},  " +
            "#{minimumSellCount},  " +
            "#{allocationQuantity},  " +
            "#{purchaseQuantity},  " +
            "#{waitAllocationQuantity},  " +
            "#{status},  " +
            "#{costPrice},  " +
            "#{sellPrice},  " +
            "#{marketPrice},  " +
            "#{discountPrice},  " +
            "#{salePrice},  " +
            "#{colleaguePrice},  " +
            "#{memberPrice},  " +
            "#{discountType},  " +
            "#{restrictionQuantity},  " +
            "#{beyondControll},  " +
            "#{deliveryDelay},  " +
            "#{initialSales},  " +
            "#{actualSales},  " +
            "#{totalStorage},  " +
            "#{totalOutStorage},  " +
            "#{description},  " +
            "#{priceId},  " +
            "#{goodReturn},  " +
            "#{startingQuantity},  " +
            "#{goodTypeId}" +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainGood(SupplychainGood supplychainGood);

    @Results(id = "supplychainGood", value = {
            @Result(property = "id", column = "id"), @Result(property = "goodTemplateId", column = "good_template_id"),
            @Result(property = "defineName", column = "define_name"), @Result(property = "defineImage", column = "define_image"),
            @Result(property = "defaultSort", column = "default_sort"), @Result(property = "shopId", column = "shop_id"),
            @Result(property = "productBatches", column = "product_batches"), @Result(property = "batchesCode", column = "batches_code"),
            @Result(property = "stock", column = "stock"), @Result(property = "pendingQuantity", column = "pending_quantity"),
            @Result(property = "warningQuantity", column = "warning_quantity"), @Result(property = "keepQuantity", column = "keep_quantity"),
            @Result(property = "oversoldQuantity", column = "oversold_quantity"), @Result(property = "minimumSellCount", column = "minimum_sell_count"),
            @Result(property = "allocationQuantity", column = "allocation_quantity"), @Result(property = "purchaseQuantity", column = "purchase_quantity"),
            @Result(property = "waitAllocationQuantity", column = "wait_allocation_quantity"), @Result(property = "status", column = "status"),
            @Result(property = "costPrice", column = "cost_price"), @Result(property = "sellPrice", column = "sell_price"),
            @Result(property = "marketPrice", column = "market_price"), @Result(property = "discountPrice", column = "discount_price"),
            @Result(property = "salePrice", column = "sale_price"), @Result(property = "colleaguePrice", column = "colleague_price"),@Result(property = "memberPrice", column = "member_price"),
            @Result(property = "discountType", column = "discount_type"), @Result(property = "restrictionQuantity", column = "restriction_quantity"),
            @Result(property = "beyondControll", column = "beyond_controll"), @Result(property = "deliveryDelay", column = "delivery_delay"),
            @Result(property = "initialSales", column = "initial_sales"), @Result(property = "actualSales", column = "actual_sales"),
            @Result(property = "totalStorage", column = "total_storage"), @Result(property = "totalOutStorage", column = "total_out_storage"),
            @Result(property = "description", column = "description"), @Result(property = "createTime", column = "create_time"),
            @Result(property = "goodReturn", column = "good_return"), @Result(property = "goodTypeId", column = "good_type_id"),
            @Result(property = "priceId", column = "price_id"),
            @Result(property = "startingQuantity", column = "starting_quantity"),
            @Result(property = "goodTypeId", column = "good_type_id"),
            @Result(property = "relationItemId", column = "relation_item_id"),@Result(property = "priceId", column = "price_id") })
    @Select("select " + column + " from supplychain_good where id = #{id}")
    SupplychainGood getSupplychainGood(@Param("id") String id);


    @ResultMap("supplychainGood")
    @Select("<script>select " + column + " from supplychain_good where id = #{id} <if test=\"status != null\"> and status = #{status} limit 1 </if> </script>")
    SupplychainGood getSupplychainGoodByIdAndStatus(@Param("id") String id, @Param("status")Integer status);

    @Select("<script>select  " + column + "  from supplychain_good   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by default_sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getSupplychainGoods(@Param("status") int status, @Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_good   where    1=1  and status=0 " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getSupplychainGoodAllByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    @Select("<script>select  " + column + "  from supplychain_good   where    1=1  and status=0  </script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getSupplychainGoodAll();

    @Select("<script>select count(1) from supplychain_good   where     1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainGoodCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_good  " +
            "set " +
            "`good_template_id` = #{goodTemplateId}  , " +
            "`define_name` = #{defineName}  , " +
            "`define_image` = #{defineImage}  , " +
            "`default_sort` = #{defaultSort}  , " +
            "`shop_id` = #{shopId}  , " +
            "`product_batches` = #{productBatches}  , " +
            "`batches_code` = #{batchesCode}  , " +
            "`stock` = #{stock}  , " +
            "`pending_quantity` = #{pendingQuantity}  , " +
            "`warning_quantity` = #{warningQuantity}  , " +
            "`keep_quantity` = #{keepQuantity}  , " +
            "`oversold_quantity` = #{oversoldQuantity}  , " +
            "`minimum_sell_count` = #{minimumSellCount}  , " +
            "`allocation_quantity` = #{allocationQuantity}  , " +
            "`purchase_quantity` = #{purchaseQuantity}  , " +
            "`wait_allocation_quantity` = #{waitAllocationQuantity}  , " +
            "`status` = #{status}  , " +
            "`cost_price` = #{costPrice}  , " +
            "`change_time` = unix_timestamp() , " +
            "`sell_price` = #{sellPrice}  , " +
            "`market_price` = #{marketPrice}  , " +
            "`discount_price` = #{discountPrice}  , " +
            "`discount_type` = #{discountType}  , " +
            "`restriction_quantity` = #{restrictionQuantity}  , " +
            "`beyond_controll` = #{beyondControll}  , " +
            "`delivery_delay` = #{deliveryDelay}  , " +
            "`initial_sales` = #{initialSales}  , " +
            "`actual_sales` = #{actualSales}  , " +
            "`total_storage` = #{totalStorage}  , " +
            "`total_out_storage` = #{totalOutStorage}  , " +
            "`description` = #{description}  , " +
            "`create_time` = #{createTime}  , " +
            "`relation_item_id` = #{relationItemId},  " +
            "`price_id` = #{priceId},  " +
            "`good_return` = #{goodReturn},  " +
            "`starting_quantity` = #{startingQuantity},  " +
            "`good_type` = #{goodTpeId}  " +
            " where id = #{id}")
    void changeSupplychainGood(SupplychainGood supplychainGood);

    @Select("<script>select  " + column + "  from supplychain_good  where  1=1 and shop_id = #{shopId} and good_type_id=#{typeId}  and status in (0,1)" +
            "  order by default_sort desc   " +
            " <when test=\"limitStart != null and limitEnd != null\">" +
            "  limit #{limitStart}, #{limitEnd}" +
            "</when></script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getSupplychainGoodByShopIdAndTypeId(@Param("shopId") String shopId,@Param("typeId") String typeId,
                                                              @Param("limitStart") Integer limitStart,@Param("limitEnd") Integer limitEnd);
    @Select("<script>" +
            "select  " + column + "  from supplychain_good  where    shop_id = #{shopId}   and status in (0,1)" +//and good_type_id=#{typeId}
            "  order by change_time desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when>" +
            "</script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getSupplychainGoodPosList(@Param("shopId") String shopId,@Param("typeId") String typeId, @Param("pageIndex") int pageIndex, @Param("pageSize")int pageSize);


    @Select("<script>select  " + column + "  from supplychain_good  where status in (0,1) and shop_id = #{shopId} " +
            "<if test=\"lists != null and lists.size > 0\">and good_type_id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            "  order by default_sort desc   " +
            " <when test=\"limitStart != null and limitEnd != null\">" +
            "  limit #{limitStart}, #{limitEnd}" +
            "</when>" +
            "</script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getSupplychainGoodByShopIdAndTypes(@Param("shopId") String shopId, @Param("lists") List<String> typeIds,@Param("limitStart") Integer limitStart,@Param("limitEnd") Integer limitEnd);

    @Update("<script>update supplychain_good set" +
            " cost_price = #{costPrice}," +
            "`change_time` = unix_timestamp() , " +
            "sell_price = #{sellPrice} ,stock = #{stock} ,description = #{description} where id = #{id} </script>")
    void changeSupplychainGoodById(SupplychainGood supplychainGood);

    @Update("<script>update supplychain_good set" +
            "  status = #{status} ," +
            "`change_time` = unix_timestamp()  " +
            " where id = #{id} </script>")
    void downSupplychainGoodByGoodId(@Param("id") String id,@Param("status") int status);

    @Update("<script>update supplychain_good set " +
            "`change_time` = unix_timestamp() , " +
            "status = #{status}" +
            " where id in <foreach collection=\"ids\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    void batchDownSupplychainGoodByGoodId(@Param("ids")List<String> ids,@Param("status") int status);

    @Update("update supplychain_good  " +
            "set " +
            "`good_template_id` = #{goodTemplateId}  , " +
            "`define_name` = #{defineName}  , " +
            "`define_image` = #{defineImage}  , " +
            "`default_sort` = #{defaultSort}  , " +
            "`stock` = #{stock}  , " +
            "`status` = #{status}  , " +
            "`cost_price` = #{costPrice}  , " +
            "`sell_price` = #{sellPrice}  , " +
            "`description` = #{description}  , " +
            "`price_id` = #{priceId} ," +
            "`change_time` = unix_timestamp() , " +
            "`starting_quantity` = #{startingQuantity} , " +
            "`good_type_id` = #{goodTypeId} " +
            " where id = #{id}")
    void updateChainGood(SupplychainGood supplychainGood);

    @Select("<script> select " + column + " from supplychain_good where  1 = 1 " +
            "<if test=\"ids != null and ids.size > 0\">and id in <foreach collection=\"ids\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            "<if test=\" goodType != null \">and good_type_id = #{goodType}</if> " +
            "<when test=\"goodPriceSort != null and goodPriceSort == '1'.toString()\">order by sell_price asc</when> " +
            "<when test=\"goodPriceSort != null and goodPriceSort == '2'.toString()\">order by sell_price desc</when> " +//价格排序 默认0 1升序 2降序
            "</script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getSupplychainGoodById(@Param("ids") List<String> ids,@Param("goodType") String goodType,@Param("goodPriceSort") Integer goodPriceSort);

    @Select("<script> select " + column + " from supplychain_good where status != -1 and id in" +
            "<foreach collection=\"ids\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach>" +
            " order by status desc " +
            "<when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when>  "+
            " </script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getSupplychainGoodByIdPage(@Param("ids") List<String> ids,@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);


    @Select("<script> select " + column + " from supplychain_good where 1 = 1 and id in" +
            "<foreach collection=\"ids\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach>" +
            " order by status desc " +
            "<when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when>  "+
            " </script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getSupplychainGoodByIdForShopCart(@Param("ids") List<String> ids,@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    @Update("<script>update supplychain_good  " +
            "set " +
            "`change_time` = unix_timestamp() , " +
            "`status` = -1 " +
            " where shop_id = #{shopId} " +
            "<if test=\"typeIds != null and typeIds.size() > 0 \"> and good_type_id in <foreach collection=\"typeIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>" +
            "</script>")
    void delSupplychainGoodByShopIdAndTypeId(@Param("shopId") String shopId, @Param("typeIds")List<String> typeId);

    @Update("update supplychain_good  " +
            "set " +
            "`change_time` = unix_timestamp() , " +
            "`status` = -1 " +
            " where id = #{id}")
    void delSupplychainGoodById(@Param("id")String id);

    @Select("<script>select  " + column + "  from supplychain_good where shop_id = #{shopId} " +
            "and define_name like concat('%', #{goodName}, '%')  " +
            "and status in (0,1) order by default_sort desc  " +
            "<when test=\"limitStart != null and limitStart != null \"> limit #{limitStart}, #{limitEnd} </when></script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> searchSupplychainGoodByShopIdAndGoodName(@Param("shopId")String shopId, @Param("goodName")String goodName,@Param("limitStart")Integer limitStart,@Param("limitEnd")Integer limitEnd);

    @Select("<script>select  " + column + "  from supplychain_good where shop_id = #{shopId} " +
            "<if test=\"lists != null\">and price_id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>" +
            "and status in (0,1) order by default_sort desc  " +
            "<when test=\"limitStart != null and limitStart != null \"> limit #{limitStart}, #{limitEnd} </when></script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> searchSupplychainGoodByShopIdAndPriceId(@Param("shopId")String shopId,@Param("lists") List<String> priceId, @Param("limitStart")Integer limitStart, @Param("limitEnd")Integer limitEnd);

    @Select("<script>select  " + column + "  from supplychain_good  where  1=1 and shop_id = #{shopId}" +
            "<if test=\"typeId != null\">and good_type_id=#{typeId}</if>  and status = 1 " +
            "<when test=\"goodPriceSort != null and goodPriceSort == '1'.toString()\">order by sell_price asc</when> " +
            "<when test=\"goodPriceSort != null and goodPriceSort == '2'.toString()\">order by sell_price desc</when> " +//价格排序 默认0 1升序 2降序
            "<when test=\"limitStart != null and limitEnd != null\">" +
            "  limit #{limitStart}, #{limitEnd} " +
            "</when> " +
            "</script> ")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getSupplychainGoodForHomePage(@Param("shopId")String shopId, @Param("typeId")String typeId, @Param("goodPriceSort")Integer goodPriceSort,
                                                        @Param("limitStart")Integer limitStart, @Param("limitEnd")Integer limitEnd);

    @Select("<script>select  " + column + "  from supplychain_good  where  1=1 and shop_id = #{shopId}  and status in (0,1)" +
            "<if test=\"lists != null\">and good_template_id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>"+
            " <when test=\"pageStartIndex != null and pageSize != null\">" +
            "  limit #{pageStartIndex}, #{pageSize}" +
            "</when>" +
            "</script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getSupplychainGoodByShopIdAndTempateIds(@Param("shopId") String shopId,@Param("lists")  List<String> templateIds, @Param("pageStartIndex") Integer pageStartIndex, @Param("pageSize") Integer pageSize);

    @Select("<script>select  " + column + "  from supplychain_good  where  1=1 " +
            "<if test=\"shopId != null\">and shop_id = #{shopId}</if>  " +
            "and status in (0,1) and define_name like  concat('%', #{goodNameOrBrandName}, '%') " +
            " <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when> " +
            "</script> ")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getsupplychainGoodByGoodName(@Param("goodNameOrBrandName") String goodNameOrBrandName, @Param("shopId") String shopId, @Param("pageStartIndex") Integer pageStartIndex,
                                                       @Param("pageSize") Integer pageSize);

    @Select("<script>select  " + column + "  from supplychain_good  where  1=1 " +
            "<if test=\"shopId != null\">and shop_id = #{shopId}</if>  " +
            "and status = #{status} and define_name like  concat('%', #{goodNameOrBrandName}, '%') " +
            " <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when> " +
            "</script> ")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getsupplychainGoodByGoodNameAndStatus(@Param("goodNameOrBrandName") String goodNameOrBrandName, @Param("shopId") String shopId, @Param("status") int status, @Param("pageStartIndex") Integer pageStartIndex,
                                                                @Param("pageSize") Integer pageSize);

    @Select("<script>select  " + column + "  from supplychain_good  where  1=1 " +
            "<if test=\"shopId != null \">and shop_id = #{shopId}</if>  " +
            "<when test=\"status == null\">and status in (0,1)</when> " +
            "<when test=\"status != null and status != ''\">and status = #{status}</when> " +
            " <if test=\"lists != null and lists.size() > 0 \"> " +
            "  and price_id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> " +
            "</if> " +
            " <if test=\"typeIds != null and typeIds.size() > 0\"> " +
            "  and good_type_id in <foreach collection=\"typeIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> " +
            "</if> " +
            " order by default_sort desc <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when> " +
            "</script> ")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getsupplychaingoodByShopIdAndPriceId(@Param("shopId") String shopId, @Param("lists") List<String> priceId,@Param("typeIds") List<String>typeIds,
                                                               @Param("status")Integer status,@Param("pageStartIndex") Integer pageStartIndex,@Param("pageSize") Integer pageSize);

    @Select("<script>select  count(1)  from supplychain_good  where  1=1 " +
            "<if test=\"shopId != null \">and shop_id = #{shopId}</if>  " +
            "<when test=\"status == null\">and status in (0,1)</when> " +
            "<when test=\"status != null and status != ''\">and status = #{status}</when> " +
            " <if test=\"lists != null and lists.size() > 0 \"> " +
            "  and price_id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> " +
            "</if> " +
            " <if test=\"typeIds != null and typeIds.size() > 0\"> " +
            "  and good_type_id in <foreach collection=\"typeIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> " +
            "</if> " +
            " order by default_sort desc " +
            "</script> ")
    int getCountSupplychaingoodByShopIdAndPriceId(@Param("shopId") String shopId, @Param("lists") List<String> priceId,@Param("typeIds") List<String>typeIds,
                                                  @Param("status")Integer status);


    @Select("<script>select  " + column + "  from supplychain_good  where  1=1 " +
            "<if test=\"shopId != null \">and shop_id in <foreach collection=\"shopIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>  " +
            "and status in (0,1) " +
            " <if test=\"lists != null and lists.size() > 0 \"> " +
            "  and price_id in <foreach collection=\"priceIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> " +
            "</if> " +
            " <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when> " +
            "</script> ")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getsupplychaingoodByShopIdAndPriceId2(@Param("shopIds") List<String> shopId, @Param("priceIds") List<String> priceId,@Param("pageStartIndex") Integer pageStartIndex,
                                                                @Param("pageSize") Integer pageSize);

    @Insert("<script> insert into supplychain_good " +
            " (`id`,  " +
            " `good_template_id`,  " +
            " `define_name`,  " +
            " `define_image`,  " +
            " `shop_id`,  " +
            " `batches_code`,  " +
            " `cost_price`,  " +
            " `sell_price`,  " +
            " `market_price`,  " +
            " `discount_price`,  " +
            " `sale_price`,  " +
            " `colleague_price`,  " +
            " `member_price`,  " +
            " `discount_type`,  " +
            " `description`,  " +
            " `price_id`,  " +
            " `good_return`,  " +
            " `starting_quantity`,  " +
            " `good_type_id` " +
            ") " +
            "<foreach collection=\"lists\" item=\"item\" separator=\"UNION ALL\"> " +
            " (SELECT #{item.id},#{item.goodTemplateId},#{item.defineName},#{item.defineImage}, #{item.shopId},#{item.batchesCode},#{item.costPrice},#{item.sellPrice},#{item.salePrice},#{item.colleaguePrice}, "+
            "#{item.memberPrice}, #{item.marketPrice},#{item.discountPrice},#{item.discountType},#{item.description},#{item.priceId}, #{item.goodReturn},#{item.startingQuantity},#{item.goodTypeId}  FROM DUAL WHERE (SELECT COUNT(*) FROM  supplychain_good " +
            " WHERE good_template_id = #{item.goodTemplateId} and shop_id = #{item.shopId} and price_Id = #{item.priceId} and  status <![CDATA[!=]]> -1 ) <![CDATA[<]]> 1 ) " +
            "</foreach> </script>")
    void addSupplychainGoods(@Param("lists") List<SupplychainGood> supplychainGoodSaveList);


    @Select("<script>select  " + column + "  from supplychain_good  where  1=1 and status in (0,1)" +
            "<if test=\"shopIds != null\">and shop_id in <foreach collection=\"shopIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if>"+
            " <when test=\"pageStartIndex != null and pageSize != null\">" +
            "  limit #{pageStartIndex}, #{pageSize}" +
            "</when>" +
            "</script>")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getsupplychaingoodByShopIds(@Param("shopIds") List<String> shopIds, @Param("pageStartIndex")Integer pageStartIndex, @Param("pageSize")Integer pageSize);

    @Select("<script>select  " + column + "  from supplychain_good  where  1=1 " +
            "<when test=\"status == null\">and status in (0,1)</when> " +
            "<when test=\"status != null\">and status = #{status}</when> " +
            "<if test=\"goodName != null\">and define_name like concat('%',#{goodName},'%')</if> "+
            "<if test=\"shopIds != null and shopIds.size > 0\">and shop_id in <foreach collection=\"shopIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> "+
            "<if test=\"goodType != null and goodType.size > 0\">and good_type_id in <foreach collection=\"goodType\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> "+
            " order by default_sort desc <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when> " +
            "</script> ")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getsupplychainGoodByshopIdAndGoodName(@Param("goodName")String goodName,@Param("shopIds") List<String> shopIds,@Param("goodType") List<String>goodType,@Param("status")Integer status, @Param("pageStartIndex")Integer pageStartIndex, @Param("pageSize")Integer pageSize);

    @Select("<script>select  " + column + "  from supplychain_good  where  1=1 " +
            "<when test=\"status == null\">and status in (0,1)</when> " +
            "<when test=\"status != null\">and status = #{status}</when> " +
            "<if test=\"goodName != null\">and define_name like concat('%',#{goodName},'%')</if> "+
            "<if test=\"shopIds != null and shopIds.size > 0\">and shop_id in <foreach collection=\"shopIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> "+
            "<if test=\"goodType != null and goodType.size > 0\">and good_type_id in <foreach collection=\"goodType\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> "+
            "<if test=\"notPriceIn != null and notPriceIn.size > 0\">and price_id not in <foreach collection=\"notPriceIn\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> "+
            " order by default_sort desc <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when> " +
            "</script> ")
    @ResultMap("supplychainGood")
    List<SupplychainGood> getsupplychainGoodByshopIdAndGoodNameAndNotPrice(@Param("goodName")String goodName,@Param("shopIds") List<String> shopIds,@Param("goodType") List<String>goodType,@Param("notPriceIn") List<String> notPriceIn, @Param("status")Integer status, @Param("pageStartIndex")Integer pageStartIndex, @Param("pageSize")Integer pageSize);

    @Select("<script>select  count(1)  from supplychain_good  where  1=1 " +
            "<when test=\"status == null\">and status in (0,1)</when> " +
            "<when test=\"status != null\">and status = #{status}</when> " +
            "<if test=\"goodName != null\">and define_name like concat('%',#{goodName},'%')</if> "+
            "<if test=\"shopIds != null and shopIds.size > 0\">and shop_id in <foreach collection=\"shopIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> "+
            "<if test=\"goodType != null and goodType.size > 0\">and good_type_id in <foreach collection=\"goodType\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> "+
            "<if test=\"notPriceIn != null and notPriceIn.size > 0\">and price_id not in  <foreach collection=\"notPriceIn\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> "+
            " order by default_sort desc " +
            "</script> ")
    int getCountSupplychainGoodByshopIdAndGoodNameAndNotPrice(@Param("goodName")String goodName,@Param("shopIds") List<String> shopIds,@Param("goodType") List<String>goodType,@Param("notPriceIn") List<String> notPriceIn, @Param("status")Integer status);

    @Update("<script>update supplychain_good " +
            "set " +
            "`change_time` = unix_timestamp() , " +
            "stock = case <foreach collection=\"lists\" item=\"item\" > " +
            "when id = #{item.id} then #{item.stock}</foreach> end where id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\"> #{item.id}</foreach></script>")
    void updateChainGoodList(@Param("lists")List<SupplychainGood> goodList1);


    @Update("<script>update supplychain_good set" +
            "`change_time` = unix_timestamp() , " +
            "<when test=\"isAdd != null and isAdd == 1\">stock  = stock + #{stock} </when> " +
            "<when test=\"isAdd != null and isAdd == 0\">stock  = stock - #{stock} </when> " +
            "where id = #{id} </script>")
    void changSupplychainGoodByGoodId(@Param("id") String id,@Param("stock") int stock,@Param("isAdd") int isAdd);

    @Update("<script>update supplychain_good set" +
            "`change_time` = unix_timestamp() , " +
            "stock  = #{stock} " +
            "where id = #{id} </script>")
    void changSupplychainGoodByStock(@Param("id") String id,@Param("stock") int stock);


    @Select("<script>select  count(*)  from supplychain_good  where  1=1 " +
            "<when test=\"status == null\">and status in (0,1)</when> " +
            "<when test=\"status != null\">and status = #{status}</when> " +
            "<if test=\"goodName != null\">and define_name like concat('%',#{goodName},'%')</if> "+
            "<if test=\"shopIds != null and shopIds.size > 0\">and shop_id in <foreach collection=\"shopIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> "+
            "<if test=\"goodType != null and goodType.size > 0\">and good_type_id in <foreach collection=\"goodType\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> "+
            "<if test=\"priceIds != null and priceIds.size > 0\">and price_id in <foreach collection=\"priceIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> "+
            "</script> ")
    Integer getCountSupplychainGoodByParam(@Param("goodName") String goodNameOrBarcode, @Param("goodType") List<String> typeIds,
                                           @Param("shopIds") List<String> shopList, @Param("priceIds") List<String> priceIds, @Param("status") Integer status);

    @Update("<script>update supplychain_good set" +
            " `change_time` = unix_timestamp() , " +
            " status = -1 where id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    void batchDeleteSupplychainGoodByGoodId(@Param("lists") List<String> strings);

    @Update("<script>update supplychain_good  " +
            "<set> " +
            "`change_time` = unix_timestamp() , " +
            "<when test=\"map.defaultSort != null\">" +
            "`default_sort` = #{map.defaultSort}  , " +
            "</when> "+
            "<when test=\"map.stock != null\">" +
                "`stock` = #{map.stock}  , " +
            "</when> "+
            "<when test=\"map.status != null\">" +
                "`status` = #{map.status}  , " +
            "</when> "+
            "<when test=\"map.costPrice != null\">" +
                "`cost_price` = #{map.costPrice}  , " +
            "</when> "+
            "<when test=\"map.sellPrice != null\">" +
                "`sell_price` = #{map.sellPrice}  , " +
            "</when> "+
            "<when test=\"map.memberPrice != null\">" +
                "`member_price` = #{map.memberPrice}  , " +
            "</when> "+
            "<when test=\"map.marketPrice != null\">" +
                "`market_price` = #{map.marketPrice}  , " +
            "</when> "+
            "<when test=\"map.salePrice != null\">" +
                "`sale_price` = #{map.salePrice}  , " +
            "</when> "+
            "<when test=\"map.discountPrice != null\">" +
                "`discount_price` = #{map.discountPrice}  , " +
            "</when> "+
            "<when test=\"map.startingQuantity != null\">" +
                "`starting_quantity` = #{map.startingQuantity}  , " +
            "</when> "+
            "<when test=\"map.colleaguePrice != null\">" +
                "`colleague_price` = #{map.colleaguePrice}  , " +
            "</when> "+
            "</set>"+
            " where id = #{map.id}</script>")
    void updateChainGoods(@Param("map") Map<String, Object> paramMap);

    @Select("<script> select "+column+ " from supplychain_good where shop_id = #{shopId} and price_id = #{priceId} and status in (0,1) </script>")
    @ResultMap("supplychainGood")
    SupplychainGood getSupplychainGoodByShopIdAndPriceId(@Param("shopId") String shopId,@Param("priceId") String priceId);

    @Update("<script> update supplychain_good set status = #{map.status} where 1 = 1 " +
            " <if test=\"map.goodId != null and map.goodId != ''\">and good_template_id = #{map.goodId} </if>  " +
            " <if test=\"map.priceId != null and map.priceId != ''\">and price_id = #{map.priceId} </if>  " +
            " </script>")
    void updateSupplychainGoodStatus(@Param("map") Map<String, Object> map1);

    @Delete("<script>DELETE FROM supplychain_good WHERE shop_id = #{shopId}</script>")
    void clearSupplychainGoodsByShopId(String shopId);

}
