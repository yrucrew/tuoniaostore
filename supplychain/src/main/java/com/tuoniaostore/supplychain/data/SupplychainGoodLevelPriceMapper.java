package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainGoodLevelPrice;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainGoodLevelPriceMapper {

    String column = "`id`, " +//
            "`role_id`, " +//角色ID(仓库ID)
            "`relation_type`, " +//关联类型 如：0--商品 1--商品模版
            "`relation_id`, " +//关联实体ID
            "`discount_type`, " +//优惠类型，如：0--设定价格 1--折扣 2--按原价减免
            "`level_price`, " +//对应优惠类型的具体值
            "`level_template_id`, " +//匹配的等级模版
            "`create_time`"//
            ;

    @Insert("insert into supplychain_good_level_price (" +
            " `id`,  " +
            " `role_id`,  " +
            " `relation_type`,  " +
            " `relation_id`,  " +
            " `discount_type`,  " +
            " `level_price`,  " +
            " `level_template_id`,  " +
            ")values(" +
            "#{id},  " +
            "#{roleId},  " +
            "#{relationType},  " +
            "#{relationId},  " +
            "#{discountType},  " +
            "#{levelPrice},  " +
            "#{levelTemplateId},  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainGoodLevelPrice(SupplychainGoodLevelPrice supplychainGoodLevelPrice);

    @Results(id = "supplychainGoodLevelPrice", value = {
            @Result(property = "id", column = "id"), @Result(property = "roleId", column = "role_id"), @Result(property = "relationType", column = "relation_type"), @Result(property = "relationId", column = "relation_id"), @Result(property = "discountType", column = "discount_type"), @Result(property = "levelPrice", column = "level_price"), @Result(property = "levelTemplateId", column = "level_template_id"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from supplychain_good_level_price where id = #{id}")
    SupplychainGoodLevelPrice getSupplychainGoodLevelPrice(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_good_level_price   where    1=1 " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainGoodLevelPrice")
    List<SupplychainGoodLevelPrice> getSupplychainGoodLevelPrices(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name );

    @Select("<script>select  " + column + "  from supplychain_good_level_price   where    status=0 </script>")
    @ResultMap("supplychainGoodLevelPrice")
    List<SupplychainGoodLevelPrice> getSupplychainGoodLevelPriceAll();

    @Select("<script>select count(1) from supplychain_good_level_price   where     1=1" +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainGoodLevelPriceCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_good_level_price  " +
            "set " +
            "`role_id` = #{roleId}  , " +
            "`relation_type` = #{relationType}  , " +
            "`relation_id` = #{relationId}  , " +
            "`discount_type` = #{discountType}  , " +
            "`level_price` = #{levelPrice}  , " +
            "`level_template_id` = #{levelTemplateId}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeSupplychainGoodLevelPrice(SupplychainGoodLevelPrice supplychainGoodLevelPrice);

}
