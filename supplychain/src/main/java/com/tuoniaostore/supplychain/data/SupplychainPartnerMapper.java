package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainPartner;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainPartnerMapper {

    String column = "`id`, " +//
            "`title`, " +//合作方标题 如网址
            "`company_name`, " +//公司名字
            "`type`, " +//合作类型
            "`proxy_type`, " +//代理属性类型
            "`status`, " +//合作状态 0-停止合作 1-合作中
            "`create_time`"//建立时间
            ;

    @Insert("insert into supplychain_partner (" +
            " `id`,  " +
            " `title`,  " +
            " `company_name`,  " +
            " `type`,  " +
            " `proxy_type`,  " +
            " `status`,  " +
            ")values(" +
            "#{id},  " +
            "#{title},  " +
            "#{companyName},  " +
            "#{type},  " +
            "#{proxyType},  " +
            "#{status},  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainPartner(SupplychainPartner supplychainPartner);

    @Results(id = "supplychainPartner", value = {
            @Result(property = "id", column = "id"), @Result(property = "title", column = "title"), @Result(property = "companyName", column = "company_name"), @Result(property = "type", column = "type"), @Result(property = "proxyType", column = "proxy_type"), @Result(property = "status", column = "status"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from supplychain_partner where id = #{id}")
    SupplychainPartner getSupplychainPartner(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_partner   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainPartner")
    List<SupplychainPartner> getSupplychainPartners(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_partner   where    1=1 and status=0 </script>")
    @ResultMap("supplychainPartner")
    List<SupplychainPartner> getSupplychainPartnerAll();

    @Select("<script>select count(1) from supplychain_partner   where     1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainPartnerCount(@Param("status") int status, @Param("name") String name );

    @Update("update supplychain_partner  " +
            "set " +
            "`title` = #{title}  , " +
            "`company_name` = #{companyName}  , " +
            "`type` = #{type}  , " +
            "`proxy_type` = #{proxyType}  , " +
            "`status` = #{status}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeSupplychainPartner(SupplychainPartner supplychainPartner);

}
