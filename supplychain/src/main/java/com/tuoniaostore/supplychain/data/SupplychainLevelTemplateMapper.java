package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainLevelTemplate;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainLevelTemplateMapper {

    String column = "`id`, " +//
            "`level_value`, " +//等级值
            "`define_parameter`, " +//其他内容
            "`status`, " +//是否启用 0--无效 1--启动
            "`create_time`"//建立的时间
            ;

    @Insert("insert into supplychain_level_template (" +
            " `id`,  " +
            " `level_value`,  " +
            " `define_parameter`,  " +
            " `status`,  " +
            ")values(" +
            "#{id},  " +
            "#{levelValue},  " +
            "#{defineParameter},  " +
            "#{status},  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainLevelTemplate(SupplychainLevelTemplate supplychainLevelTemplate);

    @Results(id = "supplychainLevelTemplate", value = {
            @Result(property = "id", column = "id"), @Result(property = "levelValue", column = "level_value"), @Result(property = "defineParameter", column = "define_parameter"), @Result(property = "status", column = "status"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from supplychain_level_template where id = #{id}")
    SupplychainLevelTemplate getSupplychainLevelTemplate(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_level_template   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainLevelTemplate")
    List<SupplychainLevelTemplate> getSupplychainLevelTemplates(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_level_template   where    1=1  and status=0 </script>")
    @ResultMap("supplychainLevelTemplate")
    List<SupplychainLevelTemplate> getSupplychainLevelTemplateAll();

    @Select("<script>select count(1) from supplychain_level_template   where     1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainLevelTemplateCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_level_template  " +
            "set " +
            "`level_value` = #{levelValue}  , " +
            "`define_parameter` = #{defineParameter}  , " +
            "`status` = #{status}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeSupplychainLevelTemplate(SupplychainLevelTemplate supplychainLevelTemplate);

}
