package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainTimeTaskLock;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainTimeTaskLockMapper {

    String column = "`id`, " +//
            "`title`, " +//标题
            "`type`, " +//类型
            "`happen_time`, " +//发生的时间 由逻辑决定
            "`winner_address`"//获得者地址
            ;

    @Insert("insert into supplychain_time_task_lock (" +
            " `id`,  " +
            " `title`,  " +
            " `type`,  " +
            " `happen_time`,  " +
            " `winner_address` " +
            ")values(" +
            "#{id},  " +
            "#{title},  " +
            "#{type},  " +
            "#{happenTime},  " +
            "#{winnerAddress} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainTimeTaskLock(SupplychainTimeTaskLock supplychainTimeTaskLock);

    @Results(id = "supplychainTimeTaskLock", value = {
            @Result(property = "id", column = "id"), @Result(property = "title", column = "title"), @Result(property = "type", column = "type"), @Result(property = "happenTime", column = "happen_time"), @Result(property = "winnerAddress", column = "winner_address")})
    @Select("select " + column + " from supplychain_time_task_lock where id = #{id}")
    SupplychainTimeTaskLock getSupplychainTimeTaskLock(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_time_task_lock   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainTimeTaskLock")
    List<SupplychainTimeTaskLock> getSupplychainTimeTaskLocks(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_time_task_lock   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("supplychainTimeTaskLock")
    List<SupplychainTimeTaskLock> getSupplychainTimeTaskLockAll();

    @Select("<script>select count(1) from supplychain_time_task_lock   where   1=1 " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainTimeTaskLockCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_time_task_lock  " +
            "set " +
            "`title` = #{title}  , " +
            "`type` = #{type}  , " +
            "`happen_time` = #{happenTime}  , " +
            "`winner_address` = #{winnerAddress}  " +
            " where id = #{id}")
    void changeSupplychainTimeTaskLock(SupplychainTimeTaskLock supplychainTimeTaskLock);

}
