package com.tuoniaostore.supplychain.data;

import com.tuoniaostore.datamodel.supplychain.SupplychainShopProxyRelation;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Component
@Mapper
public interface SupplychainShopProxyRelationMapper {

    String column = "`id`, " +//
            "`host_shop_id`, " +//主要角色ID
            "`guest_shop_id`, " +//客人角色ID
            "`type`, " +//关系类型 如：独代、合作等
            "`status`, " +//关系状态 1--可用 2--不可用
            "`create_time`"//建立时间
            ;

    @Insert("insert into supplychain_shop_proxy_relation (" +
            " `id`,  " +
            " `host_shop_id`,  " +
            " `guest_shop_id`,  " +
            " `type`,  " +
            " `status`,  " +
            ")values(" +
            "#{id},  " +
            "#{hostShopId},  " +
            "#{guestShopId},  " +
            "#{type},  " +
            "#{status},  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSupplychainShopProxyRelation(SupplychainShopProxyRelation supplychainShopProxyRelation);

    @Results(id = "supplychainShopProxyRelation", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "hostShopId", column = "host_shop_id"),
            @Result(property = "guestShopId", column = "guest_shop_id"),
            @Result(property = "type", column = "type"), @Result(property = "status", column = "status"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from supplychain_shop_proxy_relation where id = #{id}")
    SupplychainShopProxyRelation getSupplychainShopProxyRelation(@Param("id") String id);

    @Select("<script>select  " + column + "  from supplychain_shop_proxy_relation   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("supplychainShopProxyRelation")
    List<SupplychainShopProxyRelation> getSupplychainShopProxyRelations(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from supplychain_shop_proxy_relation   where    1=1  and status=0 </script>")
    @ResultMap("supplychainShopProxyRelation")
    List<SupplychainShopProxyRelation> getSupplychainShopProxyRelationAll();

    @Select("<script>select count(1) from supplychain_shop_proxy_relation   where     1=1 " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSupplychainShopProxyRelationCount(@Param("status") int status, @Param("name") String name);

    @Update("update supplychain_shop_proxy_relation  " +
            "set " +
            "`host_shop_id` = #{hostShopId}  , " +
            "`guest_shop_id` = #{guestShopId}  , " +
            "`type` = #{type}  , " +
            "`status` = #{status}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeSupplychainShopProxyRelation(SupplychainShopProxyRelation supplychainShopProxyRelation);

}
