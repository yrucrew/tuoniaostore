package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainRolePropertiesService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainRoleProperties")
public class SupplychainRolePropertiesController {
    @Autowired
    private SupplychainRolePropertiesService supplychainRolePropertiesService;


    @RequestMapping("addSupplychainRoleProperties")
    @ResponseBody
    public JSONObject addSupplychainRoleProperties() {
        return supplychainRolePropertiesService.addSupplychainRoleProperties();
    }

    @RequestMapping("getSupplychainRoleProperties")
    @ResponseBody
    public JSONObject getSupplychainRoleProperties() {
        return supplychainRolePropertiesService.getSupplychainRoleProperties();
    }


    @RequestMapping("getSupplychainRolePropertiess")
    @ResponseBody
    public JSONObject getSupplychainRolePropertiess() {
        return supplychainRolePropertiesService.getSupplychainRolePropertiess();
    }

    @RequestMapping("getSupplychainRolePropertiesAll")
    @ResponseBody
    public JSONObject getSupplychainRolePropertiesAll() {
        return supplychainRolePropertiesService.getSupplychainRolePropertiesAll();
    }

    @RequestMapping("getSupplychainRolePropertiesCount")
    @ResponseBody
    public JSONObject getSupplychainRolePropertiesCount() {
        return supplychainRolePropertiesService.getSupplychainRolePropertiesCount();
    }

    @RequestMapping("changeSupplychainRoleProperties")
    @ResponseBody
    public JSONObject changeSupplychainRoleProperties() {
        return supplychainRolePropertiesService.changeSupplychainRoleProperties();
    }

}
