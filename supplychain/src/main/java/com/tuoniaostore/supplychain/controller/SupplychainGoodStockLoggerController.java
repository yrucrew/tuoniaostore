package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainGoodStockLoggerService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 商品的出入库日志
主要是针对商品的库存改变
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainGoodStockLogger")
public class SupplychainGoodStockLoggerController {
    @Autowired
    private SupplychainGoodStockLoggerService supplychainGoodStockLoggerService;


    @RequestMapping("getSupplychainGoodStockLogger")
    @ResponseBody
    public JSONObject getSupplychainGoodStockLogger() {
        return supplychainGoodStockLoggerService.getSupplychainGoodStockLogger();
    }

    @RequestMapping("addSupplychainGoodStockLoggers")
    @ResponseBody
    public JSONObject addSupplychainGoodStockLoggers() {
        return supplychainGoodStockLoggerService.addSupplychainGoodStockLoggers();
    }

    @RequestMapping("addSupplychainGoodStockLogger")
    @ResponseBody
    public JSONObject addSupplychainGoodStockLogger() {
        return supplychainGoodStockLoggerService.addSupplychainGoodStockLogger();
    }


    @RequestMapping("getSupplychainGoodStockLoggers")
    @ResponseBody
    public JSONObject getSupplychainGoodStockLoggers() {
        return supplychainGoodStockLoggerService.getSupplychainGoodStockLoggers();
    }

    @RequestMapping("getSupplychainGoodStockLoggerAll")
    @ResponseBody
    public JSONObject getSupplychainGoodStockLoggerAll() {
        return supplychainGoodStockLoggerService.getSupplychainGoodStockLoggerAll();
    }

    @RequestMapping("getSupplychainGoodStockLoggerCount")
    @ResponseBody
    public JSONObject getSupplychainGoodStockLoggerCount() {
        return supplychainGoodStockLoggerService.getSupplychainGoodStockLoggerCount();
    }

    @RequestMapping("changeSupplychainGoodStockLogger")
    @ResponseBody
    public JSONObject changeSupplychainGoodStockLogger() {
        return supplychainGoodStockLoggerService.changeSupplychainGoodStockLogger();
    }
    
    /**
     * 后台系统：进出库列表
     * @return
     */
    @RequestMapping("list")
    @ResponseBody
    public JSONObject getGoodStockLoggerList() {
        return supplychainGoodStockLoggerService.getGoodStockLoggerList();
    }

}
