package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainShopProxyRelationService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainShopProxyRelation")
public class SupplychainShopProxyRelationController {
    @Autowired
    private SupplychainShopProxyRelationService supplychainShopProxyRelationService;


    @RequestMapping("addSupplychainShopProxyRelation")
    @ResponseBody
    public JSONObject addSupplychainShopProxyRelation() {
        return supplychainShopProxyRelationService.addSupplychainShopProxyRelation();
    }

    @RequestMapping("getSupplychainShopProxyRelation")
    @ResponseBody
    public JSONObject getSupplychainShopProxyRelation() {
        return supplychainShopProxyRelationService.getSupplychainShopProxyRelation();
    }


    @RequestMapping("getSupplychainShopProxyRelations")
    @ResponseBody
    public JSONObject getSupplychainShopProxyRelations() {
        return supplychainShopProxyRelationService.getSupplychainShopProxyRelations();
    }

    @RequestMapping("getSupplychainShopProxyRelationAll")
    @ResponseBody
    public JSONObject getSupplychainShopProxyRelationAll() {
        return supplychainShopProxyRelationService.getSupplychainShopProxyRelationAll();
    }

    @RequestMapping("getSupplychainShopProxyRelationCount")
    @ResponseBody
    public JSONObject getSupplychainShopProxyRelationCount() {
        return supplychainShopProxyRelationService.getSupplychainShopProxyRelationCount();
    }

    @RequestMapping("changeSupplychainShopProxyRelation")
    @ResponseBody
    public JSONObject changeSupplychainShopProxyRelation() {
        return supplychainShopProxyRelationService.changeSupplychainShopProxyRelation();
    }

}
