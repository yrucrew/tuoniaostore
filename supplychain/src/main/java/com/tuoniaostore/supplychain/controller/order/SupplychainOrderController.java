package com.tuoniaostore.supplychain.controller.order;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.supplychain.service.order.SupplychainOrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("supplychainOrder")
public class SupplychainOrderController {

    @Autowired
    SupplychainOrderService supplychainOrderService;

    @ApiOperation("获取销售订单列表")
    @RequestMapping("receiptOrderEntryByPos")
    public JSONObject receiptOrderEntryByPos() {
        return supplychainOrderService.getSaleOrderList();
    }
}
