package com.tuoniaostore.supplychain.controller;

import com.tuoniaostore.supplychain.service.SupplychainShopPropertiesService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * 仓库的属性
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainShopProperties")
public class SupplychainShopPropertiesController {
    @Autowired
    private SupplychainShopPropertiesService supplychainShopPropertiesService;


    @RequestMapping("addSupplychainShopProperties")
    @ResponseBody
    public JSONObject addSupplychainShopProperties() {
        return supplychainShopPropertiesService.addSupplychainShopProperties();
    }

    /**
     * 添加门店
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/9
     */
    @RequestMapping("addSupplychainShopPropertiesForStore")
    @ResponseBody
    public JSONObject addSupplychainShopPropertiesForStore() {
        return supplychainShopPropertiesService.addSupplychainShopPropertiesForStore();
    }


    @RequestMapping("getSupplychainShopProperties")
    @ResponseBody
    public JSONObject getSupplychainShopProperties() {
        return supplychainShopPropertiesService.getSupplychainShopProperties();
    }

    @RequestMapping("getSupplychainShopPropertiess")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiess() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiess();
    }

    @ApiOperation("获取仓库列表")
    @RequestMapping("getSupplychainShopPropertiess2")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiess2() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiess2();
    }


    @RequestMapping("getSupplychainShopPropertiesAll")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiesAll() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiesAll();
    }

    @RequestMapping("getSupplychainShopPropertiesCount")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiesCount() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiesCount();
    }

    @RequestMapping("changeSupplychainShopProperties")
    @ResponseBody
    public JSONObject changeSupplychainShopProperties() {
        return supplychainShopPropertiesService.changeSupplychainShopProperties();
    }

    @RequestMapping("changeSupplychainShopProperties2")
    @ResponseBody
    public JSONObject changeSupplychainShopProperties2() {
        return supplychainShopPropertiesService.changeSupplychainShopProperties2();
    }

    @ApiOperation("删除门店/仓库")
    @RequestMapping("deleteSupplychainShopProperties")
    @ResponseBody
    public JSONObject deleteSupplychainShopProperties() {
        return supplychainShopPropertiesService.deleteSupplychainShopProperties();
    }

    /**
     * 获取当前用户的商店信息 通用
     *
     * @return
     */
    @RequestMapping("getSupplychainShopPropertiesByUserIdCommon")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiesByUserIdCommon() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiesByUserIdCommon();
    }

    /**
     * 商家端：获取当前用户的商店信息
     *
     * @return
     */
    @RequestMapping("getSupplychainShopPropertiesByUserId")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiesByUserId() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiesByUserId();
    }

    /**
     * 商家端：修改当前的用户的商店名称
     *
     * @return
     */
    @RequestMapping("changeSupplychainShopPropertiesById")
    @ResponseBody
    public JSONObject changeSupplychainShopPropertiesById() {
        return supplychainShopPropertiesService.changeSupplychainShopPropertiesById();
    }

    /**
     * 供应商端：修改供应商的营业状态
     *
     * @return
     */
    @RequestMapping("changeSupplychainShopPropertiesStatus")
    @ResponseBody
    public JSONObject changeSupplychainShopPropertiesStatus() {
        return supplychainShopPropertiesService.changeSupplychainShopPropertiesStatus();
    }

    /**
     * 供应商端：供应商页面数据
     *
     * @return
     */
    @RequestMapping("getSupplychainShopPropertiesVO")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiesVO() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiesVO();
    }

    /**
     * 供应商端：修改供应商的配送距离
     *
     * @return
     */
    @RequestMapping("changeSupplychainShopPropertiesCoveringDistance")
    @ResponseBody
    public JSONObject changeSupplychainShopPropertiesCoveringDistance() {
        return supplychainShopPropertiesService.changeSupplychainShopPropertiesCoveringDistance();
    }

    /**
     * 供应商端：修改打印机
     *
     * @return
     */
    @RequestMapping("changeSupplychainShopPropertiesBindPrinterCode")
    @ResponseBody
    public JSONObject changeSupplychainShopPropertiesBindPrinterCode() {
        return supplychainShopPropertiesService.changeSupplychainShopPropertiesBindPrinterCode();
    }

    /**
     * 供应商端：修改联系号码
     *
     * @return
     */
    @RequestMapping("changeSupplychainShopPropertiesPhoneNumber")
    @ResponseBody
    public JSONObject changeSupplychainShopPropertiesPhoneNumber() {
        return supplychainShopPropertiesService.changeSupplychainShopPropertiesPhoneNumber();
    }

    /**
     * 查询当前用户供应店信息
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/3/31
     */
    @RequestMapping("selectSupplychainShopPropertiesById")
    @ResponseBody
    public JSONObject selectSupplychainShopPropertiesById() {
        return supplychainShopPropertiesService.selectSupplychainShopPropertiesById();
    }

    /**
     * 查询当前商店的仓库
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/3/31
     */
    @RequestMapping("findRecentSelfShopForRemote")
    @ResponseBody
    public JSONObject findRelationShopKey() {
        return supplychainShopPropertiesService.findRecentSelfShopForRemote();
    }

    /**
     * 审核供应商
     *
     * @return
     */
    @RequestMapping("authSupplychainShopProperties")
    @ResponseBody
    public JSONObject authSupplychainShopProperties() {
        return supplychainShopPropertiesService.authSupplychainShopProperties();
    }

    /**
     * POS：获取首页分类
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/29
     */
    @RequestMapping("getSupplychainShopPropertiesForPosHome")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiesForPosHome() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiesForPosHome();
    }

    /**
     * POS：根据商品分类获取首页商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/30
     */
    @RequestMapping("getSupplychainShopPropertiesForPosHomeGood")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiesForPosHomeGood() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiesForPosHomeGood();
    }

    /**
     * 获取供应商详情和统计总收入
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/1
     */
    @RequestMapping("getSupplychainShopForSupplier")
    @ResponseBody
    public JSONObject getSupplychainShopForSupplier() {
        return supplychainShopPropertiesService.getSupplychainShopForSupplier();
    }


    /**
     * 根据当前用户查找对应的商店
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/5
     */
    @RequestMapping("getSupplychainShopByLoginUser")
    @ResponseBody
    public JSONObject getSupplychainShopByLoginUser() {
        return supplychainShopPropertiesService.getSupplychainShopByLoginUser();
    }

    /**
     * 查找该用户是否有商店
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/9
     */
    @RequestMapping("getExistSupplychainShopByUserId")
    @ResponseBody
    public JSONObject getExistSupplychainShopByUserId() {
        return supplychainShopPropertiesService.getExistSupplychainShopByUserId();
    }

    /**
     * 根据店名查询商店
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/20
     */
    @RequestMapping("getSupplychainShopPropertiesByShopName")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiesByShopName() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiesByShopName();
    }

    /**
     * 根据商店 获取对应可绑定的实体仓
     * @author oy
     * @date 2019/5/26
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainShopPropertiesForBind")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiesForBind() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiesForBind();
    }

    /**
     * 绑定父仓
     * @author oy
     * @date 2019/5/26
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("bindSupplychainShopProperties")
    @ResponseBody
    public JSONObject bindSupplychainShopProperties() {
        return supplychainShopPropertiesService.bindSupplychainShopProperties();
    }

    /***
     * 获取门店统计
     * @author oy
     * @date 2019/6/2
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainShopPropertiesTotal")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiesTotal() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiesTotal();
    }

    @ApiOperation("根据UserId获取当前用户的门店信息")
    @RequestMapping("getSupplychainShopPropertiesByUid")
    @ResponseBody
    public JSONObject getSupplychainShopPropertiesByUid() {
        return supplychainShopPropertiesService.getSupplychainShopPropertiesByUid();
    }

    @RequestMapping("UpdateBindPrinterCodeByUserId")
    @ResponseBody
    public JSONObject UpdateBindPrinterCodeByUserId() {
        return supplychainShopPropertiesService.UpdateBindPrinterCodeByUserId();
    }

}
