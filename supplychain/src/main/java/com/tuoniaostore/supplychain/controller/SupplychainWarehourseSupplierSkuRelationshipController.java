package com.tuoniaostore.supplychain.controller;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.supplychain.service.SupplychainWarehourseSupplierSkuRelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author oy
 * @description
 * @date 2019/6/3
 */
@RestController
@RequestMapping("supplychainWarehourseSupplierSkuRelationship")
public class SupplychainWarehourseSupplierSkuRelationshipController {

    @Autowired
    SupplychainWarehourseSupplierSkuRelationshipService supplychainWarehourseSupplierSkuRelationshipService;

    @RequestMapping("getSupplychainWarehourseBindRelationships")
    public JSONObject getSupplychainWarehourseSupplierSkuRelationships(){
        return supplychainWarehourseSupplierSkuRelationshipService.getSupplychainWarehourseSupplierSkuRelationships();
    }

    /***
     * 添加仓库绑定供应商的商品（priceId）
     * @author oy
     * @date 2019/6/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("addSupplychainWarehourseSupplierSkuRelationships")
    public JSONObject addSupplychainWarehourseSupplierSkuRelationships(){
        return supplychainWarehourseSupplierSkuRelationshipService.addSupplychainWarehourseSupplierSkuRelationships();
    }

}
