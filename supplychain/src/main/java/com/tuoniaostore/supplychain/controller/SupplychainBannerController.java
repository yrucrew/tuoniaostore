package com.tuoniaostore.supplychain.controller;

import com.tuoniaostore.supplychain.service.SupplychainBannerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@RestController
@RequestMapping("supplychainBanner")
public class SupplychainBannerController {
    @Autowired
    private SupplychainBannerService supplychainBannerService;

    @ApiOperation("添加")
    @RequestMapping("addSupplychainBanner")
    public JSONObject addSupplychainBanner() {
        return supplychainBannerService.addSupplychainBanner();
    }

    @ApiOperation("修改")
    @RequestMapping("changeSupplychainBanner")
    public JSONObject changeSupplychainBanner() {
        return supplychainBannerService.changeSupplychainBanner();
    }

    @ApiOperation("批量删除")
    @RequestMapping("batchDeleteSupplychainBanner")
    public JSONObject batchDeleteSupplychainBanner() {
        return supplychainBannerService.batchDeleteSupplychainBanner();
    }

    @RequestMapping("getSupplychainBanner")
    public JSONObject getSupplychainBanner() {
        return supplychainBannerService.getSupplychainBanner();
    }

    @ApiOperation("列表")
    @RequestMapping("getSupplychainBanners")
    public JSONObject getSupplychainBanners() {
        return supplychainBannerService.getSupplychainBanners();
    }

    @RequestMapping("getSupplychainBannerAll")
    public JSONObject getSupplychainBannerAll() {
        return supplychainBannerService.getSupplychainBannerAll();
    }

    @RequestMapping("getSupplychainBannerCount")
    public JSONObject getSupplychainBannerCount() {
        return supplychainBannerService.getSupplychainBannerCount();
    }


    /**
     * 商家端采购：获取首页的banner
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/12
     */
    @RequestMapping("getSupplychainBannerFirst")
    public JSONObject getSupplychainBannerFirst() {
        return supplychainBannerService.getSupplychainBannerFirst();
    }


    /**
     * 商家端采购：首页获取仓库地址(如果没有获取到地址 传999，999 获取 不管获没获取到地址 都会查绑定的 有绑定的就用绑定的)
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/15
     */
    @RequestMapping("getSupplychainShopFirst")
    public JSONObject getSupplychainShopFirst() {
        return supplychainBannerService.getSupplychainShopFirst();
    }

}
