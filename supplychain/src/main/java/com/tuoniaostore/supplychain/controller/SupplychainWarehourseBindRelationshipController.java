package com.tuoniaostore.supplychain.controller;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.supplychain.service.SupplychainWarehourseBindRelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author oy
 * @description
 * @date 2019/5/28
 */
@RestController
@RequestMapping("supplychainWarehourseBindRelationship")
public class SupplychainWarehourseBindRelationshipController {

    @Autowired
    private SupplychainWarehourseBindRelationshipService supplychainWarehourseBindRelationshipService;

    @RequestMapping("getSupplychainWarehourseBindRelationships")
    public JSONObject getSupplychainWarehourseBindRelationships() {
        return supplychainWarehourseBindRelationshipService.getSupplychainWarehourseBindRelationships();
    }

    /**
     * 获取 仓库绑定供应商列表
     * @author oy
     * @date 2019/5/28
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainWarehourseBindRelationshipsBySupplier")
    public JSONObject getSupplychainWarehourseBindRelationshipsBySupplier() {
        return supplychainWarehourseBindRelationshipService.getSupplychainWarehourseBindRelationshipsBySupplier();
    }

    /**
     * 获取该门店绑定的 供应商（门店）信息
     * @author oy
     * @date 2019/5/28
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("listForBindingWareHourse")
    public JSONObject listForBindingWareHourse() {
        return supplychainWarehourseBindRelationshipService.listForBindingWareHourse();
    }

    /**
     * 获取该门店绑定的 供应商（门店）详情信息
     * @author oy
     * @date 2019/5/28
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("listForBindingWareHourseDetails")
    public JSONObject listForBindingWareHourseDetails() {
        return supplychainWarehourseBindRelationshipService.listForBindingWareHourseDetails();
    }



    /**
     * 绑定供应商（门店） 和 仓库
     * @author oy
     * @date 2019/5/28
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("bindingWareHourseWithStore")
    public JSONObject bindingWareHourseWithStore() {
        return supplychainWarehourseBindRelationshipService.bindingWareHourseWithStore();
    }



}
