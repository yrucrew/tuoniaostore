package com.tuoniaostore.supplychain.controller;

import com.tuoniaostore.supplychain.service.SupplychainGoodLevelPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainGoodLevelPrice")
public class SupplychainGoodLevelPriceController {
    @Autowired
    private SupplychainGoodLevelPriceService supplychainGoodLevelPriceService;


    @RequestMapping("addSupplychainGoodLevelPrice")
    @ResponseBody
    public JSONObject addSupplychainGoodLevelPrice() {
        return supplychainGoodLevelPriceService.addSupplychainGoodLevelPrice();
    }

    @RequestMapping("getSupplychainGoodLevelPrice")
    @ResponseBody
    public JSONObject getSupplychainGoodLevelPrice() {
        return supplychainGoodLevelPriceService.getSupplychainGoodLevelPrice();
    }


    @RequestMapping("getSupplychainGoodLevelPrices")
    @ResponseBody
    public JSONObject getSupplychainGoodLevelPrices() {
        return supplychainGoodLevelPriceService.getSupplychainGoodLevelPrices();
    }

    @RequestMapping("getSupplychainGoodLevelPriceAll")
    @ResponseBody
    public JSONObject getSupplychainGoodLevelPriceAll() {
        return supplychainGoodLevelPriceService.getSupplychainGoodLevelPriceAll();
    }

    @RequestMapping("getSupplychainGoodLevelPriceCount")
    @ResponseBody
    public JSONObject getSupplychainGoodLevelPriceCount() {
        return supplychainGoodLevelPriceService.getSupplychainGoodLevelPriceCount();
    }

    @RequestMapping("changeSupplychainGoodLevelPrice")
    @ResponseBody
    public JSONObject changeSupplychainGoodLevelPrice() {
        return supplychainGoodLevelPriceService.changeSupplychainGoodLevelPrice();
    }

}
