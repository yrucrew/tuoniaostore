package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainShopDailyGoodSalesLoggerService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainShopDailyGoodSalesLogger")
public class SupplychainShopDailyGoodSalesLoggerController {
    @Autowired
    private SupplychainShopDailyGoodSalesLoggerService supplychainShopDailyGoodSalesLoggerService;


    @RequestMapping("addSupplychainShopDailyGoodSalesLogger")
    @ResponseBody
    public JSONObject addSupplychainShopDailyGoodSalesLogger() {
        return supplychainShopDailyGoodSalesLoggerService.addSupplychainShopDailyGoodSalesLogger();
    }

    @RequestMapping("getSupplychainShopDailyGoodSalesLogger")
    @ResponseBody
    public JSONObject getSupplychainShopDailyGoodSalesLogger() {
        return supplychainShopDailyGoodSalesLoggerService.getSupplychainShopDailyGoodSalesLogger();
    }


    @RequestMapping("getSupplychainShopDailyGoodSalesLoggers")
    @ResponseBody
    public JSONObject getSupplychainShopDailyGoodSalesLoggers() {
        return supplychainShopDailyGoodSalesLoggerService.getSupplychainShopDailyGoodSalesLoggers();
    }

    @RequestMapping("getSupplychainShopDailyGoodSalesLoggerAll")
    @ResponseBody
    public JSONObject getSupplychainShopDailyGoodSalesLoggerAll() {
        return supplychainShopDailyGoodSalesLoggerService.getSupplychainShopDailyGoodSalesLoggerAll();
    }

    @RequestMapping("getSupplychainShopDailyGoodSalesLoggerCount")
    @ResponseBody
    public JSONObject getSupplychainShopDailyGoodSalesLoggerCount() {
        return supplychainShopDailyGoodSalesLoggerService.getSupplychainShopDailyGoodSalesLoggerCount();
    }

    @RequestMapping("changeSupplychainShopDailyGoodSalesLogger")
    @ResponseBody
    public JSONObject changeSupplychainShopDailyGoodSalesLogger() {
        return supplychainShopDailyGoodSalesLoggerService.changeSupplychainShopDailyGoodSalesLogger();
    }

}
