package com.tuoniaostore.supplychain.controller;

import com.tuoniaostore.supplychain.service.SupplychainGoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainGood")
public class SupplychainGoodController {
    @Autowired
    private SupplychainGoodService supplychainGoodService;


    @RequestMapping("addSupplychainGood")
    @ResponseBody
    public JSONObject addSupplychainGood() {
        return supplychainGoodService.addSupplychainGood();
    }

    @RequestMapping("changSupplychainGoodByStock")
    @ResponseBody
    public JSONObject changSupplychainGoodByStock() {
        return supplychainGoodService.changSupplychainGoodByStock();
    }

    @RequestMapping("getSupplychainGood")
    @ResponseBody
    public JSONObject getSupplychainGood() {
        return supplychainGoodService.getSupplychainGood();
    }


    @RequestMapping("getSupplychainGoods")
    @ResponseBody
    public JSONObject getSupplychainGoods() {
        return supplychainGoodService.getSupplychainGoods();
    }

    @RequestMapping("getSupplychainGoodAll")
    @ResponseBody
    public JSONObject getSupplychainGoodAll() {
        return supplychainGoodService.getSupplychainGoodAll();
    }

    @RequestMapping("getSupplychainGoodCount")
    @ResponseBody
    public JSONObject getSupplychainGoodCount() {
        return supplychainGoodService.getSupplychainGoodCount();
    }

    @RequestMapping("changeSupplychainGood")
    @ResponseBody
    public JSONObject changeSupplychainGood() {
        return supplychainGoodService.changeSupplychainGood();
    }

    /**
     * 供应商端：根据类别 查找下面的商品
     *
     * @return
     */
    @RequestMapping("getSupplychainGoodByGoodTypeId")
    @ResponseBody
    public JSONObject getSupplychainGoodByGoodTypeId() {
        return supplychainGoodService.getSupplychainGoodByGoodTypeId();
    }

    /**
     * 商家端：根据类别 查找下面的商品
     *
     * @return
     */
    @RequestMapping("getSupplychainGoodByGoodTypeId2")
    @ResponseBody
    public JSONObject getSupplychainGoodByGoodTypeId2() {
        return supplychainGoodService.getSupplychainGoodByGoodTypeId2();
    }

    /**
     * 供应商：编辑商品
     *
     * @return
     */
    @RequestMapping("changeSupplychainGoodByGoodTypeId")
    @ResponseBody
    public JSONObject changeSupplychainGoodByGoodTypeId() {
        return supplychainGoodService.changeSupplychainGoodByGoodTypeId();
    }

    /**
     * 下架商品
     *
     * @return
     */
    @RequestMapping("downSupplychainGoodByGoodId")
    @ResponseBody
    public JSONObject downSupplychainGoodByGoodId() {
        return supplychainGoodService.downSupplychainGoodByGoodId();
    }

    /**
     * 批量下架商品
     *
     * @return
     */
    @RequestMapping("batchDownSupplychainGoodByGoodId")
    @ResponseBody
    public JSONObject batchDownSupplychainGoodByGoodId() {
        return supplychainGoodService.batchDownSupplychainGoodByGoodId();
    }

    /**
     * 批量上架商品
     *
     * @return
     */
    @RequestMapping("batchUpSupplychainGoodByGoodId")
    @ResponseBody
    public JSONObject batchUpSupplychainGoodByGoodId() {
        return supplychainGoodService.batchUpSupplychainGoodByGoodId();
    }

    /**
     * 上架商品
     *
     * @return
     */
    @RequestMapping("upSupplychainGoodByGoodId")
    @ResponseBody
    public JSONObject upSupplychainGoodByGoodId() {
        return supplychainGoodService.upSupplychainGoodByGoodId();
    }

    /**
     * 删除商品
     *
     * @return
     */
    @RequestMapping("deleteSupplychainGoodByGoodId")
    @ResponseBody
    public JSONObject deleteSupplychainGoodByGoodId() {
        return supplychainGoodService.deleteSupplychainGoodByGoodId();
    }

    /**
     * 批量删除商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/8
     */
    @RequestMapping("batchDeleteSupplychainGoodByGoodId")
    @ResponseBody
    public JSONObject batchDeleteSupplychainGoodByGoodId() {
        return supplychainGoodService.batchDeleteSupplychainGoodByGoodId();
    }

    /**
     * 商家：添加商品
     *
     * @return
     */
    @RequestMapping("addSupplychainGoodByUser")
    @ResponseBody
    public JSONObject addSupplychainGoodByUser() {
        return supplychainGoodService.addSupplychainGoodByUser();
    }

    /**
     * 商家：编辑商品
     *
     * @return
     */
    @RequestMapping("changeSupplychainGoodByGoodTypeId2")
    @ResponseBody
    public JSONObject changeSupplychainGoodByGoodTypeId2() {
        return supplychainGoodService.changeSupplychainGoodByGoodTypeId2();
    }

    /**
     * 功能描述
     * @author oy
     * @date 2019/6/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainGoodByIdAndStatus")
    @ResponseBody
    public JSONObject getSupplychainGoodByIdAndStatus() {
        return supplychainGoodService.getSupplychainGoodByIdAndStatus();
    }

    /**
     * 商家：修改之前 进入获取商品详细信息
     *
     * @return
     */
    @RequestMapping("getSupplychainGoodByGoodId")
    @ResponseBody
    public JSONObject getSupplychainGoodByGoodId() {
        return supplychainGoodService.getSupplychainGoodByGoodId();
    }

    /**
     * 商家：删除商品信息
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/10
     */
    @RequestMapping("delSupplychainGoodByGoodId")
    @ResponseBody
    public JSONObject delSupplychainGoodByGoodId() {
        return supplychainGoodService.delSupplychainGoodByGoodId();
    }

    /**
     * 商家：搜索商品信息(模糊查询)
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/10
     */
    @RequestMapping("searchSupplychainGoodByGoodId")
    @ResponseBody
    public JSONObject searchSupplychainGoodByGoodId() {
        return supplychainGoodService.searchSupplychainGoodByGoodId();
    }


    /**
     * 根据商品id查询商品
     *
     * @return
     */
    @RequestMapping("getSupplychainGoodById")
    @ResponseBody
    public JSONObject getSupplychainGoodById() {
        return supplychainGoodService.getSupplychainGoodById();
    }

    /**
     * 根据商品ids查询商品
     *
     * @return
     */
    @RequestMapping("getSupplychainGoodByIds")
    @ResponseBody
    public JSONObject getSupplychainGoodByIds() {
        return supplychainGoodService.getSupplychainGoodByIds();
    }

    /**
     * 根据商品id查询商品,和购物车数量
     *
     * @return
     */
    @RequestMapping("getSupplychainGoodByIdAndShopNumber")
    @ResponseBody
    public JSONObject getSupplychainGoodByIdAndShopNumber() {
        return supplychainGoodService.getSupplychainGoodByIdAndShopNumber();
    }

    /**
     * 通过商店id 查找所有的商品(远端调用)
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/23
     */
    @RequestMapping("getSupplychainGoodByShopId")
    @ResponseBody
    public JSONObject getSupplychainGoodByShopId() {
        return supplychainGoodService.getSupplychainGoodByShopId();
    }

    /**
     * 通过商店id 查找所有的商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/23
     */
    @RequestMapping("getSupplychainGoodByShopId2")
    @ResponseBody
    public JSONObject getSupplychainGoodByShopId2() {
        return supplychainGoodService.getSupplychainGoodByShopId2();
    }

    /**
     * 添加仓库和商品的绑定关系
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/23
     */
    @RequestMapping("addSupplychainGoodWithShopRelation")
    @ResponseBody
    public JSONObject addSupplychainGoodWithShopRelation() {
        return supplychainGoodService.addSupplychainGoodWithShopRelation();
    }

    /**
     * 添加仓库和商品的绑定关系2
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/23
     */
    @RequestMapping("addSupplychainGoodWithShopRelation2")
    @ResponseBody
    public JSONObject addSupplychainGoodWithShopRelation2() {
        return supplychainGoodService.addSupplychainGoodWithShopRelation2();
    }

    /**
     * 查询下单商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/4/19
     */
    @RequestMapping("getSupplychainGoodByIdOrder")
    @ResponseBody
    public JSONObject getSupplychainGoodByIdOrder() {
        return supplychainGoodService.getSupplychainGoodByIdOrder();
    }

    /**
     * 按仓库名 商品名 或者商品条码搜索仓库商品详情
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/24
     */
    @RequestMapping("searchSupplychainGoodBySHOrNameOrBarcode")
    @ResponseBody
    public JSONObject searchSupplychainGoodBySHOrNameOrBarcode() {
        return supplychainGoodService.searchSupplychainGoodBySHOrNameOrBarcode();
    }

    /***
     * 查找订单中的详情数据
     * @author oy
     * @date 2019/4/24
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainGoodForOrder")
    @ResponseBody
    public JSONObject getSupplychainGoodForOrder() {
        return supplychainGoodService.getSupplychainGoodForOrder();
    }

    /**
     * 更新库存消息
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/25
     */
    @RequestMapping("updateStockByList")
    @ResponseBody
    public JSONObject updateStockByList() {
        return supplychainGoodService.updateStockByList();
    }

    /**
     * 修改商品库存
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/6
     */
    @RequestMapping("changSupplychainGoodByGoodId")
    @ResponseBody
    public JSONObject changSupplychainGoodByGoodId() {
        return supplychainGoodService.changSupplychainGoodByGoodId();
    }

    /**
     * 修改绑定商品的一些信息
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/9
     */
    @RequestMapping("changSupplychainGoodDetailsByGoodId")
    @ResponseBody
    public JSONObject changSupplychainGoodDetailsByGoodId() {
        return supplychainGoodService.changSupplychainGoodDetailsByGoodId();
    }

    /**
     * 获取POS商品信息
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/9
     */
    @RequestMapping("getSupplychainGoodPosList")
    @ResponseBody
    public JSONObject getSupplychainGoodPosList() {
        return supplychainGoodService.getSupplychainGoodPosList();
    }

    /***
     * 根据商店id 价格id 获取对应的商品信息
     * @author oy
     * @date 2019/5/24
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainGoodByShopIdAndPriceId")
    @ResponseBody
    public JSONObject getSupplychainGoodByShopIdAndPriceId() {
        return supplychainGoodService.getSupplychainGoodByShopIdAndPriceId();
    }

    /**
     * 根据shopId 和 barcode 获取对应的商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/25
     */
    @RequestMapping("getSupplychainGoodByShopIdAndBarcode")
    @ResponseBody
    public JSONObject getSupplychainGoodByShopIdAndBarcode() {
        return supplychainGoodService.getSupplychainGoodByShopIdAndBarcode();
    }

    /**
     * 远程更新商品状态
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/27
     */
    @RequestMapping("updateSupplychainGoodStatus")
    @ResponseBody
    public JSONObject updateSupplychainGoodStatus() {
        return supplychainGoodService.updateSupplychainGoodStatus();
    }

    /**
     * 获取筛选仓库绑定的供应商的所有商品
     * @author oy
     * @date 2019/6/3
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainGoodForBindSupplierWithStore")
    @ResponseBody
    public JSONObject getSupplychainGoodForBindSupplierWithStore() {
        return supplychainGoodService.getSupplychainGoodForBindSupplierWithStore();
    }

}
