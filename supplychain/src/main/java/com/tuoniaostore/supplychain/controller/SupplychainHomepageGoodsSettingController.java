package com.tuoniaostore.supplychain.controller;

import com.tuoniaostore.supplychain.service.SupplychainHomepageGoodsSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * 首页设置表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-04 14:40:15
 */
@Controller
@RequestMapping("supplychainHomepageGoodsSetting")
public class SupplychainHomepageGoodsSettingController {
    @Autowired
    private SupplychainHomepageGoodsSettingService supplychainHomepageGoodsSettingService;


    @RequestMapping("addSupplychainHomepageGoodsSetting")
    @ResponseBody
    public JSONObject addSupplychainHomepageGoodsSetting() {
        return supplychainHomepageGoodsSettingService.addSupplychainHomepageGoodsSetting();
    }

    @RequestMapping("getSupplychainHomepageGoodsSetting")
    @ResponseBody
    public JSONObject getSupplychainHomepageGoodsSetting() {
        return supplychainHomepageGoodsSettingService.getSupplychainHomepageGoodsSetting();
    }


    @RequestMapping("getSupplychainHomepageGoodsSettings")
    @ResponseBody
    public JSONObject getSupplychainHomepageGoodsSettings() {
        return supplychainHomepageGoodsSettingService.getSupplychainHomepageGoodsSettings();
    }

    @RequestMapping("getSupplychainHomepageGoodsSettingAll")
    @ResponseBody
    public JSONObject getSupplychainHomepageGoodsSettingAll() {
        return supplychainHomepageGoodsSettingService.getSupplychainHomepageGoodsSettingAll();
    }

    @RequestMapping("getSupplychainHomepageGoodsSettingCount")
    @ResponseBody
    public JSONObject getSupplychainHomepageGoodsSettingCount() {
        return supplychainHomepageGoodsSettingService.getSupplychainHomepageGoodsSettingCount();
    }

    @RequestMapping("changeSupplychainHomepageGoodsSetting")
    @ResponseBody
    public JSONObject changeSupplychainHomepageGoodsSetting() {
        return supplychainHomepageGoodsSettingService.changeSupplychainHomepageGoodsSetting();
    }

    /**
     * 商家端采购：获取首页的 精选商品
     * @author oy
     * @date 2019/4/22
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getHomepageGoodsSettingForSelected")
    @ResponseBody
    public JSONObject getHomepageGoodsSettingForSelected() {
        return supplychainHomepageGoodsSettingService.getHomepageGoodsSettingForSelected();
    }


    /**
     * 商家端采购：首页数据 获取精选商品 固定10个 分页 点进去的话 就是多个
     * @author oy
     * @date 2019/4/12
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainHomepageGoodsSettingForSelected")
    @ResponseBody
    public JSONObject getSupplychainHomepageGoodsSettingForSelected() {
        return supplychainHomepageGoodsSettingService.getSupplychainHomepageGoodsSettingForSelected();
    }

    /**
     * 商家端采购：首页数据 获取热门品牌
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainHomepageGoodsSettingForHotBrand")
    @ResponseBody
    public JSONObject getSupplychainHomepageGoodsSettingForHotBrand() {
        return supplychainHomepageGoodsSettingService.getSupplychainHomepageGoodsSettingForHotBrand();
    }

    /**
     * 商家端采购：首页数据 获取热门品类
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainHomepageGoodsSettingForHotCategory")
    @ResponseBody
    public JSONObject getSupplychainHomepageGoodsSettingForHotCategory() {
        return supplychainHomepageGoodsSettingService.getSupplychainHomepageGoodsSettingForHotCategory();
    }

    /**
     * 商家端采购：首页数据 获取推荐商品
     * @author oy
     * @date 2019/4/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainHomepageGoodsSettingForRecommendingCommodities")
    @ResponseBody
    public JSONObject getSupplychainHomepageGoodsSettingForRecommendingCommodities() {
        return supplychainHomepageGoodsSettingService.getSupplychainHomepageGoodsSettingForRecommendingCommodities();
    }

    /**
     * 商家端：采购 获取热门品类下面的商品
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainGoodByGoodTypeByStore")
    @ResponseBody
    public JSONObject getSupplychainGoodByGoodTypeByStore() {
        return supplychainHomepageGoodsSettingService.getSupplychainGoodByGoodTypeByStore();
    }

    /**
     * 商家端：采购 获取热门品牌品牌下面的商品
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainGoodByGoodBrandByStore")
    @ResponseBody
    public JSONObject getSupplychainGoodByGoodBrandByStore() {
        return supplychainHomepageGoodsSettingService.getSupplychainGoodByGoodBrandByStore();
    }

    /**
     * 商家端：采购 页面搜索商品
     * @author oy
     * @date 2019/4/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainGoodByGoodNameSearch")
    @ResponseBody
    public JSONObject getSupplychainGoodByGoodNameSearch() {
        return supplychainHomepageGoodsSettingService.getSupplychainGoodByGoodNameSearch();
    }

    /***
     * 精选（推荐）商品配置首页
     * @author oy
     * @date 2019/4/25
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("listSupplychainGoodForHomeSetting")
    @ResponseBody
    public JSONObject listSupplychainGoodForHomeSetting() {
        return supplychainHomepageGoodsSettingService.listSupplychainGoodForHomeSetting();
    }

    /***
     * 精选（推荐）商品配置首页 批量删除
     * @author oy
     * @date 2019/4/26
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("bacchDeleteSupplychainGoodForHomeSetting")
    @ResponseBody
    public JSONObject bacchDeleteSupplychainGoodForHomeSetting() {
        return supplychainHomepageGoodsSettingService.bacchDeleteSupplychainGoodForHomeSetting();
    }

    /***
     * 精选（推荐）商品配置首页 添加
     * @author oy
     * @date 2019/4/26
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("addSupplychainGoodForHomeSetting")
    @ResponseBody
    public JSONObject addSupplychainGoodForHomeSetting() {
        return supplychainHomepageGoodsSettingService.addSupplychainGoodForHomeSetting();
    }

    /**
     * 推荐品牌首页显示 （带  区域名称，品牌名称 搜索 ）
     * @author oy
     * @date 2019/4/29
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("listForHomeSettingByBrand")
    @ResponseBody
    public JSONObject listForHomeSettingByBrand() {
        return supplychainHomepageGoodsSettingService.listForHomeSettingByBrand();
    }

    /**
     * 推荐分类首页显示 （带  区域名称，分类名称 搜索 ）
     * @author oy
     * @date 2019/4/29
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("listForHomeSettingByGoodType")
    @ResponseBody
    public JSONObject listForHomeSettingByGoodType() {
        return supplychainHomepageGoodsSettingService.listForHomeSettingByGoodType();
    }

    /***
     * 远程调用  通过地址获取对应的homePageGood
     * @author oy
     * @date 2019/5/1
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getHomePageGoodByAreaId")
    @ResponseBody
    public JSONObject getHomePageGoodByAreaId() {
        return supplychainHomepageGoodsSettingService.getHomePageGoodByAreaId();
    }

    /**
     * 批量修改推荐首页设置
     * @author oy
     * @date 2019/5/2
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("batchChangeHomeGoods")
    @ResponseBody
    public JSONObject batchChangeHomeGoods() {
        return supplychainHomepageGoodsSettingService.batchChangeHomeGoods();
    }


}
