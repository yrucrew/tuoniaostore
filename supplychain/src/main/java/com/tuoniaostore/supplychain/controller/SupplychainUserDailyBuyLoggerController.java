package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainUserDailyBuyLoggerService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainUserDailyBuyLogger")
public class SupplychainUserDailyBuyLoggerController {
    @Autowired
    private SupplychainUserDailyBuyLoggerService supplychainUserDailyBuyLoggerService;


    @RequestMapping("addSupplychainUserDailyBuyLogger")
    @ResponseBody
    public JSONObject addSupplychainUserDailyBuyLogger() {
        return supplychainUserDailyBuyLoggerService.addSupplychainUserDailyBuyLogger();
    }

    @RequestMapping("getSupplychainUserDailyBuyLogger")
    @ResponseBody
    public JSONObject getSupplychainUserDailyBuyLogger() {
        return supplychainUserDailyBuyLoggerService.getSupplychainUserDailyBuyLogger();
    }


    @RequestMapping("getSupplychainUserDailyBuyLoggers")
    @ResponseBody
    public JSONObject getSupplychainUserDailyBuyLoggers() {
        return supplychainUserDailyBuyLoggerService.getSupplychainUserDailyBuyLoggers();
    }

    @RequestMapping("getSupplychainUserDailyBuyLoggerAll")
    @ResponseBody
    public JSONObject getSupplychainUserDailyBuyLoggerAll() {
        return supplychainUserDailyBuyLoggerService.getSupplychainUserDailyBuyLoggerAll();
    }

    @RequestMapping("getSupplychainUserDailyBuyLoggerCount")
    @ResponseBody
    public JSONObject getSupplychainUserDailyBuyLoggerCount() {
        return supplychainUserDailyBuyLoggerService.getSupplychainUserDailyBuyLoggerCount();
    }

    @RequestMapping("changeSupplychainUserDailyBuyLogger")
    @ResponseBody
    public JSONObject changeSupplychainUserDailyBuyLogger() {
        return supplychainUserDailyBuyLoggerService.changeSupplychainUserDailyBuyLogger();
    }

}
