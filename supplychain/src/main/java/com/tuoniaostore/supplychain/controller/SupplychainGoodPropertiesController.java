package com.tuoniaostore.supplychain.controller;

import com.tuoniaostore.supplychain.service.SupplychainGoodPropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainGoodProperties")
public class SupplychainGoodPropertiesController {
    @Autowired
    private SupplychainGoodPropertiesService supplychainGoodPropertiesService;


    @RequestMapping("addSupplychainGoodProperties")
    @ResponseBody
    public JSONObject addSupplychainGoodProperties() {
        return supplychainGoodPropertiesService.addSupplychainGoodProperties();
    }

    @RequestMapping("getSupplychainGoodProperties")
    @ResponseBody
    public JSONObject getSupplychainGoodProperties() {
        return supplychainGoodPropertiesService.getSupplychainGoodProperties();
    }


    @RequestMapping("getSupplychainGoodPropertiess")
    @ResponseBody
    public JSONObject getSupplychainGoodPropertiess() {
        return supplychainGoodPropertiesService.getSupplychainGoodPropertiess();
    }

    @RequestMapping("getSupplychainGoodPropertiesAll")
    @ResponseBody
    public JSONObject getSupplychainGoodPropertiesAll() {
        return supplychainGoodPropertiesService.getSupplychainGoodPropertiesAll();
    }

    @RequestMapping("getSupplychainGoodPropertiesCount")
    @ResponseBody
    public JSONObject getSupplychainGoodPropertiesCount() {
        return supplychainGoodPropertiesService.getSupplychainGoodPropertiesCount();
    }

    @RequestMapping("changeSupplychainGoodProperties")
    @ResponseBody
    public JSONObject changeSupplychainGoodProperties() {
        return supplychainGoodPropertiesService.changeSupplychainGoodProperties();
    }

}
