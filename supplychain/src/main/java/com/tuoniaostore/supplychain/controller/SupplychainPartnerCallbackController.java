package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainPartnerCallbackService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainPartnerCallback")
public class SupplychainPartnerCallbackController {
    @Autowired
    private SupplychainPartnerCallbackService supplychainPartnerCallbackService;


    @RequestMapping("addSupplychainPartnerCallback")
    @ResponseBody
    public JSONObject addSupplychainPartnerCallback() {
        return supplychainPartnerCallbackService.addSupplychainPartnerCallback();
    }

    @RequestMapping("getSupplychainPartnerCallback")
    @ResponseBody
    public JSONObject getSupplychainPartnerCallback() {
        return supplychainPartnerCallbackService.getSupplychainPartnerCallback();
    }


    @RequestMapping("getSupplychainPartnerCallbacks")
    @ResponseBody
    public JSONObject getSupplychainPartnerCallbacks() {
        return supplychainPartnerCallbackService.getSupplychainPartnerCallbacks();
    }

    @RequestMapping("getSupplychainPartnerCallbackAll")
    @ResponseBody
    public JSONObject getSupplychainPartnerCallbackAll() {
        return supplychainPartnerCallbackService.getSupplychainPartnerCallbackAll();
    }

    @RequestMapping("getSupplychainPartnerCallbackCount")
    @ResponseBody
    public JSONObject getSupplychainPartnerCallbackCount() {
        return supplychainPartnerCallbackService.getSupplychainPartnerCallbackCount();
    }

    @RequestMapping("changeSupplychainPartnerCallback")
    @ResponseBody
    public JSONObject changeSupplychainPartnerCallback() {
        return supplychainPartnerCallbackService.changeSupplychainPartnerCallback();
    }

}
