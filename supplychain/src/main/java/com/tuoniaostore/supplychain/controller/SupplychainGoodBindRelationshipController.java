package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainGoodBindRelationshipService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainGoodBindRelationship")
public class SupplychainGoodBindRelationshipController {
    @Autowired
    private SupplychainGoodBindRelationshipService supplychainGoodBindRelationshipService;


    @RequestMapping("addSupplychainGoodBindRelationship")
    @ResponseBody
    public JSONObject addSupplychainGoodBindRelationship() {
        return supplychainGoodBindRelationshipService.addSupplychainGoodBindRelationship();
    }

    @RequestMapping("getSupplychainGoodBindRelationship")
    @ResponseBody
    public JSONObject getSupplychainGoodBindRelationship() {
        return supplychainGoodBindRelationshipService.getSupplychainGoodBindRelationship();
    }

    @RequestMapping("getSupplychainGoodBindRelationships")
    @ResponseBody
    public JSONObject getSupplychainGoodBindRelationships() {
        return supplychainGoodBindRelationshipService.getSupplychainGoodBindRelationships();
    }

    @RequestMapping("getSupplychainGoodBindRelationshipAll")
    @ResponseBody
    public JSONObject getSupplychainGoodBindRelationshipAll() {
        return supplychainGoodBindRelationshipService.getSupplychainGoodBindRelationshipAll();
    }

    @RequestMapping("getSupplychainGoodBindRelationshipCount")
    @ResponseBody
    public JSONObject getSupplychainGoodBindRelationshipCount() {
        return supplychainGoodBindRelationshipService.getSupplychainGoodBindRelationshipCount();
    }

    @RequestMapping("changeSupplychainGoodBindRelationship")
    @ResponseBody
    public JSONObject changeSupplychainGoodBindRelationship() {
        return supplychainGoodBindRelationshipService.changeSupplychainGoodBindRelationship();
    }

    /**
     * 获取供应链所有的商品类型
     * @return
     */
    @RequestMapping("getSupplychainGoodBindRelationshipByUserId")
    @ResponseBody
    public JSONObject getSupplychainGoodBindRelationshipByUserId() {
        return supplychainGoodBindRelationshipService.getSupplychainGoodBindRelationshipByUserId();
    }

    /**
     * 获取商家端所有的商品类型
     * @author oy
     * @date 2019/3/31
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainGoodBindRelationshipByUserId2")
    @ResponseBody
    public JSONObject getSupplychainGoodBindRelationshipByUserId2() {
        return supplychainGoodBindRelationshipService.getSupplychainGoodBindRelationshipByUserId2();
    }


    /**
     * 商家端：添加商品分类
     * @author oy
     * @date 2019/3/31
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("addSupplychainGoodBindRelationshipByUser")
    @ResponseBody
    public JSONObject addSupplychainGoodBindRelationshipByUser() {
        return supplychainGoodBindRelationshipService.addSupplychainGoodBindRelationshipByUser();
    }

    /**
     * 商家端：删除商品分类
     * @author oy
     * @date 2019/3/31
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("delSupplychainGoodBindRelationshipByUser")
    @ResponseBody
    public JSONObject delSupplychainGoodBindRelationshipByUser() {
        return supplychainGoodBindRelationshipService.delSupplychainGoodBindRelationshipByUser();
    }

    /**
     * 通过id 查找所有的绑定关系
     * @author oy
     * @date 2019/4/23
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainGoodBindRelationshipByUserIdForRemote")
    @ResponseBody
    public JSONObject getSupplychainGoodBindRelationshipByUserIdForRemote() {
        return supplychainGoodBindRelationshipService.getSupplychainGoodBindRelationshipByUserIdForRemote();
    }

    /**
     * 为商店绑定商品分类
     * @author oy
     * @date 2019/5/6
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("bindSupplychainGoodBindRelationships")
    @ResponseBody
    public JSONObject bindSupplychainGoodBindRelationships() {
        return supplychainGoodBindRelationshipService.bindSupplychainGoodBindRelationships();
    }


    /**
     * 获取商店绑定的分类
     * @author oy
     * @date 2019/5/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getbindSupplychainGoodBindRelationships")
    @ResponseBody
    public JSONObject getbindSupplychainGoodBindRelationships() {
        return supplychainGoodBindRelationshipService.getbindSupplychainGoodBindRelationships();
    }

    /**
     * 商品绑定分类 筛选已选fenlei
     * @author oy
     * @date 2019/5/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getTypeBySupplychainGoodBindType")
    @ResponseBody
    public JSONObject getTypeBySupplychainGoodBindType() {
        return supplychainGoodBindRelationshipService.getTypeBySupplychainGoodBindType();
    }

    /**
     * 获取商店绑定的分类详情
     * @author oy
     * @date 2019/5/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getbindSupplychainGoodBindRelationshipsDetails")
    @ResponseBody
    public JSONObject getbindSupplychainGoodBindRelationshipsDetails() {
        return supplychainGoodBindRelationshipService.getbindSupplychainGoodBindRelationshipsDetails();
    }

    /**
     * 修改商店绑定的分类
     * @author oy
     * @date 2019/5/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("updatebindSupplychainGoodBindRelationships")
    @ResponseBody
    public JSONObject updatebindSupplychainGoodBindRelationships() {
        return supplychainGoodBindRelationshipService.updatebindSupplychainGoodBindRelationships();
    }

}
