package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainPartnerShopFocusGoodService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainPartnerShopFocusGood")
public class SupplychainPartnerShopFocusGoodController {
    @Autowired
    private SupplychainPartnerShopFocusGoodService supplychainPartnerShopFocusGoodService;


    @RequestMapping("addSupplychainPartnerShopFocusGood")
    @ResponseBody
    public JSONObject addSupplychainPartnerShopFocusGood() {
        return supplychainPartnerShopFocusGoodService.addSupplychainPartnerShopFocusGood();
    }

    @RequestMapping("getSupplychainPartnerShopFocusGood")
    @ResponseBody
    public JSONObject getSupplychainPartnerShopFocusGood() {
        return supplychainPartnerShopFocusGoodService.getSupplychainPartnerShopFocusGood();
    }


    @RequestMapping("getSupplychainPartnerShopFocusGoods")
    @ResponseBody
    public JSONObject getSupplychainPartnerShopFocusGoods() {
        return supplychainPartnerShopFocusGoodService.getSupplychainPartnerShopFocusGoods();
    }

    @RequestMapping("getSupplychainPartnerShopFocusGoodAll")
    @ResponseBody
    public JSONObject getSupplychainPartnerShopFocusGoodAll() {
        return supplychainPartnerShopFocusGoodService.getSupplychainPartnerShopFocusGoodAll();
    }

    @RequestMapping("getSupplychainPartnerShopFocusGoodCount")
    @ResponseBody
    public JSONObject getSupplychainPartnerShopFocusGoodCount() {
        return supplychainPartnerShopFocusGoodService.getSupplychainPartnerShopFocusGoodCount();
    }

    @RequestMapping("changeSupplychainPartnerShopFocusGood")
    @ResponseBody
    public JSONObject changeSupplychainPartnerShopFocusGood() {
        return supplychainPartnerShopFocusGoodService.changeSupplychainPartnerShopFocusGood();
    }

}
