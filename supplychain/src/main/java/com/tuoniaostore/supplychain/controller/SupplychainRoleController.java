package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainRoleService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainRole")
public class SupplychainRoleController {
    @Autowired
    private SupplychainRoleService supplychainRoleService;


    @RequestMapping("addSupplychainRole")
    @ResponseBody
    public JSONObject addSupplychainRole() {
        return supplychainRoleService.addSupplychainRole();
    }

    @RequestMapping("getSupplychainRole")
    @ResponseBody
    public JSONObject getSupplychainRole() {
        return supplychainRoleService.getSupplychainRole();
    }


    @RequestMapping("getSupplychainRoles")
    @ResponseBody
    public JSONObject getSupplychainRoles() {
        return supplychainRoleService.getSupplychainRoles();
    }

    @RequestMapping("getSupplychainRoleAll")
    @ResponseBody
    public JSONObject getSupplychainRoleAll() {
        return supplychainRoleService.getSupplychainRoleAll();
    }

    @RequestMapping("getSupplychainRoleCount")
    @ResponseBody
    public JSONObject getSupplychainRoleCount() {
        return supplychainRoleService.getSupplychainRoleCount();
    }

    @RequestMapping("changeSupplychainRole")
    @ResponseBody
    public JSONObject changeSupplychainRole() {
        return supplychainRoleService.changeSupplychainRole();
    }

}
