package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainPartnerService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainPartner")
public class SupplychainPartnerController {
    @Autowired
    private SupplychainPartnerService supplychainPartnerService;


    @RequestMapping("addSupplychainPartner")
    @ResponseBody
    public JSONObject addSupplychainPartner() {
        return supplychainPartnerService.addSupplychainPartner();
    }

    @RequestMapping("getSupplychainPartner")
    @ResponseBody
    public JSONObject getSupplychainPartner() {
        return supplychainPartnerService.getSupplychainPartner();
    }


    @RequestMapping("getSupplychainPartners")
    @ResponseBody
    public JSONObject getSupplychainPartners() {
        return supplychainPartnerService.getSupplychainPartners();
    }

    @RequestMapping("getSupplychainPartnerAll")
    @ResponseBody
    public JSONObject getSupplychainPartnerAll() {
        return supplychainPartnerService.getSupplychainPartnerAll();
    }

    @RequestMapping("getSupplychainPartnerCount")
    @ResponseBody
    public JSONObject getSupplychainPartnerCount() {
        return supplychainPartnerService.getSupplychainPartnerCount();
    }

    @RequestMapping("changeSupplychainPartner")
    @ResponseBody
    public JSONObject changeSupplychainPartner() {
        return supplychainPartnerService.changeSupplychainPartner();
    }

}
