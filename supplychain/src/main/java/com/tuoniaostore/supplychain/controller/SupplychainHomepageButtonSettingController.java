package com.tuoniaostore.supplychain.controller;

import com.tuoniaostore.supplychain.service.SupplychainHomepageButtonSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * 首页按钮配置
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-04 14:40:15
 */
@Controller
@RequestMapping("supplychainHomepageButtonSetting")
public class SupplychainHomepageButtonSettingController {
    @Autowired
    private SupplychainHomepageButtonSettingService supplychainHomepageButtonSettingService;


    @RequestMapping("addSupplychainHomepageButtonSetting")
    @ResponseBody
    public JSONObject addSupplychainHomepageButtonSetting() {
        return supplychainHomepageButtonSettingService.addSupplychainHomepageButtonSetting();
    }

    @RequestMapping("getSupplychainHomepageButtonSetting")
    @ResponseBody
    public JSONObject getSupplychainHomepageButtonSetting() {
        return supplychainHomepageButtonSettingService.getSupplychainHomepageButtonSetting();
    }


    @RequestMapping("getSupplychainHomepageButtonSettings")
    @ResponseBody
    public JSONObject getSupplychainHomepageButtonSettings() {
        return supplychainHomepageButtonSettingService.getSupplychainHomepageButtonSettings();
    }

    @RequestMapping("getSupplychainHomepageButtonSettingAll")
    @ResponseBody
    public JSONObject getSupplychainHomepageButtonSettingAll() {
        return supplychainHomepageButtonSettingService.getSupplychainHomepageButtonSettingAll();
    }

    @RequestMapping("getSupplychainHomepageButtonSettingCount")
    @ResponseBody
    public JSONObject getSupplychainHomepageButtonSettingCount() {
        return supplychainHomepageButtonSettingService.getSupplychainHomepageButtonSettingCount();
    }

    @RequestMapping("changeSupplychainHomepageButtonSetting")
    @ResponseBody
    public JSONObject changeSupplychainHomepageButtonSetting() {
        return supplychainHomepageButtonSettingService.changeSupplychainHomepageButtonSetting();
    }

    /**
     * 商家端采购：获取采购首页菜单按钮
     * @author oy
     * @date 2019/4/11
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSupplychainHomepageButton")
    @ResponseBody
    public JSONObject getSupplychainHomepageButton() {
        return supplychainHomepageButtonSettingService.getSupplychainHomepageButton();
    }

    /**
     * 检查按钮是否存在
     * @author oy
     * @date 2019/5/30
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("validateSupplychainHomepageButton")
    @ResponseBody
    public JSONObject validateSupplychainHomepageButton() {
        return supplychainHomepageButtonSettingService.validateSupplychainHomepageButton();
    }

    /**
     * 批量删除接口
     * @author oy
     * @date 2019/5/30
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("batchDeleteSupplychainHomepageButton")
    @ResponseBody
    public JSONObject batchDeleteSupplychainHomepageButton() {
        return supplychainHomepageButtonSettingService.batchDeleteSupplychainHomepageButton();
    }

}
