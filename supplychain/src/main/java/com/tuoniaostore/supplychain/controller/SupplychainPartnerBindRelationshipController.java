package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainPartnerBindRelationshipService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainPartnerBindRelationship")
public class SupplychainPartnerBindRelationshipController {
    @Autowired
    private SupplychainPartnerBindRelationshipService supplychainPartnerBindRelationshipService;


    @RequestMapping("addSupplychainPartnerBindRelationship")
    @ResponseBody
    public JSONObject addSupplychainPartnerBindRelationship() {
        return supplychainPartnerBindRelationshipService.addSupplychainPartnerBindRelationship();
    }

    @RequestMapping("getSupplychainPartnerBindRelationship")
    @ResponseBody
    public JSONObject getSupplychainPartnerBindRelationship() {
        return supplychainPartnerBindRelationshipService.getSupplychainPartnerBindRelationship();
    }


    @RequestMapping("getSupplychainPartnerBindRelationships")
    @ResponseBody
    public JSONObject getSupplychainPartnerBindRelationships() {
        return supplychainPartnerBindRelationshipService.getSupplychainPartnerBindRelationships();
    }

    @RequestMapping("getSupplychainPartnerBindRelationshipAll")
    @ResponseBody
    public JSONObject getSupplychainPartnerBindRelationshipAll() {
        return supplychainPartnerBindRelationshipService.getSupplychainPartnerBindRelationshipAll();
    }

    @RequestMapping("getSupplychainPartnerBindRelationshipCount")
    @ResponseBody
    public JSONObject getSupplychainPartnerBindRelationshipCount() {
        return supplychainPartnerBindRelationshipService.getSupplychainPartnerBindRelationshipCount();
    }

    @RequestMapping("changeSupplychainPartnerBindRelationship")
    @ResponseBody
    public JSONObject changeSupplychainPartnerBindRelationship() {
        return supplychainPartnerBindRelationshipService.changeSupplychainPartnerBindRelationship();
    }

}
