package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainTimeTaskLockService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainTimeTaskLock")
public class SupplychainTimeTaskLockController {
    @Autowired
    private SupplychainTimeTaskLockService supplychainTimeTaskLockService;


    @RequestMapping("addSupplychainTimeTaskLock")
    @ResponseBody
    public JSONObject addSupplychainTimeTaskLock() {
        return supplychainTimeTaskLockService.addSupplychainTimeTaskLock();
    }

    @RequestMapping("getSupplychainTimeTaskLock")
    @ResponseBody
    public JSONObject getSupplychainTimeTaskLock() {
        return supplychainTimeTaskLockService.getSupplychainTimeTaskLock();
    }


    @RequestMapping("getSupplychainTimeTaskLocks")
    @ResponseBody
    public JSONObject getSupplychainTimeTaskLocks() {
        return supplychainTimeTaskLockService.getSupplychainTimeTaskLocks();
    }

    @RequestMapping("getSupplychainTimeTaskLockAll")
    @ResponseBody
    public JSONObject getSupplychainTimeTaskLockAll() {
        return supplychainTimeTaskLockService.getSupplychainTimeTaskLockAll();
    }

    @RequestMapping("getSupplychainTimeTaskLockCount")
    @ResponseBody
    public JSONObject getSupplychainTimeTaskLockCount() {
        return supplychainTimeTaskLockService.getSupplychainTimeTaskLockCount();
    }

    @RequestMapping("changeSupplychainTimeTaskLock")
    @ResponseBody
    public JSONObject changeSupplychainTimeTaskLock() {
        return supplychainTimeTaskLockService.changeSupplychainTimeTaskLock();
    }

}
