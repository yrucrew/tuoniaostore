package com.tuoniaostore.supplychain.controller;

import com.tuoniaostore.supplychain.service.SupplychainGoodModifyLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * 商品库存修改日志
 * 注意：该表主要记录人为介入修改的数据
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainGoodModifyLogger")
public class SupplychainGoodModifyLoggerController {
    @Autowired
    private SupplychainGoodModifyLoggerService supplychainGoodModifyLoggerService;


    @RequestMapping("addSupplychainGoodModifyLogger")
    @ResponseBody
    public JSONObject addSupplychainGoodModifyLogger() {
        return supplychainGoodModifyLoggerService.addSupplychainGoodModifyLogger();
    }

    @RequestMapping("getSupplychainGoodModifyLogger")
    @ResponseBody
    public JSONObject getSupplychainGoodModifyLogger() {
        return supplychainGoodModifyLoggerService.getSupplychainGoodModifyLogger();
    }


    @RequestMapping("getSupplychainGoodModifyLoggers")
    @ResponseBody
    public JSONObject getSupplychainGoodModifyLoggers() {
        return supplychainGoodModifyLoggerService.getSupplychainGoodModifyLoggers();
    }

    @RequestMapping("getSupplychainGoodModifyLoggerAll")
    @ResponseBody
    public JSONObject getSupplychainGoodModifyLoggerAll() {
        return supplychainGoodModifyLoggerService.getSupplychainGoodModifyLoggerAll();
    }

    @RequestMapping("getSupplychainGoodModifyLoggerCount")
    @ResponseBody
    public JSONObject getSupplychainGoodModifyLoggerCount() {
        return supplychainGoodModifyLoggerService.getSupplychainGoodModifyLoggerCount();
    }

    @RequestMapping("changeSupplychainGoodModifyLogger")
    @ResponseBody
    public JSONObject changeSupplychainGoodModifyLogger() {
        return supplychainGoodModifyLoggerService.changeSupplychainGoodModifyLogger();
    }

}
