package com.tuoniaostore.supplychain.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.supplychain.service.SupplychainPurchaseBindGoodService;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("purchaseBindGood")
public class SupplychainPurchaseBindGoodController {

    @Autowired
    SupplychainPurchaseBindGoodService purchaseBindGoodService;

    @ApiOperation("添加采购员绑定商品")
    @RequestMapping("add")
    public JSONObject addPurchaseBindGood() {
        return purchaseBindGoodService.addPurchaseBindGood();
    }

    @ApiOperation("修改采购员绑定商品")
    @RequestMapping("update")
    public JSONObject updatePurchaseBindGood() {
        return purchaseBindGoodService.updatePurchaseBindGood();
    }

    @ApiOperation("删除采购员绑定商品")
    @RequestMapping("delete")
    public JSONObject deletePurchaseBindGood() {
        return purchaseBindGoodService.deletePurchaseBindGood();
    }

    @ApiOperation("采购员绑定商品列表查询")
    @RequestMapping("list")
    public JSONObject purchaseBindGoodList() {
        return purchaseBindGoodService.purchaseBindGoodList();
    }

    @ApiOperation("后台系统：查询商品模板列表")
    @RequestMapping("getGoodTemplates")
    public JSONObject getGoodTemplates() {
        return purchaseBindGoodService.getGoodTemplates();
    }

    @ApiOperation("检查采购员是否已绑定商品")
    @RequestMapping("checkPurchaseBindGood")
    public JSONObject checkPurchaseBindGood() {
        return purchaseBindGoodService.checkPurchaseBindBood();
    }

}
