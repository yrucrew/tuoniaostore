package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainShopGoodSaleService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-20 20:05:41
 */
@Controller
@RequestMapping("supplychainShopGoodSale")
public class SupplychainShopGoodSaleController {
    @Autowired
    private SupplychainShopGoodSaleService supplychainShopGoodSaleService;


    @RequestMapping("addSupplychainShopGoodSale")
    @ResponseBody
    public JSONObject addSupplychainShopGoodSale() {
        return supplychainShopGoodSaleService.addSupplychainShopGoodSale();
    }

    @RequestMapping("getSupplychainShopGoodSale")
    @ResponseBody
    public JSONObject getSupplychainShopGoodSale() {
        return supplychainShopGoodSaleService.getSupplychainShopGoodSale();
    }


    @RequestMapping("getSupplychainShopGoodSales")
    @ResponseBody
    public JSONObject getSupplychainShopGoodSales() {
        return supplychainShopGoodSaleService.getSupplychainShopGoodSales();
    }

    @RequestMapping("getSupplychainShopGoodSaleAll")
    @ResponseBody
    public JSONObject getSupplychainShopGoodSaleAll() {
        return supplychainShopGoodSaleService.getSupplychainShopGoodSaleAll();
    }

    @RequestMapping("getSupplychainShopGoodSaleCount")
    @ResponseBody
    public JSONObject getSupplychainShopGoodSaleCount() {
        return supplychainShopGoodSaleService.getSupplychainShopGoodSaleCount();
    }

    @RequestMapping("changeSupplychainShopGoodSale")
    @ResponseBody
    public JSONObject changeSupplychainShopGoodSale() {
        return supplychainShopGoodSaleService.changeSupplychainShopGoodSale();
    }
    /**
     * 销售报表首页（包括筛选）
     * @author oy
     * @date 2019/5/25
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("listSupplychainShopGoodSale")
    @ResponseBody
    public JSONObject listSupplychainShopGoodSale() {
        return supplychainShopGoodSaleService.listSupplychainShopGoodSale();
    }

}
