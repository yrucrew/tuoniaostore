package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainLevelTemplateService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:25
 */
@Controller
@RequestMapping("supplychainLevelTemplate")
public class SupplychainLevelTemplateController {
    @Autowired
    private SupplychainLevelTemplateService supplychainLevelTemplateService;


    @RequestMapping("addSupplychainLevelTemplate")
    @ResponseBody
    public JSONObject addSupplychainLevelTemplate() {
        return supplychainLevelTemplateService.addSupplychainLevelTemplate();
    }

    @RequestMapping("getSupplychainLevelTemplate")
    @ResponseBody
    public JSONObject getSupplychainLevelTemplate() {
        return supplychainLevelTemplateService.getSupplychainLevelTemplate();
    }


    @RequestMapping("getSupplychainLevelTemplates")
    @ResponseBody
    public JSONObject getSupplychainLevelTemplates() {
        return supplychainLevelTemplateService.getSupplychainLevelTemplates();
    }

    @RequestMapping("getSupplychainLevelTemplateAll")
    @ResponseBody
    public JSONObject getSupplychainLevelTemplateAll() {
        return supplychainLevelTemplateService.getSupplychainLevelTemplateAll();
    }

    @RequestMapping("getSupplychainLevelTemplateCount")
    @ResponseBody
    public JSONObject getSupplychainLevelTemplateCount() {
        return supplychainLevelTemplateService.getSupplychainLevelTemplateCount();
    }

    @RequestMapping("changeSupplychainLevelTemplate")
    @ResponseBody
    public JSONObject changeSupplychainLevelTemplate() {
        return supplychainLevelTemplateService.changeSupplychainLevelTemplate();
    }

}
