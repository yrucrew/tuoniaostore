package com.tuoniaostore.supplychain.controller;

import com.tuoniaostore.supplychain.service.PosWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-06 10:47:35
 */
@Controller
@RequestMapping("posWork")
public class PosWorkController {
    @Autowired
    private PosWorkService posWorkService;


    @RequestMapping("addPosWork")
    @ResponseBody
    public JSONObject addPosWork() {
        return posWorkService.addPosWork();
    }

    @RequestMapping("getPosWork")
    @ResponseBody
    public JSONObject getPosWork() {
        return posWorkService.getPosWork();
    }


    @RequestMapping("getPosWorks")
    @ResponseBody
    public JSONObject getPosWorks() {
        return posWorkService.getPosWorks();
    }

    @RequestMapping("getPosWorkAll")
    @ResponseBody
    public JSONObject getPosWorkAll() {
        return posWorkService.getPosWorkAll();
    }

    @RequestMapping("getPosWorkCount")
    @ResponseBody
    public JSONObject getPosWorkCount() {
        return posWorkService.getPosWorkCount();
    }

    @RequestMapping("changePosWork")
    @ResponseBody
    public JSONObject changePosWork() {
        return posWorkService.changePosWork();
    }

    /**
     * 新增交班信息
     * @author oy
     * @date 2019/5/7
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("addBusinessShift")
    @ResponseBody
    public JSONObject addBusinessShift() {
        return posWorkService.addBusinessShift();
    }

    /**
     * 结算交班
     * @author oy
     * @date 2019/5/6
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("endBusinessShift")
    @ResponseBody
    public JSONObject endBusinessShift() {
        return posWorkService.endBusinessShift();
    }

    @RequestMapping("isExchangeDuty")
    @ResponseBody
    public JSONObject isExchangeDuty() {
        return posWorkService.isExchangeDuty();
    }
    @RequestMapping("addPosWorkModel")
    @ResponseBody
    public JSONObject addPosWorkModel() {
        return posWorkService.addPosWorkModel();
    }

    @RequestMapping("getPosWrokListByMap")
    @ResponseBody
    public JSONObject getPosWrokListByMap() {
        return posWorkService.getPosWrokListByMap();
    }

    @RequestMapping("getPosWrokDetil")
    @ResponseBody
    public JSONObject getPosWrokDetil() {
        return posWorkService.getPosWrokDetil();
    }

    @RequestMapping("getPosWorkByUserId")
    @ResponseBody
    public JSONObject getPosWorkByUserId() {
        return posWorkService.getPosWorkByUserId();
    }


}
