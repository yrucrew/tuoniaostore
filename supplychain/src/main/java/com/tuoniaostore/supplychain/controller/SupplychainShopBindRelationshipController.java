package com.tuoniaostore.supplychain.controller;
        import com.tuoniaostore.supplychain.service.SupplychainShopBindRelationshipService;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 15:31:24
 */
@Controller
    @RequestMapping("supplychainShopBindRelationship")
public class SupplychainShopBindRelationshipController {
    @Autowired
    private SupplychainShopBindRelationshipService supplychainShopBindRelationshipService;


    @RequestMapping("addSupplychainShopBindRelationship")
    @ResponseBody
    public JSONObject addSupplychainShopBindRelationship() {
        return supplychainShopBindRelationshipService.addSupplychainShopBindRelationship();
    }

    @RequestMapping("getSupplychainShopBindRelationship")
    @ResponseBody
    public JSONObject getSupplychainShopBindRelationship() {
        return supplychainShopBindRelationshipService.getSupplychainShopBindRelationship();
    }


    @RequestMapping("getSupplychainShopBindRelationships")
    @ResponseBody
    public JSONObject getSupplychainShopBindRelationships() {
        return supplychainShopBindRelationshipService.getSupplychainShopBindRelationships();
    }

    @RequestMapping("getSupplychainShopBindRelationshipAll")
    @ResponseBody
    public JSONObject getSupplychainShopBindRelationshipAll() {
        return supplychainShopBindRelationshipService.getSupplychainShopBindRelationshipAll();
    }

    @RequestMapping("getSupplychainShopBindRelationshipCount")
    @ResponseBody
    public JSONObject getSupplychainShopBindRelationshipCount() {
        return supplychainShopBindRelationshipService.getSupplychainShopBindRelationshipCount();
    }

    @RequestMapping("changeSupplychainShopBindRelationship")
    @ResponseBody
    public JSONObject changeSupplychainShopBindRelationship() {
        return supplychainShopBindRelationshipService.changeSupplychainShopBindRelationship();
    }
    
    /** 获取采购绑定的仓库 */
    @RequestMapping("getPurchaseBindShops")
    @ResponseBody
    public JSONObject getPurchaseBindShops() {
        return supplychainShopBindRelationshipService.getPurchaseBindShops();
    }
    
    @ApiOperation("采购员绑定仓库和供应商")
	@RequestMapping("purchaseBindShop")
	@ResponseBody
	public JSONObject purchaseBindShop() {
		return supplychainShopBindRelationshipService.purchaseBindShop();
	}
	
	/**
	 * 后台系统：采购员查询仓库/供应商列表，查询使用
	 * @return
	 */
	@RequestMapping("getSupplychainShopPropertiesList")
	@ResponseBody
	public JSONObject getSupplychainShopPropertiesList() {
		return supplychainShopBindRelationshipService.getSupplychainShopPropertiesList();
	}
	/**
	 * 查询用户商店绑定关系
	 * @author sqd
	 * @date 2019/5/10
	 * @param
	 * @return com.alibaba.fastjson.JSONObject
	 */
	@RequestMapping("getShopBindRelationships")
	@ResponseBody
	public JSONObject getShopBindRelationships() {
		return supplychainShopBindRelationshipService.getShopBindRelationships();
	}

    @ApiOperation(value = "POS-门店管理-员工管理-列表")
    @RequestMapping(value = "getSysUsersListByPos")
    @ResponseBody
    public JSONObject getSysUsersListByPos() {
        return supplychainShopBindRelationshipService.getSysUsersListByPos();
    }

    @ApiOperation(value = "POS-门店管理-员工管理-新增员工")
    @RequestMapping("addSysUserByPos")
    @ResponseBody
    public JSONObject addSysUserByPos() {
        return supplychainShopBindRelationshipService.addSysUserByPos();
    }

    @ApiOperation(value = "POS-门店管理-员工管理-编辑")
    @RequestMapping("editSysUserByPos")
    @ResponseBody
    public JSONObject editSysUserByPos() {
        return supplychainShopBindRelationshipService.editSysUserByPos();
    }

    @ApiOperation(value = "POS-门店管理-员工管理-冻结")
    @RequestMapping(value = "forbidSysUserByPos")
    @ResponseBody
    public JSONObject forbidSysUserByPos() {
        return supplychainShopBindRelationshipService.forbidSysUserByPos();
    }

    @ApiOperation(value = "POS-门店管理-员工管理-删除")
    @RequestMapping(value = "deleteSysUserByPos")
    @ResponseBody
    public JSONObject deleteSysUserByPos() {
        return supplychainShopBindRelationshipService.delSysUserByPos();
    }

}
