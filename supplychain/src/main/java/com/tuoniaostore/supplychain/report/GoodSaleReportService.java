package com.tuoniaostore.supplychain.report;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.support.good.GoodPricePosRemoteService;
import com.tuoniaostore.commons.support.order.OrderEntryPosRemoteService;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopGoodSale;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.datamodel.vo.order.GoodSaleMsgVO;
import com.tuoniaostore.supplychain.cache.SupplychainDataAccessManager;
import com.tuoniaostore.supplychain.service.SupplychainShopGoodSaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author oy
 * @description
 * @date 2019/5/20
 */
@Service
public class GoodSaleReportService extends BasicWebService {

    @Autowired
    private SupplychainDataAccessManager dataAccessManager;

    /**
     * 每天凌晨1点半  获取定时统计昨天商品的销售情况
     * @author oy
     * @date 2019/5/20
     * @param
     * @return void
     */
    @Scheduled(cron = "0 30 0 * * * ")
    public void updateTodayReport(){
        // 清空表
//        dataAccessManager.clearSupplyShopGoodSale();

        System.out.println("定时器启动------------------"+System.currentTimeMillis());
        //获取每个门店
        String type= "6,7";
        List<SupplychainShopProperties> supplychainShopPropertiesAll = dataAccessManager.getSupplychainShopPropertiesAll(type);
        if(supplychainShopPropertiesAll != null && supplychainShopPropertiesAll.size() > 0){
            supplychainShopPropertiesAll.forEach(x->{//遍历每个商店 查找对应的订单
                //获取昨天的商品销售情况
                String shopId = x.getId();
                List<GoodSaleMsgVO> orderMsg = OrderEntryPosRemoteService.getOrderEntryForYesterDay(shopId);//统计
                //遍历每个订单商品 获取
                if(orderMsg != null && orderMsg.size() > 0){
                    Map<String,SupplychainShopGoodSale> mapSaveEntity = new HashMap<>();//当前商店需要保存统计好的数据 tempalte - SupplychainShopGoodSale
                    List<SupplychainShopGoodSale> addList = new ArrayList<>(); //批量增加的记录
                    orderMsg.forEach(y -> {
                        //将数据 准备好 存入报表数据
                        SupplychainShopGoodSale entity = new SupplychainShopGoodSale();
                        //根据商品id 获取对应的规格 条码
                        String goodId = y.getGoodId();
                        if(goodId != null && !goodId.equals("")){//说明是门店的商品
                            //查询对应的商品信息
                            SupplychainGood supplychainGood = dataAccessManager.getSupplychainGood(goodId);
                            if(supplychainGood != null){
                                String priceId = supplychainGood.getPriceId();//价格id
                                if(priceId != null && !priceId.equals("")){
                                    GoodPriceCombinationVO templateByPriceIdByParam = GoodPricePosRemoteService.getDateByPriceId(priceId);
                                    String goodTemplateId = templateByPriceIdByParam.getGoodTemplateId();
                                    if(goodTemplateId != null && !goodTemplateId.equals("")){
                                        SupplychainShopGoodSale supplychainShopGoodSale = mapSaveEntity.get(goodTemplateId);
                                        if(supplychainShopGoodSale != null){
                                            BigDecimal saleNum = supplychainShopGoodSale.getSaleNum();
                                            BigDecimal saleNum1 = y.getSaleNum();
                                            saleNum.add(saleNum1);
                                            supplychainShopGoodSale.setSaleNum(saleNum);

                                            BigDecimal saleMoney = supplychainShopGoodSale.getSaleMoney();
                                            BigDecimal saleTotlePrice = y.getSaleTotlePrice();
                                            if(!saleTotlePrice.equals(BigDecimal.ZERO)){
                                                saleMoney.add(saleTotlePrice);
                                            }else{
                                                saleMoney.add(BigDecimal.ZERO);
                                            }
                                            supplychainShopGoodSale.setSaleMoney(saleMoney);
                                        }else{
                                            entity.setShopId(x.getId());//商店id
                                            entity.setGoodBarcode(templateByPriceIdByParam.getBarcode());//条码
                                            entity.setGoodName(y.getGoodName());//商品名称（pos机的数据）
                                            entity.setGoodTempalteId(goodTemplateId);//模板id
                                            entity.setGoodType(templateByPriceIdByParam.getTypeNameOneName());//商品名称  一级分类名称
                                            entity.setGoodTypeId(templateByPriceIdByParam.getTypeNameOneId());//商品id 一级分类id
                                            entity.setGoodUnitId(templateByPriceIdByParam.getUnitTitleId());//单位id
                                            entity.setGoodUnit(templateByPriceIdByParam.getUnitTitle());//单位名称
                                            entity.setSaleNum(y.getSaleNum());//销售数量
                                            entity.setSaleMoney(y.getSaleTotlePrice());//销售金额
                                            entity.setSaleTime(y.getCreateDate());//时间
                                            mapSaveEntity.put(entity.getGoodTempalteId(),entity);//加入map中
                                        }
                                    }
                                }
                            }
                        }else{//说明是其他
                            entity.setGoodName(y.getGoodName());//商品名称（pos机的数据）
                            entity.setSaleNum(y.getSaleNum());//销售数量
                            entity.setSaleMoney(y.getSaleTotlePrice());//销售金额
                            entity.setShopId(shopId);//商店id
                            mapSaveEntity.put(entity.getGoodTempalteId(),entity);//加入map中
                        }
                    });
                    //如果不为空 就批量增加
                    if(mapSaveEntity != null && mapSaveEntity.entrySet().size() > 0){
                        mapSaveEntity.forEach((k,v)->{
                            addList.add(v);
                        });
                    }
                    //批量增加
                    if(addList != null && addList.size() > 0){
                        dataAccessManager.batchAddSupplychainShopGoodSales(addList);
                    }
                }
            });
        }
        System.out.println("定时器结束------------------"+System.currentTimeMillis());
    }

}
