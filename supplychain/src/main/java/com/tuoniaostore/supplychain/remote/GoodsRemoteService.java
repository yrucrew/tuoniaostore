package com.tuoniaostore.supplychain.remote;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.*;
import com.tuoniaostore.commons.support.good.*;
import com.tuoniaostore.commons.thread.AsyncScheduledService;
import com.tuoniaostore.datamodel.good.*;
import com.tuoniaostore.supplychain.config.DomainNameConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by liyunbiao on 2019/6/1.
 */
@Component
public class GoodsRemoteService {

    private static final Logger logger = LoggerFactory.getLogger(GoodsRemoteService.class);
    // 商品单位专用读写锁
    private static final ReentrantReadWriteLock GOODS_UNIT_READ_WRITE_LOCK = new ReentrantReadWriteLock();
    // 商品单位专用写锁
    private static final ReentrantReadWriteLock.WriteLock GOODS_UNIT_WRITE_LOCK = GOODS_UNIT_READ_WRITE_LOCK.writeLock();
    // 所有商品单位映射关系
    private static final Map<String, GoodUnit> goodsUnitMap = new ConcurrentHashMap<>();
    // 商品适条码专用读写锁
    private static final ReentrantReadWriteLock GOODBARCODE_READ_WRITE_LOCK = new ReentrantReadWriteLock();
    // 商品单位专用写锁
    private static final ReentrantReadWriteLock.WriteLock GOODBARCODE_UNIT_WRITE_LOCK = GOODS_UNIT_READ_WRITE_LOCK.writeLock();
    // 所有商品单位映射关系
    private static final Map<String, GoodBarcode> goodBarcodeMap = new ConcurrentHashMap<>();
    private static final Map<String, List<GoodBarcode>> goodPriceBarcodeMap = new ConcurrentHashMap<>();


    // 商品品牌专用读写锁
    private static final ReentrantReadWriteLock BRAND_READ_WRITE_LOCK = new ReentrantReadWriteLock();
    // 商品品牌专用写锁
    private static final ReentrantReadWriteLock.WriteLock BRAND_WRITE_LOCK = BRAND_READ_WRITE_LOCK.writeLock();
    // 所有商品品牌映射关系
    private static final Map<String, GoodBrand> goodsBrandMap = new ConcurrentHashMap<>();


    // 商品分类专用读写锁
    private static final ReentrantReadWriteLock TYPE_READ_WRITE_LOCK = new ReentrantReadWriteLock();
    // 商品分类专用写锁
    private static final ReentrantReadWriteLock.WriteLock TYPE_WRITE_LOCK = TYPE_READ_WRITE_LOCK.writeLock();
    // 所有商品分类映射关系
    private static final Map<String, GoodType> goodTypeMap = new ConcurrentHashMap<>();

    // 商品模版专用读写锁
    private static final ReentrantReadWriteLock GOODSTEMPLATE_READ_WRITE_LOCK = new ReentrantReadWriteLock();
    // 商品模版专用写锁
    private static final ReentrantReadWriteLock.WriteLock GOODSTEMPLATE_WRITE_LOCK = GOODSTEMPLATE_READ_WRITE_LOCK.writeLock();
    // 所有商品模版映射关系
    private static final Map<String, GoodTemplate> goodTemplateMap = new ConcurrentHashMap<>();

    // 商品标签专用读写锁
    private static final ReentrantReadWriteLock GOODS_PRICE_READ_WRITE_LOCK = new ReentrantReadWriteLock();
    // 商品标签专用写锁
    private static final ReentrantReadWriteLock.WriteLock GOODS_PRICE_WRITE_LOCK = GOODS_PRICE_READ_WRITE_LOCK.writeLock();
    // 所有商品标签映射关系
    private static final Map<String, GoodPrice> goodPriceMap = new ConcurrentHashMap<>();


    public static final long WAIT_LOCK_TIME_OUT = 2;
    public static final TimeUnit WAIT_LOCK_TIME_UNIT = TimeUnit.SECONDS;

    DomainNameConfig domainNameConfig;
    String encrypt;// = RSA.encrypt("supplychain",domainNameConfig.AccessKeySecretModulus, domainNameConfig.AccessKeySecretExponent);
    String token;// = TokenUtils.AddToken(encrypt);
    String signature;// = (MD5.encode(token + "&" + 2 + "&" + domainNameConfig.appKey));


    public void initGoodsCache(DomainNameConfig domainNameConfig) {
        this.domainNameConfig = domainNameConfig;
        JSONObject json = new JSONObject();
        json.put("id", System.currentTimeMillis() + "");
        json.put("defaultName", "内部系统" + System.currentTimeMillis() + "");
        json.put("phoneNumber", System.currentTimeMillis() + "");
        json.put("realName", System.currentTimeMillis() + "");
        json.put("roleId", System.currentTimeMillis() + "");


        encrypt = RSA.encrypt(json.toJSONString(), domainNameConfig.AccessKeySecretModulus, domainNameConfig.AccessKeySecretExponent);
        token = TokenUtils.AddToken(encrypt);
        signature = (MD5.encode(token + "&" + 2 + "&" + domainNameConfig.appKey));
        loaderGoodsUnits();
        loaderGoodBrand();
        loaderGoodTyp();
        loaderGoodTemplate();
        loaderGoodPrice();
        loaderGoodBarcodes();
    }

    public GoodUnit getGoodsUnit(String id) {
        if (goodsUnitMap.get(id) != null) {
            return goodsUnitMap.get(id);
        } else {
            return GoodUnitRemoteService.getGoodUnitById(id, token, signature, domainNameConfig.appKey);
        }
    }

    public List<GoodBarcode> getGoodBarcode(String pricId) {
        return goodPriceBarcodeMap.get(pricId);
    }

    @Scheduled(cron = "0 0/5 * * * * ")
    public void loaderGoodBarcodes() {
        AsyncScheduledService.submitDataSaveTask(new Runnable() {
            @Override
            public void run() {
                List<GoodBarcode> goodBarcodes = GoodBarcodeRemoteService.loadGoodBarcodes(token, domainNameConfig.appKey, signature);
                Map<String, GoodBarcode> tmpGoodBarcodeMap = new ConcurrentHashMap<>();
                Map<String, List<GoodBarcode>> tmpGoodBarcodePriceMap = new ConcurrentHashMap<>();
                goodBarcodeMap.clear();
                for (GoodBarcode goodBarcode : goodBarcodes) {
                    tmpGoodBarcodeMap.put(goodBarcode.getId(), goodBarcode);
                    List<GoodBarcode> list = tmpGoodBarcodePriceMap.get(goodBarcode.getPriceId());
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    list.add(goodBarcode);
                    tmpGoodBarcodePriceMap.put(goodBarcode.getPriceId(), list);
                }
                try {
                    if (GOODS_UNIT_WRITE_LOCK.tryLock(WAIT_LOCK_TIME_OUT, WAIT_LOCK_TIME_UNIT)) {
                        try {
                            goodBarcodeMap.clear();
                            goodBarcodeMap.putAll(tmpGoodBarcodeMap);
                            goodPriceBarcodeMap.putAll(tmpGoodBarcodePriceMap);
                            logger.info("===================供应链中心loaderGoodBarcodes=" + goodBarcodeMap.size());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            GOODS_UNIT_WRITE_LOCK.unlock();
                        }
                    }
                } catch (Throwable cause) {
                    cause.printStackTrace();
                }
            }
        }, 3, TimeUnit.SECONDS);
    }


    @Scheduled(cron = "0 0/5 * * * * ")
    public void loaderGoodsUnits() {
        AsyncScheduledService.submitDataSaveTask(new Runnable() {
            @Override
            public void run() {
                List<GoodUnit> goodsUnits = GoodUnitRemoteService.loadGoodsUnits(token, domainNameConfig.appKey, signature);
                Map<String, GoodUnit> tmpGoodsUnitMap = new ConcurrentHashMap<>();

                goodsUnitMap.clear();
                for (GoodUnit goodUnit : goodsUnits) {
                    tmpGoodsUnitMap.put(goodUnit.getId(), goodUnit);
                }
                try {
                    if (GOODS_UNIT_WRITE_LOCK.tryLock(WAIT_LOCK_TIME_OUT, WAIT_LOCK_TIME_UNIT)) {
                        try {
                            goodsUnitMap.clear();
                            goodsUnitMap.putAll(tmpGoodsUnitMap);
                            logger.info("===================供应链中心loaderGoodsUnits=" + goodsUnitMap.size());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            GOODS_UNIT_WRITE_LOCK.unlock();
                        }
                    }
                } catch (Throwable cause) {
                    cause.printStackTrace();
                }
            }
        }, 3, TimeUnit.SECONDS);
    }

    public GoodBrand getGoodBrand(String id) {
        if (goodsBrandMap.get(id) != null) {
            return goodsBrandMap.get(id);
        } else {
            return GoodBrandRemoteService.getGoodBrand(id, token, signature, domainNameConfig.appKey);
        }
    }

    @Scheduled(cron = "0 0/5 * * * * ")
    public void loaderGoodBrand() {
        AsyncScheduledService.submitDataSaveTask(new Runnable() {
            @Override
            public void run() {
                List<GoodBrand> goodBrands = GoodBrandRemoteService.loadGoodBrands(token, signature, domainNameConfig.appKey);
                Map<String, GoodBrand> tmpGoodsBrandMap = new ConcurrentHashMap<>();

                for (GoodBrand brand : goodBrands) {
                    tmpGoodsBrandMap.put(brand.getId(), brand);
                }
                try {
                    if (BRAND_WRITE_LOCK.tryLock(WAIT_LOCK_TIME_OUT, WAIT_LOCK_TIME_UNIT)) {
                        try {
                            goodsBrandMap.clear();
                            goodsBrandMap.putAll(tmpGoodsBrandMap);
                            logger.info("===================供应链中心loaderGoodBrand=" + goodsBrandMap.size());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            try {
                                BRAND_WRITE_LOCK.unlock();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Throwable cause) {
                    cause.printStackTrace();
                }
            }
        }, 3, TimeUnit.SECONDS);

    }

    public GoodType getGoodType(String id) {
        try {
            if (goodTypeMap.get(id) != null) {
                return goodTypeMap.get(id);
            } else {
                GoodType goodType = GoodTypeRemoteService.getGoodType(id, token, signature, domainNameConfig.appKey);
                if (goodType != null) {
                    goodTypeMap.put(goodType.getId(), goodType);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Scheduled(cron = "0 0/5 * * * * ")
    public void loaderGoodTyp() {
        AsyncScheduledService.submitDataSaveTask(new Runnable() {
            @Override
            public void run() {
                List<GoodType> goodTypes = GoodTypeRemoteService.loadGoodTypes(token, signature, domainNameConfig.appKey);
                Map<String, GoodType> tmpGoodsTypeMap = new ConcurrentHashMap<>();

                for (GoodType type : goodTypes) {
                    tmpGoodsTypeMap.put(type.getId(), type);
                }
                goodTypeMap.clear();
                try {
                    if (TYPE_WRITE_LOCK.tryLock(WAIT_LOCK_TIME_OUT, WAIT_LOCK_TIME_UNIT)) {
                        try {
                            goodTypeMap.clear();
                            goodTypeMap.putAll(tmpGoodsTypeMap);
                            logger.info("===================供应链中心loaderGoodTyp=" + goodTypeMap.size());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            TYPE_WRITE_LOCK.unlock();
                        }
                    }
                } catch (Throwable cause) {
                    cause.printStackTrace();
                }
            }
        }, 3, TimeUnit.SECONDS);
    }

    public GoodTemplate getGoodTemplate(String id) {
        if (goodTemplateMap.get(id) != null) {
            return goodTemplateMap.get(id);
        } else {
            return GoodTemplateRemoteService.getGoodTemplate(id, token, signature, domainNameConfig.appKey);
        }
    }

    @Scheduled(cron = "0 0/5 * * * * ")
    public void loaderGoodTemplate() {
        AsyncScheduledService.submitDataSaveTask(new Runnable() {
            @Override
            public void run() {
                List<GoodTemplate> goodTemplates = GoodTemplateRemoteService.loadGoodTemplates(token, signature, domainNameConfig.appKey);
                Map<String, GoodTemplate> tmpGoodTemplateMap = new ConcurrentHashMap<>();

                for (GoodTemplate goodTemplate : goodTemplates) {
                    tmpGoodTemplateMap.put(goodTemplate.getId(), goodTemplate);
                }
                goodTemplateMap.clear();
                try {
                    if (GOODSTEMPLATE_WRITE_LOCK.tryLock(WAIT_LOCK_TIME_OUT, WAIT_LOCK_TIME_UNIT)) {
                        try {
                            goodTemplateMap.clear();
                            goodTemplateMap.putAll(tmpGoodTemplateMap);
                            logger.info("===================供应链中心loaderGoodTemplate=" + goodTemplateMap.size());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            GOODSTEMPLATE_WRITE_LOCK.unlock();
                        }
                    }
                } catch (Throwable cause) {
                    cause.printStackTrace();
                }
            }
        }, 3, TimeUnit.SECONDS);
    }

    public GoodPrice getGoodPrice(String id) {

        if (goodPriceMap.get(id) != null) {
            return goodPriceMap.get(id);
        } else {
            return GoodPriceRemoteService.getGoodPrice(id, token, signature, domainNameConfig.appKey);
        }
    }

    @Scheduled(cron = "0 0/5 * * * * ")
    public void loaderGoodPrice() {
        AsyncScheduledService.submitDataSaveTask(new Runnable() {
            @Override
            public void run() {
                List<GoodPrice> goodPrices = GoodPriceRemoteService.loadGoodPrices(token, signature, domainNameConfig.appKey);
                Map<String, GoodPrice> tmpGoodPriceMap = new ConcurrentHashMap<>();
                for (GoodPrice goodPrice : goodPrices) {
                    tmpGoodPriceMap.put(goodPrice.getId(), goodPrice);
                }
                goodPriceMap.clear();
                try {
                    if (GOODS_PRICE_WRITE_LOCK.tryLock(WAIT_LOCK_TIME_OUT, WAIT_LOCK_TIME_UNIT)) {
                        try {
                            goodPriceMap.clear();
                            goodPriceMap.putAll(tmpGoodPriceMap);
                            logger.info("===================供应链中心loaderGoodPrice=" + goodPriceMap.size());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            GOODS_PRICE_WRITE_LOCK.unlock();
                        }
                    }
                } catch (Throwable cause) {
                    cause.printStackTrace();
                }
            }
        }, 3, TimeUnit.SECONDS);


    }
}
