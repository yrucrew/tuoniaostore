package com.tuoniaostore.supplychain.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by liyunbiao on 2019/1/12.
 */
@Configuration
public class DomainNameConfig {
    @Value("${communityRemoteURL}")
    public String communityRemoteURL;
    @Value("${goodRemoteURL}")
    public String goodRemoteURL;
    @Value("${logisticsRemoteURL}")
    public String logisticsRemoteURL;
    @Value("${orderRemoteURL}")
    public String orderRemoteURL;
    @Value("${paymentRemoteURL}")
    public String paymentRemoteURL;
    @Value("${supplychainRemoteURL}")
    public String supplychainRemoteURL;

    @Value("${AccessKeySecretModulus}")
    public String AccessKeySecretModulus;
    @Value("${AccessKeySecretExponent}")
    public String AccessKeySecretExponent;
    @Value("${appKey}")
    public String appKey;
}
