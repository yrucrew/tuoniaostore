package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.good.GoodShopCart;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.user.SysUserAddress;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysUserAddressMapper {

    String column = "`id`, " +//
            "`user_id`, " +//通行证ID
            "`address_alias`, " +//地址别名
            "`name`, " +//地址联系人姓名
            "`phone_number`, " +//联系人电话
            "`country`, " +//国家
            "`province`, " +//省份
            "`city`, " +//城市
            "`district`, " +//行政区域
            "`street`, " +//街道名
            "`street_num`, " +//街道号
            "`display_name`, " +//对应百度地图API的displayName(name)
            "`detail_address`, " +//详细地址
            "`adcode`, " +//城市编码(国标)
            "`longitude`, " +//经度
            "`latitude`, " +//纬度
            "`type`, " +//类型；0-收货地址；1-开拓签约地址
            "`status`, " +//状态，0-启用；1-失效；2-默认
            "`create_time`, " +//建立时间
            "`sex`, " +//0-女性；1-男性
            "`sort`"//
            ;

    @Insert("insert into sys_user_address (" +
            " `id`,  " +
            " `user_id`,  " +
            " `address_alias`,  " +
            " `name`,  " +
            " `phone_number`,  " +
            " `country`,  " +
            " `province`,  " +
            " `city`,  " +
            " `district`,  " +
            " `street`,  " +
            " `street_num`,  " +
            " `display_name`,  " +
            " `detail_address`,  " +
            " `adcode`,  " +
            " `longitude`,  " +
            " `latitude`,  " +
            " `type`,  " +
            " `status`,  " +
            " `sex`,  " +
            " `sort` " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{addressAlias},  " +
            "#{name},  " +
            "#{phoneNumber},  " +
            "#{country},  " +
            "#{province},  " +
            "#{city},  " +
            "#{district},  " +
            "#{street},  " +
            "#{streetNum},  " +
            "#{displayName},  " +
            "#{detailAddress},  " +
            "#{adcode},  " +
            "#{longitude},  " +
            "#{latitude},  " +
            "#{type},  " +
            "#{status},  " +
            "#{sex},  " +
            "#{sort} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysUserAddress(SysUserAddress sysUserAddress);

    @Results(id = "sysUserAddress", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"), @Result(property = "addressAlias", column = "address_alias"), @Result(property = "name", column = "name"), @Result(property = "phoneNumber", column = "phone_number"), @Result(property = "country", column = "country"), @Result(property = "province", column = "province"), @Result(property = "city", column = "city"), @Result(property = "district", column = "district"), @Result(property = "street", column = "street"), @Result(property = "streetNum", column = "street_num"), @Result(property = "displayName", column = "display_name"), @Result(property = "detailAddress", column = "detail_address"), @Result(property = "adcode", column = "adcode"), @Result(property = "longitude", column = "longitude"), @Result(property = "latitude", column = "latitude"), @Result(property = "type", column = "type"), @Result(property = "status", column = "status"), @Result(property = "createTime", column = "create_time"), @Result(property = "sex", column = "sex"), @Result(property = "sort", column = "sort")})
    @Select("select " + column + " from sys_user_address where id = #{id}")
    SysUserAddress getSysUserAddress(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_user_address   where  1=1" +
            " <when test=\"status != null and status != -1\">" +
            "  and  status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysUserAddress")
    List<SysUserAddress> getSysUserAddresss(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_user_address   where    status=0 </script>")
    @ResultMap("sysUserAddress")
    List<SysUserAddress> getSysUserAddressAll();

    @Select("<script>select count(1) from sys_user_address   where  1=1" +
            " <when test=\"status != null and status != -1\">" +
            "  and  status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "</script>")
    int getSysUserAddressCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_user_address  " +
            "set " +
            "`address_alias` = #{addressAlias}  , " +
            "`name` = #{name}  , " +
            "`phone_number` = #{phoneNumber}  , " +
            "`country` = #{country}  , " +
            "`province` = #{province}  , " +
            "`city` = #{city}  , " +
            "`district` = #{district}  , " +
            "`street` = #{street}  , " +
            "`street_num` = #{streetNum}  , " +
            "`display_name` = #{displayName}  , " +
            "`detail_address` = #{detailAddress}  , " +
            "`adcode` = #{adcode}  , " +
            "`longitude` = #{longitude}  , " +
            "`latitude` = #{latitude}  , " +
            "`type` = #{type}  , " +
            "`status` = #{status}  , " +
            "`sex` = #{sex}  , " +
            "`sort` = #{sort}  " +
            " where id = #{id}")
    void changeSysUserAddress(SysUserAddress sysUserAddress);

    @Select("select  " + column + " from sys_user_address   where user_id = #{userId} and status != 1 and type = 0")
    @ResultMap("sysUserAddress")
    List<SysUserAddress> getMyAddress(@Param("userId") String userId);

    @Update("update sys_user_address set status = 0 where user_id = #{userId} and status != 1")
    void clearSysUserDefaultAddress(@Param("userId")String userId);

    @Update("update sys_user_address set status = 2 where id = #{id}")
    void changeSysUserDefaultAddress(@Param("id") String id);

    @Update("update sys_user_address set status = 1 where id = #{id}")
    void deleteSysUserDefaultAddressById(String id);

    @Select("select  " + column + " from sys_user_address   where user_id = #{userId} and status != 1 and type = 0 order by create_time desc limit 1")
    @ResultMap("sysUserAddress")
    SysUserAddress getSysUserAddressByUserId(@Param("userId") String userId);

    @Select("select "+column+" from sys_user_address where user_id = #{map.userId} and status = 2 and type = 0")
    @ResultMap("sysUserAddress")
    List<GoodShopCart> getGoodShopCartByDefault(@Param("map") Map<String,Object> map);

    @Select("select  " + column + " from sys_user_address   where user_id = #{userId} and status = 2 limit 1")
    @ResultMap("sysUserAddress")
    SysUserAddress getDefaultSysUserAddress(@Param("userId") String userId);
}
