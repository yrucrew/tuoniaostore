package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysRoleMenu;
import com.tuoniaostore.datamodel.user.SysRoleMenuPos;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色与菜单对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysRoleMenuMapper {

    String column = "`id`, " +//
            "`role_id`, " +//角色ID
            "`menu_id`, " +//菜单ID
            "`sys_channel_id`, " +//
            "`create_time`, " +//
            "`user_id`, " +//
            "`retrieve_status`"//
            ;

    @Insert("<script>INSERT INTO  `sys_role_menu`( `id`,`role_id`,`menu_id`,`sys_channel_id`,`user_id`)" +
            "VALUES" +
            "<foreach collection='list' item='sysRoleMenu' separator=','  >" +
            "(#{sysRoleMenu.id}, #{sysRoleMenu.roleId}, #{sysRoleMenu.menuId}, #{sysRoleMenu.sysChannelId},#{sysRoleMenu.userId})" +
            "</foreach></script>")
    void addSysRoleMenu(List<SysRoleMenu> list);

    @Results(id = "sysRoleMenu", value = {
            @Result(property = "id", column = "id"), @Result(property = "roleId", column = "role_id"), @Result(property = "menuId", column = "menu_id"), @Result(property = "sysChannelId", column = "sys_channel_id"), @Result(property = "createTime", column = "create_time"),
            @Result(property = "userId", column = "user_id"), @Result(property = "retrieveStatus", column = "retrieve_status")})
    @Select("select " + column + " from sys_role_menu where id = #{id}")
    SysRoleMenu getSysRoleMenu(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_role_menu   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysRoleMenu")
    List<SysRoleMenu> getSysRoleMenus( @Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_role_menu   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysRoleMenu")
    List<SysRoleMenu> getSysRoleMenuAll();

    @Select("<script>select count(1) from sys_role_menu   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysRoleMenuCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_role_menu  " +
            "set " +
            "`role_id` = #{roleId}  , " +
            "`menu_id` = #{menuId}  , " +
            "`sys_channel_id` = #{sysChannelId}  , " +
            "`create_time` = #{createTime}    " +
            " where id = #{id}")
    void changeSysRoleMenu(SysRoleMenu sysRoleMenu);

    @Select("SELECT  " + column + "  FROM `sys_role_menu`  WHERE   role_id=#{roleId} and  retrieve_status=0 ")
    @ResultMap("sysRoleMenu")
    List<SysRoleMenu> getSysRoleMenusByRoleId(@Param("roleId") String roleId);

    @Delete("DELETE FROM `sys_role_menu` where `role_id`=#{roleId}")
    void delSysRoleMenuByRoleId(@Param("roleId") String roleId);

    @Update("<script> update sys_role_menu set retrieve_status = 1 where menu_id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    void deleteSysRoleMenu(@Param("lists") List<String> menuIds);

}
