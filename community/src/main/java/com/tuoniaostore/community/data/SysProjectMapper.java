package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysProject;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
@Component
@Mapper
public interface SysProjectMapper {

    String column = "`id`, " +//项目ID
            "`name`, " +//项目名称
            "`sort`, " +//
            "`logo`, " +//项目logo
            "`parent_id`, " +
            "`imgUrl`"//项目封面
            ;

    @Insert("insert into sys_project (" +
            " `id`,  " +
            " `name`,  " +
            " `sort`,  " +
            " `logo`,  " +
            " `parent_id`,  " +
            " `imgUrl` " +
            ")values(" +
            "#{id},  " +
            "#{name},  " +
            "#{sort},  " +
            "#{logo},  " +
            "#{parentId},  " +
            "#{imgurl} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysProject(SysProject sysProject);

    @Results(id = "sysProject", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "logo", column = "logo"),
            @Result(property = "parentId", column = "parent_id"),
            @Result(property = "imgurl", column = "imgUrl")})
    @Select("select " + column + " from sys_project where id = #{id}")
    SysProject getSysProject(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_project   where   1=1  " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysProject")
    List<SysProject> getSysProjects(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_project    </script>")
    @ResultMap("sysProject")
    List<SysProject> getSysProjectAll();

    @Select("<script>select  " + column + "  from sys_project where parent_id =#{parentId}  </script>")
    @ResultMap("sysProject")
    List<SysProject> getSysProjectByParentId(@Param("parentId") String parentId);

    @Select("<script>select count(1) from sys_project   where     1=1  " +

            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysProjectCount(@Param("name") String name);

    @Update("update sys_project  " +
            "set " +
            "`name` = #{name}  , " +
            "`sort` = #{sort}  , " +
            "`logo` = #{logo}  , " +
            "`parent_id` = #{parentId}  , " +
            "`imgUrl` = #{imgurl}  " +
            " where id = #{id}")
    void changeSysProject(SysProject sysProject);

}
