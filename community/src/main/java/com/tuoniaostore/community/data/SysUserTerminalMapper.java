package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysUserTerminal;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysUserTerminalMapper {

    String column = "`id`, " +//
            "`user_id`, " +//
            "`terminal_id`"//
            ;

    @Insert("insert into sys_user_terminal (" +
            " `id`,  " +
            " `user_id`,  " +
            " `terminal_id` " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{terminalId} " +
            ")")
    void addSysUserTerminal(SysUserTerminal sysUserTerminal);

    @Insert("<script>insert into sys_user_terminal( `id`,`user_id`,`terminal_id`) " +
            "<foreach collection='lists' item='sysUserTerminal' separator='UNION ALL'  >" +
            "( select  #{sysUserTerminal.id}, #{sysUserTerminal.userId},#{sysUserTerminal.terminalId} FROM DUAL " +
            " WHERE (SELECT COUNT(*) FROM sys_user_terminal  WHERE user_id = #{sysUserTerminal.userId} and " +
            " terminal_id = #{sysUserTerminal.terminalId} ) <![CDATA[<]]> 1 ) " +
            "</foreach></script>")
    void addSysUserTerminals(@Param("lists") List<SysUserTerminal> list);

    @Results(id = "sysUserTerminal", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"), @Result(property = "terminalId", column = "terminal_id")})
    @Select("select " + column + " from sys_user_terminal where id = #{id}  ")
    SysUserTerminal getSysUserTerminal(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_user_terminal   where   status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysUserTerminal")
    List<SysUserTerminal> getSysUserTerminals(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_user_terminal   where   status=0 </script>")
    @ResultMap("sysUserTerminal")
    List<SysUserTerminal> getSysUserTerminalAll();

    @Select("<script>select count(1) from sys_user_terminal   where  status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysUserTerminalCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_user_terminal  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`terminal_id` = #{terminalId}  " +
            " where id = #{id}")
    void changeSysUserTerminal(SysUserTerminal sysUserTerminal);

    @Delete("DELETE from sys_user_terminal where `user_id` = #{userId}  ")
    void delSysUserTerminal(@Param("userId") String userId);

    @Select("<script>select  " + column + "  from sys_user_terminal   where  user_id = #{userId} and terminal_id = #{terminalId} limit 1 </script>")
    @ResultMap("sysUserTerminal")
    SysUserTerminal getSysUserTerminal2(@Param("terminalId") String terminalId, @Param("userId") String userId);

    @Select("<script>select  " + column + "  from sys_user_terminal   where  user_id = #{userId} </script>")
    @ResultMap("sysUserTerminal")
    List<SysUserTerminal> getSysUserTerminalForManyToMany(@Param("userId") String userId);

}
