package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysMenuHistory;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysMenuHistoryMapper {

    String column = "`id`, " +//
            "`sys_menu_id`, " +//
            "`user_id`, " +//
            "`create_time`, " +//
            "`hit`, " +//
            "`last_time`, " +//
            "`sys_role_id`, " +//
            "`retrieve_status`"//
            ;

    @Insert("insert into sys_menu_history (" +
            " `id`,  " +
            " `sys_menu_id`,  " +
            " `user_id`,  " +
            " `hit`,  " +
            " `last_time`,  " +
            " `sys_role_id`  " +
            ")values(" +
            "#{id},  " +
            "#{sysMenuId},  " +
            "#{userId},  " +
            "#{hit},  " +
            "#{lastTime},  " +
            "#{sysRoleId}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysMenuHistory(SysMenuHistory sysMenuHistory);

    @Results(id = "sysMenuHistory", value = {
            @Result(property = "id", column = "id"), @Result(property = "sysMenuId", column = "sys_menu_id"),
            @Result(property = "userId", column = "user_id"), @Result(property = "createTime", column = "create_time"), @Result(property = "hit", column = "hit"), @Result(property = "lastTime", column = "last_time"), @Result(property = "sysRoleId", column = "sys_role_id"), @Result(property = "retrieveStatus", column = "retrieve_status")})
    @Select("select " + column + " from sys_menu_history where id = #{id}")
    SysMenuHistory getSysMenuHistory(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_menu_history   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysMenuHistory")
    List<SysMenuHistory> getSysMenuHistorys( @Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_menu_history   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysMenuHistory")
    List<SysMenuHistory> getSysMenuHistoryAll();

    @Select("<script>select count(1) from sys_menu_history   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysMenuHistoryCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_menu_history  " +
            "set " +
            "`sys_menu_id` = #{sysMenuId}  , " +
            "`create_time` = #{createTime}  , " +
            "`hit` = #{hit}  , " +
            "`last_time` = #{lastTime}  , " +
            "`sys_role_id` = #{sysRoleId}   " +
            " where id = #{id}")
    void changeSysMenuHistory(SysMenuHistory sysMenuHistory);

}
