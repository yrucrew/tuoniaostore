package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysUserPartner;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysUserPartnerMapper {

    String column = "`id`, " +//
            "`user_id`, " +//通行证ID
            "`name`, " +//合作方名字
            "`introduce`, " +//简介内容
            "`type`, " +//合作类型，暂时为1
            "`status`, " +//状态 1可用
            "`retrieve_status`, " +//回收站
            "`create_time`, " +//
            "`sort`"//排序
            ;

    @Insert("insert into sys_user_partner (" +
            " `id`,  " +
            " `user_id`,  " +
            " `name`,  " +
            " `introduce`,  " +
            " `type`,  " +
            " `status`,  " +
            " `sort` " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{name},  " +
            "#{introduce},  " +
            "#{type},  " +
            "#{status},  " +
            "#{sort} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysUserPartner(SysUserPartner sysUserPartner);

    @Results(id = "sysUserPartner", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "name", column = "name"), @Result(property = "introduce", column = "introduce"), @Result(property = "type", column = "type"), @Result(property = "status", column = "status"), @Result(property = "retrieveStatus", column = "retrieve_status"), @Result(property = "createTime", column = "create_time"), @Result(property = "sort", column = "sort")})
    @Select("select " + column + " from sys_user_partner where id = #{id}")
    SysUserPartner getSysUserPartner(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_user_partner   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysUserPartner")
    List<SysUserPartner> getSysUserPartners(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_user_partner   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysUserPartner")
    List<SysUserPartner> getSysUserPartnerAll();

    @Select("<script>select count(1) from sys_user_partner   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysUserPartnerCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_user_partner  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`name` = #{name}  , " +
            "`introduce` = #{introduce}  , " +
            "`type` = #{type}  , " +
            "`status` = #{status}  , " +
            "`sort` = #{sort}  " +
            " where id = #{id}")
    void changeSysUserPartner(SysUserPartner sysUserPartner);

}
