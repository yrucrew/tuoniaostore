package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysMenu;
import com.tuoniaostore.datamodel.user.SysMenuPos;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 菜单管理
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
@Component
@Mapper
public interface SysMenuPosMapper {

    String column = "`id`, " +//
            "`parent_id`, " +//父菜单ID，一级菜单为0
            "`name`, " +//菜单名称
            "`url`, " +//菜单URL
            "`perms`, " +//授权(多个用逗号分隔，如：user:list,user:create)
            "`type`, " +//类型   0：目录   1：菜单   2：按钮
            "`icon`, " +//菜单图标
            "`order_num`, " +//排序
            "`sort`, " +//
            "`status`, " +//
            "`level`, " +//级别
            "`create_time`, " +//
            "`user_id`, " +//
            "`retrieve_status`, " +//
            "`remark`"//
            ;

    @Insert("insert into sys_menu_pos (" +
            " `id`,  " +
            " `parent_id`,  " +
            " `name`,  " +
            " `url`,  " +
            " `perms`,  " +
            " `type`,  " +
            " `icon`,  " +
            " `order_num`,  " +
            " `sort`,  " +
            " `status`,  " +
            " `level`,  " +
            " `user_id`,  " +
            " `retrieve_status`,  " +
            " `remark` " +
            ")values(" +
            "#{id},  " +
            "#{parentId},  " +
            "#{name},  " +
            "#{url},  " +
            "#{perms},  " +
            "#{type},  " +
            "#{icon},  " +
            "#{orderNum},  " +
            "#{sort},  " +
            "#{status},  " +
            "#{level},  " +
            "#{userId},  " +
            "#{retrieveStatus},  " +
            "#{remark} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysMenuPos(SysMenuPos sysMenuPos);

    @Results(id = "sysMenuPos", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "parentId", column = "parent_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "url", column = "url"),
            @Result(property = "perms", column = "perms"),
            @Result(property = "type", column = "type"),
            @Result(property = "icon", column = "icon"),
            @Result(property = "orderNum", column = "order_num"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "status", column = "status"),
            @Result(property = "level", column = "level"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "retrieveStatus", column = "retrieve_status"),
            @Result(property = "remark", column = "remark")})
    @Select("select " + column + " from sys_menu_pos where id = #{id}")
    SysMenuPos getSysMenuPos(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_menu_pos   where   retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysMenuPos")
    List<SysMenuPos> getSysMenuPoss(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from sys_menu_pos   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysMenuPos")
    List<SysMenuPos> getSysMenuPosAll();

    @Select("<script>select count(1) from sys_menu_pos   where     retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysMenuPosCount(@Param("status") int status, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Update("update sys_menu_pos  " +
            "set " +
            "`parent_id` = #{parentId}  , " +
            "`name` = #{name}  , " +
            "`url` = #{url}  , " +
            "`perms` = #{perms}  , " +
            "`type` = #{type}  , " +
            "`icon` = #{icon}  , " +
            "`order_num` = #{orderNum}  , " +
            "`sort` = #{sort}  , " +
            "`status` = #{status}  , " +
            "`level` = #{level}  , " +
            "`create_time` = #{createTime}  , " +
            "`user_id` = #{userId}  , " +
            "`retrieve_status` = #{retrieveStatus}  , " +
            "`remark` = #{remark}  " +
            " where id = #{id}")
    void changeSysMenuPos(SysMenuPos sysMenuPos);

    @Select("<script> select " + column + " from sys_menu_pos where retrieve_status = 0  " +
            "<if test=\"lists != null\">and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            "</script>")
    List<SysMenuPos> getSysMenuPosByIds(@Param("lists") List<String> menuIds);

    @Select("<script>SELECT  " + column + "  FROM `sys_menu_pos`  where retrieve_status=0 and  `parent_id`=#{parentId}" +
            " <when test=\"menuIds != null \">" +
            " and  id in " +
            "<foreach collection='menuIds' item='menuId' open='(' separator=',' close=')'>" +
            "#{menuId}" +
            "</foreach>" +
            " </when>" +
            " order by sort desc </script>")
    @ResultMap("sysMenuPos")
    List<SysMenuPos> getSysMenuPosChilds(@Param("parentId") String parentId, @Param("menuIds") List<String> menuIds);

    @Update("<script>update sys_menu_pos set " +
            "retrieve_status = #{status}" +
            " where id in <foreach collection=\"menuIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    void updateSysMenuPosByIds(@Param("menuIds") List<String> menuIds, @Param("status")int status);
}
