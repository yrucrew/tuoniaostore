package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysConfig;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 系统配置信息表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysConfigMapper {

    String column = "`id`, " +//
            "`key`, " +//key
            "`value`, " +//value
            "`status`, " +//状态   0：隐藏   1：显示
            "`sort`, " +//
            "`remark`, " +//备注
            "`sys_channel_id`, " +//0.通用

            "`create_time`, " +//
            "`user_id`, " +//
            "`retrieve_status`"//
            ;

    @Insert("insert into sys_config (" +
            " `id`,  " +
            " `key`,  " +
            " `value`,  " +
            " `status`,  " +
            " `sort`,  " +
            " `remark`,  " +
            " `sys_channel_id`,  " +
            " `user_id`  " +
            ")values(" +
            "#{id},  " +
            "#{key},  " +
            "#{value},  " +
            "#{status},  " +
            "#{sort},  " +
            "#{remark},  " +
            "#{sysChannelId},  " +
            "#{userId} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysConfig(SysConfig sysConfig);

    @Results(id = "sysConfig", value = {
            @Result(property = "id", column = "id"), @Result(property = "key", column = "key"), @Result(property = "value", column = "value"), @Result(property = "status", column = "status"), @Result(property = "sort", column = "sort"), @Result(property = "remark", column = "remark"), @Result(property = "sysChannelId", column = "sys_channel_id"), @Result(property = "createTime", column = "create_time"),
            @Result(property = "userId", column = "user_id"), @Result(property = "retrieveStatus", column = "retrieve_status")})
    @Select("select " + column + " from sys_config where id = #{id}")
    SysConfig getSysConfig(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_config   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysConfig")
    List<SysConfig> getSysConfigs( @Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,@Param("name") String name);

    @Select("<script>select  " + column + "  from sys_config   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysConfig")
    List<SysConfig> getSysConfigAll();

    @Select("<script>select count(1) from sys_config   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysConfigCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_config  " +
            "set " +
            "`key` = #{key}  , " +
            "`value` = #{value}  , " +
            "`status` = #{status}  , " +
            "`sort` = #{sort}  , " +
            "`remark` = #{remark}  " +
            " where id = #{id}")
    void changeSysConfig(SysConfig sysConfig);

}
