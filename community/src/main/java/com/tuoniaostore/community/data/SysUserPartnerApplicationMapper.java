package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysUserPartnerApplication;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysUserPartnerApplicationMapper {

    String column = "`id`, " +//
            "`partner_id`, " +//合作者ID
            "`app_name`, " +//应用产品名
            "`app_key`, " +//应用key
            "`access_key`, " +//访问key 预留字段 必须存在
            "`access_key_exponent`, " +//
            "`access_key_secret`, " +//访问密钥 预留字段 必须存在
            "`access_key_secret_exponent`, " +//
            "`create_time`, " +//
            "`sort`, " +//
            "`status`, " +//
            "`retrieve_status`, " +//回收站
            "`remark`, " +//
            "`access_key_modulus`, " +//
            "`access_key_secret_modulus`"//
            ;

    @Insert("insert into sys_user_partner_application (" +
            " `id`,  " +
            " `partner_id`,  " +
            " `app_name`,  " +
            " `app_key`,  " +
            " `access_key`,  " +
            " `access_key_exponent`,  " +
            " `access_key_secret`,  " +
            " `access_key_secret_exponent`,  " +
            " `sort`,  " +
            " `status`,  " +
            " `retrieve_status`,  " +
            " `remark`,  " +
            " `access_key_modulus`,  " +
            " `access_key_secret_modulus` " +
            ")values(" +
            "#{id},  " +
            "#{partnerId},  " +
            "#{appName},  " +
            "#{appKey},  " +
            "#{accessKey},  " +
            "#{accessKeyExponent},  " +
            "#{accessKeySecret},  " +
            "#{accessKeySecretExponent},  " +
            "#{sort},  " +
            "#{status},  " +
            "#{retrieveStatus},  " +
            "#{remark},  " +
            "#{accessKeyModulus},  " +
            "#{accessKeySecretModulus} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysUserPartnerApplication(SysUserPartnerApplication sysUserPartnerApplication);

    @Results(id = "sysUserPartnerApplication", value = {
            @Result(property = "id", column = "id"), @Result(property = "partnerId", column = "partner_id"), @Result(property = "appName", column = "app_name"), @Result(property = "appKey", column = "app_key"), @Result(property = "accessKey", column = "access_key"), @Result(property = "accessKeyExponent", column = "access_key_exponent"), @Result(property = "accessKeySecret", column = "access_key_secret"), @Result(property = "accessKeySecretExponent", column = "access_key_secret_exponent"), @Result(property = "createTime", column = "create_time"), @Result(property = "sort", column = "sort"), @Result(property = "status", column = "status"), @Result(property = "retrieveStatus", column = "retrieve_status"), @Result(property = "remark", column = "remark"), @Result(property = "accessKeyModulus", column = "access_key_modulus"), @Result(property = "accessKeySecretModulus", column = "access_key_secret_modulus")})
    @Select("select " + column + " from sys_user_partner_application where id = #{id}")
    SysUserPartnerApplication getSysUserPartnerApplication(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_user_partner_application   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysUserPartnerApplication")
    List<SysUserPartnerApplication> getSysUserPartnerApplications(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_user_partner_application   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysUserPartnerApplication")
    List<SysUserPartnerApplication> getSysUserPartnerApplicationAll();

    @Select("<script>select count(1) from sys_user_partner_application   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysUserPartnerApplicationCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_user_partner_application  " +
            "set " +
            "`partner_id` = #{partnerId}  , " +
            "`app_name` = #{appName}  , " +
            "`app_key` = #{appKey}  , " +
            "`access_key` = #{accessKey}  , " +
            "`access_key_exponent` = #{accessKeyExponent}  , " +
            "`access_key_secret` = #{accessKeySecret}  , " +
            "`access_key_secret_exponent` = #{accessKeySecretExponent}  , " +
            "`create_time` = #{createTime}  , " +
            "`sort` = #{sort}  , " +
            "`status` = #{status}  , " +
            "`retrieve_status` = #{retrieveStatus}  , " +
            "`remark` = #{remark}  , " +
            "`access_key_modulus` = #{accessKeyModulus}  , " +
            "`access_key_secret_modulus` = #{accessKeySecretModulus}  " +
            " where id = #{id}")
    void changeSysUserPartnerApplication(SysUserPartnerApplication sysUserPartnerApplication);
    @Select("select " + column + " from sys_user_partner_application where app_key=#{appKey} and status=0 limit 1")
    @ResultMap("sysUserPartnerApplication")
    SysUserPartnerApplication getSysUserPartnerApplicationAppKey(@Param("appKey") String appKey);

}
