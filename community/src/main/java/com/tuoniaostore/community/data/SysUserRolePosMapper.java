package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysUserRolePos;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户与角色对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
@Component
@Mapper
public interface SysUserRolePosMapper {

    String column = "`id`, " +//
            "`user_id`, " +//用户ID
            "`role_id`, " +//角色ID
            "`create_time`, " +//
            "`operation_id`, " +//
            "`retrieve_status`, " +//
            "`status`, " +//
            "`remark`"//
            ;

    @Insert("insert into sys_user_role_pos (" +
            " `id`,  " +
            " `user_id`,  " +
            " `role_id`  " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{roleId}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysUserRolePos(SysUserRolePos sysUserRolePos);

    @Results(id = "sysUserRolePos", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "roleId", column = "role_id"), @Result(property = "createTime", column = "create_time"), @Result(property = "operationId", column = "operation_id"), @Result(property = "retrieveStatus", column = "retrieve_status"), @Result(property = "status", column = "status"), @Result(property = "remark", column = "remark")})
    @Select("select " + column + " from sys_user_role_pos where id = #{id}")
    SysUserRolePos getSysUserRolePos(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_user_role_pos   where   retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysUserRolePos")
    List<SysUserRolePos> getSysUserRolePoss(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from sys_user_role_pos   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysUserRolePos")
    List<SysUserRolePos> getSysUserRolePosAll();

    @Select("<script>select count(1) from sys_user_role_pos   where     retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysUserRolePosCount(@Param("status") int status, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Update("update sys_user_role_pos  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`role_id` = #{roleId}  , " +
            "`create_time` = #{createTime}  , " +
            "`operation_id` = #{operationId}  , " +
            "`retrieve_status` = #{retrieveStatus}  , " +
            "`status` = #{status}  , " +
            "`remark` = #{remark}  " +
            " where id = #{id}")
    void changeSysUserRolePos(SysUserRolePos sysUserRolePos);

    @Select("<script>SELECT * FROM sys_user_role_pos WHERE user_id = #{userId} </script>")
    List<SysUserRolePos> getSysUserRolePosByUserId(@Param("userId") String userId);

    @Update("<script>update sys_role_pos set " +
            "retrieve_status = #{status}" +
            " where id in <foreach collection=\"ids\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    void updateSysRolePosByIds(@Param("ids") List<String> ids, int status);
}
