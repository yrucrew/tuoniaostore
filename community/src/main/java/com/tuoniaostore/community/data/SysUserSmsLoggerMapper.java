package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysUserSmsLogger;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysUserSmsLoggerMapper {

    String column = "`id`, " +//
            "`phone`, " +//接收号码
            "`content`, " +//发送内容
            "`code`, " +//验证码内容(验证码短信有效)
            "`type`, " +//短信类型
            "`status`, " +//状态 0--已发送
            "`create_time`"//发送时间
            ;

    @Insert("insert into sys_user_sms_logger (" +
            " `id`,  " +
            " `phone`,  " +
            " `content`,  " +
            " `code`,  " +
            " `type`,  " +
            " `status`   " +
            ")values(" +
            "#{id},  " +
            "#{phone},  " +
            "#{content},  " +
            "#{code},  " +
            "#{type},  " +
            "#{status}   " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysUserSmsLogger(SysUserSmsLogger sysUserSmsLogger);

    @Results(id = "sysUserSmsLogger", value = {
            @Result(property = "id", column = "id"), @Result(property = "phone", column = "phone"), @Result(property = "content", column = "content"), @Result(property = "code", column = "code"), @Result(property = "type", column = "type"), @Result(property = "status", column = "status"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from sys_user_sms_logger where id = #{id}")
    SysUserSmsLogger getSysUserSmsLogger(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_user_sms_logger   where    status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by create_time desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysUserSmsLogger")
    List<SysUserSmsLogger> getSysUserSmsLoggers(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_user_sms_logger   where     status=0 </script>")
    @ResultMap("sysUserSmsLogger")
    List<SysUserSmsLogger> getSysUserSmsLoggerAll();

    @Select("<script>select count(1) from sys_user_sms_logger   where    status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysUserSmsLoggerCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_user_sms_logger  " +
            "set " +
            "`phone` = #{phone}  , " +
            "`content` = #{content}  , " +
            "`code` = #{code}  , " +
            "`type` = #{type}  , " +
            "`status` = #{status}   " +
            " where id = #{id}")
    void changeSysUserSmsLogger(SysUserSmsLogger sysUserSmsLogger);

    @ResultMap("sysUserSmsLogger")
    @Select("select " + column + " from sys_user_sms_logger where phone = #{phone} and  type = #{type}  and create_time>=DATE_SUB(NOW(),INTERVAL 2 MINUTE) order by create_time desc limit 1")
    SysUserSmsLogger getSysUserSmsLoggerByPhone(@Param("phone") String phone,@Param("type") int type);
}
