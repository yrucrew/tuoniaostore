package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysUserChannel;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 合作渠道，用于区分来自不同渠道的用户数据
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysUserChannelMapper {

    String column = "`id`, " +//
            "`name`, " +//渠道名
            "`type`, " +//渠道类型，由逻辑决定；暂时为1
            "`status`, " +//状态 0可用,1禁用
            "`create_time`, " +//
            "`retrieve_status`, " +//
            "`sort`"//
            ;

    @Insert("insert into sys_user_channel (" +
            " `id`,  " +
            " `name`,  " +
            " `type`,  " +
            " `status`,  " +
            " `sort` " +
            ")values(" +
            "#{id},  " +
            "#{name},  " +
            "#{type},  " +
            "#{status},  " +
            "#{sort} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysUserChannel(SysUserChannel sysUserChannel);

    @Results(id = "sysUserChannel", value = {
            @Result(property = "id", column = "id"), @Result(property = "name", column = "name"), @Result(property = "type", column = "type"), @Result(property = "status", column = "status"), @Result(property = "createTime", column = "create_time"), @Result(property = "retrieveStatus", column = "retrieve_status"), @Result(property = "sort", column = "sort")})
    @Select("select " + column + " from sys_user_channel where id = #{id}")
    SysUserChannel getSysUserChannel(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_user_channel   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysUserChannel")
    List<SysUserChannel> getSysUserChannels(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_user_channel   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysUserChannel")
    List<SysUserChannel> getSysUserChannelAll();

    @Select("<script>select count(1) from sys_user_channel   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysUserChannelCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_user_channel  " +
            "set " +
            "`name` = #{name}  , " +
            "`type` = #{type}  , " +
            "`status` = #{status}  , " +
            "`sort` = #{sort}  " +
            " where id = #{id}")
    void changeSysUserChannel(SysUserChannel sysUserChannel);

    @ResultMap("sysUserChannel")
    @Select("select " + column + " from sys_user_channel where name = #{name}")
    SysUserChannel getSysUserChannelByName(@Param("name") String name);
}
