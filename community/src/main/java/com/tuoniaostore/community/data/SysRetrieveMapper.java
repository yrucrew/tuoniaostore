package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysRetrieve;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysRetrieveMapper {

    String column = "`id`, " +//
            "`title`, " +//
            "`retrieve_table_name`, " +//
            "`retrieve_table_id`, " +//
            "`user_id`, " +//
            "`create_time`"//
            ;

    @Insert("insert into sys_retrieve (" +
            " `id`,  " +
            " `title`,  " +
            " `retrieve_table_name`,  " +
            " `retrieve_table_id`,  " +
            " `user_id`  " +
            ")values(" +
            "#{id},  " +
            "#{title},  " +
            "#{retrieveTableName},  " +
            "#{retrieveTableId},  " +
            "#{userId} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysRetrieve(SysRetrieve sysRetrieve);

    @Insert("<script>insert into sys_retrieve (" +
            " `id`,  " +
            " `title`,  " +
            " `retrieve_table_name`,  " +
            " `retrieve_table_id`,  " +
            " `user_id` " +
            ") " +
            "<foreach collection=\"lists\" item=\"item\" separator=\"UNION ALL\"> " +
            " (SELECT #{item.id},#{item.title},#{item.retrieveTableName},#{item.retrieveTableId}, #{item.userId}  FROM DUAL WHERE (SELECT COUNT(*) FROM  sys_retrieve " +
            " WHERE retrieve_table_id = #{item.retrieveTableId} and retrieve_table_name = #{item.retrieveTableName} ) <![CDATA[<]]> 1 ) " +
            "</foreach> </script>")
    void addSysRetrieves(@Param("lists") List<SysRetrieve> sysRetrieve);


    @Results(id = "sysRetrieve", value = {
            @Result(property = "id", column = "id"), @Result(property = "title", column = "title"), @Result(property = "retrieveTableName", column = "retrieve_table_name"), @Result(property = "retrieveTableId", column = "retrieve_table_id"),
            @Result(property = "userId", column = "user_id"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from sys_retrieve where id = #{id}")
    SysRetrieve getSysRetrieve(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_retrieve   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysRetrieve")
    List<SysRetrieve> getSysRetrieves(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_retrieve   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysRetrieve")
    List<SysRetrieve> getSysRetrieveAll();

    @Select("<script>select count(1) from sys_retrieve   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysRetrieveCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_retrieve  " +
            "set " +
            "`title` = #{title}  , " +
            "`retrieve_table_name` = #{retrieveTableName}  , " +
            "`retrieve_table_id` = #{retrieveTableId}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeSysRetrieve(SysRetrieve sysRetrieve);

    @Update("UPDATE   ${tableName}  SET  `retrieve_status` =#{retrieveStatus}  WHERE `id` = #{id}")
    void changeRetrieveStatus(@Param("tableName") String tableName, @Param("retrieveStatus") int retrieveStatus, @Param("id") String id);

}
