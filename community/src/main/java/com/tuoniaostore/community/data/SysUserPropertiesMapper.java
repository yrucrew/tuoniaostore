package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysUserProperties;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysUserPropertiesMapper {

    String column = "`id`, " +//
            "`user_id`, " +//归属的帐号ID
            "`type`, " +//类型
            "`k`, " +//属性索引
            "`v`, " +//属性值
            "`create_time`"//建立时间
            ;

    @Insert("insert into sys_user_properties (" +
            " `id`,  " +
            " `user_id`,  " +
            " `type`,  " +
            " `k`,  " +
            " `v`,  " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{type},  " +
            "#{k},  " +
            "#{v},  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysUserProperties(SysUserProperties sysUserProperties);

    @Results(id = "sysUserProperties", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "type", column = "type"), @Result(property = "k", column = "k"), @Result(property = "v", column = "v"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from sys_user_properties where id = #{id}")
    SysUserProperties getSysUserProperties(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_user_properties   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysUserProperties")
    List<SysUserProperties> getSysUserPropertiess(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_user_properties   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysUserProperties")
    List<SysUserProperties> getSysUserPropertiesAll();

    @Select("<script>select count(1) from sys_user_properties   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysUserPropertiesCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_user_properties  " +
            "set " +
            "`type` = #{type}  , " +
            "`k` = #{k}  , " +
            "`v` = #{v}    " +
            " where id = #{id}")
    void changeSysUserProperties(SysUserProperties sysUserProperties);

}
