package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysUserRole;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户与角色对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysUserRoleMapper {

    String column = "`id`, " +//
            "`user_id`, " +//用户ID
            "`role_id`, " +//角色ID
//            "`sys_channel_id`, " +//
            "`create_time`, " +//
//            "`user_id`, " +//
            "`operation_id`, " +//
            "`retrieve_status`, " +//
            "`status`, " +//
            "`remark`"//
            ;

    @Insert("insert into sys_user_role (" +
            " `id`,  " +
            " `user_id`,  " +
            " `role_id`,  " +
            " `operation_id`,  " +
            " `retrieve_status`,  " +
            " `status`,  " +
            " `remark` " +
            ")values(" +
            "#{id},  " +
            "#{userId},  " +
            "#{roleId},  " +
            "#{operationId},  " +
            "#{retrieveStatus},  " +
            "#{status},  " +
            "#{remark} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysUserRole(SysUserRole sysUserRole);

    @Results(id = "sysUserRole", value = {
            @Result(property = "id", column = "id"), @Result(property = "userId", column = "user_id"), @Result(property = "roleId", column = "role_id"), @Result(property = "sysChannelId", column = "sys_channel_id"), @Result(property = "createTime", column = "create_time"), @Result(property = "userId", column = "user_id"), @Result(property = "retrieveStatus", column = "retrieve_status"), @Result(property = "status", column = "status"), @Result(property = "remark", column = "remark")})
    @Select("select " + column + " from sys_user_role where id = #{id}")
    SysUserRole getSysUserRole(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_user_role   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysUserRole")
    List<SysUserRole> getSysUserRoles(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_user_role   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysUserRole")
    List<SysUserRole> getSysUserRoleAll();

    @Select("<script>select count(1) from sys_user_role   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysUserRoleCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_user_role  " +
            "set " +
            "`user_id` = #{userId}  , " +
            "`role_id` = #{roleId}  , " +
            "`create_time` = #{createTime}  , " +
            "`status` = #{status}  , " +
            "`remark` = #{remark}  " +
            " where id = #{id}")
    void changeSysUserRole(SysUserRole sysUserRole);

    @Select("select * from sys_user_role where role_id = #{roleId}")
	List<SysUserRole> getSysUserRolesByRole(@Param("roleId") String roleId);

    @Select("select * from sys_user_role where role_id = #{roleId} and user_id = #{userId}")
	SysUserRole getSysUserRoleByRoleAndUser(@Param("roleId") String roleId, @Param("userId") String userId);

    @Delete("delete from sys_user_role where role_id = #{roleId} and user_id = #{userId}")
	int deleteSysUserRole(@Param("roleId") String roleId, @Param("userId") String userId);

    @Select("<script> select * from  sys_user_role where user_id = #{userId} limit 1</script>")
    SysUserRole getSysUserRoleByUser(@Param("userId") String userId);

    @Update("<script> update sys_user_role " +
            "<set>" +
            "<when test=\"roleId != null and roleId !=''\">role_id = #{roleId},</when>" +
            "</set> where user_id = #{userId}" +
            "</script>")
    void updateSysUserRoleByParam(SysUserRole sysUserRole);
}
