package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysInterface;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
@Component
@Mapper
public interface SysInterfaceMapper {

    String column = "`id`, " +//
            "`name`, " +//接口名称
            "`project_id`, " +//项目ID
            "`url`, " +//接口访问地址
            "`status`, " +//0.开发1.测试 2.上线 3.禁用
            "`version`, " +//
            "`sort`, " +//排序
            "`return_type`, " +//返回类型0.application/json 1.text/html 2.x-application 3.application/xml
            "`error_list`, " +//错误列表
            "`explain`, " +//接口说明
            "`response`, " +//返回值
            "`remark`"//
            ;

    @Insert("insert into sys_interface (" +
            " `id`,  " +
            " `name`,  " +
            " `project_id`,  " +
            " `url`,  " +
            " `status`,  " +
            " `version`,  " +
            " `sort`,  " +
            " `return_type`,  " +
            " `error_list`,  " +
            " `explain`,  " +
            " `response`,  " +
            " `remark` " +
            ")values(" +
            "#{id},  " +
            "#{name},  " +
            "#{projectId},  " +
            "#{url},  " +
            "#{status},  " +
            "#{version},  " +
            "#{sort},  " +
            "#{returnType},  " +
            "#{errorList},  " +
            "#{explain},  " +
            "#{response},  " +
            "#{remark} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysInterface(SysInterface sysInterface);

    @Results(id = "sysInterface", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "url", column = "url"),
            @Result(property = "status", column = "status"),
            @Result(property = "version", column = "version"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "returnType", column = "return_type"), @Result(property = "errorList", column = "error_list"), @Result(property = "explain", column = "explain"), @Result(property = "response", column = "response"), @Result(property = "remark", column = "remark")})
    @Select("select " + column + " from sys_interface where id = #{id}")
    SysInterface getSysInterface(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_interface   where   1=1   " +
            " <when test=\"status != null and status != -1 \">" +
            "   and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysInterface")
    List<SysInterface> getSysInterfaces(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name );

    @Select("<script>select  " + column + "  from sys_interface   where    status=0 </script>")
    @ResultMap("sysInterface")
    List<SysInterface> getSysInterfaceAll();
    @Select("<script>select  " + column + "  from sys_interface   where    status=0 and project_id=#{projectId} </script>")
    @ResultMap("sysInterface")
    List<SysInterface> getSysInterfaceByProjectId(@Param("projectId") String projectId);

    @Select("<script>select count(1) from sys_interface   where    1=1  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysInterfaceCount(@Param("status") int status, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Update("update sys_interface  " +
            "set " +
            "`name` = #{name}  , " +
            "`project_id` = #{projectId}  , " +
            "`url` = #{url}  , " +
            "`status` = #{status}  , " +
            "`version` = #{version}  , " +
            "`sort` = #{sort}  , " +
            "`return_type` = #{returnType}  , " +
            "`error_list` = #{errorList}  , " +
            "`explain` = #{explain}  , " +
            "`response` = #{response}  , " +
            "`remark` = #{remark}  " +
            " where id = #{id}")
    void changeSysInterface(SysInterface sysInterface);

}
