package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysVersion;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysVersionMapper {

    String column = "`id`, " +//
            "`device_type`, " +//对应的设备类型 如：Android IOS等
            "`client_type`, " +//客户端类型，逻辑决定，如：供应链、商家端等
            "`version_index`, " +//内部版本号 一般递增 初始为1
            "`show_version`, " +//用于展示的版本号
            "`min_version_index`, " +//最低兼容的版本(内部版本号)
            "`version_url`, " +//下载地址
            "`version_introduce`, " +//更新内容介绍
            "`open_time`, " +//开放更新时间
            "`islocal`, " +//是否局部更新：0否，1是
            "`create_time`, " +//发布时间
            "`user_id`, " +//
            "`retrieve_status`"//
            ;

    @Insert("insert into sys_version (" +
            " `id`,  " +
            " `device_type`,  " +
            " `client_type`,  " +
            " `version_index`,  " +
            " `show_version`,  " +
            " `min_version_index`,  " +
            " `version_url`,  " +
            " `version_introduce`,  " +
            " `open_time`,  " +
            " `islocal`,  " +
            " `user_id`  " +
            ")values(" +
            "#{id},  " +
            "#{deviceType},  " +
            "#{clientType},  " +
            "#{versionIndex},  " +
            "#{showVersion},  " +
            "#{minVersionIndex},  " +
            "#{versionUrl},  " +
            "#{versionIntroduce},  " +
            "#{openTime},  " +
            "#{islocal},  " +
            "#{userId}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysVersion(SysVersion sysVersion);

    @Results(id = "sysVersion", value = {
            @Result(property = "id", column = "id"), @Result(property = "deviceType", column = "device_type"), @Result(property = "clientType", column = "client_type"), @Result(property = "versionIndex", column = "version_index"), @Result(property = "showVersion", column = "show_version"), @Result(property = "minVersionIndex", column = "min_version_index"), @Result(property = "versionUrl", column = "version_url"), @Result(property = "versionIntroduce", column = "version_introduce"), @Result(property = "openTime", column = "open_time"), @Result(property = "islocal", column = "islocal"), @Result(property = "createTime", column = "create_time"), @Result(property = "userId", column = "user_id"), @Result(property = "retrieveStatus", column = "retrieve_status")})
    @Select("select " + column + " from sys_version where id = #{id}")
    SysVersion getSysVersion(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_version   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysVersion")
    List<SysVersion> getSysVersions(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_version   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysVersion")
    List<SysVersion> getSysVersionAll();

    @Select("<script>select count(1) from sys_version   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysVersionCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_version  " +
            "set " +
            "`device_type` = #{deviceType}  , " +
            "`client_type` = #{clientType}  , " +
            "`version_index` = #{versionIndex}  , " +
            "`show_version` = #{showVersion}  , " +
            "`min_version_index` = #{minVersionIndex}  , " +
            "`version_url` = #{versionUrl}  , " +
            "`version_introduce` = #{versionIntroduce}  , " +
            "`open_time` = #{openTime}  , " +
            "`islocal` = #{islocal}  , " +
            "`user_id` = #{userId}  , " +
            " where id = #{id}")
    void changeSysVersion(SysVersion sysVersion);

    @ResultMap("sysVersion")
    @Select("SELECT " + column + "FROM `sys_version` where retrieve_status = 0 AND device_type = #{deviceType} AND client_type = #{clientType} AND version_index > #{versionIndex} AND SYSDATE() > open_time")
    SysVersion getUsefulVersion(@Param("deviceType") Integer deviceType,@Param("clientType") Integer clientType,@Param("versionIndex") Integer versionIndex);
}
