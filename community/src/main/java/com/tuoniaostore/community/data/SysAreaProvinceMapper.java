package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysAreaProvince;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
@Component
@Mapper
public interface SysAreaProvinceMapper {

    String column = "`id`, " +//
            "`area_id`, " +//区域Id
            "`city_id`"//省份ID
            ;

    @Insert("<script>insert into sys_area_province (" +
            " `id`,  " +
            " `area_id`,  " +
            " `city_id` " +
            ")values" +
            "<foreach collection='list' item='sysAreaProvince' separator=','  >" +
            "(" +
            "#{sysAreaProvince.id},  " +
            "#{sysAreaProvince.areaId},  " +
            "#{sysAreaProvince.cityId} " +
            ")" +
            "</foreach></script>")
    void addSysAreaProvince(List<SysAreaProvince> list);

    @Results(id = "sysAreaProvince", value = {
            @Result(property = "id", column = "id"), @Result(property = "areaId", column = "area_id"), @Result(property = "cityId", column = "city_id")})
    @Select("select " + column + " from sys_area_province where id = #{id}")
    SysAreaProvince getSysAreaProvince(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_area_province   where   retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysAreaProvince")
    List<SysAreaProvince> getSysAreaProvinces(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from sys_area_province   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysAreaProvince")
    List<SysAreaProvince> getSysAreaProvinceAll();


    @Select("<script>select  " + column + "  from sys_area_province     where area_id  <![CDATA[ <> ]]>  #{areaId} </script>")
    @ResultMap("sysAreaProvince")
    List<SysAreaProvince> getSysAreaNotId(@Param("areaId") String areaId);

    @Select("<script>select  " + column + "  from sys_area_province     where area_id  = #{areaId} </script>")
    @ResultMap("sysAreaProvince")
    List<SysAreaProvince> getSysAreaProvinceByAreaId(@Param("areaId") String areaId);

    @Select("<script>select count(1) from sys_area_province   where     retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysAreaProvinceCount(@Param("status") int status, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Update("update sys_area_province  " +
            "set " +
            "`area_id` = #{areaId}  , " +
            "`city_id` = #{cityId}  " +
            " where id = #{id}")
    void changeSysAreaProvince(SysAreaProvince sysAreaProvince);

    @Delete("delete  from sys_area_province where  area_id=#{areaId}")
    void delSysAreaProvince(@Param("areaId") String areaId);

    @Delete(" <script> " +
            " DELETE FROM sys_area_province WHERE 1=1 " +
            " AND area_id in <foreach collection=\"areaIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> " +
            " </script>")
    void deleteSysAreaProvinceByAreaIds(@Param("areaIds") List<String> areaIds);
}
