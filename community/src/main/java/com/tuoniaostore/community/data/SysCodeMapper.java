package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysCode;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysCodeMapper {

    String column = "`id`, " +//
            "`code`, " +//
            "`codeName`, " +//
            "`create_time`, " +//
            "`code_url`"//
            ;

    @Insert("insert into sys_code (" +
            " `id`,  " +
            " `code`,  " +
            " `codeName`,  " +
            " `code_url` " +
            ")values(" +
            "#{id},  " +
            "#{code},  " +
            "#{codename},  " +
            "#{codeUrl} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysCode(SysCode sysCode);

    @Results(id = "sysCode", value = {
            @Result(property = "id", column = "id"), @Result(property = "code", column = "code"), @Result(property = "codename", column = "codeName"), @Result(property = "createTime", column = "create_time"), @Result(property = "codeUrl", column = "code_url")})
    @Select("select " + column + " from sys_code where id = #{id}")
    SysCode getSysCode(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_code   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysCode")
    List<SysCode> getSysCodes(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_code   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysCode")
    List<SysCode> getSysCodeAll();

    @Select("<script>select count(1) from sys_code   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('', #{name}, '%') " +
            " </when></script>")
    int getSysCodeCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_code  " +
            "set " +
            "`code` = #{code}  , " +
            "`codeName` = #{codename}  , " +
            "`create_time` = #{createTime}  , " +
            "`code_url` = #{codeUrl}  " +
            " where id = #{id}")
    void changeSysCode(SysCode sysCode);
    @Select("select " + column + " from sys_code where codeName = #{codeName}")
    @ResultMap("sysCode")
    SysCode getSysCodeName(@Param("codeName") String codeName);
}
