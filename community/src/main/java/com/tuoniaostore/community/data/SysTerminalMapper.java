package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysTerminal;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysTerminalMapper {

    String column = "`id`, " +//ID
            "`title`, " +//终端名称
            "`sort`, " +//
            "`remark`, " +//
            "`status`, " +//
            "`user_id`, " +//
            "`create_time`, " +//
            "`retrieve_status`"//
            ;

    @Insert("insert into sys_terminal (" +
            " `id`,  " +
            " `title`,  " +
            " `sort`,  " +
            " `remark`,  " +
            " `status`,  " +
            " `user_id`  " +
            ")values(" +
            "#{id},  " +
            "#{title},  " +
            "#{sort},  " +
            "#{remark},  " +
            "#{status},  " +
            "#{userId}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysTerminal(SysTerminal sysTerminal);

    @Results(id = "sysTerminal", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "remark", column = "remark"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createTime", column = "create_time"), @Result(property = "retrieveStatus", column = "retrieve_status")})
    @Select("select " + column + " from sys_terminal where id = #{id}")
    SysTerminal getSysTerminal(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_terminal   where   retrieve_status=#{retrieveStatus} " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysTerminal")
    List<SysTerminal> getSysTerminals(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("title") String title, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from sys_terminal   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysTerminal")
    List<SysTerminal> getSysTerminalAll();

    @Select("<script>select count(1) from sys_terminal   where    retrieve_status=#{retrieveStatus} " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when></script>")
    int getSysTerminalCount(@Param("status") int status, @Param("title") String title, @Param("retrieveStatus") int retrieveStatus);

    @Update("update sys_terminal  " +
            "set " +
            "`title` = #{title}  , " +
            "`sort` = #{sort}  , " +
            "`remark` = #{remark}  , " +
            "`status` = #{status}  " +
            " where id = #{id}")
    void changeSysTerminal(SysTerminal sysTerminal);


    @Select("<script>select  " + column + "  from sys_terminal   where   retrieve_status=0  and status=0 and id=#{id} and user_id = #{userId} limit 1 </script>")
    @ResultMap("sysTerminal")
    SysTerminal getSysTerminalByIdAndUserId(@Param("id") String id, @Param("userId")String userId);

    @Select("<script> select " + column + " from sys_terminal where id in (" +
            "SELECT t1.terminal_id FROM sys_user_terminal  t1 where t1.user_id = #{id}) </script>")
    List<SysTerminal> getSysUserTerminalListByUserId(@Param("id") String id);

    @Select("<script> select GROUP_CONCAT(title) from sys_terminal where " +
            " <if test=\"lists != null\"> id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> </script>")
    String getSysTerminalTitleByList(@Param("lists") List<String> terminalIds);

    @Select("<script> select * from sys_terminal where title = #{terminalName} </script>")
    SysTerminal findSysUserByTerminalName(String terminalName);

    @Update("<script> update sys_terminal set  retrieve_status = 1 where id  in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script> ")
    void batchDeleteSysTerminal(@Param("lists") List<String> ids);
}
