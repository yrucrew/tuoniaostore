package com.tuoniaostore.community.data;

import com.tuoniaostore.community.vo.PurchaseUserVo;
import com.tuoniaostore.datamodel.user.SysUser;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysUserMapper {

    String column = "`id`, " +//通行证ID 全世界唯一
            "`default_name`, " +//默认登录用户名 全世界唯一
            "`password`, " +//登录密码
            "`show_name`, " +//用于展示的名字(可重复)
            "`real_name`, " +//用户真实名(方便扩展实名认证的功能)
            "`id_number`, " +//身份证号
            "`phone_number`, " +//电话号码(不局限于手机号)
            "`sex`, " +//用户性别(0:女性 1:男性；默认值:0)
            "`type`, " +//用户类型(普通用户，VIP用户等，由逻辑设定)51,无人便利店，52无人售货架
            "`status`, " +//当前的状态(如：正常、禁言、禁登、黑名单等)
            "`create_time`, " +//注册时间
            "`tax_rate`, " +//增值税税率
            "`from_channel`, " +//注册的渠道(如：官网、第三方渠道等；用于分析推广方式)
            "`device_type`, " +//设备类型(分析用户群，可针对不同用户开展不同的活动)
            "`device_name`, " +//设备名称
            "`access_token`, " +//最后的访问令牌(每次登录进行重置)
            "`login_date`, " +//
            "`role_id`, " +//角色
            "`pos_role_id`, " +//pos机角色
            "`consume`, " +//
            "`code`, " +//
            "`longitude`, " +//
            "`latitude`, " +//
            "`passportcol`, " +//
            "`sort`, " +//排序
            "`remark`, " +//备注
            "`access_key`, " +//私钥
            "`user_id`, " +//创建用户ID
            "`retrieve_status`, " +//回收站状态
            "`network_type`, " +//网络类型
            "`machine_code`, " +//机器码
            "`machine_brand`, " +//机器品牌
            "`system_version`, " +//系统版本
            "`version_index`," +//软件版本号
            "`leader_user_id`," +//上级用户ID
            "`open_id`" //上级用户ID
            ;

    String column2 = "t1.id as id, " +//通行证ID 全世界唯一
            "t1.default_name  as default_name , " +//默认登录用户名 全世界唯一
            "t1.password as password , " +//登录密码
            "t1.show_name as show_name, " +//用于展示的名字(可重复)
            "t1.real_name as real_name, " +//用户真实名(方便扩展实名认证的功能)
            "t1.id_number as id_number , " +//身份证号
            "t1.phone_number as phone_number, " +//电话号码(不局限于手机号)
            "t1.sex as sex, " +//用户性别(0:女性 1:男性；默认值:0)
            "t1.type as type, " +//用户类型(普通用户，VIP用户等，由逻辑设定)51,无人便利店，52无人售货架
            "t1.status as status, " +//当前的状态(如：正常、禁言、禁登、黑名单等)
            "t1.create_time as create_time, " +//注册时间
            "t1.tax_rate as tax_rate, " +//增值税税率
            "t1.from_channel as from_channel, " +//注册的渠道(如：官网、第三方渠道等；用于分析推广方式)
            "t1.device_type as device_type, " +//设备类型(分析用户群，可针对不同用户开展不同的活动)
            "t1.device_name as device_name, " +//设备名称
            "t1.access_token as access_token, " +//最后的访问令牌(每次登录进行重置)
            "t1.login_date as login_date, " +//
            "t1.role_id as role_id, " +//角色
            "t1.consume as consume, " +//
            "t1.code as code, " +//
            "t1.longitude as longitude, " +//
            "t1.latitude as latitude, " +//
            "t1.passportcol as passportcol, " +//
            "t1.sort as sort, " +//排序
            "t1.remark as remark, " +//备注
            "t1.access_key as access_key, " +//私钥
            "t1.user_id as user_id , " +//创建用户ID
            "t1.retrieve_status as retrieve_status, " +//回收站状态
            "t1.network_type as network_type, " +//网络类型
            "t1.machine_code as machine_code, " +//机器码
            "t1.machine_brand as machine_brand, " +//机器品牌
            "t1.system_version as system_version, " +//系统版本
            "t1.version_index as version_index," +//软件版本号
            "t1.leader_user_id as leader_user_id," +//上级用户ID
            "t1.open_id as open_id" //上级用户ID
            ;


    @Insert("insert into sys_user (" +
            " `id`,  " +
            " `default_name`,  " +
            " `password`,  " +
            " `show_name`,  " +
            " `real_name`,  " +
            " `id_number`,  " +
            " `phone_number`,  " +
            " `sex`,  " +
            " `type`,  " +
            " `status`,  " +
            " `tax_rate`,  " +
            " `from_channel`,  " +
            " `device_type`,  " +
            " `device_name`,  " +
            " `access_token`,  " +
            " `login_date`,  " +
            " `role_id`,  " +
            " `consume`,  " +
            " `code`,  " +
            " `longitude`,  " +
            " `latitude`,  " +
            " `passportcol`,  " +
            " `sort`,  " +
            " `remark`,  " +
            " `access_key`,  " +
            " `user_id`,  " +
            " `retrieve_status`,  " +
            " `network_type`,  " +
            " `machine_code`,  " +
            " `machine_brand`,  " +
            " `system_version`,  " +
            " `version_index` " +
            ")values(" +
            "#{id},  " +
            "#{defaultName},  " +
            "#{password},  " +
            "#{showName},  " +
            "#{realName},  " +
            "#{idNumber},  " +
            "#{phoneNumber},  " +
            "#{sex},  " +
            "#{type},  " +
            "#{status},  " +
            "#{taxRate},  " +
            "#{fromChannel},  " +
            "#{deviceType},  " +
            "#{deviceName},  " +
            "#{accessToken},  " +
            "#{loginDate},  " +
            "#{roleId},  " +
            "#{consume},  " +
            "#{code},  " +
            "#{longitude},  " +
            "#{latitude},  " +
            "#{passportcol},  " +
            "#{sort},  " +
            "#{remark},  " +
            "#{accessKey},  " +
            "#{userId},  " +
            "#{retrieveStatus},  " +
            "#{networkType},  " +
            "#{machineCode},  " +
            "#{machineBrand},  " +
            "#{systemVersion},  " +
            "#{versionIndex} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysUser(SysUser sysUser);

    @Insert("insert into sys_user (" +
            " `id`,  " +
            " `default_name`,  " +
            " `password`,  " +
            " `show_name`,  " +
            " `real_name`,  " +
            " `phone_number`,  " +
            " `status`,  " +
            " `from_channel`,  " +
            " `pos_role_id`,  " +
            " `user_id`  " +
            ")values(" +
            "#{id},  " +
            "#{defaultName},  " +
            "#{password},  " +
            "#{showName},  " +
            "#{realName},  " +
            "#{phoneNumber},  " +
            "#{status},  " +
            "#{fromChannel},  " +
            "#{posRoleId},  " +
            "#{userId}  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysUserForPos(SysUser sysUser);


    @Results(id = "sysUser", value = {
            @Result(property = "id", column = "id"), @Result(property = "defaultName", column = "default_name"),
            @Result(property = "password", column = "password"),
            @Result(property = "showName", column = "show_name"), @Result(property = "realName", column = "real_name"),
            @Result(property = "idNumber", column = "id_number"),
            @Result(property = "phoneNumber", column = "phone_number"), @Result(property = "sex", column = "sex"),
            @Result(property = "type", column = "type"), @Result(property = "status", column = "status"),
            @Result(property = "createTime", column = "create_time"), @Result(property = "taxRate", column = "tax_rate"),
            @Result(property = "fromChannel", column = "from_channel"), @Result(property = "deviceType", column = "device_type"),
            @Result(property = "deviceName", column = "device_name"), @Result(property = "accessToken", column = "access_token"),
            @Result(property = "loginDate", column = "login_date"), @Result(property = "roleId", column = "role_id"),
            @Result(property = "consume", column = "consume"), @Result(property = "code", column = "code"),
            @Result(property = "longitude", column = "longitude"), @Result(property = "latitude", column = "latitude"),
            @Result(property = "passportcol", column = "passportcol"), @Result(property = "sort", column = "sort"),
            @Result(property = "remark", column = "remark"), @Result(property = "accessKey", column = "access_key"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "retrieveStatus", column = "retrieve_status"),
            @Result(property = "networkType", column = "network_type"),
            @Result(property = "machineCode", column = "machine_code"),
            @Result(property = "machineBrand", column = "machine_brand"),
            @Result(property = "systemVersion", column = "system_version"),
            @Result(property = "versionIndex", column = "version_index"),
            @Result(property = "leaderUserId", column = "leader_user_id"),
            @Result(property = "userName", column = "user_name"),
            @Result(property = "sysUserTerminals", column = "id", many = @Many(select = "com.tuoniaostore.community.data.SysUserTerminalMapper.getSysUserTerminalForManyToMany")),
            @Result(property = "roleName", column = "role_id", one = @One(select = "com.tuoniaostore.community.data.SysRoleMapper.getSysRoleName"))
    })
    @Select("select " + column + " from sys_user where id = #{id}")
    SysUser getSysUser(@Param("id") String id);

    @Select("<script>select  " + column2 + ",t2.real_name as user_name from sys_user t1 left join  sys_user t2 on t1.user_id = t2.id " +
            "  where   t1.retrieve_status=0  and t1.status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and ( t1.default_name like concat('%', #{name}, '%') " +
            " or    t1.default_name like concat('%', #{name}, '%')" +
            " or   t1.show_name like concat('%', #{name}, '%')" +
            " or    t1.real_name like concat('%', #{name}, '%')" +
            " or   t1.phone_number like concat('%', #{name}, '%')" +
            ") " +
            " </when>" +
            "  order by t1.sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysUser")
    List<SysUser> getSysUsers(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_user   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysUser")
    List<SysUser> getSysUserAll();

    @Select("<script>select count(1) from sys_user   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and ( default_name like concat('%', #{name}, '%') " +
            " or    default_name like concat('%', #{name}, '%')" +
            " or    show_name like concat('%', #{name}, '%')" +
            " or    real_name like concat('%', #{name}, '%')" +
            " or   phone_number like concat('%', #{name}, '%')" +
            ") " +
            " </when></script>")
    int getSysUserCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_user  " +
            "set " +
            "`default_name` = #{defaultName}  , " +
            "`show_name` = #{showName}  , " +
            "`real_name` = #{realName}  , " +
            "`id_number` = #{idNumber}  , " +
            "`phone_number` = #{phoneNumber}  , " +
            "`sex` = #{sex}  , " +
            "`type` = #{type}  , " +
            "`status` = #{status}  , " +
            "`create_time` = #{createTime}  , " +
            "`tax_rate` = #{taxRate}  , " +
            "`from_channel` = #{fromChannel}  , " +
            "`device_type` = #{deviceType}  , " +
            "`device_name` = #{deviceName}  , " +
            "`access_token` = #{accessToken}  , " +
            "`login_date` = #{loginDate}  , " +
            "`role_id` = #{roleId}  , " +
            "`consume` = #{consume}  , " +
            "`code` = #{code}  , " +
            "`longitude` = #{longitude}  , " +
            "`latitude` = #{latitude}  , " +
            "`passportcol` = #{passportcol}  , " +
            "`sort` = #{sort}  , " +
            "`remark` = #{remark}  , " +
            "`access_key` = #{accessKey}  , " +
            "`user_id` = #{userId}  , " +
            "`retrieve_status` = #{retrieveStatus}  , " +
            "`network_type` = #{networkType}  , " +
            "`machine_code` = #{machineCode}  , " +
            "`machine_brand` = #{machineBrand}  , " +
            "`system_version` = #{systemVersion}  , " +
            "`version_index` = #{versionIndex}  ," +
            "`leader_user_id` = #{leaderUserId}" +
            " where id = #{id}")
    void changeSysUser(SysUser sysUser);

    @Select("SELECT   " + column + "  FROM `sys_user` WHERE `default_name` =#{name}  or  `phone_number` =#{name}")
    @ResultMap("sysUser")
    List<SysUser> getDefaultNameOrPhone(@Param("name") String name);

    @Update("<script>update `sys_user` SET " +
            "    `access_token`=#{accessToken}," +
            "    <if test=\"openId != null \">open_id=#{openId},</if> " +
            "    `login_date`=#{loginDate}, " +
            "    `longitude`=#{longitude}," +
            "    `latitude`=#{latitude},   " +
            "    `network_type`=#{networkType}," +
            "    `machine_code`=#{machineCode}," +
            "    `machine_brand`=#{machineBrand}," +
            "    `system_version`=#{systemVersion}," +
            "    `version_index`=#{versionIndex}   " +
            "  where id =#{id}</script> ")
    void changeLogin(SysUser sysUser);

    //只会有一条记录 为了防止不必要的问题 限制一下
    @Select("SELECT   " + column + "  FROM `sys_user` WHERE `phone_number` =#{phoneNumber} and retrieve_status = 0 limit 1")
    @ResultMap("sysUser")
    SysUser getSysUserByPhoneNumber(@Param("phoneNumber") String phoneNumber);

    @Update("update `sys_user` SET " +
            "    `password`= #{password}" +
            "  where phone_number = #{phoneNum} ")
    void updatePasswordByPhoneNum(@Param("phoneNum") String phoneNum, @Param("password") String password);

    @Update("update `sys_user` SET " +
            "    `password`= #{password}" +
            "  where id = #{id} ")
    void updatePasswordById(@Param("id") String id, @Param("password") String password);

    @Update("update `sys_user` SET " +
            "    `phone_number`= #{password}" +
            "  where id = #{id} ")
    void changeSysUserPhoneNum(@Param("id") String id, @Param("phoneNum") String phoneNum);

    String purchaseUserListUrl = "from sys_user p " +
            "left join sys_user l on l.id = p.leader_user_id " +
            "left join sys_role r on p.role_id = r.id " +
            "where 1=1 " +
            " and p.retrieve_status = 0 " +
            " and p.role_id in" +
            "<foreach collection='roleIds' item='roleId' open='(' separator=',' close=')'>" +
            "#{roleId}" +
            "</foreach>" +
            "<if test = \" searchKey != null and searchKey != '' \">" +
            "and ( p.real_name like concat('%', #{searchKey} , '%') or p.phone_number like concat('%', #{searchKey}, '%') )" +
            "</if>";
//        	"<if test = 'param.status != -1'> and p.status = #{param.status} </if>" +
//        	"<if test = 'param.purchaseRealName != null'> and p.real_name like concat('%', #{param.purchaseRealName} , '%') </if>" +
//        	"<if test = 'param.leaderRealName != null'> and l.real_name like concat('%', #{param.leaderRealName} , '%') </if>";

    String getPurchaseUserListUrl = "" +
            " r.role_name, " +
            " p.id purchaseUserId," +
            " p.default_name, " +
            " p.real_name, " +
            " p.phone_number, " +
            " p.status, " +
            " l.id leaderUserId, " +
            " l.real_name leaderRealName, " +
            " l.phone_number leaderPhoneNumber ";

    @Select({
            "<script>",
            "select "+getPurchaseUserListUrl,
            purchaseUserListUrl,
            "order by p.create_time desc",
            "limit #{pageStartIndex}, #{pageSize}",
            "</script>"
    })
    List<PurchaseUserVo> getPurchaseUserList(@Param("searchKey") String searchKey, @Param("roleIds") List<String> roleIds, @Param("pageStartIndex") int pageStartIndex, @Param("pageSize") int pageSize);

    @Select({
            "<script>",
            "select count(1)",
            purchaseUserListUrl,
            "</script>"
    })
    int getPurchaseUserListCount(@Param("searchKey") String searchKey, @Param("roleIds") List<String> roleIds);

    @Select("select * from sys_user where ${column} like concat('%',#{value},'%')")
    List<SysUser> getSysUsersForSearch(@Param("column") String column, @Param("value") Object value);

    @Select("<script>select  " + column2 + ",t2.real_name as user_name from sys_user t1 left join  sys_user t2 on t1.user_id = t2.id   where 1=1 and t1.retrieve_status = 0 " +
            "<when test=\"map.defaultName != null and map.defaultName.trim() != null\"> and t1.default_name like concat('%',#{map.defaultName},'%') </when>" +
            "<when test=\"map.realName != null and map.realName.trim() != null\"> and t1.real_name like concat('%',#{map.realName},'%') </when>" +
            "<when test=\"map.idNumber != null and map.idNumber.trim() != null\"> and t1.id_number like concat('%',#{map.idNumber},'%') </when>" +
            "<when test=\"map.phoneNumber != null and map.phoneNumber.trim() != null\"> and t1.phone_number like concat('%',#{map.phoneNumber},'%') </when>" +
            "<when test=\"map.status != null and map.status != -1 \"> and t1.status = #{map.status} </when>" +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize} " +
            "</when>" +
            " </script>")
    @ResultMap("sysUser")
    List<SysUser> getSysUserByParam(@Param("map") Map<String, Object> map, @Param("pageIndex") Integer pageStartIndex, @Param("pageSize") Integer pageSize);

    @Select("<script> select count(*) from  sys_user where 1=1 and retrieve_status = 0 " +
            "<when test=\"map.defaultName != null and map.defaultName.trim() != null\"> and default_name like concat('%',#{map.defaultName},'%') </when>" +
            "<when test=\"map.realName != null and map.realName.trim() != null\"> and real_name like concat('%',#{map.realName},'%') </when>" +
            "<when test=\"map.idNumber != null and map.idNumber.trim() != null\"> and id_number like concat('%',#{map.idNumber},'%') </when>" +
            "<when test=\"map.phoneNumber != null and map.phoneNumber.trim() != null\"> and phone_number like concat('%',#{map.phoneNumber},'%') </when>" +
            "<when test=\"map.status != null and map.status != -1 \"> and status = #{map.status} </when>" +
            " </script>")
    int getCountSysUserByParam(@Param("map") Map<String, Object> map);

    @Update("<script> update sys_user set retrieve_status = 1 where id = #{id} </script>")
    void delSysUserByUserId(@Param("id") String id);

    @Update("<script> update sys_user " +
            "<set> " +
            "<when test=\"defaultName != null and defaultName != '' \"> default_name = #{defaultName},</when>" +
            "<when test=\"showName != null and showName != '' \"> show_name = #{showName},</when>" +
            "<when test=\"realName != null and realName != '' \"> real_name = #{realName},</when>" +
            "<when test=\"idNumber != null and idNumber != '' \"> id_number = #{idNumber},</when>" +
            "<when test=\"phoneNumber != null and phoneNumber != '' \"> phone_number = #{phoneNumber},</when>" +
            "<when test=\"sex != null \"> sex = #{sex},</when>" +
            "<when test=\"status != null  \"> status = #{status},</when>" +
            "<when test=\"sort != null  \"> sort = #{sort},</when>" +
            "<when test=\"remark != null \"> remark = #{remark},</when>" +
            "<when test=\"deviceType != null  \"> device_type = #{deviceType},</when>" +
            "<when test=\"longitude != null  \"> longitude = #{longitude},</when>" +
            "<when test=\"latitude != null  \"> latitude = #{latitude},</when>" +
            "<when test=\"roleId != null  \"> role_id = #{roleId},</when>" +
            "</set> where id = #{id}</script>")
    void updateSysUserParam(SysUser sysUser);

    @Select("<script> select * from sys_user where 1 = 2 " +
            "<when test=\"phoneNumber != null \">or phone_number = #{phoneNumber}</when> " +
            "<when test=\"defaultName != null \">or default_name = #{defaultName}</when> " +
            "and retrieve_status = 0 limit 1 </script>")
    SysUser findSysUserByPhoneOrDefaultName(@Param("phoneNumber") String phoneNumber, @Param("defaultName") String defaultName);

    @Select("<script> select " + column + " from sys_user where (default_name = #{accountOrName} or  show_name = #{accountOrName}) </script>")
    @ResultMap("sysUser")
    List<SysUser> getSysUserAccountOrName(@Param("accountOrName") String accountOrName);

    @Update("UPDATE `sys_user` AS u SET u.`real_name`=#{realName}, u.`password`=#{password} WHERE u.`id` = #{id}")
    int updateSysUserByPos(SysUser sysUser);

    @Update("UPDATE `sys_user` AS u SET u.`status` = #{status} WHERE u.`id` = #{id}")
    int forbidSysUserByPos(String id, int status);

    @Select("<script> SELECT  " + column + "  FROM `sys_user` where `id` in <foreach collection=\"ids\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    @ResultMap("sysUser")
    List<SysUser> getSysUserInIds(@Param("ids") List<String> ids);

    @Select("<script>  </script>")
    List<SysUser> getSysUsersListByPos(@Param("ids") List<String> ids, @Param("ids") Integer pageIndex, @Param("ids") Integer pageSize, @Param("ids") Integer pageSize1);

    @Select("<script>" +
            " SELECT " +
            column2 +
            " FROM " +
            " sys_user t1 " +
//            "LEFT JOIN sys_user_role as ur ON ur.user_id = t1.id   \n" +
            "where 1=1 AND t1.retrieve_status = 0 AND t1.status = #{status}" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and ( t1.default_name like concat('%', #{name}, '%') " +
            " or    t1.default_name like concat('%', #{name}, '%')" +
            " or   t1.show_name like concat('%', #{name}, '%')" +
            " or    t1.real_name like concat('%', #{name}, '%')" +
            " or   t1.phone_number like concat('%', #{name}, '%')" +
            " ) " +
            " </when>" +
//            " AND ur.role_id IN " +
            " AND t1.role_id IN " +
            " <foreach collection='roleIds' item='roleId' open='(' separator=',' close=')'> " +
            " #{roleId} " +
            " </foreach> " +
            "  order by t1.sort desc   " +
            "</script>")
    @ResultMap("sysUser")
    List<SysUser> getAllUserByRoleIds(@Param("name") String name, @Param("status") int status, @Param("roleIds") List<String> roleIds);

    @Select("<script> select * from sys_user where phone_number like concat('%',#{phoneNum},'%') and retrieve_status = 0 </script>")
    @ResultMap("sysUser")
    List<SysUser> getSysUserByPhoneNumLike(@Param("phoneNum") String phoneNum);

    @Select("<script>" +
            " SELECT " +
            column2 +
            " FROM " +
            " sys_user t1 " +
            "where 1=1 AND t1.retrieve_status = 0 " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and ( t1.default_name like concat('%', #{name}, '%') " +
            " or   t1.show_name like concat('%', #{name}, '%')" +
            " or    t1.real_name like concat('%', #{name}, '%')" +
            " ) " +
            " </when>" +
            "</script>")
    @ResultMap("sysUser")
    List<SysUser> getSysUserByName(@Param("name") String name);
}
