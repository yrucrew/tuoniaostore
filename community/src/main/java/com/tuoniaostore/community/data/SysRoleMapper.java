package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysRole;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysRoleMapper {

    String column = "`id`, " +//
            "`role_name`, " +//角色名称
            "`remark`, " +//备注
            "`create_time`, " +//创建时间
            "`icon`, " +//
            "`sort`, " +//
            "`user_id`, " +//
            "`retrieve_status`, " +//
            "`status`"//
            ;

    @Insert("insert into sys_role (" +
            " `id`,  " +
            " `role_name`,  " +
            " `remark`,  " +
            " `icon`,  " +
            " `sort`,  " +
            " `user_id`,  " +
            " `status` " +
            ")values(" +
            "#{id},  " +
            "#{roleName},  " +
            "#{remark},  " +
            "#{icon},  " +
            "#{sort},  " +
            "#{userId},  " +
            "#{status} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysRole(SysRole sysRole);

    @Results(id = "sysRole", value = {
            @Result(property = "id", column = "id"), @Result(property = "roleName", column = "role_name"),
            @Result(property = "remark", column = "remark"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "icon", column = "icon"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "retrieveStatus", column = "retrieve_status"),
            @Result(property = "status", column = "status")})
    @Select("select " + column + " from sys_role where id = #{id}")
    SysRole getSysRole(@Param("id") String id);

    //供一对一查找 别删
    @Select("select role_name  from sys_role where id = #{id}")
    String getSysRoleName(@Param("id") String id);


    @Select("<script>select  " + column + "  from sys_role   where   retrieve_status=#{retrieveStatus}" +
            " <when test=\"status != null and status != -1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"roleName != null and roleName.trim() != ''\">" +
            "  and role_name like concat('%', #{roleName}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysRole")
    List<SysRole> getSysRoles(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("roleName") String roleName, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from sys_role   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysRole")
    List<SysRole> getSysRoleAll();

    @Select("<script>select count(1) from sys_role   where    retrieve_status=#{retrieveStatus}" +
            " <when test=\"status != null and status != -1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"roleName != null and roleName.trim() != ''\">" +
            "  and role_name like concat('%', #{roleName}, '%') " +
            " </when>" +
            "</script>")
    int getSysRoleCount(@Param("status") int status,  @Param("roleName") String roleName, @Param("retrieveStatus") int retrieveStatus);


    @Update("update sys_role  " +
            "set " +
            "`role_name` = #{roleName}  , " +
            "`remark` = #{remark}  , " +
            "`icon` = #{icon}  , " +
            "`sort` = #{sort}  , " +
            "`status` = #{status}  " +
            " where id = #{id}")
    void changeSysRole(SysRole sysRole);
    @Update("<script>update sys_role  " +
            "set " +
            " retrieve_status=#{retrieveStatus}" +
            " where id in " +
            "<foreach collection='ids' item='id' index='index' open='(' separator=',' close=')'>" +
            "#{id}" +
            "</foreach></script>" )
    void changeSysRoleRetrieve(@Param("ids") String []id,int retrieveStatus);

    @Select("select * from sys_role where role_name = #{roleName} limit 1")
	SysRole getSysRoleByRoleName(String roleName);

    @Update("<script> update  sys_role  set retrieve_status = 1 where id in <foreach collection='ids' item='id' index='index' open='(' separator=',' close=')'>#{id}</foreach> </script>")
    void deleteSysRole(@Param("ids") List<String> idList);

    @Select("select * from sys_role where role_name = #{roleName} limit 1")
    SysRole findSysRoleByRoleName(String roleName);
}
