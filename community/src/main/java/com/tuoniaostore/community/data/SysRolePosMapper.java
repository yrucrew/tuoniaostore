package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysRolePos;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
@Component
@Mapper
public interface SysRolePosMapper {

    String column = "`id`, " +//
            "`role_name`, " +//角色名称
            "`remark`, " +//备注
            "`create_time`, " +//创建时间
            "`icon`, " +//
            "`sort`, " +//
            "`user_id`, " +//
            "`retrieve_status`, " +//0正常 1删除
            "`status`"//
            ;

    @Insert("insert into sys_role_pos (" +
            " `id`,  " +
            " `role_name`,  " +
            " `remark`,  " +
            " `icon`,  " +
            " `sort`,  " +
            " `user_id`,  " +
            " `retrieve_status`,  " +
            " `status` " +
            ")values(" +
            "#{id},  " +
            "#{roleName},  " +
            "#{remark},  " +
            "#{icon},  " +
            "#{sort},  " +
            "#{userId},  " +
            "#{retrieveStatus},  " +
            "#{status} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysRolePos(SysRolePos sysRolePos);

    @Results(id = "sysRolePos", value = {
            @Result(property = "id", column = "id"), @Result(property = "roleName", column = "role_name"), @Result(property = "remark", column = "remark"), @Result(property = "createTime", column = "create_time"), @Result(property = "icon", column = "icon"), @Result(property = "sort", column = "sort"), @Result(property = "userId", column = "user_id"), @Result(property = "retrieveStatus", column = "retrieve_status"), @Result(property = "status", column = "status")})
    @Select("select " + column + " from sys_role_pos where id = #{id}")
    SysRolePos getSysRolePos(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_role_pos   where   retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysRolePos")
    List<SysRolePos> getSysRolePoss(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from sys_role_pos   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysRolePos")
    List<SysRolePos> getSysRolePosAll();

    @Select("<script>select count(1) from sys_role_pos   where     retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysRolePosCount(@Param("status") int status, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Update("update sys_role_pos  " +
            "set " +
            "`role_name` = #{roleName}  , " +
            "`remark` = #{remark}  , " +
            "`create_time` = #{createTime}  , " +
            "`icon` = #{icon}  , " +
            "`sort` = #{sort}  , " +
            "`user_id` = #{userId}  , " +
            "`retrieve_status` = #{retrieveStatus}  , " +
            "`status` = #{status}  " +
            " where id = #{id}")
    void changeSysRolePos(SysRolePos sysRolePos);

    @Select("<script>select " + column + "  from sys_role_pos where role_name=#{roleName} and retrieve_status=0  and status=0 </script>")
    List<SysRolePos> getSysRolePosByRoleName(@Param("roleName") String roleName);
}
