package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysRoleMenu;
import com.tuoniaostore.datamodel.user.SysRoleMenuPos;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色与菜单对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
@Component
@Mapper
public interface SysRoleMenuPosMapper {

    String column = "`id`, " +//
            "`role_id`, " +//角色ID
            "`menu_id`, " +//菜单ID
            "`sys_channel_id`, " +//
            "`create_time`, " +//
            "`user_id`, " +//
            "`retrieve_status`"//
            ;

    @Insert("insert into sys_role_menu_pos (" +
            " `id`,  " +
            " `role_id`,  " +
            " `menu_id`,  " +
            " `sys_channel_id`,  " +
            " `user_id`,  " +
            " `retrieve_status` " +
            ")values(" +
            "#{id},  " +
            "#{roleId},  " +
            "#{menuId},  " +
            "#{sysChannelId},  " +
            "#{userId},  " +
            "#{retrieveStatus} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysRoleMenuPos(SysRoleMenuPos sysRoleMenuPos);

    @Results(id = "sysRoleMenuPos", value = {
            @Result(property = "id", column = "id"), @Result(property = "roleId", column = "role_id"), @Result(property = "menuId", column = "menu_id"), @Result(property = "sysChannelId", column = "sys_channel_id"), @Result(property = "createTime", column = "create_time"), @Result(property = "userId", column = "user_id"), @Result(property = "retrieveStatus", column = "retrieve_status")})
    @Select("select " + column + " from sys_role_menu_pos where id = #{id}")
    SysRoleMenuPos getSysRoleMenuPos(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_role_menu_pos   where   retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysRoleMenuPos")
    List<SysRoleMenuPos> getSysRoleMenuPoss(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from sys_role_menu_pos   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysRoleMenuPos")
    List<SysRoleMenuPos> getSysRoleMenuPosAll();

    @Select("<script>select count(1) from sys_role_menu_pos   where     retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysRoleMenuPosCount(@Param("status") int status, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Update("update sys_role_menu_pos  " +
            "set " +
            "`role_id` = #{roleId}  , " +
            "`menu_id` = #{menuId}  , " +
            "`sys_channel_id` = #{sysChannelId}  , " +
            "`create_time` = #{createTime}  , " +
            "`user_id` = #{userId}  , " +
            "`retrieve_status` = #{retrieveStatus}  " +
            " where id = #{id}")
    void changeSysRoleMenuPos(SysRoleMenuPos sysRoleMenuPos);

    @Insert("<script>insert into sys_role_menu_pos (" +
            " `id`,  " +
            " `role_id`,  " +
            " `menu_id`,  " +
            " `sys_channel_id`,  " +
            " `user_id`,  " +
            " `retrieve_status` " +
            ")values" +
            "<foreach collection=\"lists\" item=\"item\" separator=\",\">" +
            "(#{item.id},  " +
            "#{item.roleId},  " +
            "#{item.menuId},  " +
            "#{item.sysChannelId},  " +
            "#{item.userId},  " +
            "#{item.retrieveStatus} " +
            ")</foreach></script>")
    @Options(useGeneratedKeys = true)
    void batchAddSysRoleMenuPos(@Param("lists") List<SysRoleMenuPos> list);

    @Select("<script> select "+column+" from sys_role_menu_pos where role_id = #{roleId} and retrieve_status = 0 </script>")
    List<SysRoleMenuPos> getSysRoleMenuPosByRoleId(@Param("roleId") String roleId);

    @Select("<script> select "+column+" from sys_role_menu_pos where role_id in " +
            "<foreach collection='roleIds' item='roleId' open='(' separator=',' close=')'>" +
            "#{roleId}" +
            "</foreach>" +
            "and retrieve_status = 0 </script>")
    List<SysRoleMenuPos> getSysRoleMenusPosInRoleId(@Param("roleIds") List<String> roleIds);

    @Delete("DELETE FROM `sys_role_menu_pos` where `role_id`=#{roleId}")
    void delSysRoleMenuPosByRoleId(@Param("roleId")String roleId);
}
