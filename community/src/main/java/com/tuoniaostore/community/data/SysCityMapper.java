package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysCity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
@Component
@Mapper
public interface SysCityMapper {

    String column = "`id`, " +//
            "`code`, " +//
            "`sheng`, " +//
            "`di`, " +//
            "`xian`, " +//
            "`name`, " +//
            "`level`, " +//
            "`create_time`, " +//
            "`sort`, " +//
            "`status`, " +//
            "`longitude`, " +//
            "`latitude`, " +//
            "`retrieve_status`"//
            ;

    @Insert("insert into sys_city (" +
            " `id`,  " +
            " `code`,  " +
            " `sheng`,  " +
            " `di`,  " +
            " `xian`,  " +
            " `name`,  " +
            " `level`,  " +
            " `sort`,  " +
            " `status`,  " +
            " `longitude`,  " +
            " `latitude` " +
            ")values(" +
            "#{id},  " +
            "#{code},  " +
            "#{sheng},  " +
            "#{di},  " +
            "#{xian},  " +
            "#{name},  " +
            "#{level},  " +
            "#{sort},  " +
            "#{status},  " +
            "#{longitude},  " +
            "#{latitude} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysCity(SysCity sysCity);

    @Results(id = "sysCity", value = {
            @Result(property = "id", column = "id"), @Result(property = "code", column = "code"), @Result(property = "sheng", column = "sheng"), @Result(property = "di", column = "di"), @Result(property = "xian", column = "xian"), @Result(property = "name", column = "name"), @Result(property = "level", column = "level"), @Result(property = "createTime", column = "create_time"), @Result(property = "sort", column = "sort"), @Result(property = "status", column = "status"), @Result(property = "longitude", column = "longitude"), @Result(property = "latitude", column = "latitude"), @Result(property = "retrieveStatus", column = "retrieve_status")})
    @Select("select " + column + " from sys_city where id = #{id}")
    SysCity getSysCity(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_city   where   retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysCity")
    List<SysCity> getSysCitys(@Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Select("<script>select  " + column + "  from sys_city   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysCity")
    List<SysCity> getSysCityAll();

    @Select("<script>select  " + column + "  from sys_city   where   retrieve_status=0  and status=0 " +
            " and id in " +
            "<foreach collection='ids' item='id' index='index' open='(' separator=',' close=')'>" +
            "#{id}" +
            "</foreach>" +
            " </script>")
    @ResultMap("sysCity")
    List<SysCity> getSysCityByIds(@Param("ids") List<String> ids);

    @Select("<script>select  " + column + "  from sys_city   where   di=#{di} and level =#{level}; </script>")
    @ResultMap("sysCity")
    List<SysCity> getSysProvinceAll(@Param("di") int di, @Param("level") int level);


    @Select("<script>select  " + column + "  from sys_city   where    level=#{level} and sheng=#{sheng}; </script>")
    @ResultMap("sysCity")
    List<SysCity> getSysCityByShengLevel(@Param("sheng") int sheng, @Param("level") int level);

    @Select("<script>select  " + column + "  from sys_city   where sheng =#{sheng} and di=#{di}; </script>")
    @ResultMap("sysCity")
    List<SysCity> getSysCityByShengDi(@Param("sheng") int sheng, @Param("di") int di);


    @Select("<script>select count(1) from sys_city   where     retrieve_status=#{retrieveStatus}  " +
            " <when test=\"status != null and status !=-1\">" +
            "  and status=#{status} " +
            " </when>" +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysCityCount(@Param("status") int status, @Param("name") String name, @Param("retrieveStatus") int retrieveStatus);

    @Update("update sys_city  " +
            "set " +
            "`code` = #{code}  , " +
            "`sheng` = #{sheng}  , " +
            "`di` = #{di}  , " +
            "`xian` = #{xian}  , " +
            "`name` = #{name}  , " +
            "`level` = #{level}  , " +
            "`create_time` = #{createTime}  , " +
            "`sort` = #{sort}  , " +
            "`status` = #{status}  , " +
            "`longitude` = #{longitude}  , " +
            "`latitude` = #{latitude}  , " +
            "`retrieve_status` = #{retrieveStatus}  " +
            " where id = #{id}")
    void changeSysCity(SysCity sysCity);

    @Select("SELECT c.*,\n" +
            "(case when ap.`city_id` is null then FALSE\n" +
            "else TRUE\n" +
            "END) check_flag\n" +
            "FROM `sys_city` as c\n" +
            "LEFT JOIN (\n" +
            "SELECT ap.`city_id`\n" +
            "FROM `sys_area_province` as ap\n" +
            "WHERE ap.`area_id` = #{areaId}) as ap on c.`id` = ap.`city_id`\n" +
            "WHERE c.`level` = 1")
    @ResultMap("sysCity")
    List<SysCity> getSysProvincesCheckBind(String areaId);

    @Select("SELECT c.* FROM sys_city AS c LEFT JOIN sys_area_province AS ap ON c.id = ap.city_id WHERE c.`level` = 1 AND ap.city_id IS NULL")
    @ResultMap("sysCity")
    List<SysCity> getSysProvincesUnBind();
}
