package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysLog;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysLogMapper {

    String column = "`id`, " +//
            "`table_name`, " +//
            "`table_field`, " +//
            "`before_content`, " +//
            "`after_content`, " +//
            "`create_time`, " +//
            "`user_id`, " +//
            "`remarks`"//
            ;

    @Insert("insert into sys_log (" +
            " `id`,  " +
            " `table_name`,  " +
            " `table_field`,  " +
            " `before_content`,  " +
            " `after_content`,  " +
            " `user_id`,  " +
            " `remarks` " +
            ")values(" +
            "#{id},  " +
            "#{tableName},  " +
            "#{tableField},  " +
            "#{beforeContent},  " +
            "#{afterContent},  " +
            "#{userId},  " +
            "#{remarks} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysLog(SysLog sysLog);

    @Results(id = "sysLog", value = {
            @Result(property = "id", column = "id"), @Result(property = "tableName", column = "table_name"), @Result(property = "tableField", column = "table_field"), @Result(property = "beforeContent", column = "before_content"), @Result(property = "afterContent", column = "after_content"), @Result(property = "createTime", column = "create_time"), @Result(property = "userId", column = "user_id"), @Result(property = "remarks", column = "remarks")})
    @Select("select " + column + " from sys_log where id = #{id}")
    SysLog getSysLog(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_log   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysLog")
    List<SysLog> getSysLogs( @Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_log   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysLog")
    List<SysLog> getSysLogAll();

    @Select("<script>select count(1) from sys_log   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysLogCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_log  " +
            "set " +
            "`table_name` = #{tableName}  , " +
            "`table_field` = #{tableField}  , " +
            "`before_content` = #{beforeContent}  , " +
            "`after_content` = #{afterContent}  , " +
            "`remarks` = #{remarks}  " +
            " where id = #{id}")
    void changeSysLog(SysLog sysLog);

}
