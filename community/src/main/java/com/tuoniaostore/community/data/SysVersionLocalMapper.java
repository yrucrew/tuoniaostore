package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysVersionLocal;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@Mapper
public interface SysVersionLocalMapper {

    String column = "`id`, " +//
            "`user_version_id`, " +//版本Id
            "`user_id`, " +//可更新用户Id
            "`sys_channel_id`, " +//
            "`operate_user_id`, " +//操作人
            "`create_time`, " +//
            "`retrieve_status`"//
            ;

    @Insert("insert into sys_version_local (" +
            " `id`,  " +
            " `user_version_id`,  " +
            " `user_id`,  " +
            " `operate_user_id` " +
            ")" +
            "values(" +
            "#{id},  " +
            "#{userVersionId},  " +
            "#{userId},  " +
            "#{operateUserId}   " +
            ")" +
            "")
    @Options(useGeneratedKeys = true)
    void addSysVersionLocal(SysVersionLocal sysVersionLocal);

    @Insert("<script> insert into sys_version_local (" +
            " `id`,  " +
            " `user_version_id`,  " +
            " `user_id`,  " +
            " `operate_user_id`,  " +
            " `retrieve_status` " +
            ")values <foreach collection='list' item='list' separator=','>(#{list.id},#{list.userVersionId},#{list.userId},#{list.operateUserId},#{list.retrieveStatus})</foreach></script> ")
    @Options(useGeneratedKeys = true)
    void addSysVersionLocalBatch(@Param("list") List<SysVersionLocal> sysVersionLocal);

    @Results(id = "sysVersionLocal", value = {
            @Result(property = "id", column = "id"), @Result(property = "userVersionId", column = "user_version_id"), @Result(property = "userId", column = "user_id"), @Result(property = "operateUserId", column = "operate_user_id"), @Result(property = "createTime", column = "create_time"), @Result(property = "retrieveStatus", column = "retrieve_status")})
    @Select("select " + column + " from sys_version_local where id = #{id}")
    SysVersionLocal getSysVersionLocal(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_version_local   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysVersionLocal")
    List<SysVersionLocal> getSysVersionLocals( @Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_version_local   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("sysVersionLocal")
    List<SysVersionLocal> getSysVersionLocalAll();

    @Select("<script>select count(1) from sys_version_local   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysVersionLocalCount(@Param("status") int status, @Param("name") String name);

    @Update("update sys_version_local  " +
            "set " +
            "`user_version_id` = #{userVersionId}  , " +
            "`user_id` = #{userId}  , " +
            "`operate_user_id` = #{operateUserId}    " +
            " where id = #{id}")
    void changeSysVersionLocal(SysVersionLocal sysVersionLocal);

    @Select("SELECT " + column + " FROM `sys_version_local` where retrieve_status = 0 AND user_id = #{userId}")
    @ResultMap("sysVersionLocal")
    SysVersionLocal getSysVersionLocalByUserId(@Param("userId")String userId);
}
