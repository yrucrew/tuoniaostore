package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysArea;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
@Component
@Mapper
public interface SysAreaMapper {

    String column = "`id`, " +//
            "`title`, " +//区域标题
            "`sort`, " +//排序
            "`status`, " +//状态0.可用 1.禁用
            "`remarks`"//备注
            ;

    @Insert("insert into sys_area (" +
            " `id`,  " +
            " `title`,  " +
            " `sort`,  " +
            " `status`,  " +
            "`retrieve_status`," +
            " `remarks` " +
            ")values(" +
            "#{id},  " +
            "#{title},  " +
            "#{sort},  " +
            "#{status},  " +
            "#{retrieve_status}," +
            "#{remarks} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addSysArea(SysArea sysArea);

    @Results(id = "sysArea", value = {
            @Result(property = "id", column = "id"), @Result(property = "title", column = "title"), @Result(property = "sort", column = "sort"), @Result(property = "status", column = "status"), @Result(property = "remarks", column = "remarks")})
    @Select("select " + column + " from sys_area where id = #{id}")
    SysArea getSysArea(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_area   where    1=1 and retrieve_status = 0 " +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when>" +
            " <when test=\"status == -1\">" +
            " and status in (0,1)"+
            " </when> "+
            " <when test=\"status != -1\">" +
            " and status =#{status}"+
            " </when> "+
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysArea")
    List<SysArea> getSysAreas(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("title") String title, @Param("status") int status);

    @Select("<script>select  " + column + "  from sys_area   where    status=0 </script>")
    @ResultMap("sysArea")
    List<SysArea> getSysAreaAll();

    @Select("<script>select count(1) from sys_area   where    1=1  and retrieve_status = 0 " +
            " <when test=\"title != null and title.trim() != ''\">" +
            "  and title like concat('%', #{title}, '%') " +
            " </when></script>")
    int getSysAreaCount(@Param("title") String title);

    @Update("update sys_area  " +
            "set " +
            "`title` = #{title}  , " +
            "`sort` = #{sort}  , " +
            "`status` = #{status}  , " +
            "`remarks` = #{remarks}  " +
            " where id = #{id}")
    void changeSysArea(SysArea sysArea);

    @Select("<script>select  " + column + "  from sys_area  where retrieve_status = 0 and status = 0 " +
            "<if test=\"areaName != null and areaName != ''\">and title like concat('%',#{areaName},'%')</if> " +
            " <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysArea")
    List<SysArea> getSysAreasByPage(@Param("areaName") String areaName,@Param("pageStartIndex") Integer pageStartIndex, @Param("pageSize") Integer pageSize);

    @Select("<script>select  count(*)  from sys_area  where 1 = 1 and status = 0 " +
            "<if test=\"areaName != null and areaName != ''\">and title like concat('%',#{areaName},'%')</if> " +
            " </script>")
    int getSysAreasByPageCount(@Param("areaName")String areaName);

    @Select("<script>select  " + column + "  from sys_area where 1 = 1 and status = 0 " +
            "<if test=\"lists != null and lists.size > 0\">and id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></if> " +
            " <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when></script>")
    @ResultMap("sysArea")
    List<SysArea> getSysAreasByIds(@Param("lists")List<String> ids, @Param("pageStartIndex") int pageStartIndex, @Param("pageSize") int pageSize);

    @Select("<script>select  " + column + "  from sys_area where 1 = 1 and status = 0 " +
            "<if test=\"areaName != null and areaName != ''\">and title like concat('%',#{areaName},'%')</if> " +
            " <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            "</when></script>")
    @ResultMap("sysArea")
    List<SysArea> getSysAreasByNameLike(@Param("areaName")String areaName, @Param("pageStartIndex")Integer pageStartIndex,@Param("pageSize") Integer pageSize);

    @Select("<script>select  " + column + "  from sys_area where 1 = 1 and status = 0" +
            "<if test=\"title != null and title != ''\"> and title = #{title} </if> " +
            "</script>")
    @ResultMap("sysArea")
    SysArea getSysAreasByTitle(@Param("title") String title);

    @Update("<script>UPDATE `sys_area` AS a SET a.`retrieve_status` = #{status} where a.`id` in <foreach collection=\"ids\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </script>")
    void deleteSysAreas(List<String> ids, int status);


}
