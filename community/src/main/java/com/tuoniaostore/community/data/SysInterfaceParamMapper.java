package com.tuoniaostore.community.data;

import com.tuoniaostore.datamodel.user.SysInterfaceParam;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
@Component
@Mapper
public interface SysInterfaceParamMapper {

    String column = "`id`, " +//
            "`name`, " +//参数名称
            "`interface_id`, " +//接口
            "`data_type`, " +//
            "`fill_type`, " +//0.选填 1.必填
            "`default_value`, " +//默认值
            "`remark`, " +//备注
            "`type`"//0.入参 1.输入字段说明
            ;

    @Insert("<script>insert into sys_interface_param (" +
            " `id`," +
            " `name`," +
            " `interface_id`," +
            " `data_type`," +
            " `fill_type`," +
            " `default_value`," +
            " `remark`," +
            " `type`" +
            ")values" +
                "<foreach collection='list' item='sysInterfaceParam' separator=','  >" +
                    "(#{sysInterfaceParam.id}," +
                    " #{sysInterfaceParam.name}," +
                    " #{sysInterfaceParam.interfaceId}, " +
                    "#{sysInterfaceParam.dataType}," +
                    " #{sysInterfaceParam.fillType}, " +
                    "#{sysInterfaceParam.defaultValue}," +
                    "#{sysInterfaceParam.remark}," +
                    "#{sysInterfaceParam.type})" +
                "</foreach>" +
            "</script>")
    @Options(useGeneratedKeys = true)
    void addSysInterfaceParam(List<SysInterfaceParam> list);

    @Results(id = "sysInterfaceParam", value = {
            @Result(property = "id", column = "id"), @Result(property = "name", column = "name"),
            @Result(property = "interfaceId", column = "interface_id"),
            @Result(property = "dataType", column = "data_type"),
            @Result(property = "fillType", column = "fill_type"), @Result(property = "defaultValue", column = "default_value"), @Result(property = "remark", column = "remark"), @Result(property = "type", column = "type")})
    @Select("select " + column + " from sys_interface_param where id = #{id}")
    SysInterfaceParam getSysInterfaceParam(@Param("id") String id);

    @Select("<script>select  " + column + "  from sys_interface_param   where   1=1  " +

            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("sysInterfaceParam")
    List<SysInterfaceParam> getSysInterfaceParams(  @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from sys_interface_param     </script>")
    @ResultMap("sysInterfaceParam")
    List<SysInterfaceParam> getSysInterfaceParamAll();

    @Select("<script>select  " + column + "  from sys_interface_param   where    interface_id=#{interfaceId} </script>")
    @ResultMap("sysInterfaceParam")
    List<SysInterfaceParam> getSysInterfaceParamByInterfaceId(@Param("interfaceId") String interfaceId);

    @Select("<script>select count(1) from sys_interface_param   where   1=1   " +

            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getSysInterfaceParamCount(  @Param("name") String name );

    @Update("update sys_interface_param  " +
            "set " +
            "`name` = #{name}  , " +
            "`interface_id` = #{interfaceId}  , " +
            "`data_type` = #{dataType}  , " +
            "`fill_type` = #{fill_type}  , " +
            "`default_value` = #{defaultValue}  , " +
            "`remark` = #{remark}  , " +
            "`type` = #{type}  " +
            " where id = #{id}")
    void changeSysInterfaceParam(SysInterfaceParam sysInterfaceParam);

    @Delete("DELETE FROM  `sys_interface_param` where interface_id=#{interfaceId}")
    void  delSysInterfaceParam(@Param("interfaceId")String interfaceId);
}
