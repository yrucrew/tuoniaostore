package com.tuoniaostore.community.upload;

import com.tuoniaostore.commons.constant.OssConstant;
import com.tuoniaostore.commons.file.oss.AliyunOSSUtils;
import com.tuoniaostore.datamodel.DefineRandom;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;

/**
 * 图片上传
 * Created by liyunbiao on 2017/11/27.
 */
@Controller
@RequestMapping(value = "file")
public class UploadFileController {

    private static final Logger logger = LoggerFactory.getLogger(UploadFileController.class);

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public @ResponseBody
    Object upload(MultipartFile file) {//1
        Map map = new HashMap();
        try {
            String filename = getFileName() + file.getOriginalFilename();
            String url = AliyunOSSUtils.uploadImageFiles(filename, "ostrichapps", file.getInputStream());
            map.put("code", 0);
            map.put("msg", "上传成功");
            Map path = new HashMap();
            path.put("src", url);
            path.put("url", filename);
            map.put("data", path);
        } catch (Exception e) {
            map.put("code", -1);
            e.printStackTrace();
        }
        return map;
    }

    public String getFileName() {
        long time = System.currentTimeMillis();
        String fileName = (time + DefineRandom.random(128)) + "";
        return fileName;
    }


    @RequestMapping(value = "/uploadimage", method = RequestMethod.POST)
    @ResponseBody
    public Object uploadbanner(MultipartFile file) {
        Map map = new HashMap();
        try {
            String filename = getFileName();
            String url = AliyunOSSUtils.uploadImageFiles(filename + "_" + file.getOriginalFilename(), "ostrichapps", file.getInputStream());
            map.put("code", 0);
            map.put("msg", "上传成功");
            Map path = new HashMap();
            path.put("src", url);
            path.put("url", filename);
            map.put("data", path);
        } catch (Exception e) {
            map.put("code", -1);
            e.printStackTrace();
        }
        return map;
    }

    @ApiOperation("上传退款备注图片")
    @PostMapping("uploadOrderRefundImg")
    @ResponseBody
    public Object uploadOrderRefundImg(MultipartFile file) {
        Map map = new HashMap();
        logger.info("开始上传退款备注图片");
        try {
            String filename = getFileName() + new Date().getTime();
            String originalFilename = file.getOriginalFilename();
            String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
            String url = AliyunOSSUtils.uploadImageFiles(OssConstant.ORDER_REFUND_REMARK + filename + suffix, "ostrichapps", file.getInputStream());
            map.put("code", 0);
            map.put("msg", "上传退款备注图片成功");
            Map path = new HashMap();
            path.put("src", url);
            path.put("filename", filename);
            map.put("data", path);
        } catch (Exception e) {
            map.put("code", -1);
            logger.info("上传退款备注图片失败");
            e.printStackTrace();
        }
        return map;
    }

    @RequestMapping(value = "/uploadimageExp", method = RequestMethod.POST)
    @ResponseBody
    public Object uploadbannerExp(MultipartFile file) {
        Map mapOut = new HashMap();
        try {
            InputStream is = file.getInputStream();
            Workbook wb = null;
            String extString = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            try {
                if (".xls".equals(extString)) {
                    return wb = new HSSFWorkbook(is);
                } else if (".xlsx".equals(extString)) {
                    return wb = new XSSFWorkbook(is);
                } else {
                    return wb = null;
                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Sheet sheet = null;
            Row row = null;
            List<Map<String, String>> list = null;
            String cellData = null;
//            String columns[] = {"品牌名称", "默认条形码", "商品名称", "规格", "单位", "保质期","装箱数", "建议零售价", "零售默认价格","中分类"}; good_tempalte good_price good_barcode
            String columns[] = {"goodBrandName", "barcode", "goodName", "priceName", "unitName", "shelfLife","num", "salePrice", "costPrice","twoTpeName"};
            if (wb != null) {
                //用来存放表中数据
                list = new ArrayList<Map<String, String>>();
                //获取第一个sheet
                sheet = wb.getSheetAt(0);
                //获取最大行数
                int rownum = sheet.getPhysicalNumberOfRows();
                //获取第一行
                row = sheet.getRow(0);
                //获取最大列数
                int colnum = row.getPhysicalNumberOfCells();
                for (int i = 1; i < rownum; i++) {
                    Map<String, String> map = new LinkedHashMap<String, String>();
                    row = sheet.getRow(i);
                    if (row != null) {
                        for (int j = 0; j < colnum; j++) {
                            cellData = (String) getCellFormatValue(row.getCell(j));
                            map.put(columns[j], cellData);
                        }
                    } else {
                        break;
                    }
                    list.add(map);
                }
            }
            //遍历解析出来的list
            for (Map<String, String> map : list) {
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    System.out.print(entry.getKey() + ":" + entry.getValue() + ",");
                }
                System.out.println();
            }
        } catch (Exception e) {
            mapOut.put("code", -1);
            e.printStackTrace();
        }
        return mapOut;
    }


    public static void main(String[] args) {
        UploadFileController uploadFileController = new UploadFileController();
        String fileName = uploadFileController.getFileName();
        File file = new File("C:\\Users\\Quan\\Pictures\\avatar.jpg");
        String originalFilename = file.getName();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        try {
            String url = AliyunOSSUtils.uploadImageFiles(OssConstant.ORDER_REFUND_REMARK + fileName + "_" + new Date().getTime() + suffix, "ostrichapps", new FileInputStream(file));
            System.out.println(url);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//        try {
//            String filepath = "D:"+File.separator+"test"+File.separator+"good.xlsx";
//
//            File file = new File(filepath);
//            if (!file.isDirectory()) {
//                System.out.println("文件");
//                System.out.println("path=" + file.getPath());
//                System.out.println("absolutepath=" + file.getAbsolutePath());
//                System.out.println("name=" + file.getName());
//
//                String[] filelist = file.list();
//                for (int i = 0; i < filelist.length; i++) {
//                    File readfile = new File(filepath + "\\" + filelist[i]);
//                    if (!readfile.isDirectory()) {
//
//                        String barcode = readfile.getName().substring(0, readfile.getName().indexOf(".")).trim().replace("00000\\", "");
//
//                        if (barcode != null && barcode.length() > 0) {
//                            System.out.println("" + barcode.replace("00000\\", ""));
//                        }
//                    }
//                }
//
//            } else if (file.isDirectory()) {
//                System.out.println("文件夹");
//
//            }
//
//        } catch (Exception e) {
//            //   System.out.println("readfile()   Exception:" + e.getMessage());
//        }
    }


    public static Object getCellFormatValue(Cell cell) {
        Object cellValue = null;
        if (cell != null) {
            //判断cell类型
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_NUMERIC: {
                    cellValue = String.valueOf(cell.getNumericCellValue());
                    break;
                }
                case Cell.CELL_TYPE_FORMULA: {
                    //判断cell是否为日期格式
                    if (DateUtil.isCellDateFormatted(cell)) {
                        //转换为日期格式YYYY-mm-dd
                        cellValue = cell.getDateCellValue();
                    } else {
                        //数字
                        cellValue = String.valueOf(cell.getNumericCellValue());
                    }
                    break;
                }
                case Cell.CELL_TYPE_STRING: {
                    cellValue = cell.getRichStringCellValue().getString();
                    break;
                }
                default:
                    cellValue = "";
            }
        } else {
            cellValue = "";
        }
        return cellValue;
    }


}
