package com.tuoniaostore.community;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by liyunbiao on 2019/5/21.
 */
@Controller
public class HelloController {
    @RequestMapping("/")
    @ResponseBody
    public JSONObject hello() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 0);
        return jsonObject;
    }
}
