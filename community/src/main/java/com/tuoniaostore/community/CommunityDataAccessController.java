package com.tuoniaostore.community;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.community.service.SysCodeService;
import com.tuoniaostore.community.service.SysUserService;
import com.tuoniaostore.datamodel.user.SysUser;
import io.swagger.annotations.Api;
import org.apache.ibatis.annotations.ResultMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/18
 */
@RequestMapping("api")
@Controller
public class CommunityDataAccessController {

    @Autowired
    private SysUserService sysUserService;


    @Autowired
    private SysCodeService sysCodeService;


    @RequestMapping("addSysCode")
    @ResponseBody
    public JSONObject addSysCode() {
        return sysCodeService.addSysCode();
    }

    @RequestMapping("getSysCode")
    @ResponseBody
    public JSONObject getSysCode() {
        return sysCodeService.getSysCode();
    }


    @RequestMapping("getSysCodes")
    @ResponseBody
    public JSONObject getSysCodes() {
        return sysCodeService.getSysCodes();
    }

    @RequestMapping("getSysCodeAll")
    @ResponseBody
    public JSONObject getSysCodeAll() {
        return sysCodeService.getSysCodeAll();
    }

    @RequestMapping("getSysCodeCount")
    @ResponseBody
    public JSONObject getSysCodeCount() {
        return sysCodeService.getSysCodeCount();
    }

    @RequestMapping("changeSysCode")
    @ResponseBody
    public JSONObject changeSysCode() {
        return sysCodeService.changeSysCode();
    }

    /**
     *  登录
     * @return
     */
    @RequestMapping("login")
    @ResponseBody
    public JSONObject login() {
        return sysUserService.login();
    }


    /**
     *  注册
     * @return
     */
    @RequestMapping("register")
    @ResponseBody
    public JSONObject register() {
        return sysUserService.register();
    }

    /**
     *  注册时 获取短信验证码
     * @return
     */
    @RequestMapping("getMsgCode")
    @ResponseBody
    public JSONObject getMsgCode() {
        return sysUserService.getMsgCode();
    }


    /**
     *  根据手机号码找回密码 获取手机验证码
     * @return
     */
    @RequestMapping("findPasswordByPhone")
    @ResponseBody
    public JSONObject findPasswordByPhone() {
        return sysUserService.findPasswordByPhone();
    }


    /**
     *  根据手机号码找回密码
     * @return
     */
    @RequestMapping("findPassword")
    @ResponseBody
    public JSONObject findPassword() {
        return sysUserService.findPassword();
    }

}
