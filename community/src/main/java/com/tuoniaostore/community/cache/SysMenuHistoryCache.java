package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysMenuHistory;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysMenuHistoryCache {

    void addSysMenuHistory(SysMenuHistory sysMenuHistory);

    SysMenuHistory getSysMenuHistory(String id);

    List<SysMenuHistory> getSysMenuHistorys(int pageIndex, int pageSize, int status, String name);
    List<SysMenuHistory> getSysMenuHistoryAll();
    int getSysMenuHistoryCount(int status, String name);
    void changeSysMenuHistory(SysMenuHistory sysMenuHistory);

}
