package com.tuoniaostore.community.cache;

import com.alibaba.fastjson.JSONArray;
import com.tuoniaostore.datamodel.user.SysMenu;
import com.tuoniaostore.datamodel.user.SysUser;

import java.util.List;

/**
 * 菜单管理
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysMenuCache {

    void addSysMenu(SysMenu sysMenu);

    SysMenu getSysMenu(String id);

    List<SysMenu> getSysMenus(int pageIndex, int pageSize, int status, String name);

    List<SysMenu> getSysMenuAll();

    int getSysMenuCount(int status, String name);

    void changeSysMenu(SysMenu sysMenu);
    SysMenu findByMenu(  String name,  String parentId);
    List<SysMenu> getSysMenuChilds(String parentId, List<String> menuIds);
    JSONArray getWebSysMenus(String roleId);

    void deleteSysMenu(List<String> menuIds);
}
