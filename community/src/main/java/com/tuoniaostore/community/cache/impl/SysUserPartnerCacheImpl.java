package com.tuoniaostore.community.cache.impl;
        import com.tuoniaostore.community.cache.SysUserPartnerCache;
        import com.tuoniaostore.community.data.SysUserPartnerMapper;
        import com.tuoniaostore.datamodel.user.SysUserPartner;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysUserPartner")
public class SysUserPartnerCacheImpl implements SysUserPartnerCache {

    @Autowired
    SysUserPartnerMapper mapper;

    @Override
    @CacheEvict(value = "sysUserPartner", allEntries = true)
    public void addSysUserPartner(SysUserPartner sysUserPartner) {
        mapper.addSysUserPartner(sysUserPartner);
    }

    @Override
    @Cacheable(value = "sysUserPartner", key = "'getSysUserPartner_'+#id")
    public   SysUserPartner getSysUserPartner(String id) {
        return mapper.getSysUserPartner(id);
    }

    @Override
    @Cacheable(value = "sysUserPartner", key = "'getSysUserPartners_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysUserPartner> getSysUserPartners( int status,int pageIndex, int pageSize, String name) {
        return mapper.getSysUserPartners( status, pageIndex, pageSize,name);
    }

    @Override
    @Cacheable(value = "sysUserPartner", key = "'getSysUserPartnerAll'")
    public List<SysUserPartner> getSysUserPartnerAll() {
        return mapper.getSysUserPartnerAll();
    }

    @Override
    @Cacheable(value = "sysUserPartner", key = "'getSysUserPartnerCount_'+#status+'_'+#name")
    public int getSysUserPartnerCount(int status, String name) {
        return mapper.getSysUserPartnerCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysUserPartner", allEntries = true)
    public void changeSysUserPartner(SysUserPartner sysUserPartner) {
        mapper.changeSysUserPartner(sysUserPartner);
    }

}
