package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysUserPartner;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysUserPartnerCache {

    void addSysUserPartner(SysUserPartner sysUserPartner);

    SysUserPartner getSysUserPartner(String id);

    List<SysUserPartner> getSysUserPartners(int pageIndex, int pageSize, int status, String name);
    List<SysUserPartner> getSysUserPartnerAll();
    int getSysUserPartnerCount(int status, String name);
    void changeSysUserPartner(SysUserPartner sysUserPartner);

}
