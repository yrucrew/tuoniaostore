package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysConfig;

import java.util.List;
/**
 * 系统配置信息表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysConfigCache {

    void addSysConfig(SysConfig sysConfig);

    SysConfig getSysConfig(String id);

    List<SysConfig> getSysConfigs(int pageIndex, int pageSize, int status, String name);
    List<SysConfig> getSysConfigAll();
    int getSysConfigCount(int status, String name);
    void changeSysConfig(SysConfig sysConfig);

}
