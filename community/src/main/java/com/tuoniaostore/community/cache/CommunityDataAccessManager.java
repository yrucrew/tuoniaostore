package com.tuoniaostore.community.cache;

import com.alibaba.fastjson.JSONArray;
import com.tuoniaostore.commons.constant.user.RetrieveStatusEnum;
import com.tuoniaostore.commons.constant.user.UserRoleTypeEnum;
import com.tuoniaostore.community.vo.PurchaseUserVo;
import com.tuoniaostore.datamodel.good.GoodShopCart;
import com.tuoniaostore.datamodel.user.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by liyunbiao on 2019/3/5.
 */
@Component
public class CommunityDataAccessManager {
    @Autowired
    SysAreaCache sysAreaCache;

    public void addSysArea(SysArea sysArea) {
        sysAreaCache.addSysArea(sysArea);
    }

    public SysArea getSysArea(String id) {
        return sysAreaCache.getSysArea(id);
    }

    public List<SysArea> getSysAreas(int pageIndex, int pageSize, String title, int status) {
        return sysAreaCache.getSysAreas(pageIndex, pageSize, title, status);
    }

    public List<SysArea> getSysAreaAll() {
        return sysAreaCache.getSysAreaAll();
    }

    public int getSysAreaCount(String title) {
        return sysAreaCache.getSysAreaCount(title);
    }

    public void changeSysArea(SysArea sysArea) {
        sysAreaCache.changeSysArea(sysArea);
    }

    public List<SysArea> getSysAreasByPage(String areaName,int pageStartIndex, int pageSize) {
        return sysAreaCache.getSysAreasByPage(areaName,pageStartIndex,pageSize);
    }

    public int getSysAreasByPageCount(String areaName) {
        return sysAreaCache.getSysAreasByPageCount(areaName);
    }

    public List<SysArea> getSysAreasByIds(List<String> ids, int pageStartIndex, int pageSize) {
        return sysAreaCache.getSysAreasByIds(ids,pageStartIndex,pageSize);
    }

    public List<SysArea> getSysAreasByNameLike(String areaName, Integer pageStartIndex, Integer pageSize) {
        return sysAreaCache.getSysAreasByNameLike(areaName,pageStartIndex,pageSize);
    }

    public SysArea getSysAreasByTitle(String title) {
        return sysAreaCache.getSysAreasByTitle(title);
    }

    /**
     * 删除区域
     * @param ids
     */
    public void deleteSysAreas(List<String> ids) {
        sysAreaCache.deleteSysAreas(ids);
    }

    @Autowired
    SysAreaProvinceCache sysAreaProvinceCache;

    public void addSysAreaProvince(List<SysAreaProvince> list) {
        sysAreaProvinceCache.addSysAreaProvince(list);
    }

    public List<SysAreaProvince> getSysAreaNotId(String areaId) {
        return sysAreaProvinceCache.getSysAreaNotId(areaId);
    }

    public List<SysAreaProvince> getSysAreaProvinceByAreaId(String areaId) {
        return sysAreaProvinceCache.getSysAreaProvinceByAreaId(areaId);
    }

    public void delSysAreaProvince(String areaId) {
        sysAreaProvinceCache.delSysAreaProvince(areaId);
    }

    public SysAreaProvince getSysAreaProvince(String id) {
        return sysAreaProvinceCache.getSysAreaProvince(id);
    }

    public List<SysAreaProvince> getSysAreaProvinces(int status, int pageIndex, int pageSize, String name) {
        return sysAreaProvinceCache.getSysAreaProvinces(status, pageIndex, pageSize, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public List<SysAreaProvince> getSysAreaProvinceAll() {
        return sysAreaProvinceCache.getSysAreaProvinceAll();
    }

    public int getSysAreaProvinceCount(int status, String name) {
        return sysAreaProvinceCache.getSysAreaProvinceCount(status, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeSysAreaProvince(SysAreaProvince sysAreaProvince) {
        sysAreaProvinceCache.changeSysAreaProvince(sysAreaProvince);
    }

    public void deleteSysAreaProvinceByAreaIds(List<String> areaIds) {
        sysAreaProvinceCache.deleteSysAreaProvinceByAreaIds(areaIds);
    }

    @Autowired
    SysCityCache sysCityCache;

    public List<SysCity> getSysCityByIds(List<String> ids) {
        return sysCityCache.getSysCityByIds(ids);
    }

    public List<SysCity> getSysCityByShengLevel(int sheng, int level) {
        return sysCityCache.getSysCityByShengLevel(sheng, level);
    }

    public List<SysCity> getSysCityByShengDi(int sheng, int di) {
        return sysCityCache.getSysCityByShengDi(sheng, di);
    }

    public void addSysCity(SysCity sysCity) {
        sysCityCache.addSysCity(sysCity);
    }

    public SysCity getSysCity(String id) {
        return sysCityCache.getSysCity(id);
    }

    public List<SysCity> getSysCitys(int status, int pageIndex, int pageSize, String name) {
        return sysCityCache.getSysCitys(status, pageIndex, pageSize, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public List<SysCity> getSysCityAll() {
        return sysCityCache.getSysCityAll();
    }

    public int getSysCityCount(int status, String name) {
        return sysCityCache.getSysCityCount(status, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeSysCity(SysCity sysCity) {
        sysCityCache.changeSysCity(sysCity);
    }

    public List<SysCity> getSysProvinceAll(int di, int level) {
        return sysCityCache.getSysProvinceAll(di, level);
    }

    public List<SysCity> getSysProvincesCheckBind(String areaId) {
        return sysCityCache.getSysProvincesCheckBind(areaId);
    }

    public List<SysCity> getSysProvincesUnBind() {
        return sysCityCache.getSysProvincesUnBind();
    }

    @Autowired
    SysCodeCache sysCodeCache;

    public void addSysCode(SysCode sysCode) {
        sysCodeCache.addSysCode(sysCode);
    }

    public SysCode getSysCode(String id) {
        return sysCodeCache.getSysCode(id);
    }

    public List<SysCode> getSysCodes(int status, int pageIndex, int pageSize, String name) {
        return sysCodeCache.getSysCodes(status, pageIndex, pageSize, name);
    }

    public SysCode getSysCodeName(String codeName) {
        return sysCodeCache.getSysCodeName(codeName);
    }

    public List<SysCode> getSysCodeAll() {
        return sysCodeCache.getSysCodeAll();
    }

    public int getSysCodeCount(int status, String name) {
        return sysCodeCache.getSysCodeCount(status, name);
    }

    public void changeSysCode(SysCode sysCode) {
        sysCodeCache.changeSysCode(sysCode);
    }

    @Autowired
    SysConfigCache sysConfigCache;

    public void addSysConfig(SysConfig sysConfig) {
        sysConfigCache.addSysConfig(sysConfig);
    }

    public SysConfig getSysConfig(String id) {
        return sysConfigCache.getSysConfig(id);
    }

    public List<SysConfig> getSysConfigs(int status, int pageIndex, int pageSize, String name) {
        return sysConfigCache.getSysConfigs(status, pageIndex, pageSize, name);
    }

    public List<SysConfig> getSysConfigAll() {
        return sysConfigCache.getSysConfigAll();
    }

    public int getSysConfigCount(int status, String name) {
        return sysConfigCache.getSysConfigCount(status, name);
    }

    public void changeSysConfig(SysConfig sysConfig) {
        sysConfigCache.changeSysConfig(sysConfig);
    }

    @Autowired
    SysLogCache sysLogCache;

    public void addSysLog(SysLog sysLog) {
        sysLogCache.addSysLog(sysLog);
    }

    public SysLog getSysLog(String id) {
        return sysLogCache.getSysLog(id);
    }

    public List<SysLog> getSysLogs(int status, int pageIndex, int pageSize, String name) {
        return sysLogCache.getSysLogs(status, pageIndex, pageSize, name);
    }

    public List<SysLog> getSysLogAll() {
        return sysLogCache.getSysLogAll();
    }

    public int getSysLogCount(int status, String name) {
        return sysLogCache.getSysLogCount(status, name);
    }

    public void changeSysLog(SysLog sysLog) {
        sysLogCache.changeSysLog(sysLog);
    }

    @Autowired
    SysMenuCache sysMenuCache;

    public SysMenu findByMenu(String name, String parentId) {
        return sysMenuCache.findByMenu(name, parentId);
    }

    public void addSysMenu(SysMenu sysMenu) {
        sysMenuCache.addSysMenu(sysMenu);
    }

    public SysMenu getSysMenu(String id) {
        return sysMenuCache.getSysMenu(id);
    }

    public List<SysMenu> getSysMenus(int status, int pageIndex, int pageSize, String name) {
        return sysMenuCache.getSysMenus(status, pageIndex, pageSize, name);
    }

    public void changeRetrieveStatus(String tableName, int retrieveStatus, String id) {
        sysRetrieveCache.changeRetrieveStatus(tableName, retrieveStatus, id);
    }

    public List<SysMenu> getSysMenuAll() {
        return sysMenuCache.getSysMenuAll();
    }

    public int getSysMenuCount(int status, String name) {
        return sysMenuCache.getSysMenuCount(status, name);
    }

    public void changeSysMenu(SysMenu sysMenu) {
        sysMenuCache.changeSysMenu(sysMenu);
    }

    public List<SysMenu> getSysMenuChilds(String parentId, List<String> menuIds) {
        return sysMenuCache.getSysMenuChilds(parentId, menuIds);
    }

    public List<SysRoleMenu> getSysRoleMenusByRoleId(String roleId) {
        return sysRoleMenuCache.getSysRoleMenusByRoleId(roleId);
    }

    public JSONArray getWebSysMenus(String roleId) {
        return sysMenuCache.getWebSysMenus(roleId);
    }

    public void deleteSysMenu(List<String> menuIds) {
        sysMenuCache.deleteSysMenu(menuIds);
    }

    @Autowired
    SysMenuHistoryCache sysMenuHistoryCache;

    public void addSysMenuHistory(SysMenuHistory sysMenuHistory) {
        sysMenuHistoryCache.addSysMenuHistory(sysMenuHistory);
    }

    public SysMenuHistory getSysMenuHistory(String id) {
        return sysMenuHistoryCache.getSysMenuHistory(id);
    }

    public List<SysMenuHistory> getSysMenuHistorys(int status, int pageIndex, int pageSize, String name) {
        return sysMenuHistoryCache.getSysMenuHistorys(status, pageIndex, pageSize, name);
    }

    public List<SysMenuHistory> getSysMenuHistoryAll() {
        return sysMenuHistoryCache.getSysMenuHistoryAll();
    }

    public int getSysMenuHistoryCount(int status, String name) {
        return sysMenuHistoryCache.getSysMenuHistoryCount(status, name);
    }

    public void changeSysMenuHistory(SysMenuHistory sysMenuHistory) {
        sysMenuHistoryCache.changeSysMenuHistory(sysMenuHistory);
    }

    @Autowired
    SysRetrieveCache sysRetrieveCache;

    public void addSysRetrieve(SysRetrieve sysRetrieve) {
        sysRetrieveCache.addSysRetrieve(sysRetrieve);
    }

    public void addSysRetrieves(List<SysRetrieve> sysRetrieve) {
        sysRetrieveCache.addSysRetrieves(sysRetrieve);
    }

    public SysRetrieve getSysRetrieve(String id) {
        return sysRetrieveCache.getSysRetrieve(id);
    }

    public List<SysRetrieve> getSysRetrieves(int status, int pageIndex, int pageSize, String name) {
        return sysRetrieveCache.getSysRetrieves(status, pageIndex, pageSize, name);
    }

    public List<SysRetrieve> getSysRetrieveAll() {
        return sysRetrieveCache.getSysRetrieveAll();
    }

    public int getSysRetrieveCount(int status, String name) {
        return sysRetrieveCache.getSysRetrieveCount(status, name);
    }

    public void changeSysRetrieve(SysRetrieve sysRetrieve) {
        sysRetrieveCache.changeSysRetrieve(sysRetrieve);
    }

    @Autowired
    SysRoleCache sysRoleCache;

    public void addSysRole(SysRole sysRole) {
        sysRoleCache.addSysRole(sysRole);
    }

    public SysRole getSysRole(String id) {
        return sysRoleCache.getSysRole(id);
    }

    public List<SysRole> getSysRoles(int status, int pageIndex, int pageSize, String name) {
        return sysRoleCache.getSysRoles(status, pageIndex, pageSize, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public List<SysRole> getSysRoleAll() {
        return sysRoleCache.getSysRoleAll();
    }

    public int getSysRoleCount(int status,  String name) {
        return sysRoleCache.getSysRoleCount(status,   name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeSysRole(SysRole sysRole) {
        sysRoleCache.changeSysRole(sysRole);
    }

    public SysRole getSysRoleByRoleName(UserRoleTypeEnum userRoleTypeEnum) {
        return sysRoleCache.getSysRoleByRoleName(userRoleTypeEnum.getValue());
    }
    public void changeSysRoleRetrieve(String[] id, int retrieveStatus) {
        sysRoleCache.changeSysRoleRetrieve(id, retrieveStatus);
    }

    public void deleteSysRole(List<String> idList) {
        sysRoleCache.deleteSysRole(idList);
    }

    public SysRole findSysRoleByRoleName(String roleName) {
        return sysRoleCache.findSysRoleByRoleName(roleName);
    }

    @Autowired
    SysRoleMenuCache sysRoleMenuCache;

    public void addSysRoleMenu(List<SysRoleMenu> list) {
        sysRoleMenuCache.addSysRoleMenu(list);
    }

    public void delSysRoleMenuByRoleId(String roleId) {
        sysRoleMenuCache.delSysRoleMenuByRoleId(roleId);
    }

    public SysRoleMenu getSysRoleMenu(String id) {
        return sysRoleMenuCache.getSysRoleMenu(id);
    }

    public List<SysRoleMenu> getSysRoleMenus(int status, int pageIndex, int pageSize, String name) {
        return sysRoleMenuCache.getSysRoleMenus(status, pageIndex, pageSize, name);
    }

    public List<SysRoleMenu> getSysRoleMenuAll() {
        return sysRoleMenuCache.getSysRoleMenuAll();
    }

    public int getSysRoleMenuCount(int status, String name) {
        return sysRoleMenuCache.getSysRoleMenuCount(status, name);
    }

    public void changeSysRoleMenu(SysRoleMenu sysRoleMenu) {
        sysRoleMenuCache.changeSysRoleMenu(sysRoleMenu);
    }

    public void deleteSysRoleMenu(List<String> menuIds) {
        sysRoleMenuCache.deleteSysRoleMenu(menuIds);
    }


    @Autowired
    SysTerminalCache sysTerminalCache;

    public void addSysTerminal(SysTerminal sysTerminal) {
        sysTerminalCache.addSysTerminal(sysTerminal);
    }

    public SysTerminal getSysTerminal(String id) {
        return sysTerminalCache.getSysTerminal(id);
    }

    public List<SysTerminal> getSysTerminals(int status, int pageIndex, int pageSize, String title) {
        return sysTerminalCache.getSysTerminals(status, pageIndex, pageSize, title, RetrieveStatusEnum.NORMAL.getKey());
    }

    public List<SysTerminal> getSysTerminalAll() {
        return sysTerminalCache.getSysTerminalAll();
    }

    public int getSysTerminalCount(int status, String title) {
        return sysTerminalCache.getSysTerminalCount(status, title, RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeSysTerminal(SysTerminal sysTerminal) {
        sysTerminalCache.changeSysTerminal(sysTerminal);
    }

    public List<SysTerminal> getSysUserTerminalListByUserId(String id) {
        return sysTerminalCache.getSysUserTerminalListByUserId(id);
    }

    public String getSysTerminalTitleByList(List<String> terminalIds) {
        return sysTerminalCache.getSysTerminalTitleByList(terminalIds);
    }

    public SysTerminal findSysUserByTerminalName(String terminalName) {
        return sysTerminalCache.findSysUserByTerminalName(terminalName);
    }

    public void batchDeleteSysTerminal(List<String> ids) {
        sysTerminalCache.batchDeleteSysTerminal(ids);
    }

    @Autowired
    SysUserAddressCache sysUserAddressCache;

    public void addSysUserAddress(SysUserAddress sysUserAddress) {
        sysUserAddressCache.addSysUserAddress(sysUserAddress);
    }

    public SysUserAddress getSysUserAddress(String id) {
        return sysUserAddressCache.getSysUserAddress(id);
    }

    public List<SysUserAddress> getSysUserAddresss(int status, int pageIndex, int pageSize, String name) {
        return sysUserAddressCache.getSysUserAddresss(status, pageIndex, pageSize, name);
    }

    public List<SysUserAddress> getSysUserAddressAll() {
        return sysUserAddressCache.getSysUserAddressAll();
    }

    public int getSysUserAddressCount(int status, String name) {
        return sysUserAddressCache.getSysUserAddressCount(status, name);
    }

    public void changeSysUserAddress(SysUserAddress sysUserAddress) {
        sysUserAddressCache.changeSysUserAddress(sysUserAddress);
    }

    public List<SysUserAddress> getMyAddress(String userId) {
        return sysUserAddressCache.getMyAddress(userId);
    }

    public void changeSysUserDefaultAddressById(String id, String userId) {
        sysUserAddressCache.changeSysUserDefaultAddressById(id, userId);
    }

    public void deleteSysUserDefaultAddressById(String id) {
        sysUserAddressCache.deleteSysUserDefaultAddressById(id);
    }

    public void changeSysUserDefaultAddress(String id) {
        sysUserAddressCache.changeSysUserDefaultAddress(id);
    }

    public void clearSysUserDefaultAddress(String userId) {
        sysUserAddressCache.clearSysUserDefaultAddress(userId);
    }

    public SysUserAddress getSysUserAddressByUserId(String userId) {
        return sysUserAddressCache.getSysUserAddressByUserId(userId);
    }
    public List<GoodShopCart> getGoodShopCartByDefault(Map<String,Object> map){
        return sysUserAddressCache.getGoodShopCartByDefault(map);
    }

    public SysUserAddress getDefaultSysUserAddress(String userId) {
        return sysUserAddressCache.getDefaultSysUserAddress(userId);
    }

    @Autowired
    SysUserChannelCache sysUserChannelCache;

    public void addSysUserChannel(SysUserChannel sysUserChannel) {
        sysUserChannelCache.addSysUserChannel(sysUserChannel);
    }

    public SysUserChannel getSysUserChannel(String id) {
        return sysUserChannelCache.getSysUserChannel(id);
    }

    public List<SysUserChannel> getSysUserChannels(int status, int pageIndex, int pageSize, String name) {
        return sysUserChannelCache.getSysUserChannels(status, pageIndex, pageSize, name);
    }

    public List<SysUserChannel> getSysUserChannelAll() {
        return sysUserChannelCache.getSysUserChannelAll();
    }

    public int getSysUserChannelCount(int status, String name) {
        return sysUserChannelCache.getSysUserChannelCount(status, name);
    }

    public void changeSysUserChannel(SysUserChannel sysUserChannel) {
        sysUserChannelCache.changeSysUserChannel(sysUserChannel);
    }

    public SysUserChannel getSysUserChannelByName(String fromChannel) {
        return sysUserChannelCache.getSysUserChannelByName(fromChannel);
    }

    @Autowired
    SysUserCache sysUserCache;

    public void addSysUser(SysUser sysUser) {
        sysUserCache.addSysUser(sysUser);
    }

    public void addSysUserForPos(SysUser sysUser) {
        sysUserCache.addSysUserForPos(sysUser);
    }

    public int updateSysUserByPosByRemote(SysUser sysUser) {
        return sysUserCache.updateSysUserByPosByRemote(sysUser);
    }


    public SysUser getSysUser(String id) {
        return sysUserCache.getSysUser(id);
    }

    public List<SysUser> getSysUsers(int status, int pageIndex, int pageSize, String name) {
        return sysUserCache.getSysUsers(status, pageIndex, pageSize, name);
    }

    public List<SysUser> getDefaultNameOrPhone(String name) {
        return sysUserCache.getDefaultNameOrPhone(name);
    }

    public void changeLogin(SysUser sysUser) {
        sysUserCache.changeLogin(sysUser);
    }

    public List<SysUser> getSysUserAll() {
        return sysUserCache.getSysUserAll();
    }

    public int getSysUserCount(int status, String name) {
        return sysUserCache.getSysUserCount(status, name);
    }

    public List<SysUser> getSysUserByName(String name) {
        return sysUserCache.getSysUserByName(name);
    }

    public void changeSysUser(SysUser sysUser) {
        sysUserCache.changeSysUser(sysUser);
    }

    public SysUser getSysUserByPhoneNumber(String phoneNumber) {
        return sysUserCache.getSysUserByPhoneNumber(phoneNumber);
    }

    public void updatePasswordByPhoneNum(String phoneNum, String password) {
        sysUserCache.updatePasswordByPhoneNum(phoneNum, password);
    }

    public void updatePasswordById(String id, String password) {
        sysUserCache.updatePasswordById(id, password);
    }

    public void changeSysUserPhoneNum(String id, String phoneNum) {
        sysUserCache.changeSysUserPhoneNum(id, phoneNum);
    }

    public List<SysUser> getSysUserInIds(List<String> ids){
        return sysUserCache.getSysUserInIds(ids);
    }

    public List<PurchaseUserVo> getPurchaseUserList(String searchKey, List<String> roleIds , int pageStartIndex, int pageSize) {
		return sysUserCache.getPurchaseUserList(searchKey, roleIds, pageStartIndex, pageSize);
	}

    public int getPurchaseUserListCount(String searchKey, List<String> roleIds) {
    	return sysUserCache.getPurchaseUserListCount(searchKey, roleIds);
    }

    public List<SysUser> getSysUsersForSearch(String column, Object value) {
		return sysUserCache.getSysUsersForSearch(column, value);
	}

    public List<SysUser> getSysUserByParam(Map<String, Object> map, Integer pageStartIndex, Integer pageSize) {
        return sysUserCache.getSysUserByParam(map, pageStartIndex,pageSize);
    }

    public int getCountSysUserByParam(Map<String, Object> map) {
        return sysUserCache.getCountSysUserByParam(map);
    }

    public void delSysUserByUserId(String id) {
        sysUserCache.delSysUserByUserId(id);
    }

    public void updateSysUserParam(SysUser sysUser) {
        sysUserCache.updateSysUserParam(sysUser);
    }

    public SysUser findSysUserByPhoneOrDefaultName(String phoneNumber, String defaultName) {
        return  sysUserCache.findSysUserByPhoneOrDefaultName(phoneNumber,defaultName);
    }
    public List<SysUser> getSysUserAccountOrName(String accountOrName) {
        return sysUserCache.getSysUserAccountOrName(accountOrName);
    }

    public List<SysUser> getAllUserByRoleIds(String name, int status, List<String> roleIds) {
        return sysUserCache.getAllUserByRoleIds(name, status, roleIds);
    }

    public List<SysUser> getSysUserByPhoneNumLike(String phoneNum) {
        return sysUserCache.getSysUserByPhoneNumLike(phoneNum);
    }


    @Autowired
    SysUserPartnerApplicationCache sysUserPartnerApplicationCache;

    public void addSysUserPartnerApplication(SysUserPartnerApplication sysUserPartnerApplication) {
        sysUserPartnerApplicationCache.addSysUserPartnerApplication(sysUserPartnerApplication);
    }

    public SysUserPartnerApplication getSysUserPartnerApplication(String id) {
        return sysUserPartnerApplicationCache.getSysUserPartnerApplication(id);
    }

    public SysUserPartnerApplication getSysUserPartnerApplicationAppKey(String appKey) {
        return sysUserPartnerApplicationCache.getSysUserPartnerApplicationAppKey(appKey);
    }

    public List<SysUserPartnerApplication> getSysUserPartnerApplications(int status, int pageIndex, int pageSize, String name) {
        return sysUserPartnerApplicationCache.getSysUserPartnerApplications(status, pageIndex, pageSize, name);
    }

    public List<SysUserPartnerApplication> getSysUserPartnerApplicationAll() {
        return sysUserPartnerApplicationCache.getSysUserPartnerApplicationAll();
    }

    public int getSysUserPartnerApplicationCount(int status, String name) {
        return sysUserPartnerApplicationCache.getSysUserPartnerApplicationCount(status, name);
    }

    public void changeSysUserPartnerApplication(SysUserPartnerApplication sysUserPartnerApplication) {
        sysUserPartnerApplicationCache.changeSysUserPartnerApplication(sysUserPartnerApplication);
    }

    @Autowired
    SysUserPartnerCache sysUserPartnerCache;

    public void addSysUserPartner(SysUserPartner sysUserPartner) {
        sysUserPartnerCache.addSysUserPartner(sysUserPartner);
    }

    public SysUserPartner getSysUserPartner(String id) {
        return sysUserPartnerCache.getSysUserPartner(id);
    }

    public List<SysUserPartner> getSysUserPartners(int status, int pageIndex, int pageSize, String name) {
        return sysUserPartnerCache.getSysUserPartners(status, pageIndex, pageSize, name);
    }

    public List<SysUserPartner> getSysUserPartnerAll() {
        return sysUserPartnerCache.getSysUserPartnerAll();
    }

    public int getSysUserPartnerCount(int status, String name) {
        return sysUserPartnerCache.getSysUserPartnerCount(status, name);
    }

    public void changeSysUserPartner(SysUserPartner sysUserPartner) {
        sysUserPartnerCache.changeSysUserPartner(sysUserPartner);
    }

    @Autowired
    SysUserPropertiesCache sysUserPropertiesCache;

    public void addSysUserProperties(SysUserProperties sysUserProperties) {
        sysUserPropertiesCache.addSysUserProperties(sysUserProperties);
    }

    public SysUserProperties getSysUserProperties(String id) {
        return sysUserPropertiesCache.getSysUserProperties(id);
    }

    public List<SysUserProperties> getSysUserPropertiess(int status, int pageIndex, int pageSize, String name) {
        return sysUserPropertiesCache.getSysUserPropertiess(status, pageIndex, pageSize, name);
    }

    public List<SysUserProperties> getSysUserPropertiesAll() {
        return sysUserPropertiesCache.getSysUserPropertiesAll();
    }

    public int getSysUserPropertiesCount(int status, String name) {
        return sysUserPropertiesCache.getSysUserPropertiesCount(status, name);
    }

    public void changeSysUserProperties(SysUserProperties sysUserProperties) {
        sysUserPropertiesCache.changeSysUserProperties(sysUserProperties);
    }

    @Autowired
    SysUserRoleCache sysUserRoleCache;

    public void addSysUserRole(SysUserRole sysUserRole) {
        sysUserRoleCache.addSysUserRole(sysUserRole);
    }

    public SysUserRole getSysUserRole(String id) {
        return sysUserRoleCache.getSysUserRole(id);
    }

    public List<SysUserRole> getSysUserRoles(int status, int pageIndex, int pageSize, String name) {
        return sysUserRoleCache.getSysUserRoles(status, pageIndex, pageSize, name);
    }

    public List<SysUserRole> getSysUserRoleAll() {
        return sysUserRoleCache.getSysUserRoleAll();
    }

    public int getSysUserRoleCount(int status, String name) {
        return sysUserRoleCache.getSysUserRoleCount(status, name);
    }

    public void changeSysUserRole(SysUserRole sysUserRole) {
        sysUserRoleCache.changeSysUserRole(sysUserRole);
    }

    public List<SysUserRole> getSysUserRolesByRole(String roleId) {
        return sysUserRoleCache.getSysUserRolesByRole(roleId);
    }

    public SysUserRole getSysUserRoleByRoleAndUser(String roleId, String userId) {
		return sysUserRoleCache.getSysUserRoleByRoleAndUser(roleId, userId);
	}

    public int deleteSysUserRole(String roleId, String userId) {
    	return sysUserRoleCache.deleteSysUserRole(roleId, userId);
	}

    public SysUserRole getSysUserRoleByUser(String userId) {
        return sysUserRoleCache.getSysUserRoleByUser(userId);
    }

    public void updateSysUserRoleByParam(SysUserRole sysUserRole) {
        sysUserRoleCache.updateSysUserRoleByParam(sysUserRole);
    }

    @Autowired
    SysUserSmsLoggerCache sysUserSmsLoggerCache;

    public void addSysUserSmsLogger(SysUserSmsLogger sysUserSmsLogger) {
        sysUserSmsLoggerCache.addSysUserSmsLogger(sysUserSmsLogger);
    }

    public SysUserSmsLogger getSysUserSmsLogger(String id) {
        return sysUserSmsLoggerCache.getSysUserSmsLogger(id);
    }

    public List<SysUserSmsLogger> getSysUserSmsLoggers(int status, int pageIndex, int pageSize, String name) {
        return sysUserSmsLoggerCache.getSysUserSmsLoggers(status, pageIndex, pageSize, name);
    }

    public List<SysUserSmsLogger> getSysUserSmsLoggerAll() {
        return sysUserSmsLoggerCache.getSysUserSmsLoggerAll();
    }

    public int getSysUserSmsLoggerCount(int status, String name) {
        return sysUserSmsLoggerCache.getSysUserSmsLoggerCount(status, name);
    }

    public void changeSysUserSmsLogger(SysUserSmsLogger sysUserSmsLogger) {
        sysUserSmsLoggerCache.changeSysUserSmsLogger(sysUserSmsLogger);
    }

    public SysUserSmsLogger getSysUserSmsLoggerByPhone(String phone, int type) {
        return sysUserSmsLoggerCache.getSysUserSmsLoggerByPhone(phone, type);
    }

    @Autowired
    SysUserTerminalCache sysUserTerminalCache;
    public void addSysUserTerminal(SysUserTerminal sysUserTerminal) {
        sysUserTerminalCache.addSysUserTerminal(sysUserTerminal);
    }
    public void addSysUserTerminals(List<SysUserTerminal> list) {
        sysUserTerminalCache.addSysUserTerminals(list);
    }
    public void delSysUserTerminal(String userId) {
        sysUserTerminalCache.delSysUserTerminal(userId);
    }

    public SysUserTerminal getSysUserTerminal(String id) {
        return sysUserTerminalCache.getSysUserTerminal(id);
    }

    public List<SysUserTerminal> getSysUserTerminals(int status, int pageIndex, int pageSize, String name) {
        return sysUserTerminalCache.getSysUserTerminals(status, pageIndex, pageSize, name);
    }

    public List<SysUserTerminal> getSysUserTerminalAll() {
        return sysUserTerminalCache.getSysUserTerminalAll();
    }

    public int getSysUserTerminalCount(int status, String name) {
        return sysUserTerminalCache.getSysUserTerminalCount(status, name);
    }

    public void changeSysUserTerminal(SysUserTerminal sysUserTerminal) {
        sysUserTerminalCache.changeSysUserTerminal(sysUserTerminal);
    }

    public SysUserTerminal getSysUserTerminal2(String id, String userId) {
        return sysUserTerminalCache.getSysUserTerminal2(id, userId);
    }

    @Autowired
    SysVersionCache sysVersionCache;

    public void addSysVersion(SysVersion sysVersion) {
        sysVersionCache.addSysVersion(sysVersion);
    }

    public SysVersion getSysVersion(String id) {
        return sysVersionCache.getSysVersion(id);
    }

    public List<SysVersion> getSysVersions(int status, int pageIndex, int pageSize, String name) {
        return sysVersionCache.getSysVersions(status, pageIndex, pageSize, name);
    }

    public List<SysVersion> getSysVersionAll() {
        return sysVersionCache.getSysVersionAll();
    }

    public int getSysVersionCount(int status, String name) {
        return sysVersionCache.getSysVersionCount(status, name);
    }

    public void changeSysVersion(SysVersion sysVersion) {
        sysVersionCache.changeSysVersion(sysVersion);
    }

    public SysVersion getUsefulVersion(Integer deviceType, Integer clientType, Integer versionIndex) {
        return sysVersionCache.getUsefulVersion(deviceType, clientType, versionIndex);
    }


    @Autowired
    SysVersionLocalCache sysVersionLocalCache;

    public void addSysVersionLocal(SysVersionLocal sysVersionLocal) {
        sysVersionLocalCache.addSysVersionLocal(sysVersionLocal);
    }

    public void addSysVersionLocalBatch(List<SysVersionLocal> sysVersionLocal) {
        sysVersionLocalCache.addSysVersionLocalBatch(sysVersionLocal);
    }

    public SysVersionLocal getSysVersionLocal(String id) {
        return sysVersionLocalCache.getSysVersionLocal(id);
    }

    public List<SysVersionLocal> getSysVersionLocals(int status, int pageIndex, int pageSize, String name) {
        return sysVersionLocalCache.getSysVersionLocals(status, pageIndex, pageSize, name);
    }

    public List<SysVersionLocal> getSysVersionLocalAll() {
        return sysVersionLocalCache.getSysVersionLocalAll();
    }

    public int getSysVersionLocalCount(int status, String name) {
        return sysVersionLocalCache.getSysVersionLocalCount(status, name);
    }

    public void changeSysVersionLocal(SysVersionLocal sysVersionLocal) {
        sysVersionLocalCache.changeSysVersionLocal(sysVersionLocal);
    }

    public SysVersionLocal getSysVersionLocalByUserId(String userId) {
        return sysVersionLocalCache.getSysVersionLocalByUserId(userId);
    }

    @Autowired
    SysProjectCache sysProjectCache;

    public void addSysProject(SysProject sysProject) {
        sysProjectCache.addSysProject(sysProject);
    }

    public SysProject getSysProject(String id) {
        return sysProjectCache.getSysProject(id);
    }

    public List<SysProject> getSysProjects(int pageIndex, int pageSize, String name) {
        return sysProjectCache.getSysProjects(pageIndex, pageSize, name);
    }

    /**
     * 获取项目父类
     *
     * @param parentId
     * @return
     */
    public List<SysProject> getSysProjectByParentId(String parentId) {
        List<SysProject> sysProjects = sysProjectCache.getSysProjectByParentId(parentId);
        return sysProjects;
    }

    public JSONArray getSysProjectTree() {
        return sysProjectCache.getSysProjectTree();
    }

    public List<SysProject> getSysProjectAll() {
        return sysProjectCache.getSysProjectAll();
    }

    public int getSysProjectCount(String name) {
        return sysProjectCache.getSysProjectCount(name);
    }

    public void changeSysProject(SysProject sysProject) {
        sysProjectCache.changeSysProject(sysProject);
    }

    @Autowired
    SysInterfaceParamCache sysInterfaceParamCache;

    public List<SysInterfaceParam> getSysInterfaceParamByInterfaceId(String interfaceId) {
        return sysInterfaceParamCache.getSysInterfaceParamByInterfaceId(interfaceId);
    }

    public void addSysInterfaceParam(List<SysInterfaceParam> sysInterfaceParam) {
        sysInterfaceParamCache.addSysInterfaceParam(sysInterfaceParam);
    }

    public SysInterfaceParam getSysInterfaceParam(String id) {
        return sysInterfaceParamCache.getSysInterfaceParam(id);
    }

    public List<SysInterfaceParam> getSysInterfaceParams(int pageIndex, int pageSize, String name) {
        return sysInterfaceParamCache.getSysInterfaceParams(pageIndex, pageSize, name);
    }

    public List<SysInterfaceParam> getSysInterfaceParamAll() {
        return sysInterfaceParamCache.getSysInterfaceParamAll();
    }

    public int getSysInterfaceParamCount(String name) {
        return sysInterfaceParamCache.getSysInterfaceParamCount(name);
    }

    public void delSysInterfaceParam(String interfaceId) {
        sysInterfaceParamCache.delSysInterfaceParam(interfaceId);
    }

    public void changeSysInterfaceParam(SysInterfaceParam sysInterfaceParam) {
        sysInterfaceParamCache.changeSysInterfaceParam(sysInterfaceParam);
    }

    @Autowired
    SysInterfaceCache sysInterfaceCache;

    public void addSysInterface(SysInterface sysInterface) {
        sysInterfaceCache.addSysInterface(sysInterface);
    }

    public List<SysInterface> getSysInterfaceByProjectId(String projectId) {
        return sysInterfaceCache.getSysInterfaceByProjectId(projectId);
    }

    public SysInterface getSysInterface(String id) {
        return sysInterfaceCache.getSysInterface(id);
    }

    public List<SysInterface> getSysInterfaces(int status, int pageIndex, int pageSize, String name) {
        return sysInterfaceCache.getSysInterfaces(status, pageIndex, pageSize, name);
    }

    public List<SysInterface> getSysInterfaceAll() {
        return sysInterfaceCache.getSysInterfaceAll();
    }

    public int getSysInterfaceCount(int status, String name) {
        return sysInterfaceCache.getSysInterfaceCount(status, name, RetrieveStatusEnum.NORMAL.getKey());
    }

    public JSONArray getSysInterfaceTree() {
        return sysInterfaceCache.getSysInterfaceTree();
    }

    public void changeSysInterface(SysInterface sysInterface) {
        sysInterfaceCache.changeSysInterface(sysInterface);
    }

    @Autowired
    SysMenuPosCache sysMenuPosCache;

    public void addSysMenuPos(SysMenuPos sysMenuPos) {
        sysMenuPosCache.addSysMenuPos(sysMenuPos);
    }

    public void updateSysMenuPosByIds(List<String> menuIds, int status) {
        sysMenuPosCache.updateSysMenuPosByIds(menuIds, status);
    }

    public   SysMenuPos getSysMenuPos(String id) {
        return sysMenuPosCache.getSysMenuPos(id);
    }

    public List<SysMenuPos> getSysMenuPoss(int status,int pageIndex, int pageSize, String name) {
        return sysMenuPosCache.getSysMenuPoss(status,pageIndex, pageSize, name,RetrieveStatusEnum.NORMAL.getKey());
    }

    public List<SysMenuPos> getSysMenuPosAll() {
        return sysMenuPosCache.getSysMenuPosAll();
    }

    public int getSysMenuPosCount(int status, String name) {
        return sysMenuPosCache.getSysMenuPosCount(status, name,RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeSysMenuPos(SysMenuPos sysMenuPos) {
        sysMenuPosCache.changeSysMenuPos(sysMenuPos);
    }


    public List<SysMenuPos> getSysMenuPosByIds(List<String> menuIds) {
        return sysMenuPosCache.getSysMenuPosByIds(menuIds);
    }

    public JSONArray getSysMenuPosByRoleId(String roleId) {
        return sysMenuPosCache.getSysMenuPosByRoleId(roleId);
    }

    public JSONArray getSysMenuPosInRoleId(List<String> roleIds) {
        return sysMenuPosCache.getSysMenuPosInRoleId(roleIds);
    }

    @Autowired
    SysRoleMenuPosCache sysRoleMenuPosCache;

    public void addSysRoleMenuPos(SysRoleMenuPos sysRoleMenuPos) {
        sysRoleMenuPosCache.addSysRoleMenuPos(sysRoleMenuPos);
    }

    public   SysRoleMenuPos getSysRoleMenuPos(String id) {
        return sysRoleMenuPosCache.getSysRoleMenuPos(id);
    }

    public List<SysRoleMenuPos> getSysRoleMenuPoss(int status,int pageIndex, int pageSize, String name) {
        return sysRoleMenuPosCache.getSysRoleMenuPoss(status,pageIndex, pageSize, name,RetrieveStatusEnum.NORMAL.getKey());
    }

    public List<SysRoleMenuPos> getSysRoleMenuPosAll() {
        return sysRoleMenuPosCache.getSysRoleMenuPosAll();
    }

    public int getSysRoleMenuPosCount(int status, String name) {
        return sysRoleMenuPosCache.getSysRoleMenuPosCount(status, name,RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeSysRoleMenuPos(SysRoleMenuPos sysRoleMenuPos) {
        sysRoleMenuPosCache.changeSysRoleMenuPos(sysRoleMenuPos);
    }

    public void batchAddSysRoleMenuPos(List<SysRoleMenuPos> list) {
        sysRoleMenuPosCache.batchAddSysRoleMenuPos(list);
    }

    public void delSysRoleMenuPosByRoleId(String roleId) {
        sysRoleMenuPosCache.delSysRoleMenuPosByRoleId(roleId);
    }

    public List<SysRoleMenuPos> getSysRoleMenuPosByRoleId(String roleId) {
        return sysRoleMenuPosCache.getSysRoleMenuPosByRoleId(roleId);
    }

    @Autowired
    SysRolePosCache sysRolePosCache;

    public void addSysRolePos(SysRolePos sysRolePos) {
        sysRolePosCache.addSysRolePos(sysRolePos);
    }

    public   SysRolePos getSysRolePos(String id) {
        return sysRolePosCache.getSysRolePos(id);
    }

    public List<SysRolePos> getSysRolePoss(int status,int pageIndex, int pageSize, String name) {
        return sysRolePosCache.getSysRolePoss(status,pageIndex, pageSize, name,RetrieveStatusEnum.NORMAL.getKey());
    }

    public List<SysRolePos> getSysRolePosAll() {
        return sysRolePosCache.getSysRolePosAll();
    }

    public int getSysRolePosCount(int status, String name) {
        return sysRolePosCache.getSysRolePosCount(status, name,RetrieveStatusEnum.NORMAL.getKey());
    }

    public Collection<?> getSysRolePosByRoleName(String roleName) {
        return sysRolePosCache.getSysRolePosByRoleName(roleName);
    }

    public void changeSysRolePos(SysRolePos sysRolePos) {
        sysRolePosCache.changeSysRolePos(sysRolePos);
    }

    @Autowired
    SysUserRolePosCache sysUserRolePosCache;

    public void addSysUserRolePos(SysUserRolePos sysUserRolePos) {
        sysUserRolePosCache.addSysUserRolePos(sysUserRolePos);
    }

    public   SysUserRolePos getSysUserRolePos(String id) {
        return sysUserRolePosCache.getSysUserRolePos(id);
    }

    public List<SysUserRolePos> getSysUserRolePoss(int status,int pageIndex, int pageSize, String name) {
        return sysUserRolePosCache.getSysUserRolePoss(status,pageIndex, pageSize, name,RetrieveStatusEnum.NORMAL.getKey());
    }

    public List<SysUserRolePos> getSysUserRolePosAll() {
        return sysUserRolePosCache.getSysUserRolePosAll();
    }

    public int getSysUserRolePosCount(int status, String name) {
        return sysUserRolePosCache.getSysUserRolePosCount(status, name,RetrieveStatusEnum.NORMAL.getKey());
    }

    public void changeSysUserRolePos(SysUserRolePos sysUserRolePos) {
        sysUserRolePosCache.changeSysUserRolePos(sysUserRolePos);
    }

    public List<SysUserRolePos> getSysUserRolePosByUserId(String userId) {
        return sysUserRolePosCache.getSysUserRolePosByUserId(userId);
    }

    public void updateSysRolePosByIds(List<String> ids, int status) {
        sysUserRolePosCache.updateSysRolePosByIds(ids, status);
    }
}
