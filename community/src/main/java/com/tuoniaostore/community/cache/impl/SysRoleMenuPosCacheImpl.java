package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysRoleMenuPosCache;
import com.tuoniaostore.community.data.SysRoleMenuPosMapper;
import com.tuoniaostore.datamodel.user.SysRoleMenuPos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色与菜单对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
@Component
@CacheConfig(cacheNames = "sysRoleMenuPos")
public class SysRoleMenuPosCacheImpl implements SysRoleMenuPosCache {

    @Autowired
    SysRoleMenuPosMapper mapper;

    @Override
    @CacheEvict(value = "sysRoleMenuPos", allEntries = true)
    public void addSysRoleMenuPos(SysRoleMenuPos sysRoleMenuPos) {
        mapper.addSysRoleMenuPos(sysRoleMenuPos);
    }

    @Override
    @Cacheable(value = "sysRoleMenuPos", key = "'getSysRoleMenuPos_'+#id")
    public SysRoleMenuPos getSysRoleMenuPos(String id) {
        return mapper.getSysRoleMenuPos(id);
    }

    @Override
    @Cacheable(value = "sysRoleMenuPos", key = "'getSysRoleMenuPoss_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name+'_'+#retrieveStatus")
    public List<SysRoleMenuPos> getSysRoleMenuPoss(int status, int pageIndex, int pageSize, String name, int retrieveStatus) {
        return mapper.getSysRoleMenuPoss(pageIndex, pageSize, status, name, retrieveStatus);
    }

    @Override
    @Cacheable(value = "sysRoleMenuPos", key = "'getSysRoleMenuPosAll'")
    public List<SysRoleMenuPos> getSysRoleMenuPosAll() {
        return mapper.getSysRoleMenuPosAll();
    }

    @Override
    @Cacheable(value = "sysRoleMenuPos", key = "'getSysRoleMenuPosCount_'+#status+'_'+#name+'_'+#retrieveStatus")
    public int getSysRoleMenuPosCount(int status, String name, int retrieveStatus) {
        return mapper.getSysRoleMenuPosCount(status, name, retrieveStatus);
    }

    @Override
    @CacheEvict(value = "sysRoleMenuPos", allEntries = true)
    public void changeSysRoleMenuPos(SysRoleMenuPos sysRoleMenuPos) {
        mapper.changeSysRoleMenuPos(sysRoleMenuPos);
    }

    @Override
    @CacheEvict(value = "sysRoleMenuPos", allEntries = true)
    public void batchAddSysRoleMenuPos(List<SysRoleMenuPos> list) {
        mapper.batchAddSysRoleMenuPos(list);
    }

    @Override
    @Cacheable(value = "sysRoleMenuPos", key = "'getSysRoleMenuPosByRoleId_'+#roleId")
    public List<SysRoleMenuPos> getSysRoleMenuPosByRoleId(String roleId) {
        return mapper.getSysRoleMenuPosByRoleId(roleId);
    }

    @Override
    @Cacheable(value = "sysRoleMenuPos", key = "'getSysRoleMenusPosInRoleId_'+#roleIds")
    public List<SysRoleMenuPos> getSysRoleMenusPosInRoleId(List<String> roleIds) {
        return mapper.getSysRoleMenusPosInRoleId(roleIds);
    }

    @Override
    public void delSysRoleMenuPosByRoleId(String roleId) {
        mapper.delSysRoleMenuPosByRoleId(roleId);
    }

}
