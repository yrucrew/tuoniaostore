package com.tuoniaostore.community.cache;

import com.tuoniaostore.datamodel.user.SysInterfaceParam;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
public interface SysInterfaceParamCache {

    void addSysInterfaceParam(List<SysInterfaceParam> sysInterfaceParam);

    SysInterfaceParam getSysInterfaceParam(String id);

    List<SysInterfaceParam> getSysInterfaceParams(int pageIndex, int pageSize, String name);

    List<SysInterfaceParam> getSysInterfaceParamAll();

    int getSysInterfaceParamCount( String name);

    void changeSysInterfaceParam(SysInterfaceParam sysInterfaceParam);

    void delSysInterfaceParam(String interfaceId);

    List<SysInterfaceParam> getSysInterfaceParamByInterfaceId(String interfaceId);

}
