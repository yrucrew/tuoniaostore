package com.tuoniaostore.community.cache;
import com.alibaba.fastjson.JSONArray;
import com.tuoniaostore.datamodel.user.SysInterface;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
public interface SysInterfaceCache {

    void addSysInterface(SysInterface sysInterface);

    SysInterface getSysInterface(String id);

    List<SysInterface> getSysInterfaces(int status, int pageIndex, int pageSize, String name);
    List<SysInterface> getSysInterfaceAll();
    JSONArray getSysInterfaceTree();
    int getSysInterfaceCount(int status, String name, int retrieveStatus);
    void changeSysInterface(SysInterface sysInterface);
    List<SysInterface> getSysInterfaceByProjectId(String projectId);

}
