package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysLog;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysLogCache {

    void addSysLog(SysLog sysLog);

    SysLog getSysLog(String id);

    List<SysLog> getSysLogs(int pageIndex, int pageSize, int status, String name);
    List<SysLog> getSysLogAll();
    int getSysLogCount(int status, String name);
    void changeSysLog(SysLog sysLog);

}
