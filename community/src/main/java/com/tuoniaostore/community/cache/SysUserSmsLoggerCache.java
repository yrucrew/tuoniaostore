package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysUserSmsLogger;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysUserSmsLoggerCache {

    void addSysUserSmsLogger(SysUserSmsLogger sysUserSmsLogger);

    SysUserSmsLogger getSysUserSmsLogger(String id);

    List<SysUserSmsLogger> getSysUserSmsLoggers(int pageIndex, int pageSize, int status, String name);
    List<SysUserSmsLogger> getSysUserSmsLoggerAll();
    int getSysUserSmsLoggerCount(int status, String name);
    void changeSysUserSmsLogger(SysUserSmsLogger sysUserSmsLogger);

    /**
     * 通过手机号码 获取验证码
     * @param phone
     */
    SysUserSmsLogger getSysUserSmsLoggerByPhone(String phone,int type);
}
