package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysRolePos;

import java.util.Collection;
import java.util.List;
/**
 * 角色
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
public interface SysRolePosCache {

    void addSysRolePos(SysRolePos sysRolePos);

    SysRolePos getSysRolePos(String id);

    List<SysRolePos> getSysRolePoss(int status, int pageIndex, int pageSize, String name, int retrieveStatus);
    List<SysRolePos> getSysRolePosAll();
    int getSysRolePosCount(int status, String name, int retrieveStatus);
    void changeSysRolePos(SysRolePos sysRolePos);

    List<SysRolePos> getSysRolePosByRoleName(String roleName);
}
