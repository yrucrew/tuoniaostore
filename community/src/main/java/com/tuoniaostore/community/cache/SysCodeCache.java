package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysCode;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysCodeCache {

    void addSysCode(SysCode sysCode);

    SysCode getSysCode(String id);
    SysCode getSysCodeName(  String codeName);
    List<SysCode> getSysCodes(int pageIndex, int pageSize, int status, String name);
    List<SysCode> getSysCodeAll();
    int getSysCodeCount(int status, String name);
    void changeSysCode(SysCode sysCode);

}
