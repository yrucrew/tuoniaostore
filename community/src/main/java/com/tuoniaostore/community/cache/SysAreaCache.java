package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysArea;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
public interface SysAreaCache {

    void addSysArea(SysArea sysArea);

    SysArea getSysArea(String id);

    List<SysArea> getSysAreas(int pageIndex, int pageSize, String title, int status);
    List<SysArea> getSysAreaAll();

    int getSysAreaCount(String title);

    void changeSysArea(SysArea sysArea);


    List<SysArea> getSysAreasByPage(String areaName,Integer pageStartIndex, Integer pageSize);

    int getSysAreasByPageCount(String areaName);

    List<SysArea> getSysAreasByIds(List<String> ids, int pageStartIndex, int pageSize);

    List<SysArea> getSysAreasByNameLike(String areaName, Integer pageStartIndex, Integer pageSize);

    SysArea getSysAreasByTitle(String title);

    void deleteSysAreas(List<String> ids);

}
