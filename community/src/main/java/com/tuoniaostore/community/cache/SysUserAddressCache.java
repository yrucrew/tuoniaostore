package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.good.GoodShopCart;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.user.SysUserAddress;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysUserAddressCache {

    void addSysUserAddress(SysUserAddress sysUserAddress);

    SysUserAddress getSysUserAddress(String id);

    List<SysUserAddress> getSysUserAddresss(int pageIndex, int pageSize, int status, String name);
    List<SysUserAddress> getSysUserAddressAll();
    int getSysUserAddressCount(int status, String name);
    void changeSysUserAddress(SysUserAddress sysUserAddress);

    /**
     * 查找当前用户的所有地址信息
     * @param userId
     * @return
     */
    List<SysUserAddress> getMyAddress(String userId);

    /**
     * 修改默认用户地址
     * @param id
     * @param userId
     */
    void changeSysUserDefaultAddressById(String id, String userId);

    /**
     * 删除用户地址
     * @param id
     */
    void deleteSysUserDefaultAddressById(String id);

    /**
     * 设置默认地址
     * @param id
     */
    void changeSysUserDefaultAddress(String id);

    /**
     * 清除默认地址
     * @param userId
     */
    void clearSysUserDefaultAddress(String userId);

    /**
     * 通过用户 查找最近的一个地址
     * @param userId
     */
    SysUserAddress getSysUserAddressByUserId(String userId);

    /**
     * 查询用户的默认地址
     * @author sqd
     * @date 2019/4/9
     * @param
     * @return
     */
    List<GoodShopCart> getGoodShopCartByDefault(Map<String,Object> map);

    /**
     * 通过用户id 查找默认的地址
     * @author oy
     * @date 2019/4/12
     * @param userId
     * @return com.tuoniaostore.datamodel.user.SysUserAddress
     */
    SysUserAddress getDefaultSysUserAddress(String userId);
}
