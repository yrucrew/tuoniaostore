package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysRole;

import java.util.List;
/**
 * 角色
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysRoleCache {

    void addSysRole(SysRole sysRole);

    SysRole getSysRole(String id);

    List<SysRole> getSysRoles(int pageIndex, int pageSize, int status, String name,int retrieveStatus);
    List<SysRole> getSysRoleAll();
    int getSysRoleCount(int status,   String roleName, int retrieveStatus);
    void changeSysRole(SysRole sysRole);

	SysRole getSysRoleByRoleName(String value);
    void changeSysRoleRetrieve(String[] id, int retrieveStatus);

    void deleteSysRole(List<String> idList);

    SysRole findSysRoleByRoleName(String roleName);
}
