package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysInterfaceParamCache;
import com.tuoniaostore.community.data.SysInterfaceParamMapper;
import com.tuoniaostore.datamodel.user.SysInterfaceParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
@Component
@CacheConfig(cacheNames = "sysInterfaceParam")
public class SysInterfaceParamCacheImpl implements SysInterfaceParamCache {

    @Autowired
    SysInterfaceParamMapper mapper;

    @Override
    @CacheEvict(value = "sysInterfaceParam", allEntries = true)
    public void delSysInterfaceParam(String interfaceId) {
        mapper.delSysInterfaceParam(interfaceId);
    }

    @Override
    @Cacheable(value = "sysInterfaceParam", key = "'getSysInterfaceParamByInterfaceId_'+#interfaceId")
    public List<SysInterfaceParam> getSysInterfaceParamByInterfaceId(String interfaceId) {
        return mapper.getSysInterfaceParamByInterfaceId(interfaceId);
    }

    @Override
    @CacheEvict(value = "sysInterfaceParam", allEntries = true)
    public void addSysInterfaceParam(List<SysInterfaceParam> sysInterfaceParam) {
        mapper.addSysInterfaceParam(sysInterfaceParam);
    }

    @Override
    @Cacheable(value = "sysInterfaceParam", key = "'getSysInterfaceParam_'+#id")
    public SysInterfaceParam getSysInterfaceParam(String id) {
        return mapper.getSysInterfaceParam(id);
    }

    @Override
    @Cacheable(value = "sysInterfaceParam", key = "'getSysInterfaceParams_'+#pageIndex+'_'+#pageSize+'_'+#name")
    public List<SysInterfaceParam> getSysInterfaceParams(int pageIndex, int pageSize, String name) {
        return mapper.getSysInterfaceParams(pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "sysInterfaceParam", key = "'getSysInterfaceParamAll'")
    public List<SysInterfaceParam> getSysInterfaceParamAll() {
        return mapper.getSysInterfaceParamAll();
    }

    @Override
    @Cacheable(value = "sysInterfaceParam", key = "'getSysInterfaceParamCount_'+#name")
    public int getSysInterfaceParamCount(String name) {
        return mapper.getSysInterfaceParamCount(name);
    }

    @Override
    @CacheEvict(value = "sysInterfaceParam", allEntries = true)
    public void changeSysInterfaceParam(SysInterfaceParam sysInterfaceParam) {
        mapper.changeSysInterfaceParam(sysInterfaceParam);
    }

}
