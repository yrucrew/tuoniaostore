package com.tuoniaostore.community.cache.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.community.cache.SysMenuPosCache;
import com.tuoniaostore.community.cache.SysRoleMenuPosCache;
import com.tuoniaostore.community.data.SysMenuPosMapper;
import com.tuoniaostore.datamodel.user.SysMenuPos;
import com.tuoniaostore.datamodel.user.SysRoleMenuPos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 菜单管理
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
@Component
@CacheConfig(cacheNames = "sysMenuPos")
public class SysMenuPosCacheImpl implements SysMenuPosCache {

    @Autowired
    SysMenuPosMapper mapper;

    @Autowired
    SysRoleMenuPosCache sysRoleMenuPosCache;

    @Override
    @CacheEvict(value = "sysMenuPos", allEntries = true)
    public void addSysMenuPos(SysMenuPos sysMenuPos) {
        mapper.addSysMenuPos(sysMenuPos);
    }

    @Override
    @Cacheable(value = "sysMenuPos", key = "'getSysMenuPos_'+#id")
    public SysMenuPos getSysMenuPos(String id) {
        return mapper.getSysMenuPos(id);
    }

    @Override
    @Cacheable(value = "sysMenuPos", key = "'getSysMenuPoss_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name+'_'+#retrieveStatus")
    public List<SysMenuPos> getSysMenuPoss(int status, int pageIndex, int pageSize, String name, int retrieveStatus) {
        return mapper.getSysMenuPoss(pageIndex, pageSize, status, name, retrieveStatus);
    }

    @Override
    @Cacheable(value = "sysMenuPos", key = "'getSysMenuPosAll'")
    public List<SysMenuPos> getSysMenuPosAll() {
        return mapper.getSysMenuPosAll();
    }

    @Override
    @Cacheable(value = "sysMenuPos", key = "'getSysMenuPosCount_'+#status+'_'+#name+'_'+#retrieveStatus")
    public int getSysMenuPosCount(int status, String name, int retrieveStatus) {
        return mapper.getSysMenuPosCount(status, name, retrieveStatus);
    }

    @Override
    @CacheEvict(value = "sysMenuPos", allEntries = true)
    public void changeSysMenuPos(SysMenuPos sysMenuPos) {
        mapper.changeSysMenuPos(sysMenuPos);
    }

    @Override
    @Cacheable(value = "sysMenuPos", key = "'getSysMenuPosByIds_'+#menuIds")
    public List<SysMenuPos> getSysMenuPosByIds(List<String> menuIds) {
        return mapper.getSysMenuPosByIds(menuIds);
    }

    @Override
    @Cacheable(value = "sysMenuPos", key = "'getSysMenuPosByRoleId_'+#roleId")
    public JSONArray getSysMenuPosByRoleId(String roleId) {
        JSONArray jsonArray = null;
        try {
            // 获取角色id对应的角色菜单关系
            List<SysRoleMenuPos> sysRoleMenus = sysRoleMenuPosCache.getSysRoleMenuPosByRoleId(roleId);
            // 生成菜单id列表
            List<String> menuIds = new ArrayList<>();
            sysRoleMenus.stream().forEach(sysRoleMenu -> menuIds.add(sysRoleMenu.getMenuId()));
            if (menuIds.size() != 0) {
                jsonArray = getWebMenus("0", menuIds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    @Override
    @Cacheable(value = "sysMenuPos", key = "'getSysMenuPosInRoleId_'+#roleIds")
    public JSONArray getSysMenuPosInRoleId(List<String> roleIds) {
        JSONArray jsonArray = null;
        try {
            // 获取角色ids对应的角色菜单关系
            List<SysRoleMenuPos> sysRoleMenus = sysRoleMenuPosCache.getSysRoleMenusPosInRoleId(roleIds);

            // 菜单id列表
            if (CollectionUtils.isEmpty(sysRoleMenus)){
                return null;
            }
            List<String> menuIds = sysRoleMenus.stream().map(SysRoleMenuPos::getMenuId).collect(Collectors.toList());
            if (menuIds.size() != 0) {
                jsonArray = getWebMenus("0", menuIds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    @Override
    public void updateSysMenuPosByIds(List<String> menuId, int status) {
        mapper.updateSysMenuPosByIds(menuId, status);
    }

    /**
     * 获取菜单
     *
     * @param parentId 父菜单id
     * @param menuIds  菜单ids
     * @return
     */
    private JSONArray getWebMenus(String parentId, List<String> menuIds) {
        try {
            List<SysMenuPos> list = getSysMenuPosChilds(parentId, menuIds);
            JSONArray jsonArray = new JSONArray();
            if (list != null) {
                int count = list.size();
                for (int k = 0; k < count; k++) {
                    JSONObject json = new JSONObject();
                    json.put("title", list.get(k).getName());
                    json.put("icon", list.get(k).getIcon());
                    json.put("jump", list.get(k).getUrl());
                    json.put("name", list.get(k).getName());
                    String id = list.get(k).getId();
                    JSONArray child = getWebMenus(id, menuIds);
                    if (child != null) {
                        json.put("list", child);
                    }
                    jsonArray.add(json);
                }
            }
            return jsonArray;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Cacheable(value = "sysMenuPos", key = "'getSysMenuChilds_'+#parentId+'_'+#menuIds")
    public List<SysMenuPos> getSysMenuPosChilds(String parentId, List<String> menuIds) {
        return mapper.getSysMenuPosChilds(parentId, menuIds);
    }

}
