package com.tuoniaostore.community.cache.impl;
        import com.tuoniaostore.community.cache.SysUserRoleCache;
        import com.tuoniaostore.community.data.SysUserRoleMapper;
        import com.tuoniaostore.datamodel.user.SysUserRole;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 用户与角色对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysUserRole")
public class SysUserRoleCacheImpl implements SysUserRoleCache {

    @Autowired
    SysUserRoleMapper mapper;

    @Override
    @CacheEvict(value = "sysUserRole", allEntries = true)
    public void addSysUserRole(SysUserRole sysUserRole) {
        mapper.addSysUserRole(sysUserRole);
    }

    @Override
    @Cacheable(value = "sysUserRole", key = "'getSysUserRole_'+#id")
    public   SysUserRole getSysUserRole(String id) {
        return mapper.getSysUserRole(id);
    }

    @Override
    @Cacheable(value = "sysUserRole", key = "'getSysUserRoles_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysUserRole> getSysUserRoles(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getSysUserRoles(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "sysUserRole", key = "'getSysUserRoleAll'")
    public List<SysUserRole> getSysUserRoleAll() {
        return mapper.getSysUserRoleAll();
    }

    @Override
    @Cacheable(value = "sysUserRole", key = "'getSysUserRoleCount_'+#status+'_'+#name")
    public int getSysUserRoleCount(int status, String name) {
        return mapper.getSysUserRoleCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysUserRole", allEntries = true)
    public void changeSysUserRole(SysUserRole sysUserRole) {
        mapper.changeSysUserRole(sysUserRole);
    }

	@Override
	@Cacheable(value = "sysUserRole", key = "'getSysUserRolesByRole_'+#roleId")
	public List<SysUserRole> getSysUserRolesByRole(String roleId) {
		return mapper.getSysUserRolesByRole(roleId);
	}

	@Override
	@Cacheable(value = "sysUserRole", key = "'getSysUserRoleByRoleAndUser_'+#roleId+'_'+#userId")
	public SysUserRole getSysUserRoleByRoleAndUser(String roleId, String userId) {
		return mapper.getSysUserRoleByRoleAndUser(roleId, userId);
	}

	@Override
	@CacheEvict(value = "sysUserRole", allEntries = true)
	public int deleteSysUserRole(String roleId, String userId) {
		return mapper.deleteSysUserRole(roleId, userId);
	}

    @Override
    @CacheEvict(value = "sysUserRole", allEntries = true)
    public SysUserRole getSysUserRoleByUser(String userId) {
        return mapper.getSysUserRoleByUser(userId);
    }

    @Override
    @CacheEvict(value = "sysUserRole", allEntries = true)
    public void updateSysUserRoleByParam(SysUserRole sysUserRole) {
        mapper.updateSysUserRoleByParam(sysUserRole);
    }


}
