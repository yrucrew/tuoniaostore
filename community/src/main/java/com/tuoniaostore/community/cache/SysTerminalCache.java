package com.tuoniaostore.community.cache;

import com.tuoniaostore.datamodel.user.SysTerminal;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysTerminalCache {

    void addSysTerminal(SysTerminal sysTerminal);

    SysTerminal getSysTerminal(String id);

    List<SysTerminal> getSysTerminals(int pageIndex, int pageSize, int status, String title, int retrieveStatus);

    List<SysTerminal> getSysTerminalAll();

    int getSysTerminalCount(int status, String title, int retrieveStatus);

    void changeSysTerminal(SysTerminal sysTerminal);

    SysTerminal getSysTerminalByIdAndUserId(String id, String userId);

    List<SysTerminal> getSysUserTerminalListByUserId(String id);

    String getSysTerminalTitleByList(List<String> terminalIds);

    SysTerminal findSysUserByTerminalName(String terminalName);

    void batchDeleteSysTerminal(List<String> ids);
}
