package com.tuoniaostore.community.cache.impl;
        import com.tuoniaostore.community.cache.SysUserAddressCache;
        import com.tuoniaostore.community.data.SysUserAddressMapper;
        import com.tuoniaostore.datamodel.good.GoodShopCart;
        import com.tuoniaostore.datamodel.user.SysUser;
        import com.tuoniaostore.datamodel.user.SysUserAddress;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;
        import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysUserAddress")
public class SysUserAddressCacheImpl implements SysUserAddressCache {

    @Autowired
    SysUserAddressMapper mapper;

    @Override
    @CacheEvict(value = "sysUserAddress", allEntries = true)
    public void addSysUserAddress(SysUserAddress sysUserAddress) {
        mapper.addSysUserAddress(sysUserAddress);
    }

    @Override
    @Cacheable(value = "sysUserAddress", key = "'getSysUserAddress_'+#id")
    public   SysUserAddress getSysUserAddress(String id) {
        return mapper.getSysUserAddress(id);
    }

    @Override
    @Cacheable(value = "sysUserAddress", key = "'getSysUserAddresss_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysUserAddress> getSysUserAddresss(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getSysUserAddresss(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "sysUserAddress", key = "'getSysUserAddressAll'")
    public List<SysUserAddress> getSysUserAddressAll() {
        return mapper.getSysUserAddressAll();
    }

    @Override
    @Cacheable(value = "sysUserAddress", key = "'getSysUserAddressCount_'+#status+'_'+#name")
    public int getSysUserAddressCount(int status, String name) {
        return mapper.getSysUserAddressCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysUserAddress", allEntries = true)
    public void changeSysUserAddress(SysUserAddress sysUserAddress) {
        mapper.changeSysUserAddress(sysUserAddress);
    }

    @Override
    @Cacheable(value = "sysUserAddress",key = "'getSysUserAddressCount_'+#userId")
    public List<SysUserAddress> getMyAddress(String userId) {
        return mapper.getMyAddress(userId);
    }

    @Override
    @CacheEvict(value = "sysUserAddress", allEntries = true)
    public void changeSysUserDefaultAddressById(String id, String userId) {
        mapper.clearSysUserDefaultAddress(userId);//消除当前用户所有的默认地址
        mapper.changeSysUserDefaultAddress(id);//更新当前的用户默认地址
    }

    @Override
    @CacheEvict(value = "sysUserAddress", allEntries = true)
    public void changeSysUserDefaultAddress(String id) {
        mapper.changeSysUserDefaultAddress(id);//更新当前的用户默认地址
    }

    @Override
    @CacheEvict(value = "sysUserAddress", allEntries = true)
    public void deleteSysUserDefaultAddressById(String id) {
        mapper.deleteSysUserDefaultAddressById(id);
    }


    @Override
    @CacheEvict(value = "sysUserAddress", allEntries = true)
    public void clearSysUserDefaultAddress(String userId) {
        mapper.clearSysUserDefaultAddress(userId);//消除当前用户所有的默认地址
    }

    @Override
    @CacheEvict(value = "sysUserAddress", allEntries = true)
    public SysUserAddress getSysUserAddressByUserId(String userId) {
        //通过用户查找最近的一个地址
        return mapper.getSysUserAddressByUserId(userId);
    }

    @Override
    public List<GoodShopCart> getGoodShopCartByDefault(Map<String, Object> map) {
        return mapper.getGoodShopCartByDefault(map);
    }

    @Override
    @Cacheable(value = "sysUserAddress",key = "'getDefaultSysUserAddress_'+#userId")
    public SysUserAddress getDefaultSysUserAddress(String userId) {
        return mapper.getDefaultSysUserAddress(userId);
    }

}
