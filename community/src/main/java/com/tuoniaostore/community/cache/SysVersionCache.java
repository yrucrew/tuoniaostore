package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysVersion;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysVersionCache {

    void addSysVersion(SysVersion sysVersion);

    SysVersion getSysVersion(String id);

    List<SysVersion> getSysVersions(int pageIndex, int pageSize, int status, String name);
    List<SysVersion> getSysVersionAll();
    int getSysVersionCount(int status, String name);
    void changeSysVersion(SysVersion sysVersion);

    //获取当前是否可更新 可更新返回更新信息 不可更新返回null
    SysVersion getUsefulVersion(Integer deviceType,Integer clientType,Integer versionIndex);
}
