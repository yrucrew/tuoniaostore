package com.tuoniaostore.community.cache;

import com.tuoniaostore.datamodel.user.SysAreaProvince;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
public interface SysAreaProvinceCache {

    void addSysAreaProvince(List<SysAreaProvince> list);

    SysAreaProvince getSysAreaProvince(String id);

    List<SysAreaProvince> getSysAreaProvinces(int status, int pageIndex, int pageSize, String name, int retrieveStatus);

    List<SysAreaProvince> getSysAreaProvinceAll();

    int getSysAreaProvinceCount(int status, String name, int retrieveStatus);

    void changeSysAreaProvince(SysAreaProvince sysAreaProvince);

    void delSysAreaProvince(String areaId);

    List<SysAreaProvince> getSysAreaNotId(String areaId);

    List<SysAreaProvince> getSysAreaProvinceByAreaId(String areaId);

    void deleteSysAreaProvinceByAreaIds(List<String> areaIds);
}
