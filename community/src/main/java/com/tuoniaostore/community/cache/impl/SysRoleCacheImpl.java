package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysRoleCache;
import com.tuoniaostore.community.data.SysRoleMapper;
import com.tuoniaostore.datamodel.user.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysRole")
public class SysRoleCacheImpl implements SysRoleCache {

    @Autowired
    SysRoleMapper mapper;

    @Override
    @CacheEvict(value = "sysRole", allEntries = true)
    public void addSysRole(SysRole sysRole) {
        mapper.addSysRole(sysRole);
    }

    @Override
    @Cacheable(value = "sysRole", key = "'getSysRole_'+#id")
    public SysRole getSysRole(String id) {
        return mapper.getSysRole(id);
    }

    @Override
    @Cacheable(value = "sysRole", key = "'getSysRoles_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#roleName+'_'+#retrieveStatus")
    public List<SysRole> getSysRoles(int status, int pageIndex, int pageSize, String roleName, int retrieveStatus) {
        return mapper.getSysRoles(status, pageIndex, pageSize, roleName, retrieveStatus);
    }

    @Override
    @Cacheable(value = "sysRole", key = "'getSysRoleAll'")
    public List<SysRole> getSysRoleAll() {
        return mapper.getSysRoleAll();
    }

    @Override
    @Cacheable(value = "sysRole", key = "'getSysRoleCount_'+#status+'_'+#roleName+'_'+#retrieveStatus")
    public int getSysRoleCount(int status, String roleName, int retrieveStatus) {
        return mapper.getSysRoleCount(status,  roleName, retrieveStatus);
    }

    @Override
    @CacheEvict(value = "sysRole", allEntries = true)
    public void changeSysRole(SysRole sysRole) {
        mapper.changeSysRole(sysRole);
    }

    @Override
    @Cacheable(value = "sysRole", key = "'getSysRoleByRoleName_'+#roleName")
    public SysRole getSysRoleByRoleName(String roleName) {
        return mapper.getSysRoleByRoleName(roleName);
    }

    @Override
    @CacheEvict(value = "sysRole", allEntries = true)
    public void changeSysRoleRetrieve(String[] id, int retrieveStatus) {
        mapper.changeSysRoleRetrieve(id, retrieveStatus);
    }

    @Override
    @CacheEvict(value = "sysRole", allEntries = true)
    public void deleteSysRole(List<String> idList) {
        mapper.deleteSysRole(idList);
    }

    @Override
    @Cacheable(value = "sysRole", key = "'getSysRoleByRoleName_'+#roleName")
    public SysRole findSysRoleByRoleName(String roleName) {
        return mapper.findSysRoleByRoleName(roleName);
    }

}
