package com.tuoniaostore.community.cache.impl;
        import com.tuoniaostore.community.cache.SysUserChannelCache;
        import com.tuoniaostore.community.data.SysUserChannelMapper;
        import com.tuoniaostore.datamodel.user.SysUserChannel;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 合作渠道，用于区分来自不同渠道的用户数据
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysUserChannel")
public class SysUserChannelCacheImpl implements SysUserChannelCache {

    @Autowired
    SysUserChannelMapper mapper;

    @Override
    @CacheEvict(value = "sysUserChannel", allEntries = true)
    public void addSysUserChannel(SysUserChannel sysUserChannel) {
        mapper.addSysUserChannel(sysUserChannel);
    }

    @Override
    @Cacheable(value = "sysUserChannel", key = "'getSysUserChannel_'+#id")
    public   SysUserChannel getSysUserChannel(String id) {
        return mapper.getSysUserChannel(id);
    }

    @Override
    @Cacheable(value = "sysUserChannel", key = "'getSysUserChannels_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysUserChannel> getSysUserChannels( int status,int pageIndex, int pageSize, String name) {
        return mapper.getSysUserChannels(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "sysUserChannel", key = "'getSysUserChannelAll'")
    public List<SysUserChannel> getSysUserChannelAll() {
        return mapper.getSysUserChannelAll();
    }

    @Override
    @Cacheable(value = "sysUserChannel", key = "'getSysUserChannelCount_'+#status+'_'+#name")
    public int getSysUserChannelCount(int status, String name) {
        return mapper.getSysUserChannelCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysUserChannel", allEntries = true)
    public void changeSysUserChannel(SysUserChannel sysUserChannel) {
        mapper.changeSysUserChannel(sysUserChannel);
    }

    @Override
    @Cacheable(value = "sysUserChannel", key = "'getSysUserChannel_'+#fromChannel")
    public SysUserChannel getSysUserChannelByName(String fromChannel) {
        return mapper.getSysUserChannelByName(fromChannel);
    }

}
