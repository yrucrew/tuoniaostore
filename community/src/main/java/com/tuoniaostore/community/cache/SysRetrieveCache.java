package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysRetrieve;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysRetrieveCache {

    void addSysRetrieve(SysRetrieve sysRetrieve);

    SysRetrieve getSysRetrieve(String id);

    List<SysRetrieve> getSysRetrieves(int pageIndex, int pageSize, int status, String name);
    List<SysRetrieve> getSysRetrieveAll();
    int getSysRetrieveCount(int status, String name);
    void changeSysRetrieve(SysRetrieve sysRetrieve);
    void changeRetrieveStatus(String tableName, int retrieveStatus, String id);

    void addSysRetrieves(List<SysRetrieve> sysRetrieve);
}
