package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysRoleMenuCache;
import com.tuoniaostore.community.data.SysRoleMenuMapper;
import com.tuoniaostore.datamodel.user.SysRoleMenu;
import com.tuoniaostore.datamodel.user.SysRoleMenuPos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色与菜单对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysRoleMenu")
public class SysRoleMenuCacheImpl implements SysRoleMenuCache {

    @Autowired
    SysRoleMenuMapper mapper;

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysRoleMenu", allEntries = true),
            @CacheEvict(value = "sysMenu", allEntries = true)
    })
    public void addSysRoleMenu(List<SysRoleMenu> list) {
        mapper.addSysRoleMenu(list);
    }

    @Override
    @Cacheable(value = "sysRoleMenu", key = "'getSysRoleMenu_'+#id")
    public SysRoleMenu getSysRoleMenu(String id) {
        return mapper.getSysRoleMenu(id);
    }

    @Override
    @Cacheable(value = "sysRoleMenu", key = "'getSysRoleMenus_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysRoleMenu> getSysRoleMenus(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSysRoleMenus(status, pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "sysRoleMenu", key = "'getSysRoleMenuAll'")
    public List<SysRoleMenu> getSysRoleMenuAll() {
        return mapper.getSysRoleMenuAll();
    }

    @Override
    @Cacheable(value = "sysRoleMenu", key = "'getSysRoleMenuCount_'+#status+'_'+#name")
    public int getSysRoleMenuCount(int status, String name) {
        return mapper.getSysRoleMenuCount(status, name);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysRoleMenu", allEntries = true),
            @CacheEvict(value = "sysMenu", allEntries = true)
    })
    public void changeSysRoleMenu(SysRoleMenu sysRoleMenu) {
        mapper.changeSysRoleMenu(sysRoleMenu);
    }

    @Override
    public List<SysRoleMenu> getSysRoleMenusByRoleId(String roleId) {
        return mapper.getSysRoleMenusByRoleId(roleId);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysRoleMenu", allEntries = true),
            @CacheEvict(value = "sysMenu", allEntries = true)
    })
    public void delSysRoleMenuByRoleId(String roleId) {
        mapper.delSysRoleMenuByRoleId(roleId);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysRoleMenu", allEntries = true),
            @CacheEvict(value = "sysMenu", allEntries = true)
    })
    public void deleteSysRoleMenu(List<String> menuIds) {
        mapper.deleteSysRoleMenu(menuIds);
    }

}
