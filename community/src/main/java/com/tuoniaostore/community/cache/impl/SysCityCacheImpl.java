package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysCityCache;
import com.tuoniaostore.community.data.SysCityMapper;
import com.tuoniaostore.datamodel.user.SysCity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
@Component
@CacheConfig(cacheNames = "sysCity")
public class SysCityCacheImpl implements SysCityCache {

    @Autowired
    SysCityMapper mapper;

    @Override
    @CacheEvict(value = "sysCity", allEntries = true)
    public void addSysCity(SysCity sysCity) {
        mapper.addSysCity(sysCity);
    }

    @Override
    @Cacheable(value = "sysCity", key = "'getSysCity_'+#id")
    public SysCity getSysCity(String id) {
        return mapper.getSysCity(id);
    }

    @Override
    @Cacheable(value = "sysCity", key = "'getSysCitys_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name+'_'+#retrieveStatus")
    public List<SysCity> getSysCitys(int status, int pageIndex, int pageSize, String name, int retrieveStatus) {
        return mapper.getSysCitys(status, pageIndex, pageSize, name, retrieveStatus);
    }

    @Override
    @Cacheable(value = "sysCity", key = "'getSysCityAll'")
    public List<SysCity> getSysCityAll() {
        return mapper.getSysCityAll();
    }

    @Override
    @Cacheable(value = "sysCity", key = "'getSysCityByIds_'+#ids")
    public List<SysCity> getSysCityByIds(List<String> ids) {
        return mapper.getSysCityByIds(ids);
    }

    @Override
    @Cacheable(value = "sysCity", key = "'getSysCityCount_'+#status+'_'+#name+'_'+#retrieveStatus")
    public int getSysCityCount(int status, String name, int retrieveStatus) {
        return mapper.getSysCityCount(status, name, retrieveStatus);
    }

    @Override
    @Cacheable(value = "sysCity", key = "'getSysCityByShengLevel_'+#sheng+'_'+#level")
    public List<SysCity> getSysCityByShengLevel(int sheng, int level) {
        return mapper.getSysCityByShengLevel(sheng, level);
    }

    @Override
    public List<SysCity> getSysCityByShengDi(int sheng, int di) {
        return mapper.getSysCityByShengDi(sheng, di);
    }

    @Override
    public List<SysCity> getSysProvincesCheckBind(String areaId) {
        return mapper.getSysProvincesCheckBind(areaId);
    }

    @Override
    public List<SysCity> getSysProvincesUnBind() {
        return mapper.getSysProvincesUnBind();
    }

    @Override
    @CacheEvict(value = "sysCity", allEntries = true)
    public void changeSysCity(SysCity sysCity) {
        mapper.changeSysCity(sysCity);
    }

    @Override
    @Cacheable(value = "sysCity", key = "'getSysProvinceAll_'+#di+'_'+#level")
    public List<SysCity> getSysProvinceAll(int di, int level) {
        return mapper.getSysProvinceAll(di, level);
    }
}
