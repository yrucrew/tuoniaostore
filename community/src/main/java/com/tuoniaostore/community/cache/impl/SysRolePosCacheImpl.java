package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysRolePosCache;
import com.tuoniaostore.community.data.SysRolePosMapper;
import com.tuoniaostore.datamodel.user.SysRolePos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
@Component
@CacheConfig(cacheNames = "sysRolePos")
public class SysRolePosCacheImpl implements SysRolePosCache {

    @Autowired
    SysRolePosMapper mapper;

    @Override
    @CacheEvict(value = "sysRolePos", allEntries = true)
    public void addSysRolePos(SysRolePos sysRolePos) {
        mapper.addSysRolePos(sysRolePos);
    }

    @Override
    @Cacheable(value = "sysRolePos", key = "'getSysRolePos_'+#id")
    public SysRolePos getSysRolePos(String id) {
        return mapper.getSysRolePos(id);
    }

    @Override
    @Cacheable(value = "sysRolePos", key = "'getSysRolePoss_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name+'_'+#retrieveStatus")
    public List<SysRolePos> getSysRolePoss(int status, int pageIndex, int pageSize, String name, int retrieveStatus) {
        return mapper.getSysRolePoss(pageIndex, pageSize, status, name, retrieveStatus);
    }

    @Override
    @Cacheable(value = "sysRolePos", key = "'getSysRolePosAll'")
    public List<SysRolePos> getSysRolePosAll() {
        return mapper.getSysRolePosAll();
    }

    @Override
    @Cacheable(value = "sysRolePos", key = "'getSysRolePosCount_'+#status+'_'+#name+'_'+#retrieveStatus")
    public int getSysRolePosCount(int status, String name, int retrieveStatus) {
        return mapper.getSysRolePosCount(status, name, retrieveStatus);
    }

    @Override
    @CacheEvict(value = "sysRolePos", allEntries = true)
    public void changeSysRolePos(SysRolePos sysRolePos) {
        mapper.changeSysRolePos(sysRolePos);
    }

    @Override
    public List<SysRolePos> getSysRolePosByRoleName(String roleName) {
        return mapper.getSysRolePosByRoleName(roleName);
    }

}
