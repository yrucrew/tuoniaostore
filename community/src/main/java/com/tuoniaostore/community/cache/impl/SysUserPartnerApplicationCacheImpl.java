package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysUserPartnerApplicationCache;
import com.tuoniaostore.community.data.SysUserPartnerApplicationMapper;
import com.tuoniaostore.datamodel.user.SysUserPartnerApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysUserPartnerApplication")
public class SysUserPartnerApplicationCacheImpl implements SysUserPartnerApplicationCache {

    @Autowired
    SysUserPartnerApplicationMapper mapper;

    @Override
    @CacheEvict(value = "sysUserPartnerApplication", allEntries = true)
    public void addSysUserPartnerApplication(SysUserPartnerApplication sysUserPartnerApplication) {
        mapper.addSysUserPartnerApplication(sysUserPartnerApplication);
    }

    @Override
    @Cacheable(value = "sysUserPartnerApplication", key = "'getSysUserPartnerApplication_'+#id")
    public SysUserPartnerApplication getSysUserPartnerApplication(String id) {
        return mapper.getSysUserPartnerApplication(id);
    }

    @Override
    @Cacheable(value = "sysUserPartnerApplication", key = "'getSysUserPartnerApplications_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysUserPartnerApplication> getSysUserPartnerApplications(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getSysUserPartnerApplications( status, pageIndex, pageSize,name);
    }

    @Override
    @Cacheable(value = "sysUserPartnerApplication", key = "'getSysUserPartnerApplicationAll'")
    public List<SysUserPartnerApplication> getSysUserPartnerApplicationAll() {
        return mapper.getSysUserPartnerApplicationAll();
    }

    @Override
    @Cacheable(value = "sysUserPartnerApplication", key = "'getSysUserPartnerApplicationCount_'+#status+'_'+#name")
    public int getSysUserPartnerApplicationCount(int status, String name) {
        return mapper.getSysUserPartnerApplicationCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysUserPartnerApplication", allEntries = true)
    public void changeSysUserPartnerApplication(SysUserPartnerApplication sysUserPartnerApplication) {
        mapper.changeSysUserPartnerApplication(sysUserPartnerApplication);
    }

    @Override
    @Cacheable(value = "sysUserPartnerApplication", key = "'getSysUserPartnerApplicationAppKey_'+#appKey")
    public SysUserPartnerApplication getSysUserPartnerApplicationAppKey(String appKey) {
        return mapper.getSysUserPartnerApplicationAppKey(appKey);
    }
}
