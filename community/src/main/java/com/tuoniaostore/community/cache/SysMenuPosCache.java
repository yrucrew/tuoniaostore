package com.tuoniaostore.community.cache;
import com.alibaba.fastjson.JSONArray;
import com.tuoniaostore.datamodel.user.SysMenuPos;

import java.util.List;
/**
 * 菜单管理
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
public interface SysMenuPosCache {

    void addSysMenuPos(SysMenuPos sysMenuPos);

    SysMenuPos getSysMenuPos(String id);

    List<SysMenuPos> getSysMenuPoss(int status, int pageIndex, int pageSize, String name, int retrieveStatus);
    List<SysMenuPos> getSysMenuPosAll();
    int getSysMenuPosCount(int status, String name, int retrieveStatus);
    void changeSysMenuPos(SysMenuPos sysMenuPos);

    List<SysMenuPos> getSysMenuPosByIds(List<String> menuIds);

    JSONArray getSysMenuPosByRoleId(String roleId);

    JSONArray getSysMenuPosInRoleId(List<String> roleIds);

    void updateSysMenuPosByIds(List<String> menuId, int status);
}
