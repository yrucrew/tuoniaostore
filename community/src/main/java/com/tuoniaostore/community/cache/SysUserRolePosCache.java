package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysUserRolePos;

import java.util.List;
/**
 * 用户与角色对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
public interface SysUserRolePosCache {

    void addSysUserRolePos(SysUserRolePos sysUserRolePos);

    SysUserRolePos getSysUserRolePos(String id);

    List<SysUserRolePos> getSysUserRolePoss(int status, int pageIndex, int pageSize, String name, int retrieveStatus);
    List<SysUserRolePos> getSysUserRolePosAll();
    int getSysUserRolePosCount(int status, String name, int retrieveStatus);
    void changeSysUserRolePos(SysUserRolePos sysUserRolePos);

    List<SysUserRolePos> getSysUserRolePosByUserId(String userId);

    void updateSysRolePosByIds(List<String> ids, int status);
}
