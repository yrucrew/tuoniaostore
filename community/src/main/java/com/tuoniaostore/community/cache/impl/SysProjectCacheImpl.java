package com.tuoniaostore.community.cache.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.community.cache.SysProjectCache;
import com.tuoniaostore.community.data.SysProjectMapper;
import com.tuoniaostore.datamodel.user.SysProject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
@Component
@CacheConfig(cacheNames = "sysProject")
public class SysProjectCacheImpl implements SysProjectCache {

    @Autowired
    SysProjectMapper mapper;
    @Override
    @CacheEvict(value = "sysProject", allEntries = true)
    public void addSysProject(SysProject sysProject) {
        mapper.addSysProject(sysProject);
    }

    @Override
    @Cacheable(value = "sysProject", key = "'getSysProject_'+#id")
    public SysProject getSysProject(String id) {
        return mapper.getSysProject(id);
    }

    @Override
    @Cacheable(value = "sysProject", key = "'getSysProjectTree'")
    public JSONArray getSysProjectTree() {
        List<SysProject> sysProjects =getSysProjectByParentId("0");
        int size = sysProjects.size();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < size; i++) {
            JSONObject json = new JSONObject();
            json.put("id", sysProjects.get(i).getId());
            json.put("name", sysProjects.get(i).getName());
            List<SysProject> child=getSysProjectByParentId(sysProjects.get(i).getId());
            json.put("children", child);
            jsonArray.add(json);
        }
        return jsonArray;
    }

    @Override
    @Cacheable(value = "sysProject", key = "'getSysProjects_'+#pageIndex+'_'+#pageSize+'_'+#name")
    public List<SysProject> getSysProjects(int pageIndex, int pageSize, String name) {
        List<SysProject> sysProjects = mapper.getSysProjects(pageIndex, pageSize, name);
        fillSysProject(sysProjects);
        return sysProjects;
    }

    private void fillSysProject(List<SysProject> sysProjects) {
        int size = sysProjects.size();
        for (int i = 0; i < size; i++) {
            SysProject sysProject = getSysProject(sysProjects.get(i).getParentId());
            if (sysProject != null && sysProject.getParentId() != "0") {
                sysProjects.get(i).setParentName(sysProject.getName());
            } else {
                sysProjects.get(i).setParentName("父类");
            }
        }
    }

    @Override
    @Cacheable(value = "sysProject", key = "'getSysProjectAll'")
    public List<SysProject> getSysProjectAll() {
        List<SysProject> sysProjects = mapper.getSysProjectAll();
        fillSysProject(sysProjects);
        return sysProjects;
    }

    @Override
    @Cacheable(value = "sysProject", key = "'getSysProjectCount_'+#name")
    public int getSysProjectCount(String name) {
        return mapper.getSysProjectCount(name);
    }

    @Override
    @CacheEvict(value = "sysProject", allEntries = true)
    public void changeSysProject(SysProject sysProject) {
        mapper.changeSysProject(sysProject);
    }

    @Override
    @Cacheable(value = "sysProject", key = "'getSysProjectByParentId_'+#parentId")
    public List<SysProject> getSysProjectByParentId( String parentId) {
        List<SysProject> sysProjects = mapper.getSysProjectByParentId(parentId);
        return sysProjects;
    }
}
