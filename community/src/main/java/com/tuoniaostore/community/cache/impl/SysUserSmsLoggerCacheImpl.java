package com.tuoniaostore.community.cache.impl;
        import com.tuoniaostore.community.cache.SysUserSmsLoggerCache;
        import com.tuoniaostore.community.data.SysUserSmsLoggerMapper;
        import com.tuoniaostore.datamodel.user.SysUserSmsLogger;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysUserSmsLogger")
public class SysUserSmsLoggerCacheImpl implements SysUserSmsLoggerCache {

    @Autowired
    SysUserSmsLoggerMapper mapper;

    @Override
    @CacheEvict(value = "sysUserSmsLogger", allEntries = true)
    public void addSysUserSmsLogger(SysUserSmsLogger sysUserSmsLogger) {
        mapper.addSysUserSmsLogger(sysUserSmsLogger);
    }

    @Override
    public   SysUserSmsLogger getSysUserSmsLogger(String id) {
        return mapper.getSysUserSmsLogger(id);
    }

    @Override
    @Cacheable(value = "sysUserSmsLogger", key = "'getSysUserSmsLoggers_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysUserSmsLogger> getSysUserSmsLoggers(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getSysUserSmsLoggers( status,pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "sysUserSmsLogger", key = "'getSysUserSmsLoggerAll'")
    public List<SysUserSmsLogger> getSysUserSmsLoggerAll() {
        return mapper.getSysUserSmsLoggerAll();
    }

    @Override
    @Cacheable(value = "sysUserSmsLogger", key = "'getSysUserSmsLoggerCount_'+#status+'_'+#name")
    public int getSysUserSmsLoggerCount(int status, String name) {
        return mapper.getSysUserSmsLoggerCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysUserSmsLogger", allEntries = true)
    public void changeSysUserSmsLogger(SysUserSmsLogger sysUserSmsLogger) {
        mapper.changeSysUserSmsLogger(sysUserSmsLogger);
    }

    @Override
    @Cacheable(value = "sysUserSmsLogger", key = "'getSysUserSmsLogger_'+#phone+'_'+#type")
    public SysUserSmsLogger getSysUserSmsLoggerByPhone(String phone,int type) {
        return mapper.getSysUserSmsLoggerByPhone(phone,type);
    }


}
