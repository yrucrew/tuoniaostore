package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysUserRolePosCache;
import com.tuoniaostore.community.data.SysUserRolePosMapper;
import com.tuoniaostore.datamodel.user.SysUserRolePos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户与角色对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
@Component
@CacheConfig(cacheNames = "sysUserRolePos")
public class SysUserRolePosCacheImpl implements SysUserRolePosCache {

    @Autowired
    SysUserRolePosMapper mapper;

    @Override
    @Async
    @CacheEvict(value = "sysUserRolePos", allEntries = true)
    public void addSysUserRolePos(SysUserRolePos sysUserRolePos) {
        mapper.addSysUserRolePos(sysUserRolePos);
    }

    @Override
    @Cacheable(value = "sysUserRolePos", key = "'getSysUserRolePos_'+#id")
    public   SysUserRolePos getSysUserRolePos(String id) {
        return mapper.getSysUserRolePos(id);
    }

    @Override
    @Cacheable(value = "sysUserRolePos", key = "'getSysUserRolePoss_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name+'_'+#retrieveStatus")
    public List<SysUserRolePos> getSysUserRolePoss(int status, int pageIndex, int pageSize, String name,int retrieveStatus) {
        return mapper.getSysUserRolePoss(pageIndex, pageSize, status, name,retrieveStatus);
    }

    @Override
    @Cacheable(value = "sysUserRolePos", key = "'getSysUserRolePosAll'")
    public List<SysUserRolePos> getSysUserRolePosAll() {
        return mapper.getSysUserRolePosAll();
    }

    @Override
    @Cacheable(value = "sysUserRolePos", key = "'getSysUserRolePosCount_'+#status+'_'+#name+'_'+#retrieveStatus")
    public int getSysUserRolePosCount(int status, String name,int retrieveStatus) {
        return mapper.getSysUserRolePosCount(status, name,retrieveStatus);
    }

    @Override
    @CacheEvict(value = "sysUserRolePos", allEntries = true)
    public void changeSysUserRolePos(SysUserRolePos sysUserRolePos) {
        mapper.changeSysUserRolePos(sysUserRolePos);
    }

    @Override
    public List<SysUserRolePos> getSysUserRolePosByUserId(String userId) {
        return mapper.getSysUserRolePosByUserId(userId);
    }

    @Override
    public void updateSysRolePosByIds(List<String> ids, int status) {
        mapper.updateSysRolePosByIds(ids, status);
    }

}
