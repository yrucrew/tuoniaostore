package com.tuoniaostore.community.cache.impl;
        import com.alibaba.fastjson.JSONArray;
        import com.alibaba.fastjson.JSONObject;
        import com.tuoniaostore.community.cache.SysMenuCache;
        import com.tuoniaostore.community.cache.SysRoleCache;
        import com.tuoniaostore.community.cache.SysRoleMenuCache;
        import com.tuoniaostore.community.data.SysMenuMapper;
        import com.tuoniaostore.datamodel.user.SysMenu;
        import com.tuoniaostore.datamodel.user.SysRoleMenu;
        import com.tuoniaostore.datamodel.user.SysUser;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.cache.annotation.Caching;
        import org.springframework.stereotype.Component;

        import java.util.ArrayList;
        import java.util.List;

/**
 * 菜单管理
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysMenu")
public class SysMenuCacheImpl implements SysMenuCache {

    @Autowired
    SysMenuMapper mapper;
    @Autowired
    SysRoleMenuCache sysRoleMenuCache;

    @Override
    @Cacheable(value = "sysMenu", key = "'findByMenu'+#name+'_'+#parentId")
    public SysMenu findByMenu(String name, String parentId) {
        return mapper.findByMenu(name, parentId);
    }
    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysRoleMenu", allEntries = true),
            @CacheEvict(value = "sysMenu", allEntries = true)
    })
    public void addSysMenu(SysMenu sysMenu) {
        mapper.addSysMenu(sysMenu);
    }

    @Override
    @Cacheable(value = "sysMenu", key = "'getSysMenu_'+#id")
    public   SysMenu getSysMenu(String id) {
        return mapper.getSysMenu(id);
    }

    @Override
    @Cacheable(value = "sysMenu", key = "'getSysMenus_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysMenu> getSysMenus( int status,int pageIndex, int pageSize, String name) {
        List<SysMenu>  sysMenus= mapper.getSysMenus( status,pageIndex, pageSize, name);
        fillSysMenus(sysMenus);
        return sysMenus;
    }

    private void  fillSysMenus(List<SysMenu>  sysMenus){
        int size=sysMenus.size();
        for(int i=0;i<size;i++){
            // 创建人
            fullUserMsg(sysMenus.get(i));

            // 父菜单名
            SysMenu parentMenu=getSysMenu(sysMenus.get(i).getParentId());
            if(parentMenu!=null){
                sysMenus.get(i).setParentName(parentMenu.getName());
            }
        }
    }

    /**
     * 填充用户信息
     */
    private void fullUserMsg(SysMenu currMenu) {
        if (currMenu.getRealName() != null) {
            currMenu.setUserName(currMenu.getRealName());
        } else if (currMenu.getShowName() != null) {
            currMenu.setUserName(currMenu.getShowName());
        } else if (currMenu.getDefaultName()!=null) {
            currMenu.setUserName(currMenu.getDefaultName());
        } else if (currMenu.getPhoneNumber()!=null) {
            currMenu.setUserName(currMenu.getPhoneNumber());
        }
    }

    @Override
    @Cacheable(value = "sysMenu", key = "'getWebSysMenus_'+#roleId")
    public JSONArray getWebSysMenus(String roleId) {
        JSONArray jsonArray = null;
        try {
            List<SysRoleMenu> roles = sysRoleMenuCache.getSysRoleMenusByRoleId(roleId);
            List<String> menuIds = new ArrayList<>();
            for (int i = 0; i < roles.size(); i++) {
                menuIds.add(roles.get(i).getMenuId());
            }
            if (menuIds.size() != 0) {
                jsonArray = getWebMenus("0", menuIds);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysRoleMenu", allEntries = true),
            @CacheEvict(value = "sysMenu", allEntries = true)
    })
    public void deleteSysMenu(List<String> menuIds) {
        mapper.deleteSysMenu(menuIds);
    }

    private JSONArray getWebMenus(String parentId, List<String> menuIds) throws Exception {
        try {
            List<SysMenu> list =  getSysMenuChilds(parentId, menuIds);
            JSONArray jsonArray = new JSONArray();
            if (list != null) {
                int count = list.size();
                for (int k = 0; k < count; k++) {
                    JSONObject json = new JSONObject();
                    json.put("title", list.get(k).getName());
                    json.put("icon", list.get(k).getIcon());
                    json.put("jump", list.get(k).getUrl());
                    json.put("name", list.get(k).getName());
                    String id = list.get(k).getId();
                    JSONArray child = getWebMenus(id, menuIds);
                    if (child != null) {
                        json.put("list", child);
                    }
                    jsonArray.add(json);
                }
            }
            return jsonArray;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @Cacheable(value = "sysMenu", key = "'getSysMenuAll'")
    public List<SysMenu> getSysMenuAll() {
        List<SysMenu> sysMenus= mapper.getSysMenuAll();
        fillSysMenus(sysMenus);
        return sysMenus;
    }

    @Override
    @Cacheable(value = "sysMenu", key = "'getSysMenuCount_'+#status+'_'+#name")
    public int getSysMenuCount(int status, String name) {
        return mapper.getSysMenuCount(status, name);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysRoleMenu", allEntries = true),
            @CacheEvict(value = "sysMenu", allEntries = true)
    })
    public void changeSysMenu(SysMenu sysMenu) {
        mapper.changeSysMenu(sysMenu);
    }

    @Override
    @Cacheable(value = "sysMenu", key = "'getSysMenuChilds_'+#parentId+'_'+#menuIds")
    public List<SysMenu> getSysMenuChilds(String parentId, List<String> menuIds) {
        return mapper.getSysMenuChilds(parentId,menuIds);
    }
}
