package com.tuoniaostore.community.cache;
import com.tuoniaostore.community.vo.PurchaseUserVo;
import com.tuoniaostore.datamodel.user.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysUserCache {

    void addSysUser(SysUser sysUser);

    void addSysUserForPos(SysUser sysUser);

    SysUser getSysUser(String id);

    List<SysUser> getSysUsers(int pageIndex, int pageSize, int status, String name);
    List<SysUser> getSysUserAll();

    List<SysUser> getSysUserByName(String name);

    int getSysUserCount(int status, String name);
    void changeSysUser(SysUser sysUser);
    List<SysUser> getDefaultNameOrPhone(String name);
    void changeLogin(SysUser sysUser);

    SysUser getSysUserByPhoneNumber(String phoneNumber);

    /**
     * 通过手机号码更新密码
     * @param phoneNum
     */
    void updatePasswordByPhoneNum(String phoneNum,String password);

    /**
     * 用户自己修改密码
     * @param id 用户id
     * @param password 密码
     */
    void updatePasswordById(String id, String password);

    /**
     * 更换手机号码
     * @param id 用户id
     * @param phoneNum 手机号码
     */
    void changeSysUserPhoneNum(String id, String phoneNum);

    /**
     * 根据id查询用户
     * @author sqd
     * @date 2019/4/16
     * @param
     * @return
     */
     List<SysUser> getSysUserInIds(@Param("ids") List<String> ids);

	List<PurchaseUserVo> getPurchaseUserList(String searchKey, List<String> roleIds, int pageStartIndex, int pageSize);

	int getPurchaseUserListCount(String searchKey, List<String> roleIds);

	List<SysUser> getSysUsersForSearch(String column, Object value);

    List<SysUser> getSysUserByParam(Map<String, Object> map, Integer pageStartIndex, Integer pageSize);

    int getCountSysUserByParam(Map<String, Object> map);

    void delSysUserByUserId(String id);

    void updateSysUserParam(SysUser sysUser);

    SysUser findSysUserByPhoneOrDefaultName(String phoneNumber, String defaultName);
    List<SysUser> getSysUserAccountOrName(String accountOrName);

    /**
     * pos机修改门店用户
     */
    int updateSysUserByPosByRemote(SysUser sysUser);

    List<SysUser> getAllUserByRoleIds(String name, int status, List<String> roleIds);

    List<SysUser> getSysUserByPhoneNumLike(String phoneNum);

}
