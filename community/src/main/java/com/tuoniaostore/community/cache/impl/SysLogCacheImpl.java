package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysLogCache;
import com.tuoniaostore.community.data.SysLogMapper;
import com.tuoniaostore.datamodel.user.SysLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysLog")
public class SysLogCacheImpl implements SysLogCache {

    @Autowired
    SysLogMapper mapper;

    @Override
    @CacheEvict(value = "sysLog", allEntries = true)
    public void addSysLog(SysLog sysLog) {
        mapper.addSysLog(sysLog);
    }

    @Override
    @Cacheable(value = "sysLog", key = "'getSysLog_'+#id")
    public SysLog getSysLog(String id) {
        return mapper.getSysLog(id);
    }

    @Override
    @Cacheable(value = "sysLog", key = "'getSysLogs_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysLog> getSysLogs( int status, int pageIndex, int pageSize,String name) {
        return mapper.getSysLogs(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "sysLog", key = "'getSysLogAll'")
    public List<SysLog> getSysLogAll() {
        return mapper.getSysLogAll();
    }

    @Override
    @Cacheable(value = "sysLog", key = "'getSysLogCount_'+#status+'_'+#name")
    public int getSysLogCount(int status, String name) {
        return mapper.getSysLogCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysLog", allEntries = true)
    public void changeSysLog(SysLog sysLog) {
        mapper.changeSysLog(sysLog);
    }

}
