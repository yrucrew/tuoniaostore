package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysRoleMenuPos;

import java.util.List;
/**
 * 角色与菜单对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
public interface SysRoleMenuPosCache {

    void addSysRoleMenuPos(SysRoleMenuPos sysRoleMenuPos);

    SysRoleMenuPos getSysRoleMenuPos(String id);

    List<SysRoleMenuPos> getSysRoleMenuPoss(int status, int pageIndex, int pageSize, String name, int retrieveStatus);
    List<SysRoleMenuPos> getSysRoleMenuPosAll();
    int getSysRoleMenuPosCount(int status, String name, int retrieveStatus);
    void changeSysRoleMenuPos(SysRoleMenuPos sysRoleMenuPos);

    void batchAddSysRoleMenuPos(List<SysRoleMenuPos> list);

    List<SysRoleMenuPos> getSysRoleMenuPosByRoleId(String roleId);

    List<SysRoleMenuPos> getSysRoleMenusPosInRoleId(List<String> roleIds);

    void delSysRoleMenuPosByRoleId(String roleId);
}
