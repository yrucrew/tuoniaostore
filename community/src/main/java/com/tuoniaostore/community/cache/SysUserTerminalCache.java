package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysUserTerminal;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysUserTerminalCache {

    void addSysUserTerminal(SysUserTerminal sysUserTerminal);

    SysUserTerminal getSysUserTerminal(String id);

    List<SysUserTerminal> getSysUserTerminals(int pageIndex, int pageSize, int status, String name);
    List<SysUserTerminal> getSysUserTerminalAll();
    void addSysUserTerminals(List<SysUserTerminal> list);
    int getSysUserTerminalCount(int status, String name);
    void changeSysUserTerminal(SysUserTerminal sysUserTerminal);
    void delSysUserTerminal(String userId);

    /**
     * 查找该终端是否有这个用户
     * @param id
     * @param userId
     * @return
     */
    SysUserTerminal getSysUserTerminal2(String id, String userId);

}
