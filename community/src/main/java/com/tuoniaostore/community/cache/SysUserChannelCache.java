package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysUserChannel;

import java.util.List;
/**
 * 合作渠道，用于区分来自不同渠道的用户数据
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysUserChannelCache {

    void addSysUserChannel(SysUserChannel sysUserChannel);

    SysUserChannel getSysUserChannel(String id);

    List<SysUserChannel> getSysUserChannels(int pageIndex, int pageSize, int status, String name);
    List<SysUserChannel> getSysUserChannelAll();
    int getSysUserChannelCount(int status, String name);
    void changeSysUserChannel(SysUserChannel sysUserChannel);

    /**
     * 通过渠道名字 获取渠道id
     * @param fromChannel
     * @return
     */
    SysUserChannel getSysUserChannelByName(String fromChannel);
}
