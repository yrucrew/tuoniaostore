package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysAreaProvinceCache;
import com.tuoniaostore.community.data.SysAreaProvinceMapper;
import com.tuoniaostore.datamodel.user.SysAreaProvince;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
@Component
@CacheConfig(cacheNames = "sysAreaProvince")
public class SysAreaProvinceCacheImpl implements SysAreaProvinceCache {

    @Autowired
    SysAreaProvinceMapper mapper;

    @Override
    @CacheEvict(value = "sysAreaProvince", allEntries = true)
    public void addSysAreaProvince(List<SysAreaProvince> list) {
        mapper.addSysAreaProvince(list);
    }

    @Override
    @Cacheable(value = "sysAreaProvince", key = "'getSysAreaNotId_'+#areaId")
    public List<SysAreaProvince> getSysAreaNotId(String areaId) {
        return mapper.getSysAreaNotId(areaId);
    }

    @Override
    @Cacheable(value = "sysAreaProvince", key = "'getSysAreaById_'+#areaId")
    public List<SysAreaProvince> getSysAreaProvinceByAreaId(String areaId) {
        return mapper.getSysAreaProvinceByAreaId(areaId);
    }

    @Override
    public void deleteSysAreaProvinceByAreaIds(List<String> areaIds) {
        mapper.deleteSysAreaProvinceByAreaIds(areaIds);
    }

    @Override
    @CacheEvict(value = "sysAreaProvince", allEntries = true)
    public void delSysAreaProvince(String areaId) {
        mapper.delSysAreaProvince(areaId);
    }

    @Override
    @Cacheable(value = "sysAreaProvince", key = "'getSysAreaProvince_'+#id")
    public SysAreaProvince getSysAreaProvince(String id) {
        return mapper.getSysAreaProvince(id);
    }

    @Override
    @Cacheable(value = "sysAreaProvince", key = "'getSysAreaProvinces_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name+'_'+#retrieveStatus")
    public List<SysAreaProvince> getSysAreaProvinces(int status, int pageIndex, int pageSize, String name, int retrieveStatus) {
        return mapper.getSysAreaProvinces(status, pageIndex, pageSize, name, retrieveStatus);
    }

    @Override
    @Cacheable(value = "sysAreaProvince", key = "'getSysAreaProvinceAll'")
    public List<SysAreaProvince> getSysAreaProvinceAll() {
        return mapper.getSysAreaProvinceAll();
    }

    @Override
    @Cacheable(value = "sysAreaProvince", key = "'getSysAreaProvinceCount_'+#status+'_'+#name+'_'+#retrieveStatus")
    public int getSysAreaProvinceCount(int status, String name, int retrieveStatus) {
        return mapper.getSysAreaProvinceCount(status, name, retrieveStatus);
    }

    @Override
    @CacheEvict(value = "sysAreaProvince", allEntries = true)
    public void changeSysAreaProvince(SysAreaProvince sysAreaProvince) {
        mapper.changeSysAreaProvince(sysAreaProvince);
    }

}
