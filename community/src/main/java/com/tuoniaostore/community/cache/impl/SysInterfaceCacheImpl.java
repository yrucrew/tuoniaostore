package com.tuoniaostore.community.cache.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.cache.SysInterfaceCache;
import com.tuoniaostore.community.data.SysInterfaceMapper;
import com.tuoniaostore.datamodel.user.SysInterface;
import com.tuoniaostore.datamodel.user.SysInterfaceParam;
import com.tuoniaostore.datamodel.user.SysProject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
@Component
@CacheConfig(cacheNames = "sysInterface")
public class SysInterfaceCacheImpl implements SysInterfaceCache {

    @Autowired
    SysInterfaceMapper mapper;
    @Autowired
    CommunityDataAccessManager dataAccessManager;

    @Override
    @Cacheable(value = "sysInterface", key = "'getSysInterfaceTree'")
    public JSONArray getSysInterfaceTree() {
        List<SysProject> sysProjects = dataAccessManager.getSysProjectByParentId("0");
        int size = sysProjects.size();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < size; i++) {
            JSONObject json = new JSONObject();
            json.put("id", sysProjects.get(i).getId());
            json.put("name", sysProjects.get(i).getName());
            List<SysProject> child = dataAccessManager.getSysProjectByParentId(sysProjects.get(i).getId());
            JSONArray childArr = new JSONArray();
            int ksize = child.size();
            for (int k = 0; k < ksize; k++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id", child.get(k).getId());
                jsonObject.put("name", child.get(k).getName());
                fillChild(child.get(k).getId(), jsonObject);
                childArr.add(jsonObject);
            }
            json.put("children", childArr);
            jsonArray.add(json);
        }
        return jsonArray;
    }

    private void fillChild(String interfaceId, JSONObject jsonObject) {
        List<SysInterface> sysInterfaces = getSysInterfaceByProjectId(interfaceId);
        fillSysInfterface(sysInterfaces);
        if (sysInterfaces != null) {
            jsonObject.put("children", sysInterfaces);
        }
    }

    @Override
    @Cacheable(value = "sysInterface", key = "'getSysInterfaceByProjectId_'+#projectId")
    public List<SysInterface> getSysInterfaceByProjectId(String projectId) {
        return mapper.getSysInterfaceByProjectId(projectId);
    }

    @Override
    @CacheEvict(value = "sysInterface", allEntries = true)
    public void addSysInterface(SysInterface sysInterface) {
        mapper.addSysInterface(sysInterface);
    }

    @Override
    @Cacheable(value = "sysInterface", key = "'getSysInterface_'+#id")
    public SysInterface getSysInterface(String id) {
        return mapper.getSysInterface(id);
    }

    @Override
    @Cacheable(value = "sysInterface", key = "'getSysInterfaces_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysInterface> getSysInterfaces(int status, int pageIndex, int pageSize, String name) {
        List<SysInterface> sysInterfaces = mapper.getSysInterfaces(status, pageIndex, pageSize, name);
          fillSysInfterface(sysInterfaces);
        return sysInterfaces;
    }

    @Override
    @Cacheable(value = "sysInterface", key = "'getSysInterfaceAll'")
    public List<SysInterface> getSysInterfaceAll() {
        List<SysInterface> sysInterfaces = mapper.getSysInterfaceAll();
        fillSysInfterface(sysInterfaces);
        return sysInterfaces;
    }

    private void fillSysInfterface(List<SysInterface> sysInterfacess) {
        int size = sysInterfacess.size();
        Map<String, SysProject> map = new HashMap<>();
        List<SysProject> sysProjects = dataAccessManager.getSysProjectAll();
        for (int i = 0; i < sysProjects.size(); i++) {
            map.put(sysProjects.get(i).getId(), sysProjects.get(i));
        }
        for (int i = 0; i < size; i++) {
            SysProject sysProject = map.get(sysInterfacess.get(i).getProjectId());
            if (sysProject != null) {
                sysInterfacess.get(i).setProjectName(sysProject.getName());
            }
//            List<SysInterfaceParam> params = dataAccessManager.getSysInterfaceParamByInterfaceId(sysInterfacess.get(i).getId());
//            if (params != null) {
//                int psize = params.size();
//                StringBuffer insb = new StringBuffer();
//                StringBuffer outsb = new StringBuffer();
//                insb.append("?");
//                for (int k = 0; k < psize; k++) {
//                    if (params.get(k).getType() == 0) {
//                        insb.append(params.get(k).getName() + "=" + params.get(k).getDefaultValue() + "&");
//                    } else {
//                        outsb.append(params.get(k).getName() + "|");
//                    }
//                }
//                sysInterfacess.get(i).setInParam(insb.toString());
//                sysInterfacess.get(i).setOutParam(outsb.toString());
//            }
        }
    }

    @Override
    @Cacheable(value = "sysInterface", key = "'getSysInterfaceCount_'+#status+'_'+#name+'_'+#retrieveStatus")
    public int getSysInterfaceCount(int status, String name, int retrieveStatus) {
        return mapper.getSysInterfaceCount(status, name, retrieveStatus);
    }

    @Override
    @CacheEvict(value = "sysInterface", allEntries = true)
    public void changeSysInterface(SysInterface sysInterface) {
        mapper.changeSysInterface(sysInterface);
    }

}
