package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysUserProperties;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysUserPropertiesCache {

    void addSysUserProperties(SysUserProperties sysUserProperties);

    SysUserProperties getSysUserProperties(String id);

    List<SysUserProperties> getSysUserPropertiess(int pageIndex, int pageSize, int status, String name);
    List<SysUserProperties> getSysUserPropertiesAll();
    int getSysUserPropertiesCount(int status, String name);
    void changeSysUserProperties(SysUserProperties sysUserProperties);

}
