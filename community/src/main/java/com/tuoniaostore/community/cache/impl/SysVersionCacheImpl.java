package com.tuoniaostore.community.cache.impl;
        import com.tuoniaostore.community.cache.SysVersionCache;
        import com.tuoniaostore.community.data.SysVersionMapper;
        import com.tuoniaostore.datamodel.user.SysVersion;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysVersion")
public class SysVersionCacheImpl implements SysVersionCache {

    @Autowired
    SysVersionMapper mapper;

    @Override
    @CacheEvict(value = "sysVersion", allEntries = true)
    public void addSysVersion(SysVersion sysVersion) {
        mapper.addSysVersion(sysVersion);
    }

    @Override
    @Cacheable(value = "sysVersion", key = "'getSysVersion_'+#id")
    public   SysVersion getSysVersion(String id) {
        return mapper.getSysVersion(id);
    }

    @Override
    @Cacheable(value = "sysVersion", key = "'getSysVersions_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysVersion> getSysVersions(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSysVersions(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "sysVersion", key = "'getSysVersionAll'")
    public List<SysVersion> getSysVersionAll() {
        return mapper.getSysVersionAll();
    }

    @Override
    @Cacheable(value = "sysVersion", key = "'getSysVersionCount_'+#status+'_'+#name")
    public int getSysVersionCount(int status, String name) {
        return mapper.getSysVersionCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysVersion", allEntries = true)
    public void changeSysVersion(SysVersion sysVersion) {
        mapper.changeSysVersion(sysVersion);
    }

    @Override
    @Cacheable(value = "sysVersion", key = "'getUsefulVersion_'+#deviceType+'_'+#clientType+'_'+#versionIndex")
    public SysVersion getUsefulVersion(Integer deviceType,Integer clientType,Integer versionIndex) {
        return mapper.getUsefulVersion(deviceType,clientType,versionIndex);
    }

}
