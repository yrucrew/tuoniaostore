package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysUserRole;

import java.util.List;
/**
 * 用户与角色对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysUserRoleCache {

    void addSysUserRole(SysUserRole sysUserRole);

    SysUserRole getSysUserRole(String id);

    List<SysUserRole> getSysUserRoles(int pageIndex, int pageSize, int status, String name);
    List<SysUserRole> getSysUserRoleAll();
    int getSysUserRoleCount(int status, String name);
    void changeSysUserRole(SysUserRole sysUserRole);

	List<SysUserRole> getSysUserRolesByRole(String roleId);

	SysUserRole getSysUserRoleByRoleAndUser(String roleId, String userId);

	int deleteSysUserRole(String userId, String userId2);

    SysUserRole getSysUserRoleByUser(String userId);

    void updateSysUserRoleByParam(SysUserRole sysUserRole);
}
