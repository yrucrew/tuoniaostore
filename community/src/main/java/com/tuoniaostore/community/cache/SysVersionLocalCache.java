package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysVersionLocal;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysVersionLocalCache {

    void addSysVersionLocal(SysVersionLocal sysVersionLocal);

    void addSysVersionLocalBatch(List<SysVersionLocal> sysVersionLocal);

    SysVersionLocal getSysVersionLocal(String id);

    List<SysVersionLocal> getSysVersionLocals(int pageIndex, int pageSize, int status, String name);
    List<SysVersionLocal> getSysVersionLocalAll();
    int getSysVersionLocalCount(int status, String name);
    void changeSysVersionLocal(SysVersionLocal sysVersionLocal);

    /**
     * 查询当前用户是否可以更新
     * @param userId
     * @return
     */
    SysVersionLocal getSysVersionLocalByUserId(String userId);

}
