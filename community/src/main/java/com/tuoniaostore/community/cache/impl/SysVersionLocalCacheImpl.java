package com.tuoniaostore.community.cache.impl;
        import com.tuoniaostore.community.cache.SysVersionLocalCache;
        import com.tuoniaostore.community.data.SysVersionLocalMapper;
        import com.tuoniaostore.datamodel.user.SysVersionLocal;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysVersionLocal")
public class SysVersionLocalCacheImpl implements SysVersionLocalCache {

    @Autowired
    SysVersionLocalMapper mapper;

    @Override
    @CacheEvict(value = "sysVersionLocal", allEntries = true)
    public void addSysVersionLocal(SysVersionLocal sysVersionLocal) {
        mapper.addSysVersionLocal(sysVersionLocal);
    }

    @Override
    public void addSysVersionLocalBatch(List<SysVersionLocal> sysVersionLocal) {
        mapper.addSysVersionLocalBatch(sysVersionLocal);
    }

    @Override
    @Cacheable(value = "sysVersionLocal", key = "'getSysVersionLocal_'+#id")
    public   SysVersionLocal getSysVersionLocal(String id) {
        return mapper.getSysVersionLocal(id);
    }

    @Override
    @Cacheable(value = "sysVersionLocal", key = "'getSysVersionLocals_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysVersionLocal> getSysVersionLocals( int status, int pageIndex, int pageSize,String name) {
        return mapper.getSysVersionLocals(status, pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "sysVersionLocal", key = "'getSysVersionLocalAll'")
    public List<SysVersionLocal> getSysVersionLocalAll() {
        return mapper.getSysVersionLocalAll();
    }

    @Override
    @Cacheable(value = "sysVersionLocal", key = "'getSysVersionLocalCount_'+#status+'_'+#name")
    public int getSysVersionLocalCount(int status, String name) {
        return mapper.getSysVersionLocalCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysVersionLocal", allEntries = true)
    public void changeSysVersionLocal(SysVersionLocal sysVersionLocal) {
        mapper.changeSysVersionLocal(sysVersionLocal);
    }


    @Override
    @Cacheable(value = "sysVersionLocal", key = "'getSysVersionLocalByUserId_'+#userId")
    public SysVersionLocal getSysVersionLocalByUserId(String userId) {
        return mapper.getSysVersionLocalByUserId(userId);
    }
}
