package com.tuoniaostore.community.cache.impl;
        import com.tuoniaostore.community.cache.SysUserPropertiesCache;
        import com.tuoniaostore.community.data.SysUserPropertiesMapper;
        import com.tuoniaostore.datamodel.user.SysUserProperties;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysUserProperties")
public class SysUserPropertiesCacheImpl implements SysUserPropertiesCache {

    @Autowired
    SysUserPropertiesMapper mapper;

    @Override
    @CacheEvict(value = "sysUserProperties", allEntries = true)
    public void addSysUserProperties(SysUserProperties sysUserProperties) {
        mapper.addSysUserProperties(sysUserProperties);
    }

    @Override
    @Cacheable(value = "sysUserProperties", key = "'getSysUserProperties_'+#id")
    public   SysUserProperties getSysUserProperties(String id) {
        return mapper.getSysUserProperties(id);
    }

    @Override
    @Cacheable(value = "sysUserProperties", key = "'getSysUserPropertiess_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysUserProperties> getSysUserPropertiess(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getSysUserPropertiess( status,pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "sysUserProperties", key = "'getSysUserPropertiesAll'")
    public List<SysUserProperties> getSysUserPropertiesAll() {
        return mapper.getSysUserPropertiesAll();
    }

    @Override
    @Cacheable(value = "sysUserProperties", key = "'getSysUserPropertiesCount_'+#status+'_'+#name")
    public int getSysUserPropertiesCount(int status, String name) {
        return mapper.getSysUserPropertiesCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysUserProperties", allEntries = true)
    public void changeSysUserProperties(SysUserProperties sysUserProperties) {
        mapper.changeSysUserProperties(sysUserProperties);
    }

}
