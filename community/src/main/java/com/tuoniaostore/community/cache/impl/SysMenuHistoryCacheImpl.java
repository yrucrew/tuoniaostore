package com.tuoniaostore.community.cache.impl;
        import com.tuoniaostore.community.cache.SysMenuHistoryCache;
        import com.tuoniaostore.community.data.SysMenuHistoryMapper;
        import com.tuoniaostore.datamodel.user.SysMenuHistory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysMenuHistory")
public class SysMenuHistoryCacheImpl implements SysMenuHistoryCache {

    @Autowired
    SysMenuHistoryMapper mapper;

    @Override
    @CacheEvict(value = "sysMenuHistory", allEntries = true)
    public void addSysMenuHistory(SysMenuHistory sysMenuHistory) {
        mapper.addSysMenuHistory(sysMenuHistory);
    }

    @Override
    @Cacheable(value = "sysMenuHistory", key = "'getSysMenuHistory_'+#id")
    public   SysMenuHistory getSysMenuHistory(String id) {
        return mapper.getSysMenuHistory(id);
    }

    @Override
    @Cacheable(value = "sysMenuHistory", key = "'getSysMenuHistorys_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysMenuHistory> getSysMenuHistorys( int status,int pageIndex, int pageSize, String name) {
        return mapper.getSysMenuHistorys( status,pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "sysMenuHistory", key = "'getSysMenuHistoryAll'")
    public List<SysMenuHistory> getSysMenuHistoryAll() {
        return mapper.getSysMenuHistoryAll();
    }

    @Override
    @Cacheable(value = "sysMenuHistory", key = "'getSysMenuHistoryCount_'+#status+'_'+#name")
    public int getSysMenuHistoryCount(int status, String name) {
        return mapper.getSysMenuHistoryCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysMenuHistory", allEntries = true)
    public void changeSysMenuHistory(SysMenuHistory sysMenuHistory) {
        mapper.changeSysMenuHistory(sysMenuHistory);
    }

}
