package com.tuoniaostore.community.cache;
import com.tuoniaostore.datamodel.user.SysRoleMenu;
import com.tuoniaostore.datamodel.user.SysRoleMenuPos;

import java.util.List;
/**
 * 角色与菜单对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysRoleMenuCache {

    void addSysRoleMenu(List<SysRoleMenu> list);

    SysRoleMenu getSysRoleMenu(String id);

    List<SysRoleMenu> getSysRoleMenus(int pageIndex, int pageSize, int status, String name);
    List<SysRoleMenu> getSysRoleMenuAll();
    int getSysRoleMenuCount(int status, String name);
    void changeSysRoleMenu(SysRoleMenu sysRoleMenu);
    List<SysRoleMenu> getSysRoleMenusByRoleId(String roleId);

    void delSysRoleMenuByRoleId(String roleId);

    void deleteSysRoleMenu(List<String> menuIds);

}
