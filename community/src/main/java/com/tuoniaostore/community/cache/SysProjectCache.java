package com.tuoniaostore.community.cache;
import com.alibaba.fastjson.JSONArray;
import com.tuoniaostore.datamodel.user.SysProject;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
public interface SysProjectCache {

    void addSysProject(SysProject sysProject);

    SysProject getSysProject(String id);

    List<SysProject> getSysProjects( int pageIndex, int pageSize, String name);
    List<SysProject> getSysProjectAll();
    JSONArray getSysProjectTree();
    int getSysProjectCount( String name);
    void changeSysProject(SysProject sysProject);
    List<SysProject> getSysProjectByParentId(String parentId);

}
