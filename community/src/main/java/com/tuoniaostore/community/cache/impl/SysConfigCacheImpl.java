package com.tuoniaostore.community.cache.impl;
        import com.tuoniaostore.community.cache.SysConfigCache;
        import com.tuoniaostore.community.data.SysConfigMapper;
        import com.tuoniaostore.datamodel.user.SysConfig;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 系统配置信息表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysConfig")
public class SysConfigCacheImpl implements SysConfigCache {

    @Autowired
    SysConfigMapper mapper;

    @Override
    @CacheEvict(value = "sysConfig", allEntries = true)
    public void addSysConfig(SysConfig sysConfig) {
        mapper.addSysConfig(sysConfig);
    }

    @Override
    @Cacheable(value = "sysConfig", key = "'getSysConfig_'+#id")
    public   SysConfig getSysConfig(String id) {
        return mapper.getSysConfig(id);
    }

    @Override
    @Cacheable(value = "sysConfig", key = "'getSysConfigs_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysConfig> getSysConfigs(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSysConfigs(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "sysConfig", key = "'getSysConfigAll'")
    public List<SysConfig> getSysConfigAll() {
        return mapper.getSysConfigAll();
    }

    @Override
    @Cacheable(value = "sysConfig", key = "'getSysConfigCount_'+#status+'_'+#name")
    public int getSysConfigCount(int status, String name) {
        return mapper.getSysConfigCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysConfig", allEntries = true)
    public void changeSysConfig(SysConfig sysConfig) {
        mapper.changeSysConfig(sysConfig);
    }

}
