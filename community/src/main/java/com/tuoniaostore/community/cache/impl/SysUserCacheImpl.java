package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysUserCache;
import com.tuoniaostore.community.data.SysUserMapper;
import com.tuoniaostore.community.vo.PurchaseUserVo;
import com.tuoniaostore.datamodel.user.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysUser")
public class SysUserCacheImpl implements SysUserCache {

    @Autowired
    SysUserMapper mapper;

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUser", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void addSysUser(SysUser sysUser) {
        mapper.addSysUser(sysUser);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUser", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void addSysUserForPos(SysUser sysUser) {
        mapper.addSysUserForPos(sysUser);
    }

    @Override
    @Cacheable(value = "sysUser", key = "'getSysUser_'+#id")
    public SysUser getSysUser(String id) {
        return mapper.getSysUser(id);
    }

    @Override
    @Cacheable(value = "sysUser_sysTerminal_sysUserTerminal", key = "'getSysUsers_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysUser> getSysUsers(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSysUsers(status, pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "sysUser", key = "'getSysUserAll'")
    public List<SysUser> getSysUserAll() {
        return mapper.getSysUserAll();
    }

    @Override
    @Cacheable(value = "sysUser", key = "'getSysUserByName_'+#name")
    public List<SysUser> getSysUserByName(String name) {
        return mapper.getSysUserByName(name);
    }

    @Override
    @Cacheable(value = "sysUser", key = "'getSysUserCount_'+#status+'_'+#name")
    public int getSysUserCount(int status, String name) {
        return mapper.getSysUserCount(status, name);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUser", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void changeSysUser(SysUser sysUser) {
        mapper.changeSysUser(sysUser);
    }

    @Override
    @Cacheable(value = "sysUser", key = "'getDefaultNameOrPhone_'+#name")
    public List<SysUser> getDefaultNameOrPhone(String name) {
        return mapper.getDefaultNameOrPhone(name);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUser", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void changeLogin(SysUser sysUser) {
        mapper.changeLogin(sysUser);
    }

    @Override
    @Cacheable(value = "sysUser", key = "'getSysUserByPhoneNumber'+#phoneNumber")
    public SysUser getSysUserByPhoneNumber(String phoneNumber) {
        return mapper.getSysUserByPhoneNumber(phoneNumber);
    }

    @Override
    @CacheEvict(value = "sysUser", allEntries = true)
    public void updatePasswordByPhoneNum(String phoneNum, String password) {
        mapper.updatePasswordByPhoneNum(phoneNum, password);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUser", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void updatePasswordById(String id, String password) {
        mapper.updatePasswordById(id, password);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUser", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void changeSysUserPhoneNum(String id, String phoneNum) {
        mapper.changeSysUserPhoneNum(id, phoneNum);
    }

    @Override
    @Cacheable(value = "sysUser", key = "'getSysUserInIds'+#ids")
    public List<SysUser> getSysUserInIds(List<String> ids) {
        return mapper.getSysUserInIds(ids);
    }

    @Override
	@Cacheable(value = "sysUser",
		key = "'getPurchaseUserList_' + #searchKey + '_' + #roleId + '_' + #pageStartIndex + '_' + #pageSize")
    public List<PurchaseUserVo> getPurchaseUserList(String searchKey, List<String> roleIds, int pageStartIndex, int pageSize) {
        return mapper.getPurchaseUserList(searchKey, roleIds, pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "sysUser",
            key = "'getPurchaseUserListCount_' + #searchKey + '_' + #roleId")
    public int getPurchaseUserListCount(String searchKey, List<String> roleIds) {
        return mapper.getPurchaseUserListCount(searchKey, roleIds);
    }

    @Override
    @Cacheable(value = "sysUser",
            key = "'getSysUsersForSearch_' + #column + '_' + #value")
    public List<SysUser> getSysUsersForSearch(String column, Object value) {
        return mapper.getSysUsersForSearch(column, value);
    }

    @Override
    @Cacheable(value = "sysUser", key = "'getSysUserByParam_' + #map + '_' + #pageStartIndex+'_'+#pageSize")
    public List<SysUser> getSysUserByParam(Map<String, Object> map, Integer pageStartIndex, Integer pageSize) {
        return mapper.getSysUserByParam(map, pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "sysUser", key = "'getCountSysUserByParam_' + #map ")
    public int getCountSysUserByParam(Map<String, Object> map) {
        return mapper.getCountSysUserByParam(map);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUser", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void delSysUserByUserId(String id) {
        mapper.delSysUserByUserId(id);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUser", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void updateSysUserParam(SysUser sysUser) {
        mapper.updateSysUserParam(sysUser);
    }

    @Override
    @Cacheable(value = "sysUser", key = "'findSysUserByPhoneOrDefaultName_' + #phoneNumber+'_'+#defaultName")
    public SysUser findSysUserByPhoneOrDefaultName(String phoneNumber, String defaultName) {
        return mapper.findSysUserByPhoneOrDefaultName(phoneNumber, defaultName);
    }

    @Override
    @Cacheable(value = "sysUser", key = "'getSysUserAccountOrName_' + #accountOrName")
    public List<SysUser> getSysUserAccountOrName(String accountOrName) {
        return mapper.getSysUserAccountOrName(accountOrName);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUser", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public int updateSysUserByPosByRemote(SysUser sysUser) {
        return mapper.updateSysUserByPos(sysUser);
    }

    @Override
    public List<SysUser> getAllUserByRoleIds(String name, int status, List<String> roleIds) {
        return mapper.getAllUserByRoleIds(name, status, roleIds);
    }

    @Override
    @Cacheable(value = "sysUser", key = "'getSysUserByPhoneNumLike_' + #phoneNum")
    public List<SysUser> getSysUserByPhoneNumLike(String phoneNum) {
        return mapper.getSysUserByPhoneNumLike(phoneNum);
    }

}
