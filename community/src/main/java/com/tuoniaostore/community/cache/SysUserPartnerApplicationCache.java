package com.tuoniaostore.community.cache;

import com.tuoniaostore.datamodel.user.SysUserPartnerApplication;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public interface SysUserPartnerApplicationCache {

    void addSysUserPartnerApplication(SysUserPartnerApplication sysUserPartnerApplication);

    SysUserPartnerApplication getSysUserPartnerApplication(String id);

    List<SysUserPartnerApplication> getSysUserPartnerApplications(int pageIndex, int pageSize, int status, String name);

    List<SysUserPartnerApplication> getSysUserPartnerApplicationAll();

    int getSysUserPartnerApplicationCount(int status, String name);

    void changeSysUserPartnerApplication(SysUserPartnerApplication sysUserPartnerApplication);

    SysUserPartnerApplication getSysUserPartnerApplicationAppKey(String appKey);
}
