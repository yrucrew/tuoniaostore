package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysUserTerminalCache;
import com.tuoniaostore.community.data.SysUserTerminalMapper;
import com.tuoniaostore.datamodel.user.SysUserTerminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysUserTerminal")
public class SysUserTerminalCacheImpl implements SysUserTerminalCache {

    @Autowired
    SysUserTerminalMapper mapper;

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUserTerminal", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })

    @Async
    public void addSysUserTerminal(SysUserTerminal sysUserTerminal) {
        mapper.addSysUserTerminal(sysUserTerminal);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUserTerminal", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void addSysUserTerminals(List<SysUserTerminal> list) {
        mapper.addSysUserTerminals(list);
    }

    @Override
    @Cacheable(value = "sysUserTerminal", key = "'getSysUserTerminal_'+#id")
    public SysUserTerminal getSysUserTerminal(String id) {
        return mapper.getSysUserTerminal(id);
    }

    @Override
    @Cacheable(value = "sysUserTerminal", key = "'getSysUserTerminals_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysUserTerminal> getSysUserTerminals(int status, int pageIndex, int pageSize, String name) {
        return mapper.getSysUserTerminals(status, pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "sysUserTerminal", key = "'getSysUserTerminalAll'")
    public List<SysUserTerminal> getSysUserTerminalAll() {
        return mapper.getSysUserTerminalAll();
    }

    @Override
    @Cacheable(value = "sysUserTerminal", key = "'getSysUserTerminalCount_'+#status+'_'+#name")
    public int getSysUserTerminalCount(int status, String name) {
        return mapper.getSysUserTerminalCount(status, name);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUserTerminal", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void changeSysUserTerminal(SysUserTerminal sysUserTerminal) {
        mapper.changeSysUserTerminal(sysUserTerminal);
    }

    @Override
    @Cacheable(value = "sysUserTerminal", key = "'getSysUserTerminal_'+#id+'_'+#userId")
    public SysUserTerminal getSysUserTerminal2(String id, String userId) {
        return mapper.getSysUserTerminal2(id, userId);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysUserTerminal", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void delSysUserTerminal(String userId) {
        mapper.delSysUserTerminal(userId);
    }
}
