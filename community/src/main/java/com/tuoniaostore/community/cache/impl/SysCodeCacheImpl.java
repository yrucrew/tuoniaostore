package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysCodeCache;
import com.tuoniaostore.community.data.SysCodeMapper;
import com.tuoniaostore.datamodel.user.SysCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysCode")
public class SysCodeCacheImpl implements SysCodeCache {

    @Autowired
    SysCodeMapper mapper;

    @Override
    @CacheEvict(value = "sysCode", allEntries = true)
    public void addSysCode(SysCode sysCode) {
        mapper.addSysCode(sysCode);
    }

    @Override
    @Cacheable(value = "sysCode", key = "'getSysCode_'+#id")
    public SysCode getSysCode(String id) {
        return mapper.getSysCode(id);
    }

    @Override
    @Cacheable(value = "sysCode", key = "'getSysCodes_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysCode> getSysCodes(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getSysCodes( status,pageIndex, pageSize, name);
    }

    @Override
    @Cacheable(value = "sysCode", key = "'getSysCodeAll'")
    public List<SysCode> getSysCodeAll() {
        return mapper.getSysCodeAll();
    }

    @Override
    @Cacheable(value = "sysCode", key = "'getSysCodeCount_'+#status+'_'+#name")
    public int getSysCodeCount(int status, String name) {
        return mapper.getSysCodeCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysCode", allEntries = true)
    public void changeSysCode(SysCode sysCode) {
        mapper.changeSysCode(sysCode);
    }


    @Override
    @Cacheable(value = "sysCode", key = "'getSysCodeName_'+#codeName")
    public SysCode getSysCodeName(String codeName) {
        return mapper.getSysCodeName(codeName);
    }

}
