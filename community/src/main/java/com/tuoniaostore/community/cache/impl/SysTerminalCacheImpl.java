package com.tuoniaostore.community.cache.impl;

import com.tuoniaostore.community.cache.SysTerminalCache;
import com.tuoniaostore.community.data.SysTerminalMapper;
import com.tuoniaostore.datamodel.user.SysTerminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysTerminal")
public class SysTerminalCacheImpl implements SysTerminalCache {

    @Autowired
    SysTerminalMapper mapper;

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysTerminal", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void addSysTerminal(SysTerminal sysTerminal) {
        mapper.addSysTerminal(sysTerminal);
    }

    @Override
    @Cacheable(value = "sysTerminal", key = "'getSysTerminal_'+#id")
    public SysTerminal getSysTerminal(String id) {
        return mapper.getSysTerminal(id);
    }

    @Override
    @Cacheable(value = "sysTerminal", key = "'getSysTerminals_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#title+'_'+#retrieveStatus")
    public List<SysTerminal> getSysTerminals(int status, int pageIndex, int pageSize, String title, int retrieveStatus) {
        return mapper.getSysTerminals(status, pageIndex, pageSize, title, retrieveStatus);
    }

    @Override
    @Cacheable(value = "sysTerminal", key = "'getSysTerminalAll'")
    public List<SysTerminal> getSysTerminalAll() {
        return mapper.getSysTerminalAll();
    }

    @Override
    @Cacheable(value = "sysTerminal", key = "'getSysTerminalCount_'+#status+'_'+#title+'_'+#retrieveStatus")
    public int getSysTerminalCount(int status, String title, int retrieveStatus) {
        return mapper.getSysTerminalCount(status, title, retrieveStatus);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysTerminal", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void changeSysTerminal(SysTerminal sysTerminal) {
        mapper.changeSysTerminal(sysTerminal);
    }

    @Override
    @Cacheable(value = "sysTerminal", key = "'getSysTerminalByIdAndUserId_'+#id+'_'+#userId")
    public SysTerminal getSysTerminalByIdAndUserId(String id, String userId) {
        return mapper.getSysTerminalByIdAndUserId(id, userId);
    }

    @Override
    @Cacheable(value = "sysTerminal", key = "'getSysUserTerminalListByUserId_'+#id")
    public List<SysTerminal> getSysUserTerminalListByUserId(String id) {
        return mapper.getSysUserTerminalListByUserId(id);
    }

    @Override
    @Cacheable(value = "sysTerminal", key = "'getSysTerminalTitleByList_'+#terminalIds")
    public String getSysTerminalTitleByList(List<String> terminalIds) {
        return mapper.getSysTerminalTitleByList(terminalIds);
    }

    @Override
    @Cacheable(value = "sysTerminal", key = "'findSysUserByTerminalName_'+#terminalName")
    public SysTerminal findSysUserByTerminalName(String terminalName) {
        return mapper.findSysUserByTerminalName(terminalName);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "sysTerminal", allEntries = true),
            @CacheEvict(value = "sysUser_sysTerminal_sysUserTerminal", allEntries = true)
    })
    public void batchDeleteSysTerminal(List<String> ids) {
        mapper.batchDeleteSysTerminal(ids);
    }

}
