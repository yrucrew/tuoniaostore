package com.tuoniaostore.community.cache;

import com.tuoniaostore.datamodel.user.SysCity;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
public interface SysCityCache {

    void addSysCity(SysCity sysCity);

    SysCity getSysCity(String id);

    List<SysCity> getSysCitys(int status, int pageIndex, int pageSize, String name, int retrieveStatus);

    List<SysCity> getSysCityAll();

    int getSysCityCount(int status, String name, int retrieveStatus);

    void changeSysCity(SysCity sysCity);

    List<SysCity> getSysProvinceAll(int di, int level);

    List<SysCity> getSysCityByIds(List<String> ids);

    List<SysCity> getSysCityByShengLevel(int di, int level);

    List<SysCity> getSysCityByShengDi(int sheng, int di);

    List<SysCity> getSysProvincesCheckBind(String areaId);

    List<SysCity> getSysProvincesUnBind();
}
