package com.tuoniaostore.community.cache.impl;
        import com.tuoniaostore.commons.constant.StatusEnum;
        import com.tuoniaostore.community.cache.SysAreaCache;
        import com.tuoniaostore.community.data.SysAreaMapper;
        import com.tuoniaostore.datamodel.user.SysArea;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import org.springframework.transaction.annotation.Transactional;

        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
@Component
@CacheConfig(cacheNames = "sysArea")
public class SysAreaCacheImpl implements SysAreaCache {

    @Autowired
    SysAreaMapper mapper;

    @Override
    @CacheEvict(value = "sysArea", allEntries = true)
    public void addSysArea(SysArea sysArea) {
        mapper.addSysArea(sysArea);
    }

    @Override
    @Cacheable(value = "sysArea", key = "'getSysArea_'+#id")
    public   SysArea getSysArea(String id) {
        return mapper.getSysArea(id);
    }

    @Override
    @Cacheable(value = "sysArea", key = "'getSysAreas_'+#pageIndex+'_'+#pageSize+'_'+#title+'_'+#status")
    public List<SysArea> getSysAreas(int pageIndex, int pageSize, String title, int status) {
        return mapper.getSysAreas(pageIndex, pageSize, title, status);
    }

    @Override
    @Cacheable(value = "sysArea", key = "'getSysAreaAll'")
    public List<SysArea> getSysAreaAll() {
        return mapper.getSysAreaAll();
    }

    @Override
    @Cacheable(value = "sysArea", key = "'getSysAreaCount_'+#name")
    public int getSysAreaCount(String name) {
        return mapper.getSysAreaCount(name);
    }

    @Override
    @CacheEvict(value = "sysArea", allEntries = true)
    public void changeSysArea(SysArea sysArea) {
        mapper.changeSysArea(sysArea);
    }

    @Override
    @Cacheable(value = "sysArea", key = "'getSysAreasByPage_'+#areaName+'_'+#pageStartIndex+'_'+#pageStartIndex")
    public List<SysArea> getSysAreasByPage(String areaName,Integer pageStartIndex, Integer pageSize) {
        return mapper.getSysAreasByPage(areaName,pageStartIndex,pageSize);
    }

    @Override
    @Cacheable(value = "sysArea", key = "'getSysAreasByPageCount_'+#areaName")
    public int getSysAreasByPageCount(String areaName) {
        return mapper.getSysAreasByPageCount(areaName);
    }

    @Override
    @Cacheable(value = "sysArea", key = "'getSysAreasByIds_'+#ids+'_'+#pageStartIndex+'_'+#pageStartIndex")
    public List<SysArea> getSysAreasByIds(List<String> ids, int pageStartIndex, int pageSize) {
        return mapper.getSysAreasByIds(ids,pageStartIndex,pageSize);
    }

    @Override
    @Cacheable(value = "sysArea", key = "'getSysAreasByNameLike_'+#areaName+'_'+#pageStartIndex+'_'+#pageStartIndex")
    public List<SysArea> getSysAreasByNameLike(String areaName, Integer pageStartIndex, Integer pageSize) {
        return mapper.getSysAreasByNameLike(areaName,pageStartIndex,pageSize);
    }

    @Override
    @Cacheable(value = "sysArea", key = "'getSysAreasByTitle_'+#title")
    public SysArea getSysAreasByTitle(String title) {
        return mapper.getSysAreasByTitle(title);
    }

    @Override
    @CacheEvict(value = "sysArea", allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void deleteSysAreas(List<String> ids) {
        mapper.deleteSysAreas(ids, StatusEnum.DISABLE.getKey());
    }
}
