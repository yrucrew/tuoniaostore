package com.tuoniaostore.community.cache.impl;
        import com.tuoniaostore.community.cache.SysRetrieveCache;
        import com.tuoniaostore.community.data.SysRetrieveMapper;
        import com.tuoniaostore.datamodel.user.SysRetrieve;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@Component
@CacheConfig(cacheNames = "sysRetrieve")
public class SysRetrieveCacheImpl implements SysRetrieveCache {

    @Autowired
    SysRetrieveMapper mapper;

    @Override
    @CacheEvict(value = "sysRetrieve", allEntries = true)
    public void addSysRetrieve(SysRetrieve sysRetrieve) {
        mapper.addSysRetrieve(sysRetrieve);
    }

    @Override
    @Cacheable(value = "sysRetrieve", key = "'getSysRetrieve_'+#id")
    public   SysRetrieve getSysRetrieve(String id) {
        return mapper.getSysRetrieve(id);
    }

    @Override
    @Cacheable(value = "sysRetrieve", key = "'getSysRetrieves_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<SysRetrieve> getSysRetrieves(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getSysRetrieves(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "sysRetrieve", key = "'getSysRetrieveAll'")
    public List<SysRetrieve> getSysRetrieveAll() {
        return mapper.getSysRetrieveAll();
    }

    @Override
    @Cacheable(value = "sysRetrieve", key = "'getSysRetrieveCount_'+#status+'_'+#name")
    public int getSysRetrieveCount(int status, String name) {
        return mapper.getSysRetrieveCount(status, name);
    }

    @Override
    @CacheEvict(value = "sysRetrieve", allEntries = true)
    public void changeSysRetrieve(SysRetrieve sysRetrieve) {
        mapper.changeSysRetrieve(sysRetrieve);
    }

    @Override
    @CacheEvict(value = "sysRetrieve", allEntries = true)
    public void changeRetrieveStatus(String tableName, int retrieveStatus, String id) {
        mapper.changeRetrieveStatus(tableName,retrieveStatus,id);
    }

    @Override
    @CacheEvict(value = "sysRetrieve", allEntries = true)
    public void addSysRetrieves(List<SysRetrieve> sysRetrieve) {
        mapper.addSysRetrieves(sysRetrieve);
    }
}
