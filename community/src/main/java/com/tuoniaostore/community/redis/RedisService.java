package com.tuoniaostore.community.redis;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/16
 */
public interface RedisService {

    /**
     * 往redis里面存放短信验证码 设置过期时间
     * @return
     */
    void setMessageCode(String phoneNum,String code);

    /**
     * 取出对应手机号码的验证码
     * @param phoneNum
     * @return
     */
    String getMessageCode(String phoneNum);

}
