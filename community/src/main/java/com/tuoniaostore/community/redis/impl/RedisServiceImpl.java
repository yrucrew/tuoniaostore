package com.tuoniaostore.community.redis.impl;

import com.tuoniaostore.community.redis.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/16
 */
@Component
public class RedisServiceImpl implements RedisService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void setMessageCode(String phoneNum,String code){
        stringRedisTemplate.opsForValue().set(phoneNum, code,60*11, TimeUnit.SECONDS);
    }

    @Override
    public String getMessageCode(String phoneNum) {
        return stringRedisTemplate.opsForValue().get(phoneNum);
    }

}
