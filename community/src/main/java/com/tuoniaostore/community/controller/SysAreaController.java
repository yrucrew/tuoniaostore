package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysAreaService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysArea")
public class SysAreaController {
    @Autowired
    private SysAreaService sysAreaService;

    @RequestMapping("addSysArea")
    @ResponseBody
    public JSONObject addSysArea() {
        return sysAreaService.addSysArea();
    }

    @RequestMapping("validateSysArea")
    @ResponseBody
    public JSONObject validateSysArea() {
        return sysAreaService.validateSysArea();
    }


    @RequestMapping("getSysArea")
    @ResponseBody
    public JSONObject getSysArea() {
        return sysAreaService.getSysArea();
    }

    @RequestMapping("getSysAreas")
    @ResponseBody
    public JSONObject getSysAreas() {
        return sysAreaService.getSysAreas();
    }

    @ApiOperation(value = "删除区域")
    @RequestMapping(value = "deleteSysAreas")
    @ResponseBody
    public JSONObject deleteSysAreas() {
        return sysAreaService.deleteSysAreas();
    }

    /**
     * 选择包含省份
     * @return
     */
    @RequestMapping("getSysSelArea")
    @ResponseBody
    public JSONObject getSysSelArea() {
        return sysAreaService.getSysSelArea();
    }

    @RequestMapping("getSysAreaAll")
    @ResponseBody
    public JSONObject getSysAreaAll() {
        return sysAreaService.getSysAreaAll();
    }

    /**
     * 根据地区名 模糊搜索 分页
     * @author oy
     * @date 2019/4/27
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSysAreaAllByPage")
    @ResponseBody
    public JSONObject getSysAreaAllByPage() {
        return sysAreaService.getSysAreaAllByPage();
    }

    @RequestMapping("getSysAreaCount")
    @ResponseBody
    public JSONObject getSysAreaCount() {
        return sysAreaService.getSysAreaCount();
    }

    @RequestMapping("changeSysArea")
    @ResponseBody
    public JSONObject changeSysArea() {
        return sysAreaService.changeSysArea();
    }

    @RequestMapping("getSysAreasByIds")
    @ResponseBody
    public JSONObject getSysAreasByIds() {
        return sysAreaService.getSysAreasByIds();
    }

    @RequestMapping("getSysAreasByNameLike")
    @ResponseBody
    public JSONObject getSysAreasByNameLike() {
        return sysAreaService.getSysAreasByNameLike();
    }


}
