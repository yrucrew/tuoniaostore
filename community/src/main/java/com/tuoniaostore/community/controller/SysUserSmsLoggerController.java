package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysUserSmsLoggerService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysUserSmsLogger")
public class SysUserSmsLoggerController {
    @Autowired
    private SysUserSmsLoggerService sysUserSmsLoggerService;


    @RequestMapping("addSysUserSmsLogger")
    @ResponseBody
    public JSONObject addSysUserSmsLogger() {
        return sysUserSmsLoggerService.addSysUserSmsLogger();
    }

    @RequestMapping("getSysUserSmsLogger")
    @ResponseBody
    public JSONObject getSysUserSmsLogger() {
        return sysUserSmsLoggerService.getSysUserSmsLogger();
    }


    @RequestMapping("getSysUserSmsLoggers")
    @ResponseBody
    public JSONObject getSysUserSmsLoggers() {
        return sysUserSmsLoggerService.getSysUserSmsLoggers();
    }

    @RequestMapping("getSysUserSmsLoggerAll")
    @ResponseBody
    public JSONObject getSysUserSmsLoggerAll() {
        return sysUserSmsLoggerService.getSysUserSmsLoggerAll();
    }

    @RequestMapping("getSysUserSmsLoggerCount")
    @ResponseBody
    public JSONObject getSysUserSmsLoggerCount() {
        return sysUserSmsLoggerService.getSysUserSmsLoggerCount();
    }

    @RequestMapping("changeSysUserSmsLogger")
    @ResponseBody
    public JSONObject changeSysUserSmsLogger() {
        return sysUserSmsLoggerService.changeSysUserSmsLogger();
    }

    /**
     * 通过手机号码获取短信记录
     * @return
     */

    @RequestMapping("getSysUserSmsLoggerByPhone")
    @ResponseBody
    public JSONObject getSysUserSmsLoggerByPhone() {
        return sysUserSmsLoggerService.getSysUserSmsLoggerByPhone();
    }

}
