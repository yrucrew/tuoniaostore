package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysAreaProvinceService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-19 19:25:50
 */
@Controller
@RequestMapping("sysAreaProvince")
public class SysAreaProvinceController {
    @Autowired
    private SysAreaProvinceService sysAreaProvinceService;


    @RequestMapping("addSysAreaProvince")
    @ResponseBody
    public JSONObject addSysAreaProvince() {
        return sysAreaProvinceService.addSysAreaProvince();
    }


    @RequestMapping("getSysAreaProvince")
    @ResponseBody
    public JSONObject getSysAreaProvince() {
        return sysAreaProvinceService.getSysAreaProvince();
    }


    @RequestMapping("getSysAreaProvinces")
    @ResponseBody
    public JSONObject getSysAreaProvinces() {
        return sysAreaProvinceService.getSysAreaProvinces();
    }


    @RequestMapping("getSysAreaProvinceAll")
    @ResponseBody
    public JSONObject getSysAreaProvinceAll() {
        return sysAreaProvinceService.getSysAreaProvinceAll();
    }


    @RequestMapping("getSysAreaProvinceCount")
    @ResponseBody
    public JSONObject getSysAreaProvinceCount() {
        return sysAreaProvinceService.getSysAreaProvinceCount();
    }


    @RequestMapping("changeSysAreaProvince")
    @ResponseBody
    public JSONObject changeSysAreaProvince() {
        return sysAreaProvinceService.changeSysAreaProvince();
    }

}
