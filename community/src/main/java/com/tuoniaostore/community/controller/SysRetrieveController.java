package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysRetrieveService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysRetrieve")
public class SysRetrieveController {
    @Autowired
    private SysRetrieveService sysRetrieveService;


    @RequestMapping("addSysRetrieve")
    @ResponseBody
    public JSONObject addSysRetrieve() {
        return sysRetrieveService.addSysRetrieve();
    }

    @RequestMapping("addSysRetrieveForRemote")
    @ResponseBody
    public JSONObject addSysRetrieveForRemote() {
        return sysRetrieveService.addSysRetrieveForRemote();
    }

    @RequestMapping("addSysRetrievesForRemote")
    @ResponseBody
    public JSONObject addSysRetrievesForRemote() {
        return sysRetrieveService.addSysRetrievesForRemote();
    }

    @RequestMapping("getSysRetrieve")
    @ResponseBody
    public JSONObject getSysRetrieve() {
        return sysRetrieveService.getSysRetrieve();
    }


    @RequestMapping("getSysRetrieves")
    @ResponseBody
    public JSONObject getSysRetrieves() {
        return sysRetrieveService.getSysRetrieves();
    }


    @RequestMapping("getSysRetrieveAll")
    @ResponseBody
    public JSONObject getSysRetrieveAll() {
        return sysRetrieveService.getSysRetrieveAll();
    }


    @RequestMapping("getSysRetrieveCount")
    @ResponseBody
    public JSONObject getSysRetrieveCount() {
        return sysRetrieveService.getSysRetrieveCount();
    }

    @RequestMapping("changeSysRetrieve")
    @ResponseBody
    public JSONObject changeSysRetrieve() {
        return sysRetrieveService.changeSysRetrieve();
    }

}
