package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysUserTerminalService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysUserTerminal")
public class SysUserTerminalController {
    @Autowired
    private SysUserTerminalService sysUserTerminalService;


    @RequestMapping("addSysUserTerminal")
    @ResponseBody
    public JSONObject addSysUserTerminal() {
        return sysUserTerminalService.addSysUserTerminal();
    }

    /**
     * 远程调价绑定终端
     * @author oy
     * @date 2019/5/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("addSysUserTerminalByRemote")
    @ResponseBody
    public JSONObject addSysUserTerminalByRemote() {
        return sysUserTerminalService.addSysUserTerminalByRemote();
    }


    @RequestMapping("getSysUserTerminal")
    @ResponseBody
    public JSONObject getSysUserTerminal() {
        return sysUserTerminalService.getSysUserTerminal();
    }


    @RequestMapping("getSysUserTerminals")
    @ResponseBody
    public JSONObject getSysUserTerminals() {
        return sysUserTerminalService.getSysUserTerminals();
    }

    @RequestMapping("getSysUserTerminalAll")
    @ResponseBody
    public JSONObject getSysUserTerminalAll() {
        return sysUserTerminalService.getSysUserTerminalAll();
    }

    @RequestMapping("getSysUserTerminalCount")
    @ResponseBody
    public JSONObject getSysUserTerminalCount() {
        return sysUserTerminalService.getSysUserTerminalCount();
    }

    @RequestMapping("changeSysUserTerminal")
    @ResponseBody
    public JSONObject changeSysUserTerminal() {
        return sysUserTerminalService.changeSysUserTerminal();
    }

}
