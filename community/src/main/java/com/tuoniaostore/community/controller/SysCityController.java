package com.tuoniaostore.community.controller;

import com.tuoniaostore.community.service.SysCityService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-19 15:41:13
 */
@Controller
@RequestMapping("sysCity")
public class SysCityController {
    @Autowired
    private SysCityService sysCityService;


    @RequestMapping("addSysCity")
    @ResponseBody
    public JSONObject addSysCity() {
        return sysCityService.addSysCity();
    }

    @RequestMapping("getSysCity")
    @ResponseBody
    public JSONObject getSysCity() {
        return sysCityService.getSysCity();
    }

    @RequestMapping("getSysCitys")
    @ResponseBody
    public JSONObject getSysCitys() {
        return sysCityService.getSysCitys();
    }


    @RequestMapping("getSysCityAll")
    @ResponseBody
    public JSONObject getSysCityAll() {
        return sysCityService.getSysCityAll();
    }


    @RequestMapping("getSysCityByIds")
    @ResponseBody
    public JSONObject getSysCityByIds() {
        return sysCityService.getSysCityByIds();
    }


    @RequestMapping("getSysProvinceAll")
    @ResponseBody
    public JSONObject getSysProvinceAll() {
        return sysCityService.getSysProvinceAll();
    }

    @ApiOperation(value = "获取未与区域绑定的省")
    @RequestMapping("getSysProvincesUnBind")
    @ResponseBody
    public JSONObject getSysProvincesUnBind() {
        return sysCityService.getSysProvincesUnBind();
    }

    @ApiOperation(value = "获取所有省", notes = "若入参areaId则筛选出关联的省")
    @RequestMapping("getSysProvincesCheckBind")
    @ResponseBody
    public JSONObject getSysProvincesCheckBind() {
        return sysCityService.getSysProvincesCheckBind();
    }

    @RequestMapping("getSysCityCount")
    @ResponseBody
    public JSONObject getSysCityCount() {
        return sysCityService.getSysCityCount();
    }


    @RequestMapping("changeSysCity")
    @ResponseBody
    public JSONObject changeSysCity() {
        return sysCityService.changeSysCity();
    }


    @RequestMapping("getSysCityByShengLevel")
    @ResponseBody
    public JSONObject getSysCityByShengLevel() {
        return sysCityService.getSysCityByShengLevel();
    }

    @RequestMapping("getSysCityByShengDi")
    @ResponseBody
    public JSONObject getSysCityByShengDi() {
        return sysCityService.getSysCityByShengDi();
    }

}
