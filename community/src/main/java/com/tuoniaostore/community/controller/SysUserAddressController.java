package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysUserAddressService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysUserAddress")
public class SysUserAddressController {
    @Autowired
    private SysUserAddressService sysUserAddressService;


    @RequestMapping("addSysUserAddress")
    @ResponseBody
    public JSONObject addSysUserAddress() {
        return sysUserAddressService.addSysUserAddress();
    }

    @RequestMapping("getSysUserAddress")
    @ResponseBody
    public JSONObject getSysUserAddress() {
        return sysUserAddressService.getSysUserAddress();
    }

    @RequestMapping("getSysUserAddresss")
    @ResponseBody
    public JSONObject getSysUserAddresss() {
        return sysUserAddressService.getSysUserAddresss();
    }


    @RequestMapping("getSysUserAddressAll")
    @ResponseBody
    public JSONObject getSysUserAddressAll() {
        return sysUserAddressService.getSysUserAddressAll();
    }

    @RequestMapping("getSysUserAddressCount")
    @ResponseBody
    public JSONObject getSysUserAddressCount() {
        return sysUserAddressService.getSysUserAddressCount();
    }

    @RequestMapping("changeSysUserAddress")
    @ResponseBody
    public JSONObject changeSysUserAddress() {
        return sysUserAddressService.changeSysUserAddress();
    }

    /**
     *  获取我的收货地址
     * @return
     */
    @RequestMapping("getMyAddress")
    @ResponseBody
    public JSONObject getMyAddress() {
        return sysUserAddressService.getMyAddress();
    }


    /**
     *  新增我的收货地址
     * @return
     */
    @RequestMapping(value = "addMyAddress")
    @ResponseBody
    public JSONObject addMyAddress() {
        return sysUserAddressService.addMyAddress();
    }

    /**
     *  修改我的收货地址
     * @return
     */
    @RequestMapping(value = "changeMyAddress")
    @ResponseBody
    public JSONObject changeMyAddress() {
        return sysUserAddressService.changeMyAddress();
    }

    /**
     *  修改我的默认收货地址
     * @return
     */

    @RequestMapping(value = "changeMyDefaultAddress")
    @ResponseBody
    public JSONObject changeMyDefaultAddress() {
        return sysUserAddressService.changeMyDefaultAddress();
    }

    /**
     *  删除我的收货地址
     * @return
     */
    @RequestMapping(value = "deleteMyAddress")
    @ResponseBody
    public JSONObject deleteMyAddress() {
        return sysUserAddressService.deleteMyAddress();
    }

    /**
     * 商家端：查询当前用户默认地址
     * @author sqd
     * @date 2019/4/9
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping(value = "getGoodShopCartByDefault")
    @ResponseBody
    public JSONObject getGoodShopCartByDefault() {
        return sysUserAddressService.getGoodShopCartByDefault();
    }


    /**
     *  获取我的默认收货地址
     * @return
     */
    @RequestMapping(value = "getDefaultSysUserAddress")
    @ResponseBody
    public JSONObject getDefaultSysUserAddress() {
        return sysUserAddressService.getDefaultSysUserAddress();
    }


}
