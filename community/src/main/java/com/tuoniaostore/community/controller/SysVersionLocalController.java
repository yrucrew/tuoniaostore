package com.tuoniaostore.community.controller;

import com.tuoniaostore.community.service.SysVersionLocalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysVersionLocal")
public class SysVersionLocalController {
    @Autowired
    private SysVersionLocalService sysVersionLocalService;

    @RequestMapping("addSysVersionLocal")
    @ResponseBody
    public JSONObject addSysVersionLocal() {
        return sysVersionLocalService.addSysVersionLocal();
    }

    @RequestMapping("getSysVersionLocal")
    @ResponseBody
    public JSONObject getSysVersionLocal() {
        return sysVersionLocalService.getSysVersionLocal();
    }

    @RequestMapping("getSysVersionLocals")
    @ResponseBody
    public JSONObject getSysVersionLocals() {
        return sysVersionLocalService.getSysVersionLocals();
    }

    @RequestMapping("getSysVersionLocalAll")
    @ResponseBody
    public JSONObject getSysVersionLocalAll() {
        return sysVersionLocalService.getSysVersionLocalAll();
    }

    @RequestMapping("getSysVersionLocalCount")
    @ResponseBody
    public JSONObject getSysVersionLocalCount() {
        return sysVersionLocalService.getSysVersionLocalCount();
    }

    @RequestMapping("changeSysVersionLocal")
    @ResponseBody
    public JSONObject changeSysVersionLocal() {
        return sysVersionLocalService.changeSysVersionLocal();
    }

}
