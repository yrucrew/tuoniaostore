package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysUserPropertiesService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:41
 */
@Controller
@RequestMapping("sysUserProperties")
public class SysUserPropertiesController {
    @Autowired
    private SysUserPropertiesService sysUserPropertiesService;


    @RequestMapping("addSysUserProperties")
    @ResponseBody
    public JSONObject addSysUserProperties() {
        return sysUserPropertiesService.addSysUserProperties();
    }

    @RequestMapping("getSysUserProperties")
    @ResponseBody
    public JSONObject getSysUserProperties() {
        return sysUserPropertiesService.getSysUserProperties();
    }


    @RequestMapping("getSysUserPropertiess")
    @ResponseBody
    public JSONObject getSysUserPropertiess() {
        return sysUserPropertiesService.getSysUserPropertiess();
    }

    @RequestMapping("getSysUserPropertiesAll")
    @ResponseBody
    public JSONObject getSysUserPropertiesAll() {
        return sysUserPropertiesService.getSysUserPropertiesAll();
    }

    @RequestMapping("getSysUserPropertiesCount")
    @ResponseBody
    public JSONObject getSysUserPropertiesCount() {
        return sysUserPropertiesService.getSysUserPropertiesCount();
    }

    @RequestMapping("changeSysUserProperties")
    @ResponseBody
    public JSONObject changeSysUserProperties() {
        return sysUserPropertiesService.changeSysUserProperties();
    }

}
