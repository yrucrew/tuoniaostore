package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysLogService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestBody;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysLog")
public class SysLogController {
    @Autowired
    private SysLogService sysLogService;


    @RequestMapping("addSysLog")
    @ResponseBody
    public JSONObject addSysLog() {
        return sysLogService.addSysLog();
    }

    @RequestMapping("getSysLog")
    @ResponseBody
    public JSONObject getSysLog() {
        return sysLogService.getSysLog();
    }


    @RequestMapping("getSysLogs")
    @ResponseBody
    public JSONObject getSysLogs() {
        return sysLogService.getSysLogs();
    }


    @RequestMapping("getSysLogAll")
    @ResponseBody
    public JSONObject getSysLogAll() {
        return sysLogService.getSysLogAll();
    }


    @RequestMapping("getSysLogCount")
    @ResponseBody
    public JSONObject getSysLogCount() {
        return sysLogService.getSysLogCount();
    }


    @RequestMapping("changeSysLog")
    @ResponseBody
    public JSONObject changeSysLog() {
        return sysLogService.changeSysLog();
    }

}
