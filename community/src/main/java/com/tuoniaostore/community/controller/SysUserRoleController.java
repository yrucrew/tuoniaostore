package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysUserRoleService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 用户与角色对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysUserRole")
public class SysUserRoleController {
    @Autowired
    private SysUserRoleService sysUserRoleService;


    @RequestMapping("addSysUserRole")
    @ResponseBody
    public JSONObject addSysUserRole() {
        return sysUserRoleService.addSysUserRole();
    }

    @RequestMapping("getSysUserRole")
    @ResponseBody
    public JSONObject getSysUserRole() {
        return sysUserRoleService.getSysUserRole();
    }


    @RequestMapping("getSysUserRoles")
    @ResponseBody
    public JSONObject getSysUserRoles() {
        return sysUserRoleService.getSysUserRoles();
    }


    @RequestMapping("getSysUserRoleAll")
    @ResponseBody
    public JSONObject getSysUserRoleAll() {
        return sysUserRoleService.getSysUserRoleAll();
    }

    @RequestMapping("getSysUserRoleCount")
    @ResponseBody
    public JSONObject getSysUserRoleCount() {
        return sysUserRoleService.getSysUserRoleCount();
    }

    @RequestMapping("changeSysUserRole")
    @ResponseBody
    public JSONObject changeSysUserRole() {
        return sysUserRoleService.changeSysUserRole();
    }

}
