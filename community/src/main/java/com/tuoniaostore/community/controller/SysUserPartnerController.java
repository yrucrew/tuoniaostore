package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysUserPartnerService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysUserPartner")
public class SysUserPartnerController {
    @Autowired
    private SysUserPartnerService sysUserPartnerService;


    @RequestMapping("addSysUserPartner")
    @ResponseBody
    public JSONObject addSysUserPartner() {
        return sysUserPartnerService.addSysUserPartner();
    }

    @RequestMapping("getSysUserPartner")
    @ResponseBody
    public JSONObject getSysUserPartner() {
        return sysUserPartnerService.getSysUserPartner();
    }


    @RequestMapping("getSysUserPartners")
    @ResponseBody
    public JSONObject getSysUserPartners() {
        return sysUserPartnerService.getSysUserPartners();
    }

    @RequestMapping("getSysUserPartnerAll")
    @ResponseBody
    public JSONObject getSysUserPartnerAll() {
        return sysUserPartnerService.getSysUserPartnerAll();
    }

    @RequestMapping("getSysUserPartnerCount")
    @ResponseBody
    public JSONObject getSysUserPartnerCount() {
        return sysUserPartnerService.getSysUserPartnerCount();
    }

    @RequestMapping("changeSysUserPartner")
    @ResponseBody
    public JSONObject changeSysUserPartner() {
        return sysUserPartnerService.changeSysUserPartner();
    }

}
