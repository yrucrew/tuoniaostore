package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysUserChannelService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 合作渠道，用于区分来自不同渠道的用户数据
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysUserChannel")
public class SysUserChannelController {
    @Autowired
    private SysUserChannelService sysUserChannelService;


    @RequestMapping("addSysUserChannel")
    @ResponseBody
    public JSONObject addSysUserChannel() {
        return sysUserChannelService.addSysUserChannel();
    }

    @RequestMapping("getSysUserChannel")
    @ResponseBody
    public JSONObject getSysUserChannel() {
        return sysUserChannelService.getSysUserChannel();
    }


    @RequestMapping("getSysUserChannels")
    @ResponseBody
    public JSONObject getSysUserChannels() {
        return sysUserChannelService.getSysUserChannels();
    }

    @RequestMapping("getSysUserChannelAll")
    @ResponseBody
    public JSONObject getSysUserChannelAll() {
        return sysUserChannelService.getSysUserChannelAll();
    }

    @RequestMapping("getSysUserChannelCount")
    @ResponseBody
    public JSONObject getSysUserChannelCount() {
        return sysUserChannelService.getSysUserChannelCount();
    }

    @RequestMapping("changeSysUserChannel")
    @ResponseBody
    public JSONObject changeSysUserChannel() {
        return sysUserChannelService.changeSysUserChannel();
    }

}
