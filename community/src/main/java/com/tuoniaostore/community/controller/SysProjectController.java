package com.tuoniaostore.community.controller;

import com.tuoniaostore.community.service.SysProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
@Controller
@RequestMapping("sysProject")
public class SysProjectController {
    @Autowired
    private SysProjectService sysProjectService;


    @RequestMapping("addSysProject")
    @ResponseBody
    public JSONObject addSysProject() {
        return sysProjectService.addSysProject();
    }

    @RequestMapping("getSysProject")
    @ResponseBody
    public JSONObject getSysProject() {
        return sysProjectService.getSysProject();
    }


    @RequestMapping("getSysProjects")
    @ResponseBody
    public JSONObject getSysProjects() {
        return sysProjectService.getSysProjects();
    }

    @RequestMapping("getSysProjectAll")
    @ResponseBody
    public JSONObject getSysProjectAll() {
        return sysProjectService.getSysProjectAll();
    }

    @RequestMapping("getSysProjectCount")
    @ResponseBody
    public JSONObject getSysProjectCount() {
        return sysProjectService.getSysProjectCount();
    }

    @RequestMapping("changeSysProject")
    @ResponseBody
    public JSONObject changeSysProject() {
        return sysProjectService.changeSysProject();
    }

    @RequestMapping("getSysProjectByParentId")
    @ResponseBody
    public JSONObject getSysProjectByParentId() {
        return sysProjectService.getSysProjectByParentId();
    }

    @RequestMapping("getSysProjectTree")
    @ResponseBody
    public JSONObject getSysProjectTree() {
        return sysProjectService.getSysProjectTree();
    }

}
