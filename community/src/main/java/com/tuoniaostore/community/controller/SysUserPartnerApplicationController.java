package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysUserPartnerApplicationService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysUserPartnerApplication")
public class SysUserPartnerApplicationController {
    @Autowired
    private SysUserPartnerApplicationService sysUserPartnerApplicationService;

    @RequestMapping("addSysUserPartnerApplication")
    @ResponseBody
    public JSONObject addSysUserPartnerApplication() {
        return sysUserPartnerApplicationService.addSysUserPartnerApplication();
    }

    @RequestMapping("getSysUserPartnerApplication")
    @ResponseBody
    public JSONObject getSysUserPartnerApplication() {
        return sysUserPartnerApplicationService.getSysUserPartnerApplication();
    }


    @RequestMapping("getSysUserPartnerApplications")
    @ResponseBody
    public JSONObject getSysUserPartnerApplications() {
        return sysUserPartnerApplicationService.getSysUserPartnerApplications();
    }

    @RequestMapping("getSysUserPartnerApplicationAll")
    @ResponseBody
    public JSONObject getSysUserPartnerApplicationAll() {
        return sysUserPartnerApplicationService.getSysUserPartnerApplicationAll();
    }

    @RequestMapping("getSysUserPartnerApplicationCount")
    @ResponseBody
    public JSONObject getSysUserPartnerApplicationCount() {
        return sysUserPartnerApplicationService.getSysUserPartnerApplicationCount();
    }

    @RequestMapping("changeSysUserPartnerApplication")
    @ResponseBody
    public JSONObject changeSysUserPartnerApplication() {
        return sysUserPartnerApplicationService.changeSysUserPartnerApplication();
    }

    @RequestMapping("getSysUserPartnerApplicationAppKey")
    @ResponseBody
    public JSONObject getSysUserPartnerApplicationAppKey() {
        return sysUserPartnerApplicationService.getSysUserPartnerApplicationAppKey();
    }

}
