package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysVersionService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysVersion")
public class SysVersionController {
    @Autowired
    private SysVersionService sysVersionService;


    @RequestMapping("addSysVersion")
    @ResponseBody
    public JSONObject addSysVersion() {
        return sysVersionService.addSysVersion();
    }

    @RequestMapping("getSysVersion")
    @ResponseBody
    public JSONObject getSysVersion() {
        return sysVersionService.getSysVersion();
    }


    @RequestMapping("getSysVersions")
    @ResponseBody
    public JSONObject getSysVersions() {
        return sysVersionService.getSysVersions();
    }

    @RequestMapping("getSysVersionAll")
    @ResponseBody
    public JSONObject getSysVersionAll() {
        return sysVersionService.getSysVersionAll();
    }

    @RequestMapping("getSysVersionCount")
    @ResponseBody
    public JSONObject getSysVersionCount() {
        return sysVersionService.getSysVersionCount();
    }

    @RequestMapping("changeSysVersion")
    @ResponseBody
    public JSONObject changeSysVersion() {
        return sysVersionService.changeSysVersion();
    }


    @RequestMapping("getVersionControl")
    @ResponseBody
    public JSONObject getVersionControl(){
        return sysVersionService.getVersionControl();
    }

}
