package com.tuoniaostore.community.controller;

import com.tuoniaostore.community.service.SysInterfaceParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
@Controller
@RequestMapping("sysInterfaceParam")
public class SysInterfaceParamController {
    @Autowired
    private SysInterfaceParamService sysInterfaceParamService;

    @RequestMapping("getSysInterfaceParam")
    @ResponseBody
    public JSONObject getSysInterfaceParam() {
        return sysInterfaceParamService.getSysInterfaceParam();
    }


    @RequestMapping("getSysInterfaceParams")
    @ResponseBody
    public JSONObject getSysInterfaceParams() {
        return sysInterfaceParamService.getSysInterfaceParams();
    }

    @RequestMapping("getSysInterfaceParamAll")
    @ResponseBody
    public JSONObject getSysInterfaceParamAll() {
        return sysInterfaceParamService.getSysInterfaceParamAll();
    }

    @RequestMapping("getSysInterfaceParamByInterfaceId")
    @ResponseBody
    public JSONObject getSysInterfaceParamByInterfaceId() {
        return sysInterfaceParamService.getSysInterfaceParamByInterfaceId();
    }

    @RequestMapping("getSysInterfaceParamCount")
    @ResponseBody
    public JSONObject getSysInterfaceParamCount() {
        return sysInterfaceParamService.getSysInterfaceParamCount();
    }

    @RequestMapping("changeSysInterfaceParam")
    @ResponseBody
    public JSONObject changeSysInterfaceParam() {
        return sysInterfaceParamService.changeSysInterfaceParam();
    }


}
