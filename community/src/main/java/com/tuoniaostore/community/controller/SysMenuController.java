package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysMenuService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 菜单管理
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysMenu")
public class SysMenuController {
    @Autowired
    private SysMenuService service;

    @RequestMapping("addSysMenu")
    @ResponseBody
    public JSONObject addSysMenu() {
        return service.addSysMenu();
    }


    @RequestMapping("getSysMenu")
    @ResponseBody
    public JSONObject getSysMenu() {
        return service.getSysMenu();
    }


    @RequestMapping("getSysMenus")
    @ResponseBody
    public JSONObject getSysMenus() {
        return service.getSysMenus();
    }


    @RequestMapping("getSysMenuAll")
    @ResponseBody
    public JSONObject getSysMenuAll() {
        return service.getSysMenuAll();
    }


    @RequestMapping("getSysMenuCount")
    @ResponseBody
    public JSONObject getSysMenuCount() {
        return service.getSysMenuCount();
    }


    @RequestMapping("changeSysMenu")
    @ResponseBody
    public JSONObject changeSysMenu() {
        return service.changeSysMenu();
    }


    @RequestMapping("getWebSysMenus")
    @ResponseBody
    public JSONObject getWebSysMenus() {
        return service.getWebSysMenus();
    }


    @RequestMapping("getPurviewMenus")
    @ResponseBody
    public JSONObject getPurviewMenus() {
        return service.getPurviewMenus();
    }


    @RequestMapping("getLevelMenus")
    @ResponseBody
    public JSONObject getLevelMenus() {
        return service.getLevelMenus();
    }


    @RequestMapping("changeSysMenuRetrieve")
    @ResponseBody
    public JSONObject changeSysMenuRetrieve() {
        return service.changeSysMenuRetrieve();
    }

    /**
     * 删除菜单
     * @author oy
     * @date 2019/5/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("deleteSysMenu")
    @ResponseBody
    public JSONObject deleteSysMenu() {
        return service.deleteSysMenu();
    }


}
