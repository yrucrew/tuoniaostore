package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysMenuHistoryService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysMenuHistory")
public class SysMenuHistoryController {
    @Autowired
    private SysMenuHistoryService sysMenuHistoryService;


    @RequestMapping("addSysMenuHistory")
    @ResponseBody
    public JSONObject addSysMenuHistory() {
        return sysMenuHistoryService.addSysMenuHistory();
    }


    @RequestMapping("getSysMenuHistory")
    @ResponseBody
    public JSONObject getSysMenuHistory() {
        return sysMenuHistoryService.getSysMenuHistory();
    }


    @RequestMapping("getSysMenuHistorys")
    @ResponseBody
    public JSONObject getSysMenuHistorys() {
        return sysMenuHistoryService.getSysMenuHistorys();
    }


    @RequestMapping("getSysMenuHistoryAll")
    @ResponseBody
    public JSONObject getSysMenuHistoryAll() {
        return sysMenuHistoryService.getSysMenuHistoryAll();
    }


    @RequestMapping("getSysMenuHistoryCount")
    @ResponseBody
    public JSONObject getSysMenuHistoryCount() {
        return sysMenuHistoryService.getSysMenuHistoryCount();
    }


    @RequestMapping("changeSysMenuHistory")
    @ResponseBody
    public JSONObject changeSysMenuHistory() {
        return sysMenuHistoryService.changeSysMenuHistory();
    }

}
