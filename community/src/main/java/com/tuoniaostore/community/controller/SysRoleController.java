package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysRoleService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 角色
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysRole")
public class SysRoleController {
    @Autowired
    private SysRoleService sysRoleService;


    @RequestMapping("addSysRole")
    @ResponseBody
    public JSONObject addSysRole() {
        return sysRoleService.addSysRole();
    }


    @RequestMapping("getSysRole")
    @ResponseBody
    public JSONObject getSysRole() {
        return sysRoleService.getSysRole();
    }


    @RequestMapping("getSysRoles")
    @ResponseBody
    public JSONObject getSysRoles() {
        return sysRoleService.getSysRoles();
    }

    @RequestMapping("getSysRoleAll")
    @ResponseBody
    public JSONObject getSysRoleAll() {
        return sysRoleService.getSysRoleAll();
    }

    @RequestMapping("getSysRoleCount")
    @ResponseBody
    public JSONObject getSysRoleCount() {
        return sysRoleService.getSysRoleCount();
    }

    @RequestMapping("changeSysRole")
    @ResponseBody
    public JSONObject changeSysRole() {
        return sysRoleService.changeSysRole();
    }

    @RequestMapping("changeSysRoleRetrieve")
    @ResponseBody
    public JSONObject changeSysRoleRetrieve() {
        return sysRoleService.changeSysRoleRetrieve();
    }

    @RequestMapping("deleteSysRole")
    @ResponseBody
    public JSONObject deleteSysRole() {
        return sysRoleService.deleteSysRole();
    }

    @RequestMapping("validateSysRole")
    @ResponseBody
    public JSONObject validateSysRole() {
        return sysRoleService.validateSysRole();
    }

}
