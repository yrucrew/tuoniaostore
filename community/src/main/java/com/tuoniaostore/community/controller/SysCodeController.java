package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysCodeService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysCode")
public class SysCodeController {
    @Autowired
    private SysCodeService sysCodeService;


    @RequestMapping("addSysCode")
    @ResponseBody
    public JSONObject addSysCode() {
        return sysCodeService.addSysCode();
    }


    @RequestMapping("getSysCode")
    @ResponseBody
    public JSONObject getSysCode() {
        return sysCodeService.getSysCode();
    }


    @RequestMapping("getSysCodes")
    @ResponseBody
    public JSONObject getSysCodes() {
        return sysCodeService.getSysCodes();
    }


    @RequestMapping("getSysCodeAll")
    @ResponseBody
    public JSONObject getSysCodeAll() {
        return sysCodeService.getSysCodeAll();
    }


    @RequestMapping("getSysCodeCount")
    @ResponseBody
    public JSONObject getSysCodeCount() {
        return sysCodeService.getSysCodeCount();
    }


    @RequestMapping("changeSysCode")
    @ResponseBody
    public JSONObject changeSysCode() {
        return sysCodeService.changeSysCode();
    }

}
