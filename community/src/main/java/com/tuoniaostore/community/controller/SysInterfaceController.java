package com.tuoniaostore.community.controller;

import com.tuoniaostore.community.service.SysInterfaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
@Controller
@RequestMapping("sysInterface")
public class SysInterfaceController {
    @Autowired
    private SysInterfaceService sysInterfaceService;


    @RequestMapping("addSysInterface")
    @ResponseBody
    public JSONObject addSysInterface() {
        return sysInterfaceService.addSysInterface();
    }

    @RequestMapping("getSysInterface")
    @ResponseBody
    public JSONObject getSysInterface() {
        return sysInterfaceService.getSysInterface();
    }


    @RequestMapping("getSysInterfaces")
    @ResponseBody
    public JSONObject getSysInterfaces() {
        return sysInterfaceService.getSysInterfaces();
    }

    @RequestMapping("getSysInterfaceAll")
    @ResponseBody
    public JSONObject getSysInterfaceAll() {
        return sysInterfaceService.getSysInterfaceAll();
    }

    @RequestMapping("getSysInterfaceCount")
    @ResponseBody
    public JSONObject getSysInterfaceCount() {
        return sysInterfaceService.getSysInterfaceCount();
    }

    @RequestMapping("changeSysInterface")
    @ResponseBody
    public JSONObject changeSysInterface() {
        return sysInterfaceService.changeSysInterface();
    }

    @RequestMapping("changeSysInterfaceParam")
    @ResponseBody
    public JSONObject changeSysInterfaceParam() {
        return sysInterfaceService.changeSysInterfaceParam();
    }

    @RequestMapping("getSysInterfaceTree")
    @ResponseBody
    public JSONObject getSysInterfaceTree() {
        return sysInterfaceService.getSysInterfaceTree();
    }

}
