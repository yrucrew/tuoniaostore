package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysTerminalService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysTerminal")
public class SysTerminalController {
    @Autowired
    private SysTerminalService sysTerminalService;


    @RequestMapping("addSysTerminal")
    @ResponseBody
    public JSONObject addSysTerminal() {
        return sysTerminalService.addSysTerminal();
    }


    @RequestMapping("getSysTerminal")
    @ResponseBody
    public JSONObject getSysTerminal() {
        return sysTerminalService.getSysTerminal();
    }


    @RequestMapping("getSysTerminals")
    @ResponseBody
    public JSONObject getSysTerminals() {
        return sysTerminalService.getSysTerminals();
    }


    @RequestMapping("getSysTerminalAll")
    @ResponseBody
    public JSONObject getSysTerminalAll() {
        return sysTerminalService.getSysTerminalAll();
    }

    @RequestMapping("getSysTerminalCount")
    @ResponseBody
    public JSONObject getSysTerminalCount() {
        return sysTerminalService.getSysTerminalCount();
    }

    @RequestMapping("changeSysTerminal")
    @ResponseBody
    public JSONObject changeSysTerminal() {
        return sysTerminalService.changeSysTerminal();
    }

    /**
     * 验证终端是否存在
     * @author oy
     * @date 2019/5/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("validateSysTerminal")
    @ResponseBody
    public JSONObject validateSysTerminal() {
        return sysTerminalService.validateSysTerminal();
    }

    /**
     * 批量删除终端
     * @author oy
     * @date 2019/5/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("batchDeleteSysTerminal")
    @ResponseBody
    public JSONObject batchDeleteSysTerminal() {
        return sysTerminalService.batchDeleteSysTerminal();
    }



}
