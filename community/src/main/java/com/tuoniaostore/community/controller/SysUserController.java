package com.tuoniaostore.community.controller;

import com.tuoniaostore.community.service.SysUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:41
 */
@Controller
@RequestMapping("sysUser")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    @RequestMapping(value = "addSysUser")
    @ResponseBody
    public JSONObject addSysUser() {
        return sysUserService.addSysUser();
    }

    /**
     * 添加用户 远程调用
     * @author oy
     * @date 2019/5/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping(value = "addSysUserByRemote")
    @ResponseBody
    public JSONObject addSysUserByRemote() {
        return sysUserService.addSysUserByRemote();
    }

    @ApiOperation(value = "远程调用-pos机添加用户")
    @RequestMapping(value = "addSysUserForPosByRemote")
    @ResponseBody
    public JSONObject addSysUserForPosByRemote() {
        return sysUserService.addSysUserForPosByRemote();
    }

    @RequestMapping("getSysUser")
    @ResponseBody
    public JSONObject getSysUser() {
        return sysUserService.getSysUser();
    }

    @RequestMapping("getCurrentSysUser")
    @ResponseBody
    public JSONObject getCurrentSysUser() {
        return sysUserService.getCurrentSysUser();
    }

    @RequestMapping("getSysUsers")
    @ResponseBody
    public JSONObject getSysUsers() {
        return sysUserService.getSysUsers();
    }

    @ApiOperation("根据角色id获取用户列表")
    @RequestMapping("getAllUserByRoleIds")
    @ResponseBody
    public JSONObject getAllUserByRoleIds() {
        return sysUserService.getAllUserByRoleIds();
    }

    @RequestMapping("getSysUsers2")
    @ResponseBody
    public JSONObject getSysUsers2() {
        return sysUserService.getSysUsers2();
    }

    @RequestMapping("getSysUserAll")
    @ResponseBody
    public JSONObject getSysUserAll() {
        return sysUserService.getSysUserAll();
    }


    @RequestMapping("getSysUserCount")
    @ResponseBody
    public JSONObject getSysUserCount() {
        return sysUserService.getSysUserCount();
    }

    @ApiOperation("根据名称获取所有用户列表")
    @RequestMapping("getSysUserByName")
    @ResponseBody
    public JSONObject getSysUserByName() {
        return sysUserService.getSysUserByName();
    }

    @RequestMapping("changeSysUser")
    @ResponseBody
    public JSONObject changeSysUser() {
        return sysUserService.changeSysUser();
    }

    /**
     * 修改用户
     * @author oy
     * @date 2019/5/11
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("changeSysUser2")
    @ResponseBody
    public JSONObject changeSysUser2() {
        return sysUserService.changeSysUser2();
    }

    /**
     * 我的：修改密码
     *
     * @return
     */
    @RequestMapping("changeSysUserLoginPassword")
    @ResponseBody
    public JSONObject changeSysUserLoginPassword() {
        return sysUserService.changeSysUserLoginPassword();
    }

    /**
     * 我的：重绑手机 验证码
     *
     * @return
     */
    @RequestMapping("changeSysUserPhoneNumGetMsg")
    @ResponseBody
    public JSONObject changeSysUserPhoneNumGetMsg() {
        return sysUserService.changeSysUserPhoneNumGetMsg();
    }

    /**
     * 我的：重绑手机
     *
     * @return
     */
    @RequestMapping("changeSysUserPhoneNum")
    @ResponseBody
    public JSONObject changeSysUserPhoneNum() {
        return sysUserService.changeSysUserPhoneNum();
    }

    @RequestMapping("getSysUserInIds")
    @ResponseBody
    public JSONObject getSysUserInIds() {
        return sysUserService.getSysUserInIds();
    }

    /**
     * 后台系统：采购用户列表
     * @return
     */
    @RequestMapping("getPurchaseUserList")
    @ResponseBody
    public JSONObject getPurchaseUserList() {
        return sysUserService.getPurchaseUserList();
    }
    
    @ApiOperation("后台系统：采购员绑定上下级关系列表")
    @RequestMapping("purchaseBindLeaderList")
    @ResponseBody
    public JSONObject purchaseBindLeaderList() {
        return sysUserService.purchaseBindLeaderList();
    }
    
    @ApiOperation("后台系统：添加/修改上级")
    @RequestMapping("userBindLeader")
    @ResponseBody
    public JSONObject userBindLeader() {
        return sysUserService.userBindLeader();
    }
    
    /**
     * 后台系统：解绑上级
     * @return
     */
    @RequestMapping("userUnbindLeader")
    @ResponseBody
    public JSONObject userUnbindLeader() {
        return sysUserService.userUnbindLeader();
    }
    
    /**
     * 根据SysUser单个字段搜索
     * @return
     */
    @RequestMapping("getSysUsersForSearch")
    @ResponseBody
    public JSONObject getSysUsersForSearch() {
        return sysUserService.getSysUsersForSearch();
    }


    /**
     * 仓库管理员添加员工账号
     * @author oy
     * @date 2019/4/22
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("addSysUsersForBasicWorkerByLeader")
    @ResponseBody
    public JSONObject addSysUsersForBasicWorkerByLeader() {
        return sysUserService.addSysUsersForBasicWorkerByLeader();
    }

    /**
     * 后台修改密码 密码 登录账号 登录密码
     * @author oy
     * @date 2019/4/25
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("changeSysUserPassword")
    @ResponseBody
    public JSONObject changeSysUserPassword() {
        return sysUserService.changeSysUserPassword();
    }

    /**
     * 通过手机号码查找对应的用户
     * @author oy
     * @date 2019/5/9
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getSysUserByPhoneNumForRemote")
    @ResponseBody
    public JSONObject getSysUserByPhoneNumForRemote() {
        return sysUserService.getSysUserByPhoneNumForRemote();
    }

    /**
     * 删除用户
     * @author oy
     * @date 2019/5/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("delSysUserByUserId")
    @ResponseBody
    public JSONObject delSysUserByUserId() {
        return sysUserService.delSysUserByUserId();
    }

    /**
     * 验证用户是否存在
     * @author oy
     * @date 2019/5/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("validateSysUserByPhoneNumberOrDefaultName")
    @ResponseBody
    public JSONObject validateSysUserByPhoneNumberOrDefaultName() {
        return sysUserService.validateSysUserByPhoneNumberOrDefaultName();
    }

    @RequestMapping("getSysUserAccountOrName")
    @ResponseBody
    public JSONObject getSysUserAccountOrName() {
        return sysUserService.getSysUserAccountOrName();
    }

   /* @RequestMapping("getSysUserByPhoneNumLike")
    @ResponseBody
    public JSONObject getSysUserByPhoneNumLike() {
        return sysUserService.getSysUserByPhoneNumLike();
    }
*/
    @RequestMapping("updateSysUserByPosByRemote")
    @ResponseBody
    public JSONObject updateSysUserByPosByRemote() {
        return sysUserService.updateSysUserByPosByRemote();
    }

    @RequestMapping("getSysUserByPhoneNumLike")
    @ResponseBody
    public JSONObject getSysUserByPhoneNumLike() {
        return sysUserService.getSysUserByPhoneNumLike();
    }


/*    @RequestMapping("addPosWorktest")
    @ResponseBody
    public JSONObject addPosWorktest() {
        return sysUserService.addPosWorktest();
    }*/

}
