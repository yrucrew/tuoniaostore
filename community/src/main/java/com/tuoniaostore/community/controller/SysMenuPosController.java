package com.tuoniaostore.community.controller;

import com.tuoniaostore.community.service.SysMenuPosService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.RestController;


/**
 * 菜单管理
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
@RestController
@RequestMapping("sysMenuPos")
public class SysMenuPosController {
    @Autowired
    private SysMenuPosService sysMenuPosService;


    @ApiOperation(value = "pos-添加菜单")
    @RequestMapping("addSysMenuPos")
    public JSONObject addSysMenuPos() {
        return sysMenuPosService.addSysMenuPos();
    }

    @ApiOperation(value = "pos-批量删除菜单")
    @RequestMapping("delSysMenuPosByIds")
    public JSONObject delSysMenuPosByIds() {
        return sysMenuPosService.delSysMenuPosByIds();
    }

    @RequestMapping("getSysMenuPos")
    public JSONObject getSysMenuPos() {
        return sysMenuPosService.getSysMenuPos();
    }


    @RequestMapping("getSysMenuPoss")
    public JSONObject getSysMenuPoss() {
        return sysMenuPosService.getSysMenuPoss();
    }

    @RequestMapping("getSysMenuPosAll")
    public JSONObject getSysMenuPosAll() {
        return sysMenuPosService.getSysMenuPosAll();
    }

    @RequestMapping("getSysMenuPosCount")
    public JSONObject getSysMenuPosCount() {
        return sysMenuPosService.getSysMenuPosCount();
    }

    @ApiOperation("pos-编辑菜单")
    @RequestMapping("changeSysMenuPos")
    public JSONObject changeSysMenuPos() {
        return sysMenuPosService.changeSysMenuPos();
    }

    /**
     * 通过角色获取菜单
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/18
     */
    @RequestMapping("getSysMenuPosByRoleId")
    public JSONObject getSysMenuPosByRoleId() {
        return sysMenuPosService.getSysMenuPosByRoleId();
    }

    /**
     * 根据POS用户 获取对应的菜单
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/20
     */
    @RequestMapping("getSysMenuPosByUserId")
    public JSONObject getSysMenuPosByUserId() {
        return sysMenuPosService.getSysMenuPosByUserId();
    }

}
