package com.tuoniaostore.community.controller;

import com.tuoniaostore.community.service.SysRolePosService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.RestController;


/**
 * 角色
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
@RestController
@RequestMapping("sysRolePos")
public class SysRolePosController {
    @Autowired
    private SysRolePosService sysRolePosService;

    @ApiOperation(value = "pos-添加角色")
    @RequestMapping("addSysRolePos")
    public JSONObject addSysRolePos() {
        return sysRolePosService.addSysRolePos();
    }

    @ApiOperation(value = "pos-删除角色")
    @RequestMapping("delSysRolePosByIds")
    public JSONObject delSysRolePosByIds() {
        return sysRolePosService.delSysRolePosByIds();
    }

    @RequestMapping("getSysRolePos")
    public JSONObject getSysRolePos() {
        return sysRolePosService.getSysRolePos();
    }

    @RequestMapping("getSysRolePoss")
    public JSONObject getSysRolePoss() {
        return sysRolePosService.getSysRolePoss();
    }

    @RequestMapping("getSysRolePosAll")
    public JSONObject getSysRolePosAll() {
        return sysRolePosService.getSysRolePosAll();
    }

    @RequestMapping("getSysRolePosCount")
    public JSONObject getSysRolePosCount() {
        return sysRolePosService.getSysRolePosCount();
    }

    @ApiOperation("pos-编辑角色")
    @RequestMapping("changeSysRolePos")
    public JSONObject changeSysRolePos() {
        return sysRolePosService.changeSysRolePos();
    }

}
