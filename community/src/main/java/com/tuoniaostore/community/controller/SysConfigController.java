package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysConfigService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 系统配置信息表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysConfig")
public class SysConfigController {
    @Autowired
    private SysConfigService sysConfigService;


    @RequestMapping("addSysConfig")
    @ResponseBody
    public JSONObject addSysConfig() {
        return sysConfigService.addSysConfig();
    }


    @RequestMapping("getSysConfig")
    @ResponseBody
    public JSONObject getSysConfig() {
        return sysConfigService.getSysConfig();
    }


    @RequestMapping("getSysConfigs")
    @ResponseBody
    public JSONObject getSysConfigs() {
        return sysConfigService.getSysConfigs();
    }


    @RequestMapping("getSysConfigAll")
    @ResponseBody
    public JSONObject getSysConfigAll() {
        return sysConfigService.getSysConfigAll();
    }


    @RequestMapping("getSysConfigCount")
    @ResponseBody
    public JSONObject getSysConfigCount() {
        return sysConfigService.getSysConfigCount();
    }


    @RequestMapping("changeSysConfig")
    @ResponseBody
    public JSONObject changeSysConfig() {
        return sysConfigService.changeSysConfig();
    }

}
