package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysUserRolePosService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 用户与角色对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
@Controller
@RequestMapping("sysUserRolePos")
public class SysUserRolePosController {
    @Autowired
    private SysUserRolePosService sysUserRolePosService;


    @RequestMapping("addSysUserRolePos")
    @ResponseBody
    public JSONObject addSysUserRolePos() {
        return sysUserRolePosService.addSysUserRolePos();
    }

    @RequestMapping("getSysUserRolePos")
    @ResponseBody
    public JSONObject getSysUserRolePos() {
        return sysUserRolePosService.getSysUserRolePos();
    }


    @RequestMapping("getSysUserRolePoss")
    @ResponseBody
    public JSONObject getSysUserRolePoss() {
        return sysUserRolePosService.getSysUserRolePoss();
    }

    @RequestMapping("getSysUserRolePosAll")
    @ResponseBody
    public JSONObject getSysUserRolePosAll() {
        return sysUserRolePosService.getSysUserRolePosAll();
    }

    @RequestMapping("getSysUserRolePosCount")
    @ResponseBody
    public JSONObject getSysUserRolePosCount() {
        return sysUserRolePosService.getSysUserRolePosCount();
    }

    @RequestMapping("changeSysUserRolePos")
    @ResponseBody
    public JSONObject changeSysUserRolePos() {
        return sysUserRolePosService.changeSysUserRolePos();
    }

}
