package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysRoleMenuService;
        import io.swagger.annotations.Api;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 角色与菜单对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:29:42
 */
@Controller
@RequestMapping("sysRoleMenu")
public class SysRoleMenuController {
    @Autowired
    private SysRoleMenuService sysRoleMenuService;


    @RequestMapping("addSysRoleMenu")
    @ResponseBody
    public JSONObject addSysRoleMenu() {
        return sysRoleMenuService.addSysRoleMenu();
    }


    @RequestMapping("getSysRoleMenu")
    @ResponseBody
    public JSONObject getSysRoleMenu() {
        return sysRoleMenuService.getSysRoleMenu();
    }


    @RequestMapping("getSysRoleMenus")
    @ResponseBody
    public JSONObject getSysRoleMenus() {
        return sysRoleMenuService.getSysRoleMenus();
    }


    @RequestMapping("getSysRoleMenuAll")
    @ResponseBody
    public JSONObject getSysRoleMenuAll() {
        return sysRoleMenuService.getSysRoleMenuAll();
    }


    @RequestMapping("getSysRoleMenuCount")
    @ResponseBody
    public JSONObject getSysRoleMenuCount() {
        return sysRoleMenuService.getSysRoleMenuCount();
    }


    @RequestMapping("changeSysRoleMenu")
    @ResponseBody
    public JSONObject changeSysRoleMenu() {
        return sysRoleMenuService.changeSysRoleMenu();
    }

}
