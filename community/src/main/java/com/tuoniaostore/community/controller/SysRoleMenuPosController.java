package com.tuoniaostore.community.controller;
        import com.tuoniaostore.community.service.SysRoleMenuPosService;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;
        import org.springframework.web.bind.annotation.RestController;


/**
 * 角色与菜单对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
@RestController
@RequestMapping("sysRoleMenuPos")
public class SysRoleMenuPosController {
    @Autowired
    private SysRoleMenuPosService sysRoleMenuPosService;

    @RequestMapping("addSysRoleMenuPos")
    public JSONObject addSysRoleMenuPos() {
        return sysRoleMenuPosService.addSysRoleMenuPos();
    }

    @RequestMapping("getSysRoleMenuPos")
    public JSONObject getSysRoleMenuPos() {
        return sysRoleMenuPosService.getSysRoleMenuPos();
    }

    @RequestMapping("getSysRoleMenuPoss")
    public JSONObject getSysRoleMenuPoss() {
        return sysRoleMenuPosService.getSysRoleMenuPoss();
    }

    @RequestMapping("getSysRoleMenuPosAll")
    public JSONObject getSysRoleMenuPosAll() {
        return sysRoleMenuPosService.getSysRoleMenuPosAll();
    }

    @RequestMapping("getSysRoleMenuPosCount")
    public JSONObject getSysRoleMenuPosCount() {
        return sysRoleMenuPosService.getSysRoleMenuPosCount();
    }

    @RequestMapping("changeSysRoleMenuPos")
    public JSONObject changeSysRoleMenuPos() {
        return sysRoleMenuPosService.changeSysRoleMenuPos();
    }

    @ApiOperation("根据角色绑定菜单")
    @RequestMapping("addSysRoleMenuPosInMenuIdsByRoleId")
    public JSONObject addSysRoleMenuPosInMenuIdsByRoleId() {
        return sysRoleMenuPosService.addSysRoleMenuPosInMenuIdsByRoleId();
    }

}
