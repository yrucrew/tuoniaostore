package com.tuoniaostore.community.vo;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class PurchaseUserVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String purchaseUserId; // 采购员ID
    private String defaultName; // 采购员账号
    private String realName; // 采购员名字
    private String phoneNumber; // 联系电话
    private Integer status = -1; // 账号状态
    private String leaderUserId; // 上级用户ID
    private String leaderRealName; // 上级名字
    private String leaderPhoneNumber; // 上级联系电话
    private String roleName;// 角色名称
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime; // 创建时间
    private String bindSupplierShopNames; // 绑定供应商名字
    private String bindShopName; // 绑定的仓库名

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPurchaseUserId() {
        return purchaseUserId;
    }

    public void setPurchaseUserId(String purchaseUserId) {
        this.purchaseUserId = purchaseUserId;
    }

    public String getDefaultName() {
        return defaultName;
    }

    public void setDefaultName(String defaultName) {
        this.defaultName = defaultName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getLeaderUserId() {
        return leaderUserId;
    }

    public void setLeaderUserId(String leaderUserId) {
        this.leaderUserId = leaderUserId;
    }

    public String getLeaderRealName() {
        return leaderRealName;
    }

    public void setLeaderRealName(String leaderRealName) {
        this.leaderRealName = leaderRealName;
    }

    public String getLeaderPhoneNumber() {
        return leaderPhoneNumber;
    }

    public void setLeaderPhoneNumber(String leaderPhoneNumber) {
        this.leaderPhoneNumber = leaderPhoneNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getBindSupplierShopNames() {
        return bindSupplierShopNames;
    }

    public void setBindSupplierShopNames(String bindSupplierShopNames) {
        this.bindSupplierShopNames = bindSupplierShopNames;
    }

    public String getBindShopName() {
        return bindShopName;
    }

    public void setBindShopName(String bindShopName) {
        this.bindShopName = bindShopName;
    }
}
