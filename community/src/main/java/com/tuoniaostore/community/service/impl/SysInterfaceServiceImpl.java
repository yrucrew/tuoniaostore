package com.tuoniaostore.community.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysInterfaceService;
import com.tuoniaostore.datamodel.user.SysInterface;
import com.tuoniaostore.datamodel.user.SysInterfaceParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysInterfaceService")
public class SysInterfaceServiceImpl extends BasicWebService implements SysInterfaceService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysInterface() {
        SysInterface sysInterface = new SysInterface();
        //接口名称
        String name = getUTF("name", null);
        //项目ID
        String projectId = getUTF("projectId", null);
        //接口访问地址
        String url = getUTF("url", null);
        //0.开发1.测试 2.上线 3.禁用
        Integer status = getIntParameter("status", 0);
        //
        String version = getUTF("version", null);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //返回类型0.application/json 1.text/html 2.x-application 3.application/xml
        Integer returnType = getIntParameter("returnType", 0);
        //错误列表
        String errorList = getUTF("errorList", null);
        //接口说明
        String explain = getUTF("explain", null);
        //返回值
        String response = getUTF("response", null);
        //
        String remark = getUTF("remark", null);

        changeParam(sysInterface);
        sysInterface.setName(name);
        sysInterface.setProjectId(projectId);
        sysInterface.setUrl(url);
        sysInterface.setStatus(status);
        sysInterface.setVersion(version);
        sysInterface.setSort(sort);
        sysInterface.setReturnType(returnType);
        sysInterface.setErrorList(errorList);
        sysInterface.setExplain(explain);
        sysInterface.setResponse(response);
        sysInterface.setRemark(remark);
        dataAccessManager.addSysInterface(sysInterface);
        return success();
    }

    private void fillParam(SysInterfaceParam param, JSONObject obj) {
        param.setName(obj.getString("name"));

        if (obj.getString("fillType").equals("是")) {
            param.setFillType(0);
        } else {
            param.setFillType(1);
        }
        param.setDataType(obj.getString("dataType"));
        param.setDefaultValue(obj.getString("defaultValue"));
        param.setRemark(obj.getString("remark"));
    }

    @Override
    public JSONObject getSysInterface() {
        SysInterface sysInterface = dataAccessManager.getSysInterface(getId());
        return success(sysInterface);
    }

    @Override
    public JSONObject getSysInterfaces() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        List<SysInterface> sysInterfaces = dataAccessManager.getSysInterfaces(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysInterfaceCount(status, name);
        return success(sysInterfaces, count);
    }

    @Override
    public JSONObject getSysInterfaceAll() {
        List<SysInterface> sysInterfaces = dataAccessManager.getSysInterfaceAll();
        return success(sysInterfaces);
    }

    @Override
    public JSONObject getSysInterfaceCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysInterfaceCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysInterface() {
        SysInterface sysInterface = fillSysInterface();
        dataAccessManager.changeSysInterface(sysInterface);
        return success();
    }

    private SysInterface fillSysInterface() {
        SysInterface sysInterface = dataAccessManager.getSysInterface(getId());
        //接口名称
        String name = getUTF("name", null);
        //项目ID
        String projectId = getUTF("projectId", null);
        //接口访问地址
        String url = getUTF("url", null);
        //0.开发1.测试 2.上线 3.禁用
        Integer status = getIntParameter("status", 0);
        //
        String version = getUTF("version", null);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //返回类型0.application/json 1.text/html 2.x-application 3.application/xml
        Integer returnType = getIntParameter("returnType", 0);
        //错误列表
        String errorList = getUTF("errorList", null);

        String explain = getUTF("explain", null);
        //返回值
        String response = getUTF("response", null);
        //
        String remark = getUTF("remark", null);

        sysInterface.setName(name);
        sysInterface.setProjectId(projectId);
        sysInterface.setUrl(url);
        sysInterface.setStatus(status);
        sysInterface.setVersion(version);
        sysInterface.setSort(sort);
        sysInterface.setReturnType(returnType);
        sysInterface.setErrorList(errorList);
        sysInterface.setExplain(explain);
        sysInterface.setResponse(response);
        sysInterface.setRemark(remark);
        return sysInterface;
    }

    @Override
    public JSONObject changeSysInterfaceParam() {

        SysInterface sysInterface = fillSysInterface();
        changeParam(sysInterface);
        dataAccessManager.changeSysInterface(sysInterface);
        return success();
    }

    @Override
    public JSONObject getSysInterfaceTree() {
        JSONArray jsonArray = dataAccessManager.getSysInterfaceTree();
        return success(jsonArray);
    }

    private void changeParam(SysInterface sysInterface) {
        String inInterfaces = getUTF("inInterfaces", null);
        String outInterfaces = getUTF("outInterfaces", null);


        List<SysInterfaceParam> listParam = new ArrayList<>();
        if(inInterfaces!=null) {
            JSONArray jsonArray = JSONArray.parseArray(inInterfaces);
            if (jsonArray != null) {
                int size = jsonArray.size();

                for (int i = 0; i < size; i++) {
                    if (jsonArray.getJSONObject(i).getString("name") != null && jsonArray.getJSONObject(i).getString("name").length() > 0) {
                        SysInterfaceParam param = new SysInterfaceParam();
                        param.setType(0);
                        param.setInterfaceId(sysInterface.getId());
                        fillParam(param, jsonArray.getJSONObject(i));
                        listParam.add(param);
                    }
                }
            }
        }
        JSONArray outJsonArray = JSONArray.parseArray(outInterfaces);
        if(outJsonArray!=null){
            if (outJsonArray != null) {
                int size = outJsonArray.size();
                for (int i = 0; i < size; i++) {
                    if (outJsonArray.getJSONObject(i).getString("name") != null && outJsonArray.getJSONObject(i).getString("name").length() > 0) {
                        SysInterfaceParam param = new SysInterfaceParam();
                        param.setType(1);
                        param.setInterfaceId(sysInterface.getId());
                        fillParam(param, outJsonArray.getJSONObject(i));
                        listParam.add(param);
                    }
                }
            }
        }

        dataAccessManager.delSysInterfaceParam(sysInterface.getId());
        if (listParam != null && listParam.size() > 0) {
            dataAccessManager.addSysInterfaceParam(listParam);
        }
    }

}
