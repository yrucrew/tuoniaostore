package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysTerminalService;
import com.tuoniaostore.datamodel.user.SysTerminal;
import com.tuoniaostore.datamodel.user.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysTerminalService")
public class SysTerminalServiceImpl extends BasicWebService implements SysTerminalService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysTerminal() {
        SysTerminal sysTerminal = new SysTerminal();
        //终端名称
        String title = getUTF("title", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        String remark = getUTF("remark", null);
        //
        Integer status = getIntParameter("status", 0);


        sysTerminal.setTitle(title);
        sysTerminal.setSort(sort);
        sysTerminal.setRemark(remark);
        fillUser(sysTerminal);
        sysTerminal.setStatus(status);
        dataAccessManager.addSysTerminal(sysTerminal);
        return success();
    }

    private void fillUser(SysTerminal sysTerminal) {
        try {
            SysUser sysUser = getUser();
            if (sysUser != null) {
                sysTerminal.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public JSONObject getSysTerminal() {
        SysTerminal sysTerminal = dataAccessManager.getSysTerminal(getId());
        return success(sysTerminal);
    }

    @Override
    public JSONObject getSysTerminals() {
        String title = getUTF("title", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysTerminal> sysTerminals = dataAccessManager.getSysTerminals(status, getPageStartIndex(getPageSize()), getPageSize(), title);
        fillTerminal(sysTerminals);
        int count = dataAccessManager.getSysTerminalCount(status, title);
        return success(sysTerminals, count);
    }

    private void fillTerminal(List<SysTerminal> sysTerminals) {
        int size = sysTerminals.size();
        for (int i = 0; i < size; i++) {
            if (sysTerminals.get(i).getUserId() != null) {
                SysUser sysUser = dataAccessManager.getSysUser(sysTerminals.get(i).getUserId());
                fillUserInfo(sysTerminals, i, sysUser);
            }
        }
    }

    private void fillUserInfo(List<SysTerminal> sysTerminals, int i, SysUser sysUser) {
        if (sysUser != null) {
            if (sysUser.getRealName() != null) {
                sysTerminals.get(i).setUserName(sysUser.getRealName());
            } else if (sysUser.getShowName() != null) {
                sysTerminals.get(i).setUserName(sysUser.getShowName());
            } else {
                sysTerminals.get(i).setUserName(sysUser.getDefaultName());
            }
        }
    }

    @Override
    public JSONObject getSysTerminalAll() {
        List<SysTerminal> sysTerminals = dataAccessManager.getSysTerminalAll();
        fillTerminal(sysTerminals);
        return success(sysTerminals);
    }

    @Override
    public JSONObject getSysTerminalCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysTerminalCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysTerminal() {
        SysTerminal sysTerminal = dataAccessManager.getSysTerminal(getId());
        //ID
        String id = getUTF("id", null);
        //终端名称
        String title = getUTF("title", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        String remark = getUTF("remark", null);
        //
        Integer status = getIntParameter("status", 0);
        //
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        sysTerminal.setId(id);
        sysTerminal.setTitle(title);
        sysTerminal.setSort(sort);
        sysTerminal.setRemark(remark);
        sysTerminal.setStatus(status);
        sysTerminal.setRetrieveStatus(retrieveStatus);
        dataAccessManager.changeSysTerminal(sysTerminal);
        return success();
    }

    /**
     * 验证终端是否存在
     * @author oy
     * @date 2019/5/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject validateSysTerminal() {
        String terminalName = getUTF("terminalName",null);
        String terminalId = getUTF("terminalId",null);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        if(terminalName != null && !terminalName.equals("")){
            SysTerminal sysTerminal = dataAccessManager.findSysUserByTerminalName(terminalName);
            if(sysTerminal != null){
                if(terminalId != null && !terminalId.equals("")){
                    if(sysTerminal.getId().equals(terminalId)){
                        jsonObject.put("key",0);
                        jsonObject.put("msg","可操作该终端");
                    }else{
                        jsonObject.put("key",1);
                        jsonObject.put("msg","该终端已存在");
                    }
                }else{
                    jsonObject.put("key",1);
                    jsonObject.put("msg","该终端已存在");
                }
            }else{
                jsonObject.put("key",0);
                jsonObject.put("msg","可操作该终端");
            }
            return jsonObject;
        }
        return jsonObject;
    }

    /**
     * 批量删除终端
     * @author oy
     * @date 2019/5/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject batchDeleteSysTerminal() {
        String ids = getUTF("ids");

        if(ids != null) {
            List<String> strings = JSONObject.parseArray(ids, String.class);
            if(strings != null && strings.size() > 0){
                dataAccessManager.batchDeleteSysTerminal(strings);
            }
        }
        return success("删除成功！");
    }

}
