package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
public interface SysProjectService {

    JSONObject addSysProject();

    JSONObject getSysProject();

    JSONObject getSysProjects();

    JSONObject getSysProjectCount();
    JSONObject getSysProjectAll();
    JSONObject getSysProjectTree();

    JSONObject changeSysProject();
    JSONObject getSysProjectByParentId();
}
