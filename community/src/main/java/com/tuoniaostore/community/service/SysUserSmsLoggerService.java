package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 16:36:22
 */
public interface SysUserSmsLoggerService {

    JSONObject addSysUserSmsLogger();

    JSONObject getSysUserSmsLogger();

    JSONObject getSysUserSmsLoggers();

    JSONObject getSysUserSmsLoggerCount();
    JSONObject getSysUserSmsLoggerAll();

    JSONObject changeSysUserSmsLogger();

    /**
     * 获取手机号码的验证码
     * @return
     */
    JSONObject getSysUserSmsLoggerByPhone();
}
