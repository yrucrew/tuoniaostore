package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 16:36:21
 */
public interface SysUserPropertiesService {

    JSONObject addSysUserProperties();

    JSONObject getSysUserProperties();

    JSONObject getSysUserPropertiess();

    JSONObject getSysUserPropertiesCount();
    JSONObject getSysUserPropertiesAll();

    JSONObject changeSysUserProperties();
}
