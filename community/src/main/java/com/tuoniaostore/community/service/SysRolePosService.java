package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 角色
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
public interface SysRolePosService {

    JSONObject addSysRolePos();

    JSONObject getSysRolePos();

    JSONObject getSysRolePoss();

    JSONObject getSysRolePosCount();
    JSONObject getSysRolePosAll();

    JSONObject changeSysRolePos();

    JSONObject delSysRolePosByIds();
}
