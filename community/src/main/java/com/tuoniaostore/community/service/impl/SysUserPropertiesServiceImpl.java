package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysUserPropertiesService;
import com.tuoniaostore.datamodel.user.SysUserProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysUserPropertiesService")
public class SysUserPropertiesServiceImpl extends BasicWebService implements SysUserPropertiesService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysUserProperties() {
        SysUserProperties sysUserProperties = new SysUserProperties();
        //类型
        Integer type = getIntParameter("type", 0);
        //属性索引
        String k = getUTF("k", null);
        //属性值
        String v = getUTF("v", null);
        sysUserProperties.setType(type);
        sysUserProperties.setK(k);
        sysUserProperties.setV(v);
        dataAccessManager.addSysUserProperties(sysUserProperties);
        return success();
    }

    @Override
    public JSONObject getSysUserProperties() {
        SysUserProperties sysUserProperties = dataAccessManager.getSysUserProperties(getId());
        return success(sysUserProperties);
    }

    @Override
    public JSONObject getSysUserPropertiess() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysUserProperties> sysUserPropertiess = dataAccessManager.getSysUserPropertiess(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysUserPropertiesCount(status, name);
        return success(sysUserPropertiess, count);
    }

    @Override
    public JSONObject getSysUserPropertiesAll() {
        List<SysUserProperties> sysUserPropertiess = dataAccessManager.getSysUserPropertiesAll();
        return success(sysUserPropertiess);
    }

    @Override
    public JSONObject getSysUserPropertiesCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysUserPropertiesCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysUserProperties() {
        SysUserProperties sysUserProperties = dataAccessManager.getSysUserProperties(getId());

        //类型
        Integer type = getIntParameter("type", 0);
        //属性索引
        String k = getUTF("k", null);
        //属性值
        String v = getUTF("v", null);
        sysUserProperties.setType(type);
        sysUserProperties.setK(k);
        sysUserProperties.setV(v);
        dataAccessManager.changeSysUserProperties(sysUserProperties);
        return success();
    }

}
