package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysAreaService;
import com.tuoniaostore.datamodel.user.SysArea;
import com.tuoniaostore.datamodel.user.SysAreaProvince;
import com.tuoniaostore.datamodel.user.SysCity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;


@Service("sysAreaService")
public class SysAreaServiceImpl extends BasicWebService implements SysAreaService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysArea() {
        SysArea sysArea = new SysArea();
        //区域标题
        String title = getUTF("title", null);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //状态0.可用 1.禁用
        Integer status = getIntParameter("status", 0);
        //备注
        String remarks = getUTF("remarks", null);
        sysArea.setTitle(title);
        sysArea.setSort(sort);
        sysArea.setStatus(status);
        sysArea.setRemarks(remarks);
        sysArea.setRetrieve_status(0);
        if(title == null || title.equals("")){
            return fail("地址不能为空!");
        }
        SysArea sysArea1 =  dataAccessManager.getSysAreasByTitle(title);
        if(sysArea1 != null){
            return fail("当前地址已存在!");
        }
        dataAccessManager.addSysArea(sysArea);
        return success("添加成功!");
    }

    @Override
    public JSONObject getSysArea() {
        String id = getUTF("id", null);
        if(id != null && !id.equals("")){
            SysArea sysArea = dataAccessManager.getSysArea(id);
            return success(sysArea);
        }
        return success();
    }

    @Override
    public JSONObject getSysSelArea() {
        int di = getIntParameter("di", 0);
        int level = getIntParameter("level", 1);
        String areaId = getUTF("areaId");
        List<SysCity> sysCitys = dataAccessManager.getSysProvinceAll(di, level);
        List<SysCity> nCitys = new ArrayList<>();
        List<SysAreaProvince> sysAreaServices = dataAccessManager.getSysAreaNotId(areaId);
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < sysAreaServices.size(); i++) {
            map.put(sysAreaServices.get(i).getCityId(), sysAreaServices.get(i).getAreaId());
        }
        for (int i = 0; i < sysCitys.size(); i++) {
            if (map.get(sysCitys.get(i).getId()) == null) {
                nCitys.add(sysCitys.get(i));
            }
        }
        return success(nCitys);
    }

    @Override
    public JSONObject getSysAreas() {
        String title = getUTF("title", null);
        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        List<SysArea> sysAreas = dataAccessManager.getSysAreas(getPageStartIndex(getPageSize()), getPageSize(), title, status);
        fillSysAreas(sysAreas);
        int count = dataAccessManager.getSysAreaCount(title);
        return success(sysAreas, count);
    }

    private void fillSysAreas(List<SysArea> sysAreas) {
        if (sysAreas != null) {
            int size = sysAreas.size();
            for (int i = 0; i < size; i++) {
                String areaId = sysAreas.get(i).getId();
                List<SysAreaProvince> sysAreaProvinces = dataAccessManager.getSysAreaProvinceByAreaId(areaId);
                StringBuffer sb = new StringBuffer();
                for (int k = 0; k < sysAreaProvinces.size(); k++) {
                    String cid = sysAreaProvinces.get(k).getCityId();
                    SysCity sysCity = dataAccessManager.getSysCity(cid);
                    if (sysCity != null) {
                        if (k != sysAreaProvinces.size() - 1) {
                            sb.append(sysCity.getName() + ",");
                        } else {
                            sb.append(sysCity.getName() + ",");
                        }
                    }
                }
                sysAreas.get(i).setProvinceStr(sb.toString());

            }
        }
    }

    @Override
    public JSONObject getSysAreaAll() {
        List<SysArea> sysAreas = dataAccessManager.getSysAreaAll();
        return success(sysAreas);
    }

    @Override
    public JSONObject getSysAreaCount() {
        String keyWord = getUTF("keyWord", null);
//        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysAreaCount(keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysArea() {
        SysArea sysArea = dataAccessManager.getSysArea(getId());
        //区域标题
        String title = getUTF("title", null);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //状态0.可用 1.禁用
        Integer status = getIntParameter("status", 0);
        //备注
        String remarks = getUTF("remarks", null);
        sysArea.setTitle(title);
        sysArea.setSort(sort);
        sysArea.setStatus(status);
        sysArea.setRemarks(remarks);

        if(title == null || title.equals("")){
            return fail("地址不能为空!");
        }
        SysArea sysArea1 =  dataAccessManager.getSysAreasByTitle(title);
        if(sysArea1 != null){
            String id = sysArea1.getId();//查询的
            String id1 = sysArea.getId();//当前的
            if(!id.equals(id1)){
                return fail("当前地址已存在!");
            }
        }
        dataAccessManager.changeSysArea(sysArea);
        return success("修改地址成功!");
    }

    @Override
    public JSONObject getSysAreaAllByPage() {
        String areaName = getUTF("areaName",null);
        List<SysArea> list = dataAccessManager.getSysAreasByPage(areaName,getPageStartIndex(getPageSize()),getPageSize());
        int count = 0;
        if(list != null && list.size() > 0){
            count = dataAccessManager.getSysAreasByPageCount(areaName);
        }
        return success(list,count);
    }

    @Override
    public JSONObject getSysAreasByIds() {
        String ids = getUTF("ids");
        if(ids != null && !ids.equals("")){
            List<String> strings = JSONObject.parseArray(ids, String.class);
            if(strings != null && strings.size() > 0){
                List<SysArea> list = dataAccessManager.getSysAreasByIds(strings,getPageStartIndex(getPageSize()),getPageSize());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("list",list);
                return jsonObject;
            }
        }
        return null;
    }


    @Override
    public JSONObject getSysAreasByNameLike() {
        String areaName = getUTF("areaName");

        if(areaName != null && !areaName.equals("")){
            List<SysArea> list = dataAccessManager.getSysAreasByNameLike(areaName,null,null);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("list",list);
            return jsonObject;
        }
        return null;
    }

    @Override
    public JSONObject validateSysArea() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        String title = getUTF("title");
        String id = getUTF("id",null);
        if(title != null && !title.equals("")){
            SysArea sysAreasByTitle = dataAccessManager.getSysAreasByTitle(title);
            if(sysAreasByTitle != null){
                String id1 = sysAreasByTitle.getId();
                if(id != null && !id.equals("")){
                    if(id1.equals(id)){
                        jsonObject.put("key",0);
                        jsonObject.put("msg","该地址信息可操作!");
                    }else{
                        jsonObject.put("key",1);
                        jsonObject.put("msg","该地址信息重复!");
                    }
                }else{
                    jsonObject.put("key",1);
                    jsonObject.put("msg","该地址信息重复!");
                }
            }else{
                jsonObject.put("key",0);
                jsonObject.put("msg","该地址信息没有重复的!");
            }
        }
        return jsonObject;
    }

    @Override
    public JSONObject deleteSysAreas() {
        String ids = getUTF("ids");
        List<String> areaIds = JSONObject.parseArray(ids, String.class);
        // 逻辑删除区域
        dataAccessManager.deleteSysAreas(areaIds);
        // 物理删除区域省市绑定
        dataAccessManager.deleteSysAreaProvinceByAreaIds(areaIds);
        return success("删除成功！");
    }

}
