package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
public interface SysInterfaceParamService {


    JSONObject getSysInterfaceParam();

    JSONObject getSysInterfaceParams();

    JSONObject getSysInterfaceParamCount();
    JSONObject getSysInterfaceParamAll();

    JSONObject changeSysInterfaceParam();
    JSONObject getSysInterfaceParamByInterfaceId();
}
