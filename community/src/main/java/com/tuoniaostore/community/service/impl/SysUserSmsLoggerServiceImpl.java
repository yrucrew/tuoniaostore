package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysUserSmsLoggerService;
import com.tuoniaostore.datamodel.user.SysUserSmsLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysUserSmsLoggerService")
public class SysUserSmsLoggerServiceImpl extends BasicWebService implements SysUserSmsLoggerService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysUserSmsLogger() {
        SysUserSmsLogger sysUserSmsLogger = new SysUserSmsLogger();
        //接收号码
        String phone = getUTF("phone", null);
        //发送内容
        String content = getUTF("content", null);
        //验证码内容(验证码短信有效)
        String code = getUTF("code", null);
        //短信类型
        Integer type = getIntParameter("type", 0);
        //状态 0--已发送
        Integer status = getIntParameter("status", 0);
        sysUserSmsLogger.setPhone(phone);
        sysUserSmsLogger.setContent(content);
        sysUserSmsLogger.setCode(code);
        sysUserSmsLogger.setType(type);
        sysUserSmsLogger.setStatus(status);
        dataAccessManager.addSysUserSmsLogger(sysUserSmsLogger);
        return success();
    }

    @Override
    public JSONObject getSysUserSmsLogger() {
        SysUserSmsLogger sysUserSmsLogger = dataAccessManager.getSysUserSmsLogger(getId());
        return success(sysUserSmsLogger);
    }

    @Override
    public JSONObject getSysUserSmsLoggers() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysUserSmsLogger> sysUserSmsLoggers = dataAccessManager.getSysUserSmsLoggers(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysUserSmsLoggerCount(status, name);
        return success(sysUserSmsLoggers, count);
    }

    @Override
    public JSONObject getSysUserSmsLoggerAll() {
        List<SysUserSmsLogger> sysUserSmsLoggers = dataAccessManager.getSysUserSmsLoggerAll();
        return success(sysUserSmsLoggers);
    }

    @Override
    public JSONObject getSysUserSmsLoggerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysUserSmsLoggerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysUserSmsLogger() {
        SysUserSmsLogger sysUserSmsLogger = dataAccessManager.getSysUserSmsLogger(getId());

        //接收号码
        String phone = getUTF("phone", null);
        //发送内容
        String content = getUTF("content", null);
        //验证码内容抽
        String code = getUTF("code", null);
        //短信类型
        Integer type = getIntParameter("type", 0);
        //状态 0--已发送
        Integer status = getIntParameter("status", 0);
        sysUserSmsLogger.setPhone(phone);
        sysUserSmsLogger.setContent(content);
        sysUserSmsLogger.setCode(code);
        sysUserSmsLogger.setType(type);
        sysUserSmsLogger.setStatus(status);
        dataAccessManager.changeSysUserSmsLogger(sysUserSmsLogger);
        return success();
    }


    /**
     * 通过手机号码 查找短信记录（验证码用）
     * 查询短信验证码 改了两分钟之内的  2019-03-26
     * @return
     */
    @Override
    public JSONObject getSysUserSmsLoggerByPhone() {
        String phone = getUTF("phone");//手机号码
        int type = getIntParameter("type");//验证码类型
        SysUserSmsLogger sysUserSmsLogger = dataAccessManager.getSysUserSmsLoggerByPhone(phone,type);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("response",JSONObject.toJSONString(sysUserSmsLogger));
        return success(jsonObject);
    }


}
