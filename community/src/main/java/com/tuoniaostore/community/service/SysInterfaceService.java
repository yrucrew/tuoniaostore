package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
public interface SysInterfaceService {

    JSONObject addSysInterface();

    JSONObject getSysInterface();

    JSONObject getSysInterfaces();

    JSONObject getSysInterfaceCount();
    JSONObject getSysInterfaceAll();
    JSONObject getSysInterfaceTree();

    JSONObject changeSysInterface();
    JSONObject changeSysInterfaceParam();
}
