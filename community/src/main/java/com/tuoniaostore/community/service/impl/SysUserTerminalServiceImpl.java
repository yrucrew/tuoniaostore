package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysUserTerminalService;
import com.tuoniaostore.datamodel.user.SysUserTerminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysUserTerminalService")
public class SysUserTerminalServiceImpl extends BasicWebService implements SysUserTerminalService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysUserTerminal() {
        SysUserTerminal sysUserTerminal = new SysUserTerminal();

        //
        String userId = getUTF("userId", null);
        //
        String terminalId = getUTF("terminalId", null);

        sysUserTerminal.setUserId(userId);
        sysUserTerminal.setTerminalId(terminalId);
        dataAccessManager.addSysUserTerminal(sysUserTerminal);
        return success();
    }

    @Override
    public JSONObject addSysUserTerminalByRemote() {
        String userTerminalEntity = getUTF("userTerminalEntity");
        if(userTerminalEntity != null && !userTerminalEntity.equals("")){
            SysUserTerminal sysUserTerminal = JSONObject.parseObject(userTerminalEntity, SysUserTerminal.class);
            if(sysUserTerminal != null){
                dataAccessManager.addSysUserTerminal(sysUserTerminal);
            }
        }
        return success();
    }


    @Override
    public JSONObject getSysUserTerminal() {
        SysUserTerminal sysUserTerminal = dataAccessManager.getSysUserTerminal(getId());
        return success(sysUserTerminal);
    }

    @Override
    public JSONObject getSysUserTerminals() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysUserTerminal> sysUserTerminals = dataAccessManager.getSysUserTerminals(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysUserTerminalCount(status, name);
        return success(sysUserTerminals, count);
    }

    @Override
    public JSONObject getSysUserTerminalAll() {
        List<SysUserTerminal> sysUserTerminals = dataAccessManager.getSysUserTerminalAll();
        return success(sysUserTerminals);
    }

    @Override
    public JSONObject getSysUserTerminalCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysUserTerminalCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysUserTerminal() {
        SysUserTerminal sysUserTerminal = dataAccessManager.getSysUserTerminal(getId());

        //
        String userId = getUTF("userId", null);
        //
        String terminalId = getUTF("terminalId", null);
        sysUserTerminal.setUserId(userId);
        sysUserTerminal.setTerminalId(terminalId);
        dataAccessManager.changeSysUserTerminal(sysUserTerminal);
        return success();
    }


}
