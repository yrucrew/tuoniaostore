package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysInterfaceParamService;
import com.tuoniaostore.datamodel.user.SysInterfaceParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysInterfaceParamService")
public class SysInterfaceParamServiceImpl extends BasicWebService implements SysInterfaceParamService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;


    @Override
    public JSONObject getSysInterfaceParam() {
        SysInterfaceParam sysInterfaceParam = dataAccessManager.getSysInterfaceParam(getId());
        return success(sysInterfaceParam);
    }

    @Override
    public JSONObject getSysInterfaceParams() {
        String name = getUTF("name", null);
        List<SysInterfaceParam> sysInterfaceParams = dataAccessManager.getSysInterfaceParams( getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysInterfaceParamCount( name);
        return success(sysInterfaceParams, count);
    }

    @Override
    public JSONObject getSysInterfaceParamAll() {
        List<SysInterfaceParam> sysInterfaceParams = dataAccessManager.getSysInterfaceParamAll();
        return success(sysInterfaceParams);
    }

    @Override
    public JSONObject getSysInterfaceParamCount() {
        String keyWord = getUTF("keyWord", null);
        int count = dataAccessManager.getSysInterfaceParamCount( keyWord);
        return success();
    }

    @Override
    public JSONObject getSysInterfaceParamByInterfaceId() {
        String interfaceId=getUTF("interfaceId");
        List<SysInterfaceParam> sysInterfaceParams = dataAccessManager.getSysInterfaceParamByInterfaceId(interfaceId);
        return success(sysInterfaceParams);
    }

    @Override
    public JSONObject changeSysInterfaceParam() {
        SysInterfaceParam sysInterfaceParam = dataAccessManager.getSysInterfaceParam(getId());
        //
        String id = getUTF("id", null);
        //参数名称
        String name = getUTF("name", null);
        //接口
        String interfaceId = getUTF("interfaceId", null);
        //
        String dataType = getUTF("dataType", null);
        //0.选填 1.必填
        Integer isfill = getIntParameter("isfill", 0);
        //默认值
        String defaultValue = getUTF("defaultValue", null);
        //备注
        String remark = getUTF("remark", null);
        //0.入参 1.输入字段说明
        Integer type = getIntParameter("type", 0);
        sysInterfaceParam.setId(id);
        sysInterfaceParam.setName(name);
        sysInterfaceParam.setInterfaceId(interfaceId);
        sysInterfaceParam.setDataType(dataType);
        sysInterfaceParam.setFillType(isfill);
        sysInterfaceParam.setDefaultValue(defaultValue);
        sysInterfaceParam.setRemark(remark);
        sysInterfaceParam.setType(type);
        dataAccessManager.changeSysInterfaceParam(sysInterfaceParam);
        return success();
    }

}
