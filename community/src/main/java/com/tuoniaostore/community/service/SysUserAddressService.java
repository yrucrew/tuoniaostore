package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.datamodel.user.SysUserAddress;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 16:36:22
 */
public interface SysUserAddressService {

    JSONObject addSysUserAddress();

    JSONObject getSysUserAddress();

    JSONObject getSysUserAddresss();

    JSONObject getSysUserAddressCount();
    JSONObject getSysUserAddressAll();

    JSONObject changeSysUserAddress();

    /**
     * 获取我的收货地址
     * @return
     */
    JSONObject getMyAddress();

    /**
     * 新增我的收货地址
     * @return
     */
    JSONObject addMyAddress();

    /**
     * 修改我的收货地址
     * @return
     */
    JSONObject changeMyAddress();

    /**
     * 修改我的默认收货地址
     * @return
     */
    JSONObject changeMyDefaultAddress();

    /**
     * 删除我的默认收货地址
     * @return
     */
    JSONObject deleteMyAddress();

    /**
     * 获取我的默认收货地址
     * @author oy
     * @date 2019/4/12
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getDefaultSysUserAddress();
    /**
     * 查询当前用户默认地址
     * @author sqd
     * @date 2019/4/9
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getGoodShopCartByDefault();
}
