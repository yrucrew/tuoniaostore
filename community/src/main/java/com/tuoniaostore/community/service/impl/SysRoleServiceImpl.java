package com.tuoniaostore.community.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.user.RetrieveStatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysRoleService;
import com.tuoniaostore.datamodel.user.SysRole;
import com.tuoniaostore.datamodel.user.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("sysRoleService")
public class SysRoleServiceImpl extends BasicWebService implements SysRoleService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysRole() {
        SysRole sysRole = new SysRole();
        //角色名称
        String roleName = getUTF("roleName");
        //备注
        String remark = getUTF("remark", null);
        //
        String icon = getUTF("icon", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //

        Integer status = getIntParameter("status", 0);
        sysRole.setRoleName(roleName);
        sysRole.setRemark(remark);
        sysRole.setIcon(icon);
        sysRole.setSort(sort);
        SysUser sysUser = null;
        try {
            sysUser = getUser();
            sysRole.setUserId(sysUser.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        sysRole.setStatus(status);
        dataAccessManager.addSysRole(sysRole);
        return success();
    }

    @Override
    public JSONObject getSysRole() {
        SysRole sysRole = dataAccessManager.getSysRole(getId());
        return success(sysRole);
    }

    @Override
    public JSONObject getSysRoles() {
        String roleName = getUTF("roleName", null);
        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        List<SysRole> sysRoles = dataAccessManager.getSysRoles(status, getPageStartIndex(getPageSize()), getPageSize(), roleName);
        int size = sysRoles.size();
        for (int i = 0; i < size; i++) {
            SysUser sysUser = dataAccessManager.getSysUser(sysRoles.get(i).getUserId());
            if (sysUser != null) {
                if (sysUser.getRealName() != null) {
                    sysRoles.get(i).setUserName(sysUser.getRealName());
                } else if (sysUser.getShowName() != null) {
                    sysRoles.get(i).setUserName(sysUser.getShowName());
                } else {
                    sysRoles.get(i).setUserName(sysUser.getDefaultName());
                }
            }
        }
        int count = dataAccessManager.getSysRoleCount(status, roleName);
        return success(sysRoles, count);
    }

    @Override
    public JSONObject getSysRoleAll() {
        List<SysRole> sysRoles = dataAccessManager.getSysRoleAll();
        return success(sysRoles);
    }

    @Override
    public JSONObject getSysRoleCount() {
        return success();
    }

    @Override
    public JSONObject changeSysRole() {
        SysRole sysRole = dataAccessManager.getSysRole(getId());
        //
        String id = getUTF("id");
        //角色名称
        String roleName = getUTF("roleName");
        //备注
        String remark = getUTF("remark", null);
        //
        String icon = getUTF("icon", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        //
        Integer status = getIntParameter("status", 0);
        sysRole.setId(id);
        sysRole.setRoleName(roleName);
        sysRole.setRemark(remark);
        sysRole.setIcon(icon);
        sysRole.setSort(sort);
        SysUser sysUser = null;
        try {
            sysUser = getUser();
            sysRole.setUserId(sysUser.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        sysRole.setRetrieveStatus(retrieveStatus);
        sysRole.setStatus(status);
        dataAccessManager.changeSysRole(sysRole);
        return success();
    }

    /**
     * 删除角色
     * @author oy
     * @date 2019/5/11
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject deleteSysRole() {
        String ids = getUTF("ids");
        if(ids != null && !ids.equals("")){
            List<String> idList = JSONObject.parseArray(ids, String.class);
            if(idList != null && idList.size()>0){
                dataAccessManager.deleteSysRole(idList);
            }
        }
        return success("删除成功！");
    }

    /**
     * 验证角色是否重复
     * @author oy
     * @date 2019/5/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject validateSysRole() {
        String roleName = getUTF("roleName",null);
        String roleId = getUTF("roleId",null);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        if(roleName != null && !roleName.equals("")){
            SysRole sysRole = dataAccessManager.findSysRoleByRoleName(roleName);
            if(sysRole != null){
                if(roleId != null && !roleId.equals("")){
                    if(sysRole.getId().equals(roleId)){
                        jsonObject.put("key",1);
                        jsonObject.put("msg","可操作！");
                    }else{
                        jsonObject.put("key",1);
                        jsonObject.put("msg","角色已存在！");
                    }
                }else{
                    jsonObject.put("key",1);
                    jsonObject.put("msg","角色已存在！");
                }
            }else{
                jsonObject.put("key",0);
                jsonObject.put("msg","没有重复！");
            }
        }
        return jsonObject;
    }

    @Override
    public JSONObject changeSysRoleRetrieve() {
        String id = getUTF("id");

        if (id.indexOf(",") != -1) {
            String[] ids = id.split(",");
            dataAccessManager.changeSysRoleRetrieve(ids, RetrieveStatusEnum.RETRIEVE.getKey());
        } else {
            String[] ids = new String[1];
            ids[0] = id;
            dataAccessManager.changeSysRoleRetrieve(ids, RetrieveStatusEnum.RETRIEVE.getKey());
        }

        return success();
    }
}
