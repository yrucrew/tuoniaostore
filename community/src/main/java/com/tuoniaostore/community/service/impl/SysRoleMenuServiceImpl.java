package com.tuoniaostore.community.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysRoleMenuService;
import com.tuoniaostore.datamodel.user.SysRoleMenu;
import com.tuoniaostore.datamodel.user.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service("sysRoleMenuService")
public class SysRoleMenuServiceImpl extends BasicWebService implements SysRoleMenuService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysRoleMenu() {
        String roleId = getUTF("roleId");
        String menuId = getUTF("menuId");
        String mid[] = menuId.split(",");
        List<SysRoleMenu> list = new ArrayList<>();
        SysUser sysUser= null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < mid.length; i++) {
            SysRoleMenu sysRoleMenu = new SysRoleMenu();
            sysRoleMenu.setRoleId(roleId);
            if(mid[i]!=null){
                sysRoleMenu.setMenuId(mid[i]);
                if (sysUser != null) {
                    sysRoleMenu.setUserId(sysUser.getId());
                }
                list.add(sysRoleMenu);
            }
        }
        dataAccessManager.delSysRoleMenuByRoleId(roleId);
        dataAccessManager.addSysRoleMenu(list);
        return success();
    }

    @Override
    public JSONObject getSysRoleMenu() {
        SysRoleMenu sysRoleMenu = dataAccessManager.getSysRoleMenu(getId());
        return success(sysRoleMenu);
    }

    @Override
    public JSONObject getSysRoleMenus() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysRoleMenu> sysRoleMenus = dataAccessManager.getSysRoleMenus(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysRoleMenuCount(status, name);
        return success(sysRoleMenus, count);
    }

    @Override
    public JSONObject getSysRoleMenuAll() {
        List<SysRoleMenu> sysRoleMenus = dataAccessManager.getSysRoleMenuAll();
        return success(sysRoleMenus);
    }

    @Override
    public JSONObject getSysRoleMenuCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysRoleMenuCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysRoleMenu() {
        SysRoleMenu sysRoleMenu = dataAccessManager.getSysRoleMenu(getId());
        //角色ID
        String roleId = getUTF("roleId", null);
        //菜单ID
        String menuId = getUTF("menuId", null);
        sysRoleMenu.setRoleId(roleId);
        sysRoleMenu.setMenuId(menuId);
        dataAccessManager.changeSysRoleMenu(sysRoleMenu);
        return success();
    }

}
