package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysMenuHistoryService;
import com.tuoniaostore.datamodel.user.SysMenuHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysMenuHistoryService")
public class SysMenuHistoryServiceImpl extends BasicWebService implements SysMenuHistoryService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysMenuHistory() {
        SysMenuHistory sysMenuHistory = new SysMenuHistory();
        //
        String sysMenuId = getUTF("sysMenuId", null);
        //
        Integer hit = getIntParameter("hit", 0);
        //
        Long sysRoleId = getLongParameter("sysRoleId", 0);

        sysMenuHistory.setSysMenuId(sysMenuId);
        sysMenuHistory.setHit(hit);
        sysMenuHistory.setLastTime(new Date());
        sysMenuHistory.setSysRoleId(sysRoleId);
        dataAccessManager.addSysMenuHistory(sysMenuHistory);
        return success();
    }

    @Override
    public JSONObject getSysMenuHistory() {
        SysMenuHistory sysMenuHistory = dataAccessManager.getSysMenuHistory(getId());
        return success(sysMenuHistory);
    }

    @Override
    public JSONObject getSysMenuHistorys() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysMenuHistory> sysMenuHistorys = dataAccessManager.getSysMenuHistorys(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysMenuHistoryCount(status, name);
        return success(sysMenuHistorys, count);
    }

    @Override
    public JSONObject getSysMenuHistoryAll() {
        List<SysMenuHistory> sysMenuHistorys = dataAccessManager.getSysMenuHistoryAll();
        return success(sysMenuHistorys);
    }

    @Override
    public JSONObject getSysMenuHistoryCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysMenuHistoryCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysMenuHistory() {
        SysMenuHistory sysMenuHistory = dataAccessManager.getSysMenuHistory(getId());

        String sysMenuId = getUTF("sysMenuId", null);
        //
        Integer hit = getIntParameter("hit", 0);

        //
        Long sysRoleId = getLongParameter("sysRoleId", 0);

        sysMenuHistory.setSysMenuId(sysMenuId);
        sysMenuHistory.setHit(hit);
        sysMenuHistory.setSysRoleId(sysRoleId);
        dataAccessManager.changeSysMenuHistory(sysMenuHistory);
        return success();
    }

}
