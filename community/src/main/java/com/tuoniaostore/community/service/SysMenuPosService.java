package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 菜单管理
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
public interface SysMenuPosService {

    JSONObject addSysMenuPos();

    JSONObject getSysMenuPos();

    JSONObject getSysMenuPoss();

    JSONObject getSysMenuPosCount();
    JSONObject getSysMenuPosAll();

    JSONObject changeSysMenuPos();

    JSONObject getSysMenuPosByRoleId();

    JSONObject getSysMenuPosByUserId();

    JSONObject delSysMenuPosByIds();
}
