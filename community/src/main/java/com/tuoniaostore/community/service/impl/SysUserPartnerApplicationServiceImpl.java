package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysUserPartnerApplicationService;
import com.tuoniaostore.datamodel.user.SysUserPartnerApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysUserPartnerApplicationService")
public class SysUserPartnerApplicationServiceImpl extends BasicWebService implements SysUserPartnerApplicationService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysUserPartnerApplication() {
        SysUserPartnerApplication sysUserPartnerApplication = new SysUserPartnerApplication();

        //合作者ID
        String partnerId = getUTF("partnerId", null);
        //应用产品名
        String appName = getUTF("appName", null);
        //应用key
        String appKey = getUTF("appKey", null);
        //访问key 预留字段 必须存在
        String accessKey = getUTF("accessKey", null);
        //
        String accessKeyExponent = getUTF("accessKeyExponent", null);
        //访问密钥 预留字段 必须存在
        String accessKeySecret = getUTF("accessKeySecret", null);
        //
        String accessKeySecretExponent = getUTF("accessKeySecretExponent", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        Integer status = getIntParameter("status", 0);
        //回收站
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        //
        String remark = getUTF("remark", null);
        //
        String accessKeyModulus = getUTF("accessKeyModulus", null);
        //
        String accessKeySecretModulus = getUTF("accessKeySecretModulus", null);

        sysUserPartnerApplication.setPartnerId(partnerId);
        sysUserPartnerApplication.setAppName(appName);
        sysUserPartnerApplication.setAppKey(appKey);
        sysUserPartnerApplication.setAccessKey(accessKey);
        sysUserPartnerApplication.setAccessKeyExponent(accessKeyExponent);
        sysUserPartnerApplication.setAccessKeySecret(accessKeySecret);
        sysUserPartnerApplication.setAccessKeySecretExponent(accessKeySecretExponent);
        sysUserPartnerApplication.setSort(sort);
        sysUserPartnerApplication.setStatus(status);
        sysUserPartnerApplication.setRetrieveStatus(retrieveStatus);
        sysUserPartnerApplication.setRemark(remark);
        sysUserPartnerApplication.setAccessKeyModulus(accessKeyModulus);
        sysUserPartnerApplication.setAccessKeySecretModulus(accessKeySecretModulus);
        dataAccessManager.addSysUserPartnerApplication(sysUserPartnerApplication);
        return success();
    }

    @Override
    public JSONObject getSysUserPartnerApplication() {
        SysUserPartnerApplication sysUserPartnerApplication = dataAccessManager.getSysUserPartnerApplication(getId());
        return success(sysUserPartnerApplication);
    }

    @Override
    public JSONObject getSysUserPartnerApplications() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysUserPartnerApplication> sysUserPartnerApplications = dataAccessManager.getSysUserPartnerApplications(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysUserPartnerApplicationCount(status, name);
        return success(sysUserPartnerApplications, count);
    }

    @Override
    public JSONObject getSysUserPartnerApplicationAll() {
        List<SysUserPartnerApplication> sysUserPartnerApplications = dataAccessManager.getSysUserPartnerApplicationAll();
        return success(sysUserPartnerApplications);
    }

    @Override
    public JSONObject getSysUserPartnerApplicationCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysUserPartnerApplicationCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysUserPartnerApplication() {
        SysUserPartnerApplication sysUserPartnerApplication = dataAccessManager.getSysUserPartnerApplication(getId());

        //合作者ID
        String partnerId = getUTF("partnerId", null);
        //应用产品名
        String appName = getUTF("appName", null);
        //应用key
        String appKey = getUTF("appKey", null);
        //访问key 预留字段 必须存在
            String accessKey = getUTF("accessKey", null);
        //
        String accessKeyExponent = getUTF("accessKeyExponent", null);
        //访问密钥 预留字段 必须存在
        String accessKeySecret = getUTF("accessKeySecret", null);
        //
        String accessKeySecretExponent = getUTF("accessKeySecretExponent", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        Integer status = getIntParameter("status", 0);
        //回收站
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        //
        String remark = getUTF("remark", null);
        //
        String accessKeyModulus = getUTF("accessKeyModulus", null);
        //
        String accessKeySecretModulus = getUTF("accessKeySecretModulus", null);

        sysUserPartnerApplication.setPartnerId(partnerId);
        sysUserPartnerApplication.setAppName(appName);
        sysUserPartnerApplication.setAppKey(appKey);
        sysUserPartnerApplication.setAccessKey(accessKey);
        sysUserPartnerApplication.setAccessKeyExponent(accessKeyExponent);
        sysUserPartnerApplication.setAccessKeySecret(accessKeySecret);
        sysUserPartnerApplication.setAccessKeySecretExponent(accessKeySecretExponent);
        sysUserPartnerApplication.setSort(sort);
        sysUserPartnerApplication.setStatus(status);
        sysUserPartnerApplication.setRetrieveStatus(retrieveStatus);
        sysUserPartnerApplication.setRemark(remark);
        sysUserPartnerApplication.setAccessKeyModulus(accessKeyModulus);
        sysUserPartnerApplication.setAccessKeySecretModulus(accessKeySecretModulus);
        dataAccessManager.changeSysUserPartnerApplication(sysUserPartnerApplication);
        return success();
    }

    @Override
    public JSONObject getSysUserPartnerApplicationAppKey() {
        String appKey = getUTF("appKey", null);
        SysUserPartnerApplication sysPassportPartnerApplication = dataAccessManager.getSysUserPartnerApplicationAppKey(appKey);
        return success(sysPassportPartnerApplication);
    }
}
