package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysLogService;
import com.tuoniaostore.datamodel.user.SysLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysLogService")
public class SysLogServiceImpl extends BasicWebService implements SysLogService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysLog() {
        SysLog sysLog = new SysLog();
        //
        String tableName = getUTF("tableName", null);
        //
        String tableField = getUTF("tableField", null);
        //
        String beforeContent = getUTF("beforeContent", null);
        //
        String afterContent = getUTF("afterContent", null);
        //
        String remarks = getUTF("remarks", null);
        sysLog.setTableName(tableName);
        sysLog.setTableField(tableField);
        sysLog.setBeforeContent(beforeContent);
        sysLog.setAfterContent(afterContent);
        sysLog.setRemarks(remarks);
        dataAccessManager.addSysLog(sysLog);
        return success();
    }

    @Override
    public JSONObject getSysLog() {
        SysLog sysLog = dataAccessManager.getSysLog(getId());
        return success(sysLog);
    }

    @Override
    public JSONObject getSysLogs() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysLog> sysLogs = dataAccessManager.getSysLogs(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysLogCount(status, name);
        return success(sysLogs, count);
    }

    @Override
    public JSONObject getSysLogAll() {
        List<SysLog> sysLogs = dataAccessManager.getSysLogAll();
        return success(sysLogs);
    }

    @Override
    public JSONObject getSysLogCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysLogCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysLog() {
        SysLog sysLog = dataAccessManager.getSysLog(getId());
        //
        String tableName = getUTF("tableName", null);
        //
        String tableField = getUTF("tableField", null);
        //
        String beforeContent = getUTF("beforeContent", null);
        //
        String afterContent = getUTF("afterContent", null);
        //
        String remarks = getUTF("remarks", null);
        sysLog.setTableName(tableName);
        sysLog.setTableField(tableField);
        sysLog.setBeforeContent(beforeContent);
        sysLog.setAfterContent(afterContent);
        sysLog.setRemarks(remarks);
        dataAccessManager.changeSysLog(sysLog);
        return success();
    }

}
