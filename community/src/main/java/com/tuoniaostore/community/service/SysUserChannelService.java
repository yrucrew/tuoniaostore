package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.datamodel.user.SysUserChannel;

import java.util.List;
import java.util.Map;

/**
 * 合作渠道，用于区分来自不同渠道的用户数据
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 16:36:22
 */
public interface SysUserChannelService {

    JSONObject addSysUserChannel();

    JSONObject getSysUserChannel();

    JSONObject getSysUserChannels();

    JSONObject getSysUserChannelCount();
    JSONObject getSysUserChannelAll();

    JSONObject changeSysUserChannel();

    /**
     * 根据名字查找对应的渠道id
     * @param fromChannel
     */
    SysUserChannel getSysUserChannelByName(String fromChannel);
}
