package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysRetrieveService;
import com.tuoniaostore.datamodel.user.SysRetrieve;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysRetrieveService")
public class SysRetrieveServiceImpl extends BasicWebService implements SysRetrieveService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysRetrieve() {
        SysRetrieve sysRetrieve = new SysRetrieve();
        //
        String title = getUTF("title", null);
        //
        String retrieveTableName = getUTF("retrieveTableName", null);
        //
        String retrieveTableId = getUTF("retrieveTableId", null);
        sysRetrieve.setTitle(title);
        sysRetrieve.setRetrieveTableName(retrieveTableName);
        sysRetrieve.setRetrieveTableId(retrieveTableId);
        dataAccessManager.addSysRetrieve(sysRetrieve);
        return success();
    }

    @Override
    public JSONObject addSysRetrieveForRemote() {
        String sysRetrieveEntity = getUTF("sysRetrieveEntity");
        if(sysRetrieveEntity != null && sysRetrieveEntity.equals("")){
            SysRetrieve sysRetrieve = JSONObject.parseObject(sysRetrieveEntity, SysRetrieve.class);
            if(sysRetrieve != null){
                dataAccessManager.addSysRetrieve(sysRetrieve);
            }
        }
        return success();
    }

    @Override
    public JSONObject addSysRetrievesForRemote() {
        String sysRetrieveEntityList = getUTF("sysRetrieveEntityList");
        if(sysRetrieveEntityList != null && !sysRetrieveEntityList.equals("")){
            List<SysRetrieve> sysRetrieve = JSONObject.parseArray(sysRetrieveEntityList, SysRetrieve.class);
            if(sysRetrieve != null && sysRetrieve.size() > 0){
                dataAccessManager.addSysRetrieves(sysRetrieve);
            }
        }
        return success();
    }

    @Override
    public JSONObject getSysRetrieve() {
        SysRetrieve sysRetrieve = dataAccessManager.getSysRetrieve(getId());
        return success(sysRetrieve);
    }

    @Override
    public JSONObject getSysRetrieves() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysRetrieve> sysRetrieves = dataAccessManager.getSysRetrieves(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysRetrieveCount(status, name);
        return success(sysRetrieves, count);
    }

    @Override
    public JSONObject getSysRetrieveAll() {
        List<SysRetrieve> sysRetrieves = dataAccessManager.getSysRetrieveAll();
        return success(sysRetrieves);
    }

    @Override
    public JSONObject getSysRetrieveCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysRetrieveCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysRetrieve() {
        SysRetrieve sysRetrieve = dataAccessManager.getSysRetrieve(getId());
        //
        String id = getUTF("id", null);
        //
        String title = getUTF("title", null);
        //
        String retrieveTableName = getUTF("retrieveTableName", null);
        //
        String retrieveTableId = getUTF("retrieveTableId", null);
        sysRetrieve.setId(id);
        sysRetrieve.setTitle(title);
        sysRetrieve.setRetrieveTableName(retrieveTableName);
        sysRetrieve.setRetrieveTableId(retrieveTableId);
        dataAccessManager.changeSysRetrieve(sysRetrieve);
        return success();
    }


}
