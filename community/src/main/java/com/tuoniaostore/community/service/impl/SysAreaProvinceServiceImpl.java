package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysAreaProvinceService;
import com.tuoniaostore.datamodel.user.SysAreaProvince;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysAreaProvinceService")
public class SysAreaProvinceServiceImpl extends BasicWebService implements SysAreaProvinceService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysAreaProvince() {

        //区域Id
        String areaId = getUTF("areaId", null);
        //省份ID
        String cityId = getUTF("cityId", null);

        String[] cids = cityId.split(",");
        int len = cids.length;
        List<SysAreaProvince> sysAreaProvinces = new ArrayList<>();

        for (int i = 0; i < len; i++) {

            if (cids[i] != null && cids[i].length() > 0) {
                SysAreaProvince sysAreaProvince = new SysAreaProvince();
                sysAreaProvince.setAreaId(areaId);
                sysAreaProvince.setCityId(cids[i]);
                sysAreaProvinces.add(sysAreaProvince);
            }
        }
        dataAccessManager.delSysAreaProvince(areaId);
        if (sysAreaProvinces != null && sysAreaProvinces.size() > 0) {
            dataAccessManager.addSysAreaProvince(sysAreaProvinces);
        }
        return success();
    }

    @Override
    public JSONObject getSysAreaProvince() {
        SysAreaProvince sysAreaProvince = dataAccessManager.getSysAreaProvince(getId());
        return success(sysAreaProvince);
    }

    @Override
    public JSONObject getSysAreaProvinces() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysAreaProvince> sysAreaProvinces = dataAccessManager.getSysAreaProvinces(status, getPageStartIndex(getPageSize()), getPageSize(), name);
            int count = dataAccessManager.getSysAreaProvinceCount(status, name);
        return success(sysAreaProvinces, count);
    }

    @Override
    public JSONObject getSysAreaProvinceAll() {
        List<SysAreaProvince> sysAreaProvinces = dataAccessManager.getSysAreaProvinceAll();
        return success(sysAreaProvinces);
    }

    @Override
    public JSONObject getSysAreaProvinceCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysAreaProvinceCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysAreaProvince() {
        SysAreaProvince sysAreaProvince = dataAccessManager.getSysAreaProvince(getId());
        //区域Id
        String areaId = getUTF("areaId");
        //省份ID
        String cityId = getUTF("cityId", null);

        dataAccessManager.delSysAreaProvince(areaId);
        sysAreaProvince.setAreaId(areaId);
        sysAreaProvince.setCityId(cityId);
        dataAccessManager.changeSysAreaProvince(sysAreaProvince);
        return success();
    }

}
