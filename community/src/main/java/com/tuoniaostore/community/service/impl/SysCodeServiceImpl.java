package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.CommonUtils;
import com.tuoniaostore.commons.UUIDUtils;
import com.tuoniaostore.commons.code.Captcha;
import com.tuoniaostore.commons.code.GifCaptcha;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.file.oss.AliyunOSSUtils;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysCodeService;
import com.tuoniaostore.datamodel.user.SysCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;


@Service("sysCodeService")
public class SysCodeServiceImpl extends BasicWebService implements SysCodeService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysCode() {
        SysCode sysCode = new   SysCode();
        Map map = new HashMap();
        String name ="";
        try {
            String uuid = UUIDUtils.getUUID();
            Captcha captcha = new GifCaptcha(150, 40, 5);//   gif格式动画验证码
            name = System.getProperty("user.dir") + "/image/" + uuid + CommonUtils.defineDateFormat(System.currentTimeMillis(), "yyyyMMdd")+ ".gif";
            captcha.out(new FileOutputStream(name));
            FileInputStream inputStream = new FileInputStream(name);
            String url = AliyunOSSUtils.uploadImageFiles("code/"+uuid+CommonUtils.defineDateFormat(System.currentTimeMillis(), "yyyyMMdd")+ ".gif", "ostrichapps", inputStream);
            sysCode.setCode(captcha.text());
            sysCode.setCodename(uuid);
            sysCode.setCreateTime(System.currentTimeMillis());
            map.put("codeKey", uuid);
            map.put("imageUrl", url);
            File file = new File(name);
            if(file.exists()){
                file.delete();
            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
            return fail("错误"+name);
        }
        dataAccessManager.addSysCode(sysCode);
        return success(map);
    }

    @Override
    public JSONObject getSysCode() {
        SysCode sysCode = dataAccessManager.getSysCode(getId());
        return success(sysCode);
    }

    @Override
    public JSONObject getSysCodes() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysCode> sysCodes = dataAccessManager.getSysCodes(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysCodeCount(status, name);
        return success(sysCodes, count);
    }

    @Override
    public JSONObject getSysCodeAll() {
        List<SysCode> sysCodes = dataAccessManager.getSysCodeAll();
        return success(sysCodes);
    }

    @Override
    public JSONObject getSysCodeCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysCodeCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysCode() {
        SysCode sysCode = dataAccessManager.getSysCode(getId());
        //
        String id = getUTF("id", null);
        //
        String code = getUTF("code", null);
        //
        String codename = getUTF("codename", null);
        //
        String codeUrl = getUTF("codeUrl", null);
        sysCode.setId(id);
        sysCode.setCode(code);
        sysCode.setCodename(codename);
        sysCode.setCodeUrl(codeUrl);
        dataAccessManager.changeSysCode(sysCode);
        return success();
    }

}
