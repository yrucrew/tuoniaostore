package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysUserRolePosService;
import com.tuoniaostore.datamodel.user.SysUserRolePos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysUserRolePosService")
public class SysUserRolePosServiceImpl extends BasicWebService implements SysUserRolePosService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysUserRolePos() {
        SysUserRolePos sysUserRolePos = new SysUserRolePos();
        //
        String id = getUTF("id", null);
        //用户ID
        String userId = getUTF("userId", null);
        //角色ID
        String roleId = getUTF("roleId", null);
        //
        String operationId = getUTF("operationId", null);
        //
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        //
        Integer status = getIntParameter("status", 0);
        //
        String remark = getUTF("remark", null);
        sysUserRolePos.setId(id);
        sysUserRolePos.setUserId(userId);
        sysUserRolePos.setRoleId(roleId);
        sysUserRolePos.setOperationId(operationId);
        sysUserRolePos.setRetrieveStatus(retrieveStatus);
        sysUserRolePos.setStatus(status);
        sysUserRolePos.setRemark(remark);
        dataAccessManager.addSysUserRolePos(sysUserRolePos);
        return success();
    }

    @Override
    public JSONObject getSysUserRolePos() {
        SysUserRolePos sysUserRolePos = dataAccessManager.getSysUserRolePos(getId());
        return success(sysUserRolePos);
    }

    @Override
    public JSONObject getSysUserRolePoss() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysUserRolePos> sysUserRolePoss = dataAccessManager.getSysUserRolePoss(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysUserRolePosCount(status, name);
        return success(sysUserRolePoss, count);
    }

    @Override
    public JSONObject getSysUserRolePosAll() {
        List<SysUserRolePos> sysUserRolePoss = dataAccessManager.getSysUserRolePosAll();
        return success(sysUserRolePoss);
    }

    @Override
    public JSONObject getSysUserRolePosCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysUserRolePosCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysUserRolePos() {
        SysUserRolePos sysUserRolePos = dataAccessManager.getSysUserRolePos(getId());
        //
        String id = getUTF("id", null);
        //用户ID
        String userId = getUTF("userId", null);
        //角色ID
        String roleId = getUTF("roleId", null);
        //
        String operationId = getUTF("operationId", null);
        //
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        //
        Integer status = getIntParameter("status", 0);
        //
        String remark = getUTF("remark", null);
        sysUserRolePos.setId(id);
        sysUserRolePos.setUserId(userId);
        sysUserRolePos.setRoleId(roleId);
        sysUserRolePos.setOperationId(operationId);
        sysUserRolePos.setRetrieveStatus(retrieveStatus);
        sysUserRolePos.setStatus(status);
        sysUserRolePos.setRemark(remark);
        dataAccessManager.changeSysUserRolePos(sysUserRolePos);
        return success();
    }

}
