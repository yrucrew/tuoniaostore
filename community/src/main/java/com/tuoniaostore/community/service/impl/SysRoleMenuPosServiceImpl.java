package com.tuoniaostore.community.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysRoleMenuPosService;
import com.tuoniaostore.datamodel.DefineRandom;
import com.tuoniaostore.datamodel.user.SysRoleMenuPos;
import com.tuoniaostore.datamodel.user.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysRoleMenuPosService")
public class SysRoleMenuPosServiceImpl extends BasicWebService implements SysRoleMenuPosService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

//    private void fillSysRoleMenuPos(SysRoleMenuPos sysRoleMenuPos) {
//
//    }
//
    @Override
    public JSONObject addSysRoleMenuPos() {
        SysRoleMenuPos sysRoleMenuPos = new SysRoleMenuPos();
        //
        String id = getUTF("id", null);
        //角色ID
        String roleId = getUTF("roleId", null);
        //菜单ID
        String menuId = getUTF("menuId", null);
        //
        String sysChannelId = getUTF("sysChannelId", null);
        //
        String userId = getUTF("userId", null);
        //
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        sysRoleMenuPos.setId(id);
        sysRoleMenuPos.setRoleId(roleId);
        sysRoleMenuPos.setMenuId(menuId);
        sysRoleMenuPos.setSysChannelId(sysChannelId);
        sysRoleMenuPos.setUserId(userId);
        sysRoleMenuPos.setRetrieveStatus(retrieveStatus);
        dataAccessManager.addSysRoleMenuPos(sysRoleMenuPos);
        return success();
    }

    @Override
    public JSONObject getSysRoleMenuPos() {
        SysRoleMenuPos sysRoleMenuPos = dataAccessManager.getSysRoleMenuPos(getId());
        return success(sysRoleMenuPos);
    }

    @Override
    public JSONObject getSysRoleMenuPoss() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysRoleMenuPos> sysRoleMenuPoss = dataAccessManager.getSysRoleMenuPoss(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysRoleMenuPosCount(status, name);
        return success(sysRoleMenuPoss, count);
    }

    @Override
    public JSONObject getSysRoleMenuPosAll() {
        List<SysRoleMenuPos> sysRoleMenuPoss = dataAccessManager.getSysRoleMenuPosAll();
        return success(sysRoleMenuPoss);
    }

    @Override
    public JSONObject getSysRoleMenuPosCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysRoleMenuPosCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysRoleMenuPos() {
        SysRoleMenuPos sysRoleMenuPos = dataAccessManager.getSysRoleMenuPos(getId());
        //
        String id = getUTF("id", null);
        //角色ID
        String roleId = getUTF("roleId", null);
        //菜单ID
        String menuId = getUTF("menuId", null);
        //
        String sysChannelId = getUTF("sysChannelId", null);
        //
        String userId = getUTF("userId", null);
        //
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        sysRoleMenuPos.setId(id);
        sysRoleMenuPos.setRoleId(roleId);
        sysRoleMenuPos.setMenuId(menuId);
        sysRoleMenuPos.setSysChannelId(sysChannelId);
        sysRoleMenuPos.setUserId(userId);
        sysRoleMenuPos.setRetrieveStatus(retrieveStatus);
        dataAccessManager.changeSysRoleMenuPos(sysRoleMenuPos);
        return success();
    }

    @Override
    public JSONObject addSysRoleMenuPosInMenuIdsByRoleId() {
        List<String> menuIds = JSONObject.parseArray(getUTF("menuIds"), String.class);
        String roleId = getUTF("roleId");
        // 删除roleId绑定的关系
        dataAccessManager.delSysRoleMenuPosByRoleId(roleId);
        // 绑定菜单到roleId的角色上
        SysRoleMenuPos sysRoleMenuPos = new SysRoleMenuPos();
        sysRoleMenuPos.setRoleId(roleId);
        sysRoleMenuPos.setRetrieveStatus(0);
        sysRoleMenuPos.setUserId(getUser().getId());
        sysRoleMenuPos.setSysChannelId(null);
        for (String menuId : menuIds) {
            sysRoleMenuPos.setId(DefineRandom.getUUID());
            sysRoleMenuPos.setMenuId(menuId);
            dataAccessManager.addSysRoleMenuPos(sysRoleMenuPos);
        }
        return success("添加菜单成功！");
    }

}
