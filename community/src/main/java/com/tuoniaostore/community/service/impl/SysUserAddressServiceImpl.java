package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.user.AddressType;
import com.tuoniaostore.commons.constant.user.UserAddressStatus;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysUserAddressService;
import com.tuoniaostore.datamodel.good.GoodShopCart;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.user.SysUserAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import org.springframework.transaction.annotation.Transactional;


@Service("sysUserAddressService")
public class SysUserAddressServiceImpl extends BasicWebService implements SysUserAddressService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysUserAddress() {
        SysUserAddress sysUserAddress = new SysUserAddress();


        //地址别名
        String addressAlias = getUTF("addressAlias", null);
        //地址联系人姓名
        String name = getUTF("name", null);
        //联系人电话
        String phoneNumber = getUTF("phoneNumber", null);
        //国家
        String country = getUTF("country", null);
        //省份
        String province = getUTF("province", null);
        //城市
        String city = getUTF("city", null);
        //行政区域
        String district = getUTF("district", null);
        //街道名
        String street = getUTF("street", null);
        //街道号
        String streetNum = getUTF("streetNum", null);
        //对应百度地图API的displayName(name)
        String displayName = getUTF("displayName", null);
        //详细地址
        String detailAddress = getUTF("detailAddress", null);
        //城市编码(国标)
        String adcode = getUTF("adcode", null);
        //经度
        Double longitude = getDoubleParameter("longitude", 0);
        //纬度
        Double latitude = getDoubleParameter("latitude", 0);
        //类型；0-收货地址；1-开拓签约地址
        Integer type = getIntParameter("type", 0);
        //状态，0-启用；1-失效；2-默认
        Integer status = getIntParameter("status", 0);
        //0-女性；1-男性
        Integer sex = getIntParameter("sex", 0);
        //
        Integer sort = getIntParameter("sort", 0);

        try {
            SysUser sysUser=getUser();
            if(sysUser!=null){
                sysUserAddress.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        sysUserAddress.setAddressAlias(addressAlias);
        sysUserAddress.setName(name);
        sysUserAddress.setPhoneNumber(phoneNumber);
        sysUserAddress.setCountry(country);
        sysUserAddress.setProvince(province);
        sysUserAddress.setCity(city);
        sysUserAddress.setDistrict(district);
        sysUserAddress.setStreet(street);
        sysUserAddress.setStreetNum(streetNum);
        sysUserAddress.setDisplayName(displayName);
        sysUserAddress.setDetailAddress(detailAddress);
        sysUserAddress.setAdcode(adcode);
        sysUserAddress.setLongitude(longitude);
        sysUserAddress.setLatitude(latitude);
        sysUserAddress.setType(type);
        sysUserAddress.setStatus(status);
        sysUserAddress.setSex(sex);
        sysUserAddress.setSort(sort);
        dataAccessManager.addSysUserAddress(sysUserAddress);
        return success();
    }

    @Override
    public JSONObject getSysUserAddress() {
        SysUserAddress sysUserAddress = dataAccessManager.getSysUserAddress(getId());
        return success(sysUserAddress);
    }

    @Override
    public JSONObject getSysUserAddresss() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysUserAddress> sysUserAddresss = dataAccessManager.getSysUserAddresss(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int size =sysUserAddresss.size();
        for(int i=0;i<size;i++){
            try {
                SysUser sysUser= SysUserRmoteService.getSysUser(sysUserAddresss.get(i).getUserId(),getHttpServletRequest());
                if(sysUser!=null){
                    if(sysUser.getRealName()!=null) {
                        sysUserAddresss.get(i).setUserName(sysUser.getRealName());
                    }else if(sysUser.getShowName()!=null){
                        sysUserAddresss.get(i).setUserName(sysUser.getShowName());
                    }else {
                        sysUserAddresss.get(i).setUserName(sysUser.getDefaultName());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int count = dataAccessManager.getSysUserAddressCount(status, name);
        return success(sysUserAddresss, count);
    }

    @Override
    public JSONObject getSysUserAddressAll() {
        List<SysUserAddress> sysUserAddresss = dataAccessManager.getSysUserAddressAll();
        return success(sysUserAddresss);
    }

    @Override
    public JSONObject getSysUserAddressCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysUserAddressCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysUserAddress() {
        SysUserAddress sysUserAddress = dataAccessManager.getSysUserAddress(getId());
        //
        String id = getUTF("id", null);
        //地址别名
        String addressAlias = getUTF("addressAlias", null);
        //地址联系人姓名
        String name = getUTF("name", null);
        //联系人电话
        String phoneNumber = getUTF("phoneNumber", null);
        //国家
        String country = getUTF("country", null);
        //省份
        String province = getUTF("province", null);
        //城市
        String city = getUTF("city", null);
        //行政区域
        String district = getUTF("district", null);
        //街道名
        String street = getUTF("street", null);
        //街道号
        String streetNum = getUTF("streetNum", null);
        //对应百度地图API的displayName(name)
        String displayName = getUTF("displayName", null);
        //详细地址
        String detailAddress = getUTF("detailAddress", null);
        //城市编码(国标)
        String adcode = getUTF("adcode", null);
        //经度
        Double longitude = getDoubleParameter("longitude", 0);
        //纬度
        Double latitude = getDoubleParameter("latitude", 0);
        //类型；0-收货地址；1-开拓签约地址
        Integer type = getIntParameter("type", 0);
        //状态，0-启用；1-失效；2-默认
        Integer status = getIntParameter("status", 0);
        //0-女性；1-男性
        Integer sex = getIntParameter("sex", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        sysUserAddress.setId(id);
        try {
            SysUser sysUser=getUser();
            if(sysUser!=null){
                sysUserAddress.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        sysUserAddress.setAddressAlias(addressAlias);
        sysUserAddress.setName(name);
        sysUserAddress.setPhoneNumber(phoneNumber);
        sysUserAddress.setCountry(country);
        sysUserAddress.setProvince(province);
        sysUserAddress.setCity(city);
        sysUserAddress.setDistrict(district);
        sysUserAddress.setStreet(street);
        sysUserAddress.setStreetNum(streetNum);
        sysUserAddress.setDisplayName(displayName);
        sysUserAddress.setDetailAddress(detailAddress);
        sysUserAddress.setAdcode(adcode);
        sysUserAddress.setLongitude(longitude);
        sysUserAddress.setLatitude(latitude);
        sysUserAddress.setType(type);
        sysUserAddress.setStatus(status);
        sysUserAddress.setSex(sex);
        sysUserAddress.setSort(sort);
        dataAccessManager.changeSysUserAddress(sysUserAddress);
        return success();
    }

    @Override
    public JSONObject getMyAddress() {
        SysUser user = null;
        //获取当前用户id
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //查找出当前用户的所有的地址 status 状态，0-启用；1-失效；2-默认
        List<SysUserAddress> myAddress = dataAccessManager.getMyAddress(user.getId());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("myAddress",myAddress);
        return success(jsonObject);
    }


    private void fillEntity(SysUserAddress sysUserAddress){
        String name = getUTF("name");//收货人
        String phoneNumber = getUTF("phoneNumber");//联系电话
        String country = getUTF("country","中国");//国家
        String province = getUTF("province");//省份
        String city = getUTF("city");//城市
        String district = getUTF("district");//区
        String street = getUTF("street",null);//街道名
        String streetNum = getUTF("streetNum",null);//街道号
        String displayName = getUTF("displayName",null);//显示地址名称 比如海伦堡创意园
        String detailAddress = getUTF("detailAddress");//详情地址补充
        Integer status = getIntParameter("status", UserAddressStatus.NORMAL.getKey());//是否默认地址 0-启用；1-失效；2-默认

        double longitude = getDoubleParameter("longitude", 0.0);
        double latitude = getDoubleParameter("latitude", 0.0);

        sysUserAddress.setType(AddressType.NORMAL.getKey());//类型 收货地址
        sysUserAddress.setName(name);
        sysUserAddress.setCity(city);
        sysUserAddress.setPhoneNumber(phoneNumber);
        sysUserAddress.setCountry(country);
        sysUserAddress.setProvince(province);
        sysUserAddress.setStreet(street);
        sysUserAddress.setDisplayName(displayName);
        sysUserAddress.setDistrict(district);
        sysUserAddress.setStreetNum(streetNum);
        sysUserAddress.setDetailAddress(detailAddress);
        sysUserAddress.setStatus(status);
        sysUserAddress.setLongitude(longitude);
        sysUserAddress.setLatitude(latitude);
    }

    /**
     * 添加地址
     * @return
     */
    @Override
    public JSONObject addMyAddress() {
        SysUserAddress sysUserAddress = new SysUserAddress();
        fillEntity(sysUserAddress);
        SysUser user = null;
        try {
            user = getUser();
            sysUserAddress.setUserId(user.getId());//存放用户id
            System.out.println("用户名id：：："+user.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        int status = sysUserAddress.getStatus();
        if(status == 2){//说明添加的时候 是默认地址
            dataAccessManager.clearSysUserDefaultAddress(user.getId());
        }
        dataAccessManager.addSysUserAddress(sysUserAddress);
        return success("添加用户地址成功！");
    }

    /**
     * 修改地址
     * @return
     */
    @Override
    public JSONObject changeMyAddress() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        SysUserAddress sysUserAddress = new SysUserAddress();
        String id = getUTF("id");
        fillEntity(sysUserAddress);
        sysUserAddress.setId(id);
        Integer status = sysUserAddress.getStatus();
        if(status == 2){//说明添加的时候 是默认地址
            dataAccessManager.clearSysUserDefaultAddress(user.getId());
        }
        dataAccessManager.changeSysUserAddress(sysUserAddress);
        return success("修改用户地址成功！");
    }

    /**
     * 修改默认地址
     * @return
     */
    @Override
    public JSONObject changeMyDefaultAddress() {
        String id = getUTF("id");
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userId = user.getId();
        dataAccessManager.changeSysUserDefaultAddressById(id,userId);//通过id 和 用户id 调整当前默认的地址
        return success("修改成功");
    }

    /**
     * 删除我的地址（默认地址不能删除）
     * @return
     */
    @Override
    public JSONObject deleteMyAddress() {
        String id = getUTF("id");
        int status = getIntParameter("status");
        dataAccessManager.deleteSysUserDefaultAddressById(id);//通过id删除当前的地址
        if(status == 2){
            //去数据库查找地址 给一个最近新建的地址 作为默认地址
            SysUser user = getUser();
            String userId = user.getId();
            //查找一下该用户的地址 查找最最创建的
            SysUserAddress sysUserAddress =  dataAccessManager.getSysUserAddressByUserId(userId);
            if (sysUserAddress != null) {
                dataAccessManager.changeSysUserDefaultAddress(sysUserAddress.getId());//设置默认地址
            }
        }
        return success("修改成功");
    }

   /**
    * 获取我的默认收货地址
    * @author oy
    * @date 2019/4/12
    * @param
    * @return com.alibaba.fastjson.JSONObject
    */
    @Override
    public JSONObject getDefaultSysUserAddress() {
        String userId = getUTF("userId");
        //根据userId查找对应的默认地址
        SysUserAddress sysUserAddress = dataAccessManager.getDefaultSysUserAddress(userId);
        return success(sysUserAddress);
    }

    @Override
    public JSONObject getGoodShopCartByDefault() {
        HashMap<String, Object> map = new HashMap<>();
        SysUser user = null;
        try {
            user = getUser();//获取当前用户
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user == null){
            return fail(1001,"用户不能为空");
        }
        map.put("userId",user.getId());
        List<GoodShopCart> goodShopCartByDefault = dataAccessManager.getGoodShopCartByDefault(map);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("defaultAddress",goodShopCartByDefault);
        return success(jsonObject);
    }


}
