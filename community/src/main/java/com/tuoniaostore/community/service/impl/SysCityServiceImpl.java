package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysCityService;
import com.tuoniaostore.datamodel.user.SysAreaProvince;
import com.tuoniaostore.datamodel.user.SysCity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.CollectionUtils;


@Service("sysCityService")
public class SysCityServiceImpl extends BasicWebService implements SysCityService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysCity() {
        SysCity sysCity = new SysCity();
        //
        Long code = getLongParameter("code", 0);
        //
        String sheng = getUTF("sheng", null);
        //
        String di = getUTF("di", null);
        //
        String xian = getUTF("xian", null);
        //
        String name = getUTF("name", null);
        //
        String level = getUTF("level", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        Integer status = getIntParameter("status", 0);
        //
        String longitude = getUTF("longitude", null);
        //
        String latitude = getUTF("latitude", null);
        sysCity.setCode(code);
        sysCity.setSheng(sheng);
        sysCity.setDi(di);
        sysCity.setXian(xian);
        sysCity.setName(name);
        sysCity.setLevel(level);
        sysCity.setSort(sort);
        sysCity.setStatus(status);
        sysCity.setLongitude(longitude);
        sysCity.setLatitude(latitude);
        dataAccessManager.addSysCity(sysCity);
        return success();
    }

    @Override
    public JSONObject getSysCity() {
        SysCity sysCity = dataAccessManager.getSysCity(getId());
        return success(sysCity);
    }

    @Override
    public JSONObject getSysCitys() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysCity> sysCitys = dataAccessManager.getSysCitys(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysCityCount(status, name);
        return success(sysCitys, count);
    }

    @Override
    public JSONObject getSysCityAll() {
        List<SysCity> sysCitys = dataAccessManager.getSysCityAll();
        return success(sysCitys);
    }

    @Override
    public JSONObject getSysCityCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysCityCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject getSysProvinceAll() {
        int di = getIntParameter("di", 0);
        int level = getIntParameter("level", 1);
        List<SysCity> sysCitys = dataAccessManager.getSysProvinceAll(di, level);
        return success(sysCitys);
    }

    @Override
    public JSONObject getSysCityByIds() {
        String ids = getUTF("areaId");
        List<SysAreaProvince> sysAreaProvinces = dataAccessManager.getSysAreaProvinceByAreaId(ids);
        if (sysAreaProvinces != null && sysAreaProvinces.size() > 0) {
            List<String> byIds = sysAreaProvinces.stream().map(SysAreaProvince::getCityId).collect(Collectors.toList());
            List<SysCity> sysCitys = dataAccessManager.getSysCityByIds(byIds);
            return success(sysCitys);
        }
        return success();
    }

    @Override
    public JSONObject getSysCityByShengLevel() {
        Integer sheng = getIntParameter("sheng",0);
        Integer level = getIntParameter("level",0);
        List<SysCity> sysCities = dataAccessManager.getSysCityByShengLevel(sheng, level);

        return success(sysCities);
    }

    @Override
    public JSONObject getSysCityByShengDi() {
        int sheng = getIntParameter("sheng");
        int di = getIntParameter("di");
        List<SysCity> sysCities = dataAccessManager.getSysCityByShengDi(sheng, di);
        return success(sysCities);
    }

    /**
     * 获取所有省
     * 若入参areaId则筛选check
     */
    @Override
    public JSONObject getSysProvincesCheckBind() {
        String areaId = getUTF("areaId", null);
        if (areaId == null) {
            // 未传入areaId 无需筛选
            return getSysProvinceAll();
        } else {
            // 筛选-联表查询
            return success(dataAccessManager.getSysProvincesCheckBind(areaId));
        }
    }

    @Override
    public JSONObject getSysProvincesUnBind() {
        List<SysCity> provinces = dataAccessManager.getSysProvincesUnBind();
        if (CollectionUtils.isEmpty(provinces)) {
            return success("没有未绑定的省！");
        }
        return success(provinces);
    }

    @Override
    public JSONObject changeSysCity() {
        SysCity sysCity = dataAccessManager.getSysCity(getId());
        //
        Long code = getLongParameter("code", 0);
        //
        String sheng = getUTF("sheng", null);
        //
        String di = getUTF("di", null);
        //
        String xian = getUTF("xian", null);
        //
        String name = getUTF("name", null);
        //
        String level = getUTF("level", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        Integer status = getIntParameter("status", 0);
        //
        String longitude = getUTF("longitude", null);
        //
        String latitude = getUTF("latitude", null);
        sysCity.setCode(code);
        sysCity.setSheng(sheng);
        sysCity.setDi(di);
        sysCity.setXian(xian);
        sysCity.setName(name);
        sysCity.setLevel(level);
        sysCity.setSort(sort);
        sysCity.setStatus(status);
        sysCity.setLongitude(longitude);
        sysCity.setLatitude(latitude);
        dataAccessManager.changeSysCity(sysCity);
        return success();
    }

}
