package com.tuoniaostore.community.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysMenuPosService;
import com.tuoniaostore.datamodel.user.SysMenuPos;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.user.SysUserRolePos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.CollectionUtils;


@Service("sysMenuPosService")
public class SysMenuPosServiceImpl extends BasicWebService implements SysMenuPosService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    private SysMenuPos fillSysMenuPos(SysMenuPos sysMenuPos) {
        String parentId = getUTF("parentId", "0");
        //菜单名称
        String name = getUTF("name");
        //菜单URL
        String url = getUTF("url", null);
        //授权(多个用逗号分隔，如：user:list,user:create)
        String perms = getUTF("perms", null);
        //类型   0：目录   1：菜单   2：按钮
        Integer type = getIntParameter("type", 1);
        //菜单图标
        String icon = getUTF("icon", null);
        //排序
        Integer orderNum = getIntParameter("orderNum", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        Integer status = getIntParameter("status", 0);
        //级别
        Integer level = getIntParameter("level", 0);
        //
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        //
        String remark = getUTF("remark", null);
        sysMenuPos.setParentId(parentId);
        sysMenuPos.setName(name);
        sysMenuPos.setUrl(url);
        sysMenuPos.setPerms(perms);
        sysMenuPos.setType(type);
        sysMenuPos.setIcon(icon);
        sysMenuPos.setOrderNum(orderNum);
        sysMenuPos.setSort(sort);
        sysMenuPos.setStatus(status);
        sysMenuPos.setLevel(level);
        sysMenuPos.setUserId(getUser().getId());
        sysMenuPos.setRetrieveStatus(retrieveStatus);
        sysMenuPos.setRemark(remark);
        return sysMenuPos;
    }

    @Override
    public JSONObject addSysMenuPos() {
        SysMenuPos sysMenuPos = new SysMenuPos();
        dataAccessManager.addSysMenuPos(fillSysMenuPos(sysMenuPos));
        return success("添加POS机菜单成功！");
    }

    @Override
    public JSONObject getSysMenuPos() {
        SysMenuPos sysMenuPos = dataAccessManager.getSysMenuPos(getId());
        return success(sysMenuPos);
    }

    @Override
    public JSONObject getSysMenuPoss() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysMenuPos> sysMenuPoss = dataAccessManager.getSysMenuPoss(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysMenuPosCount(status, name);
        return success(sysMenuPoss, count);
    }

    @Override
    public JSONObject getSysMenuPosAll() {
        List<SysMenuPos> sysMenuPoss = dataAccessManager.getSysMenuPosAll();
        return success(sysMenuPoss);
    }

    @Override
    public JSONObject getSysMenuPosCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysMenuPosCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysMenuPos() {
        SysMenuPos sysMenuPos = dataAccessManager.getSysMenuPos(getId());
        if (sysMenuPos == null) {
            return fail("菜单不存在或id错误！");
        }
        fillSysMenuPos(sysMenuPos);
        dataAccessManager.changeSysMenuPos(sysMenuPos);
        return success("保存成功！");
    }

    /**
     * 通过角色获取菜单
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/18
     */
    @Override
    public JSONObject getSysMenuPosByRoleId() {
        String roleId = getUTF("roleId");

//        List<SysRoleMenuPos> list = dataAccessManager.getSysRoleMenuPosByRoleId(roleId);
//        //获取对应的菜单
//        if(list != null && list.size() > 0){
//            List<String> menuId = list.stream().map(SysRoleMenuPos::getMenuId).distinct().collect(Collectors.toList());
//            List<SysMenuPos> menuPosList = dataAccessManager.getSysMenuPosByIds(menuId);
//            return success(menuPosList);
//        }

        JSONArray jsonArray = dataAccessManager.getSysMenuPosByRoleId(roleId);

        return success(jsonArray);
    }

    /**
     * 通过当前用户 获取当前的POS机菜单
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/20
     */
    @Override
    public JSONObject getSysMenuPosByUserId() {
        JSONArray jsonArray = null;
        try {
            // 当前用户
//            SysUser sysUser = getUser();
//            String userId = sysUser.getId();
            //根据用户id 查找当前的pos机的权限id
            SysUser sysUser1 = dataAccessManager.getSysUser("4a61f44eac2e4553aa4d733740a1767ftuXTHx");
            if (sysUser1 == null) {
                return fail("当前用户不存在数据。");
            }
            List<SysUserRolePos> sysUserRolePos = dataAccessManager.getSysUserRolePosByUserId(sysUser1.getId());
            // 当前用户的角色roleId列表
            List<String> roleIds = sysUserRolePos.stream().map(SysUserRolePos::getRoleId).collect(Collectors.toList());
            if (sysUser1 != null) {
                jsonArray = dataAccessManager.getSysMenuPosInRoleId(roleIds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success(jsonArray, 0);
    }

    @Override
    public JSONObject delSysMenuPosByIds() {
        List<String> menuIds = JSONObject.parseArray(getUTF("menuIds"), String.class);
        if (CollectionUtils.isEmpty(menuIds)) {
            return fail("无数据!");
        }
        dataAccessManager.updateSysMenuPosByIds(menuIds, StatusEnum.DISABLE.getKey());
        return success("删除菜单成功！");
    }
}
