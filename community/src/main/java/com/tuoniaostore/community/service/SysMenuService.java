package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 菜单管理
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 16:36:22
 */
public interface SysMenuService {

    JSONObject addSysMenu();

    JSONObject getSysMenu();

    JSONObject getSysMenus();

    JSONObject getSysMenuCount();
    JSONObject getSysMenuAll();
    JSONObject getWebSysMenus();
    JSONObject getPurviewMenus();
    JSONObject getLevelMenus();
    JSONObject changeSysMenu();
    JSONObject changeSysMenuRetrieve();
    JSONObject deleteSysMenu();
}
