package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysConfigService;
import com.tuoniaostore.datamodel.user.SysConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysConfigService")
public class SysConfigServiceImpl extends BasicWebService implements SysConfigService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysConfig() {
        SysConfig sysConfig = new SysConfig();
        //key
        String key = getUTF("key", null);
        //value
        String value = getUTF("value", null);
        //状态   0：隐藏   1：显示
        Integer status = getIntParameter("status", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        //备注
        String remark = getUTF("remark", null);

        sysConfig.setKey(key);
        sysConfig.setValue(value);
        sysConfig.setStatus(status);
        sysConfig.setSort(sort);
        sysConfig.setRemark(remark);
        dataAccessManager.addSysConfig(sysConfig);
        return success();
    }

    @Override
    public JSONObject getSysConfig() {
        SysConfig sysConfig = dataAccessManager.getSysConfig(getId());
        return success(sysConfig);
    }

    @Override
    public JSONObject getSysConfigs() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysConfig> sysConfigs = dataAccessManager.getSysConfigs(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysConfigCount(status, name);
        return success(sysConfigs, count);
    }

    @Override
    public JSONObject getSysConfigAll() {
        List<SysConfig> sysConfigs = dataAccessManager.getSysConfigAll();
        return success(sysConfigs);
    }

    @Override
    public JSONObject getSysConfigCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysConfigCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysConfig() {
        SysConfig sysConfig = dataAccessManager.getSysConfig(getId());
        //key
        String key = getUTF("key", null);
        //value
        String value = getUTF("value", null);
        //状态   0：隐藏   1：显示
        Integer status = getIntParameter("status", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        //备注
        String remark = getUTF("remark", null);
        sysConfig.setKey(key);
        sysConfig.setValue(value);
        sysConfig.setStatus(status);
        sysConfig.setSort(sort);
        sysConfig.setRemark(remark);
        dataAccessManager.changeSysConfig(sysConfig);
        return success();
    }

}
