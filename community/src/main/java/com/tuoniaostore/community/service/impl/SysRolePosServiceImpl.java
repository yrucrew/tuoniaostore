package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysRolePosService;
import com.tuoniaostore.datamodel.DefineRandom;
import com.tuoniaostore.datamodel.user.SysRoleMenuPos;
import com.tuoniaostore.datamodel.user.SysRolePos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.CollectionUtils;


@Service("sysRolePosService")
public class SysRolePosServiceImpl extends BasicWebService implements SysRolePosService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    private void fillSysRolePos(SysRolePos sysRolePos) {
        //角色名称
        String roleName = getUTF("roleName");
        //备注
        String remark = getUTF("remark", null);
        String icon = getUTF("icon", null);
        Integer sort = getIntParameter("sort", 0);
        //0正常 1删除
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        Integer status = getIntParameter("status", 0);
        // 创建人 当前用户
        String userId = getUser().getId();

        sysRolePos.setRoleName(roleName);
        sysRolePos.setRemark(remark);
        sysRolePos.setIcon(icon);
        sysRolePos.setSort(sort);
        sysRolePos.setUserId(userId);
        sysRolePos.setRetrieveStatus(retrieveStatus);
        sysRolePos.setStatus(status);
    }

    @Override
    public JSONObject addSysRolePos() {
        // 添加角色
        SysRolePos sysRolePos = new SysRolePos();
        fillSysRolePos(sysRolePos);
        if (!CollectionUtils.isEmpty(dataAccessManager.getSysRolePosByRoleName(sysRolePos.getRoleName()))) {
            return fail("角色名已存在");
        }
        dataAccessManager.addSysRolePos(sysRolePos);
        // 若传入menuIds 绑定菜单
        String ids = getUTF("menuIds", null);
        if (ids != null) {
            List<String> menuIds = JSONObject.parseArray(ids, String.class);
            List<SysRoleMenuPos> list = new ArrayList<>();
            for (int i = 0; i < menuIds.size(); i++) {
                SysRoleMenuPos sysRoleMenuPos = new SysRoleMenuPos();
                sysRoleMenuPos.setSysChannelId(null);
                sysRoleMenuPos.setUserId(getUser().getId());
                sysRoleMenuPos.setRoleId(sysRolePos.getId());
                sysRoleMenuPos.setRetrieveStatus(0);
                String menuId = menuIds.get(i);
                sysRoleMenuPos.setId(DefineRandom.getUUID());
                sysRoleMenuPos.setMenuId(menuId);
                sysRoleMenuPos.setRoleId(sysRolePos.getId());
                list.add(sysRoleMenuPos);
            }
            dataAccessManager.batchAddSysRoleMenuPos(list);
        }
        return success("添加POS机角色成功！");
    }

    @Override
    public JSONObject getSysRolePos() {
        SysRolePos sysRolePos = dataAccessManager.getSysRolePos(getId());
        return success(sysRolePos);
    }

    @Override
    public JSONObject getSysRolePoss() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysRolePos> sysRolePoss = dataAccessManager.getSysRolePoss(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysRolePosCount(status, name);
        return success(sysRolePoss, count);
    }

    @Override
    public JSONObject getSysRolePosAll() {
        List<SysRolePos> sysRolePoss = dataAccessManager.getSysRolePosAll();
        return success(sysRolePoss);
    }

    @Override
    public JSONObject getSysRolePosCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysRolePosCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysRolePos() {
        SysRolePos sysRolePos = dataAccessManager.getSysRolePos(getId());
        if (sysRolePos == null) {
            return fail("角色不存在或id错误！");
        }
        //角色名称
        String roleName = getUTF("roleName");
        //备注
        String remark = getUTF("remark", null);
        //
        String icon = getUTF("icon", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        String userId = getUTF("userId", null);
        //0正常 1删除
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        //
        Integer status = getIntParameter("status", 0);
        sysRolePos.setRoleName(roleName);
        sysRolePos.setRemark(remark);
        sysRolePos.setIcon(icon);
        sysRolePos.setSort(sort);
        sysRolePos.setUserId(userId);
        sysRolePos.setRetrieveStatus(retrieveStatus);
        sysRolePos.setStatus(status);
        dataAccessManager.changeSysRolePos(sysRolePos);
        return success("保存成功！");
    }

    @Override
    public JSONObject delSysRolePosByIds() {
        List<String> ids = JSONObject.parseArray(getUTF("ids"), String.class);
        if (CollectionUtils.isEmpty(ids)) {
            return fail("无数据!");
        }
        dataAccessManager.updateSysRolePosByIds(ids, StatusEnum.DISABLE.getKey());
        return success("删除成功!");
    }

}
