package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 用户与角色对应的关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 16:36:22
 */
public interface SysUserRoleService {

    JSONObject addSysUserRole();

    JSONObject getSysUserRole();

    JSONObject getSysUserRoles();

    JSONObject getSysUserRoleCount();
    JSONObject getSysUserRoleAll();

    JSONObject changeSysUserRole();
}
