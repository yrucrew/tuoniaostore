package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.CommonUtils;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysVersionService;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.user.SysVersion;
import com.tuoniaostore.datamodel.user.SysVersionLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysVersionService")
public class SysVersionServiceImpl extends BasicWebService implements SysVersionService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;



    @Override
    public JSONObject addSysVersion() {
        SysVersion sysVersion = new SysVersion();
        //
        //对应的设备类型 如：Android IOS等
        Integer deviceType = getIntParameter("deviceType", 0);
        //客户端类型，逻辑决定，如：供应链、商家端等
        Integer clientType = getIntParameter("clientType", 0);
        //内部版本号 一般递增 初始为1
        Integer versionIndex = getIntParameter("versionIndex", 0);
        //用于展示的版本号
        String showVersion = getUTF("showVersion", null);
        //最低兼容的版本(内部版本号)
        Integer minVersionIndex = getIntParameter("minVersionIndex", 0);
        //下载地址
        String versionUrl = getUTF("versionUrl", null);
        //更新内容介绍
        String versionIntroduce = getUTF("versionIntroduce", null);
        //开放更新时间
        String openTime = getUTF("openTime", null);
        //是否局部更新：0否，1是
        Integer islocal = getIntParameter("islocal", 0);
        //
        String localUserId = getUTF("localUserId", null);//id,id,id


        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userId = "";
        if (user != null) {
            userId = user.getId();
        }
        sysVersion.setDeviceType(deviceType);
        sysVersion.setClientType(clientType);
        sysVersion.setVersionIndex(versionIndex);
        sysVersion.setShowVersion(showVersion);
        sysVersion.setMinVersionIndex(minVersionIndex);
        sysVersion.setVersionUrl(versionUrl);
        sysVersion.setVersionIntroduce(versionIntroduce);
        sysVersion.setOpenTime(CommonUtils.strToDate(openTime));
        sysVersion.setIslocal(islocal);
        sysVersion.setUserId(userId);

        dataAccessManager.addSysVersion(sysVersion);

        if(islocal==1){
            List<SysVersionLocal> list = new ArrayList<>();
            String[] split = localUserId.split(",");
            if(split != null && split.length > 0 ){
                int length = split.length;
                for (int i = 0; i < length ; i++) {
                    String s = split[i];
                    if (s!= null && s != "") {
                        SysVersionLocal sysVersionLocal = new SysVersionLocal();
                        sysVersionLocal.setUserId(s);
                        sysVersionLocal.setOperateUserId(userId);
                        sysVersionLocal.setUserVersionId(sysVersion.getId());
                        list.add(sysVersionLocal);
                    }
                }
            }
            dataAccessManager.addSysVersionLocalBatch(list);
        }
        return success();
    }

    @Override
    public JSONObject getSysVersion() {
        SysVersion sysVersion = dataAccessManager.getSysVersion(getId());
        return success(sysVersion);
    }

    @Override
    public JSONObject getSysVersions() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysVersion> sysVersions = dataAccessManager.getSysVersions(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysVersionCount(status, name);
        return success(sysVersions, count);
    }

    @Override
    public JSONObject getSysVersionAll() {
        List<SysVersion> sysVersions = dataAccessManager.getSysVersionAll();
        return success(sysVersions);
    }

    @Override
    public JSONObject getSysVersionCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysVersionCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysVersion() {
        SysVersion sysVersion = dataAccessManager.getSysVersion(getId());
        //对应的设备类型 如：Android IOS等
        Integer deviceType = getIntParameter("deviceType", 0);
        //客户端类型，逻辑决定，如：供应链、商家端等
        Integer clientType = getIntParameter("clientType", 0);
        //内部版本号 一般递增 初始为1
        Integer versionIndex = getIntParameter("versionIndex", 0);
        //用于展示的版本号
        String showVersion = getUTF("showVersion", null);
        //最低兼容的版本(内部版本号)
        Integer minVersionIndex = getIntParameter("minVersionIndex", 0);
        //下载地址
        String versionUrl = getUTF("versionUrl", null);
        //更新内容介绍
        String versionIntroduce = getUTF("versionIntroduce", null);
        //开放更新时间
        String openTime = getUTF("openTime", null);
        //是否局部更新：0否，1是
        Integer islocal = getIntParameter("islocal", 0);
        String userId = getUTF("userId", null);

        sysVersion.setDeviceType(deviceType);
        sysVersion.setClientType(clientType);
        sysVersion.setVersionIndex(versionIndex);
        sysVersion.setShowVersion(showVersion);
        sysVersion.setMinVersionIndex(minVersionIndex);
        sysVersion.setVersionUrl(versionUrl);
        sysVersion.setVersionIntroduce(versionIntroduce);
        sysVersion.setOpenTime(CommonUtils.strToDate(openTime));
        sysVersion.setIslocal(islocal);
        sysVersion.setUserId(userId);
        dataAccessManager.changeSysVersion(sysVersion);
        return success();
    }

    /**
     * 版本控制升级
     * @return
     */
    @Override
    public JSONObject getVersionControl() {
        //对应的设备类型 如：Android IOS等
        Integer deviceType = getIntParameter("deviceType", 0);
        //客户端类型，逻辑决定，如：供应链、商家端等
        Integer clientType = getIntParameter("clientType", 0);
        //内部版本号 一般递增 初始为1
        Integer versionIndex = getIntParameter("versionIndex", 0);
        SysVersion sysVersion1 = new SysVersion();
        sysVersion1.setDeviceType(deviceType);
        sysVersion1.setClientType(clientType);
        sysVersion1.setVersionIndex(versionIndex);
        SysUser sysUser = null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //判断当前用户的版本  是否需要升级
        SysVersion sysVersion = dataAccessManager.getUsefulVersion(deviceType,clientType,versionIndex);
        if(sysVersion != null){
            Integer islocal = sysVersion.getIslocal();
            if(islocal.equals(0)){//全局更新
                return success(sysVersion);
            }else{//局部更新
                //查询是否符合条件更新
                String userId = sysUser.getId();
                if(userId != null && userId != ""){
                    SysVersionLocal sysVersionLocal = dataAccessManager.getSysVersionLocal(userId);
                    if (sysVersionLocal != null) {//说明可以升级
                        return success(sysVersion);
                    }
                }
            }
        }
        return fail();
    }

}
