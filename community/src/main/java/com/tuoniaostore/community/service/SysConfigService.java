package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 系统配置信息表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 16:36:22
 */
public interface SysConfigService {

    JSONObject addSysConfig();

    JSONObject getSysConfig();

    JSONObject getSysConfigs();

    JSONObject getSysConfigCount();
    JSONObject getSysConfigAll();

    JSONObject changeSysConfig();
}
