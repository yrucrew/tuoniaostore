package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 用户与角色对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
public interface SysUserRolePosService {

    JSONObject addSysUserRolePos();

    JSONObject getSysUserRolePos();

    JSONObject getSysUserRolePoss();

    JSONObject getSysUserRolePosCount();
    JSONObject getSysUserRolePosAll();

    JSONObject changeSysUserRolePos();
}
