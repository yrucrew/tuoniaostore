package com.tuoniaostore.community.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.user.RetrieveStatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysMenuService;
import com.tuoniaostore.datamodel.user.SysMenu;
import com.tuoniaostore.datamodel.user.SysRoleMenu;
import com.tuoniaostore.datamodel.user.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import org.springframework.transaction.annotation.Transactional;


@Service("sysMenuService")
public class SysMenuServiceImpl extends BasicWebService implements SysMenuService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysMenu() {
        SysMenu sysMenu = new SysMenu();
        String parentId = getUTF("parentId", "0");
        String name = getUTF("name");
        String url = getUTF("url");
        String perms = getUTF("perms", null);
        String remark = getUTF("remark", null);
        int type = getIntParameter("type", 0);
        String icon = getUTF("icon", null);
        int sort = getIntParameter("sort");
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());

        sysMenu.setParentId(parentId);
        if (parentId == "0") {
            sysMenu.setLevel(0);
        } else {
            SysMenu menu = dataAccessManager.getSysMenu(parentId);
            if (menu != null) {
                sysMenu.setLevel(menu.getLevel() + 1);
            } else {
                sysMenu.setLevel(0);
            }
        }
        sysMenu.setName(name);
        sysMenu.setUrl(url);
        sysMenu.setPerms(perms);
        sysMenu.setType(type);
        sysMenu.setIcon(icon);
        sysMenu.setSort(sort);
        sysMenu.setStatus(status);
        SysUser user = null;
        try {
            user = getUser();
            sysMenu.setUserId(user.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        sysMenu.setRemark(remark);
        if (dataAccessManager.findByMenu(name, parentId) == null) {
            dataAccessManager.addSysMenu(sysMenu);
        } else {
            return fail("该菜单已存在");
        }
        return success();
    }

    @Override
    public JSONObject getSysMenu() {
        SysMenu sysMenu = dataAccessManager.getSysMenu(getId());
        return success(sysMenu);
    }

    @Override
    public JSONObject getSysMenus() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysMenu> sysMenus = dataAccessManager.getSysMenus(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysMenuCount(status, name);
        return success(sysMenus, count);
    }

    @Override
    public JSONObject getSysMenuAll() {
        List<SysMenu> sysMenus = dataAccessManager.getSysMenuAll();
        return success(sysMenus);
    }

    @Override
    public JSONObject getSysMenuCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysMenuCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysMenu() {
        SysMenu sysMenu = dataAccessManager.getSysMenu(getId());
        if (sysMenu == null) {
            return fail("数据不存在!!!");
        }
        String parentId = getUTF("parentId", "0");
        //菜单名称
        String name = getUTF("name", null);
        //菜单URL
        String url = getUTF("url", null);
        //授权(多个用逗号分隔，如：user:list,user:create)
        String perms = getUTF("perms", null);
        //类型   0：目录   1：菜单   2：按钮
        Integer type = getIntParameter("type", 0);
        //菜单图标
        String icon = getUTF("icon", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //
        Integer status = getIntParameter("status", 0);
        //级别
        Integer level = getIntParameter("level", 0);

        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);
        if (!name.trim().equals(sysMenu.getName().trim())) {
            if (dataAccessManager.findByMenu(name, parentId) != null) {
                return fail("该菜单已存在");
            }
        }
        if (parentId == "0") {
            sysMenu.setLevel(0);
        } else {
            SysMenu menu = dataAccessManager.getSysMenu(parentId);
            if (menu != null) {
                sysMenu.setLevel(menu.getLevel() + 1);
            } else {
                sysMenu.setLevel(0);
            }
        }
        String remark = getUTF("remark", null);
        sysMenu.setParentId(parentId);
        sysMenu.setName(name);
        sysMenu.setUrl(url);
        sysMenu.setPerms(perms);
        sysMenu.setType(type);
        sysMenu.setIcon(icon);
        sysMenu.setSort(sort);
        sysMenu.setStatus(status);
        sysMenu.setLevel(level);
        try {
            SysUser sysUser = getUser();
            if (sysUser != null) {
                sysMenu.setUserId(sysUser.getUserId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        sysMenu.setRetrieveStatus(retrieveStatus);
        sysMenu.setRemark(remark);
        dataAccessManager.changeSysMenu(sysMenu);
        return success();
    }

    @Override
    public JSONObject getWebSysMenus() {
        JSONArray jsonArray = null;
        try {
            SysUser sysUser = getUser();
            jsonArray = dataAccessManager.getWebSysMenus(sysUser.getRoleId());//
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success(jsonArray, 0);
    }

    @Override
    public JSONObject getPurviewMenus() {
        String roleId = getUTF("roleId", null);

        Map<String, String> roleMenus = new HashMap<>();
        List<SysRoleMenu> roles = dataAccessManager.getSysRoleMenusByRoleId(roleId);
        if (roles != null) {
            int count = roles.size();
            for (int i = 0; i < count; i++) {
                roleMenus.put(roles.get(i).getMenuId(), roleId);
            }
        }
        JSONArray jsonArray = getPurviews("0", roleMenus);

        return success(jsonArray);
    }

    @Override
    public JSONObject getLevelMenus() {

        JSONArray jsonArray = getLevelData("0");
        return success(jsonArray);
    }

    private JSONArray getLevelData(String parentId) {
        List<SysMenu> list = dataAccessManager.getSysMenuChilds(parentId, null);

        JSONArray jsonArray = new JSONArray();
        if (list != null) {
            int count = list.size();
            for (int i = 0; i < count; i++) {
                JSONObject json = new JSONObject();
                json.put("id", list.get(i).getId());
                json.put("name", list.get(i).getName());
                json.put("level", list.get(i).getLevel());
                JSONArray child = null;
                if (list.get(i).getLevel() < 1) {
                    child = getLevelData(list.get(i).getId());
                }

                if (child != null) {
                    json.put("children", child);
                }
                jsonArray.add(json);
            }
        }
        return jsonArray;
    }

    private JSONArray getPurviews(String parentId, Map<String, String> roleMenus) {
        List<SysMenu> list = dataAccessManager.getSysMenuChilds(parentId, null);

        JSONArray jsonArray = new JSONArray();
        if (list != null) {
            int count = list.size();
            for (int i = 0; i < count; i++) {
                JSONObject json = new JSONObject();
                json.put("id", list.get(i).getId());
                json.put("name", list.get(i).getName());
                if (roleMenus.get(list.get(i).getId()) != null) {
                    json.put("checkbox", true);
                }
                JSONArray child = getPurviews(list.get(i).getId(), roleMenus);
                if (child != null) {
                    json.put("children", child);
                }
                jsonArray.add(json);
            }
        }
        return jsonArray;
    }


    private JSONArray getWebMenus(String parentId, List<String> menuIds) throws Exception {
        try {
            List<SysMenu> list = dataAccessManager.getSysMenuChilds(parentId, menuIds);
            JSONArray jsonArray = new JSONArray();
            if (list != null) {
                int count = list.size();
                for (int k = 0; k < count; k++) {
                    JSONObject json = new JSONObject();
                    json.put("title", list.get(k).getName());
                    json.put("icon", list.get(k).getIcon());
                    json.put("jump", list.get(k).getUrl());
                    json.put("name", list.get(k).getName());
                    String id = list.get(k).getId();
                    JSONArray child = getWebMenus(id, menuIds);
                    if (child != null) {
                        json.put("list", child);
                    }
                    jsonArray.add(json);
                }
            }
            return jsonArray;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public JSONObject changeSysMenuRetrieve() {
        dataAccessManager.changeRetrieveStatus("sys_menu", RetrieveStatusEnum.RETRIEVE.getKey(), getId());
        return success();
    }

    /**
     * 删除对应的菜单
     * @author oy
     * @date 2019/5/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    @Transactional
    public JSONObject deleteSysMenu() {
        String ids = getUTF("ids");
        if(ids == null || ids.equals("")){
            return fail("ids不能为空！");
        }
        List<String> menuIds = JSONObject.parseArray(ids, String.class);
        if(menuIds != null && menuIds.size() > 0){
            //删除对应的菜单（逻辑删除） 同时删除对应菜单对应的角色（逻辑删除）
            dataAccessManager.deleteSysRoleMenu(menuIds);//删除对应的角色-菜单
            dataAccessManager.deleteSysMenu(menuIds);//删除菜单
        }
        return success("删除成功!");
    }
}
