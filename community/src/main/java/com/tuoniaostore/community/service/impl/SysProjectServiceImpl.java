package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysProjectService;
import com.tuoniaostore.datamodel.user.SysProject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysProjectService")
public class SysProjectServiceImpl extends BasicWebService implements SysProjectService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysProject() {
        SysProject sysProject = new SysProject();
        //项目名称
        String name = getUTF("name", null);
        Integer sort = getIntParameter("sort", 0);
        //项目logo
        String logo = getUTF("logo", null);
        //项目封面
        String imgurl = getUTF("imgurl", null);
        String parentId = getUTF("parentId", "0");
        sysProject.setName(name);
        sysProject.setParentId(parentId);
        sysProject.setSort(sort);
        sysProject.setLogo(logo);
        sysProject.setImgurl(imgurl);
        dataAccessManager.addSysProject(sysProject);
        return success();
    }

    @Override
    public JSONObject getSysProjectByParentId() {
        List<SysProject> sysProjects = dataAccessManager.getSysProjectByParentId("0");
        return success(sysProjects);
    }
    @Override
    public JSONObject getSysProject() {
        SysProject sysProject = dataAccessManager.getSysProject(getId());
        return success(sysProject);
    }
    @Override
    public JSONObject getSysProjects() {
        String name = getUTF("name", null);
        List<SysProject> sysProjects = dataAccessManager.getSysProjects(getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysProjectCount(name);
        return success(sysProjects, count);
    }

    @Override
    public JSONObject getSysProjectAll() {
        List<SysProject> sysProjects = dataAccessManager.getSysProjectAll();
        return success(sysProjects);
    }

    @Override
    public JSONObject getSysProjectCount() {
        String keyWord = getUTF("keyWord", null);
        int count = dataAccessManager.getSysProjectCount(keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysProject() {
        SysProject sysProject = dataAccessManager.getSysProject(getId());
        //项目名称
        String name = getUTF("name", null);
        //
        Integer sort = getIntParameter("sort", 0);
        //项目logo
        String logo = getUTF("logo", null);
        //项目封面
        String imgurl = getUTF("imgurl", null);
        String parentId = getUTF("parentId", "0");
        sysProject.setName(name);
        sysProject.setSort(sort);
        sysProject.setLogo(logo);
        sysProject.setImgurl(imgurl);
        sysProject.setParentId(parentId);
        dataAccessManager.changeSysProject(sysProject);
        return success();
    }

    @Override
    public JSONObject getSysProjectTree() {
        return success(dataAccessManager.getSysProjectTree());
    }
}
