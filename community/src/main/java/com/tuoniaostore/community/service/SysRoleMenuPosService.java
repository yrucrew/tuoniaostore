package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 角色与菜单对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-15 10:54:39
 */
public interface SysRoleMenuPosService {

    JSONObject addSysRoleMenuPos();

    JSONObject getSysRoleMenuPos();

    JSONObject getSysRoleMenuPoss();

    JSONObject getSysRoleMenuPosCount();
    JSONObject getSysRoleMenuPosAll();

    JSONObject changeSysRoleMenuPos();

    JSONObject addSysRoleMenuPosInMenuIdsByRoleId();
}
