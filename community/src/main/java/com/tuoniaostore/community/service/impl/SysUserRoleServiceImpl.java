package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysUserRoleService;
import com.tuoniaostore.datamodel.user.SysUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysUserRoleService")
public class SysUserRoleServiceImpl extends BasicWebService implements SysUserRoleService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysUserRole() {
        SysUserRole sysUserRole = new SysUserRole();
        //用户ID
        String userId = getUTF("userId", null);
        //角色ID
        String roleId = getUTF("roleId", null);
        Integer status = getIntParameter("status", 0);
        //
        String remark = getUTF("remark", null);

        sysUserRole.setUserId(userId);
        sysUserRole.setRoleId(roleId);
        sysUserRole.setStatus(status);
        sysUserRole.setRemark(remark);
        dataAccessManager.addSysUserRole(sysUserRole);
        return success();
    }

    @Override
    public JSONObject getSysUserRole() {
        SysUserRole sysUserRole = dataAccessManager.getSysUserRole(getId());
        return success(sysUserRole);
    }

    @Override
    public JSONObject getSysUserRoles() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysUserRole> sysUserRoles = dataAccessManager.getSysUserRoles(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysUserRoleCount(status, name);
        return success(sysUserRoles, count);
    }

    @Override
    public JSONObject getSysUserRoleAll() {
        List<SysUserRole> sysUserRoles = dataAccessManager.getSysUserRoleAll();
        return success(sysUserRoles);
    }

    @Override
    public JSONObject getSysUserRoleCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysUserRoleCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysUserRole() {
        SysUserRole sysUserRole = dataAccessManager.getSysUserRole(getId());
        //
        String id = getUTF("id", null);
        //用户ID
        String userId = getUTF("userId", null);
        //角色ID
        String roleId = getUTF("roleId", null);

        Integer status = getIntParameter("status", 0);
        //
        String remark = getUTF("remark", null);
        sysUserRole.setId(id);
        sysUserRole.setUserId(userId);
        sysUserRole.setRoleId(roleId);
        sysUserRole.setStatus(status);
        sysUserRole.setRemark(remark);
        dataAccessManager.changeSysUserRole(sysUserRole);
        return success();
    }

}
