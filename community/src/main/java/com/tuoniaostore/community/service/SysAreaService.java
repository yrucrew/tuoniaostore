package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
public interface SysAreaService {

    JSONObject addSysArea();

    JSONObject getSysArea();

    JSONObject getSysAreas();
    JSONObject getSysSelArea();

    JSONObject getSysAreaCount();
    JSONObject getSysAreaAll();

    JSONObject changeSysArea();


    /**
     * 根据地区名 模糊搜索 分页
     * @author oy
     * @date 2019/4/27
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getSysAreaAllByPage();

    JSONObject getSysAreasByIds();

    JSONObject getSysAreasByNameLike();

    JSONObject validateSysArea();

    JSONObject deleteSysAreas();
}
