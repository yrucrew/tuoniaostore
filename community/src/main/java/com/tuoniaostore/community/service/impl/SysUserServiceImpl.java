package com.tuoniaostore.community.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.CommonUtils;
import com.tuoniaostore.commons.RSA;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.constant.DeviceTypeEnum;
import com.tuoniaostore.commons.constant.MessageStatus;
import com.tuoniaostore.commons.constant.MessageType;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.payment.CurrencyTypeEnum;
import com.tuoniaostore.commons.constant.user.*;
import com.tuoniaostore.commons.support.payment.PaymentCurrencyAccountRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplychainShopPropertiesRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplychainShopBindRelationshipRemoteService;
import com.tuoniaostore.commons.support.supplychain.*;
import com.tuoniaostore.commons.utils.MessageUtils;
import com.tuoniaostore.commons.utils.WeiXinPayUtils;
import com.tuoniaostore.community.CommunityAop;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.redis.RedisService;
import com.tuoniaostore.community.service.SysUserChannelService;
import com.tuoniaostore.community.service.SysUserService;
import com.tuoniaostore.community.vo.PurchaseUserVo;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyAccount;
import com.tuoniaostore.datamodel.supplychain.PosWork;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopBindRelationship;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.user.*;
import org.apache.catalina.User;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;


@Service("sysUserService")
public class SysUserServiceImpl extends BasicWebService implements SysUserService {
    private static final Logger logger = Logger.getLogger(SysUserServiceImpl.class);
    private static final org.slf4j.Logger loggerAop = LoggerFactory.getLogger(CommunityAop.class);
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Autowired
    private RedisService redisService;

    @Autowired
    private SysUserChannelService sysUserChannelService;

    private void fillSysUser(SysUser sysUser) {
        //默认登录用户名 全世界唯一  *
        String defaultName = getUTF("defaultName", null);
        //用于展示的名字(可重复)
        String showName = getUTF("showName", null);
        //用户真实名(方便扩展实名认证的功能)
        String realName = getUTF("realName", null);
        //身份证号
        String idNumber = getUTF("idNumber", null);
        //电话号码(不局限于手机号)
        String phoneNumber = getUTF("phoneNumber", null);
        //用户性别(0:女性 1:男性；默认值:0)
        Integer sex = getIntParameter("sex", 0);
        //用户类型(普通用户，VIP用户等，由逻辑设定)51,无人便利店，52无人售货架
        Integer type = getIntParameter("type", 0);
        //当前的状态(如：正常、禁言、禁登、黑名单等)
        Integer status = getIntParameter("status", 0);
        //增值税税率
        Double taxRate = getDoubleParameter("taxRate", 0);
        //注册的渠道(如：官网、第三方渠道等；用于分析推广方式)
        String fromChannel = getUTF("fromChannel", FromChannelEnum.FromChannel_BACKSTAGE.getKey() + "");
        //设备类型(分析用户群，可针对不同用户开展不同的活动)
        Integer deviceType = getIntParameter("deviceType", 0);
        //设备名称
        String deviceName = getUTF("deviceName", null);
        //
        String code = getUTF("code", null);
        String roleId = getUTF("roleId", null);
        //
        Float longitude = getFloatParameter("longitude", 0);
        //
        Float latitude = getFloatParameter("latitude", 0);
        //
        String passportcol = getUTF("passportcol", null);
        String password = getUTF("password", null);
        //排序
        Integer sort = getIntParameter("sort", 0);
        //备注
        String remark = getUTF("remark", null);
        //网络类型
        String networkType = getUTF("networkType", null);
        //机器码
        String machineCode = getUTF("machineCode", null);
        //机器品牌
        String machineBrand = getUTF("machineBrand", null);
        //系统版本
        String systemVersion = getUTF("systemVersion", null);
        //软件版本号
        String versionIndex = getUTF("versionIndex", null);
        sysUser.setDefaultName(defaultName);
        sysUser.setShowName(showName);
        sysUser.setRealName(realName);
        sysUser.setIdNumber(idNumber);
        sysUser.setPhoneNumber(phoneNumber);
        sysUser.setSex(sex);
        sysUser.setRoleId(roleId);
        sysUser.setType(type);
        sysUser.setStatus(status);
        sysUser.setTaxRate(taxRate);

        //通过渠道名字 获取渠道id
        SysUserChannel sysUserChannelByName = sysUserChannelService.getSysUserChannelByName(fromChannel);
        if (sysUserChannelByName != null) {
            sysUser.setFromChannel(sysUserChannelByName.getId());
        }
        sysUser.setDeviceType(deviceType);
        sysUser.setDeviceName(deviceName);
        sysUser.setCode(code);
        sysUser.setLongitude(longitude);
        sysUser.setLatitude(latitude);
        sysUser.setPassportcol(passportcol);
        sysUser.setSort(sort);
        sysUser.setRemark(remark);
        sysUser.setNetworkType(networkType);
        sysUser.setMachineCode(machineCode);
        sysUser.setMachineBrand(machineBrand);
        sysUser.setSystemVersion(systemVersion);
        sysUser.setVersionIndex(versionIndex);
        sysUser.setPassword(encryptionPassword(password));
        sysUser.setRetrieveStatus(RetrieveStatusEnum.NORMAL.getKey());
    }


    @Override
    @Transactional
    public JSONObject addSysUser() {
        SysUser sysUser = new SysUser();
        fillSysUser(sysUser);
        sysUser.setUserId(getUser().getId());
        if (sysUser.getPassword() == null) {
            return fail("密码不能为空!");
        }
        //终端添加
        String terminal = getUTF("terminal");
        List<SysUserTerminal> sysUserTerminals = new ArrayList<>();
        String[] terminals = terminal.split(",");
        for (int i = 0; i < terminals.length; i++) {
            SysUserTerminal sysUserTerminal = new SysUserTerminal();
            sysUserTerminal.setUserId(sysUser.getId());
            sysUserTerminal.setTerminalId(terminals[i]);
            sysUserTerminals.add(sysUserTerminal);
        }
        dataAccessManager.addSysUserTerminals(sysUserTerminals);//避免重复插入
        //异步创建一个余额/会员账户，初始化账户
        addPaymentCurrencyAccountSync(sysUser);
        dataAccessManager.addSysUser(sysUser);
        return success();
    }


    @Override
    public JSONObject getSysUser() {
        SysUser sysUser = dataAccessManager.getSysUser(getId());
        return success(sysUser);
    }

    @Override
    public JSONObject getCurrentSysUser() {
        String id = getUser().getId();
        SysUser sysUser = dataAccessManager.getSysUser(id);
        //查询一下 终端
        List<SysTerminal> terminals = dataAccessManager.getSysUserTerminalListByUserId(id);
        sysUser.setList(terminals);
        return success(sysUser);
    }

    /**
     * 通过手机号码获取用户
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/9
     */
    @Override
    public JSONObject getSysUserByPhoneNumForRemote() {
        String phoneNum = getUTF("phoneNum");
        JSONObject jsonObject = new JSONObject();
        if (phoneNum != null && !phoneNum.equals("")) {
            SysUser sysUserByPhoneNumber = dataAccessManager.getSysUserByPhoneNumber(phoneNum);
            jsonObject.put("response", sysUserByPhoneNumber);
        }
        return jsonObject;
    }

    @Override
    public JSONObject addSysUserByRemote() {
        String userEntity = getUTF("userEntity");
        if (userEntity != null && !userEntity.equals("")) {
            SysUser sysUser = JSONObject.parseObject(userEntity, SysUser.class);
            if (sysUser != null) {
                dataAccessManager.addSysUser(sysUser);
            }
        }
        return success();
    }

    @Override
    public JSONObject addSysUserForPosByRemote() {
        String userEntity = getUTF("userEntity");
        SysUser sysUser = JSONObject.parseObject(userEntity, SysUser.class);
        if (sysUser != null) {
            // 添加用户
            dataAccessManager.addSysUserForPos(sysUser);
            // 绑定用户-终端
            SysUserTerminal sysUserTerminal = new SysUserTerminal();
            sysUserTerminal.setTerminalId(getUTF("terminal"));
            sysUserTerminal.setUserId(sysUser.getId());
            dataAccessManager.addSysUserTerminal(sysUserTerminal);//用户终端表添加数据
            // 绑定权限
            SysUserRolePos sysUserRolePos = new SysUserRolePos();
            sysUserRolePos.setUserId(sysUser.getId());
            sysUserRolePos.setRoleId(sysUser.getPosRoleId());
            dataAccessManager.addSysUserRolePos(sysUserRolePos);
            // 初始化账户余额
            addPaymentCurrencyAccountSync(sysUser);
        }
        return success();
    }

    @Override
    @Transactional
    public JSONObject delSysUserByUserId() {
        String id = getUTF("id");
        if (id == null || id.equals("")) {
            return fail("id不能为空");
        }
        dataAccessManager.delSysUserByUserId(id);
        return success("删除成功！");
    }

    /**
     * 修改用户
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/11
     */
    @Override
    @Transactional
    public JSONObject changeSysUser2() {
        String userEntity = getUTF("userEntity");
        if (userEntity == null || userEntity.equals("")) {
            return fail("userEntity不能为空！");
        }
        SysUser sysUser = JSONObject.parseObject(userEntity, SysUser.class);
        if (sysUser != null) {
            //验证当前修改的手机号码 和 defaultName是否存在
            String phoneNumber = sysUser.getPhoneNumber();
            String defaultName = sysUser.getDefaultName();
            //先修改终端和角色
            String userId = sysUser.getId();
            SysUser sysUser1 = dataAccessManager.findSysUserByPhoneOrDefaultName(phoneNumber, defaultName);
            if (sysUser1 != null) {//说明有一样了
                if (!userId.equals(sysUser1.getId())) {//是否是当前用户
                    String phoneNumber1 = sysUser1.getPhoneNumber();
                    if (phoneNumber1 != null && !phoneNumber1.equals("") && phoneNumber != null && !phoneNumber.equals("")) {
                        if (phoneNumber.equals(phoneNumber1)) {
                            return fail("手机号码已存在!");
                        }
                    }
                    String defaultName1 = sysUser1.getDefaultName();
                    if (defaultName1 != null && !defaultName1.equals("") && defaultName != null && !defaultName.equals("")) {
                        if (defaultName.equals(sysUser1.getDefaultName())) {
                            return fail("用户名已存在!");
                        }
                    }
                }
            }
            List<String> sysTerminalIdsList = sysUser.getSysTerminalIdsList();
            dataAccessManager.delSysUserTerminal(userId);//清除用户终端
            if (sysTerminalIdsList != null && sysTerminalIdsList.size() > 0) {
                List<SysUserTerminal> sysUserTerminalList = new ArrayList<>();
                sysTerminalIdsList.forEach(x -> {
                    SysUserTerminal sysUserTerminal = new SysUserTerminal();
                    sysUserTerminal.setUserId(userId);
                    sysUserTerminal.setTerminalId(x);
                    sysUserTerminalList.add(sysUserTerminal);
                });
                dataAccessManager.addSysUserTerminals(sysUserTerminalList);//添加终端
            }
            String roleId = sysUser.getRoleId();
            if (roleId != null && !roleId.equals("")) {
                SysUserRole sysUserRole = dataAccessManager.getSysUserRoleByUser(userId);
                if (sysUserRole != null) {//防止小程序注册的时候没有角色 导致不能修改
                    sysUserRole.setRoleId(roleId);
                    sysUserRole.setUserId(userId);
                    dataAccessManager.updateSysUserRoleByParam(sysUserRole);
                } else {
                    SysUserRole sysUserRole1 = new SysUserRole();
                    sysUserRole1.setRoleId(roleId);
                    sysUserRole1.setUserId(userId);
                    sysUserRole1.setStatus(0);
                    sysUserRole1.setOperationId(getUser().getId());
                    sysUserRole1.setRetrieveStatus(0);
                    dataAccessManager.addSysUserRole(sysUserRole1);
                }
            }
            dataAccessManager.updateSysUserParam(sysUser);
        }
        return success("修改成功！");
    }

    @Override
    public JSONObject validateSysUserByPhoneNumberOrDefaultName() {
        String userId = getUTF("userId", null);
        String phoneNumber = getUTF("phoneNumber", null);
        String defaultName = getUTF("defaultName", null);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 0);
        if (phoneNumber != null || defaultName != null) {
            SysUser sysUserByPhoneOrDefaultName = dataAccessManager.findSysUserByPhoneOrDefaultName(phoneNumber, defaultName);
            if (sysUserByPhoneOrDefaultName != null) {
                if (userId != null) {
                    String id = sysUserByPhoneOrDefaultName.getId();
                    if (!userId.equals(id)) {
                        jsonObject.put("key", 1);
                        jsonObject.put("msg", "该用户已存在！");
                    } else {
                        jsonObject.put("key", 0);
                        jsonObject.put("msg", "可修改!");
                    }
                } else {
                    jsonObject.put("key", 1);
                    jsonObject.put("msg", "该用户已存在！");
                }
            } else {
                jsonObject.put("key", 0);
                jsonObject.put("msg", "用户没有重复！");
            }
            return jsonObject;
        }
        return jsonObject;
    }

    @Override
    public JSONObject getSysUserAccountOrName() {
        String accountOrName = getUTF("accountOrName");
        List<SysUser> sysUserAccountOrName = dataAccessManager.getSysUserAccountOrName(accountOrName);
        return success(sysUserAccountOrName);
    }

    @Override
    public JSONObject updateSysUserByPosByRemote() {
        SysUser sysUser = getParseParamValue(SysUser.class);
        dataAccessManager.updateSysUserByPosByRemote(sysUser);
        return success();
    }

    @Override
    public JSONObject getAllUserByRoleIds() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<Integer> roleIds = new ArrayList<>();
        for (String role : getUTF("roleIds").split(",")) {
            roleIds.add(Integer.parseInt(role));
        }
        if (CollectionUtils.isEmpty(roleIds)) {
            return fail("无入参：角色id");
        }
        ArrayList<String> roles = new ArrayList<>();
        for (Integer roleId : roleIds) {
            UserRoleTypeEnum roleName = UserRoleTypeEnum.getUserRoleTypeEnum(roleId);
            roles.add(dataAccessManager.getSysRoleByRoleName(roleName).getId());
        }
        List<SysUser> sysUsers = dataAccessManager.getAllUserByRoleIds(name, status, roles);

        if (!CollectionUtils.isEmpty(sysUsers)) {
            sysUsers.stream().map(item -> {
                List<SysUserTerminal> sysUserTerminals = item.getSysUserTerminals();
                if (!CollectionUtils.isEmpty(sysUserTerminals)) {
                    List<String> terminalIds = sysUserTerminals.stream().map(SysUserTerminal::getTerminalId).collect(Collectors.toList());
                    //查询数据库 获取对应的终端
                    String title = dataAccessManager.getSysTerminalTitleByList(terminalIds);
                    item.setTerminalName(title);
                }
                return item;
            }).collect(Collectors.toList());
        }
        return success(sysUsers);
    }

    @Override
    public JSONObject getSysUserByPhoneNumLike() {
        String phoneNum = getUTF("phoneNum");
        List<SysUser> list = dataAccessManager.getSysUserByPhoneNumLike(phoneNum);
        return success(list);
    }


 /*   @Override
    public JSONObject addPosWorktest() {
        SysUser sysUser = new SysUser();
        sysUser.setId("bded5a1cdfc049a680e41d6e7d832c1fXrirIM");
        addPosWork(sysUser);
        return null;
    }*/


    @Override
    public JSONObject getSysUsers() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysUser> sysUsers = dataAccessManager.getSysUsers(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        if (sysUsers != null && sysUsers.size() > 0) {
            sysUsers.stream().map(item -> {
                List<SysUserTerminal> sysUserTerminals = item.getSysUserTerminals();
                if (sysUserTerminals != null && sysUserTerminals.size() > 0) {
                    List<String> terminalIds = sysUserTerminals.stream().map(SysUserTerminal::getTerminalId).collect(Collectors.toList());
                    //查询数据库 获取对应的终端
                    String title = dataAccessManager.getSysTerminalTitleByList(terminalIds);
                    item.setTerminalName(title);
                }
                return item;
            }).collect(Collectors.toList());
        }
        int count = dataAccessManager.getSysUserCount(status, name);
        System.out.println(sysUsers.size());
        return success(sysUsers, count);
    }

    /**
     * 重新修改查找用户 通过条件查找
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/10
     */
    @Override
    public JSONObject getSysUsers2() {
        String defaultName = getUTF("defaultName", null);
        String realName = getUTF("realName", null);
        String idNumber = getUTF("idNumber", null);
        String phoneNumber = getUTF("phoneNumber", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());

        Map<String, Object> map = new HashMap<>();
        map.put("defaultName", defaultName);
        map.put("realName", realName);
        map.put("phoneNumber", phoneNumber);
        map.put("idNumber", idNumber);
        map.put("status", status);

        List<SysUser> sysUsers = dataAccessManager.getSysUserByParam(map, getPageStartIndex(getPageSize()), getPageSize());
        int count = 0;
        if (sysUsers != null && sysUsers.size() > 0) {
            count = dataAccessManager.getCountSysUserByParam(map);
            sysUsers.stream().map(item -> {
                List<SysUserTerminal> sysUserTerminals = item.getSysUserTerminals();
                if (sysUserTerminals != null && sysUserTerminals.size() > 0) {
                    List<String> terminalIds = sysUserTerminals.stream().map(SysUserTerminal::getTerminalId).collect(Collectors.toList());
                    //查询数据库 获取对应的终端
                    String title = dataAccessManager.getSysTerminalTitleByList(terminalIds);
                    item.setTerminalName(title);
                }
                return item;
            }).collect(Collectors.toList());
        }
        return success(sysUsers, count);
    }

    @Override
    public JSONObject getSysUserAll() {
        List<SysUser> sysUsers = dataAccessManager.getSysUserAll();
        return success(sysUsers);
    }

    @Override
    public JSONObject getSysUserByName() {
        String name = getUTF("name");
        if (StringUtils.isEmpty(name)){
            return fail("参数不能为空");
        }
        List<SysUser> sysUsers = dataAccessManager.getSysUserByName(name);
        return success(sysUsers);
    }

    @Override
    public JSONObject getSysUserCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysUserCount(status, keyWord);
        return success();
    }


    @Override
    @Transactional
    public JSONObject changeSysUser() {
        String userId = null;
        userId = getId();
        SysUser sysUser = dataAccessManager.getSysUser(getId());
        fillSysUser(sysUser);
        String terminal = getUTF("terminal");
        List<SysUserTerminal> sysUserTerminals = new ArrayList<>();
        String[] terminals = terminal.split(",");
        for (int i = 0; i < terminals.length; i++) {
            SysUserTerminal sysUserTerminal = new SysUserTerminal();
            sysUserTerminal.setUserId(sysUser.getId());
            sysUserTerminal.setTerminalId(terminals[i]);
            sysUserTerminals.add(sysUserTerminal);
        }
        if (userId != null && !userId.equals("")) {
            dataAccessManager.delSysUserTerminal(userId);
        }
        dataAccessManager.addSysUserTerminals(sysUserTerminals);
        dataAccessManager.changeSysUser(sysUser);
        return success();
    }


    /**
     * 注册
     *
     * @return
     */
    @Override
    public JSONObject register() {
        String phoneNum = getUTF("phoneNum");//手机号码
        String validCode = getUTF("validCode");
        String terminal = getUTF("terminal");//获取终端类型
        String password = getUTF("password");//登录密码

        //手机号码不能为空
        if (phoneNum == null || phoneNum == "") {
            return fail("手机号码不能为空!");
        }

        //密码不能为空
        if (password == null || password == "") {
            return fail("密码不能为空!");
        }

        //验证码是否正确
        String messageCode = redisService.getMessageCode(MessageType.MESSAGE_TYPE_REGISTER.getPrefix() + phoneNum);//redis获取验证码
        if (messageCode == null) {
            return fail("验证码无效!");
        }
        if (validCode.equals(messageCode)) {
            // 需要添加用户信息
            SysUser sysUser = new SysUser();
            fillSysUser(sysUser);
            sysUser.setPhoneNumber(phoneNum);
            sysUser.setPassword(encryptionPassword(password));//生成加密 保存
            dataAccessManager.addSysUser(sysUser);//用户表添加数据
            SysUserTerminal sysUserTerminal = new SysUserTerminal();
            sysUserTerminal.setTerminalId(terminal);
            sysUserTerminal.setUserId(sysUser.getId());
            dataAccessManager.addSysUserTerminal(sysUserTerminal);//用户终端表添加数据
            //异步创建一个余额/会员账户，初始化账户
            addPaymentCurrencyAccountSync(sysUser);
        } else {
            return fail("验证码错误!");
        }
        return success("注册成功!");
    }

    /**
     * 初始化账户余额
     *
     * @param sysUser
     */
    @Async
    public void addPaymentCurrencyAccountSync(SysUser sysUser) {
        PaymentCurrencyAccount paymentCurrencyAccount = new PaymentCurrencyAccount();
        paymentCurrencyAccount.setUserId(sysUser.getId());//用户id
        paymentCurrencyAccount.setCurrencyType(CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());//余额
        paymentCurrencyAccount.setName(CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getValue());//余额
        paymentCurrencyAccount.setChannelId(sysUser.getFromChannel());//渠道
        try {
            PaymentCurrencyAccountRemoteService.addPaymentCurrencyAccount(paymentCurrencyAccount, getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 注册：获取手机注册码
     *
     * @return
     */
    @Override
    public JSONObject getMsgCode() {
        //获取手机号码
        String phoneNum = getUTF("phoneNum");
        String terminal = getUTF("terminal");

        if (terminal == null || terminal.equals("")) {
            return fail("终端不能为空！");
        }

        String code = "";
        if (phoneNum == null && phoneNum.equals("")) {
            return fail("手机号码不能为空");
        }

        //查看当前手机号码是否被注册过
        SysUser sysUserByPhoneNumber = dataAccessManager.getSysUserByPhoneNumber(phoneNum);//2019-04-01修改

        if (sysUserByPhoneNumber != null) {//说明有注册过这些用户
            //当前手机号码已经注册了
            return fail("当前号码已经被注册了！");
        }

        //发短信
        SysUserSmsLogger sysUserSmsLogger = new SysUserSmsLogger();
        try {
            code = MessageUtils.sendMessage(phoneNum);
            sysUserSmsLogger.setStatus(MessageStatus.MESSAGE_STATUS_SUCCESS.getKey());
        } catch (Exception e) {
            sysUserSmsLogger.setStatus(MessageStatus.MESSAGE_STATUS_FAILD.getKey());
            e.printStackTrace();
        } finally {
            sysUserSmsLogger.setPhone(phoneNum);
            sysUserSmsLogger.setContent("注册验证码:" + code);
            sysUserSmsLogger.setCode(code);
            sysUserSmsLogger.setType(MessageType.MESSAGE_TYPE_RECHANGEPHONE.getKey());
            dataAccessManager.addSysUserSmsLogger(sysUserSmsLogger); //加入短信记录
        }
        redisService.setMessageCode(MessageType.MESSAGE_TYPE_REGISTER.getPrefix() + phoneNum, code);//保存验证码

        return success("验证码已发送");
    }

    /**
     * 用过手机号查找密码 发短信
     *
     * @return
     */
    @Override
    public JSONObject findPasswordByPhone() {
        //用户资料
        String phoneNum = getUTF("phoneNum");//电话号码
        String terminal = getUTF("terminal");//终端id

        //获取当前手机号码 是否本人 短信验证
        if (phoneNum == null || phoneNum.equals("")) {
            return fail("手机号码不能为空！");
        }

        //终端id
        if (terminal == null || terminal.equals("")) {
            return fail("终端不能为空！");
        }

        SysUser sysUserByPhoneNumber = dataAccessManager.getSysUserByPhoneNumber(phoneNum);
        if (sysUserByPhoneNumber != null) {//说明有这些用户
            //判断这些用户id 是否已经在这些终端上注册过了
            String ids = sysUserByPhoneNumber.getId();//用户id
            SysUserTerminal sysUserTerminal2 = dataAccessManager.getSysUserTerminal2(terminal, ids);//查找唯一的终端

            if (sysUserTerminal2 != null) {//说明该用户已经注册该终端
                //短信验证
                SysUserSmsLogger sysUserSmsLogger = new SysUserSmsLogger();
                String code = "";
                try {
                    code = MessageUtils.sendMessage(phoneNum);
                    sysUserSmsLogger.setStatus(MessageStatus.MESSAGE_STATUS_SUCCESS.getKey());
                } catch (Exception e) {
                    sysUserSmsLogger.setStatus(MessageStatus.MESSAGE_STATUS_FAILD.getKey());
                    e.printStackTrace();
                } finally {
                    sysUserSmsLogger.setPhone(phoneNum);
                    sysUserSmsLogger.setContent("找回密码验证码:" + code);
                    sysUserSmsLogger.setCode(code);
                    sysUserSmsLogger.setType(MessageType.MESSAGE_TYPE_FINDPASSWORD.getKey());
                    dataAccessManager.addSysUserSmsLogger(sysUserSmsLogger); //加入短信记录
                }
                redisService.setMessageCode(MessageType.MESSAGE_TYPE_FINDPASSWORD.getPrefix() + phoneNum, code);//加入session保存
            } else {
                return fail("此用户没有该终端权限！");
            }
        }
        return success("短信发送成功！");
    }

    /**
     * 找回密码
     *
     * @return
     */
    @Override
    public JSONObject findPassword() {
        String phoneNum = getUTF("phoneNum");
        String validCode = getUTF("validCode");//验证码
        String password = getUTF("password");
        String password2 = getUTF("password2");//重复密码

        if (phoneNum == null || phoneNum.equals("")) {//手机号码为空
            return fail("手机号码不能为空！");
        }

        if (validCode == null || validCode.equals("")) {//验证码为空
            return fail("验证码不能为空！");
        }

        if (password != null && !password.equals("") && password2 != null && !password2.equals("") && password.equals(password2)) {//验证密码输入是否正确
            String sessionAttribute = redisService.getMessageCode(MessageType.MESSAGE_TYPE_FINDPASSWORD.getPrefix() + phoneNum);//获取短信验证码
            if (validCode.equals(sessionAttribute)) {//验证 验证码
                //修改密码
                password = encryptionPassword(password);//加密密码
                dataAccessManager.updatePasswordByPhoneNum(phoneNum, password);
                //返回成功信息
                return success("修改密码成功！");
            } else {//失败
                return fail("验证码错误!");
            }

        } else {
            return fail("密码输入有误！");
        }
    }

    /**
     * 修改用户登录密码
     *
     * @return
     */
    @Override
    public JSONObject changeSysUserLoginPassword() {
        String password = getUTF("password");
        String newPassword = getUTF("newPassword");
        String newPassword2 = getUTF("newPassword2");
        if (password == null || password.equals("")) {//验证原密码
            return fail("密码不能为空");
        }

        if (newPassword == null || newPassword2 == null || newPassword.equals("") || newPassword2.equals("")) {//验证输入的新密码不能为空
            return fail("密码不能为空");
        } else if (!newPassword.equals(newPassword2)) {
            return fail("新密码两次输入的不一致");
        }

        //获取当前用户
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //获取原登录密码
        SysUser sysUser = dataAccessManager.getSysUser(user.getId());
        String passwordMeta = sysUser.getPassword();//数据库原登录密码
        String oldPassword = encryptionPassword(password);
        String newPasswordEn = encryptionPassword(newPassword);//新密码
        //验证是否正确
        if (oldPassword.equals(passwordMeta)) {//密码相同
            //判断原密码和新密码是否一样 如果一样 不能修改
            if (newPasswordEn.equals(passwordMeta)) {
                return fail("新密码不能和原密码一样！");
            }
            //修改新的密码
            dataAccessManager.updatePasswordById(user.getId(), newPasswordEn);
            return success("修改密码成功！");
        } else {//错误
            return fail("原密码错误！");
        }
    }

    /**
     * 重绑手机号码
     *
     * @return
     */
    @Override
    public JSONObject changeSysUserPhoneNum() {
        //获取当前用户
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //获取手机号码
        String newPhoneNum = getUTF("newPhoneNum");
        //获取原来手机号码
        String phoneNum = getUTF("phoneNum");
        //获取当前验证码
        String validCode = getUTF("validCode");

        String terminal = getUTF("terminal");//终端id

        //验证当前号码
        if (newPhoneNum == null) {
            return fail("手机号不能为空");
        }
        //验证旧号码对不对
        SysUser sysUserByPhoneNumber = dataAccessManager.getSysUserByPhoneNumber(phoneNum);//查找当前电话的所有端用户
        String id = sysUserByPhoneNumber.getId();
        if (sysUserByPhoneNumber != null) {//说明有这些用户
            //判断这些用户id 是否已经在这些终端上注册过了
            SysUserTerminal sysUserTerminal2 = dataAccessManager.getSysUserTerminal2(terminal, id);
            if (sysUserTerminal2 != null) {//查看该用户终端是否有
                //获取验证码
                String code = redisService.getMessageCode(MessageType.MESSAGE_TYPE_RECHANGEPHONE + phoneNum);
                if (!code.equals(validCode)) {
                    return fail("验证码错误！");
                }
            }
            //更新手机号码
            dataAccessManager.changeSysUserPhoneNum(user.getId(), newPhoneNum);
            return success("重绑手机号码成功！");
        } else {
            return fail("手机号码错误！");
        }
    }

    /**
     * 我的：重绑手机 获取验证码
     *
     * @return
     */
    @Override
    public JSONObject changeSysUserPhoneNumGetMsg() {
        //获取手机号码
        String newPhoneNum = getUTF("newPhoneNum", null);
        //获取原来手机号码
        String phoneNum = getUTF("phoneNum", null);
        //获取终端id
        String terminal = getUTF("terminal", null);

        //获取当前用户
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //验证当前号码
        if (newPhoneNum == null) {
            return fail("新手机号不能为空");
        }
        if (phoneNum == null) {
            return fail("原手机号不能为空");
        }
        if (terminal == null) {
            return fail("终端号不能为空");
        }

        //验证旧号码对不对
        SysUser sysUserByPhoneNumber1 = dataAccessManager.getSysUserByPhoneNumber(phoneNum);
        if (sysUserByPhoneNumber1 != null) {//说明有这些用户
            //判断这些用户id 是否已经在这些终端上注册过了
            String id = sysUserByPhoneNumber1.getId();
            SysUserTerminal sysUserTerminal3 = dataAccessManager.getSysUserTerminal2(terminal, id);
            if (sysUserTerminal3 == null) {//说明号码不正确
                return fail("原号码输入有误！");
            }
        }
        //发短信给旧号码 验证 新号码是否之前有绑定过
        SysUser sysUserByPhoneNumber = dataAccessManager.getSysUserByPhoneNumber(newPhoneNum);
        if (sysUserByPhoneNumber != null) {//说明号码已经注册过
            //判断这些用户id 是否已经在这些终端上注册过了
            String id = sysUserByPhoneNumber.getId();
            SysUserTerminal sysUserTerminal3 = dataAccessManager.getSysUserTerminal2(terminal, id);
            if (sysUserTerminal3 != null) {
                return fail("该手机号码已经被绑定！");
            }
        }

        String code = "";
        SysUserSmsLogger sysUserSmsLogger = new SysUserSmsLogger();
        try {
            code = MessageUtils.sendMessage(phoneNum);
            sysUserSmsLogger.setStatus(MessageStatus.MESSAGE_STATUS_SUCCESS.getKey());
        } catch (Exception e) {
            sysUserSmsLogger.setStatus(MessageStatus.MESSAGE_STATUS_FAILD.getKey());
            e.printStackTrace();
        } finally {
            sysUserSmsLogger.setPhone(phoneNum);
            sysUserSmsLogger.setContent("重绑手机验证码:" + code);
            sysUserSmsLogger.setCode(code);
            sysUserSmsLogger.setType(MessageType.MESSAGE_TYPE_RECHANGEPHONE.getKey());
            dataAccessManager.addSysUserSmsLogger(sysUserSmsLogger); //加入短信记录
        }
        redisService.setMessageCode(MessageType.MESSAGE_TYPE_RECHANGEPHONE + phoneNum, code);//存放验证码
        return success("发送成功");
    }


    /**
     * 登录
     *
     * @return
     */
    @Override
    @Transactional
    public JSONObject login() {
        String username = getUTF("username");
        String password = getUTF("password");
        int deviceType = getIntParameter("versionType", DeviceTypeEnum.DEVICE_TYPE_ANDROID.getKey());
        String deviceName = getUTF("deviceName", "");
        String appKey = getUTF("appKey");
        String vercode = getUTF("vercode", null);//
        String codeKey = getUTF("codeKey", null);//
        String networkType = getUTF("networkType", "");
        String machineCode = getUTF("machineCode", "");
        String machineBrand = getUTF("machineBrand", "");
        String systemVersion = getUTF("systemVersion", "");
        String isPosUser = getUTF("isPosUser", null);
        Float latitude = getFloatParameter("latitude", 0);
        Float longitude = getFloatParameter("longitude", 0);
        // 当前版本号
        int versionIndex = getIntParameter("versionIndex", 0);

        password = encryptionPassword(password);

        //获取当前手机号码的所有id
        SysUser sysUser = dataAccessManager.getSysUserByPhoneNumber(username);//用户实体

        if (sysUser == null) {
            logger.error("登录失败，错误的用户名 -- " + username);
            return fail("错误的用户名!");
        }
        String terminal = getUTF("terminal");//获取终端类型
        String id = sysUser.getId();
        //查询当前用户是否有该终端
        SysUserTerminal sysUserTerminal = dataAccessManager.getSysUserTerminal2(terminal, id);
        //说明该终端没有该用户
        if (sysUserTerminal == null) {
            logger.error("登录失败，你没有该终端权限 -- " + username);
            return fail("你没有该终端登录权限！");
        }
        //只有后台有验证码 其他不用验证码
        SysTerminal sysTerminal = dataAccessManager.getSysTerminal(terminal);//获取当前终端

        if (sysTerminal == null) {
            return fail("该用户没有配置终端!!!");
        }
        if (sysTerminal.getStatus() == StatusEnum.NORMAL.getKey() && terminal.equals(ClientTypeEnum.BACKSTAGE.getCode())) {//该终端可用且是后台 需要验证码
            //验证码
            if (vercode == null) {
                return fail("请输入验证码!!!");
            }
            SysCode sysCode = dataAccessManager.getSysCodeName(codeKey);
            if (sysCode != null) {
                if (!sysCode.getCode().toLowerCase().trim().equals(vercode.toLowerCase().trim())) {
                    return fail("验证码错误!!!");
                }
            } else {
                return fail("验证码错误!!!");
            }
        } else if (sysTerminal.getStatus() == StatusEnum.NORMAL.getKey() && terminal.equals(ClientTypeEnum.MERCHANT.getCode())) {

        } else if (sysTerminal.getStatus() == StatusEnum.NORMAL.getKey() && terminal.equals(ClientTypeEnum.CONSUMER.getCode())) {

        } else if (sysTerminal.getStatus() == StatusEnum.NORMAL.getKey() && terminal.equals(ClientTypeEnum.POS.getCode())) {

        } else if (sysTerminal.getStatus() == StatusEnum.NORMAL.getKey() && terminal.equals(ClientTypeEnum.COURIER.getCode())) {

        } else if (sysTerminal.getStatus() == StatusEnum.NORMAL.getKey() && terminal.equals(ClientTypeEnum.WAREHOUSE.getCode())) {

        } else if (sysTerminal.getStatus() == StatusEnum.NORMAL.getKey() && terminal.equals(ClientTypeEnum.PURCHASE.getCode())) {

        } else if (sysTerminal.getStatus() == StatusEnum.NORMAL.getKey() && terminal.equals(ClientTypeEnum.SUPPLIER.getCode())) {

        } else {

        }

        if (!password.equals(sysUser.getPassword())) {
            logger.error("登录失败，登录帐号(" + username + ")、登录密码(" + password + ")错误，用户 -- " + sysUser);
            return fail("帐号或密码错误");
        }
        if (sysUser.getStatus() == UserStatusEnum.FORBID_LOGIN.getKey()) {
            logger.error("登录失败，账户(" + sysUser + ")的状态类型不正常，目前状态：" + sysUser.getStatus());
            return fail("您的帐号已被禁止登录系统，如有疑问，请联系我司客服");
        }
        if (sysUser.getStatus() == UserStatusEnum.BACKLIST.getKey()) {
            logger.error("登录失败，账户(" + sysUser + ")的状态类型不正常，目前状态：" + sysUser.getStatus());
            return fail("您的帐号目前处于黑名单状态，暂时不能登录");
        }

        sysUser.setPassword("");
        sysUser.setDeviceType(deviceType);
        sysUser.setDeviceName(deviceName);
        sysUser.setNetworkType(networkType);
        sysUser.setMachineCode(machineCode);
        sysUser.setMachineBrand(machineBrand);
        sysUser.setSystemVersion(systemVersion);
        sysUser.setLatitude(latitude);
        sysUser.setLongitude(longitude);
        sysUser.setVersionIndex(versionIndex + "");
        sysUser.setLoginDate(new Date());

        // 加密处理
        SysUserPartnerApplication sysPassportPartnerApplication = dataAccessManager.getSysUserPartnerApplicationAppKey(appKey);
        if (sysPassportPartnerApplication == null) {
            return fail(1001, "appKey不存在!!!");
        }
        JSONObject json = new JSONObject();
        json.put("id", sysUser.getId());
        json.put("defaultName", sysUser.getDefaultName());
        json.put("phoneNumber", sysUser.getPhoneNumber());
        json.put("roleId", sysUser.getRoleId());
        String encrypt = RSA.encrypt(json.toJSONString(), sysPassportPartnerApplication.getAccessKeySecretModulus(), sysPassportPartnerApplication.getAccessKeySecretExponent());

        String token = TokenUtils.AddToken(encrypt);
        sysUser.setAccessToken(token);

        //20190420 oy 新增 小程序获取openId
        //判断是否是微信小程序端
        String loginCode = getUTF("loginCode", null); //微信支付code
        Integer loginType = getIntParameter("loginType", 0); // 0商家 1供应商

        if (loginCode != null) {//说明是小程序的
            if (loginType != null || !loginType.equals("")) {
                String openId1 = WeiXinPayUtils.getOpenId(loginCode, loginType);
                sysUser.setOpenId(openId1);
            }
        }
        dataAccessManager.changeLogin(sysUser);//更新用户信息
        /*AsyncScheduledService.submitDataSaveTask(new Runnable() {
            @Override
            public void run() {
                dataAccessManager.changeLogin(sysUser);
            }
        }, 10, TimeUnit.SECONDS);*/

        //pos端营业交班业务代码 ==》是pos端用户登录
        try {
            loggerAop.info("================"+isPosUser+"================");
            if (isPosUser != null) {
                //添加营业员上班记录
                addPosWork(sysUser);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject rsa = new JSONObject();
        rsa.put("token", token);
        rsa.put("realName", sysUser.getRealName());
        rsa.put("phoneNum", sysUser.getPhoneNumber());
        return success(rsa);
    }

    /**
     * 添加pos端营业员上班记录
     *
     * @param user
     * @return void
     * @author sqd
     * @date 2019/5/13
     */
    private  void addPosWork(SysUser user) {

        PosWork posWork = new PosWork();
        String shopId = "";
        if (user != null) {
            if (user.getLeaderUserId() != null && user.getLeaderUserId() != "") {
                List<SupplychainShopBindRelationship> shopBindRelationships = SupplychainShopBindRelationshipRemoteService.getShopBindRelationships(5, user.getLeaderUserId(), getHttpServletRequest());
                if (shopBindRelationships != null && shopBindRelationships.size() > 0) {
                    posWork.setShopId(shopBindRelationships.get(0).getShopId());
                    shopId = shopBindRelationships.get(0).getShopId();
                }
            } else {
                SupplychainShopProperties supplychainShopPropertiesByUserId = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(user.getId(), getHttpServletRequest());
                if (supplychainShopPropertiesByUserId != null) {
                    posWork.setShopId(supplychainShopPropertiesByUserId.getId());
                    shopId = supplychainShopPropertiesByUserId.getId();
                }
            }


            PosWork posWorkByUserId = null;
            try {
                posWorkByUserId = PosWorkRemoteService.getPosWorkByUserId(user.getId(), shopId, "0", getHttpServletRequest());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (posWorkByUserId == null) {
                //验证是否存在值班
                posWork.setUserId(user.getId());
                posWork.setWorkStatus(0);
                posWork.setCreateTime(new Date());
                posWork.setLoginTime(new Date());
                PosWorkRemoteService.addPosWork(posWork, getHttpServletRequest());
            }
        }
    }


    @Override
    public JSONObject getSysUserInIds() {
        String idsJson = getUTF("ids");
        JSONObject jsonObject = new JSONObject();
        if (idsJson != null && !idsJson.equals("")) {
            List<String> list = JSONArray.parseArray(idsJson, String.class);
            if (list != null && list.size() > 0) {
                List<SysUser> sysUserInIds = dataAccessManager.getSysUserInIds(list);
                jsonObject.put("list", sysUserInIds);
                return success(sysUserInIds);
            }
        }
        return success();
    }

    /**
     * 获取采购员角色
     */
//    private SysRole getPurchaseRole () {
//    	SysRole role = dataAccessManager.getSysRoleByRoleName(UserRoleTypeEnum.PURCHASE);
//    	if (role == null) {
//    		role = new SysRole();
//    		role.setRoleName(UserRoleTypeEnum.PURCHASE.getValue());
//    		dataAccessManager.addSysRole(role);
//    	}
//    	return role;
//    }
    @Override
    public JSONObject getPurchaseUserList() {

        // 搜索条件，采购员名字/电话号码
        String searchKey = getUTF("searchKey", null);
        int pageSize = getPageSize();
        int pageStartIndex = getPageStartIndex(pageSize);
        // 采购专员+采购管理员
        ArrayList<String> roleIds = new ArrayList<>();
        roleIds.add(RoleConfig.PURCHASE_ROLE_ID);
        roleIds.add(RoleConfig.PURCHASE_ADMIN_ROLE_ID);
        List<PurchaseUserVo> vos = dataAccessManager.getPurchaseUserList(searchKey, roleIds, pageStartIndex, pageSize);
        vos.stream().forEach(vo->{
            if (StringUtils.isEmpty(vo.getLeaderRealName())) {
                vo.setLeaderRealName(vo.getLeaderPhoneNumber());
            }
        });
        if (CollectionUtils.isEmpty(vos)) {
            return success(new ArrayList(), 0);
        }

        // 总数
        int total = dataAccessManager.getPurchaseUserListCount(searchKey, roleIds);

        List<String> userIdList = vos.stream().map(PurchaseUserVo::getPurchaseUserId).collect(Collectors.toList());

        String userIds = StringUtils.join(userIdList, ",");

        JSONArray responseArr = SupplychainShopPropertiesRemoteService.getPurchaseBindShops(userIds, getHttpServletRequest());

        // 绑定供应商、仓库
        if (!CommonUtils.isEmpty(responseArr)) {
            vos.forEach(vo -> {
                for (int i = 0; i < responseArr.size(); i++) {
                    JSONObject data = responseArr.getJSONObject(i);
                    if (data.getString("purchaseId").equals(vo.getPurchaseUserId())) {
                        vo.setBindSupplierShopNames(data.getString("bindSupplierShopNames"));
                        vo.setBindShopName(data.getString("bindShopName"));
                        break;
                    }
                }
            });
        }

        return success(vos, total);
    }

    @Override
    public JSONObject getSysUsersForSearch() {
        SysUser param = getParseParamValue(SysUser.class);
        if (param == null) {
            return fail("参数有误");
        }
        String column = null;
        Object value = null;
        Field[] fields = SysUser.class.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            String name = field.getName();
            if (name.equals("serialVersionUID") || name.equals("id")) {
                continue;
            }
            try {
                value = field.get(param);
                if (value != null) {
                    column = propertieConverColumn(name);
                    break;
                }
            } catch (Exception e) {
                return fail("系统错误");
            }
        }
        if (column == null || value == null) {
            return fail("参数有误");
        }

        List<SysUser> users = dataAccessManager.getSysUsersForSearch(column, value);

        return success(users);
    }


    // 属性名转数据库字段
    private String propertieConverColumn(String name) {
        String column;
        StringBuilder sb = new StringBuilder();
        char[] chars = name.toCharArray();
        for (char c : chars) {
            sb.append(c >= 'A' && c <= 'Z' ? "_" + String.valueOf(c).toLowerCase() : c);
        }
        column = sb.toString();
        return column;
    }


    @SuppressWarnings("Duplicates")
    @Override
    public JSONObject purchaseBindLeaderList() {
        // 搜索条件，采购员名字/电话号码
        String searchKey = getUTF("searchKey", null);
        ArrayList<String> roleIds = new ArrayList<>();
        // 采购管理员
        roleIds.add(RoleConfig.PURCHASE_ADMIN_ROLE_ID);
        roleIds.add(RoleConfig.PURCHASE_ROLE_ID);
        List<PurchaseUserVo> vos = dataAccessManager.getPurchaseUserList(searchKey, roleIds, getPageStartIndex(getPageSize()), getPageSize());
        vos.stream().forEach(vo->{
                if (StringUtils.isEmpty(vo.getLeaderRealName())){
                    vo.setLeaderRealName(vo.getLeaderPhoneNumber());
                }
        });

        int total = dataAccessManager.getPurchaseUserListCount(searchKey, roleIds);

        return success(vos, total);
    }


    @Override
    public JSONObject userBindLeader() {
        String purchaseUserId = getUTF("purchaseUserId");
        String leaderUserId = getUTF("leaderUserId");

        SysUser sysUser = dataAccessManager.getSysUser(purchaseUserId);
        if (sysUser == null) {
            return fail("用户id错误");
        }
        sysUser.setLeaderUserId(leaderUserId);

        dataAccessManager.changeSysUser(sysUser);
        return success("绑定成功");
    }


    @Override
    public JSONObject userUnbindLeader() {
        String purchaseUserId = getUTF("purchaseUserIds");
        SysUser sysUser = dataAccessManager.getSysUser(purchaseUserId);
        sysUser.setLeaderUserId(null);

        dataAccessManager.changeSysUser(sysUser);
        return success();
    }

    /**
     * purchaseBindShop
     * 仓库管理员添加员工账号
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/22
     */
    @Override
    @Transactional
    public JSONObject addSysUsersForBasicWorkerByLeader() {
        //获取员工名字
        String userName = getUTF("userName");
        //获取员工手机号码
        String phoneNum = getUTF("phoneNum");
        //获取员工设置的密码
        String password = getUTF("password");
        //获取员工领导的id
        String leaderId = getUTF("leaderId");
        //获取当前的绑定类别  1, "采购管理员"2, "运营管理员"3, "运营"4, "仓库管理员"5, "采购管理员"6, "配送管理员"7, "拣货管理员"8, "门店管理员"
        Integer type = getIntParameter("type");

        String terminalId = getUTF("terminalId");

        if (terminalId == null || terminalId.equals("")) {
            return fail("终端类型不能为空！");
        }

        if (type == null || type > 6 || type < 1) {
            return fail("用户类别无效！");
        }

        if (userName == null || userName.equals("")) {
            return fail("用户名不能为空！");
        }

        if (phoneNum == null || phoneNum.equals("")) {
            return fail("电话号码不能为空！");
        }

        if (password == null || password.equals("")) {
            return fail("密码不能为空！");
        }

        SupplychainShopProperties supplychainShopProperties = null;
        //判断这个领导人是不是 商店的管理人
        try {
            supplychainShopProperties = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(leaderId, getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (supplychainShopProperties == null) {//说明该领导者用户没权限
            return fail("该领导用户不是商店管理员！");
        }

        //判断一下 该号码在用户中是否存在
        SysUser sysUserByPhoneNumber = dataAccessManager.getSysUserByPhoneNumber(phoneNum);
        if (sysUserByPhoneNumber != null) {
            return fail("该号码已经存在！");
        }

        //查找对应的角色
        UserRoleTypeEnum userRoleTypeEnum = UserRoleTypeEnum.getUserRoleTypeEnum(type);
        SysRole sysRoleByRoleName = dataAccessManager.getSysRoleByRoleName(userRoleTypeEnum);
        String roleId = "";
        if (sysRoleByRoleName != null) {
            roleId = sysRoleByRoleName.getId();
        }

        //添加用户
        String encryptionPassword = encryptionPassword(password);//密码加密
        SysUser sysUser = new SysUser();
        sysUser.setDefaultName(userName);
        sysUser.setPhoneNumber(phoneNum);
        sysUser.setPassword(encryptionPassword);
        sysUser.setLeaderUserId(leaderId);
        sysUser.setRoleId(roleId);
        dataAccessManager.addSysUser(sysUser);

        //为用户添加终端
        SysUserTerminal sysUserTerminal = new SysUserTerminal();
        sysUserTerminal.setTerminalId(terminalId);
        sysUserTerminal.setUserId(sysUser.getId());
        dataAccessManager.addSysUserTerminal(sysUserTerminal);

        //用户-角色绑定
        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setUserId(roleId);
        sysUserRole.setOperationId(getUser().getId());
        sysUserRole.setRetrieveStatus(0);
        sysUserRole.setStatus(0);
        dataAccessManager.addSysUserRole(sysUserRole);

        //添加一下绑定关系 shop-user
        SupplychainShopBindRelationship supplychainShopBindRelationship = new SupplychainShopBindRelationship();
        supplychainShopBindRelationship.setRoleId(sysUser.getId());
        supplychainShopBindRelationship.setShopId(supplychainShopProperties.getId());
        supplychainShopBindRelationship.setStatus(1);
        supplychainShopBindRelationship.setType(type);//类别
        try {
            SupplychainShopBindRelationshipRemoteService.addSupplychainShopBindRelationship(supplychainShopBindRelationship, getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return success("用户添加成功！");
    }


    @Override
    public JSONObject changeSysUserPassword() {
        String phoneNum = getUTF("phoneNum");//手机号码
        String password = getUTF("password");//新密码
        String loginPassword = getUTF("loginPassword");//原登录密码

        if (phoneNum == null || phoneNum.equals("")) {
            return fail("手机号码不能为空!");
        }
        if (password == null || password.equals("")) {
            return fail("密码不能为空!");
        }
        if (loginPassword == null || loginPassword.equals("")) {
            return fail("登录密码不能为空!");
        }

        String phoneNumber = getUser().getPhoneNumber();
        SysUser sysUser = dataAccessManager.getSysUserByPhoneNumber(phoneNumber);
        if (sysUser != null) {
            String password1 = sysUser.getPassword();
            if (!password1.equals(encryptionPassword(loginPassword))) {
                return fail("登录密码错误！");
            }
        } else {
            return fail("当前用户不存在！");
        }

        //根据手机号码 获取当前用户的详细信息
        SysUser sysUserByPhoneNumber = dataAccessManager.getSysUserByPhoneNumber(phoneNum);
        if (sysUserByPhoneNumber != null) {
            //接下来修改密码
            String encryptionPassword = encryptionPassword(password);
            //修改密码
            dataAccessManager.updatePasswordById(sysUserByPhoneNumber.getId(), encryptionPassword);
            return success("修改密码成功!");
        } else {
            return fail("该用户不存在");
        }
    }


}
