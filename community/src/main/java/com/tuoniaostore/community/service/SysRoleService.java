package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 角色
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 16:36:22
 */
public interface SysRoleService {

    JSONObject addSysRole();

    JSONObject getSysRole();

    JSONObject getSysRoles();

    JSONObject getSysRoleCount();
    JSONObject getSysRoleAll();
    JSONObject changeSysRoleRetrieve();
    JSONObject changeSysRole();

    JSONObject deleteSysRole();

    JSONObject validateSysRole();
}
