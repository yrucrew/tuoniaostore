package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysUserPartnerService;
import com.tuoniaostore.datamodel.user.SysUserPartner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysUserPartnerService")
public class SysUserPartnerServiceImpl extends BasicWebService implements SysUserPartnerService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysUserPartner() {
        SysUserPartner sysUserPartner = new SysUserPartner();
        String name = getUTF("name", null);
        //简介内容
        String introduce = getUTF("introduce", null);
        //合作类型，暂时为1
        Integer type = getIntParameter("type", 0);
        //状态 1可用
        Integer status = getIntParameter("status", 0);
        //排序
        Integer sort = getIntParameter("sort", 0);
        sysUserPartner.setName(name);
        sysUserPartner.setIntroduce(introduce);
        sysUserPartner.setType(type);
        sysUserPartner.setStatus(status);
        sysUserPartner.setSort(sort);
        dataAccessManager.addSysUserPartner(sysUserPartner);
        return success();
    }

    @Override
    public JSONObject getSysUserPartner() {
        SysUserPartner sysUserPartner = dataAccessManager.getSysUserPartner(getId());
        return success(sysUserPartner);
    }

    @Override
    public JSONObject getSysUserPartners() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysUserPartner> sysUserPartners = dataAccessManager.getSysUserPartners(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysUserPartnerCount(status, name);
        return success(sysUserPartners, count);
    }

    @Override
    public JSONObject getSysUserPartnerAll() {
        List<SysUserPartner> sysUserPartners = dataAccessManager.getSysUserPartnerAll();
        return success(sysUserPartners);
    }

    @Override
    public JSONObject getSysUserPartnerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysUserPartnerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysUserPartner() {
        SysUserPartner sysUserPartner = dataAccessManager.getSysUserPartner(getId());

        //合作方名字
        String name = getUTF("name", null);
        //简介内容
        String introduce = getUTF("introduce", null);
        //合作类型，暂时为1
        Integer type = getIntParameter("type", 0);
        //状态 1可用
        Integer status = getIntParameter("status", 0);
              //排序
        Integer sort = getIntParameter("sort", 0);
        sysUserPartner.setName(name);
        sysUserPartner.setIntroduce(introduce);
        sysUserPartner.setType(type);
        sysUserPartner.setStatus(status);
        sysUserPartner.setSort(sort);
        dataAccessManager.changeSysUserPartner(sysUserPartner);
        return success();
    }

}
