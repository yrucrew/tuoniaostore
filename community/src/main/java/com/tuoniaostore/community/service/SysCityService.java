package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
public interface SysCityService {

    JSONObject addSysCity();

    JSONObject getSysCity();

    JSONObject getSysCitys();

    JSONObject getSysCityCount();
    JSONObject getSysCityAll();

    JSONObject changeSysCity();
    JSONObject getSysProvinceAll();
    JSONObject getSysCityByIds();
    JSONObject getSysCityByShengLevel();
    JSONObject getSysCityByShengDi();

    JSONObject getSysProvincesCheckBind();

    JSONObject getSysProvincesUnBind();
}
