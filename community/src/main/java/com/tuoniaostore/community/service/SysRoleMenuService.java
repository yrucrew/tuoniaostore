package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 角色与菜单对应关系
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 16:36:22
 */
public interface SysRoleMenuService {

    JSONObject addSysRoleMenu();

    JSONObject getSysRoleMenu();

    JSONObject getSysRoleMenus();

    JSONObject getSysRoleMenuCount();
    JSONObject getSysRoleMenuAll();

    JSONObject changeSysRoleMenu();
}
