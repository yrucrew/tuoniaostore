package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 16:36:21
 */
public interface SysUserService {

    JSONObject addSysUser();

    JSONObject getSysUser();

    JSONObject getSysUsers();

    JSONObject getSysUsers2();

    JSONObject getSysUserCount();

    JSONObject getSysUserAll();

    JSONObject getSysUserByName();

    /**
     * 登录
     * @return
     */
    JSONObject login();

    JSONObject changeSysUser();

    /**
     * 注册
     * @return
     */
    JSONObject register();

    /**
     * 注册获取手机验证码
     * @return
     */
    JSONObject getMsgCode();

    /**
     * 根据手机号码找回密码 验证短信
     * @return
     */
    JSONObject findPasswordByPhone();

    /**
     * 找回密码
     * @return
     */
    JSONObject findPassword();

    /***
     * 修改登录密码
     * @return
     */
    JSONObject changeSysUserLoginPassword();

    /**
     * 我的：重绑手机
     * @return
     */
    JSONObject changeSysUserPhoneNum();

    /**
     * 我的：重绑手机 获取验证码
     * @return
     */
    JSONObject changeSysUserPhoneNumGetMsg();
    /**
     * 订单用户id 查询对应用户名
     * @author sqd
     * @date 2019/4/16
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getSysUserInIds();

    /** 采购用户列表 */
	JSONObject getPurchaseUserList();

	JSONObject getSysUsersForSearch();

	JSONObject purchaseBindLeaderList();

	JSONObject userBindLeader();

	JSONObject userUnbindLeader();

    JSONObject addSysUsersForBasicWorkerByLeader();

    JSONObject changeSysUserPassword();

    JSONObject getCurrentSysUser();

    JSONObject getSysUserByPhoneNumForRemote();

    JSONObject addSysUserByRemote();

    JSONObject addSysUserForPosByRemote();

    JSONObject delSysUserByUserId();

    JSONObject changeSysUser2();

    JSONObject validateSysUserByPhoneNumberOrDefaultName();

    JSONObject getSysUserAccountOrName();

    JSONObject updateSysUserByPosByRemote();

    JSONObject getAllUserByRoleIds();

    JSONObject getSysUserByPhoneNumLike();

    //JSONObject addPosWorktest();

}
