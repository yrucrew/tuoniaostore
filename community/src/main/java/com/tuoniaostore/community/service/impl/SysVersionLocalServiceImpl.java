package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysVersionLocalService;
import com.tuoniaostore.datamodel.user.SysVersionLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysVersionLocalService")
public class SysVersionLocalServiceImpl extends BasicWebService implements SysVersionLocalService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysVersionLocal() {
        SysVersionLocal sysVersionLocal = new SysVersionLocal();
        //版本Id
        String passportVersionId = getUTF("passportVersionId", null);
        //可更新用户Id
        String userId = getUTF("userId", null);
        //操作人
        String operatePassportId = getUTF("operatePassportId", null);
        //
        Integer retrieveStatus = getIntParameter("retrieveStatus", 0);

        sysVersionLocal.setUserVersionId(passportVersionId);
        sysVersionLocal.setUserId(userId);
        sysVersionLocal.setOperateUserId(operatePassportId);
        sysVersionLocal.setRetrieveStatus(retrieveStatus);
        dataAccessManager.addSysVersionLocal(sysVersionLocal);
        return success();
    }

    @Override
    public JSONObject getSysVersionLocal() {
        SysVersionLocal sysVersionLocal = dataAccessManager.getSysVersionLocal(getId());
        return success(sysVersionLocal);
    }

    @Override
    public JSONObject getSysVersionLocals() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysVersionLocal> sysVersionLocals = dataAccessManager.getSysVersionLocals(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysVersionLocalCount(status, name);
        return success(sysVersionLocals, count);
    }

    @Override
    public JSONObject getSysVersionLocalAll() {
        List<SysVersionLocal> sysVersionLocals = dataAccessManager.getSysVersionLocalAll();
        return success(sysVersionLocals);
    }

    @Override
    public JSONObject getSysVersionLocalCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysVersionLocalCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysVersionLocal() {
        SysVersionLocal sysVersionLocal = dataAccessManager.getSysVersionLocal(getId());
        //
        String id = getUTF("id", null);
        //版本Id
        String userVersionId = getUTF("userVersionId", null);
        //可更新用户Id
        String userId = getUTF("userId", null);
        //操作人
        String operateuserId = getUTF("operateuserId", null);
        sysVersionLocal.setId(id);
        sysVersionLocal.setUserVersionId(userVersionId);
        sysVersionLocal.setUserId(userId);
        sysVersionLocal.setOperateUserId(operateuserId);
        dataAccessManager.changeSysVersionLocal(sysVersionLocal);
        return success();
    }

}
