package com.tuoniaostore.community.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.community.cache.CommunityDataAccessManager;
import com.tuoniaostore.community.service.SysUserChannelService;
import com.tuoniaostore.datamodel.user.SysUserChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("sysUserChannelService")
public class SysUserChannelServiceImpl extends BasicWebService implements SysUserChannelService {
    @Autowired
    private CommunityDataAccessManager dataAccessManager;

    @Override
    public JSONObject addSysUserChannel() {
        SysUserChannel sysUserChannel = new SysUserChannel();
        //渠道名
        String name = getUTF("name", null);
        //渠道类型，由逻辑决定；暂时为1
        Integer type = getIntParameter("type", 0);
        //状态 0可用,1禁用
        Integer status = getIntParameter("status", 0);
        //

        Integer sort = getIntParameter("sort", 0);
        sysUserChannel.setName(name);
        sysUserChannel.setType(type);
        sysUserChannel.setStatus(status);
        sysUserChannel.setSort(sort);
        dataAccessManager.addSysUserChannel(sysUserChannel);
        return success();
    }

    @Override
    public JSONObject getSysUserChannel() {
        SysUserChannel sysUserChannel = dataAccessManager.getSysUserChannel(getId());
        return success(sysUserChannel);
    }

    @Override
    public JSONObject getSysUserChannels() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<SysUserChannel> sysUserChannels = dataAccessManager.getSysUserChannels(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getSysUserChannelCount(status, name);
        return success(sysUserChannels, count);
    }

    @Override
    public JSONObject getSysUserChannelAll() {
        List<SysUserChannel> sysUserChannels = dataAccessManager.getSysUserChannelAll();
        return success(sysUserChannels);
    }

    @Override
    public JSONObject getSysUserChannelCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getSysUserChannelCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeSysUserChannel() {
        SysUserChannel sysUserChannel = dataAccessManager.getSysUserChannel(getId());
        //渠道名
        String name = getUTF("name", null);
        //渠道类型，由逻辑决定；暂时为1
        Integer type = getIntParameter("type", 0);
        //状态 0可用,1禁用
        Integer status = getIntParameter("status", 0);
        //
        Integer sort = getIntParameter("sort", 0);
        sysUserChannel.setName(name);
        sysUserChannel.setType(type);
        sysUserChannel.setStatus(status);
        sysUserChannel.setSort(sort);
        dataAccessManager.changeSysUserChannel(sysUserChannel);
        return success();
    }

    /**
     * 通过渠道名字 查找渠道id
     * @param fromChannel
     * @return
     */
    @Override
    public SysUserChannel getSysUserChannelByName(String fromChannel) {
        return dataAccessManager.getSysUserChannelByName(fromChannel);
    }

}
