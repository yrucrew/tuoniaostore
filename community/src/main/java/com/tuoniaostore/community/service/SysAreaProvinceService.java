package com.tuoniaostore.community.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
public interface SysAreaProvinceService {

    JSONObject addSysAreaProvince();

    JSONObject getSysAreaProvince();

    JSONObject getSysAreaProvinces();

    JSONObject getSysAreaProvinceCount();
    JSONObject getSysAreaProvinceAll();

    JSONObject changeSysAreaProvince();
}
