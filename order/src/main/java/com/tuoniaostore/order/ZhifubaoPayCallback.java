package com.tuoniaostore.order;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.support.order.OrderEntryRemoteService;
import com.tuoniaostore.commons.utils.ZhiFuBaoPayUtils;
import com.tuoniaostore.order.service.OrderEntryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/5/13
 */
@Controller
@RequestMapping(value= "zhifubaoPay")
public class ZhifubaoPayCallback extends BasicWebService {

    @Autowired
    OrderEntryService orderEntryService;

    private static final Logger logger = LoggerFactory.getLogger(ZhifubaoPayCallback.class);
    @RequestMapping(value="notify")
    public String zhifuNotify(HttpServletRequest request){
        logger.info("-----------------------------------pos扫码支付宝回调开始---------------------------------------------");
        Map<String, String> params = new HashMap<String, String>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            logger.info(">>>>>参数" + name + ":" + valueStr);
            params.put(name, valueStr);
        }
        requestParams.get("trade_status");
        String tradeNo = request.getParameter("out_trade_no");
        String tradeStatus = request.getParameter("trade_status");
        String totalAmount = request.getParameter("total_amount");

        ZhiFuBaoPayUtils zhiFuBaoPayUtils = new ZhiFuBaoPayUtils();
        Map<String, String> properties = zhiFuBaoPayUtils.readProperties();

        try {
            boolean signVerified = AlipaySignature.rsaCheckV1(params, properties.get("alipayPublicKey"),
                    properties.get("charset"),properties.get("singType"));
            if (signVerified) {//验证成功
                if (tradeStatus.equals("TRADE_FINISHED") || tradeStatus.equals("TRADE_SUCCESS")) {
                    logger.info(">>>>>状态要更新了");
                    //商品交易成功之后的业务逻辑代码
//                    tradeNo
                    HttpServletRequest request1 = getHttpServletRequest();

                    request1.setAttribute("orderNumber",tradeNo);
                    request1.setAttribute("totalPrice",totalAmount);
                    orderEntryService.zfbPosPayCallback();
                    logger.info(">>>>>下单成功" + tradeNo);
                }
                return "success";
            } else {//验证失败
                logger.info(">>>>>验签失败" + tradeNo);
                logger.info(">>>>>交易被关闭了");
                return "failure";
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
        } finally {
            logger.info("-----------------------------------pos扫码支付宝回调结束---------------------------------------------");
        }
        return "failure";
    }

    @RequestMapping(value="notify2")
    public void zhifuNotify2(){
        logger.info("支付宝回调2----------------------------------------------------");
    }

}
