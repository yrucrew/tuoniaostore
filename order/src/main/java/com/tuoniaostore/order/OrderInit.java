package com.tuoniaostore.order;

import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.order.config.DomainNameConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Created by liyunbiao on 2019/3/5.
 */

@Component
@Order(value = 1)
public class OrderInit  implements CommandLineRunner {

    @Autowired
    DomainNameConfig domainNameConfig;
    @Override
    public void run(String... args) throws Exception {

        CommonsUrlConfig.setCommunityRemoteURL(domainNameConfig.communityRemoteURL);
        CommonsUrlConfig.setOrderRemoteURL(domainNameConfig.orderRemoteURL);
        CommonsUrlConfig.setGoodRemoteURL(domainNameConfig.goodRemoteURL);
        CommonsUrlConfig.setLogisticsRemoteURL(domainNameConfig.logisticsRemoteURL);
        CommonsUrlConfig.setPaymentRemoteURL(domainNameConfig.paymentRemoteURL);
        CommonsUrlConfig.setSupplychainRemoteURL(domainNameConfig.supplychainRemoteURL);

    }
}
