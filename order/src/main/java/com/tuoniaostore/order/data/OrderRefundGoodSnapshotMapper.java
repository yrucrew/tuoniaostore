package com.tuoniaostore.order.data;

import com.tuoniaostore.datamodel.order.OrderRefundGoodSnapshot;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface OrderRefundGoodSnapshotMapper {

    String column = "`id`," +
            "`order_id`, " +//对应的订单ID
            "`user_mark`, " +//购买的用户ID或其他标志(如第三方用户ID)
            "`good_id`, " +//相关的商品ID
            "`good_template_id`, " +//商品模版ID
            "`good_name`, " +//当时的商品名称
            "`good_type_id`, " +//当时的归属类型ID
            "`good_type_name`, " +//当时归属的类型名
            "`good_unit_id`, " +//当时的单位ID
            "`good_unit_name`, " +//当时单位名称
            "`good_barcode`, " +//商品条码
            "`good_code`, " +//当时商品的编码
            "`good_batches`, " +//商品批次
            "`introduction_page`, " +//当时的介绍页面
            "`create_time`, " +//购买的时间
            "`normal_quantity`, " +//正常价格购买的数量
            "`discount_quantity`, " +//优惠价格购买的数量
            "`shipment_quantity`, " +//发货中的数量
            "`distribution_quantity`, " +//配送中的数量
            "`arrive_quantity`, " +//送达的数量
            "`receipt_quantity`, " +//被确认收货的数量
            "`refund_quantity`, " +//退货中数量
            "`return_quantity`, " +//退货的数量
            "`review_quantity`, " +//复核数量
            "`normal_price`, " +//正常价格
            "`discount_price`, " +//优惠价格
            "`cost_price`, " +//当时的成本价
            "`market_price`, " +//当时一般的市场价
            "`total_price`, " +//应收费用
            "`return_price`, " +//退货的费用(实收为应收费用-退货费用)
            "`inner_order_id`, " +//区分仓库的订单号
            "`third_party_delivery_quantity`, " +//第三方送货的数量
            "`third_party_accept_quantity`, " +//第三方排线的数量
            "`good_price_id`, " +
            "`good_price_title`, " +
            "`is_third_party`, " +//是否第三方商品，否为0
            " `shop_id`, " +
            " `shop_name` "
            ;

    @Insert("insert into order_refund_good_snapshot (" +
            " `id`,  " +
            " `order_id`,  " +
            " `user_mark`,  " +
            " `good_id`,  " +
            " `good_template_id`,  " +
            " `good_name`,  " +
            " `good_type_id`,  " +
            " `good_type_name`,  " +
            " `good_unit_id`,  " +
            " `good_unit_name`,  " +
            " `good_barcode`,  " +
            " `good_code`,  " +
            " `good_batches`,  " +
            " `introduction_page`,  " +
            " `normal_quantity`,  " +
            " `discount_quantity`,  " +
            " `shipment_quantity`,  " +
            " `distribution_quantity`,  " +
            " `arrive_quantity`,  " +
            " `receipt_quantity`,  " +
            " `refund_quantity`,  " +
            " `return_quantity`,  " +
            " `review_quantity`,  " +
            " `normal_price`,  " +
            " `discount_price`,  " +
            " `cost_price`,  " +
            " `market_price`,  " +
            " `total_price`,  " +
            " `return_price`,  " +
            " `inner_order_id`,  " +
            " `third_party_delivery_quantity`,  " +
            " `third_party_accept_quantity`,  " +
            " `good_price_id` , " +
            " `good_price_title` , " +
            " `is_third_party`, " +
            " `shop_id`, " +
            " `shop_name` " +
            ")values(" +
            "#{id},  " +
            "#{orderId},  " +
            "#{userMark},  " +
            "#{goodId},  " +
            "#{goodTemplateId},  " +
            "#{goodName},  " +
            "#{goodTypeId},  " +
            "#{goodTypeName},  " +
            "#{goodUnitId},  " +
            "#{goodUnitName},  " +
            "#{goodBarcode},  " +
            "#{goodCode},  " +
            "#{goodBatches},  " +
            "#{introductionPage},  " +
            "#{normalQuantity},  " +
            "#{discountQuantity},  " +
            "#{shipmentQuantity},  " +
            "#{distributionQuantity},  " +
            "#{arriveQuantity},  " +
            "#{receiptQuantity},  " +
            "#{refundQuantity},  " +
            "#{returnQuantity},  " +
            "#{reviewQuantity},  " +
            "#{normalPrice},  " +
            "#{discountPrice},  " +
            "#{costPrice},  " +
            "#{marketPrice},  " +
            "#{totalPrice},  " +
            "#{returnPrice},  " +
            "#{innerOrderId},  " +
            "#{thirdPartyDeliveryQuantity},  " +
            "#{thirdPartyAcceptQuantity},  " +
            "#{goodPriceId},  " +
            "#{goodPriceTitle},  " +
            "#{isThirdParty}, " +
            "#{shopId}, " +
            "#{shopName} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addOrderRefundGoodSnapshot(OrderRefundGoodSnapshot orderRefundGoodSnapshot);

    @Results(id = "orderRefundGoodSnapshot", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "orderId", column = "order_id"),
            @Result(property = "userMark", column = "user_mark"),
            @Result(property = "goodId", column = "good_id"),
            @Result(property = "goodTemplateId", column = "good_template_id"),
            @Result(property = "goodName", column = "good_name"),
            @Result(property = "goodTypeId", column = "good_type_id"),
            @Result(property = "goodTypeName", column = "good_type_name"),
            @Result(property = "goodUnitId", column = "good_unit_id"),
            @Result(property = "goodUnitName", column = "good_unit_name"),
            @Result(property = "goodBarcode", column = "good_barcode"),
            @Result(property = "goodCode", column = "good_code"),
            @Result(property = "goodBatches", column = "good_batches"),
            @Result(property = "introductionPage", column = "introduction_page"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "normalQuantity", column = "normal_quantity"),
            @Result(property = "discountQuantity", column = "discount_quantity"),
            @Result(property = "shipmentQuantity", column = "shipment_quantity"),
            @Result(property = "distributionQuantity", column = "distribution_quantity"),
            @Result(property = "arriveQuantity", column = "arrive_quantity"),
            @Result(property = "receiptQuantity", column = "receipt_quantity"),
            @Result(property = "refundQuantity", column = "refund_quantity"),
            @Result(property = "returnQuantity", column = "return_quantity"),
            @Result(property = "reviewQuantity", column = "review_quantity"),
            @Result(property = "normalPrice", column = "normal_price"),
            @Result(property = "discountPrice", column = "discount_price"),
            @Result(property = "costPrice", column = "cost_price"),
            @Result(property = "marketPrice", column = "market_price"),
            @Result(property = "totalPrice", column = "total_price"),
            @Result(property = "returnPrice", column = "return_price"),
            @Result(property = "innerOrderId", column = "inner_order_id"),
            @Result(property = "thirdPartyDeliveryQuantity", column = "third_party_delivery_quantity"),
            @Result(property = "thirdPartyAcceptQuantity", column = "third_party_accept_quantity"),
            @Result(property = "isThirdParty", column = "is_third_party"),
            @Result(property = "shopId", column = "shop_id"),
            @Result(property = "shopName", column = "shop_name")})
    @Select("select " + column + " from order_refund_good_snapshot where order_id = #{orderId} ")
    List<OrderRefundGoodSnapshot> getOrderRefundGoodSnapshotByOrderId(@Param("orderId")String orderId, @Param("pageStartIndex")int pageStartIndex, @Param("pageSize")int pageSize);

    @Insert("<script> " +
            "insert into order_refund_good_snapshot (" +
            " `id`,  " +
            " `order_id`,  " +
            " `user_mark`,  " +
            " `good_id`,  " +
            " `good_template_id`,  " +
            " `good_name`,  " +
            " `good_type_id`,  " +
            " `good_type_name`,  " +
            " `good_unit_id`,  " +
            " `good_unit_name`,  " +
            " `good_barcode`,  " +
            " `good_code`,  " +
            " `good_batches`,  " +
            " `introduction_page`,  " +
            " `normal_quantity`,  " +
            " `discount_quantity`,  " +
            " `shipment_quantity`,  " +
            " `distribution_quantity`,  " +
            " `arrive_quantity`,  " +
            " `receipt_quantity`,  " +
            " `refund_quantity`,  " +
            " `return_quantity`,  " +
            " `review_quantity`,  " +
            " `normal_price`,  " +
            " `discount_price`,  " +
            " `cost_price`,  " +
            " `market_price`,  " +
            " `total_price`,  " +
            " `return_price`,  " +
            " `inner_order_id`,  " +
            " `third_party_delivery_quantity`,  " +
            " `third_party_accept_quantity`,  " +
            " `good_price_id` , " +
            " `good_price_title` , " +
            " `is_third_party`, " +
            " `shop_id`, " +
            " `shop_name` "+
            ")values" +
            "<foreach collection=\"lists\" item=\"item\" separator=\",\"> " +
            "(#{item.id},  " +
            "#{item.orderId},  " +
            "#{item.userMark},  " +
            "#{item.goodId},  " +
            "#{item.goodTemplateId},  " +
            "#{item.goodName},  " +
            "#{item.goodTypeId},  " +
            "#{item.goodTypeName},  " +
            "#{item.goodUnitId},  " +
            "#{item.goodUnitName},  " +
            "#{item.goodBarcode},  " +
            "#{item.goodCode},  " +
            "#{item.goodBatches},  " +
            "#{item.introductionPage},  " +
            "#{item.normalQuantity},  " +
            "#{item.discountQuantity},  " +
            "#{item.shipmentQuantity},  " +
            "#{item.distributionQuantity},  " +
            "#{item.arriveQuantity},  " +
            "#{item.receiptQuantity},  " +
            "#{item.refundQuantity},  " +
            "#{item.returnQuantity},  " +
            "#{item.reviewQuantity},  " +
            "#{item.normalPrice},  " +
            "#{item.discountPrice},  " +
            "#{item.costPrice},  " +
            "#{item.marketPrice},  " +
            "#{item.totalPrice},  " +
            "#{item.returnPrice},  " +
            "#{item.innerOrderId},  " +
            "#{item.thirdPartyDeliveryQuantity},  " +
            "#{item.thirdPartyAcceptQuantity},  " +
            "#{item.goodPriceId},  " +
            "#{item.goodPriceTitle},  " +
            "#{item.isThirdParty}, " +
            "#{item.shopId}, " +
            "#{item.shopName})" +
            "</foreach>" +
            "</script>")
    void batchAddOrderRefundGoodSnapshot(@Param("lists") List<OrderRefundGoodSnapshot> orderRefundGoodSnapshotAddList);
}
