package com.tuoniaostore.order.data;

import com.tuoniaostore.datamodel.vo.order.OrderEntryPos;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Mapper
public interface OrderEntryPosMapper {

    String column = "" +
            "`id`," +
            "`sequence_number`, " +//序列号(对应于购物车)
            "`order_sequence_number`, " +//订单的序列号(用于展示)
            "`partner_id`, " +//下游商户服务号ID
            "`partner_user_id`, " +//下单用户ID 匿名时可为0或空
            "`type`, " +//订单类型 1-供应链销售订单、2-供应链调拨订单、3-供应链采购订单、4-超市POS机订单（包括超市扫码订单）、7-无人便利店订单、9-增值服务订单、10-无人货架订单、11-向1号生活扫码支付（公司收款）、12-1号生活加盟店（收加盟费）
            "`status`, " +//订单状态 不要占用0  1-创建订单（未确认）、2-已取消、8-已支付、16-已接单、32-发货中、64-配送中、128-已送达、256-确认收货/已完成、260-***确认收货/已完成、512-已收款、1024-分批配送、2048-拒绝订单。
            "`deliver_status`, " +//发货状态：0-未发货、32-发货中、64-配送中、128-已送达
            "`refund_status`, " +//退款状态 0表示未退货 同时可用于记录是否已打款状态
            "`payment_type`, " +//支付类型 参考逻辑设定 -1未支付
            "`trans_type`, " +//交易类型，微信系：JSAPI、NATIVE、APP等
            "`user_source`, " +//用户来源
            "`day_sort_number`, " +//当天排号 相对于发货方
            "`push_type`, " +//推送类型，0表示不推送 1时表示下单时推送 2时表示支付后推送
            "`sale_user_id`, " +//实际销售的用户ID（销售仓）
            "`shipping_user_id`, " +//发货用户ID
            "`shipping_nick_name`, " +//发货用户昵称
            "`shipping_province`, " +//发货省份
            "`shipping_city`, " +//发货城市
            "`shipping_district`, " +//发货区域
            "`shipping_address`, " +//发货详细地址(尽量不要包含省市区)
            "`shipping_location`, " +//latitude,longitude
            "`shipping_phone`, " +//发货人联系号码
            "`receipt_user_id`, " +//收货人用户ID
            "`receipt_nick_name`, " +//收货人昵称
            "`receipt_province`, " +//收货省份
            "`receipt_city`, " +//收货城市
            "`receipt_district`, " +//收货区域
            "`receipt_address`, " +//收货具体地址(尽量不要包含省市区)
            "`receipt_phone`, " +//收货人电话
            "`receipt_location`, " +//latitude,longitude
            "`courier_user_id`, " +//配送员用户ID
            "`courier_nick_name`, " +//配送员昵称
            "`courier_phone`, " +//配送员联系号码
            "`total_distance`, " +//订单总距离
            "`current_location`, " +//下单时所在地理位置
            "`collecting_fees`, " +//是否需要配送员代收费用 0表示不需要
            "`remark`, " +//订单描述
            "`actual_price`, " +//实收费用
            "`total_price`, " +//订单总费用
            "`discount_price`, " +//优惠的费用
            "`distribution_fee`, " +//配送费用
            "`price_logger`, " +//价格记录 如优惠的政策
            "`tax_rate`, " +//税率(万分比)
            "`cancel_logger`, " +//取消的理由
            "`create_time`, " +//订单的生成时间
            "`payment_time`, " +//支付时间
            "`confirm_time`, " +//确认订单时间
            "`body`, " +//商品或支付单简要描述
            "`detail`, " +//商品名称明细列表(仅记录体系外的数据)
            "`credit_fee`, " +//信用付费用
            "`third_party_delivery`, " +//是否使用第三方物流 0否 1是
            "`membership_price`, " +//会员价(分)
            "`third_party_delivery_status`, " +//第三方物流的状态 1-待推,2-全部推完,3-冲红
            "`refund_remark`, " +//退款备注
            "`refund_time`, " +//退款时间
            "`refund_type`, " +// 退款方式
            "`return_operator_user_id`, " +//商品退款操作人
            "`find_price`, " +//找零
            "`receiving_time`, " +//待收货时间
            "`is_join` " //是否为加盟店 1为是
            ;

    @Results(id = "orderEntryPos", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "sequenceNumber", column = "sequence_number"),
            @Result(property = "orderSequenceNumber", column = "order_sequence_number"),
            @Result(property = "partnerId", column = "partner_id"),
            @Result(property = "partnerUserId", column = "partner_user_id"),
            @Result(property = "type", column = "type"),
            @Result(property = "status", column = "status"),
            @Result(property = "deliverStatus", column = "deliver_status"),
            @Result(property = "refundStatus", column = "refund_status"),
            @Result(property = "paymentType", column = "payment_type"),
            @Result(property = "transType", column = "trans_type"),
            @Result(property = "userSource", column = "user_source"),
            @Result(property = "daySortNumber", column = "day_sort_number"),
            @Result(property = "pushType", column = "push_type"),
            @Result(property = "saleUserId", column = "sale_user_id"),
            @Result(property = "shippingUserId", column = "shipping_user_id"),
            @Result(property = "shippingNickName", column = "shipping_nick_name"),
            @Result(property = "shippingProvince", column = "shipping_province"),
            @Result(property = "refundRemark ", column = "refund_remark "),
            @Result(property = "refundTime", column = "refund_time"),
            @Result(property = "refundType", column = "refund_type"),
            @Result(property = "shippingCity", column = "shipping_city"),
            @Result(property = "findPrice", column = "find_price"),
            @Result(property = "returnOperatorUserId", column = "return_operator_user_id"),
            @Result(property = "shippingDistrict", column = "shipping_district"),
            @Result(property = "shippingAddress", column = "shipping_address"),
            @Result(property = "shippingLocation", column = "shipping_location"),
            @Result(property = "shippingPhone", column = "shipping_phone"),
            @Result(property = "receiptUserId", column = "receipt_user_id"),
            @Result(property = "receiptNickName", column = "receipt_nick_name"),
            @Result(property = "receiptProvince", column = "receipt_province"),
            @Result(property = "receiptCity", column = "receipt_city"),
            @Result(property = "receiptDistrict", column = "receipt_district"),
            @Result(property = "receiptAddress", column = "receipt_address"),
            @Result(property = "receiptPhone", column = "receipt_phone"),
            @Result(property = "receiptLocation", column = "receipt_location"),
            @Result(property = "courierUserId", column = "courier_user_id"),
            @Result(property = "courierNickName", column = "courier_nick_name"),
            @Result(property = "courierPhone", column = "courier_phone"),
            @Result(property = "totalDistance", column = "total_distance"),
            @Result(property = "currentLocation", column = "current_location"),
            @Result(property = "collectingFees", column = "collecting_fees"),
            @Result(property = "remark", column = "remark"),
            @Result(property = "actualPrice", column = "actual_price"),
            @Result(property = "totalPrice", column = "total_price"),
            @Result(property = "discountPrice", column = "discount_price"),
            @Result(property = "distributionFee", column = "distribution_fee"),
            @Result(property = "priceLogger", column = "price_logger"),
            @Result(property = "taxRate", column = "tax_rate"),
            @Result(property = "cancelLogger", column = "cancel_logger"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "paymentTime", column = "payment_time"),
            @Result(property = "confirmTime", column = "confirm_time"),
            @Result(property = "body", column = "body"),
            @Result(property = "detail", column = "detail"),
            @Result(property = "creditFee", column = "credit_fee"),
            @Result(property = "thirdPartyDelivery", column = "third_party_delivery"),
            @Result(property = "membershipPrice", column = "membership_price"),
            @Result(property = "thirdPartyDeliveryStatus", column = "third_party_delivery_status"),
            @Result(property = "isJoin", column = "is_join"),
            @Result(property = "isThirdParty", column = "is_third_party"),
            @Result(property = "receivingTime", column = "receiving_time"),
            @Result(property = "reviewUserId", column = "review_user_id")})
    @Select("<script>" +
            "select " +
            column +
            " FROM order_entry_pos WHERE 1=1 " +
            " <when test=\"params.type != null and params.type != 0\">" +
            "   and type=#{params.type} " +
            " </when>" +
            " <when test=\"params.saleUserId != null\">" +
            "   and sale_user_id=#{params.saleUserId} " +
            " </when>" +
            " ORDER BY payment_time DESC " +
            " <when test=\"pageStartIndex != null and pageSize != null\">" +
            "  limit #{pageStartIndex}, #{pageSize}" +
            " </when> "+
            "</script>")
    List<OrderEntryPos> getOrderEntryPosList(@Param("params") Map<String, Object> params,@Param("pageStartIndex") int pageStartIndex,@Param("pageSize") int pageSize);

    @Select("<script>" +
            "select " +
            " count(1) " +
            " FROM order_entry_pos WHERE 1=1 " +
            " <when test=\"params.type != null and params.type != 0\">" +
            "   and type=#{params.type} " +
            " </when>" +
            " <when test=\"params.saleUserId != null\">" +
            "   and sale_user_id=#{params.saleUserId} " +
            " </when>" +
            " ORDER BY payment_time DESC " +
            "</script>")
    int getOrderEntryPosListCount(@Param("params")Map<String, Object> params);
}
