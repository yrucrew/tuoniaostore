package com.tuoniaostore.order.data;

import com.tuoniaostore.datamodel.order.OrderStatusListener;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
@Mapper
public interface OrderStatusListenerMapper {

    String column = "`partner_id`, " +//合作商家的商户号
            "`event_type`, " +//监听类型
            "`status`, " +//状态：0 -- 不启用 1 -- 启用
            "`callback_url`, " +//回调地址
            "`create_time`"//建立时间
            ;

    @Insert("insert into order_status_listener (" +
            " `id`,  " +
            " `partner_id`,  " +
            " `event_type`,  " +
            " `status`,  " +
            " `callback_url`,  " +
            ")values(" +
            "#{id},  " +
            "#{partnerId},  " +
            "#{eventType},  " +
            "#{status},  " +
            "#{callbackUrl},  " +
            ")")
    @Options(useGeneratedKeys = true)
    void addOrderStatusListener(OrderStatusListener orderStatusListener);

    @Results(id = "orderStatusListener", value = {
            @Result(property = "id", column = "id"), @Result(property = "partnerId", column = "partner_id"), @Result(property = "eventType", column = "event_type"), @Result(property = "status", column = "status"), @Result(property = "callbackUrl", column = "callback_url"), @Result(property = "createTime", column = "create_time")})
    @Select("select " + column + " from order_status_listener where id = #{id}")
    OrderStatusListener getOrderStatusListener(@Param("id") String id);

    @Select("<script>select  " + column + "  from order_status_listener   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("orderStatusListener")
    List<OrderStatusListener> getOrderStatusListeners( @Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from order_status_listener   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("orderStatusListener")
    List<OrderStatusListener> getOrderStatusListenerAll();

    @Select("<script>select count(1) from order_status_listener   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getOrderStatusListenerCount(@Param("status") int status, @Param("name") String name);

    @Update("update order_status_listener  " +
            "set " +
            "`partner_id` = #{partnerId}  , " +
            "`event_type` = #{eventType}  , " +
            "`status` = #{status}  , " +
            "`callback_url` = #{callbackUrl}  , " +
            "`create_time` = #{createTime}  " +
            " where id = #{id}")
    void changeOrderStatusListener(OrderStatusListener orderStatusListener);

}
