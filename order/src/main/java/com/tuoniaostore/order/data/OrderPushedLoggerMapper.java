package com.tuoniaostore.order.data;

import com.tuoniaostore.datamodel.order.OrderPushedLogger;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 订单的推送日志
 * 与未接订单日志的区别为该表不物理删除数据 可以查询订单是否曾经已执行推送 且明白推送的目标
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
@Mapper
public interface OrderPushedLoggerMapper {

    String column = "`order_id`, " +//订单ID
            "`target_user_id`, " +//目标ID
            "`push_type`, " +//推送类型
            "`push_title`, " +//推送的标题
            "`push_msg_content`, " +//推送的内容
            "`push_result`, " +//推送结果内容(即时)
            "`push_time`, " +//推送时间(我方)
            "`status`, " +//推送状态(我方)
            "`feedback_msg_content`, " +//返回消息内容
            "`feedback_status`, " +//返回的推送状态
            "`feedback_time`"//返回时间
            ;

    @Insert("insert into order_pushed_logger (" +
            " `id`,  " +
            " `order_id`,  " +
            " `target_user_id`,  " +
            " `push_type`,  " +
            " `push_title`,  " +
            " `push_msg_content`,  " +
            " `push_result`,  " +
            " `push_time`,  " +
            " `status`,  " +
            " `feedback_msg_content`,  " +
            " `feedback_status`,  " +
            " `feedback_time` " +
            ")values(" +
            "#{id},  " +
            "#{orderId},  " +
            "#{targetPassportId},  " +
            "#{pushType},  " +
            "#{pushTitle},  " +
            "#{pushMsgContent},  " +
            "#{pushResult},  " +
            "#{pushTime},  " +
            "#{status},  " +
            "#{feedbackMsgContent},  " +
            "#{feedbackStatus},  " +
            "#{feedbackTime} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addOrderPushedLogger(OrderPushedLogger orderPushedLogger);

    @Results(id = "orderPushedLogger", value = {
            @Result(property = "id", column = "id"), @Result(property = "orderId", column = "order_id"), @Result(property = "targetUserId", column = "target_user_id"), @Result(property = "pushType", column = "push_type"), @Result(property = "pushTitle", column = "push_title"), @Result(property = "pushMsgContent", column = "push_msg_content"), @Result(property = "pushResult", column = "push_result"), @Result(property = "pushTime", column = "push_time"), @Result(property = "status", column = "status"), @Result(property = "feedbackMsgContent", column = "feedback_msg_content"), @Result(property = "feedbackStatus", column = "feedback_status"), @Result(property = "feedbackTime", column = "feedback_time")})
    @Select("select " + column + " from order_pushed_logger where id = #{id}")
    OrderPushedLogger getOrderPushedLogger(@Param("id") String id);

    @Select("<script>select  " + column + "  from order_pushed_logger   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("orderPushedLogger")
    List<OrderPushedLogger> getOrderPushedLoggers(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from order_pushed_logger   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("orderPushedLogger")
    List<OrderPushedLogger> getOrderPushedLoggerAll();

    @Select("<script>select count(1) from order_pushed_logger   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getOrderPushedLoggerCount(@Param("status") int status, @Param("name") String name);

    @Update("update order_pushed_logger  " +
            "set " +
            "`order_id` = #{orderId}  , " +
            "`target_user_id` = #{targetUserId}  , " +
            "`push_type` = #{pushType}  , " +
            "`push_title` = #{pushTitle}  , " +
            "`push_msg_content` = #{pushMsgContent}  , " +
            "`push_result` = #{pushResult}  , " +
            "`push_time` = #{pushTime}  , " +
            "`status` = #{status}  , " +
            "`feedback_msg_content` = #{feedbackMsgContent}  , " +
            "`feedback_status` = #{feedbackStatus}  , " +
            "`feedback_time` = #{feedbackTime}  " +
            " where id = #{id}")
    void changeOrderPushedLogger(OrderPushedLogger orderPushedLogger);

}
