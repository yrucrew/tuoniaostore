package com.tuoniaostore.order.data;

import com.tuoniaostore.datamodel.order.OrderSequence;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
@Mapper
public interface OrderSequenceMapper {

    String column = "`partner_id`, " +//合作商户号ID
            "`partner_user_id`, " +//合作商户下级用户信息
            "`sequence_number`, " +//预创建订单序列号
            "`type`, " +//申请的类型
            "`status`, " +//状态 0 - 可用 其他值为不可用
            "`create_time`, " +//建立时间
            "`validity_term_second`"//有效期，单位：秒
            ;

    @Insert("insert into order_sequence (" +
            " `id`,  " +
            " `partner_id`,  " +
            " `partner_user_id`,  " +
            " `sequence_number`,  " +
            " `type`,  " +
            " `status`,  " +
            " `validity_term_second` " +
            ")values(" +
            "#{id},  " +
            "#{partnerId},  " +
            "#{partnerUserId},  " +
            "#{sequenceNumber},  " +
            "#{type},  " +
            "#{status},  " +
            "#{validityTermSecond} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addOrderSequence(OrderSequence orderSequence);

    @Results(id = "orderSequence", value = {
            @Result(property = "id", column = "id"), @Result(property = "partnerId", column = "partner_id"), @Result(property = "partnerUserId", column = "partner_user_id"), @Result(property = "sequenceNumber", column = "sequence_number"), @Result(property = "type", column = "type"), @Result(property = "status", column = "status"), @Result(property = "createTime", column = "create_time"), @Result(property = "validityTermSecond", column = "validity_term_second")})
    @Select("select " + column + " from order_sequence where id = #{id}")
    OrderSequence getOrderSequence(@Param("id") String id);

    @Select("<script>select  " + column + "  from order_sequence   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("orderSequence")
    List<OrderSequence> getOrderSequences(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("<script>select  " + column + "  from order_sequence   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("orderSequence")
    List<OrderSequence> getOrderSequenceAll();

    @Select("<script>select count(1) from order_sequence   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getOrderSequenceCount(@Param("status") int status, @Param("name") String name);

    @Update("update order_sequence  " +
            "set " +
            "`partner_id` = #{partnerId}  , " +
            "`partner_user_id` = #{partnerUserId}  , " +
            "`sequence_number` = #{sequenceNumber}  , " +
            "`type` = #{type}  , " +
            "`status` = #{status}  , " +
            "`create_time` = #{createTime}  , " +
            "`validity_term_second` = #{validityTermSecond}  " +
            " where id = #{id}")
    void changeOrderSequence(OrderSequence orderSequence);

}
