package com.tuoniaostore.order.data;

import com.tuoniaostore.datamodel.order.OrderStateLogger;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
@Mapper
public interface OrderStateLoggerMapper {

    String column = "`order_id`, " +//对应的订单ID
            "`shipping_user_id`, " +//发货者通行证ID
            "`order_type`, " +//对应的订单类型
            "`before_status`, " +//操作前的状态
            "`status`, " +//操作后的状态
            "`operator_time`, " +//操作时间
            "`operator_user_id`, " +//操作者角色ID
            "`operator_name`, " +//操作者名字
            "`operator_describe`"//操作备注信息
            ;

    @Insert("insert into order_state_logger (" +
            " `id`,  " +
            " `order_id`,  " +
            " `shipping_user_id`,  " +
            " `order_type`,  " +
            " `before_status`,  " +
            " `status`,  " +
            " `operator_time`,  " +
            " `operator_user_id`,  " +
            " `operator_name`,  " +
            " `operator_describe` " +
            ")values(" +
            "#{id},  " +
            "#{orderId},  " +
            "#{shippingUserId},  " +
            "#{orderType},  " +
            "#{beforeStatus},  " +
            "#{status},  " +
            "#{operatorTime},  " +
            "#{operatorUserId},  " +
            "#{operatorName},  " +
            "#{operatorDescribe} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addOrderStateLogger(OrderStateLogger orderStateLogger);

    @Results(id = "orderStateLogger", value = {
            @Result(property = "id", column = "id"), @Result(property = "orderId", column = "order_id"), @Result(property = "shippingUserId", column = "shipping_user_id"), @Result(property = "orderType", column = "order_type"), @Result(property = "beforeStatus", column = "before_status"), @Result(property = "status", column = "status"), @Result(property = "operatorTime", column = "operator_time"), @Result(property = "operatorUserId", column = "operator_user_id"), @Result(property = "operatorName", column = "operator_name"), @Result(property = "operatorDescribe", column = "operator_describe")})
    @Select("select " + column + " from order_state_logger where id = #{id}")
    OrderStateLogger getOrderStateLogger(@Param("id") String id);

    @Select("<script>select  " + column + "  from order_state_logger   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("orderStateLogger")
    List<OrderStateLogger> getOrderStateLoggers( @Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("name") String name);

    @Select("<script>select  " + column + "  from order_state_logger   where   retrieve_status=0  and status=0 </script>")
    @ResultMap("orderStateLogger")
    List<OrderStateLogger> getOrderStateLoggerAll();

    @Select("<script>select count(1) from order_state_logger   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getOrderStateLoggerCount(@Param("status") int status, @Param("name") String name);

    @Update("update order_state_logger  " +
            "set " +
            "`order_id` = #{orderId}  , " +
            "`shipping_user_id` = #{shippingUserId}  , " +
            "`order_type` = #{orderType}  , " +
            "`before_status` = #{beforeStatus}  , " +
            "`status` = #{status}  , " +
            "`operator_time` = #{operatorTime}  , " +
            "`operator_user_id` = #{operatorUserId}  , " +
            "`operator_name` = #{operatorName}  , " +
            "`operator_describe` = #{operatorDescribe}  " +
            " where id = #{id}")
    void changeOrderStateLogger(OrderStateLogger orderStateLogger);

}
