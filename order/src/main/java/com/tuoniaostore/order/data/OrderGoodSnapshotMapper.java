package com.tuoniaostore.order.data;

import com.tuoniaostore.datamodel.good.GoodBarcode;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.vo.order.OrderGoodSnapshotFormatDate;
import com.tuoniaostore.order.VO.GoodSnapshotPosVO;
import com.tuoniaostore.order.VO.GoodSnapshotVO;
import com.tuoniaostore.order.VO.OrderGoodSnapshotPosGroupVO;
import org.apache.ibatis.annotations.*;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 商品销售快照表
 * 大部分字段为冗余字段
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
@Mapper
public interface OrderGoodSnapshotMapper {

    String column = "`id`," +
            "`order_id`, " +//对应的订单ID
            "`user_mark`, " +//购买的用户ID或其他标志(如第三方用户ID)
            "`good_id`, " +//相关的商品ID
            "`good_template_id`, " +//商品模版ID
            "`good_name`, " +//当时的商品名称
            "`good_type_id`, " +//当时的归属类型ID
            "`good_type_name`, " +//当时归属的类型名
            "`good_unit_id`, " +//当时的单位ID
            "`good_unit_name`, " +//当时单位名称
            "`good_barcode`, " +//商品条码
            "`good_code`, " +//当时商品的编码
            "`good_batches`, " +//商品批次
            "`introduction_page`, " +//当时的介绍页面
            "`create_time`, " +//购买的时间
            "`normal_quantity`, " +//正常价格购买的数量
            "`discount_quantity`, " +//优惠价格购买的数量
            "`shipment_quantity`, " +//发货中的数量
            "`distribution_quantity`, " +//配送中的数量
            "`arrive_quantity`, " +//送达的数量
            "`receipt_quantity`, " +//被确认收货的数量
            "`refund_quantity`, " +//退货中数量
            "`return_quantity`, " +//退货的数量
            "`review_quantity`, " +//复核数量
            "`normal_price`, " +//正常价格
            "`discount_price`, " +//优惠价格
            "`cost_price`, " +//当时的成本价
            "`market_price`, " +//当时一般的市场价
            "`total_price`, " +//应收费用
            "`return_price`, " +//退货的费用(实收为应收费用-退货费用)
            "`inner_order_id`, " +//区分仓库的订单号
            "`third_party_delivery_quantity`, " +//第三方送货的数量
            "`third_party_accept_quantity`, " +//第三方排线的数量
            "`good_price_id`, " +
            "`good_price_title`, " +
            "`is_third_party`"//是否第三方商品，否为0
            ;

    @Insert("insert into order_good_snapshot (" +
            " `id`,  " +
            " `order_id`,  " +
            " `user_mark`,  " +
            " `good_id`,  " +
            " `good_template_id`,  " +
            " `good_name`,  " +
            " `good_type_id`,  " +
            " `good_type_name`,  " +
            " `good_unit_id`,  " +
            " `good_unit_name`,  " +
            " `good_barcode`,  " +
            " `good_code`,  " +
            " `good_batches`,  " +
            " `introduction_page`,  " +
            " `normal_quantity`,  " +
            " `discount_quantity`,  " +
            " `shipment_quantity`,  " +
            " `distribution_quantity`,  " +
            " `arrive_quantity`,  " +
            " `receipt_quantity`,  " +
            " `refund_quantity`,  " +
            " `return_quantity`,  " +
            " `review_quantity`,  " +
            " `normal_price`,  " +
            " `discount_price`,  " +
            " `cost_price`,  " +
            " `market_price`,  " +
            " `total_price`,  " +
            " `return_price`,  " +
            " `inner_order_id`,  " +
            " `third_party_delivery_quantity`,  " +
            " `third_party_accept_quantity`,  " +
            " `good_price_id` , " +
            " `good_price_title` , " +
            " `is_third_party` " +
            ")values(" +
            "#{id},  " +
            "#{orderId},  " +
            "#{userMark},  " +
            "#{goodId},  " +
            "#{goodTemplateId},  " +
            "#{goodName},  " +
            "#{goodTypeId},  " +
            "#{goodTypeName},  " +
            "#{goodUnitId},  " +
            "#{goodUnitName},  " +
            "#{goodBarcode},  " +
            "#{goodCode},  " +
            "#{goodBatches},  " +
            "#{introductionPage},  " +
            "#{normalQuantity},  " +
            "#{discountQuantity},  " +
            "#{shipmentQuantity},  " +
            "#{distributionQuantity},  " +
            "#{arriveQuantity},  " +
            "#{receiptQuantity},  " +
            "#{refundQuantity},  " +
            "#{returnQuantity},  " +
            "#{reviewQuantity},  " +
            "#{normalPrice},  " +
            "#{discountPrice},  " +
            "#{costPrice},  " +
            "#{marketPrice},  " +
            "#{totalPrice},  " +
            "#{returnPrice},  " +
            "#{innerOrderId},  " +
            "#{thirdPartyDeliveryQuantity},  " +
            "#{thirdPartyAcceptQuantity},  " +
            "#{goodPriceId},  " +
            "#{goodPriceTitle},  " +
            "#{isThirdParty} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addOrderGoodSnapshot(OrderGoodSnapshot orderGoodSnapshot);

    @Insert("insert into order_good_snapshot_pos (" +
            " `id`,  " +
            " `order_id`,  " +
            " `user_mark`,  " +
            " `good_id`,  " +
            " `good_template_id`,  " +
            " `good_name`,  " +
            " `good_type_id`,  " +
            " `good_type_name`,  " +
            " `good_unit_id`,  " +
            " `good_unit_name`,  " +
            " `good_barcode`,  " +
            " `good_code`,  " +
            " `good_batches`,  " +
            " `introduction_page`,  " +
            " `normal_quantity`,  " +
            " `discount_quantity`,  " +
            " `shipment_quantity`,  " +
            " `distribution_quantity`,  " +
            " `arrive_quantity`,  " +
            " `receipt_quantity`,  " +
            " `refund_quantity`,  " +
            " `return_quantity`,  " +
            " `review_quantity`,  " +
            " `normal_price`,  " +
            " `discount_price`,  " +
            " `cost_price`,  " +
            " `market_price`,  " +
            " `total_price`,  " +
            " `return_price`,  " +
            " `inner_order_id`,  " +
            " `third_party_delivery_quantity`,  " +
            " `third_party_accept_quantity`,  " +
            " `good_price_id` , " +
            " `good_price_title` , " +
            " `is_third_party` " +
            ")values(" +
            "#{id},  " +
            "#{orderId},  " +
            "#{userMark},  " +
            "#{goodId},  " +
            "#{goodTemplateId},  " +
            "#{goodName},  " +
            "#{goodTypeId},  " +
            "#{goodTypeName},  " +
            "#{goodUnitId},  " +
            "#{goodUnitName},  " +
            "#{goodBarcode},  " +
            "#{goodCode},  " +
            "#{goodBatches},  " +
            "#{introductionPage},  " +
            "#{normalQuantity},  " +
            "#{discountQuantity},  " +
            "#{shipmentQuantity},  " +
            "#{distributionQuantity},  " +
            "#{arriveQuantity},  " +
            "#{receiptQuantity},  " +
            "#{refundQuantity},  " +
            "#{returnQuantity},  " +
            "#{reviewQuantity},  " +
            "#{normalPrice},  " +
            "#{discountPrice},  " +
            "#{costPrice},  " +
            "#{marketPrice},  " +
            "#{totalPrice},  " +
            "#{returnPrice},  " +
            "#{innerOrderId},  " +
            "#{thirdPartyDeliveryQuantity},  " +
            "#{thirdPartyAcceptQuantity},  " +
            "#{goodPriceId},  " +
            "#{goodPriceTitle},  " +
            "#{isThirdParty} " +
            ")")
    @Options(useGeneratedKeys = true)
    void addOrderGoodSnapshotPos(OrderGoodSnapshot orderGoodSnapshot);

    @Results(id = "orderGoodSnapshot", value = {
            @Result(property = "id", column = "id"), @Result(property = "orderId", column = "order_id"), @Result(property = "userMark", column = "user_mark"), @Result(property = "goodId", column = "good_id"), @Result(property = "goodTemplateId", column = "good_template_id"), @Result(property = "goodName", column = "good_name"), @Result(property = "goodTypeId", column = "good_type_id"), @Result(property = "goodTypeName", column = "good_type_name"), @Result(property = "goodUnitId", column = "good_unit_id"), @Result(property = "goodUnitName", column = "good_unit_name"), @Result(property = "goodBarcode", column = "good_barcode"), @Result(property = "goodCode", column = "good_code"), @Result(property = "goodBatches", column = "good_batches"), @Result(property = "introductionPage", column = "introduction_page"), @Result(property = "createTime", column = "create_time"), @Result(property = "normalQuantity", column = "normal_quantity"), @Result(property = "discountQuantity", column = "discount_quantity"), @Result(property = "shipmentQuantity", column = "shipment_quantity"), @Result(property = "distributionQuantity", column = "distribution_quantity"), @Result(property = "arriveQuantity", column = "arrive_quantity"), @Result(property = "receiptQuantity", column = "receipt_quantity"), @Result(property = "refundQuantity", column = "refund_quantity"), @Result(property = "returnQuantity", column = "return_quantity"), @Result(property = "reviewQuantity", column = "review_quantity"), @Result(property = "normalPrice", column = "normal_price"), @Result(property = "discountPrice", column = "discount_price"), @Result(property = "costPrice", column = "cost_price"), @Result(property = "marketPrice", column = "market_price"), @Result(property = "totalPrice", column = "total_price"), @Result(property = "returnPrice", column = "return_price"), @Result(property = "innerOrderId", column = "inner_order_id"), @Result(property = "thirdPartyDeliveryQuantity", column = "third_party_delivery_quantity"), @Result(property = "thirdPartyAcceptQuantity", column = "third_party_accept_quantity"), @Result(property = "isThirdParty", column = "is_third_party")})
    @Select("select " + column + " from order_good_snapshot where id = #{id}")
    OrderGoodSnapshot getOrderGoodSnapshot(@Param("id") String id);

    @Select("<script>select  " + column + "  from order_good_snapshot   where   retrieve_status=0  and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when>" +
            "  order by sort desc   " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("orderGoodSnapshot")
    List<OrderGoodSnapshot> getOrderGoodSnapshots(@Param("status") int status,@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize,  @Param("name") String name);

    @Select("select  " + column + "  from order_good_snapshot")
    @ResultMap("orderGoodSnapshot")
    List<OrderGoodSnapshot> getOrderGoodSnapshotAll();

    @Select("<script>select count(1) from order_good_snapshot   where   retrieve_status=0 and status=#{status} " +
            " <when test=\"name != null and name.trim() != ''\">" +
            "  and name like concat('%', #{name}, '%') " +
            " </when></script>")
    int getOrderGoodSnapshotCount(@Param("status") int status, @Param("name") String name);

    @Update("update order_good_snapshot  " +
            "set " +
            "`order_id` = #{orderId}  , " +
            "`user_mark` = #{userMark}  , " +
            "`good_id` = #{goodId}  , " +
            "`good_template_id` = #{goodTemplateId}  , " +
            "`good_name` = #{goodName}  , " +
            "`good_type_id` = #{goodTypeId}  , " +
            "`good_type_name` = #{goodTypeName}  , " +
            "`good_unit_id` = #{goodUnitId}  , " +
            "`good_unit_name` = #{goodUnitName}  , " +
            "`good_barcode` = #{goodBarcode}  , " +
            "`good_code` = #{goodCode}  , " +
            "`good_batches` = #{goodBatches}  , " +
            "`introduction_page` = #{introductionPage}  , " +
            "`create_time` = #{createTime}  , " +
            "`normal_quantity` = #{normalQuantity}  , " +
            "`discount_quantity` = #{discountQuantity}  , " +
            "`shipment_quantity` = #{shipmentQuantity}  , " +
            "`distribution_quantity` = #{distributionQuantity}  , " +
            "`arrive_quantity` = #{arriveQuantity}  , " +
            "`receipt_quantity` = #{receiptQuantity}  , " +
            "`refund_quantity` = #{refundQuantity}  , " +
            "`return_quantity` = #{returnQuantity}  , " +
            "`review_quantity` = #{reviewQuantity}  , " +
            "`normal_price` = #{normalPrice}  , " +
            "`discount_price` = #{discountPrice}  , " +
            "`cost_price` = #{costPrice}  , " +
            "`market_price` = #{marketPrice}  , " +
            "`total_price` = #{totalPrice}  , " +
            "`return_price` = #{returnPrice}  , " +
            "`inner_order_id` = #{innerOrderId}  , " +
            "`third_party_delivery_quantity` = #{thirdPartyDeliveryQuantity}  , " +
            "`third_party_accept_quantity` = #{thirdPartyAcceptQuantity}  , " +
            "`good_price_id` = #{goodPriceId}  , " +
            "`good_price_title` = #{goodPriceTitle}  , " +
            "`is_third_party` = #{isThirdParty}  " +
            " where id = #{id}")
    void changeOrderGoodSnapshot(OrderGoodSnapshot orderGoodSnapshot);

    @Update("<script>" +
            "<foreach collection=\"lists\" item=\"item\" index='index' open='' close='' separator=\";\"> " +
            "update order_good_snapshot  " +
            "set " +
            "`order_id` = #{item.orderId}  , " +
            "`user_mark` = #{item.userMark}  , " +
            "`good_id` = #{item.goodId}  , " +
            "`good_template_id` = #{item.goodTemplateId}  , " +
            "`good_name` = #{item.goodName}  , " +
            "`good_type_id` = #{item.goodTypeId}  , " +
            "`good_type_name` = #{item.goodTypeName}  , " +
            "`good_unit_id` = #{item.goodUnitId}  , " +
            "`good_unit_name` = #{item.goodUnitName}  , " +
            "`good_barcode` = #{item.goodBarcode}  , " +
            "`good_code` = #{item.goodCode}  , " +
            "`good_batches` = #{item.goodBatches}  , " +
            "`introduction_page` = #{item.introductionPage}  , " +
            "`create_time` = #{item.createTime}  , " +
            "`normal_quantity` = #{item.normalQuantity}  , " +
            "`discount_quantity` = #{item.discountQuantity}  , " +
            "`shipment_quantity` = #{item.shipmentQuantity}  , " +
            "`distribution_quantity` = #{item.distributionQuantity}  , " +
            "`arrive_quantity` = #{item.arriveQuantity}  , " +
            "`receipt_quantity` = #{item.receiptQuantity}  , " +
            "`refund_quantity` = #{item.refundQuantity}  , " +
            "`return_quantity` = #{item.returnQuantity}  , " +
            "`review_quantity` = #{item.reviewQuantity}  , " +
            "`normal_price` = #{item.normalPrice}  , " +
            "`discount_price` = #{item.discountPrice}  , " +
            "`cost_price` = #{item.costPrice}  , " +
            "`market_price` = #{item.marketPrice}  , " +
            "`total_price` = #{item.totalPrice}  , " +
            "`return_price` = #{item.returnPrice}  , " +
            "`inner_order_id` = #{item.innerOrderId}  , " +
            "`third_party_delivery_quantity` = #{item.thirdPartyDeliveryQuantity}  , " +
            "`third_party_accept_quantity` = #{item.thirdPartyAcceptQuantity}  , " +
            "`good_price_id` = #{item.goodPriceId}  , " +
            "`good_price_title` = #{item.goodPriceTitle}  , " +
            "`is_third_party` = #{item.isThirdParty}  " +
            " where `id` = #{item.id}" +
            "</foreach>" +
            ";" +
            "</script>")
    void batchChangeOrderGoodSnapshot(@Param("lists") List<OrderGoodSnapshot> orderGoodSnapshotChangeList);

    @Select("select  " + column + "  from order_good_snapshot   where   order_id = #{orderId}")
    @ResultMap("orderGoodSnapshot")
    List<OrderGoodSnapshot> getOrderGoodSnapshotByOrderId(@Param("orderId")String orderId);

    @Select("<script> select  "+column+"  from order_good_snapshot" +
            " where  (order_id in <foreach collection=\"orderIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach>)</script>")
    @ResultMap("orderGoodSnapshot")
    List<OrderGoodSnapshot> getOrderGoodSnapshotByOrderInIds(@Param("orderIds")List<String> orderIds);

    @Select("<script> select  "+column+"  from order_good_snapshot" +
            " where  id in <foreach collection=\"ids\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach></script>")
    @ResultMap("orderGoodSnapshot")
    List<OrderGoodSnapshot> getOrderGoodSnapshotByIds(@Param("ids")List<String> ids);

    @Select("<script> select  "+column+"  from order_good_snapshot_pos" +
            " where  (order_id in <foreach collection=\"orderIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach>)</script>")
    @ResultMap("orderGoodSnapshot")
    List<OrderGoodSnapshot> getOrderGoodSnapshotPosByOrderInIds(@Param("orderIds")List<String> orderIds);

    @Results(id = "orderGoodSnapshotFormatDate", value = {
            @Result(property = "id", column = "id"), @Result(property = "orderId", column = "order_id"), @Result(property = "userMark", column = "user_mark"), @Result(property = "goodId", column = "good_id"), @Result(property = "goodTemplateId", column = "good_template_id"), @Result(property = "goodName", column = "good_name"), @Result(property = "goodTypeId", column = "good_type_id"), @Result(property = "goodTypeName", column = "good_type_name"), @Result(property = "goodUnitId", column = "good_unit_id"), @Result(property = "goodUnitName", column = "good_unit_name"), @Result(property = "goodBarcode", column = "good_barcode"), @Result(property = "goodCode", column = "good_code"), @Result(property = "goodBatches", column = "good_batches"), @Result(property = "introductionPage", column = "introduction_page"), @Result(property = "createTime", column = "create_time"), @Result(property = "normalQuantity", column = "normal_quantity"), @Result(property = "discountQuantity", column = "discount_quantity"), @Result(property = "shipmentQuantity", column = "shipment_quantity"), @Result(property = "distributionQuantity", column = "distribution_quantity"), @Result(property = "arriveQuantity", column = "arrive_quantity"), @Result(property = "receiptQuantity", column = "receipt_quantity"), @Result(property = "refundQuantity", column = "refund_quantity"), @Result(property = "returnQuantity", column = "return_quantity"), @Result(property = "reviewQuantity", column = "review_quantity"), @Result(property = "normalPrice", column = "normal_price"), @Result(property = "discountPrice", column = "discount_price"), @Result(property = "costPrice", column = "cost_price"), @Result(property = "marketPrice", column = "market_price"), @Result(property = "totalPrice", column = "total_price"), @Result(property = "returnPrice", column = "return_price"), @Result(property = "innerOrderId", column = "inner_order_id"), @Result(property = "thirdPartyDeliveryQuantity", column = "third_party_delivery_quantity"), @Result(property = "thirdPartyAcceptQuantity", column = "third_party_accept_quantity"), @Result(property = "isThirdParty", column = "is_third_party")})
    @Select("<script> select  "+column+"  from order_good_snapshot_pos" +
            " where  (order_id in <foreach collection=\"orderIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach>)</script>")
    List<OrderGoodSnapshotFormatDate> getOrderGoodSnapshotFormatDateByOrderInIds(@Param("orderIds")List<String> orderIds);

    @Update("update order_good_snapshot set `distribution_quantity` = ifnull(distribution_quantity,0) + #{distributionQuantity} where id = #{id}")
    void changeOrderGoodSnapshotNormalQuantity(OrderGoodSnapshot orderGoodSnapshot);

    @Update("update order_good_snapshot set `distribution_quantity` = ifnull(distribution_quantity,0) + #{distributionQuantity} where order_id = #{orderId}  and good_price_id = #{goodPriceId}  ")
    void changePrimaryOrderGoodSnapshotNormalQuantity(OrderGoodSnapshot orderGoodSnapshot);

    @Select("<script>select  " + column + "  from order_good_snapshot   where   1 = 1" +
            " <when test=\"map.orderSequenceNumbers != null\">and order_id in " +
            "<foreach collection=\"map.orderSequenceNumbers\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> " +
            "</when> "+
            " <when test=\"map.orderIds != null\">and order_id in " +
            "<foreach collection=\"map.orderIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> " +
            "</when> "+
            " <when test=\"map.goodPriceId != null\">and good_price_id = #{map.goodPriceId}</when> "+
            " </script>")
    @ResultMap("orderGoodSnapshot")
    List<OrderGoodSnapshot> getOrderGoodSnapshotByMap(@Param("map") Map<String,Object> map);

    @Select("<script> select  "+column+"  from order_good_snapshot where order_id in " +
            "(select id from order_entry where order_sequence_number = #{orderNum} or sequence_number = #{orderNum} " +
            "UNION " +
            "select id from order_entry_pos where order_sequence_number = #{orderNum} or sequence_number = #{orderNum}) " +
            "UNION " +
            "select "+column+" from order_good_snapshot_pos where order_id in " +
            "(select id from order_entry where order_sequence_number = #{orderNum} or sequence_number = #{orderNum} " +
            "UNION " +
            "select id from order_entry_pos where order_sequence_number = #{orderNum} or sequence_number = #{orderNum} )" +
            "</script>")
    @ResultMap("orderGoodSnapshot")
    List<OrderGoodSnapshot> getOrderGoodSnapshotByOrderNum(@Param("orderNum") String orderNum);

    @Update("<script>update order_good_snapshot set receipt_quantity = case <foreach collection=\"lists\" item=\"item\" > " +
            "when id = #{item.id} then #{item.receiptQuantity}</foreach> end where id in <foreach collection=\"lists\" item=\"item\" open=\"(\" close=\")\" separator=\",\"> #{item.id}</foreach></script>")
    void updateReceiptQuantity(@Param("lists") List<OrderGoodSnapshot> orderGoodSnapshots);


    @Select("<script> select  " + column + "  from order_good_snapshot_pos   where   order_id = #{orderId} " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when>"+
            " </script>")
    @Results(id = "goodSnapshotVO", value = {
            @Result(property = "totalPrice", column = "total_price"), @Result(property = "goodBarcode", column = "good_barcode"),
            @Result(property = "goodUnitName", column = "good_unit_name"), @Result(property = "normalQuantity", column = "normal_quantity"),
            @Result(property = "goodName", column = "good_name"), @Result(property = "normalPrice", column = "normal_price")})
    List<GoodSnapshotVO> getOrderGoodSnapshotPosByOrderId(@Param("orderId")String orderId,@Param("pageIndex")int pageIndex,@Param("pageSize")int pageSize);

    @Select("<script> select  count(0)  from order_good_snapshot_pos   where   order_id = #{orderId} " +
            " <when test=\"pageIndex != null and pageSize != null\">" +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when>"+
            " </script>")
    int getOrderGoodSnapshotPosByOrderIdCount(@Param("orderId")String orderId,@Param("pageIndex")int pageIndex,@Param("pageSize")int pageSize);


    @Update("update order_good_snapshot_pos  " +
            "set " +
            "return_quantity = #{refundQuantity}" +
            ",return_price = #{retunrnMoney}" +
            " where id = #{id}")
    void changeOrderGoodSnapshotPos(GoodSnapshotPosVO goodSnapshotPosVO);


    @Select("<script> select  " + column + "  from order_good_snapshot_pos   where  1 = 1" +
            " <when test=\"map.orderId != null and map.orderId != ''\"> and order_id = #{map.orderId} </when>" +
            " <when test=\"map.orderSequenceNumbers != null\">and order_id in <foreach collection=\"map.orderSequenceNumbers\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </when> "+
            " <when test=\"map.snapshotIds != null\">and id in <foreach collection=\"map.snapshotIds\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </when> "+
            "</script>")
    @ResultMap("orderGoodSnapshot")
    List<OrderGoodSnapshot> getOrderGoodSnapshotPosByMap(@Param("map")Map map);

    @Select("<script> select  " + column + "  from order_good_snapshot_pos   where   order_id = #{map.orderId} </script>")
    OrderGoodSnapshotPosGroupVO getOrderGoodSnapshotPosGroupByMap(@Param("map")Map map);
}
