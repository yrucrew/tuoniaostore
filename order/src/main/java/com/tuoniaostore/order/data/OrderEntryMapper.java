package com.tuoniaostore.order.data;

import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.vo.order.*;
import com.tuoniaostore.order.VO.*;
import org.apache.ibatis.annotations.*;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单实体表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
@Mapper
public interface OrderEntryMapper {

    String column = "`id`," +
            "`sequence_number`, " +//序列号(对应于购物车)
            "`order_sequence_number`, " +//订单的序列号(用于展示)
            "`partner_id`, " +//下游商户服务号ID
            "`partner_user_id`, " +//下单用户ID 匿名时可为0或空
            "`type`, " +//订单类型 1-供应链销售订单、2-供应链调拨订单、3-供应链采购订单、4-超市POS机订单（包括超市扫码订单）、7-无人便利店订单、9-增值服务订单、10-无人货架订单、11-向1号生活扫码支付（公司收款）、12-1号生活加盟店（收加盟费）
            "`status`, " +//订单状态 不要占用0  1-创建订单（未确认）、2-已取消、8-已支付、16-已接单、32-发货中、64-配送中、128-已送达、256-确认收货/已完成、260-***确认收货/已完成、512-已收款、1024-分批配送、2048-拒绝订单。
            "`deliver_status`, " +//发货状态：0-未发货、32-发货中、64-配送中、128-已送达
            "`refund_status`, " +//退款状态 0表示未退货 同时可用于记录是否已打款状态
            "`payment_type`, " +//支付类型 参考逻辑设定 -1未支付
            "`trans_type`, " +//交易类型，微信系：JSAPI、NATIVE、APP等
            "`user_source`, " +//用户来源
            "`day_sort_number`, " +//当天排号 相对于发货方
            "`push_type`, " +//推送类型，0表示不推送 1时表示下单时推送 2时表示支付后推送
            "`sale_user_id`, " +//实际销售的用户ID（销售仓）
            "`shipping_user_id`, " +//发货用户ID
            "`shipping_nick_name`, " +//发货用户昵称
            "`shipping_province`, " +//发货省份
            "`shipping_city`, " +//发货城市
            "`shipping_district`, " +//发货区域
            "`shipping_address`, " +//发货详细地址(尽量不要包含省市区)
            "`shipping_location`, " +//latitude,longitude
            "`shipping_phone`, " +//发货人联系号码
            "`receipt_user_id`, " +//收货人用户ID
            "`receipt_nick_name`, " +//收货人昵称
            "`receipt_province`, " +//收货省份
            "`receipt_city`, " +//收货城市
            "`receipt_district`, " +//收货区域
            "`receipt_address`, " +//收货具体地址(尽量不要包含省市区)
            "`receipt_phone`, " +//收货人电话
            "`receipt_location`, " +//latitude,longitude
            "`courier_user_id`, " +//配送员用户ID
            "`courier_nick_name`, " +//配送员昵称
            "`courier_phone`, " +//配送员联系号码
            "`total_distance`, " +//订单总距离
            "`current_location`, " +//下单时所在地理位置
            "`collecting_fees`, " +//是否需要配送员代收费用 0表示不需要
            "`remark`, " +//订单描述
            "`actual_price`, " +//实收费用
            "`total_price`, " +//订单总费用
            "`discount_price`, " +//优惠的费用
            "`distribution_fee`, " +//配送费用
            "`price_logger`, " +//价格记录 如优惠的政策
            "`tax_rate`, " +//税率(万分比)
            "`cancel_logger`, " +//取消的理由
            "`create_time`, " +//订单的生成时间
            "`payment_time`, " +//支付时间
            "`confirm_time`, " +//确认订单时间
            "`body`, " +//商品或支付单简要描述
            "`detail`, " +//商品名称明细列表(仅记录体系外的数据)
            "`credit_fee`, " +//信用付费用
            "`third_party_delivery`, " +//是否使用第三方物流 0否 1是
            "`membership_price`, " +//会员价(分)
            "`third_party_delivery_status`, " +//第三方物流的状态 1-待推,2-全部推完,3-冲红
            "`refund_remark`, " +//退款备注
            "`refund_time`, " +//退款时间
            "`refund_type`, " +// 退款方式
            "`return_operator_user_id`, " +//商品退款操作人
            "`find_price`, " +//找零
            "`receiving_time`, " +//待收货时间
            "`is_join` " //是否为加盟店 1为是
            ;

    @Insert("insert into order_entry (" +
            " `id`,  " +
            " `sequence_number`,  " +
            " `order_sequence_number`,  " +
            " `partner_id`,  " +
            " `partner_user_id`,  " +
            " `type`,  " +
            " `status`,  " +
            " `deliver_status`,  " +
            " `refund_status`,  " +
            " `payment_type`,  " +
            " `trans_type`,  " +
            " `user_source`,  " +
            " `day_sort_number`,  " +
            " `push_type`,  " +
            " `sale_user_id`,  " +
            " `shipping_user_id`,  " +
            " `shipping_nick_name`,  " +
            " `shipping_province`,  " +
            " `shipping_city`,  " +
            " `shipping_district`,  " +
            " `shipping_address`,  " +
            " `shipping_location`,  " +
            " `shipping_phone`,  " +
            " `receipt_user_id`,  " +
            " `receipt_nick_name`,  " +
            " `receipt_province`,  " +
            " `receipt_city`,  " +
            " `receipt_district`,  " +
            " `receipt_address`,  " +
            " `receipt_phone`,  " +
            " `receipt_location`,  " +
            " `courier_user_id`,  " +
            " `courier_nick_name`,  " +
            " `courier_phone`,  " +
            " `total_distance`,  " +
            " `current_location`,  " +
            " `collecting_fees`,  " +
            " `remark`,  " +
            " `actual_price`,  " +
            " `total_price`,  " +
            " `discount_price`,  " +
            " `distribution_fee`,  " +
            " `price_logger`,  " +
            " `tax_rate`,  " +
            " `cancel_logger`,  " +
            " `payment_time`,  " +
            " `confirm_time`,  " +
            " `body`,  " +
            " `detail`,  " +
            " `credit_fee`,  " +
            " `third_party_delivery`,  " +
            " `membership_price`,  " +
            " `third_party_delivery_status`,  " +
            " `find_price`,  " +
            " `receiving_time`,  " +
            " `is_join` " +
            ")values(" +
            "#{id},  " +
            "#{sequenceNumber},  " +
            "#{orderSequenceNumber},  " +
            "#{partnerId},  " +
            "#{partnerUserId},  " +
            "#{type},  " +
            "#{status},  " +
            "#{deliverStatus},  " +
            "#{refundStatus},  " +
            "#{paymentType},  " +
            "#{transType},  " +
            "#{userSource},  " +
            "#{daySortNumber},  " +
            "#{pushType},  " +
            "#{saleUserId},  " +
            "#{shippingUserId},  " +
            "#{shippingNickName},  " +
            "#{shippingProvince},  " +
            "#{shippingCity},  " +
            "#{shippingDistrict},  " +
            "#{shippingAddress},  " +
            "#{shippingLocation},  " +
            "#{shippingPhone},  " +
            "#{receiptUserId},  " +
            "#{receiptNickName},  " +
            "#{receiptProvince},  " +
            "#{receiptCity},  " +
            "#{receiptDistrict},  " +
            "#{receiptAddress},  " +
            "#{receiptPhone},  " +
            "#{receiptLocation},  " +
            "#{courierUserId},  " +
            "#{courierNickName},  " +
            "#{courierPhone},  " +
            "#{totalDistance},  " +
            "#{currentLocation},  " +
            "#{collectingFees},  " +
            "#{remark},  " +
            "#{actualPrice},  " +
            "#{totalPrice},  " +
            "#{discountPrice},  " +
            "#{distributionFee},  " +
            "#{priceLogger},  " +
            "#{taxRate},  " +
            "#{cancelLogger},  " +
            "#{paymentTime},  " +
            "#{confirmTime},  " +
            "#{body},  " +
            "#{detail},  " +
            "#{creditFee},  " +
            "#{thirdPartyDelivery},  " +
            "#{membershipPrice},  " +
            "#{thirdPartyDeliveryStatus},  " +
            "#{findPrice},  " +
            "#{receivingTime},  " +
            "#{isJoin} " +
            ")")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void addOrderEntry(OrderEntry orderEntry);

    @Insert("insert into order_entry_pos     (" +
            " `id`,  " +
            " `sequence_number`,  " +
            " `order_sequence_number`,  " +
            " `partner_id`,  " +
            " `partner_user_id`,  " +
            " `type`,  " +
            " `status`,  " +
            " `deliver_status`,  " +
            " `refund_status`,  " +
            " `payment_type`,  " +
            " `trans_type`,  " +
            " `user_source`,  " +
            " `day_sort_number`,  " +
            " `push_type`,  " +
            " `sale_user_id`,  " +
            " `shipping_user_id`,  " +
            " `shipping_nick_name`,  " +
            " `shipping_province`,  " +
            " `shipping_city`,  " +
            " `shipping_district`,  " +
            " `shipping_address`,  " +
            " `shipping_location`,  " +
            " `shipping_phone`,  " +
            " `receipt_user_id`,  " +
            " `receipt_nick_name`,  " +
            " `receipt_province`,  " +
            " `receipt_city`,  " +
            " `receipt_district`,  " +
            " `receipt_address`,  " +
            " `receipt_phone`,  " +
            " `receipt_location`,  " +
            " `courier_user_id`,  " +
            " `courier_nick_name`,  " +
            " `courier_phone`,  " +
            " `total_distance`,  " +
            " `current_location`,  " +
            " `collecting_fees`,  " +
            " `remark`,  " +
            " `actual_price`,  " +
            " `total_price`,  " +
            " `discount_price`,  " +
            " `distribution_fee`,  " +
            " `price_logger`,  " +
            " `tax_rate`,  " +
            " `cancel_logger`,  " +
            " `payment_time`,  " +
            " `confirm_time`,  " +
            " `body`,  " +
            " `detail`,  " +
            " `credit_fee`,  " +
            " `third_party_delivery`,  " +
            " `membership_price`,  " +
            " `third_party_delivery_status`,  " +
            " `find_price`,  " +
            " `receiving_time`,  " +
            " `is_join` " +
            ")values(" +
            "#{id},  " +
            "#{sequenceNumber},  " +
            "#{orderSequenceNumber},  " +
            "#{partnerId},  " +
            "#{partnerUserId},  " +
            "#{type},  " +
            "#{status},  " +
            "#{deliverStatus},  " +
            "#{refundStatus},  " +
            "#{paymentType},  " +
            "#{transType},  " +
            "#{userSource},  " +
            "#{daySortNumber},  " +
            "#{pushType},  " +
            "#{saleUserId},  " +
            "#{shippingUserId},  " +
            "#{shippingNickName},  " +
            "#{shippingProvince},  " +
            "#{shippingCity},  " +
            "#{shippingDistrict},  " +
            "#{shippingAddress},  " +
            "#{shippingLocation},  " +
            "#{shippingPhone},  " +
            "#{receiptUserId},  " +
            "#{receiptNickName},  " +
            "#{receiptProvince},  " +
            "#{receiptCity},  " +
            "#{receiptDistrict},  " +
            "#{receiptAddress},  " +
            "#{receiptPhone},  " +
            "#{receiptLocation},  " +
            "#{courierUserId},  " +
            "#{courierNickName},  " +
            "#{courierPhone},  " +
            "#{totalDistance},  " +
            "#{currentLocation},  " +
            "#{collectingFees},  " +
            "#{remark},  " +
            "#{actualPrice},  " +
            "#{totalPrice},  " +
            "#{discountPrice},  " +
            "#{distributionFee},  " +
            "#{priceLogger},  " +
            "#{taxRate},  " +
            "#{cancelLogger},  " +
            "#{paymentTime},  " +
            "#{confirmTime},  " +
            "#{body},  " +
            "#{detail},  " +
            "#{creditFee},  " +
            "#{thirdPartyDelivery},  " +
            "#{membershipPrice},  " +
            "#{thirdPartyDeliveryStatus},  " +
            "#{findPrice},  " +
            "#{receivingTime},  " +
            "#{isJoin} " +
            ")")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void addOrderEntryPos(OrderEntry orderEntry);

    @Results(id = "orderEntry", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "sequenceNumber", column = "sequence_number"),
            @Result(property = "orderSequenceNumber", column = "order_sequence_number"),
            @Result(property = "partnerId", column = "partner_id"),
            @Result(property = "partnerUserId", column = "partner_user_id"),
            @Result(property = "type", column = "type"),
            @Result(property = "status", column = "status"),
            @Result(property = "deliverStatus", column = "deliver_status"),
            @Result(property = "refundStatus", column = "refund_status"),
            @Result(property = "paymentType", column = "payment_type"),
            @Result(property = "transType", column = "trans_type"),
            @Result(property = "userSource", column = "user_source"),
            @Result(property = "daySortNumber", column = "day_sort_number"),
            @Result(property = "pushType", column = "push_type"),
            @Result(property = "saleUserId", column = "sale_user_id"),
            @Result(property = "shippingUserId", column = "shipping_user_id"),
            @Result(property = "shippingNickName", column = "shipping_nick_name"),
            @Result(property = "shippingProvince", column = "shipping_province"),
            @Result(property = "refundRemark ", column = "refund_remark "),
            @Result(property = "refundTime", column = "refund_time"),
            @Result(property = "refundType", column = "refund_type"),
            @Result(property = "shippingCity", column = "shipping_city"),
            @Result(property = "findPrice", column = "find_price"),
            @Result(property = "returnOperatorUserId", column = "return_operator_user_id"),
            @Result(property = "shippingDistrict", column = "shipping_district"),
            @Result(property = "shippingAddress", column = "shipping_address"),
            @Result(property = "shippingLocation", column = "shipping_location"),
            @Result(property = "shippingPhone", column = "shipping_phone"),
            @Result(property = "receiptUserId", column = "receipt_user_id"),
            @Result(property = "receiptNickName", column = "receipt_nick_name"),
            @Result(property = "receiptProvince", column = "receipt_province"),
            @Result(property = "receiptCity", column = "receipt_city"),
            @Result(property = "receiptDistrict", column = "receipt_district"),
            @Result(property = "receiptAddress", column = "receipt_address"),
            @Result(property = "receiptPhone", column = "receipt_phone"),
            @Result(property = "receiptLocation", column = "receipt_location"),
            @Result(property = "courierUserId", column = "courier_user_id"),
            @Result(property = "courierNickName", column = "courier_nick_name"),
            @Result(property = "courierPhone", column = "courier_phone"),
            @Result(property = "totalDistance", column = "total_distance"),
            @Result(property = "currentLocation", column = "current_location"),
            @Result(property = "collectingFees", column = "collecting_fees"),
            @Result(property = "remark", column = "remark"),
            @Result(property = "actualPrice", column = "actual_price"),
            @Result(property = "totalPrice", column = "total_price"),
            @Result(property = "discountPrice", column = "discount_price"),
            @Result(property = "distributionFee", column = "distribution_fee"),
            @Result(property = "priceLogger", column = "price_logger"),
            @Result(property = "taxRate", column = "tax_rate"),
            @Result(property = "cancelLogger", column = "cancel_logger"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "paymentTime", column = "payment_time"),
            @Result(property = "confirmTime", column = "confirm_time"),
            @Result(property = "body", column = "body"),
            @Result(property = "detail", column = "detail"),
            @Result(property = "creditFee", column = "credit_fee"),
            @Result(property = "thirdPartyDelivery", column = "third_party_delivery"),
            @Result(property = "membershipPrice", column = "membership_price"),
            @Result(property = "thirdPartyDeliveryStatus", column = "third_party_delivery_status"),
            @Result(property = "isJoin", column = "is_join"),
            @Result(property = "isThirdParty", column = "is_third_party"),
            @Result(property = "receivingTime", column = "receiving_time"),
            @Result(property = "reviewUserId", column = "review_user_id")})
    @Select("select " + column + " from order_entry where id = #{id}")
    OrderEntry getOrderEntry(@Param("id") String id);

    @Select("<script>select  " + column + "  from order_entry   where 1=1 " +
            " <when test=\"map.status != null and map.status != -1\">" +
            "  and status=#{map.status} " +
            " </when>" +
            " <when test=\"map.type != null and map.type != -1\">" +
            "   and type=#{map.type} " +
            " </when>" +
            " <when test=\"map.name != null and map.name.trim() != ''\">" +
            "  and receipt_nick_name like concat('%', #{map.name}, '%') " +
            " </when>" +
            " <when test=\"map.sequenceNumbers != null\"> and sequence_number = #{map.sequenceNumbers}</when>" +
            " <when test=\"map.orderSequenceNumber != null\"> and order_sequence_number like concat('%',#{map.orderSequenceNumber},'%') </when>" +
            " <when test=\"map.orderSequenceNumberOne != null\"> and order_sequence_number =  #{map.orderSequenceNumberOne} </when>" +
            " <when test=\"map.paymentType != null and map.paymentType != 999 \"> and payment_type = #{map.paymentType} </when>" +
            " <when test=\"map.shippingNickNameOrPhone != null and map.shippingNickNameOrPhone != ''\"> and (shipping_nick_name = #{map.shippingNickNameOrPhone} or shipping_phone = #{map.shippingNickNameOrPhone}) </when>" +
            " <when test=\"map.receiptNickNameOrPhone != null and map.receiptNickNameOrPhone != ''\"> and (receipt_nick_name = #{map.receiptNickNameOrPhone} or receipt_phone = #{map.receiptNickNameOrPhone}) </when>" +
            " <when test=\"map.startActualPrice != null and map.startActualPrice != -1\"> and actual_price <![CDATA[>=]]>  #{map.startActualPrice} </when>" +
            " <when test=\"map.endActualPrice != null and map.endActualPrice != -1\"> and actual_price <![CDATA[<=]]> #{map.endActualPrice} </when>" +
            " <when test=\"map.startTotalPrice != null and map.startTotalPrice != -1\"> and total_price <![CDATA[>=]]> #{map.startTotalPrice} </when>" +
            " <when test=\"map.endTotalPrice != null and map.endTotalPrice != -1\"> and total_price <![CDATA[<=]]> #{map.endTotalPrice} </when>" +
            " <when test=\"map.primarySequenceNumbers != null\">and (sequence_number in <foreach collection=\"map.primarySequenceNumbers\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> " +
            " <when test=\"map.types != null\">and (type in <foreach collection=\"map.types\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> " +
            " <when test=\"map.isPrimaryOrder  != null\"> and (sequence_number is null or sequence_number = '') </when> " +
            " <when test=\"map.everyday  != null\"> and create_time = to_days(now()) </when> " +
            " <when test=\"map.createStartTime != null and  map.createEndTime != null \"> and date_format(create_time,'%Y-%m-%d') between #{map.createStartTime} and #{map.createEndTime} </when> " +
            " <when test=\"map.createStartTime != null and  map.createEndTime == null \"> and date_format(create_time,'%Y-%m-%d') = #{map.createStartTime} </when> " +
            "  order by create_time desc   " +
            " <when test=\"map.pageIndex != null and map.pageSize != null\">" +
            "  limit #{map.pageIndex}, #{map.pageSize}" +
            "</when></script>")
    @ResultMap("orderEntry")
    List<OrderEntry> getOrderEntrys(@Param("map") Map<String, Object> map);

    @Select("<script>select  " + column + "  from order_entry   where   status=0 </script>")
    @ResultMap("orderEntry")
    List<OrderEntry> getOrderEntryAll();

    @Select("<script>select  count(1)  from order_entry   where 1=1 " +
            " <when test=\"map.status != null and map.status != -1\">" +
            "  and status=#{map.status} " +
            " </when>" +
            " <when test=\"map.type != null and map.type != -1\">" +
            "   and type=#{map.type} " +
            " </when>" +
            " <when test=\"map.name != null and map.name.trim() != ''\">" +
            "  and receipt_nick_name like concat('%', #{map.name}, '%') " +
            " </when>" +
            " <when test=\"map.sequenceNumbers != null\"> and sequence_number = #{map.sequenceNumbers}</when>" +
            " <when test=\"map.orderSequenceNumber != null\"> and order_sequence_number like concat('%',#{map.orderSequenceNumber},'%') </when>" +
            " <when test=\"map.paymentType != null and map.paymentType != 999 \"> and payment_type = #{map.paymentType} </when>" +
            " <when test=\"map.shippingNickNameOrPhone != null and map.shippingNickNameOrPhone != ''\"> and (shipping_nick_name = #{map.shippingNickNameOrPhone} or shipping_phone = #{map.shippingNickNameOrPhone}) </when>" +
            " <when test=\"map.receiptNickNameOrPhone != null and map.receiptNickNameOrPhone != ''\"> and (receipt_nick_name = #{map.receiptNickNameOrPhone} or receipt_phone = #{map.receiptNickNameOrPhone}) </when>" +
            " <when test=\"map.startActualPrice != null and map.startActualPrice != -1\"> and actual_price <![CDATA[>=]]>  #{map.startActualPrice} </when>" +
            " <when test=\"map.endActualPrice != null and map.endActualPrice != -1\"> and actual_price <![CDATA[<=]]> #{map.endActualPrice} </when>" +
            " <when test=\"map.startTotalPrice != null and map.startTotalPrice != -1\"> and total_price <![CDATA[>=]]> #{map.startTotalPrice} </when>" +
            " <when test=\"map.endTotalPrice != null and map.endTotalPrice != -1\"> and total_price <![CDATA[<=]]> #{map.endTotalPrice} </when>" +
            " <when test=\"map.primarySequenceNumbers != null\">and (sequence_number in <foreach collection=\"map.primarySequenceNumbers\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> " +
            " <when test=\"map.types != null\">and (type in <foreach collection=\"map.types\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> " +
            " <when test=\"map.isPrimaryOrder  != null\"> and (sequence_number is null or sequence_number = '') </when> " +
            " <when test=\"map.everyday  != null\"> and create_time = to_days(now()) </when> " +
            " <when test=\"map.createStartTime != null and  map.createEndTime != null \"> and date_format(create_time,'%Y-%m-%d') between #{map.createStartTime} and #{map.createEndTime} </when> " +
            " <when test=\"map.createStartTime != null and  map.createEndTime == null \"> and date_format(create_time,'%Y-%m-%d') = #{map.createStartTime} </when> " +
            " </script>")
    int getOrderEntryCount(@Param("map") Map<String, Object> map);

    @Update("<script> update order_entry  " +
            "set id = id" +
            " <when test = \"sequenceNumber != null and sequenceNumber != ''\">  ,  `sequence_number` = #{sequenceNumber} </when>" +
            " <when test = \"orderSequenceNumber != null and orderSequenceNumber != ''\"> , `order_sequence_number` = #{orderSequenceNumber}  </when>" +
            " <when test = \"partnerId != null and partnerId != ''\"> , `partner_id` = #{partnerId} </when>" +
            " <when test = \"partnerUserId != null and partnerUserId != ''\"> , `partner_user_id` = #{partnerUserId} </when>" +
            " <when test = \"type != null and type != ''\"> , `type` = #{type} </when>" +
            " <when test = \"status != null and status != ''\"> , `status` = #{status} </when>" +
            " <when test = \"deliverStatus != null and deliverStatus != ''\"> , `deliver_status` = #{deliverStatus} </when>" +
            " <when test = \"refundStatus != null \"> , `refund_status` = #{refundStatus} </when>" +
            " <when test = \"paymentType != null\"> , `payment_type` = #{paymentType} </when>" +
            " <when test = \"transType != null and transType != ''\"> , `trans_type` = #{transType} </when>" +
            " <when test = \"userSource != null and userSource != ''\"> , `user_source` = #{userSource} </when>" +
            " <when test = \"daySortNumber != null and daySortNumber != ''\"> , `day_sort_number` = #{daySortNumber} </when>" +
            " <when test = \"pushType != null and pushType != ''\"> , `push_type` = #{pushType} </when>" +
            " <when test = \"saleUserId != null and saleUserId != ''\"> , `sale_user_id` = #{saleUserId} </when>" +
            " <when test = \"shippingUserId != null and shippingUserId != ''\"> , `shipping_user_id` = #{shippingUserId} </when>" +
            " <when test = \"shippingNickName != null and shippingNickName != ''\"> , `shipping_nick_name` = #{shippingNickName} </when>" +
            " <when test = \"shippingProvince != null and shippingProvince != ''\"> , `shipping_province` = #{shippingProvince} </when>" +
            " <when test = \"shippingCity != null and shippingCity != ''\"> , `shipping_city` = #{shippingCity} </when>" +
            " <when test = \"shippingDistrict != null and shippingDistrict != ''\"> , `shipping_district` = #{shippingDistrict} </when>" +
            " <when test = \"shippingAddress != null and shippingAddress != ''\"> , `shipping_address` = #{shippingAddress} </when>" +
            " <when test = \"shippingLocation != null and shippingLocation != ''\"> , `shipping_location` = #{shippingLocation} </when>" +
            " <when test = \"shippingPhone != null and shippingPhone != ''\"> , `shipping_phone` = #{shippingPhone} </when>" +
            " <when test = \"receiptUserId != null and receiptUserId != ''\"> , `receipt_user_id` = #{receiptUserId} </when>" +
            " <when test = \"receiptNickName != null and receiptNickName != ''\"> , `receipt_nick_name` = #{receiptNickName} </when>" +
            " <when test = \"receiptProvince != null and receiptProvince != ''\"> , `receipt_province` = #{receiptProvince} </when>" +
            " <when test = \"receiptCity != null and receiptCity != ''\"> , `receipt_city` = #{receiptCity} </when>" +
            " <when test = \"receiptDistrict != null and receiptDistrict != ''\"> , `receipt_district` = #{receiptDistrict} </when>" +
            " <when test = \"receiptAddress != null and receiptAddress != ''\"> , `receipt_address` = #{receiptAddress} </when>" +
            " <when test = \"receiptPhone != null and receiptPhone != ''\"> , `receipt_phone` = #{receiptPhone} </when>" +
            " <when test = \"receiptLocation != null and receiptLocation != ''\"> , `receipt_location` = #{receiptLocation} </when>" +
            " <when test = \"courierUserId != null and courierUserId != ''\"> , `courier_user_id` = #{courierUserId} </when>" +
            " <when test = \"courierNickName != null and courierNickName != ''\"> , `courier_nick_name` = #{courierNickName} </when>" +
            " <when test = \"courierPhone != null and courierPhone != ''\"> , `courier_phone` = #{courierPhone} </when>" +
            " <when test = \"totalDistance != null and totalDistance != ''\"> , `total_distance` = #{totalDistance} </when>" +
            " <when test = \"currentLocation != null and currentLocation != ''\"> , `current_location` = #{currentLocation} </when>" +
            " <when test = \"collectingFees != null and collectingFees != ''\"> , `collecting_fees` = #{collectingFees} </when>" +
            " <when test = \"remark != null and remark != ''\"> , `remark` = #{remark} </when>" +
            " <when test = \"actualPrice != null and actualPrice != ''\"> , `actual_price` = #{actualPrice} </when>" +
            " <when test = \"totalPrice != null and totalPrice != ''\"> , `total_price` = #{totalPrice} </when>" +
            " <when test = \"findPrice != null and findPrice != ''\"> , `find_price` = #{findPrice} </when>" +
            " <when test = \"discountPrice != null and discountPrice != ''\"> , `discount_price` = #{discountPrice} </when>" +
            " <when test = \"distributionFee != null and distributionFee != ''\"> , `distribution_fee` = #{distributionFee} </when>" +
            " <when test = \"priceLogger != null and priceLogger != ''\"> , `price_logger` = #{priceLogger} </when>" +
            " <when test = \"taxRate != null and taxRate != ''\"> , `tax_rate` = #{taxRate} </when>" +
            " <when test = \"cancelLogger != null and cancelLogger.trim() != ''\"> , `cancel_logger` = #{cancelLogger} </when>" +
            " <when test = \"createTime != null\"> , `create_time` = #{createTime} </when>" +
            " <when test = \"paymentTime != null\"> , `payment_time` = #{paymentTime} </when>" +
            " <when test = \"confirmTime != null\"> , `confirm_time` = #{confirmTime} </when>" +
            " <when test = \"body != null and body != ''\"> , `body` = #{body} </when>" +
            " <when test = \"detail != null and detail != ''\"> , `detail` = #{detail} </when>" +
            " <when test = \"creditFee != null and creditFee != ''\"> , `credit_fee` = #{creditFee} </when>" +
            " <when test = \"thirdPartyDelivery != null and thirdPartyDelivery != ''\"> , `third_party_delivery` = #{thirdPartyDelivery} </when>" +
            " <when test = \"membershipPrice != null and membershipPrice != ''\"> , `membership_price` = #{membershipPrice}</when>" +
            " <when test = \"thirdPartyDeliveryStatus != null and thirdPartyDeliveryStatus != ''\"> , `third_party_delivery_status` = #{thirdPartyDeliveryStatus}</when>" +
            " <when test = \"isJoin != null and isJoin!= ''\"> , `is_join` = #{isJoin}</when>" +
            " where 1 = 1  " +
            "<when test=\"id != null and id != ''\">and id = #{id} </when> " +
            "<when test=\"orderSequenceNumber != null and orderSequenceNumber != ''\">and order_sequence_number = #{orderSequenceNumber} </when> " +
            "<when test=\"sequenceNumber != null and sequenceNumber != ''\">and sequence_number = #{sequenceNumber} </when> " +
            "</script> ")
    void changeOrderEntry(OrderEntry orderEntry);

    @Update("<script> update order_entry_pos  " +
            "set id = id" +
            " <when test = \"sequenceNumber != null and sequenceNumber != ''\">  ,  `sequence_number` = #{sequenceNumber} </when>" +
            " <when test = \"partnerId != null and partnerId != ''\"> , `partner_id` = #{partnerId} </when>" +
            " <when test = \"partnerUserId != null and partnerUserId != ''\"> , `partner_user_id` = #{partnerUserId} </when>" +
            " <when test = \"type != null and type != ''\"> , `type` = #{type} </when>" +
            " <when test = \"status != null and status != ''\"> , `status` = #{status} </when>" +
            " <when test = \"deliverStatus != null and deliverStatus != ''\"> , `deliver_status` = #{deliverStatus} </when>" +
            " <when test = \"refundStatus != null and refundStatus != ''\"> , `refund_status` = #{refundStatus} </when>" +
            " <when test = \"paymentType != null and paymentType != ''\"> , `payment_type` = #{paymentType} </when>" +
            " <when test = \"transType != null and transType != ''\"> , `trans_type` = #{transType} </when>" +
            " <when test = \"userSource != null and userSource != ''\"> , `user_source` = #{userSource} </when>" +
            " <when test = \"daySortNumber != null and daySortNumber != ''\"> , `day_sort_number` = #{daySortNumber} </when>" +
            " <when test = \"pushType != null and pushType != ''\"> , `push_type` = #{pushType} </when>" +
            " <when test = \"saleUserId != null and saleUserId != ''\"> , `sale_user_id` = #{saleUserId} </when>" +
            " <when test = \"shippingUserId != null and shippingUserId != ''\"> , `shipping_user_id` = #{shippingUserId} </when>" +
            " <when test = \"shippingNickName != null and shippingNickName != ''\"> , `shipping_nick_name` = #{shippingNickName} </when>" +
            " <when test = \"shippingProvince != null and shippingProvince != ''\"> , `shipping_province` = #{shippingProvince} </when>" +
            " <when test = \"shippingCity != null and shippingCity != ''\"> , `shipping_city` = #{shippingCity} </when>" +
            " <when test = \"shippingDistrict != null and shippingDistrict != ''\"> , `shipping_district` = #{shippingDistrict} </when>" +
            " <when test = \"shippingAddress != null and shippingAddress != ''\"> , `shipping_address` = #{shippingAddress} </when>" +
            " <when test = \"shippingLocation != null and shippingLocation != ''\"> , `shipping_location` = #{shippingLocation} </when>" +
            " <when test = \"shippingPhone != null and shippingPhone != ''\"> , `shipping_phone` = #{shippingPhone} </when>" +
            " <when test = \"receiptUserId != null and receiptUserId != ''\"> , `receipt_user_id` = #{receiptUserId} </when>" +
            " <when test = \"receiptNickName != null and receiptNickName != ''\"> , `receipt_nick_name` = #{receiptNickName} </when>" +
            " <when test = \"receiptProvince != null and receiptProvince != ''\"> , `receipt_province` = #{receiptProvince} </when>" +
            " <when test = \"receiptCity != null and receiptCity != ''\"> , `receipt_city` = #{receiptCity} </when>" +
            " <when test = \"receiptDistrict != null and receiptDistrict != ''\"> , `receipt_district` = #{receiptDistrict} </when>" +
            " <when test = \"receiptAddress != null and receiptAddress != ''\"> , `receipt_address` = #{receiptAddress} </when>" +
            " <when test = \"receiptPhone != null and receiptPhone != ''\"> , `receipt_phone` = #{receiptPhone} </when>" +
            " <when test = \"receiptLocation != null and receiptLocation != ''\"> , `receipt_location` = #{receiptLocation} </when>" +
            " <when test = \"courierUserId != null and courierUserId != ''\"> , `courier_user_id` = #{courierUserId} </when>" +
            " <when test = \"courierNickName != null and courierNickName != ''\"> , `courier_nick_name` = #{courierNickName} </when>" +
            " <when test = \"courierPhone != null and courierPhone != ''\"> , `courier_phone` = #{courierPhone} </when>" +
            " <when test = \"totalDistance != null and totalDistance != ''\"> , `total_distance` = #{totalDistance} </when>" +
            " <when test = \"currentLocation != null and currentLocation != ''\"> , `current_location` = #{currentLocation} </when>" +
            " <when test = \"collectingFees != null and collectingFees != ''\"> , `collecting_fees` = #{collectingFees} </when>" +
            " <when test = \"remark != null and remark != ''\"> , `remark` = #{remark} </when>" +
            " <when test = \"actualPrice != null and actualPrice != ''\"> , `actual_price` = #{actualPrice} </when>" +
            " <when test = \"totalPrice != null and totalPrice != ''\"> , `total_price` = #{totalPrice} </when>" +
            " <when test = \"findPrice != null and findPrice != ''\"> , `find_price` = #{findPrice} </when>" +
            " <when test = \"discountPrice != null and discountPrice != ''\"> , `discount_price` = #{discountPrice} </when>" +
            " <when test = \"distributionFee != null and distributionFee != ''\"> ,`distribution_fee` = #{distributionFee} </when>" +
            " <when test = \"priceLogger != null and priceLogger != ''\"> , `price_logger` = #{priceLogger} </when>" +
            " <when test = \"taxRate != null and taxRate != ''\"> , `tax_rate` = #{taxRate} </when>" +
            " <when test = \"cancelLogger != null and cancelLogger.trim() != ''\"> , `cancel_logger` = #{cancelLogger} </when>" +
            " <when test = \"createTime != null\"> , `create_time` = #{createTime} </when>" +
            " <when test = \"paymentTime != null\"> , `payment_time` = #{paymentTime} </when>" +
            " <when test = \"confirmTime != null\"> , `confirm_time` = #{confirmTime} </when>" +
            " <when test = \"body != null and body != ''\"> , `body` = #{body} </when>" +
            " <when test = \"detail != null and detail != ''\"> , `detail` = #{detail} </when>" +
            " <when test = \"creditFee != null and creditFee != ''\"> , `credit_fee` = #{creditFee} </when>" +
            " <when test = \"thirdPartyDelivery != null and thirdPartyDelivery != ''\"> , `third_party_delivery` = #{thirdPartyDelivery} </when>" +
            " <when test = \"membershipPrice != null and membershipPrice != ''\"> , `membership_price` = #{membershipPrice}</when>" +
            " <when test = \"thirdPartyDeliveryStatus != null and thirdPartyDeliveryStatus != ''\"> , `third_party_delivery_status` = #{thirdPartyDeliveryStatus}</when>" +
            " <when test = \"isJoin != null and isJoin!= ''\"> , `is_join` = #{isJoin}</when>" +
            " where 1 = 1  " +
            "<when test=\"id != null and id != ''\">and id = #{id} </when> " +
            "<when test=\"orderSequenceNumber != null and orderSequenceNumber != ''\">and order_sequence_number = #{orderSequenceNumber} </when> " +
            "<when test=\"sequenceNumber != null and sequenceNumber != ''\">and sequence_number = #{sequenceNumber} </when> " +
            "</script> ")
    void changeOrderEntryPos(OrderEntry orderEntry);

    /**
     * 查询供应商订单
     *
     * @param type (0/处理中 、 1/已完成)
     * @return 仓管id
     */
    @Select("<script>" +
            "select "+column+" from order_entry b where b.sale_user_id = #{userId} and b.type =  #{type} " +
            "<if test=\"status != null\">and ( b.status in <foreach collection=\"status\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach>)</if> "+
            " order by create_time desc   " +
            " <when test=\"pageIndex != null and pageSize != null\"> " +
            "  limit #{pageIndex}, #{pageSize}" +
            "</when></script>")
    @ResultMap("orderEntry")
    List<OrderEntry> getOrderEntrysByShippingUserIdAndType(@Param("userId") String userId, @Param("type") int type, @Param("status") List<String> status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize);

    @Update("update order_entry  set status = #{status} ,confirm_time = #{confirmTime} ,deliver_status = #{deliverStatus},receiving_time = #{receivingTime} where id = #{id}")
    void changeOrderEntryType(OrderEntry orderEntry);


    @Update("update order_entry  set status = #{status} ,deliver_status = #{deliverStatus}  where id = #{id}")
    void changeOrderEntryStatusAndDeliverStatus(OrderEntry orderEntry);

    /**
     * 商家端订单查询
     *
     * @param
     * @return java.util.List<com.tuoniaostore.datamodel.order.OrderEntry>
     * @author sqd
     * @date 2019/3/31
     */
    @Select("<script>" +
            "select " + column + " from order_entry b where b.partner_user_id = #{userId} and b.type =  #{type}" +
            " <when test=\"status != -1 and status != null\">  and b.status = #{status} </when>" +
            " <when test=\"pageIndex != null and pageSize != null\"> " +
            " ORDER BY b.create_time DESC " +
            "  limit #{pageIndex}, #{pageSize}" +
            " </when></script>")
    @ResultMap("orderEntry")
    List<OrderEntry> getOrderEntrysByShippingUserIdAndTypeBusiness(@Param("userId") String userId, @Param("type") int type, @Param("status") int status, @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize);

    /**
     * POS订单查询
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/16
     */
    @Select("<script>select  " + column + "  from order_entry_pos   where 1=1 " +
            " <when test=\"map.status != null and map.status != -1\">" +
            "  and status=#{map.status} " +
            " </when>" +
            " <when test=\"map.orderId != null and map.orderId != ''\">" +
            "  and id=#{map.orderId} " +
            " </when>" +
            " <when test=\"map.type != null and map.type != -1\">" +
            "   and type=#{map.type} " +
            " </when>" +
            " <when test=\"map.name != null and map.name.trim() != ''\">" +
            "  and receipt_nick_name like concat('%', #{map.name}, '%') " +
            " </when>" +
            " <when test=\"map.shopIdPos != null and map.shopIdPos.trim()\"> and sale_user_id = #{map.shopIdPos}</when>" +
            " <when test=\"map.shopIdPosOne != null and map.shopIdPosOne.trim() != ''\"> and sale_user_id = #{map.shopIdPosOne}</when>" +
            " <when test=\"map.orderSequenceNumber != null\"> and order_sequence_number = #{map.orderSequenceNumber}</when>" +
            " <when test=\"map.likeOrderSequenceNumberLike != null\"> and order_sequence_number LIKE CONCAT('%',#{map.likeOrderSequenceNumberLike},'%')</when>" +
            " <when test=\"map.paymentType != null and map.paymentType != -1\"> and payment_type = #{map.paymentType} </when>" +
            " <when test=\"map.refundStatus != null and map.refundStatus != -1\"> and refund_status = #{map.refundStatus} </when>" +
            " <when test=\"map.shippingNickNameOrPhone != null and map.shippingNickNameOrPhone != ''\"> and (shipping_nick_name = #{map.shippingNickNameOrPhone} or shipping_phone = #{map.shippingNickNameOrPhone}) </when>" +
            " <when test=\"map.receiptNickNameOrPhone != null and map.receiptNickNameOrPhone != ''\"> and (receipt_nick_name = #{map.receiptNickNameOrPhone} or receipt_phone = #{map.receiptNickNameOrPhone}) </when>" +
            " <when test=\"map.orderNumberOrPhone != null and map.orderNumberOrPhone != ''\"> and (order_sequence_number like concat('%', #{map.orderNumberOrPhone}, '%') or receipt_phone = #{map.orderNumberOrPhone}) </when>" +
            " <when test=\"map.startActualPrice != null and map.startActualPrice != -1\"> and actual_price <![CDATA[>=]]>  #{map.startActualPrice} </when>" +
            " <when test=\"map.endActualPrice != null and map.endActualPrice != -1\"> and actual_price <![CDATA[<=]]> #{map.endActualPrice} </when>" +
            " <when test=\"map.startTotalPrice != null and map.startTotalPrice != -1\"> and total_price <![CDATA[>=]]> #{map.startTotalPrice} </when>" +
            " <when test=\"map.endTotalPrice != null and map.endTotalPrice != -1\"> and total_price <![CDATA[<=]]> #{map.endTotalPrice} </when>" +
            " <when test=\"map.primarySequenceNumbers != null\">and (sequence_number in <foreach collection=\"map.primarySequenceNumbers\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> " +
            " <when test=\"map.inOrderNumber != null\">and (order_sequence_number in <foreach collection=\"map.inOrderNumber\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> " +
            " <when test=\"map.shopId != null \">and sale_user_id in <foreach collection=\"map.shopId\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </when> " +
            " <when test=\"map.isPrimaryOrder  != null\"> and (sequence_number is null or sequence_number = '') </when> " +
            " <when test=\"map.startTime  != null and map.startTime != ''\"> and payment_time <![CDATA[>=]]> #{map.startTime} </when> " +
            " <when test=\"map.endTime  != null and map.endTime != ''\"> and payment_time <![CDATA[<]]> #{map.endTime} </when> " +
            " <when test=\"map.paymentTime  != null and map.paymentTime != ''\"> and payment_time <![CDATA[>=]]> #{map.paymentTime} </when> " +
            " <when test=\"map.paymentTimeDay  != null and map.paymentTimeDay != ''\"> and date_format(payment_time,'%Y-%m-%d') <![CDATA[=]]> #{map.paymentTimeDay} </when> " +
            " <when test=\"map.createStartTime  != null and map.createStartTime != ''\"> and create_time <![CDATA[>=]]> #{map.createStartTime} </when> " +
            " <when test=\"map.createEndTime  != null and map.createEndTime != ''\"> and create_time <![CDATA[<]]> #{map.createEndTime} </when> " +
            " order by create_time desc   " +
            " <when test=\"map.pageIndex != null and map.pageSize != null\">" +
            "  limit #{map.pageIndex}, #{map.pageSize}" +
            "</when>" +
            "</script>")
    @ResultMap("orderEntry")
    List<OrderEntry> getOrderEntrysPos(@Param("map") Map<String, Object> map);

    //流水订单汇总数据
    @Results(id = "payLogNumbersVo", value = {
            @Result(property = "number", column = "number"), @Result(property = "paymentType", column = "paymentType"),
            @Result(property = "moneySum", column = "moneySum")})
    @Select("<script>  select count(0) as number ,t.payment_type as paymentType, sum(t.actual_price - (ifnull(t.find_price,0))) as moneySum from " +
            " order_entry_pos t  where " +
            " sale_user_id = #{map.shopIdPos} and t.payment_type != -1 and  t.payment_time is not null and t.payment_time != '' and date_format(t.payment_time,'%Y-%m-%d') = #{map.paymentTimeDay}" +
            " group by t.payment_type  order by t.create_time desc " +
            " </script>")
    List<PayLogNumbersVo> getOrderEntrysPosNumberDate(@Param("map") Map<String, Object> map);


    /**
     * POS订单查询时间格式化
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/16
     */
    @Results(id = "orderEntryPosFormatDate", value = {
            @Result(property = "id", column = "id"), @Result(property = "sequenceNumber", column = "sequence_number"),
            @Result(property = "orderSequenceNumber", column = "order_sequence_number"), @Result(property = "partnerId", column = "partner_id"), @Result(property = "partnerUserId", column = "partner_user_id"), @Result(property = "type", column = "type"), @Result(property = "status", column = "status"), @Result(property = "deliverStatus", column = "deliver_status"), @Result(property = "refundStatus", column = "refund_status"), @Result(property = "paymentType", column = "payment_type"), @Result(property = "transType", column = "trans_type"), @Result(property = "userSource", column = "user_source"), @Result(property = "daySortNumber", column = "day_sort_number"), @Result(property = "pushType", column = "push_type"), @Result(property = "saleUserId", column = "sale_user_id"),
            @Result(property = "shippingUserId", column = "shipping_user_id"),
            @Result(property = "shippingNickName", column = "shipping_nick_name"),
            @Result(property = "shippingProvince", column = "shipping_province"),
            @Result(property = "refundRemark ", column = "refund_remark "),
            @Result(property = "refundTime", column = "refund_time"),
            @Result(property = "refundType", column = "refund_type"),
            @Result(property = "shippingCity", column = "shipping_city"),
            @Result(property = "findPrice", column = "find_price"),
            @Result(property = "returnOperatorUserId", column = "return_operator_user_id"),
            @Result(property = "shippingDistrict", column = "shipping_district"), @Result(property = "shippingAddress", column = "shipping_address"), @Result(property = "shippingLocation", column = "shipping_location"), @Result(property = "shippingPhone", column = "shipping_phone"), @Result(property = "receiptUserId", column = "receipt_user_id"), @Result(property = "receiptNickName", column = "receipt_nick_name"), @Result(property = "receiptProvince", column = "receipt_province"), @Result(property = "receiptCity", column = "receipt_city"), @Result(property = "receiptDistrict", column = "receipt_district"), @Result(property = "receiptAddress", column = "receipt_address"), @Result(property = "receiptPhone", column = "receipt_phone"), @Result(property = "receiptLocation", column = "receipt_location"),
            @Result(property = "courierUserId", column = "courier_user_id"), @Result(property = "courierNickName", column = "courier_nick_name"), @Result(property = "courierPhone", column = "courier_phone"), @Result(property = "totalDistance", column = "total_distance"), @Result(property = "currentLocation", column = "current_location"), @Result(property = "collectingFees", column = "collecting_fees"), @Result(property = "remark", column = "remark"), @Result(property = "actualPrice", column = "actual_price"), @Result(property = "totalPrice", column = "total_price"), @Result(property = "discountPrice", column = "discount_price"), @Result(property = "distributionFee", column = "distribution_fee"), @Result(property = "priceLogger", column = "price_logger"), @Result(property = "taxRate", column = "tax_rate"), @Result(property = "cancelLogger", column = "cancel_logger"), @Result(property = "createTime", column = "create_time"), @Result(property = "paymentTime", column = "payment_time"), @Result(property = "confirmTime", column = "confirm_time"), @Result(property = "body", column = "body"), @Result(property = "detail", column = "detail"), @Result(property = "creditFee", column = "credit_fee"), @Result(property = "thirdPartyDelivery", column = "third_party_delivery"), @Result(property = "membershipPrice", column = "membership_price"), @Result(property = "thirdPartyDeliveryStatus", column = "third_party_delivery_status"), @Result(property = "isJoin", column = "is_join"), @Result(property = "isThirdParty", column = "is_third_party"),
            @Result(property = "reviewUserId", column = "review_user_id")})
    @Select("<script>select  " + column + "  from order_entry_pos   where 1=1 " +
            " <when test=\"map.status != null and map.status != -1\">" +
            "  and status=#{map.status} " +
            " </when>" +
            " <when test=\"map.orderId != null and map.orderId != ''\">" +
            "  and id=#{map.orderId} " +
            " </when>" +
            " <when test=\"map.type != null and map.type != -1\">" +
            "   and type=#{map.type} " +
            " </when>" +
            " <when test=\"map.name != null and map.name.trim() != ''\">" +
            "  and receipt_nick_name like concat('%', #{map.name}, '%') " +
            " </when>" +
            " <when test=\"map.shopIdPos != null and map.shopIdPos != ''\"> and sale_user_id = #{map.shopIdPos}</when>" +
            " <when test=\"map.orderSequenceNumber != null and map.orderSequenceNumber != ''\"> and order_sequence_number = #{map.orderSequenceNumber}</when>" +
            " <when test=\"map.likeOrderSequenceNumberLike != null and map.likeOrderSequenceNumberLike != ''\"> and order_sequence_number LIKE CONCAT('%',#{map.likeOrderSequenceNumberLike},'%')</when>" +
            " <when test=\"map.paymentType != null and map.paymentType != -1\"> and payment_type = #{map.paymentType} </when>" +
            " <when test=\"map.refundStatus != null and map.refundStatus != -1\"> and refund_status = #{map.refundStatus} </when>" +
            " <when test=\"map.shippingNickNameOrPhone != null and map.shippingNickNameOrPhone != ''\"> and (shipping_nick_name = #{map.shippingNickNameOrPhone} or shipping_phone = #{map.shippingNickNameOrPhone}) </when>" +
            " <when test=\"map.receiptNickNameOrPhone != null and map.receiptNickNameOrPhone != ''\"> and (receipt_nick_name = #{map.receiptNickNameOrPhone} or receipt_phone = #{map.receiptNickNameOrPhone}) </when>" +
            " <when test=\"map.orderNumberOrPhone != null and map.orderNumberOrPhone != ''\"> and (order_sequence_number like concat('%', #{map.orderNumberOrPhone}, '%') or receipt_phone = #{map.orderNumberOrPhone} or shipping_phone = #{map.orderNumberOrPhone}) </when>" +
            " <when test=\"map.startActualPrice != null and map.startActualPrice != -1\"> and actual_price <![CDATA[>=]]>  #{map.startActualPrice} </when>" +
            " <when test=\"map.endActualPrice != null and map.endActualPrice != -1\"> and actual_price <![CDATA[<=]]> #{map.endActualPrice} </when>" +
            " <when test=\"map.startTotalPrice != null and map.startTotalPrice != -1\"> and total_price <![CDATA[>=]]> #{map.startTotalPrice} </when>" +
            " <when test=\"map.endTotalPrice != null and map.endTotalPrice != -1\"> and total_price <![CDATA[<=]]> #{map.endTotalPrice} </when>" +
            " <when test=\"map.primarySequenceNumbers != null\">and (sequence_number in <foreach collection=\"map.primarySequenceNumbers\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> " +
            " <when test=\"map.inOrderNumber != null\">and (order_sequence_number in <foreach collection=\"map.inOrderNumber\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> " +
            " <when test=\"map.shopId != null\">and sale_user_id in <foreach collection=\"map.shopId\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </when> " +
            " <when test=\"map.isPrimaryOrder  != null\"> and (sequence_number is null or sequence_number = '') </when> " +
            " <when test=\"map.startTime  != null and map.startTime != ''\"> and payment_time <![CDATA[>=]]> #{map.startTime} </when> " +
            " <when test=\"map.endTime  != null and map.endTime != ''\"> and payment_time <![CDATA[<]]> #{map.endTime} </when> " +
            " <when test=\"map.createStartTime  != null and map.createStartTime != ''\"> and create_time <![CDATA[>=]]> #{map.createStartTime} </when> " +
            " <when test=\"map.createEndTime  != null and map.createEndTime != ''\"> and create_time <![CDATA[<]]> #{map.createEndTime} </when> " +
            " <when test=\"map.isRefund  = '1' \"> and refund_status = 0  </when> " +
            "  order by create_time desc   " +
            " <when test=\"map.pageIndex != null and map.pageSize != null\">" +
            "  limit #{map.pageIndex}, #{map.pageSize}" +
            "</when>" +
            "</script>")
    List<OrderEntryPosFormatDate> getOrderEntrysPosFormatDate(@Param("map") Map<String, Object> map);

    @Select("<script>select  count(*) from order_entry_pos   where 1=1 " +
            " <when test=\"map.status != null and map.status != -1\">" +
            "  and status=#{map.status} " +
            " </when>" +
            " <when test=\"map.orderId != null and map.orderId != ''\">" +
            "  and id=#{map.orderId} " +
            " </when>" +
            " <when test=\"map.type != null and map.type != -1\">" +
            "   and type=#{map.type} " +
            " </when>" +
            " <when test=\"map.name != null and map.name.trim() != ''\">" +
            "  and receipt_nick_name like concat('%', #{map.name}, '%') " +
            " </when>" +
            " <when test=\"map.shopIdPos != null\"> and sale_user_id = #{map.shopIdPos}</when>" +
            " <when test=\"map.orderSequenceNumber != null\"> and order_sequence_number = #{map.orderSequenceNumber}</when>" +
            " <when test=\"map.likeOrderSequenceNumberLike != null\"> and order_sequence_number LIKE CONCAT('%',#{map.likeOrderSequenceNumberLike},'%')</when>" +
            " <when test=\"map.paymentType != null and map.paymentType != -1\"> and payment_type = #{map.paymentType} </when>" +
            " <when test=\"map.refundStatus != null and map.refundStatus != -1\"> and refund_status = #{map.refundStatus} </when>" +
            " <when test=\"map.shippingNickNameOrPhone != null and map.shippingNickNameOrPhone != ''\"> and (shipping_nick_name = #{map.shippingNickNameOrPhone} or shipping_phone = #{map.shippingNickNameOrPhone}) </when>" +
            " <when test=\"map.receiptNickNameOrPhone != null and map.receiptNickNameOrPhone != ''\"> and (receipt_nick_name = #{map.receiptNickNameOrPhone} or receipt_phone = #{map.receiptNickNameOrPhone}) </when>" +
            " <when test=\"map.orderNumberOrPhone != null and map.orderNumberOrPhone != ''\"> and (order_sequence_number like concat('%', #{map.orderNumberOrPhone}, '%') or receipt_phone = #{map.orderNumberOrPhone}) </when>" +
            " <when test=\"map.startActualPrice != null and map.startActualPrice != -1\"> and actual_price <![CDATA[>=]]>  #{map.startActualPrice} </when>" +
            " <when test=\"map.endActualPrice != null and map.endActualPrice != -1\"> and actual_price <![CDATA[<=]]> #{map.endActualPrice} </when>" +
            " <when test=\"map.startTotalPrice != null and map.startTotalPrice != -1\"> and total_price <![CDATA[>=]]> #{map.startTotalPrice} </when>" +
            " <when test=\"map.endTotalPrice != null and map.endTotalPrice != -1\"> and total_price <![CDATA[<=]]> #{map.endTotalPrice} </when>" +
            " <when test=\"map.primarySequenceNumbers != null\">and (sequence_number in <foreach collection=\"map.primarySequenceNumbers\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> " +
            " <when test=\"map.inOrderNumber != null\">and (order_sequence_number in <foreach collection=\"map.inOrderNumber\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> " +
            " <when test=\"map.shopId != null\">and sale_user_id in <foreach collection=\"map.shopId\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </when> " +
            " <when test=\"map.isPrimaryOrder  != null\"> and (sequence_number is null or sequence_number = '') </when> " +
            " <when test=\"map.startTime  != null and map.startTime != ''\"> and payment_time <![CDATA[>=]]> #{map.startTime} </when> " +
            " <when test=\"map.endTime  != null and map.endTime != ''\"> and payment_time <![CDATA[<]]> #{map.endTime} </when> " +
            " <when test=\"map.paymentTime  != null and map.paymentTime != ''\"> and payment_time <![CDATA[>=]]> #{map.paymentTime} </when> " +
            " <when test=\"map.createStartTime  != null and map.createStartTime != ''\"> and create_time <![CDATA[>=]]> #{map.createStartTime} </when> " +
            " <when test=\"map.createEndTime  != null and map.createEndTime != ''\"> and create_time <![CDATA[<]]> #{map.createEndTime} </when> " +
            " <when test=\"map.isRefund  = '1' \"> and refund_status = 0  </when> " +
            " order by create_time desc </script>")
    int getOrderEntrysPosount(@Param("map") Map<String, Object> map);

    /**
     * POS订单查询总数
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/4/17
     */
    @Select("<script>select count(0) from order_entry_pos   where 1=1 " +
            " <when test=\"map.status != null and map.status != -1\">" +
            "  and status=#{map.status} " +
            " </when>" +
            " <when test=\"map.orderId != null and map.orderId != ''\">" +
            "  and id=#{map.orderId} " +
            " </when>" +
            " <when test=\"map.type != null and map.type != -1\">" +
            "   and type=#{map.type} " +
            " </when>" +
            " <when test=\"map.name != null and map.name.trim() != ''\">" +
            "  and receipt_nick_name like concat('%', #{map.name}, '%') " +
            " </when>" +
            " <when test=\"map.shopIdPos != null and map.shopIdPos.trim()\"> and sale_user_id = #{map.shopIdPos}</when>" +
            " <when test=\"map.shopIdPosOne != null and map.shopIdPosOne.trim() != ''\"> and sale_user_id = #{map.shopIdPosOne}</when>" +
            " <when test=\"map.orderSequenceNumber != null\"> and order_sequence_number = #{map.orderSequenceNumber}</when>" +
            " <when test=\"map.likeOrderSequenceNumberLike != null\"> and order_sequence_number LIKE CONCAT('%',#{map.likeOrderSequenceNumberLike},'%')</when>" +
            " <when test=\"map.paymentType != null and map.paymentType != -1\"> and payment_type = #{map.paymentType} </when>" +
            " <when test=\"map.refundStatus != null and map.refundStatus != -1\"> and refund_status = #{map.refundStatus} </when>" +
            " <when test=\"map.shippingNickNameOrPhone != null and map.shippingNickNameOrPhone != ''\"> and (shipping_nick_name = #{map.shippingNickNameOrPhone} or shipping_phone = #{map.shippingNickNameOrPhone}) </when>" +
            " <when test=\"map.receiptNickNameOrPhone != null and map.receiptNickNameOrPhone != ''\"> and (receipt_nick_name = #{map.receiptNickNameOrPhone} or receipt_phone = #{map.receiptNickNameOrPhone}) </when>" +
            " <when test=\"map.orderNumberOrPhone != null and map.orderNumberOrPhone != ''\"> and (order_sequence_number like concat('%', #{map.orderNumberOrPhone}, '%') or receipt_phone = #{map.orderNumberOrPhone}) </when>" +
            " <when test=\"map.startActualPrice != null and map.startActualPrice != -1\"> and actual_price <![CDATA[>=]]>  #{map.startActualPrice} </when>" +
            " <when test=\"map.endActualPrice != null and map.endActualPrice != -1\"> and actual_price <![CDATA[<=]]> #{map.endActualPrice} </when>" +
            " <when test=\"map.startTotalPrice != null and map.startTotalPrice != -1\"> and total_price <![CDATA[>=]]> #{map.startTotalPrice} </when>" +
            " <when test=\"map.endTotalPrice != null and map.endTotalPrice != -1\"> and total_price <![CDATA[<=]]> #{map.endTotalPrice} </when>" +
            " <when test=\"map.primarySequenceNumbers != null\">and (sequence_number in <foreach collection=\"map.primarySequenceNumbers\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> " +
            " <when test=\"map.inOrderNumber != null\">and (order_sequence_number in <foreach collection=\"map.inOrderNumber\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> )</when> " +
            " <when test=\"map.shopId != null \">and sale_user_id in <foreach collection=\"map.shopId\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </when> " +
            " <when test=\"map.isPrimaryOrder  != null\"> and (sequence_number is null or sequence_number = '') </when> " +
            " <when test=\"map.startTime  != null and map.startTime != ''\"> and payment_time <![CDATA[>=]]> #{map.startTime} </when> " +
            " <when test=\"map.endTime  != null and map.endTime != ''\"> and payment_time <![CDATA[<]]> #{map.endTime} </when> " +
            " <when test=\"map.paymentTime  != null and map.paymentTime != ''\"> and payment_time <![CDATA[>=]]> #{map.paymentTime} </when> " +
            " <when test=\"map.paymentTimeDay  != null and map.paymentTimeDay != ''\"> and date_format(payment_time,'%Y-%m-%d') <![CDATA[=]]> #{map.paymentTimeDay} </when> " +
            " <when test=\"map.createStartTime  != null and map.createStartTime != ''\"> and create_time <![CDATA[>=]]> #{map.createStartTime} </when> " +
            " <when test=\"map.createEndTime  != null and map.createEndTime != ''\"> and create_time <![CDATA[<]]> #{map.createEndTime} </when> " +
            "</script>")
    int getOrderEntryPosCount(@Param("map") Map<String, Object> map);

    /**
     * 查询订单排号
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/6/1
     */
    @Select("<script>select  " + column + "  from order_entry   where 1=1 " +
            " <when test=\"map.isPrimaryOrder  != null\"> and (sequence_number is null or sequence_number = '') </when> " +
            " <when test=\"map.isPrimaryOrder  == null\"> and sequence_number is not null and sequence_number != '' </when> " +
            " <when test=\"map.everyday  != null\"> and to_days(create_time) = to_days(now()) </when> " +
            "  order by create_time desc limit 1  " +
            "</script>")
    OrderEntry getOrderEntryDaySortDesc(@Param("map") Map<String, Object> map);

    @Select("<script>select  " + column + "  from order_entry_pos   where 1=1 " +
            " <when test=\"map.isPrimaryOrder  != null\"> and (sequence_number is null or sequence_number = '') </when> " +
            " <when test=\"map.everyday  != null\"> and to_days(create_time) = to_days(now()) </when> " +
            " <when test=\"map.orderNumber  != null\"> and order_sequence_number = #{map.orderNumber} </when> " +
            "  order by create_time desc limit 1  " +
            "</script>")
    OrderEntry getOrderEntryDaySortNumberDesc(@Param("map") Map<String, Object> map);


    /**
     * pos 支付类型分类查询
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/13
     */
    @Results(id = "orderPayTypeSumVo", value = {
            @Result(property = "number", column = "number"), @Result(property = "paymentType", column = "paymentType"),
            @Result(property = "moneySum", column = "moneySum")})
    @Select("<script> select count(0) as number, sum(actual_price - ifnull(find_price,0))  as moneySum,payment_type as paymentType  from order_entry_pos where 1 = 1" +
            " <when test=\"saleUserId  != null and saleUserId != ''\"> and sale_user_id = #{saleUserId} </when>" +
            " <when test=\"loginTime  != null\"> and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[>=]]> #{loginTime}  </when> " +
            " <when test=\"logoutTime  != null\">  and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[<=]]> #{logoutTime} </when>" +
            " <when test=\"startTime  != null and startTime != ''\"> and  DATE_FORMAT(create_time,'%Y-%m-%d') <![CDATA[>=]]> #{startTime} </when> " +
            " <when test=\"endTime  != null and endTime != '' \"> and DATE_FORMAT(create_time,'%Y-%m-%d') <![CDATA[<]]> #{endTime} </when>" +
            " and payment_time is not null  and payment_type != -1 and type = 16 group by  payment_type " +
            "</script> ")
    List<OrderPayTypeSumVo> getOrderEntryGruopByPayTtpe(@Param("loginTime") String loginTime, @Param("logoutTime") String logoutTime, @Param("startTime") String startTime, @Param("endTime") String endTime, @Param("saleUserId") String saleUserId);

    @Results(id = "OrderPayTypeSumAndRefundStatisticVo", value = {
            @Result(property = "number", column = "number"), @Result(property = "paymentType", column = "paymentType"),
            @Result(property = "refundNumber", column = "refundNumber"), @Result(property = "refundMoneySum", column = "refundMoneySum"),
            @Result(property = "moneySum", column = "moneySum")})
    @Select("<script>" +
            "  select t.*,r.refundMoneySum,r.refundNumber from " +
                " (select count(0) as number, sum(actual_price - ifnull(find_price,0))  as moneySum,payment_type as paymentType  from order_entry_pos where" +
                "  sale_user_id = #{map.shopId} and payment_time is not null  and payment_type != -1 and type = 16  " +
                " <when test=\"map.loginTime  != null\"> and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[>=]]> #{map.loginTime}  </when> " +
                " <when test=\"map.logoutTime  != null\">  and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[<=]]> #{map.logoutTime} </when> " +
                     "group by  payment_type"+
                 ") t" +
            " left join" +
                "  (select count(0) as refundNumber, sum(actual_price - ifnull(find_price,0))  as refundMoneySum,payment_type as paymentType  from order_entry_pos where" +
                "   sale_user_id = #{map.shopId} and payment_time is not null  and payment_type != -1 and type = 16 and status = 4096" +
                "  <when test=\"map.loginTime  != null\"> and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[>=]]> #{map.loginTime}  </when> " +
                "  <when test=\"map.logoutTime  != null\">  and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[<=]]> #{map.logoutTime} </when>" +
                "  group by  payment_type "+
                 ") r" +
            " on t.paymentType = r.paymentType " +
            "</script> ")
    List<OrderPayTypeSumAndRefundStatisticVo> getOrderEntryGruopByPayTtpeStatistic(@Param("map")Map map);

    /**
     * 查询退款订单
     *
     * @return
     * @author sqd
     * @date 2019/5/24
     */
    @ResultMap("orderEntry")
    @Select("<script> select  " + column + "  from order_entry_pos where " +
            " DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[>=]]> #{loginTime} " +
            " and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[<=]]> #{logoutTime}" +
            "<when test=\"saleUserId  != null\"> and sale_user_id = #{saleUserId} </when>" +
            " and  status = 4096 " +
            "</script> ")
    List<OrderEntry> getOrderEntryReturn(@Param("loginTime") String loginTime, @Param("logoutTime") String logoutTime, @Param("saleUserId") String saleUserId);


    /**
     * 查询昨天的商品销售数控
     *
     * @param shopId
     * @return java.util.List<com.tuoniaostore.datamodel.vo.order.GoodSaleMsgVO>
     * @author oy
     * @date 2019/5/21
     */
    @Select("<script> SELECT t2.good_id as goodId,t1.sale_user_id as shopId,t2.good_name as goodName, DATE_FORMAT( t1.create_time,'%Y-%m-%d') as createDate, " +
            " sum(t2.normal_quantity) as saleNum,sum(t2.total_price) as saleTotlePrice FROM order_entry_pos t1  " +
            " LEFT JOIN order_good_snapshot_pos t2 ON t1.id = t2.order_id and t1.sale_user_id = #{shopId} " +
            " WHERE t1. STATUS = 8 AND DATE_FORMAT( t1.create_time,'%Y-%m-%d') = DATE_FORMAT(CURDATE()-1,'%Y-%m-%d') and t2.id is not null GROUP BY t2.good_id   </script>")
    @Results(id = "GoodSaleMsgVO", value = {
            @Result(property = "goodId", column = "goodId"),
            @Result(property = "shopId", column = "shopId"),
            @Result(property = "goodName", column = "goodName"),
            @Result(property = "saleNum", column = "saleNum"),
            @Result(property = "createDate", column = "createDate"),
            @Result(property = "saleTotlePrice", column = "saleTotlePrice")})
    List<GoodSaleMsgVO> getOrderEntryForYesterDayAll(@Param("shopId") String shopId);


    //总计数据
    @Select("<script> select t.orderNumber, t.turnover , p.refundOrderNumber,p.refundPrice from " +
            " (select count(*) as orderNumber, sum(actual_price - ifnull(find_price,0))  as turnover ,1 as  k  from  order_entry_pos where 1=1  and sale_user_id = #{map.shopId} and payment_type != -1 and type = 16 and payment_time is not null" +
            " <when test=\"map.startTime  != null and map.startTime != '' \"> and DATE_FORMAT(create_time,'%Y-%m-%d') <![CDATA[>=]]> #{map.startTime} </when>" +
            " <when test=\"map.endTime  != null and map.endTime != ''\"> and create_time <![CDATA[<]]> #{map.endTime} </when> " +
            " <when test=\"map.loginTime  != null\"> and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[>=]]> #{map.loginTime}  </when> " +
            " <when test=\"map.logoutTime  != null\">  and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[<=]]> #{map.logoutTime} </when>" +
            ") t ," +
            " (select count(*) as refundOrderNumber, sum(actual_price - ifnull(find_price,0)) as refundPrice , 1 as k from  order_entry_pos   where status = 4096 and sale_user_id = #{map.shopId} and type = 16 and payment_time is not null" +
            " <when test=\"map.startTime  != null and map.startTime != '' \"> and DATE_FORMAT(create_time,'%Y-%m-%d') <![CDATA[>=]]> #{map.startTime} </when>" +
            " <when test=\"map.endTime  != null and map.endTime != ''\"> and create_time <![CDATA[<]]> #{map.endTime} </when> " +
            " <when test=\"map.loginTime  != null\"> and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[>=]]> #{map.loginTime}  </when> " +
            " <when test=\"map.logoutTime  != null\">  and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[<=]]> #{map.logoutTime} </when>" +
            ") p " +
            " where p.k = t.k </script>")
    OrderEntrPosSum getOrderEntrPosSumPrice(@Param("map") Map map);


    String where = "<when test=\"map.hebdom  != null \">   and date_sub(CURDATE,INTERVAL 7 DAY) <![CDATA[<=]]> DATE(p.create_time) </when> " +      //查询近一个星期数据
            "<when test=\"map.oneMonth  != null\">  and DATE_SUB(CURDATE(), INTERVAL 1 MONTH) <![CDATA[<=]]> DATE(p.create_time) </when> " +  //查询近一个月数据
            "<when test=\"map.saleUserId  != null\">  and sale_user_id = #{saleUserId} </when> " +
            "<when test=\"map.createStartTime  != null and map.createStartTime != ''\"> and p.create_time <![CDATA[>=]]> #{map.createStartTime} </when> " +
            "<when test=\"map.createEndTime  != null and map.createEndTime != ''\"> and p.create_time <![CDATA[<]]> #{map.createEndTime} </when> ";

    //门店业绩报表 列表数据   //退款这里只查询退款订单号，金额从快照表计算，后期做分商品退款，方便计算金额 现都为整单退款，退款金额可直接取订单实收金额
    @Select("<script> select o.orderId as outOrderId, l.dateTime , l.actualPrice as turnover,l.orderNumber,l.unitPrice, o.refundPrice,ifnull(o.outOrderNumber,0) as refundOrderNumber from  " +
            " (select group_concat(p.id) as orderId, DATE_FORMAT(p.create_time,'%Y-%m-%d') as  dateTime , sum(p.actual_price - ifnull(p.find_price,0)) as actualPrice,count(0) as orderNumber,(sum(p.actual_price - ifnull(p.find_price,0))/count(0)) as unitPrice from order_entry_pos  p " +
            " where  1 = 1  " +
            " <when test=\"map.startTime  != null and map.startTime != '' \"> and DATE_FORMAT(create_time,'%Y-%m-%d') <![CDATA[>=]]> #{map.startTime} </when>" +
            " <when test=\"map.endTime  != null and map.endTime != ''\"> and create_time <![CDATA[<]]> #{map.endTime} </when> " +
           /* " <when test=\"map.loginTime  != null\"> and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[>=]]> #{map.loginTime}  </when> " +
            " <when test=\"map.logoutTime  != null\">  and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[<=]]> #{map.logoutTime} </when>" +*/
            "  and sale_user_id = #{map.shopId} and type = 16 and payment_time is not null group by DATE_FORMAT(p.create_time,'%Y-%m-%d')) l " +  //左边查所有营业额 left join 右边差退款
            "   left join   " +
            " (select sum(p.actual_price - ifnull(p.find_price,0)) as refundPrice, group_concat(p.id) as orderId, count(0) as outOrderNumber ,DATE_FORMAT(p.create_time,'%Y-%m-%d') as dateTime from order_entry_pos  p  " +
            " where  1 = 1 " +
            " <when test=\"map.startTime  != null and map.startTime != '' \">  and DATE_FORMAT(create_time,'%Y-%m-%d') <![CDATA[>=]]> #{map.startTime} </when>" +
            " <when test=\"map.endTime  != null and map.endTime != ''\">  and create_time <![CDATA[<]]> #{map.endTime} </when> " +
            /*" <when test=\"map.loginTime  != null\"> and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[>=]]> #{map.loginTime}  </when> " +
            " <when test=\"map.logoutTime  != null\">  and DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') <![CDATA[<=]]> #{map.logoutTime} </when>" +*/
            " and sale_user_id = #{map.shopId} and status = 4096 and type = 16 and payment_time is not null group by DATE_FORMAT(p.create_time,'%Y-%m-%d')) o  " +
            " on l.dateTime = o.dateTime </script>")
    @Results(id = "OrderEntryPriceVO", value = {
            //@Result(property = "outOrderId", column = "outOrderId"),
            @Result(property = "dateTime", column = "dateTime"),
            @Result(property = "turnover", column = "turnover"),
            @Result(property = "refundPrice", column = "refundPrice"),
            @Result(property = "unitPrice", column = "unitPrice"),
            @Result(property = "refundOrderNumber", column = "refundOrderNumber")})
    List<OrderEntryPriceVO> getOrderEntrPosPrice(@Param("map") Map map);


    /*
     * 商品分类销售报表
     * @author sqd
     * @date 2019/5/22
     * @param
     * @return*/
    @Select("<script> select  sum(p.normal_quantity) as salesNumber, sum(p.total_price) as totalPrice ,ifnull(p.good_type_id,'其它') as goodType  " +
            " from order_good_snapshot_pos p where 1 =1  " +
            "<when test=\"map.goodTypeId  != null and map.goodTypeId != ''\"> and p.good_type_id = #{map.goodTypeId} </when> " +
            "<when test=\"map.orderId  != null\"> and p.order_id in <foreach collection=\"map.orderId\" item=\"item\" open=\"(\" close=\")\" separator=\",\">#{item}</foreach> </when> " +
            //"<when test=\"map.stratDateTime  != null and map.stratDateTime != ''\"> and p.create_time <![CDATA[>=]]> #{map.stratDateTime} </when> "+
            // "<when test=\"map.endDateTime  != null and map.endDateTime != ''\"> and p.create_time <![CDATA[<]]> #{map.endDateTime} </when> " +
            //  "<when test=\"map.goodType!=null and map.goodType!=''\"> and p.good_type_id = #{map.goodType} </when> " +
            " group by p.good_type_id  </script>")
    @Results(id = "orderEntryGoodTypeVO", value = {
            @Result(property = "salesNumber", column = "salesNumber"),
            @Result(property = "totalPrice", column = "totalPrice"),
            @Result(property = "goodTypeId", column = "goodType")})
    List<OrderEntryGoodTypeVO> getGoodTypeStatistics(@Param("map") Map map);

    @Update("<script> update order_entry_pos set  refund_status = #{refundStatus},return_operator_user_id = #{returnOperatorUserId}," +
            "refund_time = #{refundTime},refund_type = #{refundType} ,status = #{status} where id = #{id} </script>")
    void updateOrderEntrysPosRefundStatus(OrderEntry orderNumer);

    @Select("<script>" +
            " SELECT " + column + " FROM order_entry AS e WHERE " +
            " 1=1 " +
            " <if test=\"searchKeys.orderSequenceNumber != null\">AND e.order_sequence_number like concat('%', #{searchKeys.orderSequenceNumber}, '%') </if> " +
            " <if test=\"searchKeys.status != 0\">AND e.status = #{searchKeys.status}</if> " +
            " <if test=\"searchKeys.status == 0\">AND e.`status` > 8 </if> " +
            " AND e.partner_user_id = #{partnerUserId} " +
            " AND e.type = #{type} " +
            " ORDER BY e.create_time DESC " +
            " <when test=\"pageStartIndex != null and pageSize != null\">" +
            " limit #{pageStartIndex}, #{pageSize}" +
            " </when>" +
            "</script>")
    @ResultMap("orderEntry")
    List<OrderEntry> getOrderEntryListByPartnerUserIdAndType(
            @Param("partnerUserId") String partnerUserId,
            @Param("type") int type,
            @Param("searchKeys") HashMap<String, Object> searchKeys,
            @Param("pageStartIndex") int pageStartIndex,
            @Param("pageSize") int pageSize
    );

    @Select("<script>" +
            " SELECT count(1) FROM order_entry AS e WHERE " +
            " 1=1 " +
            " <if test=\"searchKeys.orderSequenceNumber != null\">AND e.order_sequence_number like concat('%', #{searchKeys.orderSequenceNumber}, '%') </if> " +
            " <if test=\"searchKeys.status != 0\">AND e.status = #{searchKeys.status}</if> " +
            " <if test=\"searchKeys.status == 0\">AND e.`status` > 8 </if> " +
            " AND e.partner_user_id = #{partnerUserId} " +
            " AND e.type = #{type} " +
            "</script>")
    Integer getOrderEntryListByPartnerUserIdAndTypeCount(
            @Param("partnerUserId") String partnerUserId,
            @Param("type") int type,
            @Param("searchKeys") HashMap<String, Object> searchKeys
    );

    @Select("<script> SELECT count(1)as orderNum,sum(actual_price) as priceTotal FROM `order_entry_pos` where  type = #{orderType} " +
            "<if test=\"todayTime != null  and todayTime != ''\"> and DATE_FORMAT(create_time,'%Y-%m-%d') = #{todayTime} </if>" +
            "   </script>")
    Map<String, Object> getOrderEntrysPostForTotal(@Param("orderType") int orderType, @Param("todayTime") String todayTime);

    @Select("<script> " +
            " select t1.date as date,COALESCE(t2.num,0) as num,COALESCE(t2.money,0) as money from calendar t1 LEFT JOIN ( " +
            "SELECT DATE_FORMAT(create_time, '%Y-%m-%d') date, count(1) AS num,sum(actual_price) as money  " +
            "FROM `order_entry_pos` " +
            "WHERE type = #{orderType} AND DATE_FORMAT(create_time, '%Y-%m-%d') BETWEEN #{startTime} AND #{endTime} " +
            "GROUP BY DATE_FORMAT(create_time, '%Y-%m-%d')" +
            ")t2 on t1.date = t2.date where   t1.date BETWEEN #{startTime} AND #{endTime} ORDER BY t1.date ASC  " +
            "</script>")
    @Results(id = "orderEntrysTotalVO", value = {
            @Result(property = "date", column = "date"), @Result(property = "num", column = "num"), @Result(property = "money", column = "money")})
    List<OrderEntrysTotalVO> getOrderEntrysByTypeCountForEveryDay(@Param("orderType") int orderType,@Param("startTime") String startTime,@Param("endTime") String endTime);

    @Select("<script> " +
            "select DATE_FORMAT(t1.date, '%x%v') as date,COALESCE(t2.num,0) as num,COALESCE(t2.money,0) as money from calendar t1 LEFT JOIN ( " +
            "SELECT DATE_FORMAT(create_time, '%x-%v') as date,count(1) AS num,sum(actual_price) as money  " +
            "FROM `order_entry_pos`  " +
            "WHERE type = #{orderType} AND DATE_FORMAT(create_time, '%Y-%m-%d') BETWEEN #{startTime} AND #{endTime} " +
            "GROUP BY DATE_FORMAT(create_time, '%x%v') " +
            ") t2 on DATE_FORMAT(t1.date, '%x-%v') = t2.date where t1.date BETWEEN #{startTime} AND #{endTime} GROUP BY date ORDER BY date ASC  " +
            "</script>")
    @ResultMap("orderEntrysTotalVO")
    List<OrderEntrysTotalVO> getOrderEntrysByTypeCountForEveryWeek(@Param("orderType") int orderType,@Param("startTime") String startTime,@Param("endTime") String endTime);

    @Select("<script> " +
            "select DATE_FORMAT(t1.date, '%x-%m') as date,COALESCE(t2.num,0) as num,COALESCE(t2.money,0) as money from calendar t1 LEFT JOIN ( " +
            "SELECT DATE_FORMAT(create_time, '%x-%m') as date, count(1) AS num ,sum(actual_price) as money   " +
            "FROM `order_entry_pos`  " +
            "WHERE type = #{orderType} AND DATE_FORMAT(create_time, '%x-%m')  BETWEEN #{startTime} AND #{endTime} " +
            "GROUP BY DATE_FORMAT(create_time, '%x-%m') " +
            ") t2 on DATE_FORMAT(t1.date, '%x-%m') = t2.date where DATE_FORMAT(t1.date, '%x-%m') BETWEEN #{startTime} AND #{endTime} GROUP BY date ORDER BY date ASC  " +
            "</script>")
    @ResultMap("orderEntrysTotalVO")
    List<OrderEntrysTotalVO> getOrderEntrysByTypeCountForEveryMonth(@Param("orderType") int orderType,@Param("startTime") String startTime,@Param("endTime") String endTime);

    @Select("<script> select "+column+"  from order_entry where id = #{id} union  select "+column+"  from order_entry_pos where id = #{id} limit 1</script> ")
    @ResultMap("orderEntry")
    OrderEntry getOrderEntryAndPosById(@Param("id") String orderId);

    @Select("<script> select "+column+"  from order_entry where order_sequence_number like concat('%',#{orderNum},'%') union  select "+column+"  from order_entry_pos where order_sequence_number like concat('%',#{orderNum},'%') </script> ")
    @ResultMap("orderEntry")
    List<OrderEntry> getOrderEntryByOrderNumberAll(@Param("orderNum") String orderNum);

    /*@Select("<script> " +
            "  " +
            " SELECT t1.*,t2.* FROM  " +
            "(select  " +
            " sum(a.actual_price) AS 'turnoverYestoday',  " +
            " count(a.order_sequence_number) AS 'orderNumYestoday',  " +
            " sum(CASE WHEN a.status = 4096 THEN a.order_sequence_number ELSE 0 END) AS 'refundNumYestoday',  " +
            " COALESCE(b.return_price,0) AS 'refundCostYestoday'  " +
            " from order_entry_pos a LEFT JOIN order_good_snapshot b ON a.order_sequence_number = b.order_id  " +
            " WHERE a.sale_user_id = #{ownId} " +
            " and DATE_FORMAT(a.payment_time,'%Y-%m-%d') = #{endTime} AND payment_type in (1,2,4) AND (a.sequence_number is null or a.sequence_number = '') ) t1, " +
            " (select  " +
            " sum(a.actual_price) AS 'turnover', count(a.order_sequence_number) AS 'orderNum',  " +
            " sum(CASE WHEN a.status = 4096 THEN 1 ELSE 0 END) AS 'refundNum', " +
            " sum(CASE WHEN a.payment_type = 1 THEN a.actual_price ELSE 0 END) AS 'weixinPay' ,  " +
            " sum(CASE WHEN a.payment_type = 2 THEN a.actual_price ELSE 0 END) AS 'zhifubaoPay' ,  " +
            " sum(CASE WHEN a.payment_type = 4 THEN a.actual_price ELSE 0 END) AS 'xianjinPay' ,  " +
            " sum(CASE WHEN a.payment_type = 1 THEN 1 ELSE 0 END) AS 'weixinOrderNum' ,  " +
            " sum(CASE WHEN a.payment_type = 2 THEN 1 ELSE 0 END) AS 'zhifubaoOrderNum' ,  " +
            " sum(CASE WHEN a.payment_type = 4 THEN 1 ELSE 0 END) AS 'xianjinOrderNum' , " +
            " sum(CASE WHEN a.status = 4096 AND a.payment_type = 1 THEN 1 ELSE 0 END) AS 'weixinRefundNum' ,  " +
            " sum(CASE WHEN a.status = 4096 AND a.payment_type = 2 THEN 1 ELSE 0 END) AS 'zhifubaoRefundNum' ,  " +
            " sum(CASE WHEN a.status = 4096 AND a.payment_type = 4 THEN 1 ELSE 0 END) AS 'xianjinRefundNum' , " +
            " sum(CASE WHEN a.status = 4096 AND a.payment_type = 1 THEN b.return_price ELSE 0 END) AS 'weixinRefundCost' ,  " +
            " sum(CASE WHEN a.status = 4096 AND a.payment_type = 2 THEN b.return_price ELSE 0 END) AS 'zhifubaoRefundCost' ,  " +
            " sum(CASE WHEN a.status = 4096 AND a.payment_type = 4 THEN b.return_price ELSE 0 END) AS 'xianjinRefundCost' ," +
            " COALESCE(b.return_price,0) AS 'refundCost' " +
            " from order_entry_pos a LEFT JOIN order_good_snapshot b ON a.order_sequence_number = b.order_id  " +
            " WHERE a.sale_user_id = #{ownId} " +
            " and DATE_FORMAT(a.payment_time,'%Y-%m-%d') = #{startTime} AND payment_type in (1,2,4) AND (a.sequence_number is null or a.sequence_number = '') ) t2 " +
            "</script>")*/
    @Select("<script> " +
            " SELECT t1.*,t2.*,t3.*,t4.* FROM  " +
            "(select  " +
            " sum(a.actual_price) AS 'turnoverYestoday',  " +
            " count(a.order_sequence_number) AS 'orderNumYestoday',  " +
            " sum(CASE WHEN a.status = 4096 THEN 1 ELSE 0 END) AS 'refundNumYestoday' " +
            " from order_entry_pos a WHERE a.sale_user_id = #{ownId} " +
            " AND DATE_FORMAT(a.payment_time,'%Y-%m-%d') = #{endTime} AND a.payment_type in (1,2,4) AND (a.sequence_number is null or a.sequence_number = '') ) t1 , " +
            "(select  " +
            " sum(a.actual_price) AS 'turnover',  " +
            " count(a.order_sequence_number) AS 'orderNum',  " +
            " sum(CASE WHEN a.status = 4096 THEN 1 ELSE 0 END) AS 'refundNum', " +
            " sum(CASE WHEN a.payment_type = 1 THEN a.actual_price ELSE 0 END) AS 'weixinPay' ,  " +
            " sum(CASE WHEN a.payment_type = 2 THEN a.actual_price ELSE 0 END) AS 'zhifubaoPay' ,  " +
            " sum(CASE WHEN a.payment_type = 4 THEN a.actual_price ELSE 0 END) AS 'xianjinPay' ,  " +
            " sum(CASE WHEN a.payment_type = 1 THEN 1 ELSE 0 END) AS 'weixinOrderNum' ,  " +
            " sum(CASE WHEN a.payment_type = 2 THEN 1 ELSE 0 END) AS 'zhifubaoOrderNum' ,  " +
            " sum(CASE WHEN a.payment_type = 4 THEN 1 ELSE 0 END) AS 'xianjinOrderNum' ,  " +
            " sum(CASE WHEN a.status = 4096 AND a.payment_type = 1 THEN 1 ELSE 0 END) AS 'weixinRefundNum' ,  " +
            " sum(CASE WHEN a.status = 4096 AND a.payment_type = 2 THEN 1 ELSE 0 END) AS 'zhifubaoRefundNum' ,  " +
            " sum(CASE WHEN a.status = 4096 AND a.payment_type = 4 THEN 1 ELSE 0 END) AS 'xianjinRefundNum'  " +
            " from order_entry_pos a WHERE a.sale_user_id = #{ownId} " +
            " AND DATE_FORMAT(a.payment_time,'%Y-%m-%d') = #{startTime} AND a.payment_type in (1,2,4)  AND (a.sequence_number is null or a.sequence_number = '') ) t2, " +
            "(select  " +
            " sum(b.return_price) AS 'refundCostYestoday'  " +
            " from order_entry_pos a LEFT JOIN order_good_snapshot_pos b ON a.id = b.order_id  WHERE a.sale_user_id = #{ownId} " +
            " AND DATE_FORMAT(a.payment_time,'%Y-%m-%d') = #{endTime} AND payment_type in (1,2,4) AND (a.sequence_number is null or a.sequence_number = '') ) t3 , " +
            "(select  " +
            " sum(CASE WHEN a.status = 4096 AND a.payment_type = 1 THEN b.return_price ELSE 0 END) AS 'weixinRefundCost' ,  " +
            " sum(CASE WHEN a.status = 4096 AND a.payment_type = 2 THEN b.return_price ELSE 0 END) AS 'zhifubaoRefundCost' ,  " +
            " sum(CASE WHEN a.status = 4096 AND a.payment_type = 4 THEN b.return_price ELSE 0 END) AS 'xianjinRefundCost' ,  " +
            " sum(b.return_price) AS 'refundCost' " +
            " from order_entry_pos a LEFT JOIN order_good_snapshot_pos b ON a.id = b.order_id WHERE a.sale_user_id = #{ownId} " +
            " AND DATE_FORMAT(a.payment_time,'%Y-%m-%d') = #{startTime} AND payment_type in (1,2,4)  AND (a.sequence_number is null or a.sequence_number = '') ) t4 " +
            "</script>")
    @Results(id = "businessSideAppletReport")
    List<BusinessSideAppletReport> getBusinessSideAppletReport(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("ownId") String ownId);

    /*@Select("<script> " +
            " select   " +
            " t1.date as date, " +
            "        t2.* " +
            " from calendar t1 LEFT JOIN ( " +
            " SELECT  " +
            " DATE_FORMAT(a.payment_time, '%Y-%m-%d') AS date, " +
            " sum(a.actual_price) AS 'turnoverTotal',     " +
            " count(a.order_sequence_number) AS 'orderNumTotal'     " +
            " from  " +
            " order_entry_pos a LEFT JOIN  " +
            " order_good_snapshot_pos b ON a.order_sequence_number = b.order_id    " +
            " WHERE payment_type in (1,2,4)  " +
            " AND sale_user_id = #{ownId} " +
            " AND a.sequence_number is null or a.sequence_number = '' AND " +
            " DATE_FORMAT(a.payment_time,'%Y-%m-%d') BETWEEN #{sixDayTime} AND #{startTime}  " +
            " GROUP BY DATE_FORMAT(a.payment_time, '%Y-%m-%d') ) t2  ON t1.date = t2.date  " +
            " where t1.date BETWEEN #{sixDayTime} AND #{startTime} ORDER BY t1.date ASC " +
            " </script>")*/
    @Select("<script> " +
            " SELECT s1.*,s2.* FROM   " +
            "  (select   " +
            "  sum(b.return_price) AS 'refundCostTotal' , DATE_FORMAT(a.payment_time,'%Y-%m-%d') sdate " +
            "  from order_entry_pos a LEFT JOIN order_good_snapshot_pos b ON a.id = b.order_id   " +
            "  WHERE a.sale_user_id = #{ownId}  " +
            "   AND DATE_FORMAT(a.payment_time,'%Y-%m-%d') = #{startTime} AND payment_type in (1,2,4) AND (a.sequence_number is null or a.sequence_number = '')) s1 " +
            " RIGHT JOIN   " +
            "             (select    " +
            "             t1.date as date,  " +
            "                    t2.*  " +
            "             from calendar t1 LEFT JOIN   " +
            "            ( SELECT   " +
            "             DATE_FORMAT(a.payment_time, '%Y-%m-%d') AS date1,  " +
            "             sum(a.actual_price) AS 'turnoverTotal',      " +
            "             count(a.order_sequence_number) AS 'orderNumTotal'      " +
            "            from   " +
            "            order_entry_pos a   WHERE a.payment_type in (1,2,4) AND a.sale_user_id = #{ownId}  " +
            "            AND a.sequence_number is null or a.sequence_number = ''  AND   " +
            "            DATE_FORMAT(a.payment_time,'%Y-%m-%d') BETWEEN #{sixDayTime} AND #{startTime}  " +
            "            GROUP BY DATE_FORMAT(a.payment_time, '%Y-%m-%d') ) t2   " +
            "            ON t1.date = t2.date1 where t1.date BETWEEN #{sixDayTime} AND #{startTime} ORDER BY t1.date ASC) s2  ON s1.sdate = s2.date " +
            " </script>")
    @Results(id = "businessSideAppletReportForEveryDay", value = {
            @Result(property = "date", column = "date"), @Result(property = "turnoverTotal", column = "turnoverTotal"), @Result(property = "orderNumTotal", column = "orderNumTotal"),@Result(property = "refundCostTotal", column = "refundCostTotal")})
    List<BusinessSideAppletReportForEveryDay> getBusinessSideAppletReportForeveryDay(@Param("startTime") String startTime,@Param("sixDayTime") String sixDayTime, @Param("ownId") String ownId);

    @Select("SELECT " + column + " FROM order_entry WHERE sequence_number = #{sequenceNumber}")
    @ResultMap("orderEntry")
    List<OrderEntry> getOrderEntryListBySequenceNumber(@Param("sequenceNumber")String sequenceNumber);

    @Select("SELECT * FROM order_entry  where " +
            "DATE_SUB(CURDATE(), INTERVAL 15 DAY) >= date(receiving_time) " +
            "and status = 128 " +
            "and (sequence_number is null or sequence_number ='')")
    @ResultMap("orderEntry")
    List<OrderEntry> getLazyReceivingGoods();
}
