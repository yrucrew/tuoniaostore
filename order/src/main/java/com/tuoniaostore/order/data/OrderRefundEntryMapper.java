package com.tuoniaostore.order.data;

import com.tuoniaostore.datamodel.order.OrderRefundEntry;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
@Mapper
public interface OrderRefundEntryMapper {

    String column = "" +
            " `id`,  " +
            " `sequence_number`,  " +
            " `old_sequence_number`, " +
            " `order_sequence_number`, " +
            " `order_id`,  " +
            " `partner_user_id`,  " +
            " `partner_user_phone`,  " +
            " `refund_price`,  " +
            " `reason`,  " +
            " `mark`,  " +
            " `img`,  " +
            " `status`,  " +
            " `create_time`  "
            ;

    @Insert("insert into order_refund_entry (" +
            " `id`,  " +
            " `old_sequence_number`,  " +
            " `sequence_number`,  " +
            " `order_sequence_number`,  " +
            " `order_id`,  " +
            " `type`,  " +
            " `partner_user_id`,  " +
            " `partner_user_phone`,  " +
            " `refund_price`,  " +
            " `reason`,  " +
            " `mark`,  " +
            " `img`,  " +
            " `status`  " +
            ")values(" +
            "#{id},  " +
            "#{oldSequenceNumber},  " +
            "#{sequenceNumber},  " +
            "#{orderSequenceNumber},  " +
            "#{orderId},  " +
            "#{type},  " +
            "#{partnerUserId},  " +
            "#{partnerUserPhone},  " +
            "#{refundPrice},  " +
            "#{reason},  " +
            "#{mark},  " +
            "#{img},  " +
            "#{status}  " +
            ")")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void addOrderRefundEntry(OrderRefundEntry orderRefundEntry);

    @Results(id = "orderRefundEntry", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "oldSequenceNumber", column = "old_sequence_number"),
            @Result(property = "sequenceNumber", column = "sequence_number"),
            @Result(property = "orderSequenceNumber", column = "order_sequence_number"),
            @Result(property = "orderId", column = "order_id"),
            @Result(property = "partnerUserId", column = "partner_user_id"),
            @Result(property = "partnerUserPhone", column = "partner_user_phone"),
            @Result(property = "refundPrice", column = "refund_price"),
            @Result(property = "reason", column = "reason"),
            @Result(property = "mark", column = "mark"),
            @Result(property = "img", column = "img"),
            @Result(property = "status", column = "status"),
            @Result(property = "createTime", column = "create_time")
    })
    @Select(" <script>" +
            "SELECT " + column + " FROM order_refund_entry WHERE `id` = #{id} LIMIT 1 "+
            " </script>")
    OrderRefundEntry getOrderRefundEntry(@Param("id") String id);

    @Select(" <script>" +
            "SELECT " + column + " FROM order_refund_entry WHERE 1=1" +
            " and sequence_number = '' " +
            " <when test=\"parameters.type != null\">" +
            "  and type = #{parameters.type} " +
            " </when>" +
            " <when test=\"parameters.status != null and parameters.status.trim() != ''\">" +
            "  and status = #{parameters.status} " +
            " </when>" +
            " <when test=\"parameters.phone != null and parameters.phone.trim() != ''\">" +
            "  and partner_user_phone like concat('%', #{parameters.phone}, '%') " +
            " </when>" +
            " <when test=\"parameters.begTime != null\">" +
            "  and create_time <![CDATA[>=]]> #{parameters.begTime} " +
            " </when>" +
            " <when test=\"parameters.endTime != null\">" +
            "  and create_time <![CDATA[<=]]> #{parameters.endTime} " +
            " </when>" +
            " ORDER BY create_time DESC " +
            " <when test=\"pageStartIndex != null and pageSize != null\"> " +
            "  limit #{pageStartIndex}, #{pageSize} " +
            " </when> " +
            " </script>")
    @ResultMap("orderRefundEntry")
    List<OrderRefundEntry> getOrderRefundEntryList(@Param("parameters")HashMap<String, Object> parameters, @Param("pageStartIndex")int pageStartIndex, @Param("pageSize")int pageSize);

    @Select(" <script>" +
            " SELECT " + column + " FROM order_refund_entry WHERE `sequence_number` = #{sequenceNumber} "+
            " </script>")
    @ResultMap("orderRefundEntry")
    List<OrderRefundEntry> getOrderRefundEntryListBySequenceNumber(@Param("sequenceNumber") String sequenceNumber);

    @Update(" update order_refund_entry  " +
            " set " +
            " `status` = #{status} " +
            " where id = #{id}")
    void changeOrderRefundEntryStatus(OrderRefundEntry orderRefundEntry);

    @Select(" <script>" +
            "SELECT count(1) FROM order_refund_entry WHERE 1=1" +
            " and sequence_number = '' " +
            " <when test=\"parameters.type != null\">" +
            "  and type = #{parameters.type} " +
            " </when>" +
            " <when test=\"parameters.status != null and parameters.status.trim() != ''\">" +
            "  and status = #{parameters.status} " +
            " </when>" +
            " <when test=\"parameters.phone != null and parameters.phone.trim() != ''\">" +
            "  and partner_user_phone like concat('%', #{parameters.phone}, '%') " +
            " </when>" +
            " <when test=\"parameters.begTime != null\">" +
            "  and create_time <![CDATA[>=]]> #{parameters.begTime} " +
            " </when>" +
            " <when test=\"parameters.endTime != null\">" +
            "  and create_time <![CDATA[<=]]> #{parameters.endTime} " +
            " </when>" +
            " ORDER BY create_time DESC " +
            " </script>")
    int getOrderRefundEntryListCount(@Param("parameters")HashMap<String, Object> parameters);
}
