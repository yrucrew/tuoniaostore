package com.tuoniaostore.order.schedual;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.order.OrderStatus;
import com.tuoniaostore.commons.constant.payment.CurrencyTypeEnum;
import com.tuoniaostore.commons.constant.payment.PaymentEstimateRevenueStatusEnum;
import com.tuoniaostore.commons.constant.payment.RelationTransTypeEnum;
import com.tuoniaostore.commons.support.payment.*;
import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.order.OrderStateLogger;
import com.tuoniaostore.datamodel.payment.*;
import com.tuoniaostore.order.cache.OrderDataAccessManager;
import com.tuoniaostore.order.service.impl.OrderEntryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/*
 * 自动确认收货
 * @author sqd
 * @date 2019/6/21
 * @param
 * @return
 */
@Component
public class DeliveryService  extends BasicWebService {

    /**
     *  每天凌晨3点执行一次，
     * @author sqd
     * @date 2019/6/21
     * @param
     * @return void
     */
//     @Scheduled(cron = "0 0 3 * * ?")
    @Scheduled(cron = "*/30 * * * * ?")
    @Transactional
    public void receiveCargo(){
        System.out.println("+++++++++++++++++进入定时器+++++++++++++++++");
        //查询15天未确认的订单
        List<OrderEntry> lazyReceivingGoods = dataAccessManager.getLazyReceivingGoods();
        if(lazyReceivingGoods == null || lazyReceivingGoods.size() <= 0 ){
            return;
        }
        for (OrderEntry lazyReceivingGood : lazyReceivingGoods) {
            // 订单id
            OrderEntry orderEntry = dataAccessManager.getOrderEntry(lazyReceivingGood.getId());
            HashMap<String, Object> map = new HashMap<>();
            map.put("sequenceNumbers", orderEntry.getOrderSequenceNumber());
            //查询主单下子单
            List<OrderEntry> priOrderEntrys = dataAccessManager.getOrderEntrys(map);
            if (orderEntry == null) {
                return;

            }
            if (priOrderEntrys == null) {
                return;
            }
            orderEntry.setStatus(OrderStatus.ORDER_TYPE_CONFIRMRECEIPT.getKey());
            orderEntry.setConfirmTime(new Date());
            dataAccessManager.changeOrderEntryType(orderEntry);

            List<String> orderId = priOrderEntrys.stream().map(OrderEntry::getId).collect(Collectors.toList());
            orderId.add(orderEntry.getId());
            List<OrderGoodSnapshot> orderGoodSnapshotByOrderInIds = dataAccessManager.getOrderGoodSnapshotByOrderInIds(orderId);

            if (orderGoodSnapshotByOrderInIds != null && orderGoodSnapshotByOrderInIds.size() > 0) {
                for (OrderGoodSnapshot orderGoodSnapshots : orderGoodSnapshotByOrderInIds) {
                    if(orderGoodSnapshots.getReturnQuantity() != null){
                        orderGoodSnapshots.setArriveQuantity(orderGoodSnapshots.getNormalQuantity()-orderGoodSnapshots.getReturnQuantity());//送达的数量
                        orderGoodSnapshots.setReceiptQuantity(orderGoodSnapshots.getNormalQuantity()-orderGoodSnapshots.getReturnQuantity());//被确认收货的数量
                    }else{
                        orderGoodSnapshots.setArriveQuantity(orderGoodSnapshots.getNormalQuantity());//送达的数量
                        orderGoodSnapshots.setReceiptQuantity(orderGoodSnapshots.getNormalQuantity());//被确认收货的数量
                    }

                    orderGoodSnapshots.setDistributionQuantity(0);
                    dataAccessManager.changeOrderGoodSnapshot(orderGoodSnapshots);
                }
            }


            OrderStateLogger orderStateLogger = new OrderStateLogger();
            orderStateLogger.setBeforeStatus(OrderStatus.ORDER_TYPE_DELIVERY.getKey());//修改前状态
            orderStateLogger.setStatus(orderEntry.getStatus());//修改后状态
            orderStateLogger.setOrderId(orderEntry.getId());
            orderStateLogger.setShippingUserId("0");
            orderStateLogger.setOrderType(orderEntry.getType());
            orderStateLogger.setOperatorTime(new Date());
            orderStateLogger.setOperatorName("自动确认收货");
            //插入日志
            dataAccessManager.addOrderStateLogger(orderStateLogger);

            try {
                //添加账期
                for (OrderEntry entry : priOrderEntrys) {
                    //计算订单金额
                    long orderPrice = getOrderPrice(entry);

                    //查询用账期设置
                    PaymentEstimateRevenue paymentEstimateRevenue = new PaymentEstimateRevenue();
                    //支付成功添加
                    //获取用户当前额度
                    PaymentSettlementSet paymentSettlementSet = PaymentSettlementSetService.getPaymentSettlementSetByUserId(entry.getSaleUserId());

                    if(paymentSettlementSet != null){
                        //***添加账期
                        paymentEstimateRevenue.setStatus(PaymentEstimateRevenueStatusEnum.INITIATE_WITHDRAWAL.getKey());
                        long money = 0;
                        if(paymentSettlementSet.getType().equals(1)){ //扣点
                            money = orderPrice * (10000 - paymentSettlementSet.getCablePoint()) / 10000;//向下取整

                        }else if(paymentSettlementSet.getType().equals(2)){//反点
                            money = orderPrice * (10000 + paymentSettlementSet.getCablePoint()) / 10000;//向下取整
                        } else{
                            money = orderPrice;
                        }
                        paymentEstimateRevenue.setActualIncome(money);// 实际收入
                    }else{
                        //***直接到账
                        paymentEstimateRevenue.setStatus(PaymentEstimateRevenueStatusEnum.COMPLETED_WITHDRAWAL.getKey());
                        paymentEstimateRevenue.setActualIncome(entry.getActualPrice());// 实际收入

                        PaymentCurrencyAccount paymentCurrencyAccountById = PaymentCurrencyAccountService.getPaymentCurrencyAccountById(entry.getSaleUserId());
                        if(paymentCurrencyAccountById != null){
                            PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger = new PaymentCurrencyOffsetLogger();
                            paymentCurrencyOffsetLogger.setUserId(entry.getSaleUserId());
                            paymentCurrencyOffsetLogger.setCurrencyType(CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());
                            paymentCurrencyOffsetLogger.setBeforeAmount(paymentCurrencyAccountById.getCurrentAmount());
                            paymentCurrencyOffsetLogger.setOffsetAmount(paymentCurrencyAccountById.getCurrentAmount()+entry.getActualPrice());//变化后的额度
                            paymentCurrencyOffsetLogger.setTransTitle("商家采购支付");
                            paymentCurrencyOffsetLogger.setTransType(entry.getPaymentType()+"");
                            paymentCurrencyOffsetLogger.setRelationTransType(RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey());
                            paymentCurrencyOffsetLogger.setRelationTransSequence(entry.getOrderSequenceNumber());

                            paymentCurrencyAccountById.setCurrentAmount(paymentCurrencyAccountById.getCurrentAmount()+entry.getActualPrice());
                            //修改余额
                            PaymentCurrencyAccountService.changePaymentCurrencyAccount(paymentCurrencyAccountById);
                            //添加余额变动记录
                            PaymentCurrencyOffsetLoggerRemoteService.addPaymentCurrencyOffsetLogger(paymentCurrencyOffsetLogger);
                        }

                        //添加到账记录
                        PaymentEstimateRevenueLogger paymentEstimateRevenueLogger = new PaymentEstimateRevenueLogger();
                        paymentEstimateRevenueLogger.setUserId(entry.getSaleUserId());
                        paymentEstimateRevenueLogger.setActualIncome(entry.getActualPrice());
                        paymentEstimateRevenueLogger.setOrderAmount(entry.getActualPrice());
                        paymentEstimateRevenueLogger.setOrderId(entry.getId());
                        paymentEstimateRevenueLogger.setUserId(entry.getSaleUserId());
                        paymentEstimateRevenueLogger.setCreateTime(new Date());
                        PaymentEstimateRevenueLoggerService.addPaymentEstimateRevenueLogger(paymentEstimateRevenueLogger);
                    }

                    paymentEstimateRevenue.setOrderAmount(entry.getActualPrice()); //订单金额
                    paymentEstimateRevenue.setOrderId(entry.getId());
                    paymentEstimateRevenue.setShopsName(entry.getShippingNickName());//商家名称
                    paymentEstimateRevenue.setRequestTime(new Date());

                    //添加账期
                    PaymentEstimateRevenueService.addRemotePaymentEstimateRevenue(paymentEstimateRevenue);

                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }



    }

    @Autowired
    private OrderDataAccessManager dataAccessManager;

    /**
     * 订单金额计算
     * @author sqd
     * @date 2019/6/19
     * @param
     * @return
     */
    public  long  getOrderPrice(OrderEntry entry) {

        List<OrderGoodSnapshot> orderGoodSnapshotByOrderId = dataAccessManager.getOrderGoodSnapshotByOrderId(entry.getId());
        if(orderGoodSnapshotByOrderId == null || orderGoodSnapshotByOrderId.size() <= 0){
            return 0;
        }

        long orderPrice = 0;
        for (OrderGoodSnapshot goodSnapshot : orderGoodSnapshotByOrderId) {
            if(goodSnapshot.getReturnQuantity() != null){
                //存在退款
                orderPrice+=(goodSnapshot.getNormalQuantity()-goodSnapshot.getReturnQuantity()) * goodSnapshot.getNormalPrice();
            }else{
                orderPrice+=goodSnapshot.getNormalQuantity() * goodSnapshot.getNormalPrice();
            }
        }
        return orderPrice;
    }
}
