package com.tuoniaostore.order.VO;



import java.io.Serializable;
import java.util.Date;

public class OrderGoodSnapshotPosGroupVO implements Serializable {

    private  long refundPrice; //退款金额
    private  Date dateTime;    //日期

    public long getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(long refundPrice) {
        this.refundPrice = refundPrice;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
