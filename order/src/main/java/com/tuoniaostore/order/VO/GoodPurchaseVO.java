package com.tuoniaostore.order.VO;

import java.io.Serializable;

/**
 * @author oy
 * @description
 * @date 2019/4/25
 */
public class GoodPurchaseVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String goodId;
    private String goodNum;

    public GoodPurchaseVO() {
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getGoodNum() {
        return goodNum;
    }

    public void setGoodNum(String goodNum) {
        this.goodNum = goodNum;
    }
}
