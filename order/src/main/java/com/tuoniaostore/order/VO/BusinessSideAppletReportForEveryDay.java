package com.tuoniaostore.order.VO;

import java.io.Serializable;

/**
 * @author xh
 * @version 1.0
 * @describe  图表显示的数据
 * @date 2019/6/21 11:36
 */
public class BusinessSideAppletReportForEveryDay implements Serializable {
    private static final long serialVersionUID = 1L;

    private String date;
    private long turnoverTotal;     //营业额
    private int orderNumTotal;      //订单数
    private long refundCostTotal;     //退款金额

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getTurnoverTotal() {
        return turnoverTotal;
    }

    public void setTurnoverTotal(long turnoverTotal) {
        this.turnoverTotal = turnoverTotal;
    }

    public int getOrderNumTotal() {
        return orderNumTotal;
    }

    public void setOrderNumTotal(int orderNumTotal) {
        this.orderNumTotal = orderNumTotal;
    }

    public long getRefundCostTotal() {
        return refundCostTotal;
    }

    public void setRefundCostTotal(long refundCostTotal) {
        this.refundCostTotal = refundCostTotal;
    }
}
