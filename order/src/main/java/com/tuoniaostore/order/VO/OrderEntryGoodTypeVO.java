package com.tuoniaostore.order.VO;

import java.io.Serializable;
import java.math.BigDecimal;

public class OrderEntryGoodTypeVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String goodTypeId;
    private String goodTypeTitle;
    private Integer salesNumber;
    private BigDecimal totalPrice;


    public String getGoodTypeId() {
        return goodTypeId;
    }

    public void setGoodTypeId(String goodTypeId) {
        this.goodTypeId = goodTypeId;
    }

    public String getGoodTypeTitle() {
        return goodTypeTitle;
    }

    public void setGoodTypeTitle(String goodTypeTitle) {
        this.goodTypeTitle = goodTypeTitle;
    }

    public Integer getSalesNumber() {
        return salesNumber;
    }

    public void setSalesNumber(Integer salesNumber) {
        this.salesNumber = salesNumber;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
