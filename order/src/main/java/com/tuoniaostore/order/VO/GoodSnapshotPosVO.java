package com.tuoniaostore.order.VO;



import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;

public class GoodSnapshotPosVO implements Serializable {
    //快照id
    private  String id ;
    //商品id
    private  String goodId ;
    //退货数量数量
    private Integer refundQuantity;
    //单价
    private long normalPrice;
    private long retunrnMoney;

    public long getRetunrnMoney() {
        return retunrnMoney;
    }

    public void setRetunrnMoney(long retunrnMoney) {
        this.retunrnMoney = retunrnMoney;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getRefundQuantity() {
        return refundQuantity;
    }

    public void setRefundQuantity(Integer refundQuantity) {
        this.refundQuantity = refundQuantity;
    }

    public long getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(long normalPrice) {
        this.normalPrice = normalPrice;
    }
}
