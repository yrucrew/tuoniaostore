package com.tuoniaostore.order.VO;

import com.tuoniaostore.datamodel.order.OrderEntry;

import java.util.Date;
import java.util.List;

public class OrderRefundEntryDetailVo {
    // 退款单id
    private String id;
    // 父单号
    private String sequenceNumber;
    // 自身单号
    private String orderSequenceNumber;
    // 采购单id
    private String orderId;
    // 退单用户id
    private String partnerUserId;
    // 退单用户手机号
    private String partnerUserPhone;
    // 退款金额
    private Long refundPrice;
    // 退款原因
    private String reason;
    // 退款备注
    private String mark;
    // 退款图片
    private List<String> imgList;
    // 退款状态
    private int status;
    // 申请时间
    private Date createTime;
    // 原采购订单
    private OrderEntry orderEntry;
    // 仓库名称
    private String shopName;

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public OrderEntry getOrderEntry() {
        return orderEntry;
    }

    public void setOrderEntry(OrderEntry orderEntry) {
        this.orderEntry = orderEntry;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getOrderSequenceNumber() {
        return orderSequenceNumber;
    }

    public void setOrderSequenceNumber(String orderSequenceNumber) {
        this.orderSequenceNumber = orderSequenceNumber;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPartnerUserId() {
        return partnerUserId;
    }

    public void setPartnerUserId(String partnerUserId) {
        this.partnerUserId = partnerUserId;
    }

    public String getPartnerUserPhone() {
        return partnerUserPhone;
    }

    public void setPartnerUserPhone(String partnerUserPhone) {
        this.partnerUserPhone = partnerUserPhone;
    }

    public Long getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(Long refundPrice) {
        this.refundPrice = refundPrice;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public List<String> getImgList() {
        return imgList;
    }

    public void setImgList(List<String> imgList) {
        this.imgList = imgList;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
