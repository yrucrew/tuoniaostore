package com.tuoniaostore.order.VO;


import com.tuoniaostore.datamodel.DefineRandom;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OrderEntryVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id= DefineRandom.getUUID();
    //当天排号 相对于发货方
    private Integer daySortNumber;
    //父订单号
    private String sequenceNumber;
    //订单的序列号(用于展示)
    private String orderSequenceNumber;
    //订单的生成时间
    private Date createTime;
    //订单的生成时间字符串
    private String createTimeStr;
    //实收费用
    private Long actualPrice;
    //订单费用
    private Long totalPrice;
    //优惠的费用
    private Long discountPrice;
    //配送费用
    private Long distributionFee;
    //店铺名称
    private String shopName;
    //店铺电话
    private String shopPhone;
    //订单类型  //订单类型 1-供应链销售订单、2-供应链调拨订单、8-供应链采购订单、16-超市POS机订单（包括超市扫码订单）、32、外卖配送订单 64增值服务订单、
    private Integer type;
    //订单状态 不要占用0  1-创建订单（未确认）、2-已取消、8-已支付、16-已接单、32-发货中、64-配送中、128-已送达、256-确认收货/已完成、260-***确认收货/已完成、512-已收款、1024-分批配送、2048-拒绝订单。
    private Integer status;
    //发货状态：0-未发货、32-发货中、64-配送中、128-已送达
    private Integer deliverStatus;
    //收货人昵称
    private String receiptNickName;
    //客户姓名
    private String receiptNickPhone;
    //收货人电话
    private String receiptPhone;
    //收货具体地址(尽量不要包含省市区)
    private String receiptAddress;
    //发货用户ID
    private String shippingUserId;
    //发货用户昵称
    private String shippingNickName;
    //发货省份
    private String shippingProvince;
    //发货城市
    private String shippingCity;
    //发货区域
    private String shippingDistrict;
    //发货详细地址(尽量不要包含省市区)
    private String shippingAddress;
    //latitude,longitude
    private String shippingLocation;
    //发货人联系号码
    private String shippingPhone;
    //实际销售的用户ID（销售仓）
    private String saleUserId;
    //商品快照数量
    private Integer orderGoodSnapshotCount;

    public String getCreateTimeStr() {
        return createTimeStr;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    public String getShippingUserId() {
        return shippingUserId;
    }

    public void setShippingUserId(String shippingUserId) {
        this.shippingUserId = shippingUserId;
    }

    public String getShippingNickName() {
        return shippingNickName;
    }

    public void setShippingNickName(String shippingNickName) {
        this.shippingNickName = shippingNickName;
    }

    public String getShippingProvince() {
        return shippingProvince;
    }

    public void setShippingProvince(String shippingProvince) {
        this.shippingProvince = shippingProvince;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingDistrict() {
        return shippingDistrict;
    }

    public void setShippingDistrict(String shippingDistrict) {
        this.shippingDistrict = shippingDistrict;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingLocation() {
        return shippingLocation;
    }

    public void setShippingLocation(String shippingLocation) {
        this.shippingLocation = shippingLocation;
    }

    public String getShippingPhone() {
        return shippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        this.shippingPhone = shippingPhone;
    }

    public String getSaleUserId() {
        return saleUserId;
    }

    public void setSaleUserId(String saleUserId) {
        this.saleUserId = saleUserId;
    }

    public Integer getOrderGoodSnapshotCount() {
        return orderGoodSnapshotCount;
    }

    public void setOrderGoodSnapshotCount(Integer orderGoodSnapshotCount) {
        this.orderGoodSnapshotCount = orderGoodSnapshotCount;
    }

    public Long getDistributionFee() {
        return distributionFee;
    }

    public void setDistributionFee(Long distributionFee) {
        this.distributionFee = distributionFee;
    }

    public Integer getDeliverStatus() {
        return deliverStatus;
    }

    public void setDeliverStatus(Integer deliverStatus) {
        this.deliverStatus = deliverStatus;
    }

    public String getReceiptAddress() {
        return receiptAddress;
    }

    public void setReceiptAddress(String receiptAddress) {
        this.receiptAddress = receiptAddress;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getReceiptNickName() {
        return receiptNickName;
    }

    public String getReceiptNickPhone() {
        return receiptNickPhone;
    }

    public void setReceiptNickPhone(String receiptNickPhone) {
        this.receiptNickPhone = receiptNickPhone;
    }

    public void setReceiptNickName(String receiptNickName) {
        this.receiptNickName = receiptNickName;
    }

    public Long getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Long discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getOrderSequenceNumber() {
        return orderSequenceNumber;
    }

    public void setOrderSequenceNumber(String orderSequenceNumber) {
        this.orderSequenceNumber = orderSequenceNumber;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    private List<OrderGoodSnapshot> orderGoodSnapshots;

    public String getReceiptPhone() {
        return receiptPhone;
    }

    public void setReceiptPhone(String receiptPhone) {
        this.receiptPhone = receiptPhone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getDaySortNumber() {
        return daySortNumber;
    }

    public void setDaySortNumber(Integer daySortNumber) {
        this.daySortNumber = daySortNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Long actualPrice) {
        this.actualPrice = actualPrice;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<OrderGoodSnapshot> getOrderGoodSnapshots() {
        return orderGoodSnapshots;
    }

    public void setOrderGoodSnapshots(List<OrderGoodSnapshot> orderGoodSnapshots) {
        this.orderGoodSnapshots = orderGoodSnapshots;
    }
}
