package com.tuoniaostore.order.VO;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;

public class OrderGoodSnapshotVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id= DefineRandom.getUUID();
    //对应的订单ID
    private String orderId;
    //相关的商品ID
    private String goodId;
    //商品模版ID
    private String goodTemplateId;
    //当时的商品名称
    private String goodName;
    //当时单位名称
    private String goodUnitName;
    //正常价格购买的数量
    private Integer normalQuantity;
    //配送中的数量
    private Integer distributionQuantity;
    //被确认收货的数量
    private Integer receiptQuantity;
    //未收货的数量
    private Integer unReceiptQuantity;
    //退货中数量
    private Integer refundQuantity;
    //退货的数量
    private Integer returnQuantity;
    //正常价格
    private Long normalPrice;
    //应收费用
    private Long totalPrice;
    //商品条码
    private String goodBarcode;

    public Integer getRefundQuantity() {
        return refundQuantity;
    }

    public void setRefundQuantity(Integer refundQuantity) {
        this.refundQuantity = refundQuantity;
    }

    public String getGoodBarcode() {
        return goodBarcode;
    }

    public void setGoodBarcode(String goodBarcode) {
        this.goodBarcode = goodBarcode;
    }

    public Integer getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(Integer returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getGoodTemplateId() {
        return goodTemplateId;
    }

    public void setGoodTemplateId(String goodTemplateId) {
        this.goodTemplateId = goodTemplateId;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodUnitName() {
        return goodUnitName;
    }

    public void setGoodUnitName(String goodUnitName) {
        this.goodUnitName = goodUnitName;
    }

    public Integer getNormalQuantity() {
        return normalQuantity;
    }

    public void setNormalQuantity(Integer normalQuantity) {
        this.normalQuantity = normalQuantity;
    }

    public Integer getDistributionQuantity() {
        return distributionQuantity;
    }

    public void setDistributionQuantity(Integer distributionQuantity) {
        this.distributionQuantity = distributionQuantity;
    }

    public Integer getReceiptQuantity() {
        return receiptQuantity;
    }

    public void setReceiptQuantity(Integer receiptQuantity) {
        this.receiptQuantity = receiptQuantity;
    }

    public Integer getUnReceiptQuantity() {
        return unReceiptQuantity;
    }

    public void setUnReceiptQuantity(Integer unReceiptQuantity) {
        this.unReceiptQuantity = unReceiptQuantity;
    }

    public Long getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(Long normalPrice) {
        this.normalPrice = normalPrice;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }
}
