package com.tuoniaostore.order.VO;

import java.io.Serializable;

/**
 * @author xh
 * @version 1.0
 * @date 2019/6/19 16:44 商家端小程序报表
 */
public class BusinessSideAppletReport implements Serializable {

    private static final long serialVersionUID = 1L;

    private long turnover;          //营业额
    private int orderNum;           //订单数
    private long weixinPay;         //微信支付
    private long zhifubaoPay;       //支付宝支付
    private long xianjinPay;        //现金支付
    private int refundNum;          //退款笔数
    private long refundCost;        //退款费用
    private int weixinOrderNum;     //微信支付的订单数
    private int zhifubaoOrderNum;   //支付宝支付的订单数
    private int xianjinOrderNum;    //现金支付的订单数
    private int weixinRefundNum;    //微信退款笔数
    private int zhifubaoRefundNum;   //支付宝退款笔数
    private int xianjinRefundNum;    //现金退款笔数
    private long weixinRefundCost;    //微信退款费用
    private long zhifubaoRefundCost;    //支付宝退款费用
    private long xianjinRefundCost;    //现金退款费用


    //前一天
    private long turnoverYestoday;          //营业额
    private int orderNumYestoday;           //订单数
    private long weixinPayYestoday;         //微信支付
    private long zhifubaoPayYestoday;       //支付宝支付
    private long xianjinPayYestoday;        //现金支付
    private int refundNumYestoday;          //退款笔数
    private long refundCostYestoday;        //退款费用
    private int weixinOrderNumYestoday;     //微信支付的订单数
    private int zhifubaoOrderNumYestoday;   //支付宝支付的订单数
    private int xianjinOrderNumYestoday;    //现金支付的订单数
    private int weixinRefundNumYestoday;    //微信退款笔数
    private int zhifubaoRefundNumYestoday;   //支付宝退款笔数
    private int xianjinRefundNumYestoday;    //现金退款笔数
    private long weixinRefundCostYestoday;    //微信退款费用
    private long zhifubaoRefundCostYestoday;    //支付宝退款费用
    private long xianjinRefundCostYestoday;    //现金退款费用

    //前几天
    private long turnoverTwoDayBefore;          //营业额
    private int orderNumTwoDayBefore;           //订单数
    private long turnoverThreeDayBefore;          //营业额
    private int orderNumThreeDayBefore;           //订单数
    private long turnoverFourDayBefore;          //营业额
    private int orderNumFourDayBefore;           //订单数
    private long turnoverFiveDayBefore;          //营业额
    private int orderNumFiveDayBefore;           //订单数
    private long turnoverSixDayBefore;          //营业额
    private int orderNumSixDayBefore;           //订单数

    public long getTurnoverTwoDayBefore() {
        return turnoverTwoDayBefore;
    }

    public void setTurnoverTwoDayBefore(long turnoverTwoDayBefore) {
        this.turnoverTwoDayBefore = turnoverTwoDayBefore;
    }

    public int getOrderNumTwoDayBefore() {
        return orderNumTwoDayBefore;
    }

    public void setOrderNumTwoDayBefore(int orderNumTwoDayBefore) {
        this.orderNumTwoDayBefore = orderNumTwoDayBefore;
    }

    public long getTurnoverThreeDayBefore() {
        return turnoverThreeDayBefore;
    }

    public void setTurnoverThreeDayBefore(long turnoverThreeDayBefore) {
        this.turnoverThreeDayBefore = turnoverThreeDayBefore;
    }

    public int getOrderNumThreeDayBefore() {
        return orderNumThreeDayBefore;
    }

    public void setOrderNumThreeDayBefore(int orderNumThreeDayBefore) {
        this.orderNumThreeDayBefore = orderNumThreeDayBefore;
    }

    public long getTurnoverFourDayBefore() {
        return turnoverFourDayBefore;
    }

    public void setTurnoverFourDayBefore(long turnoverFourDayBefore) {
        this.turnoverFourDayBefore = turnoverFourDayBefore;
    }

    public int getOrderNumFourDayBefore() {
        return orderNumFourDayBefore;
    }

    public void setOrderNumFourDayBefore(int orderNumFourDayBefore) {
        this.orderNumFourDayBefore = orderNumFourDayBefore;
    }

    public long getTurnoverFiveDayBefore() {
        return turnoverFiveDayBefore;
    }

    public void setTurnoverFiveDayBefore(long turnoverFiveDayBefore) {
        this.turnoverFiveDayBefore = turnoverFiveDayBefore;
    }

    public int getOrderNumFiveDayBefore() {
        return orderNumFiveDayBefore;
    }

    public void setOrderNumFiveDayBefore(int orderNumFiveDayBefore) {
        this.orderNumFiveDayBefore = orderNumFiveDayBefore;
    }

    public long getTurnoverSixDayBefore() {
        return turnoverSixDayBefore;
    }

    public void setTurnoverSixDayBefore(long turnoverSixDayBefore) {
        this.turnoverSixDayBefore = turnoverSixDayBefore;
    }

    public int getOrderNumSixDayBefore() {
        return orderNumSixDayBefore;
    }

    public void setOrderNumSixDayBefore(int orderNumSixDayBefore) {
        this.orderNumSixDayBefore = orderNumSixDayBefore;
    }

    public long getTurnover() {
        return turnover;
    }

    public void setTurnover(long turnover) {
        this.turnover = turnover;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public long getWeixinPay() {
        return weixinPay;
    }

    public void setWeixinPay(long weixinPay) {
        this.weixinPay = weixinPay;
    }

    public long getZhifubaoPay() {
        return zhifubaoPay;
    }

    public void setZhifubaoPay(long zhifubaoPay) {
        this.zhifubaoPay = zhifubaoPay;
    }

    public long getXianjinPay() {
        return xianjinPay;
    }

    public void setXianjinPay(long xianjinPay) {
        this.xianjinPay = xianjinPay;
    }

    public int getRefundNum() {
        return refundNum;
    }

    public void setRefundNum(int refundNum) {
        this.refundNum = refundNum;
    }

    public long getRefundCost() {
        return refundCost;
    }

    public void setRefundCost(long refundCost) {
        this.refundCost = refundCost;
    }

    public long getTurnoverYestoday() {
        return turnoverYestoday;
    }

    public void setTurnoverYestoday(long turnoverYestoday) {
        this.turnoverYestoday = turnoverYestoday;
    }

    public int getOrderNumYestoday() {
        return orderNumYestoday;
    }

    public void setOrderNumYestoday(int orderNumYestoday) {
        this.orderNumYestoday = orderNumYestoday;
    }

    public long getWeixinPayYestoday() {
        return weixinPayYestoday;
    }

    public void setWeixinPayYestoday(long weixinPayYestoday) {
        this.weixinPayYestoday = weixinPayYestoday;
    }

    public long getZhifubaoPayYestoday() {
        return zhifubaoPayYestoday;
    }

    public void setZhifubaoPayYestoday(long zhifubaoPayYestoday) {
        this.zhifubaoPayYestoday = zhifubaoPayYestoday;
    }

    public long getXianjinPayYestoday() {
        return xianjinPayYestoday;
    }

    public void setXianjinPayYestoday(long xianjinPayYestoday) {
        this.xianjinPayYestoday = xianjinPayYestoday;
    }

    public int getRefundNumYestoday() {
        return refundNumYestoday;
    }

    public void setRefundNumYestoday(int refundNumYestoday) {
        this.refundNumYestoday = refundNumYestoday;
    }

    public int getWeixinOrderNum() {
        return weixinOrderNum;
    }

    public void setWeixinOrderNum(int weixinOrderNum) {
        this.weixinOrderNum = weixinOrderNum;
    }

    public int getZhifubaoOrderNum() {
        return zhifubaoOrderNum;
    }

    public void setZhifubaoOrderNum(int zhifubaoOrderNum) {
        this.zhifubaoOrderNum = zhifubaoOrderNum;
    }

    public int getXianjinOrderNum() {
        return xianjinOrderNum;
    }

    public void setXianjinOrderNum(int xianjinOrderNum) {
        this.xianjinOrderNum = xianjinOrderNum;
    }

    public int getWeixinRefundNum() {
        return weixinRefundNum;
    }

    public void setWeixinRefundNum(int weixinRefundNum) {
        this.weixinRefundNum = weixinRefundNum;
    }

    public int getZhifubaoRefundNum() {
        return zhifubaoRefundNum;
    }

    public void setZhifubaoRefundNum(int zhifubaoRefundNum) {
        this.zhifubaoRefundNum = zhifubaoRefundNum;
    }

    public int getXianjinRefundNum() {
        return xianjinRefundNum;
    }

    public void setXianjinRefundNum(int xianjinRefundNum) {
        this.xianjinRefundNum = xianjinRefundNum;
    }

    public long getWeixinRefundCost() {
        return weixinRefundCost;
    }

    public void setWeixinRefundCost(long weixinRefundCost) {
        this.weixinRefundCost = weixinRefundCost;
    }

    public long getZhifubaoRefundCost() {
        return zhifubaoRefundCost;
    }

    public void setZhifubaoRefundCost(long zhifubaoRefundCost) {
        this.zhifubaoRefundCost = zhifubaoRefundCost;
    }

    public long getXianjinRefundCost() {
        return xianjinRefundCost;
    }

    public void setXianjinRefundCost(long xianjinRefundCost) {
        this.xianjinRefundCost = xianjinRefundCost;
    }

    public int getWeixinOrderNumYestoday() {
        return weixinOrderNumYestoday;
    }

    public void setWeixinOrderNumYestoday(int weixinOrderNumYestoday) {
        this.weixinOrderNumYestoday = weixinOrderNumYestoday;
    }

    public int getZhifubaoOrderNumYestoday() {
        return zhifubaoOrderNumYestoday;
    }

    public void setZhifubaoOrderNumYestoday(int zhifubaoOrderNumYestoday) {
        this.zhifubaoOrderNumYestoday = zhifubaoOrderNumYestoday;
    }

    public int getXianjinOrderNumYestoday() {
        return xianjinOrderNumYestoday;
    }

    public void setXianjinOrderNumYestoday(int xianjinOrderNumYestoday) {
        this.xianjinOrderNumYestoday = xianjinOrderNumYestoday;
    }

    public int getWeixinRefundNumYestoday() {
        return weixinRefundNumYestoday;
    }

    public void setWeixinRefundNumYestoday(int weixinRefundNumYestoday) {
        this.weixinRefundNumYestoday = weixinRefundNumYestoday;
    }

    public int getZhifubaoRefundNumYestoday() {
        return zhifubaoRefundNumYestoday;
    }

    public void setZhifubaoRefundNumYestoday(int zhifubaoRefundNumYestoday) {
        this.zhifubaoRefundNumYestoday = zhifubaoRefundNumYestoday;
    }

    public int getXianjinRefundNumYestoday() {
        return xianjinRefundNumYestoday;
    }

    public void setXianjinRefundNumYestoday(int xianjinRefundNumYestoday) {
        this.xianjinRefundNumYestoday = xianjinRefundNumYestoday;
    }

    public long getWeixinRefundCostYestoday() {
        return weixinRefundCostYestoday;
    }

    public void setWeixinRefundCostYestoday(long weixinRefundCostYestoday) {
        this.weixinRefundCostYestoday = weixinRefundCostYestoday;
    }

    public long getZhifubaoRefundCostYestoday() {
        return zhifubaoRefundCostYestoday;
    }

    public void setZhifubaoRefundCostYestoday(long zhifubaoRefundCostYestoday) {
        this.zhifubaoRefundCostYestoday = zhifubaoRefundCostYestoday;
    }

    public long getXianjinRefundCostYestoday() {
        return xianjinRefundCostYestoday;
    }

    public void setXianjinRefundCostYestoday(long xianjinRefundCostYestoday) {
        this.xianjinRefundCostYestoday = xianjinRefundCostYestoday;
    }

    public long getRefundCostYestoday() {
        return refundCostYestoday;
    }

    public void setRefundCostYestoday(long refundCostYestoday) {
        this.refundCostYestoday = refundCostYestoday;
    }
}
