package com.tuoniaostore.order.VO;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author oy
 * @description 订单统计结果封装
 * @date 2019/6/6
 */
public class OrderEntrysTotalVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String date;
    private BigDecimal num;
    private BigDecimal money;

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }
}
