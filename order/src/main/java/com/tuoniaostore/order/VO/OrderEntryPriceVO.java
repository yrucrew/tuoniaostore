package com.tuoniaostore.order.VO;



import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

public class OrderEntryPriceVO implements Serializable {
        private long turnover;//营业额
        private Integer refundOrderNumber;//退款单数;
        private Integer orderNumber;//订单数;
        private String outOrderId;//订单数;
        private long unitPrice;//单价
        private long refundPrice;
        private Date dateTime;

    public String getOutOrderId() {
        return outOrderId;
    }

    public void setOutOrderId(String outOrderId) {
        this.outOrderId = outOrderId;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public long getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(long unitPrice) {
        this.unitPrice = unitPrice;
    }

    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public long getTurnover() {
        return turnover;
    }

    public void setTurnover(long turnover) {
        this.turnover = turnover;
    }

    public Integer getRefundOrderNumber() {
        return refundOrderNumber;
    }

    public void setRefundOrderNumber(Integer refundOrderNumber) {
        this.refundOrderNumber = refundOrderNumber;
    }

    public long getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(long refundPrice) {
        this.refundPrice = refundPrice;
    }
}
