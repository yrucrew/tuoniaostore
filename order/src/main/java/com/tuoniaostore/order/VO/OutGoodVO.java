package com.tuoniaostore.order.VO;

import java.io.Serializable;

public class OutGoodVO implements Serializable {
    private String goodName;

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }
}
