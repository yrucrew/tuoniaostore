package com.tuoniaostore.order.VO;



import java.io.Serializable;
import java.util.Date;

public class OrderEntrPosSum implements Serializable {
        private long turnover;//营业额
        private long refundPrice;//退款金额
        private Integer orderNumber;//订单数;
        private Integer refundOrderNumber;//订单数;

    public long getTurnover() {
        return turnover;
    }

    public void setTurnover(long turnover) {
        this.turnover = turnover;
    }

    public long getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(long refundPrice) {
        this.refundPrice = refundPrice;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getRefundOrderNumber() {
        return refundOrderNumber;
    }

    public void setRefundOrderNumber(Integer refundOrderNumber) {
        this.refundOrderNumber = refundOrderNumber;
    }
}
