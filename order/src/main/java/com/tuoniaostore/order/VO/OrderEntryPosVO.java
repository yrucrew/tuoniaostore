package com.tuoniaostore.order.VO;


import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;

public class OrderEntryPosVO implements Serializable {

    private String id= DefineRandom.getUUID();
    //订单的序列号(用于展示)
    private String orderSequenceNumber;
    //当天排号 相对于发货方
    private Integer daySortNumber;
    //订单的生成时间
    private Date createTime;
    //实收费用
    private Long actualPrice;
    //优惠的费用
    private Long discountPrice;
    //店铺名称
    private String shopName;
    //店铺电话
    private String shopPhone;
    //订单金额
    private Long totalPrice;

    private Integer paymentType;


    public String getOrderSequenceNumber() {
        return orderSequenceNumber;
    }

    public void setOrderSequenceNumber(String orderSequenceNumber) {
        this.orderSequenceNumber = orderSequenceNumber;
    }

    public Integer getDaySortNumber() {
        return daySortNumber;
    }

    public void setDaySortNumber(Integer daySortNumber) {
        this.daySortNumber = daySortNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Long actualPrice) {
        this.actualPrice = actualPrice;
    }

    public Long getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Long discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }
}
