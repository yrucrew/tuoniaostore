package com.tuoniaostore.order.VO;



import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;

public class GoodSnapshotVO implements Serializable {
    private String id= DefineRandom.getUUID();
    //应收费用
    private Long totalPrice;
    //当时单位名称
    private String goodUnitName;
    //当时的商品名称
    private String goodName;
    //正常价格购买的数量
    private Integer normalQuantity;
    private Integer returnQuantity;
    //单价
    private long normalPrice;
    //条码
    private String goodBarcode;

    public Integer getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(Integer returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public long getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(long normalPrice) {
        this.normalPrice = normalPrice;
    }

    public String getGoodBarcode() {
        return goodBarcode;
    }

    public void setGoodBarcode(String goodBarcode) {
        this.goodBarcode = goodBarcode;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getGoodUnitName() {
        return goodUnitName;
    }

    public void setGoodUnitName(String goodUnitName) {
        this.goodUnitName = goodUnitName;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public Integer getNormalQuantity() {
        return normalQuantity;
    }

    public void setNormalQuantity(Integer normalQuantity) {
        this.normalQuantity = normalQuantity;
    }
}
