package com.tuoniaostore.order;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.exception.Nearby123shopIllegalArgumentException;
import com.tuoniaostore.commons.exception.Nearby123shopRuntimeException;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by liyunbiao on 2019/3/5.
 */

@Service
@Aspect
public class OrderAop extends BasicWebService {

    private static final Logger logger = LoggerFactory.getLogger(OrderAop.class);

    @Around(value = "execution(* com.tuoniaostore.order.controller.*.*(..))")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        //拦截的方法参数
        Object[] args = point.getArgs();
        //拦截的实体类
        Object target = point.getTarget();
        //拦截的方法名称
        String methodName = point.getSignature().getName();
        try {
            /*if(methodName.equals("getOrderEntryForYesterDay")){
                HttpServletRequest request = getHttpServletRequest();
                String token = request.getParameter("token");
                //判断token 是否过期
                boolean b = false;
                if(token != null && !token.equals("")){
                    b = TokenUtils.validateInterFace(token);
                }
                if(!b){
                    return fail(1001, "非法接口！");
                }
            }else{
                int status = SysUserRmoteService.signatureSecurity(getHttpServletRequest());
                if (1001 == status) {
                    return fail(1001, "登录已过期请重新登录");
                }
            }*/
            return point.proceed(args);
        } catch (Nearby123shopIllegalArgumentException ex) {
            logger.error("订单中心切面拦截异常(非法参数)，错误信息：" + ex.getMessage());
            return fail(ex.getMessage());
        } catch (Nearby123shopRuntimeException ex) {
            logger.error("订单中心切面拦截异常(运行时异常)，错误信息：" + ex.getMessage());
            return fail(ex.getCode(), ex.getMessage());
        } catch (Throwable cause) {
            logger.error("订单中心切面拦截异常(未知错误)", cause);
            return fail("系统错误，请稍后重试！");
        }
    }
}