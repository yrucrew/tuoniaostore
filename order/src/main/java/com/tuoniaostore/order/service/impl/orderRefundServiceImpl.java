package com.tuoniaostore.order.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.order.OrderRefundEnum;
import com.tuoniaostore.commons.constant.order.OrderRefundStatusEnum;
import com.tuoniaostore.commons.constant.order.OrderTypeEnum;
import com.tuoniaostore.commons.support.payment.PaymentCurrencyAccountService;
import com.tuoniaostore.commons.support.supplychain.SupplychainShopPropertiesRemoteService;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.datamodel.DefineRandom;
import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.order.OrderRefundEntry;
import com.tuoniaostore.datamodel.order.OrderRefundGoodSnapshot;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyAccount;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.order.VO.OrderRefundEntryDetailVo;
import com.tuoniaostore.order.VO.OrderRefundSnapshotVo;
import com.tuoniaostore.order.cache.OrderDataAccessManager;
import com.tuoniaostore.order.service.OrderRefundService;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service("orderRefundService")
public class orderRefundServiceImpl extends BasicWebService implements OrderRefundService {

    private static final Logger logger = LoggerFactory.getLogger(orderRefundServiceImpl.class);

    @Autowired
    private OrderDataAccessManager dataAccessManager;

    @SuppressWarnings("Duplicates")
    @Override
    @Transactional
    public JSONObject orderRefundApply() {
        SysUser user = getUser();
        if (user == null) return fail("获取当前用户数据失败");
        // 采购单号
        String orderId = getUTF("orderId");
        // 退款原因
        String reason = getUTF("reason", "");
        // 退款备注
        String mark = getUTF("mark", "");
        // 退款备注图片
        String img = getUTF("img","");
        // 退款商品快照
        String goodSnapshotsList = getUTF("goodSnapshotsList");
        // 采购订单-父单
        OrderEntry parentOrderEntry = dataAccessManager.getOrderEntry(orderId);
        if (parentOrderEntry==null) return fail("获取订单数据失败");
        if (parentOrderEntry.getRefundStatus()==OrderRefundStatusEnum.REFUNDING.getKey()){
            return fail("该订单处于退货退款状况，请联系管理员处理");
        }
        // ------------------------------------------------------------父单------------------------------------------------------------
        OrderRefundEntry parentOrderRefundEntry = new OrderRefundEntry();
        parentOrderRefundEntry.setPartnerUserId(user.getId());
        parentOrderRefundEntry.setPartnerUserPhone(user.getPhoneNumber());
        parentOrderRefundEntry.setOrderId(orderId);
        parentOrderRefundEntry.setSequenceNumber("");
        parentOrderRefundEntry.setOrderSequenceNumber(uniquePrimaryKey("RNY"));
        parentOrderRefundEntry.setReason(reason);
        parentOrderRefundEntry.setMark(mark);
        if (!StringUtils.isEmpty(img)){
            if (img.split(",").length > 4) return fail("不能上传超过四张图片");
            parentOrderRefundEntry.setImg(img);
        } else {
            parentOrderRefundEntry.setImg("");
        }
        // 初始化退款状态:申请
        parentOrderRefundEntry.setStatus(OrderRefundEnum.APPLY.getKey());
        // 添加List
        List<OrderRefundGoodSnapshot> orderRefundGoodSnapshotAddList = new ArrayList<>();
        List<OrderGoodSnapshot> orderGoodSnapshotChangeList = new ArrayList<>();
        List<OrderRefundGoodSnapshot> orderRefundGoodSnapshotList;
        try {
             orderRefundGoodSnapshotList = JSONObject.parseArray(goodSnapshotsList, OrderRefundGoodSnapshot.class);
        } catch (Exception e) {
            e.printStackTrace();
            return fail("JSON格式错误");
        }
        if (CollectionUtils.isEmpty(orderRefundGoodSnapshotList)) {
            return fail("获取退款商品失败");
        }
        int check = orderRefundGoodSnapshotList.stream().mapToInt(OrderRefundGoodSnapshot::getRefundQuantity).sum();
        if (check == 0) {
            return fail("请选择退款数量");
        }
        // 父单退款金额
        AtomicReference<Long> parentRefundPrice = new AtomicReference<>(0L);
        for (OrderRefundGoodSnapshot orderRefundGoodSnapshot : orderRefundGoodSnapshotList) {
            OrderGoodSnapshot orderGoodSnapshot = dataAccessManager.getOrderGoodSnapshot(orderRefundGoodSnapshot.getId());
            orderRefundGoodSnapshot.setGoodPriceId(orderGoodSnapshot.getGoodPriceId());
            // 配送中数量
            Integer distributionQuantity = orderGoodSnapshot.getDistributionQuantity();
            // 退货中数量
            Integer refundQuantity = orderRefundGoodSnapshot.getRefundQuantity();
            Long normalPrice = orderGoodSnapshot.getNormalPrice();
            if (orderGoodSnapshot == null) {
                logger.error("商品快照不存在,快照id为 {}", orderRefundGoodSnapshot.getId());
            } else if(normalPrice!=null &&distributionQuantity!=null && refundQuantity!=null) {
                orderGoodSnapshot.setRefundQuantity(refundQuantity);
                int res = distributionQuantity-refundQuantity;
                if (res < 0) {
                    return fail("退货数量不可以大于配送中数量");
                }
                OrderRefundGoodSnapshot item = new OrderRefundGoodSnapshot();
                BeanUtils.copyProperties(orderGoodSnapshot, item);
                item.setId(DefineRandom.getUUID());
                item.setOrderId(parentOrderRefundEntry.getId());
                // 添加退款父单商品快照
                orderRefundGoodSnapshotAddList.add(item);
                orderGoodSnapshotChangeList.add(orderGoodSnapshot);
                parentRefundPrice.updateAndGet(v -> v + orderGoodSnapshot.getNormalPrice() * orderRefundGoodSnapshot.getRefundQuantity());
            }
        }
        Long totalRefundPrice = parentRefundPrice.get();
        // 退款金额
        parentOrderRefundEntry.setRefundPrice(totalRefundPrice);
        // 添加退款父单
        parentOrderRefundEntry.setType(OrderTypeEnum.SUPPLYCHAINPURCHASE.getKey());
        parentOrderRefundEntry.setOldSequenceNumber(parentOrderEntry.getOrderSequenceNumber());
        dataAccessManager.addOrderRefundEntry(parentOrderRefundEntry);
        // 修改采购订单退款状态
        parentOrderEntry.setRefundStatus(OrderRefundStatusEnum.REFUNDING.getKey());
        dataAccessManager.changeOrderEntry(parentOrderEntry);
        // ------------------------------------------------------------子单------------------------------------------------------------
        // 子单列表
        List<OrderEntry> kidOrderEntryList = dataAccessManager.getOrderEntryListBySequenceNumber(parentOrderEntry.getOrderSequenceNumber());
        for (OrderEntry kidOrderEntry : kidOrderEntryList) {
            //供应商对应字段
            String saleUserId = kidOrderEntry.getSaleUserId();
            SupplychainShopProperties supplychainShopProperties = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUid(saleUserId, getHttpServletRequest());
            String shopId = null;
            String shopName = null;
            if (supplychainShopProperties != null) {
                shopId = supplychainShopProperties.getId();
                shopName = supplychainShopProperties.getShopName();
            }
            // 添加子单退款商品快照
            List<OrderGoodSnapshot> kidOrderGoodSnapshotList = dataAccessManager.getOrderGoodSnapshotByOrderId(kidOrderEntry.getId());
            if (!CollectionUtils.isEmpty(kidOrderGoodSnapshotList)) {
                // 退款子单
                OrderRefundEntry kidOrderRefundEntry = null;
                // 子单退款金额
                Long kidRefundPrice = 0L;
                for (OrderGoodSnapshot kidOrderGoodSnapshot : kidOrderGoodSnapshotList) {
                    for (OrderRefundGoodSnapshot orderRefundGoodSnapshot : orderRefundGoodSnapshotList) {
                        if(kidOrderGoodSnapshot.getGoodPriceId().equals(orderRefundGoodSnapshot.getGoodPriceId())) {
                            // 子单包含退款商品 创建退款记录实体
                            if (kidOrderRefundEntry == null) {
                                OrderRefundEntry kid = new OrderRefundEntry();
                                BeanUtils.copyProperties(parentOrderRefundEntry, kid);
                                kid.setId(DefineRandom.getUUID());
                                kid.setSequenceNumber(parentOrderRefundEntry.getOrderSequenceNumber());
                                kid.setOrderSequenceNumber(uniquePrimaryKey("RNY"));
                                kid.setOldSequenceNumber(parentOrderEntry.getOrderSequenceNumber());
                                kidOrderRefundEntry = kid;
                            }
                            // 子单快照
                            OrderGoodSnapshot orderGoodSnapshot = dataAccessManager.getOrderGoodSnapshot(kidOrderGoodSnapshot.getId());
                            if (orderGoodSnapshot == null) {
                                logger.error("商品快照不存在,快照id为 {}", kidOrderGoodSnapshot.getId());
                            } else {
                                // 退货中数量
                                Integer refundQuantity = orderRefundGoodSnapshot.getRefundQuantity();
                                if (refundQuantity == null) {
                                    refundQuantity=0;
                                }
                                // 单价
                                Long normalPrice = orderGoodSnapshot.getNormalPrice();
                                if (normalPrice == null) {
                                    normalPrice = 0L;
                                }
                                // 配送中数量
                                Integer distributionQuantity = orderGoodSnapshot.getDistributionQuantity();
                                if (distributionQuantity == null) {
                                    distributionQuantity=0;
                                }
                                OrderRefundGoodSnapshot item = new OrderRefundGoodSnapshot();
                                BeanUtils.copyProperties(orderGoodSnapshot, item);
                                item.setShopId(shopId);
                                item.setShopName(shopName);
                                item.setId(DefineRandom.getUUID());
                                item.setOrderId(kidOrderRefundEntry.getId());
                                orderRefundGoodSnapshotAddList.add(item);
                                orderGoodSnapshot.setRefundQuantity(refundQuantity);
                                int res = distributionQuantity-refundQuantity;
                                if (res < 0) {
                                    return fail("退货数量不可以大于配送中数量");
                                }
                                orderGoodSnapshotChangeList.add(orderGoodSnapshot);
                                kidRefundPrice += orderGoodSnapshot.getNormalPrice() * orderRefundGoodSnapshot.getRefundQuantity();
                            }
                        }
                    }
                }
                if (kidOrderRefundEntry != null) {
                    // 子单退款金额
                    kidOrderRefundEntry.setRefundPrice(kidRefundPrice);
                    // 添加退款子单
                    kidOrderRefundEntry.setType(OrderTypeEnum.SUPPLYCHAIN.getKey());
                    dataAccessManager.addOrderRefundEntry(kidOrderRefundEntry);
                }
            }
        }
        if (!CollectionUtils.isEmpty(orderRefundGoodSnapshotAddList)){
            dataAccessManager.batchAddOrderRefundGoodSnapshot(orderRefundGoodSnapshotAddList);
        }
        if (!CollectionUtils.isEmpty(orderGoodSnapshotChangeList)) {
            dataAccessManager.batchChangeOrderGoodSnapshot(orderGoodSnapshotChangeList);
        }
        return success();
    }

    @Override
    public JSONObject orderRefundList() {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("phone", getUTF("phone",""));
        parameters.put("status", getUTF("status",""));
        String begTimeStr = getUTF("begTime", "");
        String endTimeStr = getUTF("endTime", "");
        Date begTime = null;
        Date endTime = null;
        if (!StringUtils.isEmpty(begTimeStr)) {
            begTime = DateUtils.parse(begTimeStr,DateUtils.FORMAT_SHORT);
        }
        if (!StringUtils.isEmpty(endTimeStr)) {
            endTime = DateUtils.parse(endTimeStr,DateUtils.FORMAT_SHORT);
        }
        if (begTime != null && endTime != null) {
            if (begTime.getTime() > endTime.getTime()) {
                return fail("起始时间不可大于结束时间");
            }
        }
        parameters.put("begTime", begTime);
        parameters.put("endTime", endTime);
        // 只查看销售[父单]退款单
        parameters.put("type", OrderTypeEnum.SUPPLYCHAINPURCHASE.getKey());
        List<OrderRefundEntry> orderRefundEntryList  = dataAccessManager.getOrderRefundEntryList(parameters, getPageStartIndex(getPageSize()), getPageSize());
        int total  = dataAccessManager.getOrderRefundEntryListCount(parameters);
        return success(orderRefundEntryList,total);
    }

    @SuppressWarnings("Duplicates")
    @Override
    @Transactional
    public JSONObject orderRefundAgree() {
        String[] ids = getUTF("ids").split(",");
        if (ArrayUtils.isEmpty(ids)) {
            return fail("参数不能为空");
        }
        for (String id : ids) {
            // 退款单id
            OrderRefundEntry parentOrderRefundEntry = dataAccessManager.getOrderRefundEntry(id);
            if (parentOrderRefundEntry == null) {
                return fail("获取退款记录失败");
            }
            // 根据原父订单id获取对应快照
            OrderEntry parentOrderEntry = dataAccessManager.getOrderEntry(parentOrderRefundEntry.getOrderId());
            if (parentOrderEntry == null) {
                return fail("获取原采购订单失败");
            }
            // 获取用户余额账户
            String userId = parentOrderRefundEntry.getPartnerUserId();
            PaymentCurrencyAccount paymentCurrencyAccount = PaymentCurrencyAccountService.getPaymentCurrencyAccountByUserId(userId, getHttpServletRequest());
            if (paymentCurrencyAccount == null) {
                return fail("获取用户余额账户失败");
            }
            // 父单快照列表
            List<OrderGoodSnapshot> parentOrderGoodSnapshotList = dataAccessManager.getOrderGoodSnapshotByOrderId(parentOrderEntry.getId());
            for (OrderGoodSnapshot parentOrderGoodSnapshot : parentOrderGoodSnapshotList) {
                Integer refundQuantity = parentOrderGoodSnapshot.getRefundQuantity();
                if (refundQuantity==null) {
                    break;
                }
                if (refundQuantity>0){
                    // 修改原父单快照退货中数量、已退货数量、退款金额
                    parentOrderGoodSnapshot.setRefundQuantity(0);
                    parentOrderGoodSnapshot.setReturnQuantity(refundQuantity);
                    Integer distributionQuantity = parentOrderGoodSnapshot.getDistributionQuantity();
                    if (distributionQuantity != null) {
                        parentOrderGoodSnapshot.setDistributionQuantity(distributionQuantity-refundQuantity);
                    }
                    Long returnPrice = parentOrderGoodSnapshot.getReturnPrice();
                    if (returnPrice == null) {
                        returnPrice=0L;
                    }
                    // 退款金额
                    parentOrderGoodSnapshot.setReturnPrice(returnPrice + parentOrderGoodSnapshot.getNormalPrice()*refundQuantity);
                    dataAccessManager.changeOrderGoodSnapshot(parentOrderGoodSnapshot);
                }
            }
            Long totalRefundPrice = parentOrderRefundEntry.getRefundPrice();
            parentOrderEntry.setRefundStatus(OrderRefundStatusEnum.REFUNDED.getKey());
            dataAccessManager.changeOrderEntry(parentOrderEntry);
            // 退款至余额账户
            paymentCurrencyAccount.setCurrentAmount(paymentCurrencyAccount.getCurrentAmount()+   totalRefundPrice);
            PaymentCurrencyAccountService.changePaymentCurrencyAccount(paymentCurrencyAccount);
            // 原子单
            List<OrderEntry> kidOrderEntryList = dataAccessManager.getOrderEntryListBySequenceNumber(parentOrderRefundEntry.getOldSequenceNumber());
            for (OrderEntry kidOrderEntry : kidOrderEntryList) {
                // 子单快照列表
                List<OrderGoodSnapshot> kidOrderGoodSnapshotList = dataAccessManager.getOrderGoodSnapshotByOrderId(kidOrderEntry.getId());
                AtomicReference<Long> refundPrice = new AtomicReference<>(0L);
                for (OrderGoodSnapshot kidOrderGoodSnapshot : kidOrderGoodSnapshotList) {
                    Integer refundQuantity = kidOrderGoodSnapshot.getRefundQuantity();
                    if (refundQuantity>0){
                        // 修改原子单快照退货中数量、已退货数量、退款金额
                        kidOrderGoodSnapshot.setRefundQuantity(0);
                        kidOrderGoodSnapshot.setReturnQuantity(refundQuantity);
                        Long returnPrice = kidOrderGoodSnapshot.getReturnPrice();
                        if (returnPrice == null) {
                            returnPrice = 0L;
                        }
                        kidOrderGoodSnapshot.setReturnPrice(returnPrice + kidOrderGoodSnapshot.getNormalPrice() * refundQuantity);
                        dataAccessManager.changeOrderGoodSnapshot(kidOrderGoodSnapshot);
                        refundPrice.updateAndGet(v -> v + kidOrderGoodSnapshot.getNormalPrice() * refundQuantity);
                    }
                }
                kidOrderEntry.setTotalPrice(kidOrderEntry.getTotalPrice()- refundPrice.get());
                dataAccessManager.changeOrderEntry(kidOrderEntry);
            }
            // 修改退款记录为同意
            List<OrderRefundEntry> kidOrderRefundEntryList =
                    dataAccessManager.getOrderRefundEntryListBySequenceNumber(parentOrderRefundEntry.getOrderSequenceNumber());
            kidOrderRefundEntryList.add(parentOrderRefundEntry);
            for (OrderRefundEntry orderRefundEntry : kidOrderRefundEntryList) {
                orderRefundEntry.setStatus(OrderRefundEnum.AGREE.getKey());
                dataAccessManager.changeOrderRefundEntryStatus(orderRefundEntry);
            }
        }
        return success();
    }

    @Override
    public JSONObject orderRefundReject() {
        String[] ids = getUTF("ids").split(",");
        if (ArrayUtils.isEmpty(ids)) {
            return fail("参数不能为空");
        }
        for (String id : ids) {
            // 退款单id
            OrderRefundEntry parentOrderRefundEntry = dataAccessManager.getOrderRefundEntry(id);
            if (parentOrderRefundEntry == null) {
                return fail("获取退款记录失败");
            }
            List<OrderRefundEntry> kidOrderRefundEntryList =
                    dataAccessManager.getOrderRefundEntryListBySequenceNumber(parentOrderRefundEntry.getOrderSequenceNumber());
            kidOrderRefundEntryList.add(parentOrderRefundEntry);
            for (OrderRefundEntry orderRefundEntry : kidOrderRefundEntryList) {
                // 拒绝
                orderRefundEntry.setStatus(OrderRefundEnum.REJECT.getKey());
                dataAccessManager.changeOrderRefundEntryStatus(orderRefundEntry);
            }
        }
        return success();
    }

    @Override
    public JSONObject orderRefundEntryDetail() {

        String id = getUTF("id");
        // 退款记录
        OrderRefundEntry orderRefundEntry = dataAccessManager.getOrderRefundEntry(id);
        if (orderRefundEntry == null) {
            return fail("获取退款记录失败");
        }
        String orderId = orderRefundEntry.getOrderId();
        // 原采购订单 父单
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(orderId);
        if (orderEntry == null) {
            return fail("获取原采购订单失败");
        }
        OrderRefundEntryDetailVo vo = new OrderRefundEntryDetailVo();
        BeanUtils.copyProperties(orderRefundEntry, vo);
        vo.setOrderEntry(orderEntry);
        // 退款备注图片
        String img = orderRefundEntry.getImg();
        if (!StringUtils.isEmpty(img)){
            List<String> imgList = Arrays.stream(img.split(",")).collect(Collectors.toList());
            vo.setImgList(imgList);
        }
        // 绑定仓库
        SupplychainShopProperties supplychainShopProperties =
                SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUid(orderEntry.getSaleUserId(), getHttpServletRequest());
        vo.setShopName(supplychainShopProperties.getShopName());

        return success(vo);
    }

    @Override
    public JSONObject orderRefundGoodSnapshotList() {
        String id = getUTF("id");
        // 退款记录
        OrderRefundEntry orderRefundEntry = dataAccessManager.getOrderRefundEntry(id);
        if (orderRefundEntry == null) {
            return fail("获取退款记录失败");
        }
        // 退款父单对应退款商品 所有
        List<OrderRefundGoodSnapshot> orderRefundGoodSnapshotList =
                dataAccessManager.getOrderRefundGoodSnapshotByOrderId(orderRefundEntry.getId(), getPageStartIndex(getPageSize()), getPageSize());

        List<OrderRefundSnapshotVo> voList = new ArrayList<>();

        orderRefundGoodSnapshotList.stream().forEach(orderRefundGoodSnapshot -> {
            OrderRefundSnapshotVo vo = new OrderRefundSnapshotVo();
            BeanUtils.copyProperties(orderRefundGoodSnapshot, vo);
            vo.setTotalPrice(vo.getNormalPrice() * vo.getRefundQuantity());
            voList.add(vo);
        });

        return success(voList);
    }

}
