package com.tuoniaostore.order.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public interface OrderStatusListenerService {

    JSONObject addOrderStatusListener();

    JSONObject getOrderStatusListener();

    JSONObject getOrderStatusListeners();

    JSONObject getOrderStatusListenerCount();
    JSONObject getOrderStatusListenerAll();

    JSONObject changeOrderStatusListener();
}
