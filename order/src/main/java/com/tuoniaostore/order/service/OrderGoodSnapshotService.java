package com.tuoniaostore.order.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 商品销售快照表
大部分字段为冗余字段
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public interface OrderGoodSnapshotService {

    JSONObject addOrderGoodSnapshot();

    JSONObject getOrderGoodSnapshot();

    JSONObject getOrderGoodSnapshots();

    JSONObject getOrderGoodSnapshotCount();
    JSONObject getOrderGoodSnapshotAll();

    JSONObject changeOrderGoodSnapshot();

    JSONObject getOrderGoodSnapshotByOrderId();

    JSONObject getOrderEntryByOrderNum();

    JSONObject orderEntryWarehousing();
    JSONObject getOrderGoodSnapshotPosByOrderId();

    JSONObject getOrderEntryByorderId();

    JSONObject getOrderGoodSnapshotPosMapByorderId();

    /**
     * 采购入库单个商品确认收货
     */
    JSONObject receiptOrderGoodSnapshotByPos();

    /**
     *  采购入库多个商品确认收货
     */
    JSONObject receiptOrderGoodSnapshots();
}
