package com.tuoniaostore.order.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public interface OrderStateLoggerService {

    JSONObject addOrderStateLogger();

    JSONObject getOrderStateLogger();

    JSONObject getOrderStateLoggers();

    JSONObject getOrderStateLoggerCount();
    JSONObject getOrderStateLoggerAll();

    JSONObject changeOrderStateLogger();
}
