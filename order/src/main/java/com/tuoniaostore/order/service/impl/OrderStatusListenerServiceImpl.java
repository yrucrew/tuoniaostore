package com.tuoniaostore.order.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.order.OrderStatusListener;
import com.tuoniaostore.order.cache.OrderDataAccessManager;
import com.tuoniaostore.order.service.OrderStatusListenerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("orderStatusListenerService")
public class OrderStatusListenerServiceImpl extends BasicWebService implements OrderStatusListenerService {
    @Autowired
    private OrderDataAccessManager dataAccessManager;

    @Override
    public JSONObject addOrderStatusListener() {
        OrderStatusListener orderStatusListener = new OrderStatusListener();
        //合作商家的商户号
        String partnerId = getUTF("partnerId", null);
        //监听类型
        Integer eventType = getIntParameter("eventType", 0);
        //状态：0 -- 不启用 1 -- 启用
        Integer status = getIntParameter("status", 0);
        //回调地址
        String callbackUrl = getUTF("callbackUrl", null);
        orderStatusListener.setPartnerId(partnerId);
        orderStatusListener.setEventType(eventType);
        orderStatusListener.setStatus(status);
        orderStatusListener.setCallbackUrl(callbackUrl);
        dataAccessManager.addOrderStatusListener(orderStatusListener);
        return success();
    }

    @Override
    public JSONObject getOrderStatusListener() {
        OrderStatusListener orderStatusListener = dataAccessManager.getOrderStatusListener(getId());
        return success(orderStatusListener);
    }

    @Override
    public JSONObject getOrderStatusListeners() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<OrderStatusListener> orderStatusListeners = dataAccessManager.getOrderStatusListeners(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getOrderStatusListenerCount(status, name);
        return success(orderStatusListeners, count);
    }

    @Override
    public JSONObject getOrderStatusListenerAll() {
        List<OrderStatusListener> orderStatusListeners = dataAccessManager.getOrderStatusListenerAll();
        return success(orderStatusListeners);
    }

    @Override
    public JSONObject getOrderStatusListenerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getOrderStatusListenerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeOrderStatusListener() {
        OrderStatusListener orderStatusListener = dataAccessManager.getOrderStatusListener(getId());
        //合作商家的商户号
        String partnerId = getUTF("partnerId", null);
        //监听类型
        Integer eventType = getIntParameter("eventType", 0);
        //状态：0 -- 不启用 1 -- 启用
        Integer status = getIntParameter("status", 0);
        //回调地址
        String callbackUrl = getUTF("callbackUrl", null);
        orderStatusListener.setPartnerId(partnerId);
        orderStatusListener.setEventType(eventType);
        orderStatusListener.setStatus(status);
        orderStatusListener.setCallbackUrl(callbackUrl);
        dataAccessManager.changeOrderStatusListener(orderStatusListener);
        return success();
    }

}
