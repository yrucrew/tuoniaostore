package com.tuoniaostore.order.service;

import com.alibaba.fastjson.JSONObject;

public interface OrderEntryPosService {

    /**
     * @return 获取销售订单列表
     */
    JSONObject getOrderEntryPosList();

    JSONObject getOrderEntryPosTotal();
}
