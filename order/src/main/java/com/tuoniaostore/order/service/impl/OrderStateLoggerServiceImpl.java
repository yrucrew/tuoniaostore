package com.tuoniaostore.order.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.order.OrderStateLogger;
import com.tuoniaostore.order.cache.OrderDataAccessManager;
import com.tuoniaostore.order.service.OrderStateLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("orderStateLoggerService")
public class OrderStateLoggerServiceImpl extends BasicWebService implements OrderStateLoggerService {
    @Autowired
    private OrderDataAccessManager dataAccessManager;

    @Override
    public JSONObject addOrderStateLogger() {
        OrderStateLogger orderStateLogger = new OrderStateLogger();
        //对应的订单ID
        String orderId = getUTF("orderId");
        //发货者通行证ID
        String shippingUserId = getUTF("shippingUserId");
        //对应的订单类型
        Integer orderType = getIntParameter("orderType", 0);
        //操作前的状态
        Integer beforeStatus = getIntParameter("beforeStatus", 0);
        //操作后的状态
        Integer status = getIntParameter("status", 0);
        //操作者角色ID
        String operatorUserId = getUTF("operatorUserId", null);
        //操作者名字
        String operatorName = getUTF("operatorName", null);
        //操作备注信息
        String operatorDescribe = getUTF("operatorDescribe", null);
        orderStateLogger.setOrderId(orderId);
        orderStateLogger.setShippingUserId(shippingUserId);
        orderStateLogger.setOrderType(orderType);
        orderStateLogger.setBeforeStatus(beforeStatus);
        orderStateLogger.setStatus(status);
        orderStateLogger.setOperatorUserId(operatorUserId);
        orderStateLogger.setOperatorName(operatorName);
        orderStateLogger.setOperatorDescribe(operatorDescribe);
        dataAccessManager.addOrderStateLogger(orderStateLogger);
        return success();
    }

    @Override
    public JSONObject getOrderStateLogger() {
        OrderStateLogger orderStateLogger = dataAccessManager.getOrderStateLogger(getId());
        return success(orderStateLogger);
    }

    @Override
    public JSONObject getOrderStateLoggers() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<OrderStateLogger> orderStateLoggers = dataAccessManager.getOrderStateLoggers(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getOrderStateLoggerCount(status, name);
        return success(orderStateLoggers, count);
    }

    @Override
    public JSONObject getOrderStateLoggerAll() {
        List<OrderStateLogger> orderStateLoggers = dataAccessManager.getOrderStateLoggerAll();
        return success(orderStateLoggers);
    }

    @Override
    public JSONObject getOrderStateLoggerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getOrderStateLoggerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeOrderStateLogger() {
        OrderStateLogger orderStateLogger = dataAccessManager.getOrderStateLogger(getId());
        //对应的订单ID
        String orderId = getUTF("orderId");
        //发货者通行证ID
        String shippingUserId = getUTF("shippingUserId");
        //对应的订单类型
        Integer orderType = getIntParameter("orderType", 0);
        //操作前的状态
        Integer beforeStatus = getIntParameter("beforeStatus", 0);
        //操作后的状态
        Integer status = getIntParameter("status", 0);

        //操作者角色ID
        String operatorUserId = getUTF("operatorUserId", null);
        //操作者名字
        String operatorName = getUTF("operatorName", null);
        //操作备注信息
        String operatorDescribe = getUTF("operatorDescribe", null);
        orderStateLogger.setOrderId(orderId);
        orderStateLogger.setShippingUserId(shippingUserId);
        orderStateLogger.setOrderType(orderType);
        orderStateLogger.setBeforeStatus(beforeStatus);
        orderStateLogger.setStatus(status);
        orderStateLogger.setOperatorUserId(operatorUserId);
        orderStateLogger.setOperatorName(operatorName);
        orderStateLogger.setOperatorDescribe(operatorDescribe);
        dataAccessManager.changeOrderStateLogger(orderStateLogger);
        return success();
    }

}
