package com.tuoniaostore.order.service;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * 订单的推送日志
与未接订单日志的区别为该表不物理删除数据 可以查询订单是否曾经已执行推送 且明白推送的目标
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public interface OrderPushedLoggerService {

    JSONObject addOrderPushedLogger();

    JSONObject getOrderPushedLogger();

    JSONObject getOrderPushedLoggers();

    JSONObject getOrderPushedLoggerCount();
    JSONObject getOrderPushedLoggerAll();

    JSONObject changeOrderPushedLogger();
}
