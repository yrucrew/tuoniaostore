package com.tuoniaostore.order.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.order.OrderDeliverStatus;
import com.tuoniaostore.commons.constant.order.OrderReceiptTitel;
import com.tuoniaostore.commons.constant.order.OrderRefundStatusEnum;
import com.tuoniaostore.commons.constant.order.OrderStatus;
import com.tuoniaostore.commons.constant.supplychain.StockOffsetTypeEnum;
import com.tuoniaostore.commons.support.good.GoodPriceRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplyChainGoodRemoteService;
import com.tuoniaostore.commons.support.supplychain.SupplychainGoodStockLoggersRemoteService;
import com.tuoniaostore.datamodel.good.GoodPrice;
import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodStockLogger;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.vo.order.OrderGoodSnapshotVO;
import com.tuoniaostore.order.VO.GoodPurchaseVO;
import com.tuoniaostore.order.VO.GoodSnapshotVO;
import com.tuoniaostore.order.VO.OrderEntryPosVO;
import com.tuoniaostore.order.cache.OrderDataAccessManager;
import com.tuoniaostore.order.service.OrderGoodSnapshotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;


@Service("orderGoodSnapshotService")
public class OrderGoodSnapshotServiceImpl extends BasicWebService implements OrderGoodSnapshotService {

    private static final Logger logger = LoggerFactory.getLogger(OrderGoodSnapshotServiceImpl.class);

    @Autowired
    private OrderDataAccessManager dataAccessManager;

    @Override
    public JSONObject addOrderGoodSnapshot() {
        OrderGoodSnapshot orderGoodSnapshot = new OrderGoodSnapshot();
        //对应的订单ID
        String orderId = getUTF("orderId");
        //购买的用户ID或其他标志(如第三方用户ID)
        String userMark = getUTF("userMark", null);
        //相关的商品ID
        String goodId = getUTF("goodId");
        //商品模版ID
        String goodTemplateId = getUTF("goodTemplateId");
        //当时的商品名称
        String goodName = getUTF("goodName", null);
        //当时的归属类型ID
        String goodTypeId = getUTF("goodTypeId");
        //当时归属的类型名
        String goodTypeName = getUTF("goodTypeName", null);
        //当时的单位ID
        String goodUnitId = getUTF("goodUnitId");
        //当时单位名称
        String goodUnitName = getUTF("goodUnitName", null);
        //商品条码
        String goodBarcode = getUTF("goodBarcode", null);
        //当时商品的编码
        String goodCode = getUTF("goodCode", null);
        //商品批次
        Integer goodBatches = getIntParameter("goodBatches", 0);
        //当时的介绍页面
        String introductionPage = getUTF("introductionPage", null);
        //正常价格购买的数量
        Integer normalQuantity = getIntParameter("normalQuantity", 0);
        //优惠价格购买的数量
        Integer discountQuantity = getIntParameter("discountQuantity", 0);
        //发货中的数量
        Integer shipmentQuantity = getIntParameter("shipmentQuantity", 0);
        //配送中的数量
        Integer distributionQuantity = getIntParameter("distributionQuantity", 0);
        //送达的数量
        Integer arriveQuantity = getIntParameter("arriveQuantity", 0);
        //被确认收货的数量
        Integer receiptQuantity = getIntParameter("receiptQuantity", 0);
        //退货中数量
        Integer refundQuantity = getIntParameter("refundQuantity", 0);
        //退货的数量
        Integer returnQuantity = getIntParameter("returnQuantity", 0);
        //复核数量
        Integer reviewQuantity = getIntParameter("reviewQuantity", 0);
        //正常价格
        Long normalPrice = getLongParameter("normalPrice", 0);
        //优惠价格
        Long discountPrice = getLongParameter("discountPrice", 0);
        //当时的成本价
        Long costPrice = getLongParameter("costPrice", 0);
        //当时一般的市场价
        Long marketPrice = getLongParameter("marketPrice", 0);
        //应收费用
        Long totalPrice = getLongParameter("totalPrice", 0);
        //退货的费用(实收为应收费用-退货费用)
        Long returnPrice = getLongParameter("returnPrice", 0);
        //区分仓库的订单号
        String innerOrderId = getUTF("innerOrderId", null);
        //第三方送货的数量
        Integer thirdPartyDeliveryQuantity = getIntParameter("thirdPartyDeliveryQuantity", 0);
        //第三方排线的数量
        Integer thirdPartyAcceptQuantity = getIntParameter("thirdPartyAcceptQuantity", 0);
        //是否第三方商品，否为0
        Integer isThirdParty = getIntParameter("isThirdParty", 0);
        orderGoodSnapshot.setOrderId(orderId);
        orderGoodSnapshot.setUserMark(userMark);
        orderGoodSnapshot.setGoodId(goodId);
        orderGoodSnapshot.setGoodTemplateId(goodTemplateId);
        orderGoodSnapshot.setGoodName(goodName);
        orderGoodSnapshot.setGoodTypeId(goodTypeId);
        orderGoodSnapshot.setGoodTypeName(goodTypeName);
        orderGoodSnapshot.setGoodUnitId(goodUnitId);
        orderGoodSnapshot.setGoodUnitName(goodUnitName);
        orderGoodSnapshot.setGoodBarcode(goodBarcode);
        orderGoodSnapshot.setGoodCode(goodCode);
        orderGoodSnapshot.setGoodBatches(goodBatches);
        orderGoodSnapshot.setIntroductionPage(introductionPage);
        orderGoodSnapshot.setNormalQuantity(normalQuantity);
        orderGoodSnapshot.setDiscountQuantity(discountQuantity);
        orderGoodSnapshot.setShipmentQuantity(shipmentQuantity);
        orderGoodSnapshot.setDistributionQuantity(distributionQuantity);
        orderGoodSnapshot.setArriveQuantity(arriveQuantity);
        orderGoodSnapshot.setReceiptQuantity(receiptQuantity);
        orderGoodSnapshot.setRefundQuantity(refundQuantity);
        orderGoodSnapshot.setReturnQuantity(returnQuantity);
        orderGoodSnapshot.setReviewQuantity(reviewQuantity);
        orderGoodSnapshot.setNormalPrice(normalPrice);
        orderGoodSnapshot.setDiscountPrice(discountPrice);
        orderGoodSnapshot.setCostPrice(costPrice);
        orderGoodSnapshot.setMarketPrice(marketPrice);
        orderGoodSnapshot.setTotalPrice(totalPrice);
        orderGoodSnapshot.setReturnPrice(returnPrice);
        orderGoodSnapshot.setInnerOrderId(innerOrderId);
        orderGoodSnapshot.setThirdPartyDeliveryQuantity(thirdPartyDeliveryQuantity);
        orderGoodSnapshot.setThirdPartyAcceptQuantity(thirdPartyAcceptQuantity);
        orderGoodSnapshot.setIsThirdParty(isThirdParty);
        dataAccessManager.addOrderGoodSnapshot(orderGoodSnapshot);
        return success();
    }

    @Override
    public JSONObject getOrderGoodSnapshot() {
        OrderGoodSnapshot orderGoodSnapshot = dataAccessManager.getOrderGoodSnapshot(getId());
        return success(orderGoodSnapshot);
    }

    @Override
    public JSONObject getOrderGoodSnapshots() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<OrderGoodSnapshot> orderGoodSnapshots = dataAccessManager.getOrderGoodSnapshots(status, getPageStartIndex(getPageSize()), getPageSize(), name);

        int count = dataAccessManager.getOrderGoodSnapshotCount(status, name);
        return success(orderGoodSnapshots, count);
    }


    @Override
    public JSONObject getOrderGoodSnapshotAll() {
        List<OrderGoodSnapshot> orderGoodSnapshots = dataAccessManager.getOrderGoodSnapshotAll();
        return success(orderGoodSnapshots);
    }

    @Override
    public JSONObject getOrderGoodSnapshotCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getOrderGoodSnapshotCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeOrderGoodSnapshot() {
        OrderGoodSnapshot orderGoodSnapshot = dataAccessManager.getOrderGoodSnapshot(getId());
        //对应的订单ID
        String orderId = getUTF("orderId");
        //购买的用户ID或其他标志(如第三方用户ID)
        String userMark = getUTF("userMark", null);
        //相关的商品ID
        String goodId = getUTF("goodId");
        //商品模版ID
        String goodTemplateId = getUTF("goodTemplateId");
        //当时的商品名称
        String goodName = getUTF("goodName", null);
        //当时的归属类型ID
        String goodTypeId = getUTF("goodTypeId");
        //当时归属的类型名
        String goodTypeName = getUTF("goodTypeName", null);
        //当时的单位ID
        String goodUnitId = getUTF("goodUnitId");
        //当时单位名称
        String goodUnitName = getUTF("goodUnitName", null);
        //商品条码
        String goodBarcode = getUTF("goodBarcode", null);
        //当时商品的编码
        String goodCode = getUTF("goodCode", null);
        //商品批次
        Integer goodBatches = getIntParameter("goodBatches", 0);
        //当时的介绍页面
        String introductionPage = getUTF("introductionPage", null);
        //正常价格购买的数量
        Integer normalQuantity = getIntParameter("normalQuantity", 0);
        //优惠价格购买的数量
        Integer discountQuantity = getIntParameter("discountQuantity", 0);
        //发货中的数量
        Integer shipmentQuantity = getIntParameter("shipmentQuantity", 0);
        //配送中的数量
        Integer distributionQuantity = getIntParameter("distributionQuantity", 0);
        //送达的数量
        Integer arriveQuantity = getIntParameter("arriveQuantity", 0);
        //被确认收货的数量
        Integer receiptQuantity = getIntParameter("receiptQuantity", 0);
        //退货中数量
        Integer refundQuantity = getIntParameter("refundQuantity", 0);
        //退货的数量
        Integer returnQuantity = getIntParameter("returnQuantity", 0);
        //复核数量
        Integer reviewQuantity = getIntParameter("reviewQuantity", 0);
        //正常价格
        Long normalPrice = getLongParameter("normalPrice", 0);
        //优惠价格
        Long discountPrice = getLongParameter("discountPrice", 0);
        //当时的成本价
        Long costPrice = getLongParameter("costPrice", 0);
        //当时一般的市场价
        Long marketPrice = getLongParameter("marketPrice", 0);
        //应收费用
        Long totalPrice = getLongParameter("totalPrice", 0);
        //退货的费用(实收为应收费用-退货费用)
        Long returnPrice = getLongParameter("returnPrice", 0);
        //区分仓库的订单号
        String innerOrderId = getUTF("innerOrderId", null);
        //第三方送货的数量
        Integer thirdPartyDeliveryQuantity = getIntParameter("thirdPartyDeliveryQuantity", 0);
        //第三方排线的数量
        Integer thirdPartyAcceptQuantity = getIntParameter("thirdPartyAcceptQuantity", 0);
        //是否第三方商品，否为0
        Integer isThirdParty = getIntParameter("isThirdParty", 0);
        orderGoodSnapshot.setOrderId(orderId);
        orderGoodSnapshot.setUserMark(userMark);
        orderGoodSnapshot.setGoodId(goodId);
        orderGoodSnapshot.setGoodTemplateId(goodTemplateId);
        orderGoodSnapshot.setGoodName(goodName);
        orderGoodSnapshot.setGoodTypeId(goodTypeId);
        orderGoodSnapshot.setGoodTypeName(goodTypeName);
        orderGoodSnapshot.setGoodUnitId(goodUnitId);
        orderGoodSnapshot.setGoodUnitName(goodUnitName);
        orderGoodSnapshot.setGoodBarcode(goodBarcode);
        orderGoodSnapshot.setGoodCode(goodCode);
        orderGoodSnapshot.setGoodBatches(goodBatches);
        orderGoodSnapshot.setIntroductionPage(introductionPage);
        orderGoodSnapshot.setNormalQuantity(normalQuantity);
        orderGoodSnapshot.setDiscountQuantity(discountQuantity);
        orderGoodSnapshot.setShipmentQuantity(shipmentQuantity);
        orderGoodSnapshot.setDistributionQuantity(distributionQuantity);
        orderGoodSnapshot.setArriveQuantity(arriveQuantity);
        orderGoodSnapshot.setReceiptQuantity(receiptQuantity);
        orderGoodSnapshot.setRefundQuantity(refundQuantity);
        orderGoodSnapshot.setReturnQuantity(returnQuantity);
        orderGoodSnapshot.setReviewQuantity(reviewQuantity);
        orderGoodSnapshot.setNormalPrice(normalPrice);
        orderGoodSnapshot.setDiscountPrice(discountPrice);
        orderGoodSnapshot.setCostPrice(costPrice);
        orderGoodSnapshot.setMarketPrice(marketPrice);
        orderGoodSnapshot.setTotalPrice(totalPrice);
        orderGoodSnapshot.setReturnPrice(returnPrice);
        orderGoodSnapshot.setInnerOrderId(innerOrderId);
        orderGoodSnapshot.setThirdPartyDeliveryQuantity(thirdPartyDeliveryQuantity);
        orderGoodSnapshot.setThirdPartyAcceptQuantity(thirdPartyAcceptQuantity);
        orderGoodSnapshot.setIsThirdParty(isThirdParty);
        dataAccessManager.changeOrderGoodSnapshot(orderGoodSnapshot);
        return success();
    }

    @Override
    public JSONObject getOrderGoodSnapshotByOrderId() {
        String orderId = getUTF("orderId");
        String isShippingfee = getUTF("isShippingfee",null);
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(orderId);
        List<OrderGoodSnapshot> snapshotByOrderId = dataAccessManager.getOrderGoodSnapshotByOrderId(orderId);
        ArrayList<OrderGoodSnapshotVO> list = new ArrayList<>();
        long refundPrice = 0;
        for (OrderGoodSnapshot orderGoodSnapshot : snapshotByOrderId) {
            OrderGoodSnapshotVO orderGoodSnapshotVO = new OrderGoodSnapshotVO();
            BeanUtils.copyProperties(orderGoodSnapshot, orderGoodSnapshotVO);
            Integer normalQuantity = orderGoodSnapshotVO.getNormalQuantity();
            if(normalQuantity == null){
                normalQuantity = 0;
            }
            Integer receiptQuantity = orderGoodSnapshotVO.getReceiptQuantity();
            if(receiptQuantity == null){
                receiptQuantity = 0;
            }
            if(orderGoodSnapshot.getReturnPrice() != null){
                refundPrice += orderGoodSnapshot.getReturnPrice();
            }
            orderGoodSnapshotVO.setUnReceiptQuantity(normalQuantity - receiptQuantity);
            list.add(orderGoodSnapshotVO);
        }
        orderEntry.setRefundPrice(refundPrice);

        JSONObject jsonObject = new JSONObject();
        if(isShippingfee != null && orderEntry != null){
            orderEntry.setDistributionFee(Long.parseLong("0"));
        }else{
            jsonObject.put("title", OrderReceiptTitel.TITLE);
            jsonObject.put("subtitle", OrderReceiptTitel.SUBTITLE);
            jsonObject.put("undersigned", OrderReceiptTitel.UNDERSIGNED);
        }
        jsonObject.put("goodsList", list);
        jsonObject.put("orderEntry", orderEntry);
        return success(jsonObject);
    }

    @Override
    public JSONObject getOrderEntryByOrderNum() {
        //获取输入的订单号
        String orderNum = getUTF("orderNum");
        if (orderNum == null || orderNum.equals("")) {
            return null;
        }
        //通过订单号，获取订单的快照详情
        List<OrderGoodSnapshot> entryList = dataAccessManager.getOrderGoodSnapshotByOrderNum(orderNum);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list", entryList);
        return jsonObject;
    }


    /**
     * 商品入库
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/4/25
     */
    @Override
    @Transactional
    public JSONObject orderEntryWarehousing() {
        //获取订单号
        String orderNum = getUTF("orderNum");

        Integer operationType = getIntParameter("operationType");//操作类型 1-销售出库 2-采购入库 3-退货入库

        //获取实体类型数组 商品id :goodId 商品数量:goodNum
        String list = getUTF("list");

        SysUser user = getUser();
        String defaultName = user.getDefaultName();
        if (defaultName == null || defaultName.equals("")) {
            defaultName = user.getPhoneNumber();
        }
        String userId = user.getId();

        List<GoodPurchaseVO> goodPurchaseVOS = JSONObject.parseArray(list, GoodPurchaseVO.class);

        //判断该订单下面是否有当前这些商品需要操作
        List<OrderGoodSnapshot> entryList = dataAccessManager.getOrderGoodSnapshotByOrderNum(orderNum);
        List<String> goodIds = entryList.stream().map(OrderGoodSnapshot::getGoodId).collect(Collectors.toList());
        List<SupplychainGood> supplychainGoodByIds = SupplyChainGoodRemoteService.getSupplychainGoodByIds(goodIds, getHttpServletRequest());

        //需要更新的集合数据
        List<SupplychainGood> goodList = new ArrayList<>();//商品库
        List<OrderGoodSnapshot> orderGoodSnapshots = new ArrayList<>();//订单快照
        List<SupplychainGoodStockLogger> supplychainGoodStockLoggers = new ArrayList<>();//库存变化表

        if (entryList != null && entryList.size() > 0) {
            for (OrderGoodSnapshot x : entryList) {
                String orderId = x.getOrderId();//订单号
                //找出对应的商品
                Optional<GoodPurchaseVO> first =
                        goodPurchaseVOS.stream().filter(item -> item.getGoodId().equals(x.getGoodId())).findFirst();
                if (first.isPresent()) {//必须要和商品快照中的  商品id数据对的上
                    GoodPurchaseVO goodPurchaseVO = first.get();
                    Integer goodNum = Integer.parseInt(goodPurchaseVO.getGoodNum());//本次签收这么多个
                    Integer normalQuantity = x.getNormalQuantity();//需要这么多个
                    Integer receiptQuantity = x.getReceiptQuantity();//已经签收这么多个
                    if (receiptQuantity == null) {
                        receiptQuantity = 0;
                    }
                    //检查数量
                    if (normalQuantity < (goodNum + receiptQuantity)) {
                        String goodName = x.getGoodName();
                        return fail("您签收的商品:" + goodName + " 数量不对！请核实！");
                    }
                    //正常操作
                    x.setReceiptQuantity(goodNum + receiptQuantity);
                    orderGoodSnapshots.add(x);

                    //需要更新一下库存日志信息 supplychain_good_stock_logger
                    //首先去看一下本身的库存
                    Optional<SupplychainGood> first1 = supplychainGoodByIds.stream().filter(item -> item.getId().equals(x.getGoodId())).findFirst();
                    if (first1.isPresent()) {
                        SupplychainGood supplychainGood = first1.get();
                        Integer beforeStock = supplychainGood.getStock();//库存前
                        if (beforeStock == null) {
                            beforeStock = 0;
                        }
                        supplychainGood.setStock(beforeStock + goodNum);//新增库存
                        goodList.add(supplychainGood);//需要修改库存

                        //库存偏移记录
                        SupplychainGoodStockLogger stockLogger = new SupplychainGoodStockLogger();
                        stockLogger.setBeforeStock(beforeStock);//之前的库存
                        stockLogger.setAfterStock(beforeStock + goodNum);//之后的库存
                        stockLogger.setOrderId(orderId);//订单号
                        stockLogger.setGoodId(supplychainGood.getId());//商品id
                        stockLogger.setOffsetStock(goodNum);//商品数偏移量
                        stockLogger.setOperatorRoleId(userId);//操作人id
                        stockLogger.setOperatorRoleName(defaultName);//操作人name
                        stockLogger.setType(StockOffsetTypeEnum.STORAGE.getType());//操作类型 StockOffsetTypeEnum
                        stockLogger.setOperationType(operationType);//操作类型 1-销售出库 2-采购入库 3-退货入库
                        supplychainGoodStockLoggers.add(stockLogger);//添加
                    }
                }
            }
        }

        if (orderGoodSnapshots != null && orderGoodSnapshots.size() > 0) {
            dataAccessManager.updateReceiptQuantity(orderGoodSnapshots);//更新快照表
        }

        if (goodList != null && goodList.size() > 0) {
            SupplyChainGoodRemoteService.updateStockByList(goodList, getHttpServletRequest());
        }
        updateByAsync(supplychainGoodStockLoggers);//异步更新
        return success("添加成功!");
    }

    /**
     * POS订单交易列表  查询订单详情
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/10
     */
    @Override
    public JSONObject getOrderGoodSnapshotPosByOrderId() {
        String orderId = getUTF("orderId");
        HashMap<String, Object> map = new HashMap<>();
        map.put("orderId", orderId);
        int pageStartIndex = getPageStartIndex(getPageSize());
        int pageSize = getPageSize();
        List<GoodSnapshotVO> goodSnapshotPosList = dataAccessManager.getOrderGoodSnapshotPosByOrderId(orderId, pageStartIndex, pageSize);
        int Count = dataAccessManager.getOrderGoodSnapshotPosByOrderIdCount(orderId, pageStartIndex, pageSize);
        List<OrderEntry> orderEntrysPos = dataAccessManager.getOrderEntrysPos(map);

        OrderEntryPosVO orderEntryPos = new OrderEntryPosVO();
        if (orderEntrysPos.size() > 0) {
            BeanUtils.copyProperties(orderEntrysPos.get(0), orderEntryPos);
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("orderEntryPos", orderEntryPos);
        jsonObject.put("goodSnapshotPosList", goodSnapshotPosList);
        jsonObject.put("Count", Count);
        return success(jsonObject);
    }

    @Override
    public JSONObject getOrderEntryByorderId() {
        String orderId = getUTF("orderId");
        List<OrderGoodSnapshot> orderGoodSnapshotByOrderId = dataAccessManager.getOrderGoodSnapshotByOrderId(orderId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list", orderGoodSnapshotByOrderId);
        return success(jsonObject);
    }

    @Override
    public JSONObject getOrderGoodSnapshotPosMapByorderId() {
        String orderId = getUTF("orderId");
        HashMap<Object, Object> map = new HashMap<>();
        map.put("orderId", orderId);
        List<OrderGoodSnapshot> orderGoodSnapshotByOrderId = new ArrayList<>();
        orderGoodSnapshotByOrderId = dataAccessManager.getOrderGoodSnapshotPosByMap(map);
        return success(orderGoodSnapshotByOrderId);
    }

    @Override
    public JSONObject receiptOrderGoodSnapshotByPos() {
        // 订单id
        String orderId = getUTF("orderId");
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(orderId);
        if (orderEntry == null) {
            return fail("获取订单数据失败");
        }
        if (orderEntry.getRefundStatus() == OrderRefundStatusEnum.REFUNDING.getKey()) {
            return fail("该订单处于退货退款状况，请联系管理员处理");
        }
        // 商品快照id
        String id = getUTF("id");
        // 本次收货数量
        int count = getIntParameter("count");
        // 收货
        String message = receiptByIdAndCount(id, count);
        if (!StringUtils.isEmpty(message)) {
            return fail(message);
        }
        // 检查是否完成收货
        checkOrderEntryFinish(orderId);
        return success("收货成功");
    }

    @Override
    @Transactional
    public JSONObject receiptOrderGoodSnapshots() {
        String orderId = getUTF("orderId");
        String params = getUTF("params");
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(orderId);
        if (orderEntry == null) {
            return fail("获取订单数据失败");
        }
        if (orderEntry.getRefundStatus() == OrderRefundStatusEnum.REFUNDING.getKey()) {
            return fail("该订单处于退货退款状况，请联系管理员处理");
        }
        List<OrderGoodSnapshot> orderGoodSnapshotList = JSONObject.parseArray(params, OrderGoodSnapshot.class);
        if (CollectionUtils.isEmpty(orderGoodSnapshotList)) {
            return fail("未接收到参数");
        }
        for (OrderGoodSnapshot orderGoodSnapshot : orderGoodSnapshotList) {
            String message = receiptByIdAndCount(orderGoodSnapshot.getId(), orderGoodSnapshot.getReceiptQuantity());
            if (!StringUtils.isEmpty(message)) {
                return fail(message);
            }
        }
        checkOrderEntryFinish(orderId);
        return success("收货成功");
    }

    /**
     * 入库
     *
     * @param id    商品快照id
     * @param count 数量
     * @return 结果信息
     */
    @Transactional
    String receiptByIdAndCount(String id, int count) {
        // ""为成功
        String SUCCESS = "";
        OrderGoodSnapshot orderGoodSnapshot = dataAccessManager.getOrderGoodSnapshot(id);
        if (orderGoodSnapshot == null) {
            return "商品快照数据不存在";
        }
        Integer normalQuantity = orderGoodSnapshot.getNormalQuantity();
        Integer receiptQuantity = orderGoodSnapshot.getReceiptQuantity();
        Integer returnQuantity = orderGoodSnapshot.getReturnQuantity();
        Integer unReceiptQuantity = 0;
        if (receiptQuantity == null) {
            receiptQuantity = 0;
        }
        if (returnQuantity == null) {
            returnQuantity = 0;
        }
        if (normalQuantity != null) {
            unReceiptQuantity = normalQuantity - receiptQuantity - returnQuantity;
        } else {
            return "该商品快照数量不存在";
        }
        if (unReceiptQuantity == 0) {
            return SUCCESS;
        }
        if (unReceiptQuantity - count < 0) {
            return "本次收货数量超出未收货数量";
        } else {
            // 更新已收货数量
            orderGoodSnapshot.setReceiptQuantity(receiptQuantity + count);
            // 更新配送中
            int distributionQuantity = orderGoodSnapshot.getDistributionQuantity() - count;
            if (distributionQuantity < 0) {
                // 确认收货数大于配送数
                return "收货数大于配送数";
            }
            orderGoodSnapshot.setDistributionQuantity(distributionQuantity);

            String parentOrderId = orderGoodSnapshot.getOrderId();
            OrderEntry parentOrderEntry = dataAccessManager.getOrderEntry(parentOrderId);
            // 子单列表
            List<OrderEntry> orderEntryListBySequenceNumber = dataAccessManager.getOrderEntryListBySequenceNumber(parentOrderEntry.getOrderSequenceNumber());
            List<String> orderIds = orderEntryListBySequenceNumber.stream().map(OrderEntry::getId).collect(Collectors.toList());
            String goodPriceId = orderGoodSnapshot.getGoodPriceId();
            Map<String, Object> map = new HashMap<>();
            map.put("goodPriceId", goodPriceId);
            map.put("orderIds", orderIds);
            List<OrderGoodSnapshot> orderGoodSnapshotByMap = dataAccessManager.getOrderGoodSnapshotByMap(map);
            Optional<OrderGoodSnapshot> first = orderGoodSnapshotByMap.stream().findFirst();
            if (first.isPresent()){
                first.get().setReceiptQuantity(orderGoodSnapshot.getReceiptQuantity());
                first.get().setDistributionQuantity(orderGoodSnapshot.getDistributionQuantity());
                // 修改子单商品快照
                logger.info("--------------------------------------修改子单商品快照---------------------------------");
                dataAccessManager.changeOrderGoodSnapshot(first.get());
            }
            dataAccessManager.changeOrderGoodSnapshot(orderGoodSnapshot);
            // 根据规格 X 数量入库
            GoodPrice goodPrice = GoodPriceRemoteService.getGoodPriceById(orderGoodSnapshot.getGoodPriceId(), getHttpServletRequest());
            if (goodPrice != null && goodPrice.getNum() > 0) {
                count *= goodPrice.getNum();
            }
            //更新商品库存 isAdd 1-增加 0-减少
            SupplyChainGoodRemoteService.changSupplychainGoodByGoodId(orderGoodSnapshot.getGoodId(), count, 1, getHttpServletRequest());
            return SUCCESS;
        }
    }

    /**
     * 检查订单是否完成
     *
     * @param orderId
     */
    void checkOrderEntryFinish(String orderId) {
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(orderId);
        if (orderEntry == null) {
            logger.error("订单数据不存在");
            return;
        }
        if (orderEntry.getStatus().equals(OrderStatus.ORDER_TYPE_CONFIRMRECEIPT.getKey())) {
            logger.error("该订单已确认收货");
            return;
        }
        boolean flag = true;
        List<OrderGoodSnapshot> orderGoodSnapshotList = dataAccessManager.getOrderGoodSnapshotByOrderId(orderId);
        for (OrderGoodSnapshot orderGoodSnapshot : orderGoodSnapshotList) {
            Integer normalPrice = orderGoodSnapshot.getNormalQuantity();
            Integer receiptQuantity = orderGoodSnapshot.getReceiptQuantity();
            Integer returnQuantity = orderGoodSnapshot.getReturnQuantity();
            if (normalPrice != null && receiptQuantity != null && returnQuantity != null) {
                if (normalPrice != receiptQuantity + returnQuantity) {
                    flag = false;
                    break;
                }
            }
        }
        if (flag) {
            // 修改订单状态
            orderEntry.setDeliverStatus(OrderDeliverStatus.HAVEARRIVED.getKey());
            orderEntry.setStatus(OrderStatus.ORDER_TYPE_CONFIRMRECEIPT.getKey());
            dataAccessManager.changeOrderEntry(orderEntry);
        }
    }

    /**
     * 异步更新记录
     *
     * @param supplychainGoodStockLoggers
     * @return void
     * @author oy
     * @date 2019/4/25
     */
    @Async
    public void updateByAsync(List<SupplychainGoodStockLogger> supplychainGoodStockLoggers) {
        if (supplychainGoodStockLoggers != null && supplychainGoodStockLoggers.size() > 0) {
            SupplychainGoodStockLoggersRemoteService.addSupplychainGoodStockLoggers(supplychainGoodStockLoggers, getHttpServletRequest());
        }
    }


}
