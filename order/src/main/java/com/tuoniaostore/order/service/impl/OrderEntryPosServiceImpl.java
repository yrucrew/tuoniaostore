package com.tuoniaostore.order.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.vo.order.OrderEntryPos;
import com.tuoniaostore.datamodel.vo.order.OrderEntryPosVO;
import com.tuoniaostore.datamodel.vo.order.OrderGoodSnapshotVO;
import com.tuoniaostore.order.VO.OrderEntrysTotalVO;
import com.tuoniaostore.order.VO.OrderGoodSnapshotVo;
import com.tuoniaostore.order.cache.OrderDataAccessManager;
import com.tuoniaostore.order.service.OrderEntryPosService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service("OrderEntryPosService")
public class OrderEntryPosServiceImpl extends BasicWebService implements OrderEntryPosService {

    @Autowired
    OrderDataAccessManager dataAccessManager;

    @Override
    public JSONObject getOrderEntryPosList() {
        Map<String, Object> params = new HashMap<>();
        // 订单类型
        params.put("type", getIntParameter("type", 0));
        // 销售仓id
        params.put("saleUserId", getUTF("saleUserId", null));

        List<OrderEntryPos> orderEntryPosList = dataAccessManager.getOrderEntryPosList(params, getPageStartIndex(getPageSize()), getPageSize());
        int total = dataAccessManager.getOrderEntryPosListCount(params);

        // 订单列表
        List<OrderEntryPosVO> voList = new ArrayList<>();
        orderEntryPosList.forEach(orderEntryPos -> {
                    OrderEntryPosVO orderEntryPosVO = new OrderEntryPosVO();
                    BeanUtils.copyProperties(orderEntryPos, orderEntryPosVO);

                    // 商品列表
                    Map<String, Object> map = new HashMap<>();
                    map.put("orderId", orderEntryPosVO.getId());
                    List<OrderGoodSnapshot> orderGoodSnapshotList = dataAccessManager.getOrderGoodSnapshotPosByMap(map);
                    List<OrderGoodSnapshotVO> snapshotVos = new ArrayList<>();
                    if (!CollectionUtils.isEmpty(orderGoodSnapshotList)) {
                        orderGoodSnapshotList.forEach(orderGoodSnapshot -> {
                            OrderGoodSnapshotVO orderGoodSnapshotVO = new OrderGoodSnapshotVO();
                            BeanUtils.copyProperties(orderGoodSnapshot, orderGoodSnapshotVO);
                            snapshotVos.add(orderGoodSnapshotVO);
                        });
                    }
                    orderEntryPosVO.setOrderGoodSnapshotVOList(snapshotVos);

                    voList.add(orderEntryPosVO);
                }
        );
        return success(voList, total);

    }

    /***
     * pos门店订单统计（所有营业订单笔数、今日营业订单笔数、累计订单营业额、今日订单营业额    订单笔数增长指标   营业额增长指标 ）
     * @author oy
     * @date 2019/6/6
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getOrderEntryPosTotal() {
        Integer type = getIntParameter("type");//区分 日：0 周：1 月：2
        JSONObject jsonObject = new JSONObject();

        //所有营业订单笔数 累计订单营业额
        int orderType = 16;
        Calendar calendarToday = Calendar.getInstance();
        Date timeToday = calendarToday.getTime();
        String todayTime = DateUtils.format(timeToday, DateUtils.FORMAT_SHORT); //今天
        //如果不传今天，那么就是查询所有的天
        //所有的
        Map<String,Object> orderTotalnum = dataAccessManager.getOrderEntrysPostForTotal(orderType,null);

        //今天
        Map<String,Object> orderTodaynum = dataAccessManager.getOrderEntrysPostForTotal(orderType,todayTime);

        Long orderNumToday = (Long)orderTodaynum.get("orderNum");//今天
        Long orderNumTotal = (Long)orderTotalnum.get("orderNum");//所有的

        //分 -》 元 除法用
        BigDecimal decimal1 = new BigDecimal("100");

        BigDecimal priceTotalToday = (BigDecimal)orderTodaynum.get("priceTotal");

        if(priceTotalToday == null){
            priceTotalToday = BigDecimal.ZERO;
        }else{
            priceTotalToday = priceTotalToday.divide(decimal1, 2, BigDecimal.ROUND_HALF_DOWN);
        }

        BigDecimal priceTotal = (BigDecimal)orderTotalnum.get("priceTotal");
        if(priceTotal == null){
            priceTotal = BigDecimal.ZERO;
        }else{
            priceTotal = priceTotal.divide(decimal1, 2, BigDecimal.ROUND_HALF_DOWN);
        }

        //计算按日 周 月

        //今日新增门店数
        String startTime = getUTF("startTime", null);
        String endTime = getUTF("endTime", null);

        //把每个map 按照顺序给转成两个list
        List<String> dateArry = new ArrayList<>();// 时间
        List<BigDecimal> numArry = new ArrayList<>();// 数量
        List<BigDecimal> moneyArry = new ArrayList<>();//金额

        if (type == 0) {//日
            if (startTime == null && endTime == null) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, -1);
                Date time = calendar.getTime();
                endTime = DateUtils.format(time, DateUtils.FORMAT_SHORT); //1天前

                Calendar calendar1 = Calendar.getInstance();
                calendar1.add(Calendar.DATE, -12);
                Date time1 = calendar1.getTime();
                startTime = DateUtils.format(time1, DateUtils.FORMAT_SHORT); //12天前
            }
            //算出当前时间内的每一天的数量
            List<OrderEntrysTotalVO>  shopDetailNumForDay = dataAccessManager.getOrderEntrysByTypeCountForEveryDay(orderType, startTime, endTime);
            if (shopDetailNumForDay != null && !shopDetailNumForDay.isEmpty()) {
                shopDetailNumForDay.forEach(x -> {
                    //金钱处理 分 -》 元
                    dateArry.add(x.getDate());
                    numArry.add(x.getNum());
                    BigDecimal money = x.getMoney();
                    BigDecimal divide = money.divide(decimal1, 2, BigDecimal.ROUND_HALF_DOWN);
                    moneyArry.add(divide);
                });
            }
        } else if (type == 1) {//周
            if (startTime == null && endTime == null) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -3);
                Date time = calendar.getTime();
                startTime = DateUtils.format(time, DateUtils.FORMAT_SHORT); //三个月
                endTime = DateUtils.format(new Date(), DateUtils.FORMAT_SHORT);//今天
            }
            //统计三个月
            List<OrderEntrysTotalVO> shopDetailNumForWeek = dataAccessManager.getOrderEntrysByTypeCountForEveryWeek(orderType,startTime,endTime);
            if (shopDetailNumForWeek != null && !shopDetailNumForWeek.isEmpty()) {
                shopDetailNumForWeek.forEach(x->{
                    dateArry.add(x.getDate());
                    numArry.add(x.getNum());
                    BigDecimal money = x.getMoney();
                    BigDecimal divide = money.divide(decimal1, 2, BigDecimal.ROUND_HALF_DOWN);
                    moneyArry.add(divide);
                });
            }

        } else if (type == 2) {//月
            if (startTime == null && endTime == null) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -12);
                Date time = calendar.getTime();
                startTime = DateUtils.format(time, DateUtils.FORMAT_SHORTS); //十二个月
                endTime = DateUtils.format(new Date(), DateUtils.FORMAT_SHORTS);//今天
            }
            //统计三个月
            List<OrderEntrysTotalVO>  shopDetailNumForMonth = dataAccessManager.getOrderEntrysByTypeCountForEveryMonth(orderType,startTime,endTime);
            if (shopDetailNumForMonth != null && !shopDetailNumForMonth.isEmpty()) {
                shopDetailNumForMonth.forEach(x->{
                    dateArry.add(x.getDate());
                    numArry.add(x.getNum());
                    BigDecimal money = x.getMoney();
                    BigDecimal divide = money.divide(decimal1, 2, BigDecimal.ROUND_HALF_DOWN);
                    moneyArry.add(divide);
                });
            }
        }

        jsonObject.put("priceTotalToday",priceTotalToday);//当天总金额
        jsonObject.put("priceTotal",priceTotal);//总金额

        jsonObject.put("orderTotalnum",orderNumTotal);//订单总数
        jsonObject.put("orderTodaynum",orderNumToday);//订单当天数量

        jsonObject.put("dateArry",dateArry);
        jsonObject.put("numArry",numArry);
        jsonObject.put("moneyArry",moneyArry);

        return success(jsonObject);
    }

}
