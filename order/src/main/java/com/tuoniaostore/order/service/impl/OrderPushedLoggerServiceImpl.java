package com.tuoniaostore.order.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.order.OrderPushedLogger;
import com.tuoniaostore.order.cache.OrderDataAccessManager;
import com.tuoniaostore.order.service.OrderPushedLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("orderPushedLoggerService")
public class OrderPushedLoggerServiceImpl extends BasicWebService implements OrderPushedLoggerService {
    @Autowired
    private OrderDataAccessManager dataAccessManager;

    @Override
    public JSONObject addOrderPushedLogger() {
        OrderPushedLogger orderPushedLogger = new OrderPushedLogger();
        //订单ID
        String orderId = getUTF("orderId");
        //目标ID
        String targetUserId = getUTF("targetUserId");
        //推送类型
        Integer pushType = getIntParameter("pushType", 0);
        //推送的标题
        String pushTitle = getUTF("pushTitle", null);
        //推送的内容
        String pushMsgContent = getUTF("pushMsgContent", null);
        //推送结果内容(即时)
        String pushResult = getUTF("pushResult", null);

        //推送状态(我方)
        Integer status = getIntParameter("status", 0);
        //返回消息内容
        String feedbackMsgContent = getUTF("feedbackMsgContent", null);
        //返回的推送状态
        Integer feedbackStatus = getIntParameter("feedbackStatus", 0);
        orderPushedLogger.setOrderId(orderId);
        orderPushedLogger.setTargetUserId(targetUserId);
        orderPushedLogger.setPushType(pushType);
        orderPushedLogger.setPushTitle(pushTitle);
        orderPushedLogger.setPushMsgContent(pushMsgContent);
        orderPushedLogger.setPushResult(pushResult);
        orderPushedLogger.setStatus(status);
        orderPushedLogger.setFeedbackMsgContent(feedbackMsgContent);
        orderPushedLogger.setFeedbackStatus(feedbackStatus);
        dataAccessManager.addOrderPushedLogger(orderPushedLogger);
        return success();
    }

    @Override
    public JSONObject getOrderPushedLogger() {
        OrderPushedLogger orderPushedLogger = dataAccessManager.getOrderPushedLogger(getId());
        return success(orderPushedLogger);
    }

    @Override
    public JSONObject getOrderPushedLoggers() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<OrderPushedLogger> orderPushedLoggers = dataAccessManager.getOrderPushedLoggers(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getOrderPushedLoggerCount(status, name);
        return success(orderPushedLoggers, count);
    }

    @Override
    public JSONObject getOrderPushedLoggerAll() {
        List<OrderPushedLogger> orderPushedLoggers = dataAccessManager.getOrderPushedLoggerAll();
        return success(orderPushedLoggers);
    }

    @Override
    public JSONObject getOrderPushedLoggerCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getOrderPushedLoggerCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeOrderPushedLogger() {
        OrderPushedLogger orderPushedLogger = dataAccessManager.getOrderPushedLogger(getId());
        //订单ID
        String orderId = getUTF("orderId");
        //目标ID
        String targetUserId = getUTF("targetUserId");
        //推送类型
        Integer pushType = getIntParameter("pushType", 0);
        //推送的标题
        String pushTitle = getUTF("pushTitle", null);
        //推送的内容
        String pushMsgContent = getUTF("pushMsgContent", null);
        //推送结果内容(即时)
        String pushResult = getUTF("pushResult", null);

        //推送状态(我方)
        Integer status = getIntParameter("status", 0);
        //返回消息内容
        String feedbackMsgContent = getUTF("feedbackMsgContent", null);
        //返回的推送状态
        Integer feedbackStatus = getIntParameter("feedbackStatus", 0);

        orderPushedLogger.setOrderId(orderId);
        orderPushedLogger.setTargetUserId(targetUserId);
        orderPushedLogger.setPushType(pushType);
        orderPushedLogger.setPushTitle(pushTitle);
        orderPushedLogger.setPushMsgContent(pushMsgContent);
        orderPushedLogger.setPushResult(pushResult);
        orderPushedLogger.setStatus(status);
        orderPushedLogger.setFeedbackMsgContent(feedbackMsgContent);
        orderPushedLogger.setFeedbackStatus(feedbackStatus);
        dataAccessManager.changeOrderPushedLogger(orderPushedLogger);
        return success();
    }

}
