package com.tuoniaostore.order.service;

import com.alibaba.fastjson.JSONObject;

public interface OrderRefundService {

    JSONObject orderRefundApply();

    JSONObject orderRefundList();

    JSONObject orderRefundAgree();

    JSONObject orderRefundReject();

    JSONObject orderRefundEntryDetail();

    JSONObject orderRefundGoodSnapshotList();
}
