package com.tuoniaostore.order.service.impl;

import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.datamodel.order.OrderSequence;
import com.tuoniaostore.order.cache.OrderDataAccessManager;
import com.tuoniaostore.order.service.OrderSequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;


@Service("orderSequenceService")
public class OrderSequenceServiceImpl extends BasicWebService implements OrderSequenceService {
    @Autowired
    private OrderDataAccessManager dataAccessManager;

    @Override
    public JSONObject addOrderSequence() {
        OrderSequence orderSequence = new OrderSequence();
        //合作商户号ID
        String partnerId = getUTF("partnerId", null);
        //合作商户下级用户信息
        String partnerUserId = getUTF("partnerUserId", null);
        //预创建订单序列号
        String sequenceNumber = getUTF("sequenceNumber", null);
        //申请的类型
        Integer type = getIntParameter("type", 0);
        //状态 0 - 可用 其他值为不可用
        Integer status = getIntParameter("status", 0);
        //有效期，单位：秒
        Integer validityTermSecond = getIntParameter("validityTermSecond", 0);
        orderSequence.setPartnerId(partnerId);
        orderSequence.setPartnerUserId(partnerUserId);
        orderSequence.setSequenceNumber(sequenceNumber);
        orderSequence.setType(type);
        orderSequence.setStatus(status);
        orderSequence.setValidityTermSecond(validityTermSecond);
        dataAccessManager.addOrderSequence(orderSequence);
        return success();
    }

    @Override
    public JSONObject getOrderSequence() {
        OrderSequence orderSequence = dataAccessManager.getOrderSequence(getId());
        return success(orderSequence);
    }

    @Override
    public JSONObject getOrderSequences() {
        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        List<OrderSequence> orderSequences = dataAccessManager.getOrderSequences(status, getPageStartIndex(getPageSize()), getPageSize(), name);
        int count = dataAccessManager.getOrderSequenceCount(status, name);
        return success(orderSequences, count);
    }

    @Override
    public JSONObject getOrderSequenceAll() {
        List<OrderSequence> orderSequences = dataAccessManager.getOrderSequenceAll();
        return success(orderSequences);
    }

    @Override
    public JSONObject getOrderSequenceCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int count = dataAccessManager.getOrderSequenceCount(status, keyWord);
        return success();
    }

    @Override
    public JSONObject changeOrderSequence() {
        OrderSequence orderSequence = dataAccessManager.getOrderSequence(getId());
        //合作商户号ID
        String partnerId = getUTF("partnerId", null);
        //合作商户下级用户信息
        String partnerUserId = getUTF("partnerUserId", null);
        //预创建订单序列号
        String sequenceNumber = getUTF("sequenceNumber", null);
        //申请的类型
        Integer type = getIntParameter("type", 0);
        //状态 0 - 可用 其他值为不可用
        Integer status = getIntParameter("status", 0);
        //有效期，单位：秒
        Integer validityTermSecond = getIntParameter("validityTermSecond", 0);
        orderSequence.setPartnerId(partnerId);
        orderSequence.setPartnerUserId(partnerUserId);
        orderSequence.setSequenceNumber(sequenceNumber);
        orderSequence.setType(type);
        orderSequence.setStatus(status);
        orderSequence.setValidityTermSecond(validityTermSecond);
        dataAccessManager.changeOrderSequence(orderSequence);
        return success();
    }

}
