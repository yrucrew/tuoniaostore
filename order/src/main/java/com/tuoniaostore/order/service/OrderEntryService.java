package com.tuoniaostore.order.service;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.utils.PrintFEUtils;
import com.tuoniaostore.datamodel.order.OrderEntry;

import java.util.List;
import java.util.Map;

/**
 * 订单实体表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public interface OrderEntryService {

    JSONObject addOrderEntry();

    JSONObject getOrderEntry();

    JSONObject getOrderEntrys();

    JSONObject getOrderEntryCount();
    JSONObject getOrderEntryAll();

    JSONObject changeOrderEntry();
    JSONObject getOrderEntrysByShippingUserIdAndType();

    JSONObject getOrderEntrysById();

    /**
     * 确认出货
     * @return
     */
    JSONObject changOrderEntrysShipment();

    JSONObject getBusinessOrderEntrys();

    JSONObject getOrderEntrysByShippingUserIdAndTypeBusiness();
    /**
     * 商家端：订单再次购买-加入购物车
     * @author sqd
     * @date 2019/4/2
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject orderEntryBuyAgain();
    /**
     * *商家端：再次购买下单
     * @author sqd
     * @date 2019/4/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject placeAnOrder();

    /**
     * POS端：购买下单
     * @author sqd
     * @date 2019/4/8
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject placeAnOrderPos();

    /**
     * Pos端订单查询
     * @author sqd
     * @date 2019/4/17
     * @param
     * @return
     */
    JSONObject getOrderEntrysPos();

    /**
     * 微信商家下单、支付回调
     * @author sqd
     * @date 2019/4/20
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject payCallback();

    /**
     * 微信商家下单-确认支付
     * @author sqd
     * @date 2019/4/20
     * @param
     * @return
     */
    JSONObject isPay();

    /**
     * 商家端订单打印
     * @author sqd
     * @date 2019/4/27
     * @param
     * @return
     */
    JSONObject printOrder();
    /**
     * 订单查询
     * @author sqd
     * @date 2019/4/27
     * @param
     * @return
     */
    JSONObject getOrderEntryByOrdenumber();

    /**
     * 订单查询
     * @author sqd
     * @date 2019/4/27
     * @param
     * @return
     */
    JSONObject getOrderEntrysPosMap();

    //JSONObject payPos();

    /**
     * pos订单退款
     * @author sqd
     * @date 2019/5/10
     * @param
     * @return
     */
    JSONObject orderRefund();

    JSONObject getOrderEntrysPosById();

    JSONObject getOrderEntryGruopByPayTtpe();
    /**
     * 退款详情接口
     * @author sqd
     * @date 2019/5/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject refundDetails();

    JSONObject getOrderEntryPosByOrdenumber();
    JSONObject inOrderNumber();
    /**
     * 支付宝回调
     * @author sqd
     * @date 2019/5/17
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject zfbPosPayCallback ();

    /**
     *
     * @author sqd
     * @date 2019/5/17
     * @param
     * @return
     */
    JSONObject shopPerformanceStatistic();

    /**
     * 获取昨日订单
     * @author oy
     * @date 2019/5/21
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject getOrderEntryForYesterDay();

    JSONObject  getGoodTypeStatistics();

    /**
     * 查询退款金额
     * @author sqd
     * @date 2019/5/24
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    JSONObject  getretudGood();
    JSONObject  getOrderEntryReturn();

    JSONObject  getOrderEntryPosByMaplog();


    JSONObject  getSmallProgramShopWxPay();

    /**
     * 获取采购订单
     * @return
     */
    JSONObject getOrderEntryListByPos();

    /**
     * 获取采购订单详情
     */
    JSONObject getOrderEntryByPos();

    JSONObject confirmTake();
    JSONObject nonPaymentOrder();

    JSONObject getOrderEntryAndPosById();

    JSONObject getShiftRecordsdetail();

    JSONObject getRefundOrder();

    JSONObject getOrderEntryByOrderNumberAll();

    //商家端小程序报表
    JSONObject getBusinessSideAppletReports();

    JSONObject addPrinter();

}
