package com.tuoniaostore.order.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.response.AlipayTradePayResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.StatusEnum;
import com.tuoniaostore.commons.constant.order.*;
import com.tuoniaostore.commons.constant.pay.WeiXinTerminalPayTypeEnum;
import com.tuoniaostore.commons.constant.pay.WeixinCallcackType;
import com.tuoniaostore.commons.constant.payment.*;
import com.tuoniaostore.commons.lbs.SimpleLocationUtils;
import com.tuoniaostore.commons.support.community.SysUserAddressRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRemoteService;
import com.tuoniaostore.commons.support.community.SysUserRmoteService;
import com.tuoniaostore.commons.support.good.*;
import com.tuoniaostore.commons.support.payment.*;
import com.tuoniaostore.commons.support.supplychain.*;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.commons.utils.PrintFormat;
import com.tuoniaostore.commons.utils.WeiXinPayUtils;
import com.tuoniaostore.commons.utils.ZhiFuBaoPayUtils;
import com.tuoniaostore.datamodel.good.*;
import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.order.OrderStateLogger;
import com.tuoniaostore.datamodel.payment.*;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopBindRelationship;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseSupplierSkuRelationship;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.user.SysUserAddress;
import com.tuoniaostore.datamodel.vo.good.GoodIdAndGoodNumberVO;
import com.tuoniaostore.datamodel.vo.good.GoodOutVO;
import com.tuoniaostore.datamodel.vo.good.GoodTypeOne;
import com.tuoniaostore.datamodel.vo.order.*;
import com.tuoniaostore.datamodel.vo.payment.PayLogVo;
import com.tuoniaostore.datamodel.vo.payment.PaymentTransactionLoggerDateFormat;
import com.tuoniaostore.datamodel.vo.supplychain.SupplychainGoodAndShopVO;
import com.tuoniaostore.datamodel.vo.supplychain.SupplychainGoodVo;
import com.tuoniaostore.order.OrderAop;
import com.tuoniaostore.order.VO.*;
import com.tuoniaostore.order.VO.OrderEntryPosVO;
import com.tuoniaostore.order.cache.OrderDataAccessManager;
import com.tuoniaostore.order.service.OrderEntryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@Service("orderEntryService")
public class OrderEntryServiceImpl extends BasicWebService implements OrderEntryService {

    private static final Logger logger = LoggerFactory.getLogger(OrderAop.class);

    @Autowired
    private OrderDataAccessManager dataAccessManager;

    @Override
    public JSONObject addOrderEntry() {
        OrderEntry orderEntry = new OrderEntry();
        //序列号(对应于购物车)
        String sequenceNumber = getUTF("sequenceNumber", null);
        //订单的序列号(用于展示)
        String orderSequenceNumber = getUTF("orderSequenceNumber", null);
        //下游商户服务号ID
        String partnerId = getUTF("partnerId", null);
        //下单用户ID 匿名时可为0或空
        String partnerUserId = getUTF("partnerUserId", null);
        //订单类型 1-供应链销售订单、2-供应链调拨订单、3-供应链采购订单、4-超市POS机订单（包括超市扫码订单）、7-无人便利店订单、9-增值服务订单、10-无人货架订单、11-向1号生活扫码支付（公司收款）、12-1号生活加盟店（收加盟费）
        Integer type = getIntParameter("type", 0);
        //订单状态 不要占用0  1-创建订单（未确认）、2-已取消、8-已支付、16-已接单、32-发货中、64-配送中、128-已送达、256-确认收货/已完成、260-***确认收货/已完成、512-已收款、1024-分批配送、2048-拒绝订单。
        Integer status = getIntParameter("status", 0);
        //发货状态：0-未发货、32-发货中、64-配送中、128-已送达
        Integer deliverStatus = getIntParameter("deliverStatus", 0);
        //退款状态 0表示未退货 同时可用于记录是否已打款状态
        Integer refundStatus = getIntParameter("refundStatus", 0);
        //支付类型 参考逻辑设定 -1未支付
        Integer paymentType = getIntParameter("paymentType", 0);
        //交易类型，微信系：JSAPI、NATIVE、APP等
        String transType = getUTF("transType", null);
        //用户来源
        Integer userSource = getIntParameter("userSource", 0);
        //当天排号 相对于发货方
        Integer daySortNumber = getIntParameter("daySortNumber", 0);
        //推送类型，0表示不推送 1时表示下单时推送 2时表示支付后推送
        Integer pushType = getIntParameter("pushType", 0);
        //实际销售的用户ID（销售仓）
        String saleUserId = getUTF("saleUserId");
        //发货用户ID
        String shippingUserId = getUTF("shippingUserId");
        //发货用户昵称
        String shippingNickName = getUTF("shippingNickName", null);
        //发货省份
        String shippingProvince = getUTF("shippingProvince", null);
        //发货城市
        String shippingCity = getUTF("shippingCity", null);
        //发货区域
        String shippingDistrict = getUTF("shippingDistrict", null);
        //发货详细地址(尽量不要包含省市区)
        String shippingAddress = getUTF("shippingAddress", null);
        //latitude,longitude
        String shippingLocation = getUTF("shippingLocation", null);
        //发货人联系号码
        String shippingPhone = getUTF("shippingPhone", null);
        //收货人用户ID
        String receiptUserId = getUTF("receiptUserId", null);
        //收货人昵称
        String receiptNickName = getUTF("receiptNickName", null);
        //收货省份
        String receiptProvince = getUTF("receiptProvince", null);
        //收货城市
        String receiptCity = getUTF("receiptCity", null);
        //收货区域
        String receiptDistrict = getUTF("receiptDistrict", null);
        //收货具体地址(尽量不要包含省市区)
        String receiptAddress = getUTF("receiptAddress", null);
        //收货人电话
        String receiptPhone = getUTF("receiptPhone", null);
        //latitude,longitude
        String receiptLocation = getUTF("receiptLocation", null);
        //配送员用户ID
        String courierPassportId = getUTF("courierPassportId", "0");
        //配送员昵称
        String courierNickName = getUTF("courierNickName", null);
        //配送员联系号码
        String courierPhone = getUTF("courierPhone", null);
        //订单总距离
        Long totalDistance = getLongParameter("totalDistance", 0);
        //下单时所在地理位置
        String currentLocation = getUTF("currentLocation", null);
        //是否需要配送员代收费用 0表示不需要
        Integer collectingFees = getIntParameter("collectingFees", 0);
        //订单描述
        String remark = getUTF("remark", null);
        //实收费用
        Long actualPrice = getLongParameter("actualPrice", 0);
        //订单总费用
        Long totalPrice = getLongParameter("totalPrice", 0);
        //优惠的费用
        Long discountPrice = getLongParameter("discountPrice", 0);
        //配送费用
        Long distributionFee = getLongParameter("distributionFee", 0);
        //价格记录 如优惠的政策
        String priceLogger = getUTF("priceLogger", null);
        //税率(万分比)
        Long taxRate = getLongParameter("taxRate", 0);
        //取消的理由
        String cancelLogger = getUTF("cancelLogger", null);
        //商品或支付单简要描述
        String body = getUTF("body", null);
        //商品名称明细列表(仅记录体系外的数据)
        String detail = getUTF("detail", null);
        //信用付费用
        String creditFee = getUTF("creditFee", null);
        //是否使用第三方物流 0否 1是
        Integer thirdPartyDelivery = getIntParameter("thirdPartyDelivery", 0);
        //会员价(分)
        Long membershipPrice = getLongParameter("membershipPrice", 0);
        //第三方物流的状态 1-待推,2-全部推完,3-冲红
        Integer thirdPartyDeliveryStatus = getIntParameter("thirdPartyDeliveryStatus", 0);
        //是否为加盟店 1为是
        Integer isJoin = getIntParameter("isJoin", 0);
        //是否为第三方 0为平台 不等于0为第三方
        Integer isThirdParty = getIntParameter("isThirdParty", 0);
        //复核员ID
        Long reviewPassportId = getLongParameter("reviewPassportId", 0);
        orderEntry.setSequenceNumber(sequenceNumber);
        orderEntry.setOrderSequenceNumber(orderSequenceNumber);
        orderEntry.setPartnerId(partnerId);
        orderEntry.setPartnerUserId(partnerUserId);
        orderEntry.setType(type);
        orderEntry.setStatus(status);
        orderEntry.setDeliverStatus(deliverStatus);
        orderEntry.setRefundStatus(refundStatus);
        orderEntry.setPaymentType(paymentType);
        orderEntry.setTransType(transType);
        orderEntry.setUserSource(userSource);
        orderEntry.setDaySortNumber(daySortNumber);
        orderEntry.setPushType(pushType);
        orderEntry.setSaleUserId(saleUserId);
        orderEntry.setShippingUserId(shippingUserId);
        orderEntry.setShippingNickName(shippingNickName);
        orderEntry.setShippingProvince(shippingProvince);
        orderEntry.setShippingCity(shippingCity);
        orderEntry.setShippingDistrict(shippingDistrict);
        orderEntry.setShippingAddress(shippingAddress);
        orderEntry.setShippingLocation(shippingLocation);
        orderEntry.setShippingPhone(shippingPhone);
        orderEntry.setReceiptUserId(receiptUserId);
        orderEntry.setReceiptNickName(receiptNickName);
        orderEntry.setReceiptProvince(receiptProvince);
        orderEntry.setReceiptCity(receiptCity);
        orderEntry.setReceiptDistrict(receiptDistrict);
        orderEntry.setReceiptAddress(receiptAddress);
        orderEntry.setReceiptPhone(receiptPhone);
        orderEntry.setReceiptLocation(receiptLocation);
        orderEntry.setCourierPassportId(courierPassportId);
        orderEntry.setCourierNickName(courierNickName);
        orderEntry.setCourierPhone(courierPhone);
        orderEntry.setTotalDistance(totalDistance);
        orderEntry.setCurrentLocation(currentLocation);
        orderEntry.setCollectingFees(collectingFees);
        orderEntry.setRemark(remark);
        orderEntry.setActualPrice(actualPrice);
        orderEntry.setTotalPrice(totalPrice);
        orderEntry.setDiscountPrice(discountPrice);
        orderEntry.setDistributionFee(distributionFee);
        orderEntry.setPriceLogger(priceLogger);
        orderEntry.setTaxRate(taxRate);
        orderEntry.setCancelLogger(cancelLogger);
        orderEntry.setBody(body);
        orderEntry.setDetail(detail);
        orderEntry.setCreditFee(creditFee);
        orderEntry.setThirdPartyDelivery(thirdPartyDelivery);
        orderEntry.setMembershipPrice(membershipPrice);
        orderEntry.setThirdPartyDeliveryStatus(thirdPartyDeliveryStatus);
        orderEntry.setIsJoin(isJoin);
        orderEntry.setIsThirdParty(isThirdParty);
        dataAccessManager.addOrderEntry(orderEntry);
        return success();
    }

    @Override
    public JSONObject getOrderEntry() {
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(getId());
        return success(orderEntry);
    }

    @Override
    public JSONObject getOrderEntrys() {
        HashMap<String, Object> map = new HashMap<>();

        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        int type = getIntParameter("type", -1);
        String typesStr = getUTF("types", null);
        int startActualPrice = getIntParameter("startActualPrice", -1);
        int endActualPrice = getIntParameter("endActualPrice", -1);
        int startTotalPrice = getIntParameter("startTotalPrice", -1);
        int endTotalPrice = getIntParameter("endTotalPrice", -1);
        int paymentType = getIntParameter("paymentType", -1);
        String orderSequenceNumber = getUTF("orderSequenceNumber", null);
        String sequenceNumber = getUTF("sequenceNumber", null);
        String shippingNickNameOrPhone = getUTF("shippingNickNameOrPhone", null);
        String receiptNickNameOrPhone = getUTF("receiptNickNameOrPhone", null);
        String shopName = getUTF("shopName", null);

        String createStartTime = getUTF("createStartTime", null); // 2019-06-04
        String createEndTime = getUTF("createEndTime", null);// 2019-06-05

        /*if(shopName != null && !shopName.equals("")){
            shopName
            map.put("saleUserId",saleUserId);
        }*/
        if (typesStr != null) {
            String[] types = typesStr.split(",");
            map.put("types", types);
        }
        map.put("type", type);
        map.put("status", status);
        map.put("pageIndex", getPageStartIndex(getPageSize()));
        map.put("pageSize", getPageSize());
        map.put("name", name);
        map.put("orderSequenceNumber", orderSequenceNumber);
        map.put("sequenceNumbers", sequenceNumber);
        map.put("paymentType", paymentType);
        map.put("shippingNickNameOrPhone", shippingNickNameOrPhone);
        map.put("receiptNickNameOrPhone", receiptNickNameOrPhone);
        map.put("startActualPrice", startActualPrice);
        map.put("endActualPrice", endActualPrice);
        map.put("startTotalPrice", startTotalPrice);
        map.put("endTotalPrice", endTotalPrice);
        map.put("createStartTime", createStartTime);
        map.put("createEndTime", createEndTime);


        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(map);
        int count = 0;
        if (orderEntrys != null && orderEntrys.size() > 0) {
            count = dataAccessManager.getOrderEntryCount(map);
        }
        //获取用户id集合
        if (orderEntrys == null || orderEntrys.size() <= 0) {
            return success(orderEntrys, 0);
        }
        List<String> partnerId = orderEntrys.stream().map(OrderEntry::getPartnerId).collect(Collectors.toList());
        if (partnerId.size() <= 0) {
            return success(orderEntrys, 0);
        }

        List<SysUser> sysUserInIds = SysUserRemoteService.getSysUserInIds(partnerId, getHttpServletRequest());

        orderEntrys.forEach(x -> {
            String id = x.getPartnerId();
            if (sysUserInIds != null && sysUserInIds.size() >= 0) {
                Optional<SysUser> first = sysUserInIds.stream().filter(item -> item.getId().equals(id)).findFirst();
                if (first.isPresent()) {
                    SysUser sysUser = first.get();
                    x.setPartnerUserName(sysUser.getShowName());
                }
            }
            String partnerUserId = x.getPartnerUserId();//店长id
            String saleUserId = x.getSaleUserId();//仓管id

            SupplychainShopProperties partnerUserIdShop = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUid(partnerUserId, getHttpServletRequest());
            if(partnerUserIdShop != null){
                String shopName1 = partnerUserIdShop.getShopName();
                x.setStoreName(shopName1);
            }
            SupplychainShopProperties saleUserIdShop = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUid(saleUserId, getHttpServletRequest());
            if(saleUserIdShop != null){
                String shopName2 = saleUserIdShop.getShopName();
                x.setWareHourseName(shopName2);
            }
        });
        return success(orderEntrys, count);
    }


    @Override
    public JSONObject getOrderEntryAll() {
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntryAll();
        return success(orderEntrys);
    }

    @Override
    public JSONObject getOrderEntryCount() {
        String keyWord = getUTF("keyWord", null);
        int status = getIntParameter("status", StatusEnum.NORMAL.getKey());
        int type = getIntParameter("type", -1);
        Map<String, Object> map = new HashMap<>();
        map.put("keyWord", keyWord);
        map.put("status", status);
        map.put("type", type);
        int count = dataAccessManager.getOrderEntryCount(map);
        return success();
    }

    @Override
    public JSONObject changeOrderEntry() {
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(getId());
        //序列号(对应于购物车)
        String sequenceNumber = getUTF("sequenceNumber", null);
        //订单的序列号(用于展示)
        String orderSequenceNumber = getUTF("orderSequenceNumber", null);
        //下游商户服务号ID
        String partnerId = getUTF("partnerId", null);
        //下单用户ID 匿名时可为0或空
        String partnerUserId = getUTF("partnerUserId", null);
        //订单类型 1-供应链销售订单、2-供应链调拨订单、3-供应链采购订单、4-超市POS机订单（包括超市扫码订单）、7-无人便利店订单、9-增值服务订单、10-无人货架订单、11-向1号生活扫码支付（公司收款）、12-1号生活加盟店（收加盟费）
        Integer type = getIntParameter("type", 0);
        //订单状态 不要占用0  1-创建订单（未确认）、2-已取消、8-已支付、16-已接单、32-发货中、64-配送中、128-已送达、256-确认收货/已完成、260-***确认收货/已完成、512-已收款、1024-分批配送、2048-拒绝订单。
        Integer status = getIntParameter("status", 0);
        //发货状态：0-未发货、32-发货中、64-配送中、128-已送达
        Integer deliverStatus = getIntParameter("deliverStatus", 0);
        //退款状态 0表示未退货 同时可用于记录是否已打款状态
        Integer refundStatus = getIntParameter("refundStatus", 0);
        //支付类型 参考逻辑设定 -1未支付
        Integer paymentType = getIntParameter("paymentType", 0);
        //交易类型，微信系：JSAPI、NATIVE、APP等
        String transType = getUTF("transType", null);
        //用户来源
        Integer userSource = getIntParameter("userSource", 0);
        //当天排号 相对于发货方
        Integer daySortNumber = getIntParameter("daySortNumber", 0);
        //推送类型，0表示不推送 1时表示下单时推送 2时表示支付后推送
        Integer pushType = getIntParameter("pushType", 0);
        //实际销售的用户ID（销售仓）
        String saleUserId = getUTF("saleUserId");
        //发货用户ID
        String shippingUserId = getUTF("shippingUserId");
        //发货用户昵称
        String shippingNickName = getUTF("shippingNickName", null);
        //发货省份
        String shippingProvince = getUTF("shippingProvince", null);
        //发货城市
        String shippingCity = getUTF("shippingCity", null);
        //发货区域
        String shippingDistrict = getUTF("shippingDistrict", null);
        //发货详细地址(尽量不要包含省市区)
        String shippingAddress = getUTF("shippingAddress", null);
        //latitude,longitude
        String shippingLocation = getUTF("shippingLocation", null);
        //发货人联系号码
        String shippingPhone = getUTF("shippingPhone", null);
        //收货人用户ID
        String receiptUserId = getUTF("receiptUserId", null);
        //收货人昵称
        String receiptNickName = getUTF("receiptNickName", null);
        //收货省份
        String receiptProvince = getUTF("receiptProvince", null);
        //收货城市
        String receiptCity = getUTF("receiptCity", null);
        //收货区域
        String receiptDistrict = getUTF("receiptDistrict", null);
        //收货具体地址(尽量不要包含省市区)
        String receiptAddress = getUTF("receiptAddress", null);
        //收货人电话
        String receiptPhone = getUTF("receiptPhone", null);
        //latitude,longitude
        String receiptLocation = getUTF("receiptLocation", null);
        //配送员用户ID
        String courierUserId = getUTF("courierUserId", "0");
        //配送员昵称
        String courierNickName = getUTF("courierNickName", null);
        //配送员联系号码
        String courierPhone = getUTF("courierPhone", null);
        //订单总距离
        Long totalDistance = getLongParameter("totalDistance", 0);
        //下单时所在地理位置
        String currentLocation = getUTF("currentLocation", null);
        //是否需要配送员代收费用 0表示不需要
        Integer collectingFees = getIntParameter("collectingFees", 0);
        //订单描述
        String remark = getUTF("remark", null);
        //实收费用
        Long actualPrice = getLongParameter("actualPrice", 0);
        //订单总费用
        Long totalPrice = getLongParameter("totalPrice", 0);
        //优惠的费用
        Long discountPrice = getLongParameter("discountPrice", 0);
        //配送费用
        Long distributionFee = getLongParameter("distributionFee", 0);
        //价格记录 如优惠的政策
        String priceLogger = getUTF("priceLogger", null);
        //税率(万分比)
        Long taxRate = getLongParameter("taxRate", 0);
        //取消的理由
        String cancelLogger = getUTF("cancelLogger", null);
        //商品或支付单简要描述
        String body = getUTF("body", null);
        //商品名称明细列表(仅记录体系外的数据)
        String detail = getUTF("detail", null);
        //信用付费用
        String creditFee = getUTF("creditFee", null);
        //是否使用第三方物流 0否 1是
        Integer thirdPartyDelivery = getIntParameter("thirdPartyDelivery", 0);
        //会员价(分)
        Long membershipPrice = getLongParameter("membershipPrice", 0);
        //第三方物流的状态 1-待推,2-全部推完,3-冲红
        Integer thirdPartyDeliveryStatus = getIntParameter("thirdPartyDeliveryStatus", 0);
        //是否为加盟店 1为是
        Integer isJoin = getIntParameter("isJoin", 0);
        //是否为第三方 0为平台 不等于0为第三方
        Integer isThirdParty = getIntParameter("isThirdParty", 0);
        orderEntry.setSequenceNumber(sequenceNumber);
        orderEntry.setOrderSequenceNumber(orderSequenceNumber);
        orderEntry.setPartnerId(partnerId);
        orderEntry.setPartnerUserId(partnerUserId);
        orderEntry.setType(type);
        orderEntry.setStatus(status);
        orderEntry.setDeliverStatus(deliverStatus);
        orderEntry.setRefundStatus(refundStatus);
        orderEntry.setPaymentType(paymentType);
        orderEntry.setTransType(transType);
        orderEntry.setUserSource(userSource);
        orderEntry.setDaySortNumber(daySortNumber);
        orderEntry.setPushType(pushType);
        orderEntry.setSaleUserId(saleUserId);
        orderEntry.setShippingUserId(shippingUserId);
        orderEntry.setShippingNickName(shippingNickName);
        orderEntry.setShippingProvince(shippingProvince);
        orderEntry.setShippingCity(shippingCity);
        orderEntry.setShippingDistrict(shippingDistrict);
        orderEntry.setShippingAddress(shippingAddress);
        orderEntry.setShippingLocation(shippingLocation);
        orderEntry.setShippingPhone(shippingPhone);
        orderEntry.setReceiptUserId(receiptUserId);
        orderEntry.setReceiptNickName(receiptNickName);
        orderEntry.setReceiptProvince(receiptProvince);
        orderEntry.setReceiptCity(receiptCity);
        orderEntry.setReceiptDistrict(receiptDistrict);
        orderEntry.setReceiptAddress(receiptAddress);
        orderEntry.setReceiptPhone(receiptPhone);
        orderEntry.setReceiptLocation(receiptLocation);
        orderEntry.setCourierPassportId(courierUserId);
        orderEntry.setCourierNickName(courierNickName);
        orderEntry.setCourierPhone(courierPhone);
        orderEntry.setTotalDistance(totalDistance);
        orderEntry.setCurrentLocation(currentLocation);
        orderEntry.setCollectingFees(collectingFees);
        orderEntry.setRemark(remark);
        orderEntry.setActualPrice(actualPrice);
        orderEntry.setTotalPrice(totalPrice);
        orderEntry.setDiscountPrice(discountPrice);
        orderEntry.setDistributionFee(distributionFee);
        orderEntry.setPriceLogger(priceLogger);
        orderEntry.setTaxRate(taxRate);
        orderEntry.setCancelLogger(cancelLogger);
        orderEntry.setBody(body);
        orderEntry.setDetail(detail);
        orderEntry.setCreditFee(creditFee);
        orderEntry.setThirdPartyDelivery(thirdPartyDelivery);
        orderEntry.setMembershipPrice(membershipPrice);
        orderEntry.setThirdPartyDeliveryStatus(thirdPartyDeliveryStatus);
        orderEntry.setIsJoin(isJoin);
        orderEntry.setIsThirdParty(isThirdParty);
        dataAccessManager.changeOrderEntry(orderEntry);
        return success();
    }

    /**
     * 供应商订单查询
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/4/23
     */
    @Override
    public JSONObject getOrderEntrysByShippingUserIdAndType() {
        String status = getUTF("type", null);
        int pageIndex = getIntParameter("pageIndex", 1);
        int pageSize = getIntParameter("pageSize", 10);
        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageIndex - 1);
        int limitEnd = pageSize;
        SysUser sysUser = null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (sysUser == null) {
            return fail(1001,"用户不能为空");
        }
        SysUser user = null;
        try {
            user = SysUserRemoteService.getSysUser(sysUser.getId(), getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String shopUserId = "";
        if (user.getLeaderUserId() != null) {
            shopUserId = user.getLeaderUserId();
        } else {
            shopUserId = user.getId();
        }
        //(1/处理中；0已完成)
        if (status != null && status.equals("1")) {
            status = OrderStatus.ORDER_TYPE_CANCELED.getKey() + "," +
                    OrderStatus.ORDER_TYPE_PAID.getKey() + "," +
                    OrderStatus.ORDER_TYPE_HAVEORDER.getKey() + "," +
                    OrderStatus.ORDER_TYPE_SHIPMENTS.getKey() + "," +
                    OrderStatus.ORDER_TYPE_PARTIALDELIVERY.getKey() + "," +
                    OrderStatus.ORDER_TYPE_DISTRIBUTION.getKey()
            ;
        } else {
            status = OrderStatus.ORDER_TYPE_DELIVERY.getKey() + "," +
                    OrderStatus.ORDER_TYPE_CONFIRMRECEIPT.getKey() + "," +
                    OrderStatus.ORDER_TYPE_STOCKS.getKey() + "," +
                    OrderStatus.ORDER_TYPE_GATHERING.getKey() + "," +
                    OrderStatus.ORDER_TYPE_REFUSEORDER.getKey()
            ;
        }

        int type = OrderTypeEnum.SUPPLYCHAIN.getKey();
        String[] split = status.split(",");
        List<String> strings = Arrays.asList(split);
        List<OrderEntry> orderEntryList = dataAccessManager.getOrderEntrysByShippingUserIdAndType(shopUserId, type, strings, limitStart, limitEnd);
        List<OrderGoodSnapshot> orderGoodSnapshotAll = dataAccessManager.getOrderGoodSnapshotAll();
        List<OrderEntryVO> OrderEntryVOlist = new ArrayList<>();
        orderEntryList.forEach(x -> {
            String id = x.getId();
            List<OrderGoodSnapshot> collect = orderGoodSnapshotAll.stream().filter(item -> item.getOrderId().equals(id)).collect(Collectors.toList());
            OrderEntryVO orderEntryVO = new OrderEntryVO();
            orderEntryVO.setActualPrice(x.getActualPrice());
            orderEntryVO.setTotalPrice(x.getTotalPrice());
            orderEntryVO.setOrderGoodSnapshots(x.getOrderGoodSnapshots());
            orderEntryVO.setCreateTime(x.getCreateTime());
            orderEntryVO.setDaySortNumber(x.getDaySortNumber());
            orderEntryVO.setId(x.getId());
            orderEntryVO.setType(x.getType());
            orderEntryVO.setStatus(x.getStatus());
            orderEntryVO.setReceiptPhone(x.getReceiptPhone());
            orderEntryVO.setOrderSequenceNumber(x.getOrderSequenceNumber());
            orderEntryVO.setReceiptNickPhone(x.getReceiptPhone());
            if (collect != null) {
                orderEntryVO.setOrderGoodSnapshots(collect);
            }
            OrderEntryVOlist.add(orderEntryVO);
        });
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("OrderEntry", OrderEntryVOlist);
        return success(jsonObject);
    }

    @Override
    public JSONObject getOrderEntrysById() {
        String orderId = getUTF("orderId");
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(orderId);
        List<OrderGoodSnapshot> orderGoodSnapshotByOrderId = dataAccessManager.getOrderGoodSnapshotByOrderId(orderId);
        JSONObject jsonObject = new JSONObject();
        orderEntry.setReceiptAddress(orderEntry.getReceiptProvince() + orderEntry.getReceiptCity() + orderEntry.getReceiptDistrict() + orderEntry.getReceiptAddress());
        orderEntry.setShippingAddress(orderEntry.getShippingProvince() + orderEntry.getShippingCity() + orderEntry.getShippingDistrict() + orderEntry.getShippingAddress());

        //计算退款金额
        long refundPrice = 0;
        if(orderGoodSnapshotByOrderId != null){
            for (OrderGoodSnapshot goodSnapshot : orderGoodSnapshotByOrderId) {
                if(goodSnapshot.getReturnPrice() != null){
                    refundPrice += goodSnapshot.getReturnPrice();
                }
            }
        }

        orderEntry.setRefundPrice(refundPrice);
        jsonObject.put("orderEntry", orderEntry);
        jsonObject.put("orderGoodSnapshot", orderGoodSnapshotByOrderId);
        return success(jsonObject);
    }

    /**
     * 后台： pos订单详情
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/11
     */
    @Override
    public JSONObject getOrderEntrysPosById() {
        String orderId = getUTF("orderId");
        HashMap<String, Object> map = new HashMap<>();
        map.put("orderId", orderId);
        List<OrderEntry> orderEntrysPos = dataAccessManager.getOrderEntrysPos(map);
        List<OrderGoodSnapshot> orderGoodSnapshotPosByMap = dataAccessManager.getOrderGoodSnapshotPosByMap(map);
        if(orderEntrysPos == null || orderEntrysPos.size() <= 0){
            return  fail("没有订单数据");
        }
        int goodNumber = 0;
        int returnGoodNumber = 0;
        long returnPrice = 0;
        if (orderGoodSnapshotPosByMap.size() > 0) {
            for (OrderGoodSnapshot orderGoodSnapshot : orderGoodSnapshotPosByMap) {
                if (orderGoodSnapshot.getNormalQuantity() != null) {
                    goodNumber += orderGoodSnapshot.getNormalQuantity();
                }
                /*if (orderGoodSnapshot.getReturnPrice() != null) {
                    returnPrice += orderGoodSnapshot.getReturnPrice();
                }*/
                if (orderGoodSnapshot.getReturnQuantity() != null) {
                    returnGoodNumber += orderGoodSnapshot.getReturnQuantity();
                }
            }
        }


        String returnOperatorUser = "";
        OrderEntry orderEntry = new OrderEntry();
        if (orderEntrysPos.size() > 0) {
            orderEntry = orderEntrysPos.get(0);
            try {
                SysUser sysUser = SysUserRmoteService.getSysUser(orderEntrysPos.get(0).getReturnOperatorUserId(), getHttpServletRequest());
                if (sysUser != null) {
                    returnOperatorUser = sysUser.getDefaultName();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(orderEntrysPos.get(0).getStatus() == OrderStatus.ORDER_TYPE_REFUND.getKey()){
                if(orderEntrysPos.get(0).getFindPrice() != null){
                    returnPrice = orderEntrysPos.get(0).getActualPrice() - orderEntrysPos.get(0).getFindPrice();
                }else{
                    returnPrice = orderEntrysPos.get(0).getActualPrice();
                }
            }
        }


        OrderEntryPos orderEntryPos = new OrderEntryPos();
        BeanUtils.copyProperties(orderEntry, orderEntryPos);
        orderEntryPos.setReturnOperatorUser(returnOperatorUser);
        orderEntryPos.setReturnGoodNumber(returnGoodNumber);
        orderEntryPos.setReturnPrice(returnPrice);
        orderEntryPos.setGoodNumber(goodNumber);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("orderGoodSnapshotPos", orderGoodSnapshotPosByMap);
        jsonObject.put("orderEntrysPos", orderEntryPos);
        return success(jsonObject);
    }


    /**
     * 后台： 退款单详细信息
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/11
     */
    @Override
    public JSONObject refundDetails() {
        String id = getUTF("id");//退款记录id
        String orderSequenceNumber = getUTF("orderSequenceNumber");//退款记录id
        HashMap<String, Object> map = new HashMap<>();
        map.put("orderSequenceNumber", orderSequenceNumber);//订单编号
        PaymentTransactionLogger paymentTransactionLogger = PaymentTransactionLoggerService.getPaymentTransactionLogger(id, getHttpServletRequest());
        List<OrderEntry> orderEntrysPos = dataAccessManager.getOrderEntrysPos(map);
        SysUser sysUser = SysUserRmoteService.getSysUser(paymentTransactionLogger.getPartnerUserId(), getHttpServletRequest());
        if (paymentTransactionLogger == null) {
            return fail("流水信息为空！");
        }
        if (orderEntrysPos.size() <= 0) {
            return fail("订单信息为空！");
        }
        String realName = "";
        if (sysUser != null) {
            realName = sysUser.getRealName();
        }
        long orderPrice = orderEntrysPos.get(0).getActualPrice();
        Date orderPayTime = orderEntrysPos.get(0).getPaymentTime();
        map.put("orderId", orderEntrysPos.get(0).getId());

        //获取商品快照
        map.put("orderSequenceNumber", "");//清楚这个键 值
        List<OrderGoodSnapshot> orderGoodSnapshotPosByMap = dataAccessManager.getOrderGoodSnapshotPosByMap(map);
        String s = JSONObject.toJSONString(orderGoodSnapshotPosByMap);
        List<GoodSnapshotVO> goodSnapshotVOS = JSONObject.parseArray(s, GoodSnapshotVO.class);
        JSONObject jsonObject = new JSONObject();

        //过滤未退款商品
        /*
        if(goodSnapshotVOS.size() > 0 ){
            for (int i = 0; i < goodSnapshotVOS.size(); i++) {
                System.out.println(goodSnapshotVOS.get(i).getReturnQuantity());
                if(goodSnapshotVOS.get(i).getReturnQuantity() == null || goodSnapshotVOS.get(i).getReturnQuantity() <= 0){
                    goodSnapshotVOS.remove(i);
                }
            }
        }*/
        PaymentTransactionLoggerDateFormat paymentTransactionLoggerDateFormat = new PaymentTransactionLoggerDateFormat();
        BeanUtils.copyProperties(paymentTransactionLogger, paymentTransactionLoggerDateFormat);
        jsonObject.put("operator", realName);
        jsonObject.put("orderPrice", orderPrice);
        jsonObject.put("orderPayTime", orderPayTime);
        jsonObject.put("refundLog", paymentTransactionLoggerDateFormat);
        jsonObject.put("goodSnapshotVO", goodSnapshotVOS);
        return success(jsonObject);
    }


    //供应商出货
    @Override
    public JSONObject changOrderEntrysShipment() {
        String orderGoodSnapshot = getUTF("normalQuantityNumber");
        String orderId = getUTF("orderId");
        String orderNumber = getUTF("orderNumber");
        SysUser user = null;
        try {
            user = getUser();//获取当前用户
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (user == null) {
            return fail(1001,"用户不能为空");
        }
        List<OrderGoodSnapshot> orderGoodSnapshotsList = JSONArray.parseArray(orderGoodSnapshot, OrderGoodSnapshot.class);

        for (OrderGoodSnapshot goodSnapshot : orderGoodSnapshotsList) {
            dataAccessManager.changeOrderGoodSnapshotNormalQuantity(goodSnapshot);
        }



        //当前单
        HashMap<String, Object> map = new HashMap<>();
        map.put("orderSequenceNumberOne", orderNumber);
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(map);
        if(orderEntrys == null || orderEntrys.size()<= 0 ){
            return  fail("订单信息有误");
        }
        List<OrderGoodSnapshot> orderGoodSnapshotByOrderId1 = dataAccessManager.getOrderGoodSnapshotByOrderId(orderEntrys.get(0).getId());
        boolean flag = false;
        if(orderGoodSnapshotByOrderId1 != null){
            for (OrderGoodSnapshot goodSnapshot : orderGoodSnapshotByOrderId1) {
                int normalQuantity = 0;
                if(goodSnapshot.getReturnQuantity()!=null){
                    normalQuantity = goodSnapshot.getNormalQuantity() - goodSnapshot.getReturnQuantity();
                }else{
                    normalQuantity = goodSnapshot.getNormalQuantity() ;
                }

                if ((goodSnapshot.getDistributionQuantity() == null) || goodSnapshot.getDistributionQuantity() < normalQuantity) {
                    flag = true;
                    break;
                }
            }
        }
        if(flag){
            orderEntrys.get(0).setDeliverStatus(OrderDeliverStatus.THEDELIVERY.getKey());
            orderEntrys.get(0).setStatus(OrderStatus.ORDER_TYPE_PARTIALDELIVERY.getKey());
        }else{
            orderEntrys.get(0).setDeliverStatus(OrderDeliverStatus.HAVEARRIVED.getKey());
            orderEntrys.get(0).setConfirmTime(new Date());
            orderEntrys.get(0).setStatus(OrderStatus.ORDER_TYPE_STOCKS.getKey());
        }
        dataAccessManager.changeOrderEntryType(orderEntrys.get(0));

        map.clear();
        map.put("orderSequenceNumberOne", orderEntrys.get(0).getSequenceNumber());
        List<OrderEntry> primaryOrderEntrys = dataAccessManager.getOrderEntrys(map);
        if (primaryOrderEntrys == null || primaryOrderEntrys.size() <= 0) {
            return fail("商家订单信息有误");
        }
        OrderEntry  porderEntry = primaryOrderEntrys.get(0);
        List<OrderGoodSnapshot> priorderGoodSnapshotByOrderId = dataAccessManager.getOrderGoodSnapshotByOrderId(porderEntry.getId());

        //赋值商品修改数量
        List<String> ids= orderGoodSnapshotsList.stream().map(OrderGoodSnapshot::getId).collect(Collectors.toList());
        List<OrderGoodSnapshot> orderGoodSnapshotByIds = dataAccessManager.getOrderGoodSnapshotByIds(ids);
        if(orderGoodSnapshotByIds != null && orderGoodSnapshotByIds.size() > 0){
            for (OrderGoodSnapshot orderGoodSnapshotById : orderGoodSnapshotByIds) {
                for (OrderGoodSnapshot goodSnapshot : orderGoodSnapshotsList) {
                    if(orderGoodSnapshotById.getId().equals(goodSnapshot.getId())){
                        orderGoodSnapshotById.setDistributionQuantity(goodSnapshot.getDistributionQuantity());
                    }
                }
            }
        }


        ArrayList<OrderGoodSnapshot> list = new ArrayList<>();
        if(priorderGoodSnapshotByOrderId != null && priorderGoodSnapshotByOrderId.size() > 0){
            for (OrderGoodSnapshot orderGoodSnapsho : orderGoodSnapshotByIds) {
                for (OrderGoodSnapshot goodSnapshot : priorderGoodSnapshotByOrderId) {

                    if(orderGoodSnapsho.getGoodPriceId().equals(goodSnapshot.getGoodPriceId())){
                        OrderGoodSnapshot goodSnapshot2 = new OrderGoodSnapshot();
                        goodSnapshot2.setOrderId(porderEntry.getId());
                        goodSnapshot2.setGoodPriceId(orderGoodSnapsho.getGoodPriceId());
                        goodSnapshot2.setDistributionQuantity(orderGoodSnapsho.getDistributionQuantity());
                        if(goodSnapshot.getDistributionQuantity()!= null && orderGoodSnapsho.getDistributionQuantity() != null){
                            goodSnapshot.setDistributionQuantity(goodSnapshot.getDistributionQuantity()+orderGoodSnapsho.getDistributionQuantity());
                        }
                        list.add(goodSnapshot2);
                    }
                }
            }
        }
        for (OrderGoodSnapshot goodSnapshot : list) {
            dataAccessManager.changePrimaryOrderGoodSnapshotNormalQuantity(goodSnapshot);
        }


        //修改库存后 修改订单状态
        boolean pflag = false;
        if(priorderGoodSnapshotByOrderId != null && priorderGoodSnapshotByOrderId.size() > 0){
            for (OrderGoodSnapshot goodSnapshot1 : priorderGoodSnapshotByOrderId) {
                int pnormalQuantity = 0;
                if(goodSnapshot1.getReturnQuantity()!=null){
                    pnormalQuantity = goodSnapshot1.getNormalQuantity() - goodSnapshot1.getReturnQuantity();
                }else{
                    pnormalQuantity = goodSnapshot1.getNormalQuantity();
                }

                if ((goodSnapshot1.getDistributionQuantity() == null) || goodSnapshot1.getDistributionQuantity() < pnormalQuantity) {
                    pflag = true;
                    break;
                }
            }
        }
        if(pflag){
            //没有配送中 ，直接到待收货 ，添加配送改此处即可 porderEntry.setStatus(OrderStatus.ORDER_TYPE_DISTRIBUTION.getKey());
            porderEntry.setStatus(OrderStatus.ORDER_TYPE_DELIVERY.getKey());
            porderEntry.setDeliverStatus(OrderDeliverStatus.THEDELIVERY.getKey());
        }else{
            //出货完成
            porderEntry.setStatus(OrderStatus.ORDER_TYPE_DELIVERY.getKey());
            porderEntry.setDeliverStatus(OrderDeliverStatus.HAVEARRIVED.getKey());
            porderEntry.setReceivingTime(new Date());//待收货时间
        }

        OrderStateLogger orderStateLogger = new OrderStateLogger();//记录状态
        orderStateLogger.setBeforeStatus(porderEntry.getStatus());//修改前状态
        dataAccessManager.changeOrderEntryType(porderEntry);

        orderStateLogger.setStatus(porderEntry.getType());//修改后状态
        orderStateLogger.setOrderId(orderId);
        orderStateLogger.setShippingUserId(user.getId());
        orderStateLogger.setOrderType(porderEntry.getType());
        orderStateLogger.setOperatorTime(new Date());
        orderStateLogger.setOperatorUserId(user.getRoleId());
        orderStateLogger.setOperatorUserId(user.getRoleId());
        orderStateLogger.setOperatorName(user.getShowName());
        //插入日志
        dataAccessManager.addOrderStateLogger(orderStateLogger);


        /*HashMap<String, Object> map = new HashMap<>();
        map.put("orderSequenceNumberOne", orderNumber);
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(map);
        if (orderEntrys == null || orderEntrys.size() <= 0) {
            return fail("供应商订单信息有误");
        }
        map.clear();
        map.put("sequenceNumbers", orderEntrys.get(0).getSequenceNumber());
        List<OrderEntry> priOrderEntrys = dataAccessManager.getOrderEntrys(map);
        if (priOrderEntrys == null || priOrderEntrys.size() < 0) {
            return fail("仓库订单信息有误");
        }
        map.clear();
        map.put("orderSequenceNumberOne", orderEntrys.get(0).getSequenceNumber());
        List<OrderEntry> primaryOrderEntrys = dataAccessManager.getOrderEntrys(map);
        if (primaryOrderEntrys == null || primaryOrderEntrys.size() <= 0) {
            return fail("商家订单信息有误");
        }

        List<OrderGoodSnapshot> byOrderInIds = null;
        String[] split = orderGoodSnapshotId.split(",");
        List<String> orderIds = Arrays.asList(split);
        if (orderIds.size() > 0) {
            byOrderInIds = dataAccessManager.getOrderGoodSnapshotByIds(orderIds);
        }
        List<OrderGoodSnapshot> orderGoodSnapshotsList2 = new ArrayList<>();
        for (int i = 0; i < byOrderInIds.size(); i++) { //快照集合
            for (int j = 0; j < orderGoodSnapshotsList.size(); j++) {//json 集合
                if (byOrderInIds.get(i).getId().equals(orderGoodSnapshotsList.get(j).getId())) {
                    if (byOrderInIds.get(i).getDistributionQuantity() != null && orderGoodSnapshotsList.get(j).getDistributionQuantity() != null && ((byOrderInIds.get(i).getDistributionQuantity() + orderGoodSnapshotsList.get(j).getDistributionQuantity()) > byOrderInIds.get(i).getNormalQuantity())) {
                        return fail("参数错误，配送值超出");
                    } else {
                        for (OrderEntry priOrderEntry : priOrderEntrys) {
                            OrderGoodSnapshot goodSnapshot = new OrderGoodSnapshot();
                            goodSnapshot.setOrderId(priOrderEntrys.get(0).getId());
                            goodSnapshot.setGoodPriceId(byOrderInIds.get(i).getGoodPriceId());
                            goodSnapshot.setDistributionQuantity(orderGoodSnapshotsList.get(j).getDistributionQuantity());
                            orderGoodSnapshotsList2.add(goodSnapshot);
                        }

                        //主单单独添加
                        OrderGoodSnapshot goodSnapshot3 = new OrderGoodSnapshot();
                        goodSnapshot3.setOrderId(primaryOrderEntrys.get(0).getId());
                        goodSnapshot3.setGoodPriceId(byOrderInIds.get(i).getGoodPriceId());
                        goodSnapshot3.setDistributionQuantity(orderGoodSnapshotsList.get(j).getDistributionQuantity());
                        orderGoodSnapshotsList2.add(goodSnapshot3);
                    }
                }
            }
        }

        for (OrderGoodSnapshot goodSnapshot : orderGoodSnapshotsList2) {
            dataAccessManager.changePrimaryOrderGoodSnapshotNormalQuantity(goodSnapshot);
        }
        if (orderIds.size() > 0) {
            byOrderInIds = dataAccessManager.getOrderGoodSnapshotByIds(orderIds);
        }
        boolean flag = true;
        if (byOrderInIds != null && byOrderInIds.size() > 0) {
            for (OrderGoodSnapshot byOrderInId : byOrderInIds) {
                if ((byOrderInId.getDistributionQuantity() == null) || byOrderInId.getDistributionQuantity() < byOrderInId.getNormalQuantity()) {
                    flag = false;
                    break;
                }
            }
        }

        OrderStateLogger orderStateLogger = new OrderStateLogger();
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(orderId);
        orderStateLogger.setBeforeStatus(orderEntry.getType());//修改前状态
        if (flag) {
            primaryOrderEntrys.get(0).setStatus(OrderStatus.ORDER_TYPE_DELIVERY.getKey());//商家单 已送达
        } else {
            primaryOrderEntrys.get(0).setStatus(OrderStatus.ORDER_TYPE_DISTRIBUTION.getKey());//商家单 配送中
        }

        //修改所有子订单 供应商单
        for (OrderEntry priOrderEntry : priOrderEntrys) {
            boolean flag1 = true;
            List<OrderGoodSnapshot> orderGoodSnapshotByOrderId = dataAccessManager.getOrderGoodSnapshotByOrderId(priOrderEntry.getId());
            for (OrderGoodSnapshot byOrderInId : orderGoodSnapshotByOrderId) {
                if ((byOrderInId.getDistributionQuantity() == null) || byOrderInId.getDistributionQuantity() < byOrderInId.getNormalQuantity()) {
                    flag = false;
                    break;
                }
            }
            if(flag1){
                priOrderEntry.setStatus(OrderStatus.ORDER_TYPE_STOCKS.getKey());
            }else{
                priOrderEntry.setStatus(OrderStatus.ORDER_TYPE_PARTIALDELIVERY.getKey());
            }
            dataAccessManager.changeOrderEntryType(priOrderEntry);
        }*/


        return success();
    }

    //确认收货
    @Override
    public JSONObject confirmTake() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 订单id
        String id = getUTF("id");
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(id);
        if (orderEntry == null) {
            return fail("获取订单数据失败");
        }
        if (orderEntry.getRefundStatus() == OrderRefundStatusEnum.REFUNDING.getKey()) {
            return fail("该订单处于退货退款状况，请联系管理员处理");
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("sequenceNumbers", orderEntry.getOrderSequenceNumber());
        //查询主单下子单
        List<OrderEntry> priOrderEntrys = dataAccessManager.getOrderEntrys(map);
        if (orderEntry == null) {
            return fail("订单信息有误");

        }
        if (priOrderEntrys == null) {
            return fail("订单信息有误");
        }
        orderEntry.setStatus(OrderStatus.ORDER_TYPE_CONFIRMRECEIPT.getKey());
        orderEntry.setDeliverStatus(OrderDeliverStatus.HAVEARRIVED.getKey());
        orderEntry.setConfirmTime(new Date());
        dataAccessManager.changeOrderEntryType(orderEntry);

        List<String> orderId = priOrderEntrys.stream().map(OrderEntry::getId).collect(Collectors.toList());
        orderId.add(orderEntry.getId());
        List<OrderGoodSnapshot> orderGoodSnapshotByOrderInIds = dataAccessManager.getOrderGoodSnapshotByOrderInIds(orderId);

        if (orderGoodSnapshotByOrderInIds != null && orderGoodSnapshotByOrderInIds.size() > 0) {
            for (OrderGoodSnapshot orderGoodSnapshots : orderGoodSnapshotByOrderInIds) {
                if(orderGoodSnapshots.getReturnQuantity() != null){
                    orderGoodSnapshots.setArriveQuantity(orderGoodSnapshots.getNormalQuantity()-orderGoodSnapshots.getReturnQuantity());//送达的数量
                    orderGoodSnapshots.setReceiptQuantity(orderGoodSnapshots.getNormalQuantity()-orderGoodSnapshots.getReturnQuantity());//被确认收货的数量
                }else{
                    orderGoodSnapshots.setArriveQuantity(orderGoodSnapshots.getNormalQuantity());//送达的数量
                    orderGoodSnapshots.setReceiptQuantity(orderGoodSnapshots.getNormalQuantity());//被确认收货的数量
                }

                orderGoodSnapshots.setDistributionQuantity(0);
                dataAccessManager.changeOrderGoodSnapshot(orderGoodSnapshots);
            }
        }


        OrderStateLogger orderStateLogger = new OrderStateLogger();
        orderStateLogger.setBeforeStatus(OrderStatus.ORDER_TYPE_DELIVERY.getKey());//修改前状态
        orderStateLogger.setStatus(orderEntry.getStatus());//修改后状态
        orderStateLogger.setOrderId(orderEntry.getId());
        orderStateLogger.setShippingUserId(user.getId());
        orderStateLogger.setOrderType(orderEntry.getType());
        orderStateLogger.setOperatorTime(new Date());
        orderStateLogger.setOperatorUserId(user.getRoleId());
        orderStateLogger.setOperatorUserId(user.getRoleId());
        orderStateLogger.setOperatorName(user.getShowName());
        //插入日志
        dataAccessManager.addOrderStateLogger(orderStateLogger);




        try {
            //添加账期
            for (OrderEntry entry : priOrderEntrys) {
                //计算订单金额
                long orderPrice = getOrderPrice(entry);

                //查询用账期设置
                PaymentEstimateRevenue paymentEstimateRevenue = new PaymentEstimateRevenue();
                //支付成功添加
                PaymentSettlementSet paymentSettlementSet = PaymentSettlementSetService.getPaymentSettlementSetByUserId(entry.getSaleUserId());

                if(paymentSettlementSet != null){
                    //***添加账期
                    paymentEstimateRevenue.setStatus(PaymentEstimateRevenueStatusEnum.INITIATE_WITHDRAWAL.getKey());
                    if(paymentSettlementSet.getType().equals(1)){ //扣点
                        orderPrice = orderPrice * (10000 - paymentSettlementSet.getCablePoint()) / 10000;//向下取整

                    }else if(paymentSettlementSet.getType().equals(2)){//反点
                        orderPrice = orderPrice * (10000 + paymentSettlementSet.getCablePoint()) / 10000;//向下取整
                    }
                    paymentEstimateRevenue.setActualIncome(orderPrice);// 实际收入
                }else{
                    //***直接到账
                    paymentEstimateRevenue.setStatus(PaymentEstimateRevenueStatusEnum.COMPLETED_WITHDRAWAL.getKey());
                    paymentEstimateRevenue.setActualIncome(entry.getActualPrice());// 实际收入
                    //获取用户当前额度
                    PaymentCurrencyAccount paymentCurrencyAccountById = PaymentCurrencyAccountService.getPaymentCurrencyAccountById(entry.getSaleUserId());
                    if(paymentCurrencyAccountById != null){
                        PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger = new PaymentCurrencyOffsetLogger();
                        paymentCurrencyOffsetLogger.setUserId(entry.getSaleUserId());
                        paymentCurrencyOffsetLogger.setCurrencyType(CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());
                        paymentCurrencyOffsetLogger.setBeforeAmount(paymentCurrencyAccountById.getCurrentAmount());
                        paymentCurrencyOffsetLogger.setOffsetAmount(paymentCurrencyAccountById.getCurrentAmount()+orderPrice);//变化后的额度
                        paymentCurrencyOffsetLogger.setTransTitle("商家采购支付");
                        paymentCurrencyOffsetLogger.setTransType(entry.getPaymentType()+"");
                        paymentCurrencyOffsetLogger.setRelationTransType(RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey());
                        paymentCurrencyOffsetLogger.setRelationTransSequence(entry.getOrderSequenceNumber());

                        paymentCurrencyAccountById.setCurrentAmount(paymentCurrencyAccountById.getCurrentAmount()+orderPrice);
                        //修改余额
                        PaymentCurrencyAccountService.changePaymentCurrencyAccount(paymentCurrencyAccountById);
                        //添加余额变动记录
                        PaymentCurrencyOffsetLoggerRemoteService.addPaymentCurrencyOffsetLogger(paymentCurrencyOffsetLogger);
                    }

                    //添加到账记录
                    PaymentEstimateRevenueLogger paymentEstimateRevenueLogger = new PaymentEstimateRevenueLogger();
                    paymentEstimateRevenueLogger.setActualIncome(orderPrice);
                    paymentEstimateRevenueLogger.setOrderAmount(entry.getActualPrice());
                    paymentEstimateRevenueLogger.setOrderId(entry.getId());
                    paymentEstimateRevenueLogger.setUserId(entry.getSaleUserId());
                    paymentEstimateRevenueLogger.setCreateTime(new Date());
                    PaymentEstimateRevenueLoggerService.addPaymentEstimateRevenueLogger(paymentEstimateRevenueLogger);
                }

                paymentEstimateRevenue.setOrderAmount(entry.getActualPrice()); //订单金额
                paymentEstimateRevenue.setUserId(user.getId());
                paymentEstimateRevenue.setOrderId(entry.getId());
                paymentEstimateRevenue.setShopsName(entry.getShippingNickName());//商家名称
                paymentEstimateRevenue.setRequestTime(new Date());


                //添加账期
                PaymentEstimateRevenueService.addPaymentEstimateRevenue(paymentEstimateRevenue, getHttpServletRequest());

            }
        } catch (Exception e) {
            logger.info("===================添加支付账期数据失败===================");
        }

        return success();
    }

    /**
     * 订单金额计算
     * @author sqd
     * @date 2019/6/19
     * @param
     * @return
     */
    public  long  getOrderPrice(OrderEntry entry) {

        List<OrderGoodSnapshot> orderGoodSnapshotByOrderId = dataAccessManager.getOrderGoodSnapshotByOrderId(entry.getId());
        if(orderGoodSnapshotByOrderId == null || orderGoodSnapshotByOrderId.size() <= 0){
            return 0;
        }

        long orderPrice = 0;
        for (OrderGoodSnapshot goodSnapshot : orderGoodSnapshotByOrderId) {
            if(goodSnapshot.getReturnQuantity() != null){
                //存在退款
                orderPrice+=(goodSnapshot.getNormalQuantity()-goodSnapshot.getReturnQuantity()) * goodSnapshot.getNormalPrice();
            }else{
                orderPrice+=goodSnapshot.getNormalQuantity() * goodSnapshot.getNormalPrice();
            }
        }
        return orderPrice;
    }
    /**
     * 待支付订单支付
     * @author sqd
     * @date 2019/6/11
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject nonPaymentOrder() {
        String orderId = getUTF("id");
        //主单
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(orderId);
        //仓库商品
        List<OrderGoodSnapshot> orderGoodSnapshotByOrderId = dataAccessManager.getOrderGoodSnapshotByOrderId(orderEntry.getId());

        if(orderEntry == null){
             return  fail("订单信息有误");
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("sequenceNumbers",orderEntry.getOrderSequenceNumber());
        //所有子单
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(map);
        if(orderEntrys == null && orderEntrys.size() <= 0){
            return fail("订单已失效");
        }
        for (OrderEntry entry : orderEntrys) {
            //验证配送距离
            SupplychainShopProperties supplychainShopPropertiesByUserId = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(entry.getSaleUserId(), getHttpServletRequest());
           /* if(orderEntry.getReceiptLocation() != null){
                if(Long.parseLong(orderEntry.getReceiptLocation()) > supplychainShopPropertiesByUserId.getCoveringDistance()){
                    return  fail("订单配送距离超出");
                }
            }*/
            //验证仓库起订值
            if(supplychainShopPropertiesByUserId != null){
                if(supplychainShopPropertiesByUserId.getMoq() != null && orderEntry.getTotalPrice() < supplychainShopPropertiesByUserId.getMoq()){
                    return  fail("订单金额未达到配送起订值");
                }
            }
        }

        //验证仓库库存
        if(orderGoodSnapshotByOrderId != null && orderGoodSnapshotByOrderId.size() > 0){
            List<String> goodId = orderGoodSnapshotByOrderId.stream().map(OrderGoodSnapshot::getGoodId).collect(Collectors.toList());
            List<SupplychainGood> supplychainGoodByIds = SupplyChainGoodRemoteService.getSupplychainGoodByIds(goodId, getHttpServletRequest());
            if(supplychainGoodByIds != null && supplychainGoodByIds.size() > 0){
                for (OrderGoodSnapshot goodSnapshot : orderGoodSnapshotByOrderId) {
                    for (SupplychainGood supplychainGoodById : supplychainGoodByIds) {
                        if(goodSnapshot.getGoodId().equals(supplychainGoodById.getId())){
                            if(goodSnapshot.getNormalQuantity() > supplychainGoodById.getStock()){
                                return fail("订单商品存在库存不足");
                            }
                            continue;
                        }
                    }
                }
            }
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("orderNumber",orderEntry.getOrderSequenceNumber());
        return success(jsonObject);
    }


    /***
     * 根据id 查找对应的记录
     * @author oy
     * @date 2019/6/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @Override
    public JSONObject getOrderEntryAndPosById() {
        String orderId = getUTF("orderId");
        OrderEntry list = dataAccessManager.getOrderEntryAndPosById(orderId);
        return success(list);
    }


    @Override
    public JSONObject getBusinessOrderEntrys() {
        String type = getUTF("type", null);
        int pageIndex = getIntParameter("pageIndex", 0);
        int pageSize = getIntParameter("pageSize", 0);
        SysUser sysUser = null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (sysUser == null) {
            return fail(1001,"用户不能为空");
        }
        // List<OrderEntry> orderEntryList = dataAccessManager.getOrderEntrysByShippingUserIdAndType(sysUser.getUserId(), OrderTypeEnum.SUPPLYCHAINPURCHASE.getKey(), "", pageIndex, pageSize);

        return null;
    }

    /**
     * 商家端订单查询
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/3/31
     */
    @Override
    public JSONObject getOrderEntrysByShippingUserIdAndTypeBusiness() {
        String status = getUTF("status", null);
        int orderType = getIntParameter("orderType", -1);
        // int orderType = getIntParameter("orderType", 0);
       /* int pageIndex = getIntParameter("pageIndex", 0);
        int pageSize = getIntParameter("pageSize", 9);*/
        JSONObject jsonObject = new JSONObject();

        int pageNum = getIntParameter("pageIndex", 1);//页码
        int pageSize = getIntParameter("pageSize", 10);//页面大小
        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageNum - 1);
        int limitEnd = pageSize;
        SysUser sysUser = null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (sysUser == null) {
            return fail(1001,"用户不能为空");
        }

        SysUser user = null;
        try {
            user = SysUserRemoteService.getSysUser(sysUser.getId(), getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String shopUserId = "";
        if(user != null){
            if (user.getLeaderUserId() != null) {
                shopUserId = user.getLeaderUserId();
            } else {
                shopUserId = user.getId();
            }
        }else{
            return fail(1001,"用户不能为空");
        }

        int type = 1;
        if (orderType == 1) {
            //采购订单
            type = OrderTypeEnum.SUPPLYCHAINPURCHASE.getKey();
        } else {
            //销售订单
            type = OrderTypeEnum.SUPPLYCHAIN.getKey();
        }
        int intstatus = -1;
        if (status != null) {
            intstatus = Integer.parseInt(status);
        }
        List<OrderEntry> orderEntryList = dataAccessManager.getOrderEntrysByShippingUserIdAndTypeBusiness(shopUserId, type, intstatus, limitStart, limitEnd);
        List<OrderGoodSnapshot> orderGoodSnapshotAll = dataAccessManager.getOrderGoodSnapshotAll();
        List<OrderEntryVO> OrderEntryVOlist = new ArrayList<>();
        if (orderEntryList != null && orderEntryList.size() > 0) {
            orderEntryList.forEach(x -> {
                String id = x.getId();
                List<OrderGoodSnapshot> collect = orderGoodSnapshotAll.stream().filter(item -> item.getOrderId().equals(id)).collect(Collectors.toList());
                OrderEntryVO orderEntryVO = new OrderEntryVO();
                orderEntryVO.setActualPrice(x.getActualPrice());
                orderEntryVO.setTotalPrice(x.getTotalPrice());
                orderEntryVO.setOrderGoodSnapshots(x.getOrderGoodSnapshots());
                orderEntryVO.setCreateTime(x.getCreateTime());
                orderEntryVO.setDaySortNumber(x.getDaySortNumber());
                orderEntryVO.setId(x.getId());
                orderEntryVO.setType(x.getType());
                orderEntryVO.setStatus(x.getStatus());
                orderEntryVO.setOrderSequenceNumber(x.getOrderSequenceNumber());
                orderEntryVO.setSequenceNumber(x.getSequenceNumber());
                orderEntryVO.setReceiptNickPhone(x.getReceiptPhone());
                orderEntryVO.setDiscountPrice(x.getDiscountPrice());
                orderEntryVO.setDistributionFee(x.getDistributionFee());
                orderEntryVO.setReceiptNickName(x.getReceiptNickName());
                if (collect.size() > 0) {
                    //orderEntryVO.setShopName(collect.get(0).getGoodName());
                    orderEntryVO.setShopPhone(x.getShippingPhone());
                    orderEntryVO.setOrderGoodSnapshots(collect);
                }
                OrderEntryVOlist.add(orderEntryVO);
            });
        }
        jsonObject.put("OrderEntry", OrderEntryVOlist);
        return success(jsonObject);


        /**主单不添加快照时需要把主单下子单快照填充到主单
         List<OrderEntry> orderEntryList = dataAccessManager.getOrderEntrysByShippingUserIdAndTypeBusiness(shopUserId, type, intstatus, limitStart, limitEnd);
         //获取主订单orderId集合
         if (orderEntryList != null && orderEntryList.size() <= 0) {
         return success(jsonObject);
         }
         List<String> primarySequenceNumber = orderEntryList.stream().map(OrderEntry::getOrderSequenceNumber).collect(Collectors.toList());
         HashMap<String, Object> map = new HashMap<>();
         map.put("primarySequenceNumbers", primarySequenceNumber);
         //主单对应的所以子单
         List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(map);
         List<OrderGoodSnapshot> orderGoodSnapshotAll = dataAccessManager.getOrderGoodSnapshotAll();
         List<OrderEntryVO> OrderEntryVOlist = new ArrayList<>();
         if (orderEntrys != null && orderEntrys.size() > 0) {
         orderEntrys.forEach(x -> {
         String id = x.getId();
         List<OrderGoodSnapshot> collect = orderGoodSnapshotAll.stream().filter(item -> item.getOrderId().equals(id)).collect(Collectors.toList());
         OrderEntryVO orderEntryVO = new OrderEntryVO();
         orderEntryVO.setActualPrice(x.getActualPrice());
         orderEntryVO.setOrderGoodSnapshots(x.getOrderGoodSnapshots());
         orderEntryVO.setCreateTime(x.getCreateTime());
         orderEntryVO.setDaySortNumber(x.getDaySortNumber());
         orderEntryVO.setId(x.getId());
         orderEntryVO.setType(x.getType());
         orderEntryVO.setStatus(x.getStatus());
         orderEntryVO.setOrderSequenceNumber(x.getOrderSequenceNumber());
         orderEntryVO.setSequenceNumber(x.getSequenceNumber());
         orderEntryVO.setReceiptNickPhone(x.getReceiptPhone());
         orderEntryVO.setDiscountPrice(x.getDiscountPrice());
         orderEntryVO.setReceiptNickName(x.getReceiptNickName());
         if (collect.size() > 0) {
         orderEntryVO.setShopName(collect.get(0).getGoodName());
         orderEntryVO.setShopPhone(x.getShippingPhone());
         orderEntryVO.setOrderGoodSnapshots(collect);
         }


         OrderEntryVOlist.add(orderEntryVO);
         });
         }

         //子单数据封装到对应主单
         List<OrderEntryVO> primaryOrderEntryVOlist = new ArrayList<>();
         if (orderEntryList != null && orderEntryList.size() > 0) {
         orderEntryList.forEach(x -> {
         String orderSequenceNumber = x.getOrderSequenceNumber();
         List<OrderEntryVO> collect = OrderEntryVOlist.stream().filter(item -> item.getSequenceNumber().equals(orderSequenceNumber)).collect(Collectors.toList());
         OrderEntryVO primaryorderEntryVO = new OrderEntryVO();
         primaryorderEntryVO.setActualPrice(x.getActualPrice());
         primaryorderEntryVO.setOrderGoodSnapshots(x.getOrderGoodSnapshots());
         primaryorderEntryVO.setCreateTime(x.getCreateTime());
         primaryorderEntryVO.setDaySortNumber(x.getDaySortNumber());
         primaryorderEntryVO.setId(x.getId());
         primaryorderEntryVO.setType(x.getType());
         primaryorderEntryVO.setStatus(x.getStatus());
         primaryorderEntryVO.setOrderSequenceNumber(x.getOrderSequenceNumber());
         primaryorderEntryVO.setSequenceNumber(x.getSequenceNumber());
         primaryorderEntryVO.setReceiptNickPhone(x.getReceiptPhone());
         primaryorderEntryVO.setDiscountPrice(x.getDiscountPrice());
         primaryorderEntryVO.setReceiptNickName(x.getReceiptNickName());
         primaryorderEntryVO.setShopPhone(x.getShippingPhone());
         if (collect.size() > 0) {
         //封装快照数据
         List<OrderGoodSnapshot> primaryOrderGoodSnapshotsAll = new ArrayList<>();
         for (OrderEntryVO orderEntryVO : collect) {
         if (orderEntryVO.getOrderGoodSnapshots() != null && orderEntryVO.getOrderGoodSnapshots().size() > 0) {
         for (OrderGoodSnapshot orderGoodSnap : orderEntryVO.getOrderGoodSnapshots()) {
         primaryOrderGoodSnapshotsAll.add(orderGoodSnap);
         }
         }
         }
         primaryorderEntryVO.setOrderGoodSnapshots(primaryOrderGoodSnapshotsAll);
         }


         primaryOrderEntryVOlist.add(primaryorderEntryVO);
         });
         }
         jsonObject.put("OrderEntry", primaryOrderEntryVOlist);*/
    }


    /**
     * 商家端：订单再次购买
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/4/2
     */
    @Override
    public JSONObject orderEntryBuyAgain() {
        String orderId = getUTF("orderId", null);

        HashMap<String, Object> map = new HashMap<>();
        map.put("orderSequenceNumberOne", orderId);
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(map);
        if (orderEntrys.size() <= 0) {
            return fail("订单数据为空");
        }
        List<String> orderSequenceNumbers = orderEntrys.stream().map(OrderEntry::getId).collect(Collectors.toList());
        map.put("orderSequenceNumbers", orderSequenceNumbers);
        List<OrderGoodSnapshot> orderGoodSnapshotByOrderId = dataAccessManager.getOrderGoodSnapshotByMap(map);
        ArrayList<String> listId = new ArrayList<>();
        try {
            //添加购物车
            GoodShopCartRemoteService.addgoodShopCart(orderGoodSnapshotByOrderId, getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success();
    }


    /**
     * POS端：购买下单
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/6
     */
    @Override
    @Transactional
    public JSONObject placeAnOrderPos() {
        SysUser sysUser = null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String goodIdsAndNumber = getUTF("goodIdsAndNumber");
        Integer payType = getIntParameter("payType");
        String authCode = getUTF("authCode", null);
        String actualPrice = getUTF("actualPrice", null);//findPrice
        Double discount = getDoubleParameter("discount", OrderReceiptTitel.DISCOUNT);
        if (discount<=0||discount>OrderReceiptTitel.DISCOUNT){
            discount = OrderReceiptTitel.DISCOUNT;
        }
        //支付方式
        String authCodeType = isPayType(authCode, payType);
        if (authCodeType == null) {
            return fail("请对准条码");
        }
        if (payType == 0 && actualPrice == null) {
            return fail("现金结算请输入实收金额");
        }
        //查询用户店铺
        SupplychainShopProperties userShop = null;
        //远程调用用户商店绑定信息
        List<SupplychainShopBindRelationship> shopBindRelationships = SupplychainShopBindRelationshipRemoteService.getShopBindRelationships(5, sysUser.getId(), getHttpServletRequest());
        if (shopBindRelationships != null && shopBindRelationships.size() > 0) {
            userShop = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesById(shopBindRelationships.get(0).getShopId(), getHttpServletRequest());
        } else {
            userShop = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(sysUser.getId(), getHttpServletRequest());
        }


        //所有商品购买数量集合
        List<GoodIdAndGoodNumberVO> goodIdAndGoodNumberVO = JSONArray.parseArray(goodIdsAndNumber, GoodIdAndGoodNumberVO.class);
        List<OrderGoodSnapshot> orderGoodSnapshotlist = new ArrayList<>();
        if (userShop == null) {
            return fail("用户店铺数据为空");
        } else {
            // 折扣
            userShop.setDiscount(discount);
        }
        //订单编号
        String orderNumber = uniquePrimaryKey("TNY");
        //订单总金额
        long totalPrice = 0;
        long discountPriceSum = 0;
        long totalPrices = 0;
        // long discountGood= 0;
        //long NodiscountGood= 0;
        //订单实体 需要用到订单id
        OrderEntry orderEntry = new OrderEntry();
        //支付记录
        PaymentTransactionLogger paymentTransactionLogger = new PaymentTransactionLogger();
        //获取商品id
        List<String> goodIds = new ArrayList<>();
        //保存下架商品
        ArrayList<OutGoodVO> outGoodVO = new ArrayList<>();
        //记录下架商品数量
        Integer outGoodNumber = 0;
        boolean isGoods = true;
        HashMap<Object, Object> numberMap = new HashMap<>();
        for (int i = 0; i < goodIdAndGoodNumberVO.size(); i++) {
            if (userShop.getId().equals(goodIdAndGoodNumberVO.get(i).getGoodId())) {
                //** 自由价格商品
                if (goodIdAndGoodNumberVO.get(i).getNumber() == null) {
                    return fail("自由价格商品数量错误");
                }
                if (goodIdAndGoodNumberVO.get(i).getPrice() == 0) {
                    return fail("自由价格商品价格错误");  //** 后期如果价格折扣活动后有可能为零，取消这里价格验证
                }
                totalPrices += goodIdAndGoodNumberVO.get(i).getNumber() * goodIdAndGoodNumberVO.get(i).getPrice();//计算总费用

                //添加商品快照记录
                OrderGoodSnapshot goodSnapshot = new OrderGoodSnapshot();
                goodSnapshot.setOrderId(orderEntry.getId());//订单ID
                goodSnapshot.setCreateTime(new Date());
                goodSnapshot.setNormalPrice(goodIdAndGoodNumberVO.get(i).getPrice());
                goodSnapshot.setNormalQuantity(goodIdAndGoodNumberVO.get(i).getNumber());
                goodSnapshot.setGoodName("其它");
                goodSnapshot.setIsThirdParty(0);
                goodSnapshot.setTotalPrice(goodIdAndGoodNumberVO.get(i).getNumber() * goodIdAndGoodNumberVO.get(i).getPrice());//应收费用
                //添加商品快照
                dataAccessManager.addOrderGoodSnapshotPos(goodSnapshot);
                orderGoodSnapshotlist.add(goodSnapshot);
                isGoods = false;
            } else {
                goodIds.add(goodIdAndGoodNumberVO.get(i).getGoodId());
                //封装 商品id ： 数量
                numberMap.put(goodIdAndGoodNumberVO.get(i).getGoodId(), goodIdAndGoodNumberVO.get(i).getNumber());
                //goodIdAndGoodNumberVO.remove(i);//删除非自由价格商品，goodIdAndGoodNumberVO 剩下的就都为自由价格商品，再单独处理
            }
        }

        //userShop.getId()
        List<SupplychainGood> supplychainGoods = null;
        try {
            supplychainGoods = SupplyChainGoodRemoteService.getSupplychainGoodByIds(goodIds, getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isGoods && supplychainGoods == null) {
            return fail("未查询到商品信息");
        }

        //** 计算非自由价格商品
        if (supplychainGoods != null) {

            for (SupplychainGood good : supplychainGoods) {
                //添加商品快照记录
                OrderGoodSnapshot goodSnapshot = new OrderGoodSnapshot();

                //获取商品购买数量
                Integer goodNumber = (Integer) numberMap.get(good.getId());
               /* //验证商品库存  《不需要验证商品库存，库存可为负数》
                if(good.getStock() < goodNumber){
                    continue;
                }*/

                //验证商品是否下架  当前状态 -1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效 3正常
                if (good.getStatus() != 1 && good.getStatus() != 3) {
                    if (outGoodNumber >= goodIdAndGoodNumberVO.size()) {
                        return fail("所选商品已下架");
                    } else {
                        OutGoodVO outGoodV = new OutGoodVO();
                        outGoodV.setGoodName(good.getDefineName());
                        outGoodVO.add(outGoodV);
                        outGoodNumber += 1;
                        continue;
                    }
                }
                //远程调用查询商品条码
                GoodBarcode goodBarcode = GoodBarcodeRemoteService.getGoodBarcodeByPriceId(good.getPriceId(), getHttpServletRequest());
                //商品价格
                GoodPrice price = GoodPriceRemoteService.getGoodPriceById(good.getPriceId(), getHttpServletRequest());
                //商品规格，单位
                GoodUnit unit = new GoodUnit();
                String goodName = "";
                if (price != null) {
                    //远程查询商品单位
                    unit = GoodUnitRemoteService.getGoodUnitById(price.getUnitId(), getHttpServletRequest());
                }


                //查询商品模板
                GoodTemplate goodTemplate = null;
                try {
                    goodTemplate = GoodTemplateRemoteService.getGoodTemplate(good.getGoodTemplateId(), getHttpServletRequest());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (goodTemplate != null) {
                    goodName += goodTemplate.getName();
                }
                if (price != null && price.getTitle() != null) {
                    goodName += price.getTitle();
                }

                //查询商品类别
                String goodTypeId = "";
                if (good.getGoodTypeId() != null) {
                    try {
                        GoodType goodType = GoodTypeRemoteService.getGoodType(good.getGoodTypeId(), getHttpServletRequest());
                        if (goodType != null) {
                            String parentId = goodType.getParentId();
                            if (parentId.equals(OrderReceiptTitel.PRIMARY_CATEGORY)) {
                                goodTypeId = goodType.getId();
                                goodSnapshot.setGoodTypeId(goodType.getId());
                                goodSnapshot.setGoodTypeName(goodType.getTitle());
                            } else {
                                GoodType goodTypeByparentId = GoodTypeRemoteService.getGoodType(parentId, getHttpServletRequest());
                                if (goodTypeByparentId != null) {
                                    goodTypeId = goodTypeByparentId.getId();
                                    goodSnapshot.setGoodTypeId(goodTypeByparentId.getId());
                                    goodSnapshot.setGoodTypeName(goodTypeByparentId.getTitle());
                                }
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                //计算商品价格
                if (good.getSellPrice() != null && goodNumber != null) {
                    totalPrices += good.getSellPrice() * goodNumber;
                    if (goodTypeId.equals(OrderReceiptTitel.DISCOUNT_CATEGORY)) {

                        goodSnapshot.setTotalPrice(good.getSellPrice() * goodNumber);//应收费用
                        totalPrice += good.getSellPrice() * goodNumber;//计算总费用

                    } else if (goodTypeId.equals(OrderReceiptTitel.DISCOUNT_CATEGORY2)) {
                        goodSnapshot.setTotalPrice(good.getSellPrice() * goodNumber);//应收费用
                        totalPrice += good.getSellPrice() * goodNumber;//计算总费用
                    } else {
                        System.out.println("进入折扣");
                        long normalPrice = good.getSellPrice() * goodNumber; //实收价格

                        long discountPrice = Math.round(normalPrice * userShop.getDiscount());  //折扣后的金额

                        goodSnapshot.setTotalPrice(normalPrice);//应收费用
                        goodSnapshot.setDiscountPrice(normalPrice - discountPrice); //折扣价格
                        totalPrice += discountPrice;//计算总费用  +=折扣后的价
                        discountPriceSum += normalPrice - discountPrice; //折扣总价
                    }
                } else {
                    return fail("该商品价格有误,请联系管理员 Name=" + good.getDefineName()+"ID ="+good.getId());
                }


                //当时归属的类型名
                if (price != null) {
                    goodSnapshot.setGoodPriceTitle(price.getTitle());
                }

                if (unit != null) {
                    goodSnapshot.setGoodUnitName(unit.getTitle()); //当时单位名
                    goodSnapshot.setGoodUnitId(unit.getId());//当时的单位ID
                }
                //商品条码
                if (goodBarcode != null) {
                    goodSnapshot.setGoodBarcode(goodBarcode.getBarcode());
                }

                goodSnapshot.setOrderId(orderEntry.getId());//订单ID
                goodSnapshot.setGoodId(good.getId());//商品id
                goodSnapshot.setGoodTemplateId(good.getGoodTemplateId());//商品模版ID
                goodSnapshot.setGoodName(goodName);//当时的商品名称
                goodSnapshot.setIsThirdParty(0);//是否第三方商品，否为0
                goodSnapshot.setCreateTime(new Date());
                goodSnapshot.setNormalPrice(good.getSellPrice());
                goodSnapshot.setNormalQuantity(goodNumber);
                //添加商品快照
                dataAccessManager.addOrderGoodSnapshotPos(goodSnapshot);
                orderGoodSnapshotlist.add(goodSnapshot);
            }
        }
        orderEntry.setOrderSequenceNumber(orderNumber);
        orderEntry.setType(OrderTypeEnum.SUPERMARKETPOS.getKey());
        orderEntry.setPartnerId(sysUser.getId());//操作人id

        if (payType == 0) {
            orderEntry.setDeliverStatus(OrderDeliverStatus.HAVEARRIVED.getKey());//发货状态
            orderEntry.setStatus(OrderStatus.ORDER_TYPE_PAID.getKey());//订单状态
        } else {
            orderEntry.setStatus(OrderStatus.ORDER_TYPE_UNCONFIRMED.getKey());//订单状态
            orderEntry.setDeliverStatus(OrderDeliverStatus.UNSHIPPED.getKey());//发货状态
        }
        orderEntry.setRefundStatus(0);//退款状态 0表示未退货 同时可用于记录是否已打款状态
        orderEntry.setPaymentType(-1);
        orderEntry.setShippingUserId(sysUser.getUserId());//发货用户Id
        orderEntry.setShippingNickName(sysUser.getShowName());//发货用户name
        orderEntry.setTotalPrice(totalPrices);//订单总费用
        // orderEntry.setActualPrice(totalPrice);//实收费用
        orderEntry.setCreateTime(new Date());
        orderEntry.setIsJoin(1);
        //orderEntry.setShippingUserId(sysUser.getUserId());
        orderEntry.setShippingNickName(sysUser.getShowName());
        orderEntry.setShippingPhone(sysUser.getPhoneNumber());
        orderEntry.setShippingUserId(userShop.getId());
        orderEntry.setSaleUserId(userShop.getId());
        orderEntry.setThirdPartyDelivery(1);
        orderEntry.setDiscountPrice(discountPriceSum);
        orderEntry.setTransType(OrderStatus.ORDER_TYPE_UNCONFIRMED.getKey() + "");

        try {
            paymentTransactionLogger.setUserId(sysUser.getId());
            paymentTransactionLogger.setUserName(sysUser.getShowName());
            paymentTransactionLogger.setTransSequenceNumber(orderNumber);
            if (payType == 1) {
                //网上支付
                paymentTransactionLogger.setTransStatus(OrderStatus.ORDER_TYPE_UNCONFIRMED.getKey());
                if (authCodeType.equals("wx")) {
                    orderEntry.setPaymentType(PayTypeEnum.TRANS_TYPE_WEI.getKey());
                    paymentTransactionLogger.setPaymentType(PayTypeEnum.TRANS_TYPE_WEI.getKey() + "");
                }
                if (authCodeType.equals("zfb")) {
                    orderEntry.setPaymentType(PayTypeEnum.TRANS_TYPE_ZFB.getKey());
                    paymentTransactionLogger.setPaymentType(PayTypeEnum.TRANS_TYPE_ZFB.getKey() + "");
                }
            } else {
                //现金支付
                orderEntry.setPaymentType(PayTypeEnum.TRANS_TYPE_CASH.getKey());
                paymentTransactionLogger.setTransStatus(OrderStatus.ORDER_TYPE_STOCKS.getKey());
                paymentTransactionLogger.setPaymentType(PayTypeEnum.TRANS_TYPE_CASH.getKey() + "");
                if (authCodeType.equals("xj")) {
                    paymentTransactionLogger.setPaymentType(PayTypeEnum.TRANS_TYPE_CASH.getKey() + "");
                }
            }

            paymentTransactionLogger.setTransStatus(TransTypeEnum.PAYMENT.getKey());
            paymentTransactionLogger.setTransType(TransTypeEnum.PAYMENT.getKey());
            paymentTransactionLogger.setPartnerTradeNumber(orderNumber);
            paymentTransactionLogger.setCurrencyType("RMB");
            paymentTransactionLogger.setTransTotalAmount(totalPrice);
            paymentTransactionLogger.setCreateTime(new Date());
            paymentTransactionLogger.setTransCreateTime(new Date());
            paymentTransactionLogger.setRefundStatus(0);
            //添加订单支付记录
            PaymentTransactionLoggerService.addPaymentTransactionLogger(paymentTransactionLogger, getHttpServletRequest());

            //添加订单
            dataAccessManager.addOrderEntryPos(orderEntry);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject();
        //HashMap<String, Object> map1 = new HashMap<>();
        //map1.put("orderId",orderEntry.getId());
        //List<OrderGoodSnapshot> orderGoodSnapshot = dataAccessManager.getOrderGoodSnapshotPosByMap(map1);
        OrderEntryPosVO orderEntryPos = new OrderEntryPosVO();
        OrderEntry updateOrderEntry = new OrderEntry();
        updateOrderEntry.setActualPrice(totalPrices - discountPriceSum);//实收金额


        BeanUtils.copyProperties(orderEntry, orderEntryPos);
        orderEntryPos.setShopName(userShop.getShopName());
        orderEntryPos.setShopPhone(userShop.getPhoneNumber());
        orderEntryPos.setActualPrice(updateOrderEntry.getActualPrice());

        jsonObject.put("title", OrderReceiptTitel.TITLE);
        jsonObject.put("subtitle", OrderReceiptTitel.SUBTITLE);
        jsonObject.put("undersigned", OrderReceiptTitel.UNDERSIGNED);
        jsonObject.put("orderEntry", orderEntryPos);
        jsonObject.put("orderGoodSnapshot", orderGoodSnapshotlist);
        //支付状态
        boolean isPaySuccees = false;
        if (payType == 1) { // 网银支付
            if (authCodeType.equals("wx")) {
                //微信支付
                isPaySuccees = wxPay(orderNumber, authCode);
            } else if (authCodeType.equals("zfb")) {
                //支付宝支付
                isPaySuccees = zfbPay(orderNumber, authCode);
            }
        } else {
            //现金支付
            if (actualPrice != null && Long.parseLong(actualPrice) >= totalPrices - discountPriceSum) {
                isPaySuccees = true;
            } else {
                return fail("支付金额不足");
            }

        }

        if (isPaySuccees) {
            //支付成功
            //支付宝
            if (authCodeType.equals("zfb")) {
                //支付宝支付走回调修改支付成功后的相关信息 ,一些不方便在支付宝回调处理的在这里处理
                zfbCallback(orderNumber, orderEntry, goodIdAndGoodNumberVO, userShop);
                // 修改订单状态
                updateOrderEntry.setId(null);
                updateOrderEntry.setOrderSequenceNumber(orderNumber);
                updateOrderEntry.setPaymentTime(new Date());
                updateOrderEntry.setStatus(TransStatusEnum.TRADE_SUCCESSED_SERVER.getKey());
                updateOrderEntry.setTransType(TransStatusEnum.TRADE_SUCCESSED_SERVER.getKey() + "");
                dataAccessManager.changeOrderEntryPos(updateOrderEntry);
                jsonObject.put("findPrice", 0); //找零
                return success(jsonObject);
            }

            HashMap<String, Object> mapObj = new HashMap<>();

            long findPrice = 0;

            if (authCodeType.equals("xj")) {
                //现金支付 找零只针对现金支付
                updateOrderEntry.setFindPrice(Long.parseLong(actualPrice) - updateOrderEntry.getActualPrice());//找零 =  实收金额 - 订单金额
                findPrice = Long.parseLong(actualPrice) - updateOrderEntry.getActualPrice();
            } else {
                //网银支付
                //findPrice = Long.parseLong(actualPrice) - totalPrice;
                updateOrderEntry.setActualPrice(totalPrice);//实收金额  totalPrice订单总金额
            }
            updateOrderEntry.setId(null);
            updateOrderEntry.setOrderSequenceNumber(orderNumber);
            updateOrderEntry.setPaymentTime(new Date());
            updateOrderEntry.setTransType(TransStatusEnum.TRADE_SUCCESSED_SERVER.getKey() + "");
            dataAccessManager.changeOrderEntryPos(updateOrderEntry);
            mapObj.put("sequenceNumbers", orderNumber);
            //添加订单操作记录
            addStaueLog(mapObj, 1);
            //添加支付账期记录
            //addPaymentEstimateRevenue(orderNumber, 1);
            //修改库存
            changeStock(goodIdAndGoodNumberVO, userShop.getId());

            jsonObject.put("findPrice", findPrice);
            jsonObject.put("outGood", outGoodVO);//下架商品
            return success(jsonObject);
        } else {
            Map<String, Object> mapPay = WeiXinPayUtils.queryPayStatus(orderNumber, WeiXinTerminalPayTypeEnum.APP_POS.getKey());
            if (mapPay.get("err_code_des").equals("101二维码已过期，请刷新再支付")) {
                return fail("二维码已过期，请刷新再支付");
            }
            if(updateOrderEntry.getActualPrice() <= 0){
                return fail("该商品已失效,请刷新商品列表");
            }
            return fail("支付失败");

        }
    }

    /**
     * pos 支付宝支付成功回调
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/16
     */
    public JSONObject zfbPosPayCallback() {
        HttpServletRequest httpServletRequest = getHttpServletRequest();
        String orderNumber = (String) httpServletRequest.getAttribute("orderNumber");
        String totalPrice1 = (String) httpServletRequest.getAttribute("totalPrice");
        try {
            HashMap<String, Object> mapObj1 = new HashMap<>();
            mapObj1.put("orderSequenceNumber", orderNumber);
            String ip = getHttpServletRequest().getRemoteAddr();
            List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrysPos(mapObj1);

            Number num = Float.parseFloat(totalPrice1) * 100;
            long totalPrice = num.intValue();
            OrderEntry updateOrderEntry = new OrderEntry();
            HashMap<String, Object> mapObj = new HashMap<>();

            if (orderEntrys != null && orderEntrys.size() > 0) {
                updateOrderEntry.setActualPrice(totalPrice - orderEntrys.get(0).getDiscountPrice());//实收金额
            }

            updateOrderEntry.setId(null);
            updateOrderEntry.setOrderSequenceNumber(orderNumber);
            updateOrderEntry.setActualPrice(totalPrice);//实收金额
            updateOrderEntry.setPaymentTime(new Date());//支付时间
            updateOrderEntry.setStatus(8);//支付时间
            dataAccessManager.changeOrderEntryPos(updateOrderEntry);
            mapObj.put("sequenceNumbers", orderNumber);
            mapObj.put("orderSequenceNumber", orderNumber);
            //添加支付账期记录
            //addPaymentEstimateRevenue(orderNumber, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*//修改库存
        changeStock(goodIdAndGoodNumberVO,userShop.getId());*/
        return success();
    }

    /**
     * 支付宝支付成功
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/23
     */

    public void zfbCallback(String orderNumber, OrderEntry orderEntry, List<GoodIdAndGoodNumberVO> goodIdAndGoodNumberVO, SupplychainShopProperties userShop) {
        //修改库存
        //添加订单支付记录
        try {
            //修改支付流水
            HashMap<String, Object> mapLog = new HashMap<>();
            mapLog.put("primarySequenceNumbers", orderNumber);
            try {
                PaymentTransactionLoggerService.changePaymentTransactionLoggerMap(mapLog, getHttpServletRequest());
            } catch (Exception e) {
                logger.info("--------------------------修改支付流水记录错误--------------------------");
                e.printStackTrace();
            }
            try {
                OrderStateLogger orderStateLogger = new OrderStateLogger();
                orderStateLogger.setOrderId(orderEntry.getId());
                orderStateLogger.setShippingUserId(orderEntry.getShippingUserId());
                orderStateLogger.setOrderType(orderEntry.getType());
                orderStateLogger.setBeforeStatus(1);
                orderStateLogger.setStatus(OrderStatus.ORDER_TYPE_PAID.getKey());
                orderStateLogger.setOperatorTime(new Date());
                SysUser user = getUser();
                if (user != null) {
                    orderStateLogger.setOperatorUserId(user.getRoleId());
                    orderStateLogger.setOperatorName(user.getShowName());
                }
                dataAccessManager.addOrderStateLogger(orderStateLogger);
            } catch (Exception e) {
                logger.info("--------------------------添加订单状态记录错误--------------------------");
                e.printStackTrace();
            }
            try {
                changeStock(goodIdAndGoodNumberVO, userShop.getId());
            } catch (Exception e) {
                logger.info("--------------------------商品库存修改失败--------------------------");
                e.printStackTrace();
            }
        } catch (Exception e) {
            logger.info("===================修改支付流水失败===================");
            e.printStackTrace();
        }
    }

    /**
     * 修改库存
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/13
     */
    public void changeStock(List<GoodIdAndGoodNumberVO> goodIdAndGoodNumberVO, String userShop) {
        if (goodIdAndGoodNumberVO.size() > 0) {
            for (GoodIdAndGoodNumberVO good : goodIdAndGoodNumberVO) {
                if (!userShop.equals(good.getGoodId())) {
                    //修改商品库存
                    SupplyChainGoodRemoteService.changSupplychainGoodByGoodId(good.getGoodId(), good.getNumber(), 0, getHttpServletRequest());
                }
            }
        }
    }

    /***
     * 判断条码类型
     * @author sqd
     * @date 2019/5/9
     * @param
     * @return isPayType
     */
    public String isPayType(String authCode, Integer payType) {
        //微信支付码规则：18位纯数字，以10、11、12、13、14、15开头
        //支付宝支付码规则：支付授权码，25~30开头的长度为16~24位的数字
        if (authCode == null && payType == 0) {
            //现金
            return "xj";
        }
        if (authCode != null && authCode.length() > 2) {
            String substring = authCode.substring(0, 2);
            if ((authCode.length() == 18) && (substring.equals("10") || substring.equals("11") || substring.equals("12") || substring.equals("13") || substring.equals("14") || substring.equals("15"))) {
                //微信
                return "wx";
            } else if ((authCode.length() > 15 && authCode.length() < 25) && (substring.equals("25") || substring.equals("26") || substring.equals("27") || substring.equals("28") || substring.equals("29") || substring.equals("30"))) {
                //支付宝
                return "zfb";
            }

        }
        //"条码错误" null
        return null;
    }

    /**
     * pos支付接口
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/6
     */
    public void payPos(String orderNumber, String authCode, String payType) {


    }

    // TODO:此处支付成功没有修改订单状态 现解决在1383行
    private boolean zfbPay(String orderNumber, String authCode) {
        HashMap<String, Object> mapObj = new HashMap<>();
        mapObj.put("orderSequenceNumber", orderNumber);
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrysPos(mapObj);
        if (orderEntrys.size() <= 0) {
            return false;
        }
        long totalPrice = 0;
        if (orderEntrys.get(0).getDiscountPrice() != null) {
            totalPrice = Math.round((orderEntrys.get(0).getTotalPrice() - orderEntrys.get(0).getDiscountPrice()));
        } else {
            totalPrice = Math.round(orderEntrys.get(0).getTotalPrice());
        }
        double totalPrices = totalPrice * 0.01;//单位 元
        if (totalPrices > 0) {
            AlipayTradePayResponse response = ZhiFuBaoPayUtils.PosPay("鸵鸟严选便利店消费", totalPrices + "", authCode, orderNumber, null);
            if (response.getCode().equals("10000")) {
                //支付成功
                return true;
            } else {
                boolean flag = true;
                int count = 0;
                while (flag) {
                    //查询支付记录
                    AlipayTradeQueryResponse alipayTradeQueryResponse = ZhiFuBaoPayUtils.queryTrade(orderNumber);
                    logger.info("alipayTradeQueryResponse.getCode()" + alipayTradeQueryResponse.getCode());
                    if (alipayTradeQueryResponse.getCode().equals("10000")) {//付款成功
                        System.out.println("======取消轮训，付款成功======");
                        flag = false;//取消轮训
                    }
                    if (count == 150) {//5分钟内没付款 就取消轮循
                        System.out.println("======2分钟内没付款 就取消轮循======" + count);
                        flag = false;
                    }
                    count += 1;
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }
        }
        return false;
    }

    //Pos端微信支付
    public boolean wxPay(String orderNumber, String authCode) {
        HashMap<String, Object> mapObj = new HashMap<>();
        mapObj.put("sequenceNumbers", orderNumber);
        mapObj.put("orderSequenceNumber", orderNumber);
        String ip = getHttpServletRequest().getRemoteAddr();
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrysPos(mapObj);
        if (orderEntrys.size() <= 0) {
            return false;
        }
        long totalPrice = 0;
        if (orderEntrys.get(0).getDiscountPrice() != null) {
            totalPrice = orderEntrys.get(0).getTotalPrice() - orderEntrys.get(0).getDiscountPrice();//
        } else {
            totalPrice = orderEntrys.get(0).getTotalPrice();
        }

        logger.info("微信付款金额------------------：" + totalPrice);
        if (totalPrice <= 0) {
            return false;
        }
        Map map = WeiXinPayUtils.createOrderByPos(orderNumber, totalPrice + "", ip, authCode, "支付");
        System.out.println("sys微信返回结果下单测试------------------：" + map);
        logger.info("logger微信返回结果下单测试：" + map);
        boolean flaglog = false;
        //查询是否支付成功
        if (map.get("return_code").equals("SUCCESS") && map.get("result_code").equals("SUCCESS")) {
            flaglog = true;
        } else if (map.get("err_code_des").equals("101二维码已过期，请刷新再支付")) {
            logger.info("101二维码已过期，请刷新再支付");
            flaglog = false;
        } else {
            boolean flag = true;
            int count = 0;
            while (flag) {
                //查询支付记录
                Map<String, Object> mapPay = WeiXinPayUtils.queryPayStatus(orderNumber, WeiXinTerminalPayTypeEnum.APP_POS.getKey());
                String return_code = (String) mapPay.get("return_code");
                String result_code = (String) mapPay.get("result_code");
                String trade_state = (String) mapPay.get("trade_state");
                if (return_code.equals("SUCCESS") && result_code.equals("SUCCESS") && trade_state.equals("SUCCESS")) {//付款成功
                    System.out.println("======取消轮训，付款成功======");
                    flag = false;//取消轮训
                    flaglog = true;//修改订单状态、添加记录
                }
                if (count == 150) {//5分钟内没付款 就取消轮循
                    System.out.println("======2分钟内没付款 就取消轮循======" + count);
                    flag = false;
                }
                count += 1;
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        //验证是否支付成功
        if (flaglog) {
            //修改主订单状态
            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setId(null);
            orderEntry.setOrderSequenceNumber(orderNumber);
            orderEntry.setStatus(OrderStatus.ORDER_TYPE_PAID.getKey());
            orderEntry.setActualPrice(totalPrice);
           // orderEntry.setTotalPrice(totalPrice);//实收金额
            dataAccessManager.changeOrderEntryPos(orderEntry);
            mapObj.put("sequenceNumbers", orderNumber);
            //添加主单操作记录
            addStaueLog(mapObj, 1);
            //添加支付账期记录
            //addPaymentEstimateRevenue(orderNumber, 1);
            return true;
        } else {
            //支付失败
            return false;
        }
    }


    /**
     * 商家端：购买下单
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/4/8
     */
    @Override
    @Transactional
    public JSONObject placeAnOrder() {
        SysUser sysUser = null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //购物车所有商品集合
        //String goodListJson = getUTF("goodListJson");
        String goodIds = getUTF("goodIds");
        String goodMap = getUTF("goodMap");// key = goodId, val  = goodNumber
        //List<SupplychainGoodAndShopVO> goodList = JSONArray.parseArray(goodListJson, SupplychainGoodAndShopVO.class);
        //收货地址Id
        String addressId = getUTF("addressId");
        //所有商品Id
        //String goodIds = getUTF("goodIds");
        //买家留言
        String remark = getUTF("remark", null);
        //实收费用
        //String actualPrice = getUTF("actualPrice");
        //订单总费用
        //String totalPrice = getUTF("totalPrice");
        //配送费用
        //String distributionFee = getUTF("distributionFee");
        //省
        //String province = getUTF("province", null);
        //市
        //String city = getUTF("city", null);
        //区
        //String area = getUTF("area", null);
        //String receiptAddress = getUTF("receiptAddress", null);
        //实付金额
        String practicalMoney = getUTF("practicalMoney", null);
        //配送费
        String distributionCost = getUTF("distributionCost", null);
        //支付方式
        String paymentType = getUTF("paymentType", null);
        //序列号(对应于购物车)
        String sequenceNumber = getUTF("sequenceNumber", null);
        //收货人昵称
        String receiptNickName = getUTF("receiptNickName", null);
        //收货人电话
        String receiptPhone = getUTF("receiptPhone", null);
        //下单时所在地理位置
        String currentLocation = getUTF("currentLocation", null);
        String transType = getUTF("transType", null);
        //优惠的费用
        String discountPrice = getUTF("discountPrice", null);
        //收货人用户ID
        String receiptUserId = getUTF("receiptUserId", null);
        ArrayList<SupplychainGoodVo> isSupMoq = new ArrayList<>(); //没有达到供应商起订金额的商品
        SysUserAddress sysUserAddressById = SysUserAddressRemoteService.getSysUserAddressById(addressId, getHttpServletRequest());
        if (sysUserAddressById == null) {
            return fail("收货地址信息有误");
        }
        String detaiAdd = "";//详细收货地址
        if (sysUserAddressById.getStreet() != null) {
            detaiAdd += sysUserAddressById.getStreet();
        }
        if (sysUserAddressById.getStreetNum() != null) {
            detaiAdd += sysUserAddressById.getStreetNum();
        }
        if (sysUserAddressById.getDisplayName() != null) {
            detaiAdd += sysUserAddressById.getDisplayName();
        }
        if (sysUserAddressById.getDetailAddress() != null) {
            detaiAdd += sysUserAddressById.getDetailAddress();
        }
        final String finalDetaiAdd = detaiAdd;
        System.out.println(detaiAdd);
        //所有商品
        List<SupplychainGoodAndShopVO> goodList = null;
        try {
            goodList = GoodShopCartRemoteService.getSupplychainGoodByUserIdOrder(goodIds, getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }



        SysUser user = null;
        try {
            user = SysUserRemoteService.getSysUser(sysUser.getId(), getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userId = sysUser.getId();
        //当前用户上级id
        String shopUserId = "";
        if (user.getLeaderUserId() != null) {
            shopUserId = user.getLeaderUserId();
        } else {
            shopUserId = user.getId();
        }
        String finalUserId = shopUserId;

        if (goodList == null) {
            return fail("购买商品已下架");
        }
        Map goodsMap = JSONObject.parseObject(goodMap, Map.class);
        String primaryUniquePrimaryKey = uniquePrimaryKey("TN"); //主单订单号

        long orderTotalGoodsMoney = 0;                      //订单商品总费用
        long orderTotalDistributionCostMoney = 0;           //订单商品总配送费用
        int i = 0;                                          //为主单添加发货用户，只添加一次
        int isMoq = 0;                                      //记录起订量不足的数量
        OrderEntry primaryOrderEntry = new OrderEntry();    //主订单实体


        ArrayList<SupplychainGoodAndShopVO> supplychainGoods = new ArrayList<>();//仓库供应商能提供商品
        ArrayList<GoodOutVO> goodOut = new ArrayList<>();   //不能购买的商品

        String supplychainGoodIds = "";
        for (SupplychainGoodAndShopVO goodAll : goodList) {
            SupplychainGoodAndShopVO supplychainGoodAndShopVO = new SupplychainGoodAndShopVO();
            //计算配送距离 .getCoveringDistance()
            int isDistribution = SimpleLocationUtils.simpleDistance(goodAll.getLatitude(), goodAll.getLongitude(), sysUserAddressById.getLatitude(), sysUserAddressById.getLongitude());

            //验证配送范围
            if (isDistribution > goodAll.getCoveringDistance()) {
                for (int k = 0; k < goodAll.getList().size(); k++) {
                    GoodOutVO goodOutVO = new GoodOutVO();
                    goodOutVO.setGoodName(goodAll.getList().get(k).getDefineName());
                    goodOutVO.setRemark("配送范围超出");
                    goodOut.add(goodOutVO);
                    goodAll.getList().remove(k);
                    k--;
                }
                continue;
            }
            //计算起订金额
            long goodPricesum = 0;

            //去除不能购买的商品
            List<SupplychainGoodAndShopVO> supShopGoodList = new ArrayList<>();
            ArrayList<SupplychainGoodVo> goods = new ArrayList<>();
            for (int j = 0; j < goodAll.getList().size(); j++) {

                Integer isGoodNumber = (Integer) goodsMap.get(goodAll.getList().get(j).getId());
                if (isGoodNumber != null && isGoodNumber > 0) {
                    if ((goodAll.getList().get(j).getStock() >= isGoodNumber) && ((goodAll.getList().get(j).getMinimumSellCount() != null && goodAll.getList().get(j).getMinimumSellCount() <= isGoodNumber) || goodAll.getList().get(j).getMinimumSellCount() == null)) {
                        //可以购买的商品
                        //goodPricesum += goodAll.getList().get(j).getSellPrice() * isGoodNumber;
                        //记录可以仓库可以购买的商品，拿去验证供应商是否能提供
                        //验证供应商是否能提供这些商品
                        //supplychainGoodIds+=goodAll.getList().get(j).getPriceId()+",";
                        //找对应的供应商
                        List<SupplychainWarehourseSupplierSkuRelationship> supplySkus =
                                SupplychainWarehourseSupplierSkuRelationshipRemoteService.getSupplychainWarehourseSupplierSkuRelationshipById
                                        (goodAll.getShopId(), goodAll.getList().get(j).getPriceId(), getHttpServletRequest());
                        if (supplySkus == null || supplySkus.size() <= 0) {
                            GoodOutVO goodOutVO = new GoodOutVO();
                            goodOutVO.setGoodName(goodAll.getList().get(j).getDefineName());
                            goodOutVO.setRemark("商品未绑定");
                            goodOut.add(goodOutVO);
                            goodAll.getList().remove(j);//出去不能购买的商品
                            j--;
                            continue;
                        }
                        //看供应商能够提供这个商品
                        SupplychainGood supplychainGood = SupplyChainGoodRemoteService.getSupplychainGoodByShopIdAndPriceId(supplySkus.get(0).getSupplierId(), supplySkus.get(0).getPriceId(), getHttpServletRequest());
                        if (supplychainGood == null) {
                            GoodOutVO goodOutVO = new GoodOutVO();
                            goodOutVO.setGoodName(goodAll.getList().get(j).getDefineName());
                            goodOutVO.setRemark("商品未绑定");
                            goodOut.add(goodOutVO);
                            goodAll.getList().remove(j);//出去不能购买的商品
                            j--;
                            continue;
                        }
                        //上面都通过该商品可以购买
                        SupplychainGoodVo supplychainGoodVo = new SupplychainGoodVo();
                        goodAll.getList().get(j).setGoodPriceId(supplySkus.get(0).getPriceId());
                        BeanUtils.copyProperties(supplychainGood, supplychainGoodVo);
                        supplychainGoodVo.setGoodNumber(isGoodNumber);
                        supplychainGoodVo.setGoodUnit(goodAll.getList().get(j).getGoodUnit());
                        supplychainGoodVo.setGoodPriceId(supplySkus.get(0).getPriceId());
                        goods.add(supplychainGoodVo);
                        goodPricesum += goodAll.getList().get(j).getSellPrice() * isGoodNumber;
                    } else {
                        GoodOutVO goodOutVO = new GoodOutVO();
                        goodOutVO.setGoodName(goodAll.getList().get(j).getDefineName());
                        goodOutVO.setRemark("商品库存不足");
                        goodOut.add(goodOutVO);
                        goodAll.getList().remove(j);//出去不能购买的商品
                        j--;
                        continue;
                    }


                }
            }

            // 验证仓库起订值
            if (goodAll != null && goodAll.getMoq() > goodPricesum) {
                for (int k = 0; k < goodAll.getList().size(); k++) {
                    GoodOutVO goodOutVO = new GoodOutVO();
                    goodOutVO.setGoodName(goodAll.getList().get(k).getDefineName());
                    goodOutVO.setRemark("起订金额不足");
                    goodOut.add(goodOutVO);
                    goodAll.getList().remove(k);
                    k--;
                }
                continue;
            }

            supplychainGoodAndShopVO.setShopName(goodAll.getShopName());
            supplychainGoodAndShopVO.setShopId(goodAll.getShopId());
            supplychainGoodAndShopVO.setShopType(goodAll.getShopType());
            supplychainGoodAndShopVO.setShopUserId(goodAll.getShopUserId());
            supplychainGoodAndShopVO.setParentId(goodAll.getParentId());
            supplychainGoodAndShopVO.setDistributionCost(goodAll.getDistributionCost());
            supplychainGoodAndShopVO.setPhoneNumber(goodAll.getPhoneNumber());
            supplychainGoodAndShopVO.setProvince(goodAll.getProvince());
            supplychainGoodAndShopVO.setCity(goodAll.getCity());
            supplychainGoodAndShopVO.setDistrict(goodAll.getDistrict());
            supplychainGoodAndShopVO.setAddress(goodAll.getAddress());
            supplychainGoodAndShopVO.setMoq(goodAll.getMoq());
            supplychainGoodAndShopVO.setDistributionType(goodAll.getDistributionType());
            supplychainGoodAndShopVO.setCreateTime(goodAll.getCreateTime());
            supplychainGoodAndShopVO.setLocation(goodAll.getLocation());
            supplychainGoodAndShopVO.setLongitude(goodAll.getLongitude());
            supplychainGoodAndShopVO.setLatitude(goodAll.getLatitude());
            supplychainGoodAndShopVO.setCoveringDistance(goodAll.getCoveringDistance());
            supplychainGoodAndShopVO.setList(goods);
            supplychainGoods.add(supplychainGoodAndShopVO);
        }

        //这里为供应商商品 验证供应商起订金额
        List<SupplychainGoodAndShopVO> sortSupplychainGood = supplychainGoods.stream().sorted(Comparator.comparing(SupplychainGoodAndShopVO::getShopId)).collect(Collectors.toList());

        Map<String, SupplychainGoodAndShopVO> map1 = new HashMap<>();//供应商id：供应商商品（shopid仓库id）实体集合


        //根据供应商拆单
        for (SupplychainGoodAndShopVO supplychainGoodVO : sortSupplychainGood) {
            String shopId = supplychainGoodVO.getShopId();//仓库id
            supplychainGoodVO.getList().forEach(x -> {
                SupplychainGoodAndShopVO supplychainGoodAndShopVO = new SupplychainGoodAndShopVO();
                String supId = x.getShopId();//供应商id
                supplychainGoodAndShopVO.setShopId(x.getShopId());//**加仓库id  供应商id
                supplychainGoodAndShopVO.setShopName(supplychainGoodVO.getShopName());
                // supplychainGoodAndShopVO.setShopId(supplychainGoodVO.getShopId());
                supplychainGoodAndShopVO.setShopType(supplychainGoodVO.getShopType());
                supplychainGoodAndShopVO.setShopUserId(supplychainGoodVO.getShopUserId());
                supplychainGoodAndShopVO.setParentId(supplychainGoodVO.getParentId());
                supplychainGoodAndShopVO.setDistributionCost(supplychainGoodVO.getDistributionCost());
                supplychainGoodAndShopVO.setPhoneNumber(supplychainGoodVO.getPhoneNumber());
                supplychainGoodAndShopVO.setProvince(supplychainGoodVO.getProvince());
                supplychainGoodAndShopVO.setCity(supplychainGoodVO.getCity());
                supplychainGoodAndShopVO.setDistrict(supplychainGoodVO.getDistrict());
                supplychainGoodAndShopVO.setAddress(supplychainGoodVO.getAddress());
                supplychainGoodAndShopVO.setMoq(supplychainGoodVO.getMoq());
                supplychainGoodAndShopVO.setDistributionType(supplychainGoodVO.getDistributionType());
                supplychainGoodAndShopVO.setCreateTime(supplychainGoodVO.getCreateTime());
                supplychainGoodAndShopVO.setLocation(supplychainGoodVO.getLocation());
                supplychainGoodAndShopVO.setLongitude(supplychainGoodVO.getLongitude());
                supplychainGoodAndShopVO.setLatitude(supplychainGoodVO.getLatitude());
                supplychainGoodAndShopVO.setCoveringDistance(supplychainGoodVO.getCoveringDistance());

                ArrayList<SupplychainGoodVo> sgood = new ArrayList<>();
                SupplychainGoodAndShopVO supplychainGoodAndShopVOS = map1.get(shopId + supId);

                if (supplychainGoodAndShopVOS == null) {
                    List<SupplychainGoodVo> list = new ArrayList<>();
                    list.add(x);
                    supplychainGoodAndShopVO.setList(list);
                    map1.put(shopId + supId, supplychainGoodAndShopVO);
                } else {
                    List<SupplychainGoodVo> list = supplychainGoodAndShopVOS.getList();
                    list.add(x);
                    supplychainGoodAndShopVOS.setList(list);
                    map1.put(shopId + supId, supplychainGoodAndShopVOS);
                }
            });
        }
        ArrayList<String> keyList = new ArrayList<>();
        //验证供应商起订值
        if (map1 != null && !map1.isEmpty()) {
            map1.forEach((x, y) -> {
                long goodPriceMoq = 0;
                for (SupplychainGoodVo supplychainGoodVo : y.getList()) {
                    goodPriceMoq+=supplychainGoodVo.getSellPrice() * supplychainGoodVo.getGoodNumber();
                }
                if(y.getList() != null && y.getList().size() > 0){
                    SupplychainShopProperties supplychainShopPropertiesById = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesById(y.getList().get(0).getShopId(), getHttpServletRequest());
                    if(supplychainShopPropertiesById != null && supplychainShopPropertiesById.getMoq() != null && goodPriceMoq < supplychainShopPropertiesById.getMoq()){
                        for (SupplychainGoodVo supplychainGoodVo : y.getList()){
                            isSupMoq.add(supplychainGoodVo);
                              GoodOutVO goodOutVO = new GoodOutVO();
                            goodOutVO.setGoodName(supplychainGoodVo.getDefineName());
                            goodOutVO.setRemark("起订金额不足");
                            goodOut.add(goodOutVO);
                        }
                        System.out.println(x);
                        keyList.add(x);

                    }
                }

            });
        }
        //除去不满足供应商起订量的商品
        if (keyList != null && keyList.size() > 0){
            for (String s : keyList) {
                map1.remove(s);
            }
        }
        //x key , y val
        if (map1 != null && !map1.isEmpty()) {
            map1.forEach((x, y) -> {
                //供应商订单总金额
                long supPrice = 0;
                int distribution = 0;
                try {
                    distribution = SimpleLocationUtils.simpleDistance(y.getLatitude(), y.getLongitude(), sysUserAddressById.getLatitude(), sysUserAddressById.getLongitude());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                SupplychainShopProperties supplychainShopPropertiesById = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesById(y.getShopId(), getHttpServletRequest());
                    if(supplychainShopPropertiesById != null){
                        OrderEntry supOrderEntry = new OrderEntry();
                        supOrderEntry.setSequenceNumber(primaryUniquePrimaryKey);//序列号
                        supOrderEntry.setOrderSequenceNumber(uniquePrimaryKey("TN"));//订单的序列号(用于展示)
                        //supOrderEntry.setPartnerUserId(sysUser.getId());//下单用户ID 匿名时可为0或空
                        supOrderEntry.setType(OrderTypeEnum.SUPPLYCHAIN.getKey());//订单类型
                        supOrderEntry.setStatus(OrderStatus.ORDER_TYPE_UNCONFIRMED.getKey());//订单状态
                        supOrderEntry.setDeliverStatus(OrderDeliverStatus.UNSHIPPED.getKey());//发货状态
                        supOrderEntry.setRefundStatus(0);//退款状态 0表示未退货 同时可用于记录是否已打款状态
                        //supOrderEntry.setTotalPrice(distributionFee + goodPrice);//* 订单总金额 配送费用 + 商品总金额
                        supOrderEntry.setTransType(String.valueOf(WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey()));//交易类型
                        //orderEntry.setUserSource("");//用户来源
                        //orderEntry.setDaySortNumber("");//当天排号 相对于发货方
                        supOrderEntry.setPushType(2);//推送类型，0表示不推送 1时表示下单时推送 2时表示支付后推送
                        supOrderEntry.setSaleUserId(supplychainShopPropertiesById.getUserId());//实际销售的用户ID（销售仓仓管id）
                        supOrderEntry.setReceiptUserId(receiptUserId);//收货人用户ID
                        supOrderEntry.setReceiptNickName(receiptNickName);//收货人昵称
                        supOrderEntry.setReceiptProvince(sysUserAddressById.getProvince());//收货省份
                        supOrderEntry.setReceiptCity(sysUserAddressById.getCity());//收货城市
                        supOrderEntry.setReceiptDistrict(sysUserAddressById.getDistrict());//收货区域
                        supOrderEntry.setReceiptAddress(finalDetaiAdd);//收货详细地址
                        supOrderEntry.setShippingProvince(y.getProvince());//发货省份
                        supOrderEntry.setShippingCity(supplychainShopPropertiesById.getCity());//发货市
                        supOrderEntry.setShippingDistrict(supplychainShopPropertiesById.getDistrict());//发货区
                        supOrderEntry.setShippingAddress(supplychainShopPropertiesById.getAddress());//发货详细地址
                        supOrderEntry.setPartnerId(userId);//下游商户id
                        supOrderEntry.setThirdPartyDeliveryStatus(y.getDistributionType());//配送方式
                        supOrderEntry.setReceiptPhone(receiptPhone);//收货人电话
                        supOrderEntry.setCurrentLocation(currentLocation);//下单时所在地理位置
                        supOrderEntry.setRemark(remark);//订单描述
                        supOrderEntry.setPartnerUserId(finalUserId);//下单用户ID 匿名时可为0或空
                        //orderEntry.setTotalPrice(goodPrice);//实收费用
                        //orderEntry.setDiscountPrice("");//优惠的费用
                        supOrderEntry.setDistributionFee(y.getDistributionCost());//配送费用
                        supOrderEntry.setCreateTime(new Date());//订单创建时间
                        supOrderEntry.setIsJoin(1);//是否为加盟店 1为是
                        supOrderEntry.setIsThirdParty(0);//是否为第三方 0为平台 不等于0为第三方
                        supOrderEntry.setThirdPartyDelivery(0);//是否使用第三方物流 0否 1是
                        //supOrderEntry.setPartnerId(sysUser.getId());//下游用户ID
                        supOrderEntry.setShippingUserId(y.getShopUserId());
                        supOrderEntry.setShippingNickName(supplychainShopPropertiesById.getShopName());
                        supOrderEntry.setReceiptLocation(distribution+"");
                        List<SupplychainGoodVo> list = y.getList();
                        if(list != null){
                            for (SupplychainGoodVo supplychainGoodVo : list) {
                                //计算金额
                                supPrice+=supplychainGoodVo.getSellPrice() * supplychainGoodVo.getGoodNumber();
                                OrderGoodSnapshot goodSnapshot = new OrderGoodSnapshot();
                                goodSnapshot.setOrderId(supOrderEntry.getId());//订单id
                                goodSnapshot.setGoodId(supplychainGoodVo.getId());//商品id
                                goodSnapshot.setGoodTemplateId(supplychainGoodVo.getGoodTemplateId());//商品模版ID
                                goodSnapshot.setGoodName(supplychainGoodVo.getDefineName());//当时的商品名称
                                goodSnapshot.setIsThirdParty(0);//是否第三方商品，否为0
                                goodSnapshot.setGoodPriceTitle(supplychainGoodVo.getGoodTitle());//当时归属的类型名
                                goodSnapshot.setGoodUnitName(supplychainGoodVo.getGoodUnit());//当时单位名
                                goodSnapshot.setCreateTime(new Date());
                                goodSnapshot.setNormalPrice(supplychainGoodVo.getSellPrice());
                                goodSnapshot.setNormalQuantity(supplychainGoodVo.getGoodNumber());
                                goodSnapshot.setGoodPriceId(supplychainGoodVo.getGoodPriceId());
                                GoodBarcode barcodeByPriceId = GoodBarcodeRemoteService.getGoodBarcodeByPriceId(supplychainGoodVo.getPriceId(), getHttpServletRequest());
                                if(barcodeByPriceId != null){
                                    goodSnapshot.setGoodBarcode(barcodeByPriceId.getBarcode());
                                }

                                //每个供应商只查一次
                                if(supOrderEntry.getShippingPhone() == null){
                                    SupplychainShopProperties supplychainShopPropertiesById1 = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesById(supplychainGoodVo.getShopId(), getHttpServletRequest());
                                    if(supplychainShopPropertiesById1 != null){
                                        supOrderEntry.setShippingPhone(supplychainShopPropertiesById1.getPhoneNumber());//发货人联系电话
                                    }
                                }
                                //添加子单快照
                                dataAccessManager.addOrderGoodSnapshot(goodSnapshot);
                            }
                        }

                        //添加排号
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("everyday", "everyday");
                        OrderEntry orderEntryDaySortNumberDesc = dataAccessManager.getOrderEntryDaySortDesc(map);
                        if (orderEntryDaySortNumberDesc != null) {
                            Integer daySortNumber = orderEntryDaySortNumberDesc.getDaySortNumber();
                            if (daySortNumber == null) {
                                daySortNumber = 0;
                            }
                            supOrderEntry.setDaySortNumber(daySortNumber + 1);
                        } else {
                            supOrderEntry.setDaySortNumber(1);
                        }
                        /*if(y.getDistributionCost() != null){
                            supPrice+=y.getDistributionCost();
                        }*/
                        supOrderEntry.setTotalPrice(supPrice);//* 订单总金额 配送费用 + 商品总金额
                        supOrderEntry.setActualPrice(supPrice);//* 订单总金额 配送费用 + 商品总金额

                        dataAccessManager.addOrderEntry(supOrderEntry);
                    }
            });
        }

        //已过滤不能购买的商品
        //根据仓库拆单
        if(isSupMoq != null && isSupMoq.size() > 0 ){
            for (SupplychainGoodVo supplychainGoodVo : isSupMoq) {
                for (SupplychainGoodAndShopVO supplychainGoodAndShopVO : goodList) {
                    for (int p = 0; p < supplychainGoodAndShopVO.getList().size(); p++) {
                        if(supplychainGoodVo.getPriceId().equals(supplychainGoodAndShopVO.getList().get(p).getPriceId())){
                            supplychainGoodAndShopVO.getList().remove(p);
                        }
                    }
                }
            }
        }

        for (SupplychainGoodAndShopVO good : goodList) {
            //计算配送距离 .getCoveringDistance()
            int cDistribution = SimpleLocationUtils.simpleDistance(good.getLatitude(), good.getLongitude(), sysUserAddressById.getLatitude(), sysUserAddressById.getLongitude());

            if (good.getList() != null && good.getList().size() > 0) {
                OrderEntry orderEntry = new OrderEntry();
                //仓库子订单编号
                String parentUniquePrimaryKey = uniquePrimaryKey("TN");
                long goodPrice = 0;//该仓库的商品总价
                long distributionFee = good.getDistributionCost();//配送费用

                for (SupplychainGoodVo goodVo : good.getList()) { //仓库商品
                    //GoodShopCart goodShopCartById = null;
                    GoodBarcode barcode = null;

                    try {
                        //远程调用购物车商品查询
                        //goodShopCartById = GoodShopCartRemoteService.getGoodShopCartById(sysUser.getId(), goodVo.getId(), getHttpServletRequest());
                        //远程调用查询商品条码
                        barcode = GoodBarcodeRemoteService.getGoodBarcodeByPriceId(goodVo.getPriceId(), getHttpServletRequest());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    //添加商品快照记录
                    OrderGoodSnapshot goodSnapshot = new OrderGoodSnapshot();
                    //获取购买的商品数量
                    Integer goodNumber = (Integer) goodsMap.get(goodVo.getId());
                    if (goodNumber != null) {
                        //计算金额
                        if (goodNumber != null && goodNumber > 0) {
                            goodSnapshot.setNormalQuantity(goodNumber);
                            goodPrice += goodVo.getSellPrice() * goodNumber;//计算商品金额
                            orderTotalGoodsMoney += goodVo.getSellPrice() * goodNumber; //计算订单总金额
                            //修改库存  改为支付成功后
                            // SupplyChainGoodRemoteService.changSupplychainGoodByGoodId(goodVo.getId(), goodNumber, 0, getHttpServletRequest());
                        }
                    }

                //商品条码
                if (barcode != null) {
                    goodSnapshot.setGoodBarcode(barcode.getBarcode());
                }
                //goodSnapshot.setOrderId(orderEntry.getId());//订单id
                goodSnapshot.setGoodId(goodVo.getId());//商品id
                goodSnapshot.setGoodTemplateId(goodVo.getGoodTemplateId());//商品模版ID
                goodSnapshot.setGoodName(goodVo.getDefineName());//当时的商品名称
                goodSnapshot.setIsThirdParty(0);//是否第三方商品，否为0
                goodSnapshot.setGoodPriceTitle(goodVo.getGoodTitle());//当时归属的类型名
                goodSnapshot.setGoodUnitName(goodVo.getGoodUnit());//当时单位名
                goodSnapshot.setCreateTime(new Date());
                goodSnapshot.setNormalPrice(goodVo.getSellPrice());
                goodSnapshot.setGoodPriceId(goodVo.getGoodPriceId());
                //添加子单快照
               // dataAccessManager.addOrderGoodSnapshot(goodSnapshot);

                    //添加主订单商品快照 *取消主单快照
                    goodSnapshot.setOrderId(primaryOrderEntry.getId());
                    //OrderGoodSnapshot goodSnapshot1 = new OrderGoodSnapshot();
                   // goodSnapshot.setId(goodSnapshot1.getId());
                    dataAccessManager.addOrderGoodSnapshot(goodSnapshot);
                    try {
                        //购物车商品状态改为已购买
                        GoodShopCartRemoteService.changeGoodShopCartStauts(goodVo.getId(), "2", getHttpServletRequest());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                orderTotalDistributionCostMoney += distributionFee;//总配送费
               /* orderEntry.setSequenceNumber(primaryUniquePrimaryKey);//序列号
                orderEntry.setOrderSequenceNumber(parentUniquePrimaryKey);//订单的序列号(用于展示)
                orderEntry.setPartnerUserId(sysUser.getId());//下单用户ID 匿名时可为0或空
                orderEntry.setType(OrderTypeEnum.SUPPLYCHAIN.getKey());//订单类型
                orderEntry.setStatus(OrderStatus.ORDER_TYPE_UNCONFIRMED.getKey());//订单状态
                orderEntry.setDeliverStatus(OrderDeliverStatus.UNSHIPPED.getKey());//发货状态
                orderEntry.setRefundStatus(0);//退款状态 0表示未退货 同时可用于记录是否已打款状态
                orderEntry.setTotalPrice(distributionFee + goodPrice);//* 订单总金额 配送费用 + 商品总金额*/

                //为主单添加发货用户 只添加一次
                if (i == 0) {
                    i += 1;
                    primaryOrderEntry.setShippingNickName(good.getShopName());//发货方
                    primaryOrderEntry.setReceiptLocation(cDistribution+"");//配送距离
                    primaryOrderEntry.setShippingProvince(good.getProvince());//发货省份
                    primaryOrderEntry.setShippingCity(good.getCity());//发货市
                    primaryOrderEntry.setShippingDistrict(good.getDistrict());//发货区
                    primaryOrderEntry.setShippingAddress(good.getAddress());//发货详细地址
                    primaryOrderEntry.setShippingPhone(good.getPhoneNumber());//发货人联系电话
                    primaryOrderEntry.setSaleUserId(good.getShopUserId());//仓管id
                    primaryOrderEntry.setShippingUserId(good.getShopUserId());//仓管id
                }
               /* //交易类型，微信系：JSAPI、NATIVE、APP等
                orderEntry.setTransType(String.valueOf(WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey()));//交易类型
                //orderEntry.setUserSource("");//用户来源
                //orderEntry.setDaySortNumber("");//当天排号 相对于发货方
                orderEntry.setPushType(2);//推送类型，0表示不推送 1时表示下单时推送 2时表示支付后推送
                orderEntry.setSaleUserId(good.getShopUserId());//实际销售的用户ID（销售仓仓管id）
                orderEntry.setReceiptUserId(receiptUserId);//收货人用户ID
                orderEntry.setReceiptNickName(receiptNickName);//收货人昵称
                orderEntry.setReceiptProvince(sysUserAddressById.getProvince());//收货省份
                orderEntry.setReceiptCity(sysUserAddressById.getCity());//收货城市
                orderEntry.setReceiptDistrict(sysUserAddressById.getDistrict());//收货区域
                orderEntry.setReceiptAddress(finalDetaiAdd);//收货详细地址
                orderEntry.setShippingProvince(good.getProvince());//发货省份
                orderEntry.setShippingCity(good.getCity());//发货市
                orderEntry.setShippingDistrict(good.getDistrict());//发货区
                orderEntry.setShippingAddress(good.getAddress());//发货详细地址
                orderEntry.setShippingPhone(good.getPhoneNumber());//发货人联系电话
                orderEntry.setThirdPartyDeliveryStatus(good.getDistributionType());//配送方式
                orderEntry.setReceiptPhone(receiptPhone);//收货人电话
                orderEntry.setCurrentLocation(currentLocation);//下单时所在地理位置
                orderEntry.setRemark(remark);//订单描述
                orderEntry.setActualPrice(distributionFee + goodPrice);//订单总费用 商品+配送费《暂时其它没有折扣业务》
                //orderEntry.setTotalPrice(goodPrice);//实收费用
                //orderEntry.setDiscountPrice("");//优惠的费用
                orderEntry.setDistributionFee(distributionFee);//配送费用
                orderEntry.setCreateTime(new Date());//订单创建时间
                orderEntry.setIsJoin(1);//是否为加盟店 1为是
                orderEntry.setIsThirdParty(0);//是否为第三方 0为平台 不等于0为第三方
                orderEntry.setThirdPartyDelivery(0);//是否使用第三方物流 0否 1是
                orderEntry.setPartnerId(sysUser.getId());//下游用户ID
                orderEntry.setShippingUserId(good.getShopUserId());*/
                //添加子订单
                //dataAccessManager.addOrderEntry(orderEntry);
            }
        }


        //验证订单金额
        JSONObject jsonObject = new JSONObject();
        if (orderTotalGoodsMoney <= 0) {
            //是否存在不能购买的商品
            if (goodOut.size() > 0) {
                jsonObject.put("flag", 1);
            } else {
                jsonObject.put("flag", 0);
            }
            jsonObject.put("orderPrice", orderTotalGoodsMoney);
            jsonObject.put("goodOut", goodOut);
            jsonObject.put("orderNumber", primaryUniquePrimaryKey);
            return success(jsonObject);
        }
        primaryOrderEntry.setOrderSequenceNumber(primaryUniquePrimaryKey);//序列号
        primaryOrderEntry.setType(OrderTypeEnum.SUPPLYCHAINPURCHASE.getKey());//订单类型
        primaryOrderEntry.setStatus(OrderStatus.ORDER_TYPE_UNCONFIRMED.getKey());//订单状态
        primaryOrderEntry.setDeliverStatus(OrderDeliverStatus.UNSHIPPED.getKey());//发货状态
        primaryOrderEntry.setRefundStatus(0);//退款状态 0表示未退货 同时可用于记录是否已打款状态
       /* if(paymentType != null){//支付类型
            orderEntry.setPaymentType(Integer.parseInt(paymentType));
        }*/
        //交易类型，微信系：JSAPI、NATIVE、APP等
        primaryOrderEntry.setTransType(String.valueOf(WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey()));//交易类型
        //orderEntry.setUserSource("");//用户来源
        //orderEntry.setDaySortNumber("");//当天排号 相对于发货方
        primaryOrderEntry.setPushType(2);//推送类型，0表示不推送 1时表示下单时推送 2时表示支付后推送
        //orderEntry.setSaleUserId(good.getShopId());//实际销售的用户ID（销售仓）
        primaryOrderEntry.setReceiptUserId(receiptUserId);//收货人用户ID
        primaryOrderEntry.setReceiptNickName(receiptNickName);//收货人昵称
        primaryOrderEntry.setReceiptProvince(sysUserAddressById.getProvince());//收货省份
        primaryOrderEntry.setReceiptCity(sysUserAddressById.getCity());//收货城市
        primaryOrderEntry.setReceiptDistrict(sysUserAddressById.getDistrict());//收货区域
        primaryOrderEntry.setReceiptAddress(detaiAdd);//收货具体地址(尽量不要包含省市区)
        primaryOrderEntry.setReceiptPhone(receiptPhone);//收货人电话

        primaryOrderEntry.setCurrentLocation(currentLocation);//下单时所在地理位置
        primaryOrderEntry.setRemark(remark);//订单描述
        //  primaryOrderEntry.setActualPrice(orderTotalGoodsMoney + orderTotalDistributionCostMoney);//订单总费用 商品+配送费《暂时其它没有折扣业务》
        primaryOrderEntry.setTotalPrice(orderTotalGoodsMoney + orderTotalDistributionCostMoney);//实收费用
        //orderEntry.setDiscountPrice("");//优惠的费用
        primaryOrderEntry.setDistributionFee(orderTotalDistributionCostMoney);//配送费用
        primaryOrderEntry.setCreateTime(new Date());//订单创建时间
        primaryOrderEntry.setIsJoin(1);//是否为加盟店 1为是
        primaryOrderEntry.setIsThirdParty(0);//是否为第三方 0为平台 不等于0为第三方receipt_address
        primaryOrderEntry.setThirdPartyDelivery(0);//是否使用第三方物流 0否 1是
        primaryOrderEntry.setPartnerId(sysUser.getId());//下游用户ID
        primaryOrderEntry.setPartnerUserId(shopUserId);//下单用户ID 匿名时可为0或空 店主id
        HashMap<String, Object> map = new HashMap<>();
        map.put("isPrimaryOrder", "isPrimaryOrder");
        map.put("everyday", "everyday");
        OrderEntry orderEntryDaySortNumberDesc = dataAccessManager.getOrderEntryDaySortDesc(map);
        if (orderEntryDaySortNumberDesc != null) {
            Integer daySortNumber = orderEntryDaySortNumberDesc.getDaySortNumber();
            if (daySortNumber == null) {
                daySortNumber = 0;
            }
            primaryOrderEntry.setDaySortNumber(daySortNumber + 1);
        } else {
            primaryOrderEntry.setDaySortNumber(1);
        }
        //《 添加主单 》
        dataAccessManager.addOrderEntry(primaryOrderEntry);
        //添加支付记录
        PaymentTransactionLogger paymentTransactionLogger = new PaymentTransactionLogger();
        try {
            paymentTransactionLogger.setUserId(sysUser.getId());
            paymentTransactionLogger.setUserName(sysUser.getShowName());
            paymentTransactionLogger.setTransSequenceNumber(primaryUniquePrimaryKey);
            paymentTransactionLogger.setTransStatus(OrderStatus.ORDER_TYPE_UNCONFIRMED.getKey());
            paymentTransactionLogger.setTransStatus(TransTypeEnum.PAYMENT.getKey());
            paymentTransactionLogger.setPartnerTradeNumber(primaryUniquePrimaryKey);
            paymentTransactionLogger.setCurrencyType("RMB");
            paymentTransactionLogger.setTransTotalAmount(orderTotalGoodsMoney);
            paymentTransactionLogger.setCreateTime(new Date());
            paymentTransactionLogger.setTransCreateTime(new Date());
            paymentTransactionLogger.setRefundStatus(0);
            paymentTransactionLogger.setPaymentType(PayTypeEnum.TRANS_TYPE_WEI.getKey() + "");
            PaymentTransactionLoggerService.addPaymentTransactionLogger(paymentTransactionLogger, getHttpServletRequest());

        } catch (Exception e) {
            e.printStackTrace();
        }

        //是否存在不能购买的商品
        if (goodOut.size() > 0) {
            jsonObject.put("flag", 1);
        } else {
            jsonObject.put("flag", 0);
        }
        jsonObject.put("orderPrice", primaryOrderEntry.getTotalPrice());
        jsonObject.put("goodOut", goodOut);
        jsonObject.put("orderNumber", primaryUniquePrimaryKey);
        return success(jsonObject);
    }


    @Override
    public JSONObject getSmallProgramShopWxPay() {
        String orderNumber = getUTF("orderNumber");
        SysUser sysUser = null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //是否微信支付
        SysUser sysUser1 = null;
        try {
            sysUser1 = SysUserRemoteService.getSysUser(sysUser.getId(), getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("orderSequenceNumber", orderNumber);
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(map);
        if (orderEntrys != null && orderEntrys.size() > 0) {
            String remoteAddr = getHttpServletRequest().getRemoteAddr();
            long totalPrice = orderEntrys.get(0).getTotalPrice();
            if (totalPrice <= 0) {
                logger.info("=============小程序商家端微信支付金额：" + totalPrice);
                return fail("支付金额错误");
            }
            logger.info("openId=" + sysUser1.getOpenId());
            Map<String, String> orderBySmallProgram = WeiXinPayUtils.createOrderBySmallProgram(orderEntrys.get(0).getOrderSequenceNumber(), totalPrice + "", remoteAddr, sysUser1.getOpenId(), "确认支付", WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey(), WeixinCallcackType.BUSINESS_SMALL_PROGRAM_ORDER.getKey(), sysUser.getId());
            return success(orderBySmallProgram);
        } else {
            return fail("没有可购买商品");
        }
    }

    /**
     * 微信商家下单-确认支付
     *
     * @param
     * @return * 小程序统一下单
     * * @date 2019/4/20
     * * @param out_trade_no：交易订单号（这是我们系统生成的订单号）
     * * param  otal_fee：金额（分）
     * * param  ip：ip 根据每个用户请求 获取ip
     * * param  openId：注册的时候 会添加到 sys_user 表中的字段（小程序使用，其他传空）
     * * param  body：支付信息说明（自己写）
     * * param  weiXinTerminalPayType：微信支付类型 详情见 WeiXinTerminalPayTypeEnum
     * @author sqd
     * @date 2019/4/20
     */
    @Override
    public JSONObject isPay() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (user == null) {
            return fail("用户登录失效");
        }
        String orderId = getUTF("orderId");
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(orderId);
        long payPrice = 0;
        if (orderEntry != null) {
            if (orderEntry.getFindPrice() != null) {
                payPrice = orderEntry.getTotalPrice() - orderEntry.getFindPrice();
            } else {
                payPrice = orderEntry.getTotalPrice();
            }

        }
        String ip = getHttpServletRequest().getRemoteAddr();
        if (payPrice > 0) {
            Map<String, String> orderBySmallProgram = WeiXinPayUtils.createOrderBySmallProgram(orderEntry.getOrderSequenceNumber(), orderEntry.getActualPrice() + "", ip, user.getOpenId(), "鸵鸟商家", WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey(), WeixinCallcackType.BUSINESS_SMALL_PROGRAM_ORDER.getKey(), user.getId());
            return success(orderBySmallProgram);
        } else {
            return fail("支付失败,订单支付金额有误");
        }
    }

    /**
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/4/28
     */
    public JSONObject payCallback() {
        String orderSequenceNumber = getUTF("orderNumber");
        String actualPriceStr = getUTF("actualPrice", null);
        OrderEntry orderEntry = new OrderEntry();
        orderEntry.setSequenceNumber(orderSequenceNumber);
        orderEntry.setId(null);

        orderEntry.setPaymentType(PaymentTypeEnum.BALANCE.getChannelId());
        orderEntry.setStatus(OrderStatus.ORDER_TYPE_PAID.getKey());
        orderEntry.setPaymentTime(new Date());
        //修改子订单状态
        orderEntry.setPaymentType(PayTypeEnum.TRANS_TYPE_BALANCE.getKey());
        dataAccessManager.changeOrderEntry(orderEntry);
        //添加子单操作记录
        HashMap<String, Object> mapObj = new HashMap<>();
        mapObj.put("sequenceNumbers", orderSequenceNumber);
        addStaueLog(mapObj, 0);
        mapObj.clear();
        //修改主订单状态
        if (actualPriceStr != null) {
            orderEntry.setActualPrice(Long.parseLong(actualPriceStr));
        }
        orderEntry.setSequenceNumber(null);
        orderEntry.setOrderSequenceNumber(orderSequenceNumber);
        orderEntry.setStatus(OrderStatus.ORDER_TYPE_HAVEORDER.getKey());
        dataAccessManager.changeOrderEntry(orderEntry);
        mapObj.put("sequenceNumbers", orderSequenceNumber);

        mapObj.clear();
        //修改库存
        mapObj.put("orderSequenceNumberOne",orderSequenceNumber);
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(mapObj);
        List<GoodIdAndGoodNumberVO> goodIdAndGoodNumberVO = new ArrayList<>();
        if(orderEntrys != null && orderEntrys.size() > 0){
            List<OrderGoodSnapshot> orderGoodSnapshotByOrderId = dataAccessManager.getOrderGoodSnapshotByOrderId(orderEntrys.get(0).getId());
            for (OrderGoodSnapshot goodSnapshot : orderGoodSnapshotByOrderId) {
                GoodIdAndGoodNumberVO goods = new GoodIdAndGoodNumberVO();
                goods.setNumber(goodSnapshot.getNormalQuantity());
                goods.setGoodId(goodSnapshot.getGoodId());
                goodIdAndGoodNumberVO.add(goods);
            }
        }
        changeStock(goodIdAndGoodNumberVO,"");
        //添加主单操作记录
        addStaueLog(mapObj, 0);
        //添加支付账期记录
        //addPaymentEstimateRevenue(orderSequenceNumber, 0);
        return success();
    }

    /**
     * 添加订单状态操作记录
     *
     * @param
     * @return void
     * @author sqd
     * @date 2019/4/25
     */
    public void addStaueLog(Map<String, Object> map, int isPos) {
        List<OrderEntry> orderEntrys = null;
        if (isPos == 0) {
            orderEntrys = dataAccessManager.getOrderEntrys(map);
        } else {
            orderEntrys = dataAccessManager.getOrderEntrysPos(map);
        }
        if (orderEntrys.size() > 0) {
            OrderStateLogger orderStateLogger = new OrderStateLogger();
            orderStateLogger.setOrderId(orderEntrys.get(0).getId());
            orderStateLogger.setShippingUserId(orderEntrys.get(0).getShippingUserId());
            orderStateLogger.setOrderType(orderEntrys.get(0).getType());
            orderStateLogger.setBeforeStatus(1);
            orderStateLogger.setStatus(OrderStatus.ORDER_TYPE_PAID.getKey());
            orderStateLogger.setOperatorTime(new Date());
            SysUser user = getUser();
            if (user != null) {
                orderStateLogger.setOperatorUserId(user.getRoleId());
                orderStateLogger.setOperatorName(user.getShowName());
            }
            //添加状态订单操作记录
            dataAccessManager.addOrderStateLogger(orderStateLogger);
        }
    }


    /**
     * 添加支付账期记录
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/4/26
     */
    /*
      public void addPaymentEstimateRevenue(String primarySequenceNumbers, int isPos) {
        SysUser sysUser = getUser();
        if(sysUser == null){
            return;
        }
        String title = "";
        HashMap<String, Object> mapObj = new HashMap<>();
        List<OrderEntry> orderEntrys = new ArrayList<>();
        if (isPos == 0) {
            //查询子单
            mapObj.put("sequenceNumbers", primarySequenceNumbers);
            orderEntrys = dataAccessManager.getOrderEntrys(mapObj);
            title = "pos机收银";
        } else {
            mapObj.put("orderSequenceNumber", primarySequenceNumbers);
            orderEntrys = dataAccessManager.getOrderEntrysPos(mapObj);
            title = "小程序下单支付";
        }
        if (orderEntrys == null || orderEntrys.size() <= 0) {
            return;
        }
        String userId = "";
        if(sysUser.getLeaderUserId() != null && !sysUser.getLeaderUserId().equals("")){
            userId = sysUser.getLeaderUserId();
        }else{
            userId = sysUser.getId();
        }

        for (OrderEntry entry : orderEntrys) {
            //查询用账期设置
            PaymentEstimateRevenue paymentEstimateRevenue = new PaymentEstimateRevenue();
            //支付成功添加
            PaymentSettlementSet paymentSettlementSet = PaymentSettlementSetService.getPaymentSettlementSetByUserId(entry.getSaleUserId(), getHttpServletRequest());
            if(paymentSettlementSet != null){
                paymentEstimateRevenue.setStatus(PaymentEstimateRevenueStatusEnum.INITIATE_WITHDRAWAL.getKey());
                long money = 0;
                if(paymentSettlementSet.getType().equals(1)){ //扣点
                    money = entry.getActualPrice() * (10000 - paymentSettlementSet.getCablePoint()) / 10000;//向下取整

                }else if(paymentSettlementSet.getType().equals(2)){//反点
                    money = entry.getActualPrice() * (10000 + paymentSettlementSet.getCablePoint()) / 10000;//向下取整
                } else{
                    money = entry.getActualPrice();
                }
                paymentEstimateRevenue.setActualIncome(money);// 实际收入
            }else{
                //直接到账
                paymentEstimateRevenue.setStatus(PaymentEstimateRevenueStatusEnum.COMPLETED_WITHDRAWAL.getKey());
                paymentEstimateRevenue.setActualIncome(entry.getActualPrice());// 实际收入
                //获取用户当前额度
                PaymentCurrencyAccount paymentCurrencyAccountById = PaymentCurrencyAccountService.getPaymentCurrencyAccountById(entry.getSaleUserId(), getHttpServletRequest());
                if(paymentCurrencyAccountById != null){
                    PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger = new PaymentCurrencyOffsetLogger();
                    paymentCurrencyOffsetLogger.setUserId(entry.getSaleUserId());
                    paymentCurrencyOffsetLogger.setCurrencyType(CurrencyTypeEnum.CURRENCY_TYPE_DEFAULT.getKey());
                    paymentCurrencyOffsetLogger.setBeforeAmount(paymentCurrencyAccountById.getCurrentAmount());
                    paymentCurrencyOffsetLogger.setOffsetAmount(paymentCurrencyAccountById.getCurrentAmount()+entry.getActualPrice());//变化后的额度
                    paymentCurrencyOffsetLogger.setTransTitle(title);
                    paymentCurrencyOffsetLogger.setTransType(entry.getPaymentType()+"");
                    paymentCurrencyOffsetLogger.setRelationTransType(RelationTransTypeEnum.RelationTrans_TYPE_INCOME.getKey());
                    paymentCurrencyOffsetLogger.setRelationTransSequence(entry.getOrderSequenceNumber());

                    paymentCurrencyAccountById.setCurrentAmount(paymentCurrencyAccountById.getCurrentAmount()+entry.getActualPrice());
                    //修改余额
                    PaymentCurrencyAccountService.changePaymentCurrencyAccount(paymentCurrencyAccountById,getHttpServletRequest());
                    //添加余额变动记录
                    PaymentCurrencyOffsetLoggerRemoteService.addPaymentCurrencyOffsetLogger(paymentCurrencyOffsetLogger,getHttpServletRequest());
                }

                //添加到账记录
                PaymentEstimateRevenueLogger paymentEstimateRevenueLogger = new PaymentEstimateRevenueLogger();
                paymentEstimateRevenueLogger.setActualIncome(entry.getActualPrice());
                paymentEstimateRevenueLogger.setOrderAmount(entry.getActualPrice());
                paymentEstimateRevenueLogger.setOrderId(entry.getId());
                paymentEstimateRevenueLogger.setUserId(entry.getSaleUserId());
                paymentEstimateRevenueLogger.setCreateTime(new Date());
                PaymentEstimateRevenueLoggerService.addPaymentEstimateRevenueLogger(paymentEstimateRevenueLogger,getHttpServletRequest());
            }

                paymentEstimateRevenue.setOrderAmount(entry.getActualPrice()); //订单金额
                paymentEstimateRevenue.setUserId(sysUser.getId());
                paymentEstimateRevenue.setOrderId(entry.getId());
                paymentEstimateRevenue.setShopsName(entry.getShippingNickName());//商家名称
                paymentEstimateRevenue.setRequestTime(new Date());

            try {
                //添加账期
                PaymentEstimateRevenueService.addPaymentEstimateRevenue(paymentEstimateRevenue, getHttpServletRequest());
            } catch (Exception e) {
                logger.info("===================添加支付账期数据失败===================");
            }
        }


            try {
                //修改支付流水
                mapObj.put("primarySequenceNumbers", primarySequenceNumbers);
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                mapObj.put("paymentTime", format.format(new Date()));
                PaymentTransactionLoggerService.changePaymentTransactionLoggerMap(mapObj, getHttpServletRequest());
            } catch (Exception e) {
                logger.info("===================修改支付流水失败===================");
                e.printStackTrace();
            }
    }*/

    @Override
    public JSONObject getOrderEntrysPos() {
        HashMap<String, Object> map = new HashMap<>();

        String name = getUTF("name", null);
        int status = getIntParameter("status", StatusEnum.ALL.getKey());
        int type = getIntParameter("type", -1);
        int startActualPrice = getIntParameter("startActualPrice", -1);
        int endActualPrice = getIntParameter("endActualPrice", -1);
        int startTotalPrice = getIntParameter("startTotalPrice", -1);
        int endTotalPrice = getIntParameter("endTotalPrice", -1);
        int paymentType = getIntParameter("paymentType", -1);
        int refundStatus = getIntParameter("refundStatus", -1);
        String createStartTime = getUTF("createStartTime", null);
        String createEndTime = getUTF("createEndTime", null);
        String orderSequenceNumber = getUTF("orderSequenceNumber", null);
        String shippingNickNameOrPhone = getUTF("shippingNickNameOrPhone", null);
        String receiptNickNameOrPhone = getUTF("receiptNickNameOrPhone", null);
        String orderNumberOrPhone = getUTF("orderNumberOrPhone", null);
        String shopName = getUTF("shopName", null);
        HashMap<String, Object> maObj = new HashMap<>(); // key shopId, val shopName
        //查询所有门店
        List<SupplychainShopProperties> supplychainShopPropertiesByShopName = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByShopName(null, getHttpServletRequest());
        if (supplychainShopPropertiesByShopName != null && supplychainShopPropertiesByShopName.size() > 0) {
            for (SupplychainShopProperties supplychainShopProperties : supplychainShopPropertiesByShopName) {
                maObj.put(supplychainShopProperties.getId(), supplychainShopProperties.getShopName());
            }
        }
        if (shopName != null) {
            List<SupplychainShopProperties> shopNames = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByShopName(shopName, getHttpServletRequest());
            List<String> shopId = new ArrayList<>();
            if (shopNames != null && shopNames.size() > 0) {
                shopId = shopNames.stream().map(SupplychainShopProperties::getId).collect(Collectors.toList());
                map.put("shopId", shopId);
            } else {
                shopId.add("-1");
                map.put("shopId", shopId);
            }
        }
        map.put("orderNumberOrPhone", orderNumberOrPhone);
        map.put("type", type);
        map.put("status", status);
        map.put("pageIndex", getPageStartIndex(getPageSize()));
        map.put("pageSize", getPageSize());
        map.put("name", name);
        map.put("orderSequenceNumber", orderSequenceNumber);
        map.put("paymentType", paymentType);
        map.put("refundStatus", refundStatus);


        map.put("shippingNickNameOrPhone", shippingNickNameOrPhone);
        map.put("receiptNickNameOrPhone", receiptNickNameOrPhone);
        map.put("startActualPrice", startActualPrice);
        map.put("endActualPrice", endActualPrice);
        map.put("startTotalPrice", startTotalPrice);
        map.put("endTotalPrice", endTotalPrice);


        if (createEndTime != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Format f = new SimpleDateFormat("yyyy-MM-dd");
            //结束时间加一天
            Date sDate = null;
            try {
                sDate = sdf.parse(createEndTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar c = Calendar.getInstance();
            c.setTime(sDate);
            c.add(Calendar.DAY_OF_MONTH, 1);
            sDate = c.getTime();
            createEndTime = f.format(sDate);
        }

        map.put("createStartTime", createStartTime);
        map.put("createEndTime", createEndTime);
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrysPos(map);
        int count = dataAccessManager.getOrderEntryPosCount(map);
        //获取用户id集合
        if (orderEntrys != null && orderEntrys.size() > 0) {
            List<String> partnerId = orderEntrys.stream().map(OrderEntry::getPartnerId).collect(Collectors.toList());
            if (partnerId != null && partnerId.size() > 0) {
                List<SysUser> sysUserInIds = SysUserRemoteService.getSysUserInIds(partnerId, getHttpServletRequest());
                if (sysUserInIds != null && sysUserInIds.size() > 0) {
                    orderEntrys.forEach(x -> {
                        String id = x.getPartnerId();
                        Optional<SysUser> first = sysUserInIds.stream().filter(item -> item.getId().equals(id)).findFirst();
                        if (first.isPresent()) {
                            SysUser sysUser = first.get();
                            x.setPartnerUserName(sysUser.getRealName());
                        }
                    });
                }

            }
        }

        //封装门店名称
        ArrayList<OrderEntryPosFormatDate> orderEntryPosFormatDate = new ArrayList<>();
        if (orderEntrys != null && orderEntrys.size() > 0) {
            for (OrderEntry orderEntry : orderEntrys) {
                OrderEntryPosFormatDate orderEntryPosFormatDate1 = new OrderEntryPosFormatDate();
                orderEntry.setShopName((String) maObj.get(orderEntry.getSaleUserId()));
                if (orderEntry.getStatus() == OrderStatus.ORDER_TYPE_REFUND.getKey()) {
                    if (orderEntry.getFindPrice() != null) {
                        orderEntry.setRefundPrice(orderEntry.getTotalPrice() - orderEntry.getFindPrice());
                    } else {
                        orderEntry.setRefundPrice(orderEntry.getActualPrice());
                    }
                }


                //最终实收金额 = 实收金额 - 退款金额
                if (orderEntry.getActualPrice() != null) {
                    orderEntry.setFinalActualPrice(orderEntry.getActualPrice() - orderEntry.getRefundPrice());
                }



                BeanUtils.copyProperties(orderEntry, orderEntryPosFormatDate1);
                orderEntryPosFormatDate.add(orderEntryPosFormatDate1);
            }

        }

        return success(orderEntryPosFormatDate, count);
    }

    /**
     * 订单打印
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @author xh
     * @date 2019/4/28
     * @date 2019/6/24
     */
    @Override
    public JSONObject printOrder() {
        String orderSequenceNumber = getUTF("orderNumber");
        HashMap<String, Object> map = new HashMap<>();
        //查询主单
        map.put("orderSequenceNumber", orderSequenceNumber);
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(map);
        if (orderEntrys != null && orderEntrys.size() <= 0) {
            return fail("订单数据为空");
        }
        map.clear();
        //主单对应的所以子单
        map.put("sequenceNumbers", orderSequenceNumber);
        List<OrderEntry> subOrderEntrys = dataAccessManager.getOrderEntrys(map);
        List<String> orderIds = new ArrayList<>();
        if (subOrderEntrys.size() <= 0) {
            orderIds.add(orderSequenceNumber);
        } else {
            orderIds = subOrderEntrys.stream().map(OrderEntry::getId).collect(Collectors.toList());
        }
        map.clear();
        SysUser sysUser = null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //查询用户店铺
        SupplychainShopProperties userShop = null;
        //远程调用用户商店绑定信息
        List<SupplychainShopBindRelationship> shopBindRelationships = SupplychainShopBindRelationshipRemoteService.getShopBindRelationships(5, sysUser.getId(), getHttpServletRequest());
        if (shopBindRelationships != null && shopBindRelationships.size() > 0) {
            userShop = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesById(shopBindRelationships.get(0).getShopId(), getHttpServletRequest());
        } else {
            userShop = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(sysUser.getId(), getHttpServletRequest());
        }
        OrderEntryPosVO orderEntryPosVO = new OrderEntryPosVO();
        if(userShop != null ) {
            orderEntryPosVO.setShopName(userShop.getShopName());
        }
        //查询订单所以商品快照
        map.put("orderIds", orderIds);
        List<OrderGoodSnapshot> orderGoodSnapshotList = dataAccessManager.getOrderGoodSnapshotByMap(map);
        //PrintFormat printFormat = new PrintFormat("217800901|6ptmhvsr");
        PrintFormat printFormat = new PrintFormat(userShop.getBindPrinterCode());
        printFormat.addTitle("打印小票");
        printFormat.addMenu();
        printFormat.append("-------------------------------------------------");
        printFormat.enter(); // 换行
        for (OrderGoodSnapshot goodSnapshot : orderGoodSnapshotList) {
            if (goodSnapshot.getNormalPrice() != null && goodSnapshot.getNormalQuantity() != null) {
                printFormat.addGoods((goodSnapshot.getGoodName()), (goodSnapshot.getNormalPrice() / 100) + "", (goodSnapshot.getNormalQuantity() / 100) + "", ((goodSnapshot.getNormalPrice() * goodSnapshot.getNormalQuantity()) / 100) + "");
            }
        }
        //收货地址
        String address = orderEntrys.get(0).getReceiptProvince() + orderEntrys.get(0).getReceiptCity() + orderEntrys.get(0).getReceiptDistrict() + orderEntrys.get(0).getReceiptAddress();
        //收货人电话
        String receiptPhone = orderEntrys.get(0).getReceiptPhone();
        //下单时间
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String datetime = format.format(orderEntrys.get(0).getCreateTime());

        printFormat.addProperty("备注", orderEntrys.get(0).getRemark() == null ? "" : orderEntrys.get(0).getRemark());
        printFormat.addProperty("合计", orderEntrys.get(0).getActualPrice() + "");
        printFormat.addProperty("收货地点", address);
        printFormat.addProperty("联系电话", receiptPhone);
        printFormat.addProperty("下单时间", datetime);
        printFormat.addProperty("店铺名称", orderEntryPosVO.getShopName());
        printFormat.addQRContent("www.tuoniaolife.com");
        try {
            printFormat.print();
        } catch (Exception e) {
            return fail("打印机未准备就绪");
        }
        return success(printFormat);
    }

    /**
     * 打印机编码和用户绑定
     * 添加打印机（通过userId绑定）
     * @param
     * @return void
     * @author xh
     * @date 2019/6/24
     */
    public JSONObject addPrinter() {
        SupplychainShopProperties supplychainShopProperties = null;
        //通过电话号码查询对应的商户
        String phoneNum = getUTF("phoneNum", null);
        String bindPrinterCode = getUTF("bindPrinterCode", null);
        if (phoneNum == null || phoneNum.equals("")) {
            return fail("电话号码不能为空！");
        }
        SysUser sysUser = null;
        //查询是否有该用户
        try {
            sysUser = SysUserRemoteService.getSysUserByPhoneNumForRemote(phoneNum, getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        //如果有用户 查询是否有门店
        if (sysUser != null) {
            //有问题
            //supplychainShopProperties = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(sysUser.getId(), getHttpServletRequest());
            //if(supplychainShopProperties != null) {
                SupplychainShopProperties ssp = new SupplychainShopProperties();
                //插入SupplychainShopProperties的打印机编码
                ssp.setBindPrinterCode(bindPrinterCode);
                SupplychainShopPropertiesRemoteService.UpdateBindPrinterCodeByUserId(ssp.getBindPrinterCode(),sysUser.getId(), getHttpServletRequest());
            //}
        }
        return success();
    }


    @Override
    public JSONObject getOrderEntryByOrdenumber() {
        String orderNumber = getUTF("orderNumber");
        HashMap<String, Object> map = new HashMap<>();
        map.put("orderSequenceNumber", orderNumber);
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(map);
        return success(orderEntrys);
    }

    @Override
    public JSONObject getOrderEntryPosByOrdenumber() {
        String orderNumber = getUTF("orderNumber");
        HashMap<String, Object> map = new HashMap<>();
        map.put("orderSequenceNumber", orderNumber);
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrysPos(map);
        return success(orderEntrys);
    }

    @Override
    public JSONObject inOrderNumber() {
        String orderNumbersStr = getUTF("orderNumbersStr");
        List<String> list = JSONObject.parseArray(orderNumbersStr, String.class);
        HashMap<String, Object> map = new HashMap<>();
        map.put("inOrderNumber", list);
        List<OrderEntry> orderEntrysPos = dataAccessManager.getOrderEntrysPos(map);
        return success(orderEntrysPos);
    }

    /**
     * 单交易列表查询高级搜索接口
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/10
     */
    @Override
    public JSONObject getOrderEntrysPosMap() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Format f = new SimpleDateFormat("yyyy-MM-dd");
        SysUser user = getUser();
        String orderNumber = getUTF("orderNumber", null);
        String startTime = getUTF("startTime", sdf.format(new Date()));
        String endTime = getUTF("endTime", null);
        String status = getUTF("status", null);//订单状态
        String paymentType = getUTF("paymentType", null);//支付方式
        int pageIndex = getIntParameter("page", 1);
        int pageSize = getIntParameter("limit", 50);
        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageIndex - 1);
        int limitEnd = pageSize;
        String shopId = "";
        //远程调用用户商店绑定信息
        List<SupplychainShopBindRelationship> shopBindRelationships = SupplychainShopBindRelationshipRemoteService.getShopBindRelationships(5, user.getId(), getHttpServletRequest());
        if (shopBindRelationships != null && shopBindRelationships.size() > 0) {
            shopId = shopBindRelationships.get(0).getShopId();
        } else {
            SupplychainShopProperties supplychainShopPropertiesByUserId = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(user.getId(), getHttpServletRequest());
            if (supplychainShopPropertiesByUserId != null) {
                shopId = supplychainShopPropertiesByUserId.getId();
            }
        }

        if (shopId.equals("")) {
            return fail("没有找到用户商店信息");
        }
        try {
            if (endTime != null) {
                //结束时间加一天
                Date sDate = sdf.parse(endTime);
                Calendar c = Calendar.getInstance();
                c.setTime(sDate);
                c.add(Calendar.DAY_OF_MONTH, 1);
                sDate = c.getTime();
                endTime = f.format(sDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("likeOrderSequenceNumberLike", orderNumber);
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        map.put("status", status);
        map.put("paymentType", paymentType);
        map.put("userId", user.getId());
        map.put("shopIdPos", shopId);
        map.put("type", 16);
        map.put("pageIndex", limitStart);
        map.put("pageSize", limitEnd);
        map.put("isRefund", "1");//是否退款  1 是， 0 否
        List<OrderEntryPosFormatDate> orderEntryPosList = dataAccessManager.getOrderEntrysPosFormatDate(map);
        int count = dataAccessManager.getOrderEntrysPosount(map);
        //获取用户id集合
        if (orderEntryPosList != null && orderEntryPosList.size() > 0) {
            List<String> partnerId = orderEntryPosList.stream().map(OrderEntryPosFormatDate::getPartnerId).collect(Collectors.toList());
            if (partnerId != null && partnerId.size() > 0) {
                List<SysUser> sysUserInIds = SysUserRemoteService.getSysUserInIds(partnerId, getHttpServletRequest());
                if (sysUserInIds != null && sysUserInIds.size() > 0) {
                    orderEntryPosList.forEach(x -> {
                        String id = x.getPartnerId();
                        Optional<SysUser> first = sysUserInIds.stream().filter(item -> item.getId().equals(id)).findFirst();
                        if (first.isPresent()) {
                            SysUser sysUser = first.get();
                            x.setPartnerUserName(sysUser.getShowName());
                        }
                    });
                }

            }

            //查询订单商品详情

            List<String> orderId = orderEntryPosList.stream().map(OrderEntryPosFormatDate::getId).collect(Collectors.toList());
            if (orderId != null && !orderId.equals("")) {
                List<OrderGoodSnapshotFormatDate> orderGoodSnapshotByOrderInIds = dataAccessManager.getOrderGoodSnapshotFormatDateByOrderInIds(orderId);
                orderEntryPosList.forEach(x -> {
                    String id = x.getId();
                    List<OrderGoodSnapshotFormatDate> orderGoodSnapshot = orderGoodSnapshotByOrderInIds.stream().filter(item -> item.getOrderId().equals(id)).collect(Collectors.toList());
                    if (orderGoodSnapshot.size() > 0) {
                        x.setOrderGoodSnapshots(orderGoodSnapshot);
                    }
                });
            }

        }


        return success(orderEntryPosList, count);
    }


    /**
     * pos订单退款
     *
     * @param
     * @return
     * @author sqd
     * @date 2019/5/10
     */
    @Override
    public JSONObject orderRefund() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //String orderGoodSnaps = "[{\"createTime\":\"2019-05-25T02:50:39.000+0000\",\"goodBarcode\":\"6941704404487\",\"goodId\":\"a716c033e6a643c393637b9b4b0540f5GjckIb\",\"goodName\":\"新希望V美草莓味乳酸菌饮品300ml\",\"goodTemplateId\":\"b76ee4918ccb4aa1aed66a598692e3eaHJEEju\",\"goodTypeName\":\"300ml\",\"goodUnitId\":\"1e3bd1cc7b4c444f8e2e3101678e685dTIPati\",\"goodUnitName\":\"瓶\",\"id\":\"01efb7be600447148a64f63555a3218dywcZPt\",\"isThirdParty\":0,\"normalPrice\":650,\"normalQuantity\":1,\"orderId\":\"424a38f53e414836bae6224e8fff86ddUvjuOi\",\"refundQuantity\":2,\"totalPrice\":650},{\"createTime\":\"2019-05-25T02:50:39.000+0000\",\"goodBarcode\":\"6900082020139\",\"goodId\":\"7d7a7e9b408045e0803ea9a7d1960048ESFAcz\",\"goodName\":\"公仔特式香辣酱炒面王112g\",\"goodTemplateId\":\"350df0c786844672ae2b9fadc7578f23VuLCEO\",\"goodTypeName\":\"112g\",\"goodUnitId\":\"0f12b69a763348fe8fef551f595f9bd1HVgiJo\",\"goodUnitName\":\"碗\",\"id\":\"3e1c1185982a4697aa960aa17e3b302dUUxWCU\",\"isThirdParty\":0,\"normalPrice\":500,\"normalQuantity\":1,\"orderId\":\"424a38f53e414836bae6224e8fff86ddUvjuOi\",\"refundQuantity\":1,\"totalPrice\":500}]";
        //退款时 要把优惠的价格平均在每件商品上 discount_price
        String orderGoodSnaps = getUTF("orderGoodSnaps"); //退款商品信息
        String orderNumber = getUTF("orderNumber");//订单编号
        Integer isStorage = getIntParameter("isStorage");//是否重新入库 1、入库 ，0 不入库
        Integer refundType = getIntParameter("refundType");//退款类型 1/现金退款 ， 0原路返回

        HashMap<String, Object> map = new HashMap<>();
        map.put("orderNumber", orderNumber);
        OrderEntry orderNumer = dataAccessManager.getOrderEntryDaySortNumberDesc(map);
        if (orderNumer == null) {
            return fail("没有找到订单数据");
        }
        long refund = 0;//退款总金额
        if (orderNumer.getFindPrice() != null) {
            refund = orderNumer.getActualPrice() - orderNumer.getFindPrice(); //退款金额 = 实收 - 找零
        } else {
            refund = orderNumer.getActualPrice();
        }

        boolean refundStatus = false;
        String failReason = "";

        int paymentType = orderNumer.getPaymentType();
        if (refundType == 0) { //需要调用微信 或者支付宝
            String orderSequenceNumber = orderNumer.getOrderSequenceNumber();
            long actualPrice = orderNumer.getActualPrice();
            String tk = uniquePrimaryKey("TK");
            if (paymentType == PayTypeEnum.TRANS_TYPE_WEI.getKey()) {//微信支付
                if (orderNumer.getStatus() == OrderStatus.ORDER_TYPE_REFUND.getKey()) {
                    return fail("订单已退款,线上退款可能有延迟，请稍等一会儿！");
                }
                logger.debug("订单号：" + orderSequenceNumber);
                logger.debug("退款单号：" + tk);
                logger.debug("退款金额：" + actualPrice);
                Map<String, String> weixinResponse = WeiXinPayUtils.refund(orderSequenceNumber, tk, actualPrice + "", actualPrice + "", 4);
                logger.debug("微信返回实体：：：：" + weixinResponse);
                weixinResponse.forEach((x, y) -> {
                    logger.debug("微信返回消息：-------------------");
                    logger.debug("key:" + x + "   value:" + y);
                });
                String return_code = weixinResponse.get("return_code");
                String result_code = weixinResponse.get("result_code");
                failReason = weixinResponse.get("return_msg");
                logger.debug("-------------------------微信退款说明：" + failReason);
                if (return_code.equals("SUCCESS") && result_code.equals("SUCCESS")) {//成功
                    refundStatus = true;
                } else {
                    failReason = weixinResponse.get("err_code_des");
                }
            } else if (paymentType == PayTypeEnum.TRANS_TYPE_ZFB.getKey()) {//支付宝支付
                AlipayTradeQueryResponse alipayTradeQueryResponse = ZhiFuBaoPayUtils.queryTrade(orderSequenceNumber);
                if (alipayTradeQueryResponse.getTradeStatus().equals("TRADE_CLOSED")) {
                    return fail("订单已退款！不能重复提交");
                }

                double money = (double) actualPrice / 100;
                AlipayTradeRefundResponse alipayTradeRefundResponse = ZhiFuBaoPayUtils.returNoney(money + "", orderSequenceNumber, "线上退款");
                String code = alipayTradeRefundResponse.getCode();
                logger.debug("-------------------------支付宝退款说明：" + alipayTradeRefundResponse.getSubMsg());
                if (code.equals("10000")) {//成功
                    refundStatus = true;
                } else {
                    failReason = alipayTradeRefundResponse.getSubMsg();
                }
            } else {
                refundStatus = true;
            }
        } else {
            refundStatus = true;
        }


        String shopId = "";
        String shopName = "";
        String shopUserId = "";
        //远程调用用户商店绑定信息
        List<SupplychainShopBindRelationship> shopBindRelationships = SupplychainShopBindRelationshipRemoteService.getShopBindRelationships(5, user.getId(), getHttpServletRequest());
        if (shopBindRelationships != null && shopBindRelationships.size() > 0) {
            SupplychainShopProperties supplychainShopPropertiesById = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesById(shopBindRelationships.get(0).getShopId(), getHttpServletRequest());
            if (supplychainShopPropertiesById != null) {
                shopId = supplychainShopPropertiesById.getId();
                shopName = supplychainShopPropertiesById.getShopName();
                shopUserId = supplychainShopPropertiesById.getUserId();
            }
        } else {
            SupplychainShopProperties supplychainShopPropertiesByUserId = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(user.getId(), getHttpServletRequest());
            if (supplychainShopPropertiesByUserId != null) {
                shopId = supplychainShopPropertiesByUserId.getId();
                shopName = supplychainShopPropertiesByUserId.getShopName();
                shopUserId = supplychainShopPropertiesByUserId.getUserId();
            }
        }
        //添加退款记录
        PaymentTransactionLogger payLog = new PaymentTransactionLogger();
        payLog.setUserId(shopUserId);//店主id
        payLog.setTransType(TransTypeEnum.REFUND.getKey());//交易类型
        payLog.setChannelTradeNumber(orderNumber);
        payLog.setTransTotalAmount(refund); //退款金额 = 订单金额 考虑现金实收大于
        payLog.setTransSequenceNumber(uniquePrimaryKey());//流水号
        payLog.setRefundStatus(0);
        payLog.setTransTitle("退款");
        payLog.setPartnerUserId(user.getId());//操作人id
        payLog.setRefundTime(new Date());
        payLog.setTransCreateTime(orderNumer.getCreateTime());
        payLog.setPaymentTime(orderNumer.getPaymentTime());
        payLog.setPaymentType(orderNumer.getPaymentType() + "");

        if (refundStatus) {//退款成功
            try {
                List<GoodSnapshotPosVO> goodIdAndGoodNumberVO = JSONArray.parseArray(orderGoodSnaps, GoodSnapshotPosVO.class);
                //获取商品快照id
                List<String> partnerId = goodIdAndGoodNumberVO.stream().map(GoodSnapshotPosVO::getId).collect(Collectors.toList());
                map.put("snapshotIds", partnerId);
                List<OrderGoodSnapshot> orderGoodSnapshotPosByMap = dataAccessManager.getOrderGoodSnapshotPosByMap(map);

                if (orderGoodSnapshotPosByMap != null && orderGoodSnapshotPosByMap.size() > 0) {
                    for (OrderGoodSnapshot snapshotPos : orderGoodSnapshotPosByMap) {
                        GoodSnapshotPosVO goodSnapshotPosVO = new GoodSnapshotPosVO();
                        goodSnapshotPosVO.setId(snapshotPos.getId()); //快照id
                        goodSnapshotPosVO.setNormalPrice(snapshotPos.getNormalPrice()); //正常购买价格
                        goodSnapshotPosVO.setRefundQuantity(snapshotPos.getNormalQuantity());//退款数量 = 购买数量
                        goodSnapshotPosVO.setGoodId(snapshotPos.getGoodId());//退款数量 = 购买数量
                        long retunrnMoney = snapshotPos.getNormalPrice() * snapshotPos.getNormalQuantity(); //退款金额
                        goodSnapshotPosVO.setRetunrnMoney(retunrnMoney);//退款金额
                        // refund += retunrnMoney;//汇总退款总金额  分商品退款 退款金额需从快照计算
                        logger.info("isStorage:{}", isStorage);
                        if (isStorage == 1 && goodSnapshotPosVO.getGoodId() != null) {
                            //重新入库
                            goodSnapshotPosVO.setRetunrnMoney(retunrnMoney);
                            try {
                                SupplyChainGoodRemoteService.changSupplychainGoodByGoodId(goodSnapshotPosVO.getGoodId(), goodSnapshotPosVO.getRefundQuantity(), 1, getHttpServletRequest());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        try {
                            dataAccessManager.changeOrderGoodSnapshotPos(goodSnapshotPosVO);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                orderNumer.setRefundType(orderNumer.getPaymentType());//退款方式 = 支付方式 原路退回
                orderNumer.setRefundStatus(1);//订单退款状态 0表示未退货
                orderNumer.setReturnOperatorUserId(user.getId());//退款操作人
                orderNumer.setRefundTime(new Date());//退款时间
                orderNumer.setStatus(OrderStatus.ORDER_TYPE_REFUND.getKey());//状态改为已退款
                dataAccessManager.updateOrderEntrysPosRefundStatus(orderNumer);//修改退货的状态


                //添加账期记录
                PaymentEstimateRevenue paymentEstimateRevenue = new PaymentEstimateRevenue();
                paymentEstimateRevenue.setUserId(shopUserId);//店主id
                paymentEstimateRevenue.setShopsName(shopName);//商店名称
                paymentEstimateRevenue.setOrderId(orderNumer.getId());
                paymentEstimateRevenue.setOrderAmount(orderNumer.getActualPrice());
                paymentEstimateRevenue.setActualIncome(orderNumer.getActualPrice());
                paymentEstimateRevenue.setStatus(5);
                paymentEstimateRevenue.setRequestTime(new Date());
                paymentEstimateRevenue.setSettlementType((long) 1);
                paymentEstimateRevenue.setAccountingTime(new Date());
                PaymentEstimateRevenueService.addPaymentEstimateRevenue(paymentEstimateRevenue, getHttpServletRequest());

                //添加支记录
                payLog.setRefundStatus(1);//退款成功
                //添加订单退款记录
                PaymentTransactionLoggerService.addPaymentTransactionLogger(payLog, getHttpServletRequest());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (paymentType == PayTypeEnum.TRANS_TYPE_CASH.getKey()) {
                return success("操作成功！此单为现金支付，请收银员使用现金退款！");
            } else {
                return success("退款成功！线上退款可能有延迟，请稍等一会儿！");
            }
        } else {
            //添加支记录
            payLog.setRefundStatus(0);//退款失败
            //添加订单退款记录
            PaymentTransactionLoggerService.addPaymentTransactionLogger(payLog, getHttpServletRequest());
            return fail("退款失败！失败原因：" + failReason);
        }
    }

    /**
     * 门店业绩报表
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/20
     */
    @Override
    public JSONObject shopPerformanceStatistic() {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        HashMap<String, Object> map = new HashMap<>();
        String startTime = getUTF("startTime", sf.format(new Date()));
        String endTime = getUTF("endTime", null);

        if (endTime != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Format f = new SimpleDateFormat("yyyy-MM-dd");
            //结束时间加一天
            Date sDate = null;
            try {
                sDate = sdf.parse(endTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar c = Calendar.getInstance();
            c.setTime(sDate);
            c.add(Calendar.DAY_OF_MONTH, 1);
            sDate = c.getTime();
            endTime = f.format(sDate);
        }

        map.put("pageIndex", getPageStartIndex(getPageSize()));
        map.put("pageSize", getPageSize());
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        SysUser user = getUser();
        if (user == null) {
            fail("用户登录失效");
        }
        String shopId = "";
        //远程调用用户商店绑定信息
        List<SupplychainShopBindRelationship> shopBindRelationships = SupplychainShopBindRelationshipRemoteService.getShopBindRelationships(5, user.getId(), getHttpServletRequest());
        if (shopBindRelationships != null && shopBindRelationships.size() > 0) {
            SupplychainShopProperties supplychainShopPropertiesById = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesById(shopBindRelationships.get(0).getShopId(), getHttpServletRequest());
            if (supplychainShopPropertiesById != null) {
                shopId = supplychainShopPropertiesById.getId();
            }
        } else {
            SupplychainShopProperties supplychainShopPropertiesByUserId = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(user.getId(), getHttpServletRequest());
            if (supplychainShopPropertiesByUserId != null) {
                shopId = supplychainShopPropertiesByUserId.getId();
            }
        }
        if (shopId.equals("")) {
            return fail("没有用户商店信息");
        }
        map.put("shopId", shopId);
        // * 总计数据
        OrderEntrPosSum orderEntrPosSumPrice = dataAccessManager.getOrderEntrPosSumPrice(map);
        map.put("refundStatus", 0);

        //查询退款订单编号
        // map.put("shopId","");
        map.put("shopIdPosOne", shopId);
        // List<OrderEntry> orderEntrysPos = dataAccessManager.getOrderEntrysPos(map);
        // map.put("shopId",shopId);
        //查询退款商品快照
        /*List<OrderGoodSnapshot> orderGoodSnapshotByMap = null;
        if(orderEntrysPos != null && orderEntrysPos.size() > 0){
            List<String> orderSequenceNumbers = orderEntrysPos.stream().map(OrderEntry::getId).collect(Collectors.toList());
            map.put("orderSequenceNumbers",orderSequenceNumbers);
            orderGoodSnapshotByMap = dataAccessManager.getOrderGoodSnapshotPosByMap(map);
        }
        //long refundPriceSum = (long)orderGoodSnapshotByMap.stream().mapToDouble(OrderGoodSnapshot::getReturnPrice).sum();//和
        long refundPriceSum = 0;
        if(orderGoodSnapshotByMap != null && orderGoodSnapshotByMap.size() > 0){
            for (OrderGoodSnapshot goodSnapshot : orderGoodSnapshotByMap) {
                System.out.println(goodSnapshot.getReturnPrice() + " ====");
                if(goodSnapshot.getReturnPrice() != null){
                    refundPriceSum += (long)goodSnapshot.getReturnPrice();
                }
            }
        }*/


        //orderEntrPosSumPrice.setRefundPrice(refundPriceSum);
        //orderEntrPosSumPrice.setRefundOrderNumber(orderEntrysPos.size());

        // * 收入来源
        List<OrderPayTypeSumVo> orderEntryGruopByPayTtpe = dataAccessManager.getOrderEntryGruopByPayTtpe(null, null, startTime, endTime, shopId);

        // * 列表数据
        List<OrderEntryPriceVO> orderEntrPosPrice = dataAccessManager.getOrderEntrPosPrice(map);
       /* 全单退款不需要差快照， 直接取订单金额
       //查询快照数据
        HashMap<String, Object> mapObj = new HashMap<>();
        List<OrderGoodSnapshot> outOrderGoodSnapshot  = new  ArrayList<>();
        if(orderEntrPosPrice != null && orderEntrPosPrice.size() > 0){
            List<String> outOrderId = orderEntrPosPrice.stream().map(OrderEntryPriceVO::getOutOrderId).collect(Collectors.toList());
            outOrderGoodSnapshot = dataAccessManager.getOrderGoodSnapshotByOrderInIds(outOrderId);
            if(outOrderGoodSnapshot.size() > 0 ){
                for (OrderGoodSnapshot orderGoodSnapshot : outOrderGoodSnapshot) {
                    mapObj.put(orderGoodSnapshot.getOrderId(),orderGoodSnapshot.getReturnPrice());
                }
            }
        }

        //封装退款金额
        if(orderEntrPosPrice.size() > 0){
            for (OrderEntryPriceVO orderVO : orderEntrPosPrice) {
                String outOrderId = orderVO.getOutOrderId();
                if(outOrderId != null && outOrderId.equals("")){
                    String[] split = outOrderId.split(",");
                    long returnPrice = 0;
                    for (String s : split) {
                        returnPrice += (long)mapObj.get(s);
                    }
                    orderVO.setRefundPrice(returnPrice);
                }
            }
        }*/

        JSONObject jsonObject = new JSONObject();
        //总计数据
        jsonObject.put("orderEntrPrice", orderEntrPosSumPrice);
        //收入来源
        jsonObject.put("income", orderEntryGruopByPayTtpe);
        //列表数据
        jsonObject.put("orderEntrPosPrice", orderEntrPosPrice);

        return success(jsonObject);
    }

    @Override
    public JSONObject getShiftRecordsdetail() {
        String loginTime = getUTF("loginTime");
        String logoutTime = getUTF("logoutTime");
        HashMap<String, Object> map = new HashMap<>();

        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user == null){
            return  fail("用户登录失效");
        }
        String shopId = "";
        //远程调用用户商店绑定信息
        List<SupplychainShopBindRelationship> shopBindRelationships = SupplychainShopBindRelationshipRemoteService.getShopBindRelationships(5, user.getId(), getHttpServletRequest());
        if (shopBindRelationships != null && shopBindRelationships.size() > 0) {
            SupplychainShopProperties supplychainShopPropertiesById = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesById(shopBindRelationships.get(0).getShopId(), getHttpServletRequest());
            if (supplychainShopPropertiesById != null) {
                shopId = supplychainShopPropertiesById.getId();
            }
        } else {
            SupplychainShopProperties supplychainShopPropertiesByUserId = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(user.getId(), getHttpServletRequest());
            if (supplychainShopPropertiesByUserId != null) {
                shopId = supplychainShopPropertiesByUserId.getId();
            }
        }

        if (shopId.equals("")) {
            return fail("没有用户商店信息");
        }
        map.put("loginTime",loginTime);
        map.put("logoutTime",logoutTime);
        map.put("shopId",shopId);


        // * 支付方式统计
        List<OrderPayTypeSumAndRefundStatisticVo> orderEntryGruopByPayTtpeStatistic = dataAccessManager.getOrderEntryGruopByPayTtpeStatistic(map);
        // * 总计数据
        OrderEntrPosSum orderEntrPosSumPrice = dataAccessManager.getOrderEntrPosSumPrice(map);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("payTtpeStatistic",orderEntryGruopByPayTtpeStatistic);
        jsonObject.put("orderSumPrice",orderEntrPosSumPrice);
        return success(jsonObject);
    }

    @Override
    public JSONObject getRefundOrder() {
        String loginTime = getUTF("loginTime");
        String logoutTime = getUTF("logoutTime");
        String shopId = getUTF("shopId");

        HashMap<String, Object> map = new HashMap<>();
        map.put("loginTime",loginTime);
        map.put("logoutTime",logoutTime);
        map.put("shopId",shopId);

        List<OrderPayTypeSumAndRefundStatisticVo> orderEntryGruopByPayTtpeStatistic = dataAccessManager.getOrderEntryGruopByPayTtpeStatistic(map);
        return success(orderEntryGruopByPayTtpeStatistic);
    }

    @Override
    public JSONObject getOrderEntryByOrderNumberAll() {
        String orderNum = getUTF("orderNum");
        if(orderNum != null && !orderNum.equals("")){
            List<OrderEntry> list = dataAccessManager.getOrderEntryByOrderNumberAll(orderNum);
            return success(list);
        }
        return null;
    }


    /**
     * @author xh
     * 商家端小程序报表
     * @return
     */
    @Override
    public JSONObject getBusinessSideAppletReports() {
        SysUser sysUser = null;
        try {
            sysUser = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String startTime = getUTF("startTime", null);
        String endTime = "";    //前一天
        String sixDayTime = "";  //六天前
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(startTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject();
        if(startTime != null) {
            Calendar c1 = Calendar.getInstance();
            c1.setTime(date);
            int day1 = c1.get(Calendar.DATE);
            c1.set(Calendar.DATE,day1-1);
            endTime = DateUtils.format(c1.getTime(), DateUtils.FORMAT_SHORT); // 指定时间的1天前
            Calendar c2 = Calendar.getInstance();
            c2.setTime(date);
            int day2 = c2.get(Calendar.DATE);
            c2.set(Calendar.DATE,day2-6);
            sixDayTime = DateUtils.format(c2.getTime(), DateUtils.FORMAT_SHORT); // 指定时间的6天前
            System.out.println(sixDayTime);
        }
        //指定时间
        List<Long> turnoverArray = new ArrayList<>();       // 营业额
        List<Integer> orderNumArray = new ArrayList<>();    // 订单数
        List<Long> refundCostArray = new ArrayList<>();     //退款费用
        List<Integer> refundNumArray = new ArrayList<>();   //退款笔数
        //交易来源的数据
        List<Long> turnoverSpitArray = new ArrayList<>();       // 营业额(明细，分为支付宝，微信，现金)
        List<Integer> orderNumSpitArray = new ArrayList<>();    // 订单数(明细，分为支付宝，微信，现金)
        List<Long> refundCostSpitArray = new ArrayList<>();     //退款费用(明细，分为支付宝，微信，现金)
        List<Integer> refundNumSpitArray = new ArrayList<>();   //退款笔数(明细，分为支付宝，微信，现金)
        //图表
        List<String> dateArray = new ArrayList<>();                 //时间集合(7天内)
        List<Long> turnoverForeveryArray = new ArrayList<>();       // 营业额
        List<Integer> orderNumForeveryArray = new ArrayList<>();    // 订单数
        //指定时间的前一天
        List<Long> turnoverYestoDayArray = new ArrayList<>();       // 营业额
        List<Integer> orderNumYestoDayArray = new ArrayList<>();    // 订单数
        List<Long> refundCostYestoDayArray = new ArrayList<>();     //退款费用
        List<Integer> refundNumYestoDayArray = new ArrayList<>();   //退款笔数
        //查询用户店铺
        SupplychainShopProperties userShop = null;
        //远程调用用户商店绑定信息
        List<SupplychainShopBindRelationship> shopBindRelationships = SupplychainShopBindRelationshipRemoteService.getShopBindRelationships(5, sysUser.getId(), getHttpServletRequest());
        if (shopBindRelationships != null && shopBindRelationships.size() > 0) {
            userShop = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesById(shopBindRelationships.get(0).getShopId(), getHttpServletRequest());
        } else {
            userShop = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(sysUser.getId(), getHttpServletRequest());
        }
        OrderEntryPosVO orderEntryPosVO = new OrderEntryPosVO();
        String ownId = "";
        if(userShop != null ) {
            orderEntryPosVO.setShopName(userShop.getShopName());
            ownId = userShop.getId();              //绑定到商家门店的数据
        }
        //算出指定时间和指定时间前一天的数据
        List<BusinessSideAppletReport> listToday = dataAccessManager.getBusinessSideAppletReport(startTime,endTime,ownId);
        //算出指定时间和指定时间前6天每一天的数据
        List<BusinessSideAppletReportForEveryDay> listEveryday = dataAccessManager.getBusinessSideAppletReportForeveryDay(startTime,sixDayTime,ownId);
        if (!listToday.isEmpty()) {
            listToday.forEach(x -> {
                turnoverArray.add(x.getTurnover()-x.getRefundCost());                   //营业额需要减去退款金额
                orderNumArray.add(x.getOrderNum());
                refundCostArray.add(x.getRefundCost());
                refundNumArray.add(x.getRefundNum());
                turnoverArray.add(x.getTurnoverYestoday()-x.getRefundCostYestoday());   //营业额需要减去退款金额
                orderNumArray.add(x.getOrderNumYestoday());
                refundCostArray.add(x.getRefundCostYestoday());
                refundNumArray.add(x.getRefundNumYestoday());
                turnoverSpitArray.add(x.getWeixinPay()-x.getWeixinRefundCost());
                turnoverSpitArray.add(x.getZhifubaoPay()-x.getZhifubaoRefundCost());
                turnoverSpitArray.add(x.getXianjinPay()-x.getXianjinRefundCost());
                orderNumSpitArray.add(x.getWeixinOrderNum());
                orderNumSpitArray.add(x.getZhifubaoOrderNum());
                orderNumSpitArray.add(x.getXianjinOrderNum());
                refundCostSpitArray.add(x.getWeixinRefundCost());
                refundCostSpitArray.add(x.getZhifubaoRefundCost());
                refundCostSpitArray.add(x.getXianjinRefundCost());
                refundNumSpitArray.add(x.getWeixinRefundNum());
                refundNumSpitArray.add(x.getZhifubaoRefundNum());
                refundNumSpitArray.add(x.getXianjinRefundNum());
            });
        }
        if(!listEveryday.isEmpty()) {
            listEveryday.forEach(x -> {
                dateArray.add(x.getDate());
                turnoverForeveryArray.add(x.getTurnoverTotal()-x.getRefundCostTotal());
                orderNumForeveryArray.add(x.getOrderNumTotal());
            });
        }

        //店铺名称
        jsonObject.put("shopName", orderEntryPosVO);
        //指定时间
        jsonObject.put("turnoverArray", turnoverArray);
        jsonObject.put("orderNumArray", orderNumArray);
        jsonObject.put("refundCostArray", refundCostArray);
        jsonObject.put("refundNumArray", refundNumArray);
        //交易来源的数据
        jsonObject.put("turnoverSpitArray", turnoverSpitArray);
        jsonObject.put("orderNumSpitArray", orderNumSpitArray);
        jsonObject.put("refundCostSpitArray", refundCostSpitArray);
        jsonObject.put("refundNumSpitArray", refundNumSpitArray);
        //图标
        jsonObject.put("dateArray", dateArray);
        jsonObject.put("turnoverForeveryArray", turnoverForeveryArray);
        jsonObject.put("orderNumForeveryArray", orderNumForeveryArray);
        /*jsonObject.put("turnoverYestoDayArray", turnoverYestoDayArray);
        jsonObject.put("orderNumYestoDayArray", orderNumYestoDayArray);
        jsonObject.put("refundCostYestoDayArray", refundCostYestoDayArray);
        jsonObject.put("refundNumYestoDayArray", refundNumYestoDayArray);*/
        return  success(jsonObject);
    }


    /**
     * 获取昨日订单
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/21
     */
    @Override
    public JSONObject getOrderEntryForYesterDay() {
        String shopId = getUTF("shopId");
        System.out.println(shopId);
        JSONObject jsonObject = new JSONObject();
        if (shopId != null && !shopId.equals("")) {
            List<GoodSaleMsgVO> list = dataAccessManager.getOrderEntryForYesterDayAll(shopId);//查询某一天的记录
            list.forEach(x -> {
                System.out.println(x.getGoodId() + ": " + x.getGoodName());
            });
            jsonObject.put("list", list);
        }
        return jsonObject;
    }

    /**
     * pos端商品销售报表
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/22
     */
    @Override
    public JSONObject getGoodTypeStatistics() {
        String stratDateTime = getUTF("startTime", null);
        String endDateTime = getUTF("endTime", null);
        String goodType = getUTF("goodType", null);
        String hebdom = getUTF("hebdom", null);//查询一个星期的数据
        String oneMonth = getUTF("oneMonth", null);//查询一个月的数据
        String shopIdstr = getUTF("shopId", null);//查询一个月的数据

        HashMap<String, Object> map = new HashMap<>();
        //查询用户店铺
        SupplychainShopProperties userShop = null;
        //远程调用用户商店绑定信息
        SysUser sysUser = getUser();
        if (shopIdstr != null && !shopIdstr.equals("")) {
            map.put("shopIdPos", shopIdstr);
        } else {
            List<SupplychainShopBindRelationship> shopBindRelationships = SupplychainShopBindRelationshipRemoteService.getShopBindRelationships(5, sysUser.getId(), getHttpServletRequest());
            if (shopBindRelationships != null && shopBindRelationships.size() > 0) {
                userShop = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesById(shopBindRelationships.get(0).getShopId(), getHttpServletRequest());
                if (userShop != null)
                    map.put("userId", userShop.getId());
            } else {
                userShop = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(sysUser.getId(), getHttpServletRequest());
                if (userShop != null)
                    map.put("userId", userShop.getId());
            }
            map.put("shopIdPos", userShop.getId());
        }


//
//        List<GoodTypeOne> goodTypeOneTwoLeave = GoodTypeRemoteService.getGoodTypeOneTwoLeave(getHttpServletRequest());
//        if (userShop == null) {
//            fail("用户未绑定商店");
//        }


        //结束时间+1天
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Format f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (endDateTime != null) {
                //结束时间加一天
                Date sDate = sdf.parse(endDateTime);
                Calendar c = Calendar.getInstance();
                c.setTime(sDate);
                c.add(Calendar.DAY_OF_MONTH, 1);
                sDate = c.getTime();
                endDateTime = f.format(sDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        map.put("createStartTime", stratDateTime);
        map.put("createEndTime", endDateTime);

        //后台统计传参 shopIdstr

        List<OrderEntry> orderEntrysPos = dataAccessManager.getOrderEntrysPos(map);
        if (orderEntrysPos == null || orderEntrysPos.size() <= 0) {
            return success();
        }
        List<String> orderId = orderEntrysPos.stream().map(OrderEntry::getId).collect(Collectors.toList());
        map.put("goodTypeId", goodType);
        map.put("orderId", orderId);

        List<OrderEntryGoodTypeVO> goodTypeStatisticsList = dataAccessManager.getGoodTypeStatistics(map);
        if (goodTypeStatisticsList == null || goodTypeStatisticsList.size() <= 0) {
            return fail("没有数据");
        }
        //**查询所有商品分类，写远程接口，替换id
        List<GoodType> goodTypeAll = null;
        try {
            goodTypeAll = GoodTypeRemoteService.getGoodTypeAll(getHttpServletRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        HashMap<String, Object> mapObj = new HashMap<>();
        if (goodTypeAll != null && goodTypeAll.size() > 0) {
            for (GoodType type : goodTypeAll) {
                mapObj.put(type.getId(), type.getTitle());
            }
        }
        List<OrderEntryGoodTypeVO> goodTypeStatistics = new ArrayList<>();
        for (OrderEntryGoodTypeVO goodTypeStatistic : goodTypeStatisticsList) {
            String typeTitle = (String) mapObj.get(goodTypeStatistic.getGoodTypeId());
            if (typeTitle != null && !typeTitle.equals("")) {
                goodTypeStatistic.setGoodTypeTitle(typeTitle);
                goodTypeStatistics.add(goodTypeStatistic);
            }
        }

        return success(goodTypeStatistics);
    }

    @Override
    public JSONObject getretudGood() {
        return null;
    }


    @Override
    public JSONObject getOrderEntryGruopByPayTtpe() {
        String loginTime = getUTF("loginTime", null);
        String logoutTime = getUTF("logoutTime", null);
        String startTime = getUTF("startTime", null);
        String endTime = getUTF("endTime", null);
        String saleUserId = getUTF("saleUserId", null);
        List<OrderPayTypeSumVo> orderEntryGruopByPayTtpe = dataAccessManager.getOrderEntryGruopByPayTtpe(loginTime, logoutTime, startTime, endTime, saleUserId);
        return success(orderEntryGruopByPayTtpe);
    }

    @Override
    public JSONObject getOrderEntryReturn() {
        String loginTime = getUTF("loginTime", null);
        String logoutTime = getUTF("logoutTime", null);
        String saleUserId = getUTF("saleUserId", null);
        List<OrderEntry> orderEntryReturn = dataAccessManager.getOrderEntryReturn(loginTime, logoutTime, saleUserId);
//        List<OrderGoodSnapshot> orderGoodSnapshotByOrderInIds = new ArrayList<>();
//        if (orderEntryReturn != null && orderEntryReturn.size() > 0) {
//            List<String> outOrderId = orderEntryReturn.stream().map(OrderEntry::getId).collect(Collectors.toList());
//            if (outOrderId != null && outOrderId.size() > 0) {
//                orderGoodSnapshotByOrderInIds = dataAccessManager.getOrderGoodSnapshotPosByOrderInIds(outOrderId);
//            }
//        }
//        return success(orderGoodSnapshotByOrderInIds);
        return success(orderEntryReturn);
    }


    @Override
    public JSONObject getOrderEntryPosByMaplog() {
        SysUser user = null;
        try {
            user = getUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (user == null) {
            return fail("用户登录失效");
        }

        //查询用户店铺
        SupplychainShopProperties userShop = null;
        //远程调用用户商店绑定信息
        SysUser sysUser = getUser();
        List<SupplychainShopBindRelationship> shopBindRelationships = SupplychainShopBindRelationshipRemoteService.getShopBindRelationships(5, sysUser.getId(), getHttpServletRequest());
        if (shopBindRelationships != null && shopBindRelationships.size() > 0) {
            userShop = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesById(shopBindRelationships.get(0).getShopId(), getHttpServletRequest());
        } else {
            userShop = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUserId(sysUser.getId(), getHttpServletRequest());
        }
        List<GoodTypeOne> goodTypeOneTwoLeave = GoodTypeRemoteService.getGoodTypeOneTwoLeave(getHttpServletRequest());
        if (userShop == null) {
            fail("用户未绑定商店");
        }

        HashMap<String, Object> map = new HashMap<>();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String paymentTimeDay = getUTF("paymentTime", sf.format(new Date()));
        Integer orderSum = 0;
        long moneySum = 0;
        map.put("paymentTimeDay", paymentTimeDay);
        map.put("shopIdPos", userShop.getId());
        int pageIndex = getIntParameter("page", 1);
        int pageSize = getIntParameter("limit", 50);
        //计算获取的数据 开始数据 - 结束数据
        int limitStart = pageSize * (pageIndex - 1);
        int limitEnd = pageSize;
        map.put("pageIndex", limitStart);
        map.put("pageSize", limitEnd);

        //汇总数据
        List<PayLogNumbersVo> orderEntrysPosNumberDate = dataAccessManager.getOrderEntrysPosNumberDate(map);
        if (orderEntrysPosNumberDate != null && orderEntrysPosNumberDate.size() > 0) {
            for (PayLogNumbersVo payLogNumbersVo : orderEntrysPosNumberDate) {
                orderSum += Integer.parseInt(payLogNumbersVo.getNumber());
                moneySum += payLogNumbersVo.getMoneySum();
            }
        }

        //分页条数
        int count = dataAccessManager.getOrderEntrysPosount(map);
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrysPos(map);

        ArrayList<PayLogVo> payLogVoList = new ArrayList<>();

        //添加商品概况
        if (orderEntrys != null && orderEntrys.size() > 0) {
            for (OrderEntry payLogVo : orderEntrys) {
                PayLogVo payLogNumbersVo = new PayLogVo();
                String general = "";
                HashMap<String, Object> map1 = new HashMap<>();
                map1.put("orderId", payLogVo.getId());
                List<OrderGoodSnapshot> orderEntryByOrderNum = dataAccessManager.getOrderGoodSnapshotPosByMap(map1);
                if (orderEntryByOrderNum != null && orderEntryByOrderNum.size() > 0) {
                    for (OrderGoodSnapshot orderGoodSnapshot : orderEntryByOrderNum) {
                        general += orderGoodSnapshot.getGoodName() + "、";
                    }
                }
                if (general != null && general.length() > 2) {
                    general = general.substring(0, general.length() - 1);
                }
                payLogNumbersVo.setCreateTime(payLogVo.getCreateTime());
                payLogNumbersVo.setOrderNumer(payLogVo.getOrderSequenceNumber());
                if (payLogVo.getTotalPrice() != null) {
                    payLogNumbersVo.setOrderPrice(payLogVo.getTotalPrice());
                }
                if (payLogVo.getPaymentType() != null) {
                    payLogNumbersVo.setPayType(payLogVo.getPaymentType());
                }
                payLogNumbersVo.setPayTime(payLogVo.getPaymentTime());
                payLogNumbersVo.setOrderConditions(general);
                if (payLogVo.getActualPrice() != null) {
                    payLogNumbersVo.setActualPrice(payLogVo.getActualPrice() - (payLogVo.getFindPrice() != null ? payLogVo.getFindPrice() : 0));
                }
                payLogVoList.add(payLogNumbersVo);
            }
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("payLogNumber", orderEntrysPosNumberDate);
        jsonObject.put("payLog", payLogVoList);
        jsonObject.put("moneySum", moneySum);
        jsonObject.put("orderSum", orderSum);
        jsonObject.put("count", count);
        return success(jsonObject);
    }

    private SupplychainShopProperties getShopByCurrUser() {
        SupplychainShopProperties supplychainShopProperties =
                SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUid(getUser().getId(), getHttpServletRequest());
        return supplychainShopProperties;
    }

    @Override
    public JSONObject getOrderEntryListByPos() {
        String orderSequenceNumber = getUTF("orderSequenceNumber", null);
        int status = getIntParameter("status", 0);
        HashMap<String, Object> searchKeys = new HashMap<>();
        searchKeys.put("orderSequenceNumber", orderSequenceNumber);
        searchKeys.put("status", status);
        // 当前用户门店
        SupplychainShopProperties supplychainShopProperties = getShopByCurrUser();
        if (supplychainShopProperties == null) {
            return fail("当前用户无门店数据");
        }
        // 店长即为下单用户
        String partnerUserId = supplychainShopProperties.getUserId();
        List<OrderEntry> orderEntryList =
                dataAccessManager.getOrderEntryListByPartnerUserIdAndType(
                        partnerUserId,
                        // 1-8 互换 查找父单
                        OrderTypeEnum.SUPPLYCHAINPURCHASE.getKey(),
                        searchKeys,
                        getPageStartIndex(getPageSize()),
                        getPageSize()
                );
        int total = dataAccessManager.getOrderEntryListByPartnerUserIdAndTypeCount(partnerUserId, OrderTypeEnum.SUPPLYCHAINPURCHASE.getKey(),searchKeys);
        List<OrderEntryVO> orderEntryVOList = new ArrayList<>();
        orderEntryList.stream().forEach(orderEntry -> {
            OrderEntryVO orderEntryVO = new OrderEntryVO();
            BeanUtils.copyProperties(orderEntry, orderEntryVO);
            // 时间转换
            orderEntryVO.setCreateTimeStr(DateUtils.format(orderEntry.getCreateTime()));
            // 商品快照数量
            List<OrderGoodSnapshot> orderGoodSnapshotList = dataAccessManager.getOrderGoodSnapshotByOrderId(orderEntry.getId());
            if (!CollectionUtils.isEmpty(orderGoodSnapshotList)) {
                orderEntryVO.setOrderGoodSnapshotCount(orderGoodSnapshotList.size());
            }
            orderEntryVOList.add(orderEntryVO);
        });
        return success(orderEntryVOList, total);
    }

    @Override
    public JSONObject getOrderEntryByPos() {
        // 订单id
        String id = getUTF("id");
        OrderEntry orderEntry = dataAccessManager.getOrderEntry(id);
        OrderEntryVO orderEntryVO = new OrderEntryVO();
        BeanUtils.copyProperties(orderEntry, orderEntryVO);
        // 时间转换
        orderEntryVO.setCreateTimeStr(DateUtils.format(orderEntry.getCreateTime()));
        if (orderEntry == null) {
            return fail("订单数据不存在");
        }
        // 配送店铺名称
        String saleUserId = orderEntry.getSaleUserId();
        SupplychainShopProperties supplychainShopProperties = SupplychainShopPropertiesRemoteService.getSupplychainShopPropertiesByUid(saleUserId, getHttpServletRequest());
        String shopName = "";
        if (supplychainShopProperties != null) {
            shopName = supplychainShopProperties.getShopName();
        }
        // 商品快照
        List<OrderGoodSnapshot> orderGoodSnapshotList = dataAccessManager.getOrderGoodSnapshotByOrderId(id);
        List<OrderGoodSnapshotVo> orderGoodSnapshotVoList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(orderGoodSnapshotList)) {

            orderGoodSnapshotList.forEach(orderGoodSnapshot -> {
                OrderGoodSnapshotVo orderGoodSnapshotVo = new OrderGoodSnapshotVo();
                BeanUtils.copyProperties(orderGoodSnapshot, orderGoodSnapshotVo);
                // 已收货数量
                int receiptQuantity = orderGoodSnapshot.getReceiptQuantity()==null?0:orderGoodSnapshot.getReceiptQuantity();

                if (orderGoodSnapshotVo.getNormalQuantity() != null) {
                    int unReceiptQuantity = orderGoodSnapshotVo.getNormalQuantity() - receiptQuantity;
                    if (unReceiptQuantity >= 0) {
                        orderGoodSnapshotVo.setUnReceiptQuantity(unReceiptQuantity);
                    } else {
                        orderGoodSnapshotVo.setUnReceiptQuantity(0);
                    }
                } else {
                    logger.error("订单{}-商品{}:正常数量异常", orderEntry.getOrderSequenceNumber(), orderGoodSnapshot.getId());
                }
                orderGoodSnapshotVoList.add(orderGoodSnapshotVo);
            });
        }

        JSONObject res = new JSONObject();
        res.put("orderEntryVO", orderEntryVO);
        // 配送店铺名称
        res.put("shopName", shopName);
        res.put("orderGoodSnapshotVoList", orderGoodSnapshotVoList);
        return success(res);
    }

}
