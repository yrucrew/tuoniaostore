package com.tuoniaostore.order.controller;
        import com.tuoniaostore.order.service.OrderSequenceService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Controller
@RequestMapping("orderSequence")
public class OrderSequenceController {
    @Autowired
    private OrderSequenceService orderSequenceService;


    @RequestMapping("addOrderSequence")
    @ResponseBody
    public JSONObject addOrderSequence() {
        return orderSequenceService.addOrderSequence();
    }

    @RequestMapping("getOrderSequence")
    @ResponseBody
    public JSONObject getOrderSequence() {
        return orderSequenceService.getOrderSequence();
    }


    @RequestMapping("getOrderSequences")
    @ResponseBody
    public JSONObject getOrderSequences() {
        return orderSequenceService.getOrderSequences();
    }

    @RequestMapping("getOrderSequenceAll")
    @ResponseBody
    public JSONObject getOrderSequenceAll() {
        return orderSequenceService.getOrderSequenceAll();
    }

    @RequestMapping("getOrderSequenceCount")
    @ResponseBody
    public JSONObject getOrderSequenceCount() {
        return orderSequenceService.getOrderSequenceCount();
    }

    @RequestMapping("changeOrderSequence")
    @ResponseBody
    public JSONObject changeOrderSequence() {
        return orderSequenceService.changeOrderSequence();
    }

}
