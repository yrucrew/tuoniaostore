package com.tuoniaostore.order.controller;

import com.tuoniaostore.order.service.OrderEntryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSONObject;


/**
 * 订单实体表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Controller
@RequestMapping("orderEntry")
public class OrderEntryController {
    @Autowired
    private OrderEntryService orderEntryService;


    @RequestMapping("addOrderEntry")
    @ResponseBody
    public JSONObject addOrderEntry() {
        return orderEntryService.addOrderEntry();
    }

    @RequestMapping("getOrderEntry")
    @ResponseBody
    public JSONObject getOrderEntry() {
        return orderEntryService.getOrderEntry();
    }

    @ApiOperation("获取订单列表")
    @RequestMapping("getOrderEntrys")
    @ResponseBody
    public JSONObject getOrderEntrys() {
        return orderEntryService.getOrderEntrys();
    }

    @ApiOperation("获取采购订单列表")
    @RequestMapping("getOrderEntryListByPos")
    @ResponseBody
    public JSONObject getOrderEntryListByPos() {
        return orderEntryService.getOrderEntryListByPos();
    }

    @ApiOperation("获取采购订单详情")
    @RequestMapping("getOrderEntryByPos")
    @ResponseBody
    public JSONObject getOrderEntryByPos() {
        return orderEntryService.getOrderEntryByPos();
    }

    @RequestMapping("getOrderEntryCount")
    @ResponseBody
    public JSONObject getOrderEntryCount() {
        return orderEntryService.getOrderEntryCount();
    }

    @RequestMapping("changeOrderEntry")
    @ResponseBody
    public JSONObject changeOrderEntry() {
        return orderEntryService.changeOrderEntry();
    }

    @RequestMapping("getOrderEntrysByShippingUserIdAndType")
    @ResponseBody
    public JSONObject getOrderEntrysByShippingUserIdAndType() {
        return orderEntryService.getOrderEntrysByShippingUserIdAndType();
    }

    @RequestMapping("getOrderEntrysById")
    @ResponseBody
    public JSONObject getOrderEntrysById() {
        return orderEntryService.getOrderEntrysById();
    }

    @RequestMapping("changOrderEntrysShipment")
    @ResponseBody
    public JSONObject changOrderEntrysShipment() {
        return orderEntryService.changOrderEntrysShipment();
    }


    @RequestMapping("getBusinessOrderEntrys")
    @ResponseBody
    public JSONObject getBusinessOrderEntrys() {
        return orderEntryService.getBusinessOrderEntrys();
    }

    @RequestMapping("getOrderEntrysByShippingUserIdAndTypeBusiness")
    @ResponseBody
    public JSONObject getOrderEntrysByShippingUserIdAndTypeBusiness() {
        return orderEntryService.getOrderEntrysByShippingUserIdAndTypeBusiness();
    }

    @RequestMapping("orderEntryBuyAgain")
    @ResponseBody
    public JSONObject orderEntryBuyAgain() {
        return orderEntryService.orderEntryBuyAgain();
    }

    @RequestMapping("placeAnOrder")
    @ResponseBody
    public JSONObject placeAnOrder() {
        return orderEntryService.placeAnOrder();
    }

    @RequestMapping("getOrderEntrysPos")
    @ResponseBody
    public JSONObject getOrderEntrysPos() {
        return orderEntryService.getOrderEntrysPos();
    }

    @RequestMapping("isPay")
    @ResponseBody
    public JSONObject isPay() {
        return orderEntryService.isPay();
    }

    @RequestMapping("payCallback")
    @ResponseBody
    public JSONObject payCallback() {
        return orderEntryService.payCallback();
    }

    @RequestMapping("printOrder")
    @ResponseBody
    public JSONObject printOrder() {
        return orderEntryService.printOrder();
    }

    @RequestMapping("addPrinter")
    @ResponseBody
    public JSONObject addPrinter() {
        return orderEntryService.addPrinter();
    }

    @RequestMapping("getOrderEntryByOrdenumber")
    @ResponseBody
    public JSONObject getOrderEntryByOrdenumber() {
        return orderEntryService.getOrderEntryByOrdenumber();
    }

    @RequestMapping("placeAnOrderPos")
    @ResponseBody
    public JSONObject placeAnOrderPos() {
        return orderEntryService.placeAnOrderPos();
    }

    /*   @RequestMapping("payPos")
       @ResponseBody
       public JSONObject payPos() {
           return orderEntryService.payPos();
       }*/
    @RequestMapping("getOrderEntrysPosMap")
    @ResponseBody
    public JSONObject getOrderEntrysPosMap() {
        return orderEntryService.getOrderEntrysPosMap();
    }

    /**
     * pos订单详情
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/11
     */
    @RequestMapping("getOrderEntrysPosById")
    @ResponseBody
    public JSONObject getOrderEntrysPosById() {
        return orderEntryService.getOrderEntrysPosById();
    }

    /**
     * pos商品退款
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/11
     */
    @RequestMapping("orderRefund")
    @ResponseBody
    public JSONObject orderRefund() {
        return orderEntryService.orderRefund();
    }

    /**
     * 查询支付分类订单
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/13
     */
    @RequestMapping("getOrderEntryGruopByPayTtpe")
    @ResponseBody
    public JSONObject getOrderEntryGruopByPayTtpe() {
        return orderEntryService.getOrderEntryGruopByPayTtpe();
    }

    @RequestMapping("getOrderEntryPosByOrdenumber")
    @ResponseBody
    public JSONObject getOrderEntryPosByOrdenumber() {
        return orderEntryService.getOrderEntryPosByOrdenumber();
    }

    @RequestMapping("inOrderNumber")
    @ResponseBody
    public JSONObject inOrderNumber() {
        return orderEntryService.inOrderNumber();
    }

    @RequestMapping("refundDetails")
    @ResponseBody
    public JSONObject refundDetails() {
        return orderEntryService.refundDetails();
    }

    @RequestMapping("zfbPosPayCallback")
    @ResponseBody
    public JSONObject zfbPosPayCallback() {
        return orderEntryService.zfbPosPayCallback();
    }

    @RequestMapping("shopPerformanceStatistic")
    @ResponseBody
    public JSONObject shopPerformanceStatistic() {
        return orderEntryService.shopPerformanceStatistic();
    }

    /**
     * pos商品分类销售报表
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/25
     */
    @RequestMapping("getGoodTypeStatistics")
    @ResponseBody
    public JSONObject getGoodTypeStatistics() {
        return orderEntryService.getGoodTypeStatistics();
    }

    /**
     * 获取昨日订单
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author oy
     * @date 2019/5/21
     */
    @RequestMapping("getOrderEntryForYesterDay")
    @ResponseBody
    public JSONObject getOrderEntryForYesterDay() {
        return orderEntryService.getOrderEntryForYesterDay();
    }

    /**
     * 查询退款商品
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/24
     */
    @RequestMapping("getOrderEntryReturn")
    @ResponseBody
    public JSONObject getOrderEntryReturn() {
        return orderEntryService.getOrderEntryReturn();
    }


    /**
     * 订单；流水记录
     *
     * @param
     * @return com.alibaba.fastjson.JSONObject
     * @author sqd
     * @date 2019/5/27
     */
    @RequestMapping("getOrderEntryPosByMaplog")
    @ResponseBody
    public JSONObject getOrderEntryPosByMaplog() {
        return orderEntryService.getOrderEntryPosByMaplog();
    }

    @RequestMapping("getSmallProgramShopWxPay")
    @ResponseBody
    public JSONObject getSmallProgramShopWxPay() {
        return orderEntryService.getSmallProgramShopWxPay();
    }

    @RequestMapping("confirmTake")
    @ResponseBody
    public JSONObject confirmTake() {
        return orderEntryService.confirmTake();
    }

    @RequestMapping("nonPaymentOrder")
    @ResponseBody
    public JSONObject nonPaymentOrder() {
        return orderEntryService.nonPaymentOrder();
    }

    /***
     * 根据id 查找对应的记录
     * @author oy
     * @date 2019/6/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getOrderEntryAndPosById")
    @ResponseBody
    public JSONObject getOrderEntryAndPosById() {
        return orderEntryService.getOrderEntryAndPosById();
    }

    /**
     * 交班记录详情
     * @author sqd
     * @date 2019/6/13
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getShiftRecordsdetail")
    @ResponseBody
    public JSONObject getShiftRecordsdetail() {
        return orderEntryService.getShiftRecordsdetail();
    }

    @RequestMapping("getRefundOrder")
    @ResponseBody
    public JSONObject getRefundOrder() {
        return orderEntryService.getRefundOrder();
    }

    /**
     * 查询所有的订单
     * @author oy
     * @date 2019/6/15
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getOrderEntryByOrderNumberAll")
    @ResponseBody
    public JSONObject getOrderEntryByOrderNumberAll() {
        return orderEntryService.getOrderEntryByOrderNumberAll();
    }

    /**
     * 商家端小程序报表
     * @author oy
     * @date 2019/6/15
     * @param
     * @return
     */
    @RequestMapping("getBusinessSideAppletReports")
    @ResponseBody
    public JSONObject getBusinessSideAppletReports() {
        return orderEntryService.getBusinessSideAppletReports();
    }

}
