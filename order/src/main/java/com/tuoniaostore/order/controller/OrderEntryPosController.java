package com.tuoniaostore.order.controller;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.order.service.OrderEntryPosService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("orderEntryPos")
public class OrderEntryPosController {

    @Autowired
    OrderEntryPosService orderEntryPosService;

    @ApiOperation("获取销售订单列表")
    @RequestMapping("getOrderEntryPosList")
    public JSONObject getOrderEntryPosList() {
        return orderEntryPosService.getOrderEntryPosList();
    }


    /***
     * pos门店订单统计
     * @author oy
     * @date 2019/6/6
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getOrderEntryPosTotal")
    public JSONObject getOrderEntryPosTotal() {
        return orderEntryPosService.getOrderEntryPosTotal();
    }

}
