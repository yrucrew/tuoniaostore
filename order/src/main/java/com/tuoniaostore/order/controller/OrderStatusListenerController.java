package com.tuoniaostore.order.controller;
        import com.tuoniaostore.order.service.OrderStatusListenerService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Controller
@RequestMapping("orderStatusListener")
public class OrderStatusListenerController {
    @Autowired
    private OrderStatusListenerService orderStatusListenerService;


    @RequestMapping("addOrderStatusListener")
    @ResponseBody
    public JSONObject addOrderStatusListener() {
        return orderStatusListenerService.addOrderStatusListener();
    }

    @RequestMapping("getOrderStatusListener")
    @ResponseBody
    public JSONObject getOrderStatusListener() {
        return orderStatusListenerService.getOrderStatusListener();
    }


    @RequestMapping("getOrderStatusListeners")
    @ResponseBody
    public JSONObject getOrderStatusListeners() {
        return orderStatusListenerService.getOrderStatusListeners();
    }

    @RequestMapping("getOrderStatusListenerAll")
    @ResponseBody
    public JSONObject getOrderStatusListenerAll() {
        return orderStatusListenerService.getOrderStatusListenerAll();
    }

    @RequestMapping("getOrderStatusListenerCount")
    @ResponseBody
    public JSONObject getOrderStatusListenerCount() {
        return orderStatusListenerService.getOrderStatusListenerCount();
    }

    @RequestMapping("changeOrderStatusListener")
    @ResponseBody
    public JSONObject changeOrderStatusListener() {
        return orderStatusListenerService.changeOrderStatusListener();
    }

}
