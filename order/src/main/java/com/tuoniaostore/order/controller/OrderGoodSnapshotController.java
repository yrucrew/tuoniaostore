package com.tuoniaostore.order.controller;
        import com.tuoniaostore.order.service.OrderGoodSnapshotService;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 商品销售快照表
大部分字段为冗余字段
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Controller
@RequestMapping("orderGoodSnapshot")
public class OrderGoodSnapshotController {
    @Autowired
    private OrderGoodSnapshotService orderGoodSnapshotService;

    @RequestMapping("addOrderGoodSnapshot")
    @ResponseBody
    public JSONObject addOrderGoodSnapshot() {
        return orderGoodSnapshotService.addOrderGoodSnapshot();
    }

    @RequestMapping("getOrderGoodSnapshot")
    @ResponseBody
    public JSONObject getOrderGoodSnapshot() {
        return orderGoodSnapshotService.getOrderGoodSnapshot();
    }


    @RequestMapping("getOrderGoodSnapshots")
    @ResponseBody
    public JSONObject getOrderGoodSnapshots() {
        return orderGoodSnapshotService.getOrderGoodSnapshots();
    }

    @RequestMapping("getOrderGoodSnapshotAll")
    @ResponseBody
    public JSONObject getOrderGoodSnapshotAll() {
        return orderGoodSnapshotService.getOrderGoodSnapshotAll();
    }

    @RequestMapping("getOrderGoodSnapshotCount")
    @ResponseBody
    public JSONObject getOrderGoodSnapshotCount() {
        return orderGoodSnapshotService.getOrderGoodSnapshotCount();
    }

    @RequestMapping("changeOrderGoodSnapshot")
    @ResponseBody
    public JSONObject changeOrderGoodSnapshot() {
        return orderGoodSnapshotService.changeOrderGoodSnapshot();
    }

    @ApiOperation("根据订单id获取订单商品快照")
    @RequestMapping("getOrderGoodSnapshotByOrderId")
    @ResponseBody
    public JSONObject getOrderGoodSnapshotByOrderId() {
        return orderGoodSnapshotService.getOrderGoodSnapshotByOrderId();
    }


    /**
     * 根据订单号查找对应的快照记录
     * @author oy
     * @date 2019/4/24
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getOrderEntryByOrderNum")
    @ResponseBody
    public JSONObject getOrderEntryByOrderNum() {
        return orderGoodSnapshotService.getOrderEntryByOrderNum();
    }

    /**
     * 商品入库
     * @author oy
     * @date 2019/4/25
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("orderEntryWarehousing")
    @ResponseBody
    public JSONObject orderEntryWarehousing() {
        return orderGoodSnapshotService.orderEntryWarehousing();
    }

    /**
     * POS订单交易列表  查询订单详情
     * @author sqd
     * @date 2019/5/10
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getOrderGoodSnapshotPosByOrderId")
    @ResponseBody
    public JSONObject getOrderGoodSnapshotPosByOrderId() {
        return orderGoodSnapshotService.getOrderGoodSnapshotPosByOrderId();
    }

    /**
     * 根据订单查询快照
     * @author sqd
     * @date 2019/5/14
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    @RequestMapping("getOrderGoodSnapshotPosMapByorderId")
    @ResponseBody
    public JSONObject getOrderGoodSnapshotPosMapByorderId() {
        return orderGoodSnapshotService.getOrderGoodSnapshotPosMapByorderId();
    }

    @ApiOperation("采购入库单个商品确认收货")
    @RequestMapping("receiptOrderGoodSnapshotByPos")
    @ResponseBody
    public JSONObject receiptOrderGoodSnapshotByPos() {
        return orderGoodSnapshotService.receiptOrderGoodSnapshotByPos();
    }

    @ApiOperation("采购入库多个商品确认收货")
    @RequestMapping("receiptOrderGoodSnapshots")
    @ResponseBody
    public JSONObject receiptOrderGoodSnapshots() {
        return orderGoodSnapshotService.receiptOrderGoodSnapshots();
    }

}
