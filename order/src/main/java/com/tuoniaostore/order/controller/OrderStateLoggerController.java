package com.tuoniaostore.order.controller;
        import com.tuoniaostore.order.service.OrderStateLoggerService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Controller
@RequestMapping("orderStateLogger")
public class OrderStateLoggerController {
    @Autowired
    private OrderStateLoggerService orderStateLoggerService;


    @RequestMapping("addOrderStateLogger")
    @ResponseBody
    public JSONObject addOrderStateLogger() {
        return orderStateLoggerService.addOrderStateLogger();
    }

    @RequestMapping("getOrderStateLogger")
    @ResponseBody
    public JSONObject getOrderStateLogger() {
        return orderStateLoggerService.getOrderStateLogger();
    }


    @RequestMapping("getOrderStateLoggers")
    @ResponseBody
    public JSONObject getOrderStateLoggers() {
        return orderStateLoggerService.getOrderStateLoggers();
    }

    @RequestMapping("getOrderStateLoggerAll")
    @ResponseBody
    public JSONObject getOrderStateLoggerAll() {
        return orderStateLoggerService.getOrderStateLoggerAll();
    }

    @RequestMapping("getOrderStateLoggerCount")
    @ResponseBody
    public JSONObject getOrderStateLoggerCount() {
        return orderStateLoggerService.getOrderStateLoggerCount();
    }

    @RequestMapping("changeOrderStateLogger")
    @ResponseBody
    public JSONObject changeOrderStateLogger() {
        return orderStateLoggerService.changeOrderStateLogger();
    }

}
