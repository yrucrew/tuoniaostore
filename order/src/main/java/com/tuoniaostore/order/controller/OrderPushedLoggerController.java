package com.tuoniaostore.order.controller;
        import com.tuoniaostore.order.service.OrderPushedLoggerService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.stereotype.Controller;
        import com.alibaba.fastjson.JSONObject;


/**
 * 订单的推送日志
与未接订单日志的区别为该表不物理删除数据 可以查询订单是否曾经已执行推送 且明白推送的目标
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Controller
@RequestMapping("orderPushedLogger")
public class OrderPushedLoggerController {
    @Autowired
    private OrderPushedLoggerService orderPushedLoggerService;


    @RequestMapping("addOrderPushedLogger")
    @ResponseBody
    public JSONObject addOrderPushedLogger() {
        return orderPushedLoggerService.addOrderPushedLogger();
    }

    @RequestMapping("getOrderPushedLogger")
    @ResponseBody
    public JSONObject getOrderPushedLogger() {
        return orderPushedLoggerService.getOrderPushedLogger();
    }


    @RequestMapping("getOrderPushedLoggers")
    @ResponseBody
    public JSONObject getOrderPushedLoggers() {
        return orderPushedLoggerService.getOrderPushedLoggers();
    }

    @RequestMapping("getOrderPushedLoggerAll")
    @ResponseBody
    public JSONObject getOrderPushedLoggerAll() {
        return orderPushedLoggerService.getOrderPushedLoggerAll();
    }

    @RequestMapping("getOrderPushedLoggerCount")
    @ResponseBody
    public JSONObject getOrderPushedLoggerCount() {
        return orderPushedLoggerService.getOrderPushedLoggerCount();
    }

    @RequestMapping("changeOrderPushedLogger")
    @ResponseBody
    public JSONObject changeOrderPushedLogger() {
        return orderPushedLoggerService.changeOrderPushedLogger();
    }

}
