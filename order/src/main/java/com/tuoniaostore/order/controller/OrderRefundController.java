package com.tuoniaostore.order.controller;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.order.service.OrderRefundService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("orderRefund")
public class OrderRefundController {

    @Autowired
    private OrderRefundService orderRefundService;

    @ApiOperation("申请退款")
    @PostMapping("orderRefundApply")
    public JSONObject orderRefundApply() {
        return orderRefundService.orderRefundApply();
    }

    @ApiOperation("退款列表")
    @RequestMapping("orderRefundList")
    public JSONObject orderRefundList() {
        return orderRefundService.orderRefundList();
    }

    @ApiOperation("同意退款申请")
    @RequestMapping("orderRefundAgree")
    public JSONObject orderRefundAgree() {
        return orderRefundService.orderRefundAgree();
    }

    @ApiOperation("拒绝退款申请")
    @RequestMapping("orderRefundReject")
    public JSONObject orderRefundReject() {
        return orderRefundService.orderRefundReject();
    }

    @ApiOperation("退款申请详情")
    @RequestMapping("orderRefundEntryDetail")
    public JSONObject orderRefundEntryDetail() {
        return orderRefundService.orderRefundEntryDetail();
    }

    @ApiOperation("退款商品列表[分页]")
    @RequestMapping("orderRefundGoodSnapshotList")
    public JSONObject orderRefundGoodSnapshotList() {
        return orderRefundService.orderRefundGoodSnapshotList();
    }
}
