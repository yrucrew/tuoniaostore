package com.tuoniaostore.order.cache.impl;
        import com.tuoniaostore.datamodel.order.OrderSequence;
        import com.tuoniaostore.order.cache.OrderSequenceCache;
        import com.tuoniaostore.order.data.OrderSequenceMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
@CacheConfig(cacheNames = "orderSequence")
public class OrderSequenceCacheImpl implements OrderSequenceCache {

    @Autowired
    OrderSequenceMapper mapper;

    @Override
    @CacheEvict(value = "orderSequence", allEntries = true)
    public void addOrderSequence(OrderSequence orderSequence) {
        mapper.addOrderSequence(orderSequence);
    }

    @Override
    @Cacheable(value = "orderSequence", key = "'getOrderSequence_'+#id")
    public   OrderSequence getOrderSequence(String id) {
        return mapper.getOrderSequence(id);
    }

    @Override
    @Cacheable(value = "orderSequence", key = "'getOrderSequences_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<OrderSequence> getOrderSequences(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getOrderSequences(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "orderSequence", key = "'getOrderSequenceAll'")
    public List<OrderSequence> getOrderSequenceAll() {
        return mapper.getOrderSequenceAll();
    }

    @Override
    @Cacheable(value = "orderSequence", key = "'getOrderSequenceCount_'+#status+'_'+#name")
    public int getOrderSequenceCount(int status, String name) {
        return mapper.getOrderSequenceCount(status, name);
    }

    @Override
    @CacheEvict(value = "orderSequence", allEntries = true)
    public void changeOrderSequence(OrderSequence orderSequence) {
        mapper.changeOrderSequence(orderSequence);
    }

}
