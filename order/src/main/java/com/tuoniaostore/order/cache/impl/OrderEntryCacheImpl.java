package com.tuoniaostore.order.cache.impl;

import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.vo.order.*;
import com.tuoniaostore.order.VO.*;
import com.tuoniaostore.order.cache.OrderEntryCache;
import com.tuoniaostore.order.data.OrderEntryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单实体表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
@CacheConfig(cacheNames = "orderEntry")
public class OrderEntryCacheImpl implements OrderEntryCache {

    @Autowired
    OrderEntryMapper mapper;

    @Override
    @CacheEvict(value = "orderEntry", allEntries = true)
    public void addOrderEntry(OrderEntry orderEntry) {
        mapper.addOrderEntry(orderEntry);
    }

    @Override
    @CacheEvict(value = "orderEntry", allEntries = true)
    public void addOrderEntryPos(OrderEntry orderEntry) {
        mapper.addOrderEntryPos(orderEntry);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntry_'+#id")
    public   OrderEntry getOrderEntry(String id) {
        return mapper.getOrderEntry(id);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntrys_'+#map")
    public List<OrderEntry> getOrderEntrys(Map<String ,Object> map) {
        return mapper.getOrderEntrys(map);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntryAll'")
    public List<OrderEntry> getOrderEntryAll() {
        return mapper.getOrderEntryAll();
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntryCount_'+#map")
    public int getOrderEntryCount(Map<String,Object> map) {
        return mapper.getOrderEntryCount(map);
    }

    @Override
    @CacheEvict(value = "orderEntry", allEntries = true)
    public void changeOrderEntry(OrderEntry orderEntry) {
        mapper.changeOrderEntry(orderEntry);
    }
    @Override
    @CacheEvict(value = "orderEntry", allEntries = true)
    public void changeOrderEntryPos(OrderEntry orderEntry) {
        mapper.changeOrderEntryPos(orderEntry);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntrysByShippingUserIdAndType_'+#userId+'_'+#type+'_'+#strings+'_'+#pageIndex+'_'+#pageSize")
    public List<OrderEntry> getOrderEntrysByShippingUserIdAndType(String userId ,int type,List<String> strings,int pageIndex, int pageSize) {
        return mapper.getOrderEntrysByShippingUserIdAndType(userId, type, strings, pageIndex, pageSize);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntrysByShippingUserIdAndTypeBusiness_'+#userId+'_'+#type+'_'+#status+'_'+#pageIndex+'_'+#pageSize")
    public List<OrderEntry> getOrderEntrysByShippingUserIdAndTypeBusiness(String userId ,int type,int status,int pageIndex, int pageSize) {
        return mapper.getOrderEntrysByShippingUserIdAndTypeBusiness(userId,type,status, pageIndex, pageSize);
    }
    @Override
    @CacheEvict(value = "orderEntry", allEntries = true)
    public void changeOrderEntryType(OrderEntry orderEntry) {
        mapper.changeOrderEntryType(orderEntry);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntryPosCount_'+#map")
    public int getOrderEntryPosCount(Map<String, Object> map) {
        return mapper.getOrderEntryPosCount(map);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntrysPos_'+#map")
    public List<OrderEntry> getOrderEntrysPos(Map<String, Object> map) {
        return mapper.getOrderEntrysPos(map);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntrysPosFormatDate_'+#map")
    public List<OrderEntryPosFormatDate> getOrderEntrysPosFormatDate(Map<String, Object> map) {
        return mapper.getOrderEntrysPosFormatDate(map);
    }
    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntryDaySortNumberDesc_'+#map")
    public OrderEntry getOrderEntryDaySortNumberDesc(Map<String,Object> map) {
        return mapper.getOrderEntryDaySortNumberDesc(map);
    }

    @Override
//    @Cacheable(value = "orderEntry", key = "'getOrderEntryGruopByPayTtpe_'+#loginTime+'_'+#logoutTime+'_'+#startTime+'_'+#endTime+'_'+#saleUserId")
    public List<OrderPayTypeSumVo> getOrderEntryGruopByPayTtpe(String loginTime, String logoutTime, String startTime, String endTime, String saleUserId) {
        return mapper.getOrderEntryGruopByPayTtpe(loginTime, logoutTime,startTime,endTime,saleUserId);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntrysPosount_'+#map")
    public int getOrderEntrysPosount(Map<String, Object> map) {
        return  mapper.getOrderEntrysPosount(map);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntrPosPrice_'+#map")
    public List<OrderEntryPriceVO> getOrderEntrPosPrice(Map map) {
        return mapper.getOrderEntrPosPrice(map);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntrPosSumPrice_'+#map")
    public OrderEntrPosSum getOrderEntrPosSumPrice(Map map) {
        return mapper.getOrderEntrPosSumPrice(map);
    }

    /**
     * 用于定时器统计昨天的记录 不需要做缓存
     * @author oy
     * @date 2019/5/21
     * @param
     * @return java.util.List<com.tuoniaostore.datamodel.vo.order.GoodSaleMsgVO>
     */
    @Override
    public List<GoodSaleMsgVO> getOrderEntryForYesterDayAll(String shopId) {
        return mapper.getOrderEntryForYesterDayAll(shopId);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getGoodTypeStatistics_'+#map")
    public List<OrderEntryGoodTypeVO> getGoodTypeStatistics(Map map) {
        return  mapper.getGoodTypeStatistics(map);
    }


    @Override
//    @Cacheable(value = "orderEntry", key = "'getOrderEntryReturn_'+#loginTime+'_'+#logoutTime+'_'+#saleUserId")
    public List<OrderEntry> getOrderEntryReturn(String loginTime, String logoutTime, String saleUserId) {
        return mapper.getOrderEntryReturn(loginTime,logoutTime,saleUserId);
    }

    @Override
    @CacheEvict(value = "orderEntry", allEntries = true)
    public void updateOrderEntrysPosRefundStatus(OrderEntry orderNumer) {
        mapper.updateOrderEntrysPosRefundStatus(orderNumer);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntrysPosNumberDate_'+#map")
    public List<PayLogNumbersVo> getOrderEntrysPosNumberDate(Map<String, Object> map) {
        return mapper.getOrderEntrysPosNumberDate(map);
    }

    //查询排号不用缓存
    @Override
    public OrderEntry getOrderEntryDaySortDesc(Map<String, Object> map) {
        return mapper.getOrderEntryDaySortDesc(map);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntryListByPartnerUserIdAndType_'+#partnerUserId+#type+#searchKeys+#pageStartIndex+#pageSize")
    public List<OrderEntry> getOrderEntryListByPartnerUserIdAndType(String partnerUserId, int type, HashMap<String, Object> searchKeys, int pageStartIndex, int pageSize) {
        return mapper.getOrderEntryListByPartnerUserIdAndType(partnerUserId, type, searchKeys, pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntryListByPartnerUserIdAndTypeCount_'+#partnerUserId+#type+#searchKeys+#pageStartIndex+#pageSize")
    public Integer getOrderEntryListByPartnerUserIdAndTypeCount(String partnerUserId, int type, HashMap<String, Object> searchKeys) {
        return mapper.getOrderEntryListByPartnerUserIdAndTypeCount(partnerUserId, type, searchKeys);
    }

    @Override
    @CacheEvict(value = "orderEntry", allEntries = true)
    public void changeOrderEntryStatusAndDeliverStatus(OrderEntry orderEntry) {
        mapper.changeOrderEntryStatusAndDeliverStatus(orderEntry);
    }

    @Override
    public Map<String,Object> getOrderEntrysPostForTotal(int orderType, String todayTime) {
        return mapper.getOrderEntrysPostForTotal(orderType,todayTime);
    }

    @Override
    public List<OrderEntrysTotalVO> getOrderEntrysByTypeCountForEveryDay(int orderType, String startTime, String endTime) {
        return mapper.getOrderEntrysByTypeCountForEveryDay(orderType,startTime,endTime);
    }

    @Override
    public List<OrderEntrysTotalVO> getOrderEntrysByTypeCountForEveryWeek(int orderType, String startTime, String endTime) {
        return mapper.getOrderEntrysByTypeCountForEveryWeek(orderType,startTime,endTime);
    }

    @Override
    public List<OrderEntrysTotalVO> getOrderEntrysByTypeCountForEveryMonth(int orderType, String startTime, String endTime) {
        return mapper.getOrderEntrysByTypeCountForEveryMonth(orderType,startTime,endTime);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntryAndPosById_'+#orderId")
    public OrderEntry getOrderEntryAndPosById(String orderId) {
        return mapper.getOrderEntryAndPosById(orderId);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntryGruopByPayTtpeStatistic'+#map")
    public List<OrderPayTypeSumAndRefundStatisticVo> getOrderEntryGruopByPayTtpeStatistic(Map map) {
        return mapper.getOrderEntryGruopByPayTtpeStatistic(map);
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntryByOrderNumberAll_'+#orderNum")
    public List<OrderEntry> getOrderEntryByOrderNumberAll(String orderNum) {
        return mapper.getOrderEntryByOrderNumberAll(orderNum);
    }

    @Override
    public List<BusinessSideAppletReport> getBusinessSideAppletReport(String startTime, String endTime, String ownId) {
        return mapper.getBusinessSideAppletReport(startTime, endTime, ownId);
    }

    @Override
    public List<BusinessSideAppletReportForEveryDay> getBusinessSideAppletReportForeveryDay(String startTime, String sixDayTime, String ownId) {
        return mapper.getBusinessSideAppletReportForeveryDay(startTime, sixDayTime, ownId);
    }

    /*
     * 定时器查询不加缓存
     * @author sqd
     * @date 2019/6/22
     */
    @Override
    public List<OrderEntry> getLazyReceivingGoods() {
        return mapper.getLazyReceivingGoods();
    }

    @Override
    @Cacheable(value = "orderEntry", key = "'getOrderEntryListBySequenceNumber'+#sequenceNumber")
    public List<OrderEntry> getOrderEntryListBySequenceNumber(String sequenceNumber) {
        return mapper.getOrderEntryListBySequenceNumber(sequenceNumber);
    }

}
