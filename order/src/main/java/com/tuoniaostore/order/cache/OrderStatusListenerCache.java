package com.tuoniaostore.order.cache;
import com.tuoniaostore.datamodel.order.OrderStatusListener;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public interface OrderStatusListenerCache {

    void addOrderStatusListener(OrderStatusListener orderStatusListener);

    OrderStatusListener getOrderStatusListener(String id);

    List<OrderStatusListener> getOrderStatusListeners(int pageIndex, int pageSize, int status, String name);
    List<OrderStatusListener> getOrderStatusListenerAll();
    int getOrderStatusListenerCount(int status, String name);
    void changeOrderStatusListener(OrderStatusListener orderStatusListener);

}
