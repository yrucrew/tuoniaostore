package com.tuoniaostore.order.cache.impl;

import com.tuoniaostore.datamodel.order.OrderRefundGoodSnapshot;
import com.tuoniaostore.order.cache.OrderRefundGoodSnapshotCache;
import com.tuoniaostore.order.data.OrderRefundGoodSnapshotMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@CacheConfig(cacheNames = "OrderRefundGoodSnapshot")
public class OrderRefundGoodSnapshotCacheImpl implements OrderRefundGoodSnapshotCache {

    @Autowired
    OrderRefundGoodSnapshotMapper mapper;

    @Override
    public void addOrderRefundGoodSnapshot(OrderRefundGoodSnapshot orderRefundGoodSnapshot) {
        mapper.addOrderRefundGoodSnapshot(orderRefundGoodSnapshot);
    }

    @Override
    public List<OrderRefundGoodSnapshot> getOrderRefundGoodSnapshotByOrderId(String orderId, int pageStartIndex, int pageSize) {
        return mapper.getOrderRefundGoodSnapshotByOrderId(orderId, pageStartIndex, pageSize);
    }

    @Override
    public void batchAddOrderRefundGoodSnapshot(List<OrderRefundGoodSnapshot> orderRefundGoodSnapshotAddList) {
        mapper.batchAddOrderRefundGoodSnapshot(orderRefundGoodSnapshotAddList);
    }
}
