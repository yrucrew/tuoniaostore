package com.tuoniaostore.order.cache;

import com.tuoniaostore.datamodel.order.OrderRefundEntry;

import java.util.HashMap;
import java.util.List;

public interface OrderRefundEntryCache {

    void addOrderRefundEntry(OrderRefundEntry orderRefundEntry);

    List<OrderRefundEntry> getOrderRefundEntryList(HashMap<String, Object> parameters, int pageStartIndex, int pageSize);

    int getOrderRefundEntryListCount(HashMap<String, Object> parameters);

    OrderRefundEntry getOrderRefundEntry(String id);

    List<OrderRefundEntry> getOrderRefundEntryListBySequenceNumber(String sequenceNumber);

    void changeOrderRefundEntryStatus(OrderRefundEntry orderRefundEntry);

}
