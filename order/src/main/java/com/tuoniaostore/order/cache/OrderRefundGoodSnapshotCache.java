package com.tuoniaostore.order.cache;

import com.tuoniaostore.datamodel.order.OrderRefundGoodSnapshot;

import java.util.List;

public interface OrderRefundGoodSnapshotCache {

    void addOrderRefundGoodSnapshot(OrderRefundGoodSnapshot orderRefundGoodSnapshot);

    List<OrderRefundGoodSnapshot> getOrderRefundGoodSnapshotByOrderId(String orderId, int pageStartIndex, int pageSize);

    void batchAddOrderRefundGoodSnapshot(List<OrderRefundGoodSnapshot> orderRefundGoodSnapshotAddList);
}
