package com.tuoniaostore.order.cache.impl;
        import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
        import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
        import com.tuoniaostore.datamodel.vo.order.OrderGoodSnapshotFormatDate;
        import com.tuoniaostore.order.VO.GoodSnapshotPosVO;
        import com.tuoniaostore.order.VO.GoodSnapshotVO;
        import com.tuoniaostore.order.cache.OrderGoodSnapshotCache;
        import com.tuoniaostore.order.data.OrderGoodSnapshotMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;
        import java.util.Map;

/**
 * 商品销售快照表
大部分字段为冗余字段
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
@CacheConfig(cacheNames = "orderGoodSnapshot")
public class OrderGoodSnapshotCacheImpl implements OrderGoodSnapshotCache {

    @Autowired
    OrderGoodSnapshotMapper mapper;

    @Override
    @CacheEvict(value = "orderGoodSnapshot", allEntries = true)
    public void addOrderGoodSnapshot(OrderGoodSnapshot orderGoodSnapshot) {
        mapper.addOrderGoodSnapshot(orderGoodSnapshot);
    }
    @Override
    @CacheEvict(value = "orderGoodSnapshot", allEntries = true)
    public void addOrderGoodSnapshotPos(OrderGoodSnapshot orderGoodSnapshot) {
        mapper.addOrderGoodSnapshotPos(orderGoodSnapshot);
    }

    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshot_'+#id")
    public   OrderGoodSnapshot getOrderGoodSnapshot(String id) {
        return mapper.getOrderGoodSnapshot(id);
    }

    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshots_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<OrderGoodSnapshot> getOrderGoodSnapshots(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getOrderGoodSnapshots(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshotAll'")
    public List<OrderGoodSnapshot> getOrderGoodSnapshotAll() {
        return mapper.getOrderGoodSnapshotAll();
    }

    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshotCount_'+#status+'_'+#name")
    public int getOrderGoodSnapshotCount(int status, String name) {
        return mapper.getOrderGoodSnapshotCount(status, name);
    }

    @Override
    @CacheEvict(value = "orderGoodSnapshot", allEntries = true)
    public void changeOrderGoodSnapshot(OrderGoodSnapshot orderGoodSnapshot) {
        mapper.changeOrderGoodSnapshot(orderGoodSnapshot);
    }

    @Override
    @CacheEvict(value = "orderGoodSnapshot", allEntries = true)
    public void batchChangeOrderGoodSnapshot(List<OrderGoodSnapshot> orderGoodSnapshotChangeList) {
        mapper.batchChangeOrderGoodSnapshot(orderGoodSnapshotChangeList);
    }

    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshotByOrderId_'+#orderId")
    public List<OrderGoodSnapshot> getOrderGoodSnapshotByOrderId(String orderId) {
        return mapper.getOrderGoodSnapshotByOrderId(orderId);
    }

    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshotByOrderInIds_'+#orderIds")
    public List<OrderGoodSnapshot> getOrderGoodSnapshotByOrderInIds(List<String> orderIds) {
        return mapper.getOrderGoodSnapshotByOrderInIds(orderIds);
    }

    @Override
    @CacheEvict(value = "orderGoodSnapshot", allEntries = true)
    public void changeOrderGoodSnapshotNormalQuantity(OrderGoodSnapshot orderGoodSnapshot) {
        mapper.changeOrderGoodSnapshotNormalQuantity(orderGoodSnapshot);
    }
/*
    @Override
    @CacheEvict(value = "orderGoodSnapshot", allEntries = true)
    public void changeOrderGoodSnapshotNormalQuantity(OrderGoodSnapshot orderGoodSnapshot) {
        mapper.changeOrderGoodSnapshotNormalQuantity(orderGoodSnapshot);
    }
*/

    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshotByMap_'+#map")
    public List<OrderGoodSnapshot> getOrderGoodSnapshotByMap(Map<String, Object> map) {
        return mapper.getOrderGoodSnapshotByMap(map);
    }


    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshotByOrderNum_'+#orderNum")
    public List<OrderGoodSnapshot> getOrderGoodSnapshotByOrderNum(String orderNum) {
        return mapper.getOrderGoodSnapshotByOrderNum(orderNum);
    }

    @Override
    @CacheEvict(value = "orderGoodSnapshot", allEntries = true)
    public void updateReceiptQuantity(List<OrderGoodSnapshot> orderGoodSnapshots) {
        mapper.updateReceiptQuantity(orderGoodSnapshots);
    }

    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshotPosByOrderId_'+#orderId+'_'+#pageIndex+'_'+#pageSize")
    public List<GoodSnapshotVO> getOrderGoodSnapshotPosByOrderId(String orderId,int pageIndex,int pageSize) {
        return mapper.getOrderGoodSnapshotPosByOrderId(orderId,pageIndex,pageSize);
    }

    @Override
    public int getOrderGoodSnapshotPosByOrderIdCount(String orderId, int pageIndex, int pageSize) {
        return mapper.getOrderGoodSnapshotPosByOrderIdCount(orderId,pageIndex,pageSize);
    }

    @Override
    @CacheEvict(value = "orderGoodSnapshot", allEntries = true)
    public void changeOrderGoodSnapshotPos(GoodSnapshotPosVO goodSnapshotPosVO) {
        mapper.changeOrderGoodSnapshotPos(goodSnapshotPosVO);
    }

    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshotPosByMap_'+#map")
    public List<OrderGoodSnapshot> getOrderGoodSnapshotPosByMap(Map map) {
        return  mapper.getOrderGoodSnapshotPosByMap(map);
    }

    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshotPosByOrderInIds_'+#orderIds")
    public List<OrderGoodSnapshot> getOrderGoodSnapshotPosByOrderInIds(List<String> orderIds) {
        return mapper.getOrderGoodSnapshotPosByOrderInIds(orderIds);
    }

    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshotFormatDateByOrderInIds_'+#orderIds")
    public List<OrderGoodSnapshotFormatDate> getOrderGoodSnapshotFormatDateByOrderInIds(List<String> orderIds) {
        return mapper.getOrderGoodSnapshotFormatDateByOrderInIds(orderIds);
    }

    @Override
    @Cacheable(value = "orderGoodSnapshot", key = "'getOrderGoodSnapshotByIds_'+#map")
    public List<OrderGoodSnapshot> getOrderGoodSnapshotByIds(List<String> ids) {
        return mapper.getOrderGoodSnapshotByIds(ids);
    }

    @Override
    @CacheEvict(value = "orderGoodSnapshot", allEntries = true)
    public void changePrimaryOrderGoodSnapshotNormalQuantity(OrderGoodSnapshot orderGoodSnapshot) {
        mapper.changePrimaryOrderGoodSnapshotNormalQuantity(orderGoodSnapshot);
    }

}
