package com.tuoniaostore.order.cache;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.vo.order.OrderGoodSnapshotFormatDate;
import com.tuoniaostore.order.VO.GoodSnapshotPosVO;
import com.tuoniaostore.order.VO.GoodSnapshotVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 商品销售快照表
大部分字段为冗余字段
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public interface OrderGoodSnapshotCache {

    void addOrderGoodSnapshot(OrderGoodSnapshot orderGoodSnapshot);
    void addOrderGoodSnapshotPos(OrderGoodSnapshot orderGoodSnapshot);

    OrderGoodSnapshot getOrderGoodSnapshot(String id);

    List<OrderGoodSnapshot> getOrderGoodSnapshots(int pageIndex, int pageSize, int status, String name);
    List<OrderGoodSnapshot> getOrderGoodSnapshotAll();
    int getOrderGoodSnapshotCount(int status, String name);

    void changeOrderGoodSnapshot(OrderGoodSnapshot orderGoodSnapshot);
    void batchChangeOrderGoodSnapshot(List<OrderGoodSnapshot> orderGoodSnapshotChangeList);

    List<OrderGoodSnapshot> getOrderGoodSnapshotByOrderId(String orderId);
    List<OrderGoodSnapshot> getOrderGoodSnapshotByOrderInIds(List<String> orderIds);
    void changeOrderGoodSnapshotNormalQuantity(OrderGoodSnapshot orderGoodSnapshot);
    List<OrderGoodSnapshot> getOrderGoodSnapshotByMap( Map<String,Object> map);

    List<OrderGoodSnapshot> getOrderGoodSnapshotByOrderNum(String orderNum);

    void updateReceiptQuantity(List<OrderGoodSnapshot> orderGoodSnapshots);

    List<GoodSnapshotVO> getOrderGoodSnapshotPosByOrderId(String orderId,int pageIndex,int pageSize);
    int getOrderGoodSnapshotPosByOrderIdCount(String orderId,int pageIndex,int pageSize);

    void changeOrderGoodSnapshotPos(GoodSnapshotPosVO goodSnapshotPosVO);

    List<OrderGoodSnapshot> getOrderGoodSnapshotPosByMap(Map map);

    List<OrderGoodSnapshot> getOrderGoodSnapshotPosByOrderInIds(List<String> orderIds);
    List<OrderGoodSnapshotFormatDate> getOrderGoodSnapshotFormatDateByOrderInIds(List<String> orderIds);

    List<OrderGoodSnapshot> getOrderGoodSnapshotByIds(List<String> ids);
    void changePrimaryOrderGoodSnapshotNormalQuantity(OrderGoodSnapshot orderGoodSnapshot);

}
