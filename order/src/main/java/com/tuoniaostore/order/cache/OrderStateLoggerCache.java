package com.tuoniaostore.order.cache;
import com.tuoniaostore.datamodel.order.OrderStateLogger;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public interface OrderStateLoggerCache {

    void addOrderStateLogger(OrderStateLogger orderStateLogger);

    OrderStateLogger getOrderStateLogger(String id);

    List<OrderStateLogger> getOrderStateLoggers(int pageIndex, int pageSize, int status, String name);
    List<OrderStateLogger> getOrderStateLoggerAll();
    int getOrderStateLoggerCount(int status, String name);
    void changeOrderStateLogger(OrderStateLogger orderStateLogger);

}
