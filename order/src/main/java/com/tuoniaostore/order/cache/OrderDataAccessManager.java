package com.tuoniaostore.order.cache;

import com.tuoniaostore.datamodel.order.*;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.vo.order.*;
import com.tuoniaostore.order.VO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单实体表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
public class OrderDataAccessManager {


    @Autowired
    OrderEntryCache orderEntryCache;

    public void addOrderEntry(OrderEntry orderEntry) {
        orderEntryCache.addOrderEntry(orderEntry);
    }

    public void addOrderEntryPos(OrderEntry orderEntry) {
        orderEntryCache.addOrderEntryPos(orderEntry);
    }

    public OrderEntry getOrderEntry(String id) {
        return orderEntryCache.getOrderEntry(id);
    }

    public List<OrderEntry> getOrderEntrys(Map<String, Object> map) {
        return orderEntryCache.getOrderEntrys(map);
    }

    public List<OrderEntry> getOrderEntrysByShippingUserIdAndType(String userId, int type, List<String> strings, int pageIndex, int pageSize) {
        return orderEntryCache.getOrderEntrysByShippingUserIdAndType(userId, type, strings, pageIndex, pageSize);
    }

    public List<OrderEntry> getOrderEntrysByShippingUserIdAndTypeBusiness(String userId, int type, int staus, int pageIndex, int pageSize) {
        return orderEntryCache.getOrderEntrysByShippingUserIdAndTypeBusiness(userId, type, staus, pageIndex, pageSize);
    }

    public List<OrderEntry> getOrderEntryAll() {
        return orderEntryCache.getOrderEntryAll();
    }
    public void changeOrderEntryStatusAndDeliverStatus(OrderEntry orderEntry) {
        orderEntryCache.changeOrderEntryStatusAndDeliverStatus(orderEntry);
    }
    public List<OrderEntry> getOrderEntryListBySequenceNumber(String sequenceNumber) {
        return orderEntryCache.getOrderEntryListBySequenceNumber(sequenceNumber);
    }
    public List<OrderEntry> getLazyReceivingGoods() {
        return orderEntryCache.getLazyReceivingGoods();
    }

    /**
     * @param partnerUserId 下单用户id
     * @param searchKeys
     * @param pageStartIndex
     * @param pageSize
     */
    public List<OrderEntry> getOrderEntryListByPartnerUserIdAndType(String partnerUserId, int type, HashMap<String, Object> searchKeys, int pageStartIndex, int pageSize) {
        return orderEntryCache.getOrderEntryListByPartnerUserIdAndType(partnerUserId, type, searchKeys, pageStartIndex, pageSize);
    }

    public int getOrderEntryListByPartnerUserIdAndTypeCount(String partnerUserId, int type, HashMap<String, Object> searchKeys) {
        return orderEntryCache.getOrderEntryListByPartnerUserIdAndTypeCount(partnerUserId, type, searchKeys);
    }

    public int getOrderEntryCount(Map<String, Object> map) {
        return orderEntryCache.getOrderEntryCount(map);
    }

    public void changeOrderEntry(OrderEntry orderEntry) {
        orderEntryCache.changeOrderEntry(orderEntry);
    }

    public void changeOrderEntryPos(OrderEntry orderEntry) {
        orderEntryCache.changeOrderEntryPos(orderEntry);
    }

    public void changeOrderEntryType(OrderEntry orderEntry) {
        orderEntryCache.changeOrderEntryType(orderEntry);
    }

    public int getOrderEntryPosCount(Map<String, Object> map) {
        return orderEntryCache.getOrderEntryPosCount(map);
    }

    public List<OrderEntry> getOrderEntrysPos(Map<String, Object> map) {
        return orderEntryCache.getOrderEntrysPos(map);
    }

    public List<OrderEntryGoodTypeVO> getGoodTypeStatistics(Map map) {
        return orderEntryCache.getGoodTypeStatistics(map);
    }

    public OrderEntry getOrderEntryDaySortNumberDesc(Map<String, Object> map) {
        return orderEntryCache.getOrderEntryDaySortNumberDesc(map);
    }

    public List<OrderEntryPosFormatDate> getOrderEntrysPosFormatDate(Map<String, Object> map) {
        return orderEntryCache.getOrderEntrysPosFormatDate(map);
    }

    public List<OrderPayTypeSumVo> getOrderEntryGruopByPayTtpe(String loginTime, String logoutTime, String startTime, String endTime, String saleUserId) {
        return orderEntryCache.getOrderEntryGruopByPayTtpe(loginTime, logoutTime, startTime, endTime, saleUserId);
    }

    public int getOrderEntrysPosount(Map<String, Object> map) {
        return orderEntryCache.getOrderEntrysPosount(map);
    }

    public List<OrderEntryPriceVO> getOrderEntrPosPrice(Map map) {
        return orderEntryCache.getOrderEntrPosPrice(map);
    }

    public OrderEntrPosSum getOrderEntrPosSumPrice(Map map) {
        return orderEntryCache.getOrderEntrPosSumPrice(map);
    }

    public List<GoodSaleMsgVO> getOrderEntryForYesterDayAll(String shopId) {
        return orderEntryCache.getOrderEntryForYesterDayAll(shopId);
    }

    public List<OrderEntry> getOrderEntryReturn(String loginTime, String logoutTime, String saleUserId) {
        return orderEntryCache.getOrderEntryReturn(loginTime, logoutTime, saleUserId);
    }

    public void updateOrderEntrysPosRefundStatus(OrderEntry orderNumer) {
        orderEntryCache.updateOrderEntrysPosRefundStatus(orderNumer);
    }

    public List<PayLogNumbersVo> getOrderEntrysPosNumberDate(Map<String, Object> map) {
        return orderEntryCache.getOrderEntrysPosNumberDate(map);
    }

    public OrderEntry getOrderEntryDaySortDesc(Map<String, Object> map) {
        return orderEntryCache.getOrderEntryDaySortDesc(map);
    }

    public Map<String, Object> getOrderEntrysPostForTotal(int orderType, String todayTime) {
        return orderEntryCache.getOrderEntrysPostForTotal(orderType,todayTime);
    }

    public List<OrderEntrysTotalVO> getOrderEntrysByTypeCountForEveryDay(int orderType, String startTime, String endTime) {
        return orderEntryCache.getOrderEntrysByTypeCountForEveryDay(orderType,startTime,endTime);
    }

    public List<OrderEntrysTotalVO> getOrderEntrysByTypeCountForEveryWeek(int orderType, String startTime, String endTime) {
        return orderEntryCache.getOrderEntrysByTypeCountForEveryWeek(orderType,startTime,endTime);
    }

    public List<OrderEntrysTotalVO> getOrderEntrysByTypeCountForEveryMonth(int orderType, String startTime, String endTime) {
        return orderEntryCache.getOrderEntrysByTypeCountForEveryMonth(orderType,startTime,endTime);
    }

    public OrderEntry getOrderEntryAndPosById(String orderId) {
        return orderEntryCache.getOrderEntryAndPosById(orderId);
    }
    public List<OrderPayTypeSumAndRefundStatisticVo> getOrderEntryGruopByPayTtpeStatistic(Map map) {
        return orderEntryCache.getOrderEntryGruopByPayTtpeStatistic(map);
    }

    public List<OrderEntry> getOrderEntryByOrderNumberAll(String orderNum) {
        return orderEntryCache.getOrderEntryByOrderNumberAll(orderNum);
    }

    public List<BusinessSideAppletReport> getBusinessSideAppletReport(String startTime, String endTime, String ownId) {
        return orderEntryCache.getBusinessSideAppletReport(startTime, endTime, ownId);
    }

    public List<BusinessSideAppletReportForEveryDay> getBusinessSideAppletReportForeveryDay(String startTime, String sixDayTime, String ownId) {
        return orderEntryCache.getBusinessSideAppletReportForeveryDay(startTime, sixDayTime, ownId);
    }


    @Autowired
    OrderEntryPosCache orderEntryPosCache;

    public List<OrderEntryPos> getOrderEntryPosList(Map<String, Object> params, int pageStartIndex, int pageSize) {
        return orderEntryPosCache.getOrderEntryPosList(params, pageStartIndex, pageSize);
    }

    public int getOrderEntryPosListCount(Map<String, Object> params) {
        return orderEntryPosCache.getOrderEntryPosListCount(params);
    }

    @Autowired
    OrderGoodSnapshotCache orderGoodSnapshotCache;

    public void addOrderGoodSnapshot(OrderGoodSnapshot orderGoodSnapshot) {
        orderGoodSnapshotCache.addOrderGoodSnapshot(orderGoodSnapshot);
    }

    public void addOrderGoodSnapshotPos(OrderGoodSnapshot orderGoodSnapshot) {
        orderGoodSnapshotCache.addOrderGoodSnapshotPos(orderGoodSnapshot);
    }

    public OrderGoodSnapshot getOrderGoodSnapshot(String id) {
        return orderGoodSnapshotCache.getOrderGoodSnapshot(id);
    }

    public List<OrderGoodSnapshot> getOrderGoodSnapshots(int status, int pageIndex, int pageSize, String name) {
        return orderGoodSnapshotCache.getOrderGoodSnapshots(status, pageIndex, pageSize, name);
    }

    public List<OrderGoodSnapshot> getOrderGoodSnapshotAll() {
        return orderGoodSnapshotCache.getOrderGoodSnapshotAll();
    }

    public int getOrderGoodSnapshotCount(int status, String name) {
        return orderGoodSnapshotCache.getOrderGoodSnapshotCount(status, name);
    }

    public void changeOrderGoodSnapshot(OrderGoodSnapshot orderGoodSnapshot) {
        orderGoodSnapshotCache.changeOrderGoodSnapshot(orderGoodSnapshot);
    }

    @Async
    public void batchChangeOrderGoodSnapshot(List<OrderGoodSnapshot> orderGoodSnapshotChangeList) {
        orderGoodSnapshotCache.batchChangeOrderGoodSnapshot(orderGoodSnapshotChangeList);
    }

    public List<OrderGoodSnapshot> getOrderGoodSnapshotByOrderId(String orderId) {
        return orderGoodSnapshotCache.getOrderGoodSnapshotByOrderId(orderId);
    }

    public List<OrderGoodSnapshot> getOrderGoodSnapshotByOrderInIds(List<String> orderIds) {
        return orderGoodSnapshotCache.getOrderGoodSnapshotByOrderInIds(orderIds);
    }

    public void changeOrderGoodSnapshotNormalQuantity(OrderGoodSnapshot orderGoodSnapshot) {
        orderGoodSnapshotCache.changeOrderGoodSnapshotNormalQuantity(orderGoodSnapshot);
    }

    public List<OrderGoodSnapshot> getOrderGoodSnapshotByOrderNum(String orderNum) {
        return orderGoodSnapshotCache.getOrderGoodSnapshotByOrderNum(orderNum);
    }

    public List<OrderGoodSnapshot> getOrderGoodSnapshotByMap(Map<String, Object> map) {
        return orderGoodSnapshotCache.getOrderGoodSnapshotByMap(map);
    }

    public void updateReceiptQuantity(List<OrderGoodSnapshot> orderGoodSnapshots) {
        orderGoodSnapshotCache.updateReceiptQuantity(orderGoodSnapshots);
    }

    public List<GoodSnapshotVO> getOrderGoodSnapshotPosByOrderId(String orderId, int pageIndex, int pageSize) {
        return orderGoodSnapshotCache.getOrderGoodSnapshotPosByOrderId(orderId, pageIndex, pageSize);
    }

    public void changeOrderGoodSnapshotPos(GoodSnapshotPosVO goodSnapshotPosVO) {
        orderGoodSnapshotCache.changeOrderGoodSnapshotPos(goodSnapshotPosVO);
    }

    public List<OrderGoodSnapshot> getOrderGoodSnapshotPosByMap(Map map) {
        return orderGoodSnapshotCache.getOrderGoodSnapshotPosByMap(map);
    }
    public int getOrderGoodSnapshotPosByOrderIdCount(String orderId, int pageIndex, int pageSize) {
        return orderGoodSnapshotCache.getOrderGoodSnapshotPosByOrderIdCount(orderId, pageIndex, pageSize);
    }
    public List<OrderGoodSnapshot> getOrderGoodSnapshotPosByOrderInIds(List<String> orderIds) {
        return orderGoodSnapshotCache.getOrderGoodSnapshotPosByOrderInIds(orderIds);
    }

    public List<OrderGoodSnapshotFormatDate> getOrderGoodSnapshotFormatDateByOrderInIds(List<String> orderIds) {
        return orderGoodSnapshotCache.getOrderGoodSnapshotFormatDateByOrderInIds(orderIds);
    }

    public List<OrderGoodSnapshot> getOrderGoodSnapshotByIds(List<String> ids) {
        return orderGoodSnapshotCache.getOrderGoodSnapshotByIds(ids);
    }

    public void changePrimaryOrderGoodSnapshotNormalQuantity(OrderGoodSnapshot orderGoodSnapshot) {
        orderGoodSnapshotCache.changePrimaryOrderGoodSnapshotNormalQuantity(orderGoodSnapshot);
    }

    @Autowired
    OrderPushedLoggerCache orderPushedLoggerCache;

    public void addOrderPushedLogger(OrderPushedLogger orderPushedLogger) {
        orderPushedLoggerCache.addOrderPushedLogger(orderPushedLogger);
    }

    public OrderPushedLogger getOrderPushedLogger(String id) {
        return orderPushedLoggerCache.getOrderPushedLogger(id);
    }

    public List<OrderPushedLogger> getOrderPushedLoggers(int status, int pageIndex, int pageSize, String name) {
        return orderPushedLoggerCache.getOrderPushedLoggers(status, pageIndex, pageSize, name);
    }

    public List<OrderPushedLogger> getOrderPushedLoggerAll() {
        return orderPushedLoggerCache.getOrderPushedLoggerAll();
    }

    public int getOrderPushedLoggerCount(int status, String name) {
        return orderPushedLoggerCache.getOrderPushedLoggerCount(status, name);
    }

    public void changeOrderPushedLogger(OrderPushedLogger orderPushedLogger) {
        orderPushedLoggerCache.changeOrderPushedLogger(orderPushedLogger);
    }

    @Autowired
    OrderSequenceCache orderSequenceCache;

    public void addOrderSequence(OrderSequence orderSequence) {
        orderSequenceCache.addOrderSequence(orderSequence);
    }

    public OrderSequence getOrderSequence(String id) {
        return orderSequenceCache.getOrderSequence(id);
    }

    public List<OrderSequence> getOrderSequences(int status, int pageIndex, int pageSize, String name) {
        return orderSequenceCache.getOrderSequences(status, pageIndex, pageSize, name);
    }

    public List<OrderSequence> getOrderSequenceAll() {
        return orderSequenceCache.getOrderSequenceAll();
    }

    public int getOrderSequenceCount(int status, String name) {
        return orderSequenceCache.getOrderSequenceCount(status, name);
    }

    public void changeOrderSequence(OrderSequence orderSequence) {
        orderSequenceCache.changeOrderSequence(orderSequence);
    }

    @Autowired
    OrderStateLoggerCache orderStateLoggerCache;

    public void addOrderStateLogger(OrderStateLogger orderStateLogger) {
        orderStateLoggerCache.addOrderStateLogger(orderStateLogger);
    }

    public OrderStateLogger getOrderStateLogger(String id) {
        return orderStateLoggerCache.getOrderStateLogger(id);
    }

    public List<OrderStateLogger> getOrderStateLoggers(int status, int pageIndex, int pageSize, String name) {
        return orderStateLoggerCache.getOrderStateLoggers(status, pageIndex, pageSize, name);
    }

    public List<OrderStateLogger> getOrderStateLoggerAll() {
        return orderStateLoggerCache.getOrderStateLoggerAll();
    }

    public int getOrderStateLoggerCount(int status, String name) {
        return orderStateLoggerCache.getOrderStateLoggerCount(status, name);
    }

    public void changeOrderStateLogger(OrderStateLogger orderStateLogger) {
        orderStateLoggerCache.changeOrderStateLogger(orderStateLogger);
    }

    @Autowired
    OrderStatusListenerCache orderStatusListenerCache;

    public void addOrderStatusListener(OrderStatusListener orderStatusListener) {
        orderStatusListenerCache.addOrderStatusListener(orderStatusListener);
    }

    public OrderStatusListener getOrderStatusListener(String id) {
        return orderStatusListenerCache.getOrderStatusListener(id);
    }

    public List<OrderStatusListener> getOrderStatusListeners(int status, int pageIndex, int pageSize, String name) {
        return orderStatusListenerCache.getOrderStatusListeners(status, pageIndex, pageSize, name);
    }

    public List<OrderStatusListener> getOrderStatusListenerAll() {
        return orderStatusListenerCache.getOrderStatusListenerAll();
    }

    public int getOrderStatusListenerCount(int status, String name) {
        return orderStatusListenerCache.getOrderStatusListenerCount(status, name);
    }

    public void changeOrderStatusListener(OrderStatusListener orderStatusListener) {
        orderStatusListenerCache.changeOrderStatusListener(orderStatusListener);
    }

    @Autowired
    OrderRefundEntryCache orderRefundEntryCache;

    public void addOrderRefundEntry(OrderRefundEntry orderRefundEntry) {
        orderRefundEntryCache.addOrderRefundEntry(orderRefundEntry);
    }

    public List<OrderRefundEntry> getOrderRefundEntryList(HashMap<String, Object> parameters, int pageStartIndex, int pageSize) {
        return orderRefundEntryCache.getOrderRefundEntryList(parameters, pageStartIndex, pageSize);
    }

    public int getOrderRefundEntryListCount(HashMap<String, Object> parameters) {
        return orderRefundEntryCache.getOrderRefundEntryListCount(parameters);
    }

    public List<OrderRefundEntry> getOrderRefundEntryListBySequenceNumber(String sequenceNumber) {
        return orderRefundEntryCache.getOrderRefundEntryListBySequenceNumber(sequenceNumber);
    }

    public OrderRefundEntry getOrderRefundEntry(String id) {
        return orderRefundEntryCache.getOrderRefundEntry(id);
    }

    public void changeOrderRefundEntryStatus(OrderRefundEntry orderRefundEntry) {
        orderRefundEntryCache.changeOrderRefundEntryStatus(orderRefundEntry);
    }

    @Autowired
    OrderRefundGoodSnapshotCache orderRefundGoodSnapshotCache;

    public void addOrderRefundGoodSnapshot(OrderRefundGoodSnapshot orderRefundGoodSnapshot) {
        orderRefundGoodSnapshotCache.addOrderRefundGoodSnapshot(orderRefundGoodSnapshot);
    }

    @Async
    public void batchAddOrderRefundGoodSnapshot(List<OrderRefundGoodSnapshot> orderRefundGoodSnapshotAddList) {
        orderRefundGoodSnapshotCache.batchAddOrderRefundGoodSnapshot(orderRefundGoodSnapshotAddList);
    }

    public List<OrderRefundGoodSnapshot> getOrderRefundGoodSnapshotByOrderId(String orderId, int pageStartIndex, int pageSize) {
        return orderRefundGoodSnapshotCache.getOrderRefundGoodSnapshotByOrderId(orderId, pageStartIndex, pageSize);
    }
}
