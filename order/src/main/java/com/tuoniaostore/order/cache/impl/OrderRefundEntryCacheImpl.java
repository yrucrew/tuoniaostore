package com.tuoniaostore.order.cache.impl;

import com.tuoniaostore.datamodel.order.OrderRefundEntry;
import com.tuoniaostore.order.cache.OrderRefundEntryCache;
import com.tuoniaostore.order.data.OrderRefundEntryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
@CacheConfig(cacheNames = "orderRefundEntry")
public class OrderRefundEntryCacheImpl implements OrderRefundEntryCache {

    @Autowired
    OrderRefundEntryMapper mapper;

    @Override
    @CacheEvict(value = "orderRefundEntry", allEntries = true)
    public void addOrderRefundEntry(OrderRefundEntry orderRefundEntry) {
        mapper.addOrderRefundEntry(orderRefundEntry);
    }

    @Override
    @Cacheable(value = "orderRefundEntry", key = "'getOrderRefundEntryList_'+#parameters+#pageStartIndex+#pageSize")
    public List<OrderRefundEntry> getOrderRefundEntryList(HashMap<String, Object> parameters, int pageStartIndex, int pageSize) {
        return mapper.getOrderRefundEntryList(parameters, pageStartIndex, pageSize);
    }

    @Override
    @Cacheable(value = "orderRefundEntry", key = "'getOrderRefundEntryListCount_'+#parameters")
    public int getOrderRefundEntryListCount(HashMap<String, Object> parameters) {
        return mapper.getOrderRefundEntryListCount(parameters);
    }

    @Override
    @Cacheable(value = "orderRefundEntry", key = "'getOrderRefundEntry_'+#id")
    public OrderRefundEntry getOrderRefundEntry(String id) {
        return mapper.getOrderRefundEntry(id);
    }

    @Override
    @Cacheable(value = "orderRefundEntry", key = "'getOrderRefundEntryListBySequenceNumber_'+#sequenceNumber")
    public List<OrderRefundEntry> getOrderRefundEntryListBySequenceNumber(String sequenceNumber) {
        return mapper.getOrderRefundEntryListBySequenceNumber(sequenceNumber);
    }

    @Override
    @CacheEvict(value = "orderRefundEntry", allEntries = true)
    public void changeOrderRefundEntryStatus(OrderRefundEntry orderRefundEntry) {
        mapper.changeOrderRefundEntryStatus(orderRefundEntry);
    }

}
