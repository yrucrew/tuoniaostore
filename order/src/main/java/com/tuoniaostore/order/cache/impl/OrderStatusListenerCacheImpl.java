package com.tuoniaostore.order.cache.impl;
        import com.tuoniaostore.datamodel.order.OrderStatusListener;
        import com.tuoniaostore.order.cache.OrderStatusListenerCache;
        import com.tuoniaostore.order.data.OrderStatusListenerMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
@CacheConfig(cacheNames = "orderStatusListener")
public class OrderStatusListenerCacheImpl implements OrderStatusListenerCache {

    @Autowired
    OrderStatusListenerMapper mapper;

    @Override
    @CacheEvict(value = "orderStatusListener", allEntries = true)
    public void addOrderStatusListener(OrderStatusListener orderStatusListener) {
        mapper.addOrderStatusListener(orderStatusListener);
    }

    @Override
    @Cacheable(value = "orderStatusListener", key = "'getOrderStatusListener_'+#id")
    public   OrderStatusListener getOrderStatusListener(String id) {
        return mapper.getOrderStatusListener(id);
    }

    @Override
    @Cacheable(value = "orderStatusListener", key = "'getOrderStatusListeners_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<OrderStatusListener> getOrderStatusListeners( int status,int pageIndex, int pageSize, String name) {
        return mapper.getOrderStatusListeners(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "orderStatusListener", key = "'getOrderStatusListenerAll'")
    public List<OrderStatusListener> getOrderStatusListenerAll() {
        return mapper.getOrderStatusListenerAll();
    }

    @Override
    @Cacheable(value = "orderStatusListener", key = "'getOrderStatusListenerCount_'+#status+'_'+#name")
    public int getOrderStatusListenerCount(int status, String name) {
        return mapper.getOrderStatusListenerCount(status, name);
    }

    @Override
    @CacheEvict(value = "orderStatusListener", allEntries = true)
    public void changeOrderStatusListener(OrderStatusListener orderStatusListener) {
        mapper.changeOrderStatusListener(orderStatusListener);
    }

}
