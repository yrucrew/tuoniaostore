package com.tuoniaostore.order.cache.impl;
        import com.tuoniaostore.datamodel.order.OrderPushedLogger;
        import com.tuoniaostore.order.cache.OrderPushedLoggerCache;
        import com.tuoniaostore.order.data.OrderPushedLoggerMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 订单的推送日志
与未接订单日志的区别为该表不物理删除数据 可以查询订单是否曾经已执行推送 且明白推送的目标
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
@CacheConfig(cacheNames = "orderPushedLogger")
public class OrderPushedLoggerCacheImpl implements OrderPushedLoggerCache {

    @Autowired
    OrderPushedLoggerMapper mapper;

    @Override
    @CacheEvict(value = "orderPushedLogger", allEntries = true)
    public void addOrderPushedLogger(OrderPushedLogger orderPushedLogger) {
        mapper.addOrderPushedLogger(orderPushedLogger);
    }

    @Override
    @Cacheable(value = "orderPushedLogger", key = "'getOrderPushedLogger_'+#id")
    public   OrderPushedLogger getOrderPushedLogger(String id) {
        return mapper.getOrderPushedLogger(id);
    }

    @Override
    @Cacheable(value = "orderPushedLogger", key = "'getOrderPushedLoggers_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<OrderPushedLogger> getOrderPushedLoggers(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getOrderPushedLoggers(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "orderPushedLogger", key = "'getOrderPushedLoggerAll'")
    public List<OrderPushedLogger> getOrderPushedLoggerAll() {
        return mapper.getOrderPushedLoggerAll();
    }

    @Override
    @Cacheable(value = "orderPushedLogger", key = "'getOrderPushedLoggerCount_'+#status+'_'+#name")
    public int getOrderPushedLoggerCount(int status, String name) {
        return mapper.getOrderPushedLoggerCount(status, name);
    }

    @Override
    @CacheEvict(value = "orderPushedLogger", allEntries = true)
    public void changeOrderPushedLogger(OrderPushedLogger orderPushedLogger) {
        mapper.changeOrderPushedLogger(orderPushedLogger);
    }

}
