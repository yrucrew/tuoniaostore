package com.tuoniaostore.order.cache.impl;
        import com.tuoniaostore.datamodel.order.OrderStateLogger;
        import com.tuoniaostore.order.cache.OrderStateLoggerCache;
        import com.tuoniaostore.order.data.OrderStateLoggerMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.cache.annotation.CacheConfig;
        import org.springframework.cache.annotation.CacheEvict;
        import org.springframework.cache.annotation.Cacheable;
        import org.springframework.stereotype.Component;
        import java.util.List;

/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
@Component
@CacheConfig(cacheNames = "orderStateLogger")
public class OrderStateLoggerCacheImpl implements OrderStateLoggerCache {

    @Autowired
    OrderStateLoggerMapper mapper;

    @Override
    @CacheEvict(value = "orderStateLogger", allEntries = true)
    public void addOrderStateLogger(OrderStateLogger orderStateLogger) {
        mapper.addOrderStateLogger(orderStateLogger);
    }

    @Override
    @Cacheable(value = "orderStateLogger", key = "'getOrderStateLogger_'+#id")
    public   OrderStateLogger getOrderStateLogger(String id) {
        return mapper.getOrderStateLogger(id);
    }

    @Override
    @Cacheable(value = "orderStateLogger", key = "'getOrderStateLoggers_'+#pageIndex+'_'+#pageSize+'_'+#status+'_'+#name")
    public List<OrderStateLogger> getOrderStateLoggers(int status,int pageIndex, int pageSize,  String name) {
        return mapper.getOrderStateLoggers(status,pageIndex, pageSize,  name);
    }

    @Override
    @Cacheable(value = "orderStateLogger", key = "'getOrderStateLoggerAll'")
    public List<OrderStateLogger> getOrderStateLoggerAll() {
        return mapper.getOrderStateLoggerAll();
    }

    @Override
    @Cacheable(value = "orderStateLogger", key = "'getOrderStateLoggerCount_'+#status+'_'+#name")
    public int getOrderStateLoggerCount(int status, String name) {
        return mapper.getOrderStateLoggerCount(status, name);
    }

    @Override
    @CacheEvict(value = "orderStateLogger", allEntries = true)
    public void changeOrderStateLogger(OrderStateLogger orderStateLogger) {
        mapper.changeOrderStateLogger(orderStateLogger);
    }

}
