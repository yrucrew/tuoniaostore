package com.tuoniaostore.order.cache.impl;

import com.tuoniaostore.datamodel.vo.order.OrderEntryPos;
import com.tuoniaostore.order.cache.OrderEntryPosCache;
import com.tuoniaostore.order.data.OrderEntryPosMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@CacheConfig(cacheNames = "orderEntryPos")
public class OrderEntryPosCacheImpl implements OrderEntryPosCache {

    @Autowired
    OrderEntryPosMapper mapper;

    @Override
    public List<OrderEntryPos> getOrderEntryPosList(Map<String, Object> params, int pageStartIndex, int pageSize) {
        return mapper.getOrderEntryPosList(params, pageStartIndex, pageSize);
    }

    @Override
    public int getOrderEntryPosListCount(Map<String, Object> params) {
        return mapper.getOrderEntryPosListCount(params);
    }
}
