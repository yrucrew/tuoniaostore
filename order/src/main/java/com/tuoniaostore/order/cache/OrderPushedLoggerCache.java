package com.tuoniaostore.order.cache;
import com.tuoniaostore.datamodel.order.OrderPushedLogger;

import java.util.List;
/**
 * 订单的推送日志
与未接订单日志的区别为该表不物理删除数据 可以查询订单是否曾经已执行推送 且明白推送的目标
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public interface OrderPushedLoggerCache {

    void addOrderPushedLogger(OrderPushedLogger orderPushedLogger);

    OrderPushedLogger getOrderPushedLogger(String id);

    List<OrderPushedLogger> getOrderPushedLoggers(int pageIndex, int pageSize, int status, String name);
    List<OrderPushedLogger> getOrderPushedLoggerAll();
    int getOrderPushedLoggerCount(int status, String name);
    void changeOrderPushedLogger(OrderPushedLogger orderPushedLogger);

}
