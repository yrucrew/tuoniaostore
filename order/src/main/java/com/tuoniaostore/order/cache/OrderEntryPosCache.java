package com.tuoniaostore.order.cache;

import com.tuoniaostore.datamodel.vo.order.OrderEntryPos;

import java.util.List;
import java.util.Map;

public interface OrderEntryPosCache {

    List<OrderEntryPos> getOrderEntryPosList(Map<String, Object> params, int pageStartIndex, int pageSize);

    int getOrderEntryPosListCount(Map<String, Object> params);
}
