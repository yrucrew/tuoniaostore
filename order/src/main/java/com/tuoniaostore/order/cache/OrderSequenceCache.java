package com.tuoniaostore.order.cache;
import com.tuoniaostore.datamodel.order.OrderSequence;

import java.util.List;
/**
 * 
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public interface OrderSequenceCache {

    void addOrderSequence(OrderSequence orderSequence);

    OrderSequence getOrderSequence(String id);

    List<OrderSequence> getOrderSequences(int pageIndex, int pageSize, int status, String name);
    List<OrderSequence> getOrderSequenceAll();
    int getOrderSequenceCount(int status, String name);
    void changeOrderSequence(OrderSequence orderSequence);

}
