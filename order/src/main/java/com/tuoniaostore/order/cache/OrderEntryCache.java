package com.tuoniaostore.order.cache;

import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.vo.order.*;
import com.tuoniaostore.order.VO.*;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单实体表
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public interface OrderEntryCache {

    void addOrderEntry(OrderEntry orderEntry);

    void addOrderEntryPos(OrderEntry orderEntry);

    OrderEntry getOrderEntry(String id);

    List<OrderEntry> getOrderEntrys(Map<String, Object> map);

    List<OrderEntry> getOrderEntryAll();

    int getOrderEntryCount(Map<String, Object> map);

    void changeOrderEntry(OrderEntry orderEntry);

    void changeOrderEntryPos(OrderEntry orderEntry);

    /**
     * 查询供应商订单
     *
     * @param type (0/处理中 、 1/已完成)
     * @return
     */
    List<OrderEntry> getOrderEntrysByShippingUserIdAndType(String userId, int type, List<String> strings, int pageIndex, int pageSize);


    List<OrderEntry> getOrderEntrysByShippingUserIdAndTypeBusiness(String userId, int type, int status, int pageIndex, int pageSize);

    void changeOrderEntryType(OrderEntry orderEntry);

    int getOrderEntryPosCount(Map<String, Object> map);

    List<OrderEntry> getOrderEntrysPos(Map<String, Object> map);

    List<OrderEntryPosFormatDate> getOrderEntrysPosFormatDate(Map<String, Object> map);

    OrderEntry getOrderEntryDaySortNumberDesc(Map<String, Object> map);

    List<OrderPayTypeSumVo> getOrderEntryGruopByPayTtpe(String loginTime, String logoutTime, String startTime, String endTime, String saleUserId);

    int getOrderEntrysPosount(Map<String, Object> map);

    List<OrderEntryPriceVO> getOrderEntrPosPrice(Map map);

    OrderEntrPosSum getOrderEntrPosSumPrice(Map map);

    List<GoodSaleMsgVO> getOrderEntryForYesterDayAll(String shopId);

    List<OrderEntryGoodTypeVO> getGoodTypeStatistics(Map map);

    List<OrderEntry> getOrderEntryReturn(String loginTime, String logoutTime, String saleUserId);

    void updateOrderEntrysPosRefundStatus(OrderEntry orderNumer);

    List<PayLogNumbersVo> getOrderEntrysPosNumberDate(Map<String, Object> map);

    OrderEntry getOrderEntryDaySortDesc(Map<String, Object> map);

    /**
     * @param partnerUserId 下单用户id
     * @param type          订单类型
     * @param searchKeys
     * @param pageStartIndex
     * @param pageSize
     */
    List<OrderEntry> getOrderEntryListByPartnerUserIdAndType(String partnerUserId, int type, HashMap<String, Object> searchKeys, int pageStartIndex, int pageSize);

    Integer getOrderEntryListByPartnerUserIdAndTypeCount(String partnerUserId, int type, HashMap<String, Object> searchKeys);

    void changeOrderEntryStatusAndDeliverStatus(OrderEntry orderEntry);

    Map<String, Object> getOrderEntrysPostForTotal(int orderType, String todayTime);

    List<OrderEntrysTotalVO> getOrderEntrysByTypeCountForEveryDay(int orderType, String startTime, String endTime);

    List<OrderEntrysTotalVO> getOrderEntrysByTypeCountForEveryWeek(int orderType, String startTime, String endTime);

    List<OrderEntrysTotalVO> getOrderEntrysByTypeCountForEveryMonth(int orderType, String startTime, String endTime);

    OrderEntry getOrderEntryAndPosById(String orderId);

    List<OrderPayTypeSumAndRefundStatisticVo> getOrderEntryGruopByPayTtpeStatistic(Map map);

    List<OrderEntry> getOrderEntryByOrderNumberAll(String orderNum);

    List<BusinessSideAppletReport> getBusinessSideAppletReport(String startTime, String endTime, String ownId);

    List<OrderEntry> getOrderEntryListBySequenceNumber(String sequenceNumber);

    List<BusinessSideAppletReportForEveryDay> getBusinessSideAppletReportForeveryDay(String startTime, String sixDayTime, String ownId);

    List<OrderEntry>  getLazyReceivingGoods();
}
