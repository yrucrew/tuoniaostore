package com.tuoniaostore.order;

import com.github.wxpay.sdk.WXPayConstants;
import com.github.wxpay.sdk.WXPayUtil;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.constant.order.OrderStatus;
import com.tuoniaostore.commons.constant.pay.WeiXinTerminalPayTypeEnum;
import com.tuoniaostore.commons.constant.payment.PayTypeEnum;
import com.tuoniaostore.commons.constant.payment.PaymentTypeEnum;
import com.tuoniaostore.commons.support.payment.PaymentCurrencyAccountService;
import com.tuoniaostore.commons.support.payment.PaymentEstimateRevenueService;
import com.tuoniaostore.commons.support.payment.PaymentTransactionLoggerService;
import com.tuoniaostore.commons.support.supplychain.SupplyChainGoodRemoteService;
import com.tuoniaostore.commons.utils.WeiXinPayUtils;
import com.tuoniaostore.commons.utils.decode.AESUtil;
import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.order.OrderStateLogger;
import com.tuoniaostore.datamodel.payment.PaymentEstimateRevenue;
import com.tuoniaostore.datamodel.payment.PaymentTransactionLogger;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.vo.good.GoodIdAndGoodNumberVO;
import com.tuoniaostore.order.cache.OrderDataAccessManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @author oy
 * @description
 * @date 2019/4/20
 */
@Controller
@RequestMapping(value="weixinPay")
public class OrderDataAccessController extends BasicWebService {

    private static final Logger logger = LoggerFactory.getLogger(OrderDataAccessController.class);
    @Autowired
    private OrderDataAccessManager dataAccessManager;

    @RequestMapping("hello")
    @ResponseBody
    public String hello(){
        logger.info("hello进来了------------------------------------");
        return "hello";
    }

    /**
     * 支付完成通知 商家端下单
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "notify",method = RequestMethod.POST)
    public String notify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.debug(".................微信服务器返回消息进来了.................");
        BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream)request.getInputStream()));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while((line = br.readLine())!=null){
            sb.append(line);
        }
        //解析并给微信发回收到通知确认
        Map map = WXPayUtil.xmlToMap(sb.toString());
        map.forEach((x,y)->{
            logger.debug(x+":");
            logger.debug(y+"||||");
        });

        String returnCode = map.get("return_code").toString();
        logger.debug("响应消息：..."+returnCode);//是否支付成功 如果网络问题，微信客户端会发送多次（8次）知道成功为止
        WeiXinPayUtils WeiXin = new WeiXinPayUtils();
        Map<String, String> properties = WeiXin.readProperties();

        if(returnCode.equals("SUCCESS")){
            String resultCode = map.get("result_code").toString();
            if(resultCode.equals("SUCCESS")){
                SortedMap<String, String> packageParams = new TreeMap<String, String>();
                packageParams.put("appid", map.get("appid").toString());
                packageParams.put("attach", map.get("attach").toString());
                packageParams.put("bank_type", map.get("bank_type").toString());
                packageParams.put("cash_fee", map.get("cash_fee").toString());
                packageParams.put("fee_type", map.get("fee_type").toString());
                packageParams.put("is_subscribe", map.get("is_subscribe").toString());
                packageParams.put("mch_id", map.get("mch_id").toString());
                packageParams.put("nonce_str", map.get("nonce_str").toString());
                packageParams.put("openid", map.get("openid").toString());
                packageParams.put("out_trade_no", map.get("out_trade_no").toString());
                packageParams.put("result_code", map.get("result_code").toString());
                packageParams.put("return_code", map.get("return_code").toString());
                packageParams.put("time_end", map.get("time_end").toString());
                packageParams.put("total_fee", map.get("total_fee").toString());
                packageParams.put("trade_type", map.get("trade_type").toString());
                packageParams.put("transaction_id", map.get("transaction_id").toString());
//                String sign = PayUtils.createSign(packageParams,key);
                String sign = WXPayUtil.generateSignature(packageParams, properties.get("mchIdkey"), WXPayConstants.SignType.MD5);//签名 验证
                String originSign = map.get("sign").toString();
                logger.debug("服务器签名:"+originSign);
                logger.debug("我们这边生成的:"+sign);

                if(sign.equals(originSign)){
                    //业务代码
                    try {
                        //订单下单支付回调
                        logger.info("+++++++++开始订单下单支付回调+++++++++++++++++++++");
                        payCallback (packageParams.get("out_trade_no"));
                        logger.info("+++++++++结束订单下单支付回调+++++++++++++++++++++");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                        //签名一致，保存支付流水
                        String xml="<xml>"
                                +"<return_code>SUCCESS</return_code>"
                                +"<return_msg>OK</return_msg>"
                                +"</xml>";

                        return xml;
                 }else{
                        String xml="<xml>"
                                +"<return_code>FAIL</return_code>"
                                +"<return_msg>签名不一致</return_msg>"
                                +"</xml>";
                        return xml;
                    }
                 }else{
                     String xml="<xml>"
                            +"<return_code>FAIL</return_code>"
                            +"<return_msg>支付通知失败</return_msg>"
                            +"</xml>";
                     return xml;
                 }
            }else{
                String xml="<xml>"
                    +"<return_code>FAIL</return_code>"
                    +"<return_msg>支付通知失败</return_msg>"
                    +"</xml>";
                return xml;
            }
    }


    /**
     * 支付完成通知 商家端余额充值
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "notifyWithRecharge",method = RequestMethod.POST)
    public String notifyWithRecharge(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.debug(".................商家端余额充值返回消息进来了.................");
        BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream)request.getInputStream()));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while((line = br.readLine())!=null){
            sb.append(line);
        }
        //解析并给微信发回收到通知确认
        Map map = WXPayUtil.xmlToMap(sb.toString());
        map.forEach((x,y)->{
            logger.debug(x+":");
            logger.debug(y+"||||");
        });

        String returnCode = map.get("return_code").toString();
        WeiXinPayUtils WeiXin = new WeiXinPayUtils();
        Map<String, String> properties = WeiXin.readProperties();
        if(returnCode.equals("SUCCESS")){
            String resultCode = map.get("result_code").toString();
            if(resultCode.equals("SUCCESS")){
                SortedMap<String, String> packageParams = new TreeMap<String, String>();
                packageParams.put("appid", map.get("appid").toString());
                //packageParams.put("attach", map.get("attach").toString()); ///
                packageParams.put("bank_type", map.get("bank_type").toString());
                packageParams.put("cash_fee", map.get("cash_fee").toString());
                packageParams.put("fee_type", map.get("fee_type").toString());
                packageParams.put("is_subscribe", map.get("is_subscribe").toString());
                packageParams.put("mch_id", map.get("mch_id").toString());
                packageParams.put("nonce_str", map.get("nonce_str").toString());
                packageParams.put("openid", map.get("openid").toString());
                packageParams.put("out_trade_no", map.get("out_trade_no").toString());
                packageParams.put("result_code", map.get("result_code").toString());
                packageParams.put("return_code", map.get("return_code").toString());
                packageParams.put("time_end", map.get("time_end").toString());
                packageParams.put("total_fee", map.get("total_fee").toString());
                packageParams.put("trade_type", map.get("trade_type").toString());
                packageParams.put("transaction_id", map.get("transaction_id").toString());
//                String sign = PayUtils.createSign(packageParams,key);
                String sign = WXPayUtil.generateSignature(packageParams, properties.get("mchIdkey"), WXPayConstants.SignType.MD5);//签名 验证
                String originSign = map.get("sign").toString();

                if(sign.equals(originSign)){
                    //业务代码
                    try {
                        //商家端余额充值
                        logger.info("+++++++++开始商家端余额充值回调+++++++++++++++++++++");
                        PaymentCurrencyAccountService.recChangeCallback (packageParams);
                        logger.info("+++++++++结束商家端余额充值回调+++++++++++++++++++++");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //签名一致，保存支付流水
                    String xml="<xml>"
                            +"<return_code>SUCCESS</return_code>"
                            +"<return_msg>OK</return_msg>"
                            +"</xml>";

                    return xml;
                }else{
                    String xml="<xml>"
                            +"<return_code>FAIL</return_code>"
                            +"<return_msg>签名不一致</return_msg>"
                            +"</xml>";
                    return xml;
                }
            }else{
                String xml="<xml>"
                        +"<return_code>FAIL</return_code>"
                        +"<return_msg>支付通知失败</return_msg>"
                        +"</xml>";
                return xml;
            }
        }else{
            String xml="<xml>"
                    +"<return_code>FAIL</return_code>"
                    +"<return_msg>支付通知失败</return_msg>"
                    +"</xml>";
            return xml;
        }
    }

    /**
     * 退款通知
     * @author oy
     * @date 2019/5/22
     * @param request, response
     * @return java.lang.String
     */
    @ResponseBody
    @RequestMapping(value = "notifyWithRefund",method = RequestMethod.POST)
    public String notifyWithRefund(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.debug(".................退款结果返回开始.................");
        BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream)request.getInputStream()));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while((line = br.readLine())!=null){
            sb.append(line);
        }

        //第一次解析xml 这次解析会得到 之前的随机数 商户号啥的
        Map<String, String> stringStringMap1 = WXPayUtil.xmlToMap(sb.toString());

        //拿到 req_info 解码
        String req_info = stringStringMap1.get("req_info");
        String decryptData = AESUtil.decryptData(req_info);//商户秘钥 直接在这个工具类中写死的

        //第一次解析 加密完成的xml
        Map<String, String> stringStringMap = WXPayUtil.xmlToMap(decryptData);
        stringStringMap.forEach((x,y)->{//输出结果 测试一下
            logger.debug("-----------------------------------------------");
            logger.debug("key:"+x);
            logger.debug("value:"+y);
            logger.debug("-----------------------------------------------");
        });
        String out_trade_no = stringStringMap.get("out_trade_no");
        HashMap<String, Object> map = new HashMap<>();
        map.put("refundStatus","1");
        map.put("transSequenceNumber",out_trade_no);
        logger.debug(".................进入回调方法.................");
        PaymentTransactionLoggerService.changePaymentTransactionLoggerMap(map,getHttpServletRequest());
        logger.debug(".................退款结果返回结束.................");

        String xml="<xml>"
                +"<return_code>SUCCESS</return_code>"
                +"<return_msg>OK</return_msg>"
                +"</xml>";

        return xml;
    }

    /**
     * 微信商家下单-支付回调
     * @author sqd
     * @date 2019/4/20
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    public  void payCallback (String orderSequenceNumber){
        Map map = WeiXinPayUtils.queryPayStatus(orderSequenceNumber, WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey());
        String  return_code = (String) map.get("return_code");
        String result_code = (String) map.get("result_code");
        String trade_state = (String) map.get("trade_state");
        String total_fee = (String) map.get("total_fee");
        String attach = (String) map.get("attach");
        //判断是否支付成功
        if(return_code.equals("SUCCESS") && result_code.equals("SUCCESS") && trade_state.equals("SUCCESS") ){
            OrderEntry orderEntry = new OrderEntry();
            orderEntry.setSequenceNumber(orderSequenceNumber);
            orderEntry.setId(null);

            orderEntry.setPaymentType(PayTypeEnum.TRANS_TYPE_WEI.getKey());
            orderEntry.setStatus(OrderStatus.ORDER_TYPE_PAID.getKey());
            orderEntry.setPaymentTime(new Date());
            orderEntry.setPaymentType(PayTypeEnum.TRANS_TYPE_WEI.getKey());;
            dataAccessManager.changeOrderEntry(orderEntry);

            logger.info("============ 修改子订单状态============");
            //添加子单操作记录
            HashMap<String, Object> mapObj = new HashMap<>();
            mapObj.put("sequenceNumbers",orderSequenceNumber);
            mapObj.put("attach",attach);
            addStaueLog(mapObj);
            mapObj.clear();
            orderEntry.setActualPrice(Long.parseLong(total_fee));//实收费用
            orderEntry.setSequenceNumber(null);
            orderEntry.setOrderSequenceNumber(orderSequenceNumber);
            orderEntry.setStatus(OrderStatus.ORDER_TYPE_HAVEORDER.getKey());
            dataAccessManager.changeOrderEntry(orderEntry);
            logger.info("============ 修改主订单状态============"+orderSequenceNumber);
            mapObj.put("primarySequenceNumbers",orderSequenceNumber);
            //添加主单操作记录
            addStaueLog(mapObj);


            //修改库存
            mapObj.clear();
            mapObj.put("orderSequenceNumberOne",orderSequenceNumber);
            List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(mapObj);
            List<GoodIdAndGoodNumberVO> goodIdAndGoodNumberVO = new ArrayList<>();
            if(orderEntrys != null && orderEntrys.size() > 0){
                List<OrderGoodSnapshot> orderGoodSnapshotByOrderId = dataAccessManager.getOrderGoodSnapshotByOrderId(orderEntrys.get(0).getId());
                for (OrderGoodSnapshot goodSnapshot : orderGoodSnapshotByOrderId) {
                    GoodIdAndGoodNumberVO goods = new GoodIdAndGoodNumberVO();
                    goods.setNumber(goodSnapshot.getNormalQuantity());
                    goods.setGoodId(goodSnapshot.getGoodId());
                    SupplyChainGoodRemoteService.changSupplychainGoodByGoodId(goodSnapshot.getGoodId(), goodSnapshot.getNormalQuantity(), 0, getHttpServletRequest());
                }
            }

            addPaymentEstimateRevenue(orderSequenceNumber);
        }else{
            //支付失败

        }
    }

    /**
     * 添加订单状态操作记录
     * @author sqd
     * @date 2019/4/25
     * @param
     * @return void
     */
    public void addStaueLog(Map<String,Object> map){
        logger.info("============进入添加订单状态操作记录============");
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(map);
        if(orderEntrys != null && orderEntrys.size()>0){
            OrderStateLogger orderStateLogger = new OrderStateLogger();
            orderStateLogger.setOrderId(orderEntrys.get(0).getId());
            orderStateLogger.setShippingUserId(orderEntrys.get(0).getShippingUserId());
            orderStateLogger.setOrderType(orderEntrys.get(0).getType());
            orderStateLogger.setBeforeStatus(1);
            orderStateLogger.setStatus(OrderStatus.ORDER_TYPE_PAID.getKey());
            orderStateLogger.setOperatorTime(new Date());
            orderStateLogger.setOperatorUserId((String) map.get("attach"));
            //orderStateLogger.setOperatorName(user.getShowName());
            //添加状态订单操作记录
            dataAccessManager.addOrderStateLogger(orderStateLogger);
            logger.info("============完成添加订单状态操作记录============");
        }
    }

    /**
     * 添加支付账期记录
     * @author sqd
     * @date 2019/4/26
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    public void addPaymentEstimateRevenue(String primarySequenceNumbers) {
        HashMap<String, Object> mapObj = new HashMap<>();
        mapObj.put("primarySequenceNumbers",primarySequenceNumbers);
        List<OrderEntry> orderEntrys = dataAccessManager.getOrderEntrys(mapObj);
        if(orderEntrys.size()>0){
            OrderEntry orderEntry = orderEntrys.get(0);
            //支付成功添加
            SysUser user = getUser();
            PaymentEstimateRevenue paymentEstimateRevenue = new PaymentEstimateRevenue();
            if(user != null){
                paymentEstimateRevenue.setUserId(user.getId());
            }
            paymentEstimateRevenue.setOrderId(orderEntry.getId());
            paymentEstimateRevenue.setShopsName(orderEntry.getReceiptNickName());//商家名称
            paymentEstimateRevenue.setActualIncome(orderEntry.getActualPrice());
            paymentEstimateRevenue.setStatus(3);
            paymentEstimateRevenue.setRequestTime(new Date());
            mapObj.put("primarySequenceNumbers",primarySequenceNumbers);
            try {
                PaymentTransactionLoggerService.changePaymentTransactionLoggerMap(mapObj,getHttpServletRequest());//微信的请求 没有我们的token sign
                PaymentEstimateRevenueService.addPaymentEstimateRevenue(paymentEstimateRevenue,getHttpServletRequest());//微信的请求 没有我们的token sign
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
