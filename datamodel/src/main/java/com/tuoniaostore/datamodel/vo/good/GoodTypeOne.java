package com.tuoniaostore.datamodel.vo.good;

import java.io.Serializable;
import java.util.List;

/**
 * 一二级标题查询结果封装
 * @author oy
 * @date 2019/4/15
 * @param
 * @return
 */
public class GoodTypeOne implements Serializable {

    private static final long serialVersionUID = 1L;

    private String oneLeaveId;
    private String oneLeaveName;
    private String parentId;

    private List<GoodTypeTwo> list;

    public GoodTypeOne() {
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getOneLeaveId() {
        return oneLeaveId;
    }

    public void setOneLeaveId(String oneLeaveId) {
        this.oneLeaveId = oneLeaveId;
    }

    public String getOneLeaveName() {
        return oneLeaveName;
    }

    public void setOneLeaveName(String oneLeaveName) {
        this.oneLeaveName = oneLeaveName;
    }

    public List<GoodTypeTwo> getList() {
        return list;
    }

    public void setList(List<GoodTypeTwo> list) {
        this.list = list;
    }
}
