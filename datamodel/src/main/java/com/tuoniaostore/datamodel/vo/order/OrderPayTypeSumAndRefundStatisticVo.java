package com.tuoniaostore.datamodel.vo.order;

import java.io.Serializable;

/**
 * 功能描述
 * @author sqd
 * @date 2019/6/13
 * @param
 * @return
 */
public class OrderPayTypeSumAndRefundStatisticVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private  String number;
    private  Integer paymentType;
    private  long moneySum;
    private  Integer refundNumber;
    private  long refundMoneySum = 0;

    public Integer getRefundNumber() {
        return refundNumber;
    }

    public void setRefundNumber(Integer refundNumber) {
        this.refundNumber = refundNumber;
    }

    public long getRefundMoneySum() {
        return refundMoneySum;
    }

    public void setRefundMoneySum(long refundMoneySum) {
        this.refundMoneySum = refundMoneySum;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public long getMoneySum() {
        return moneySum;
    }

    public void setMoneySum(long moneySum) {
        this.moneySum = moneySum;
    }
}
