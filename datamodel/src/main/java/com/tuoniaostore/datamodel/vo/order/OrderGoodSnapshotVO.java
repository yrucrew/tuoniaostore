package com.tuoniaostore.datamodel.vo.order;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品销售快照表/大部分字段为冗余字段
 * @author sqd
 * @date 2019/4/17
 * @param
 * @return
 */
public class OrderGoodSnapshotVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String id= DefineRandom.getUUID();
	//对应的订单ID
	private String orderId;
	//当时的商品名称
	private String goodName;
	//当时归属的类型名
	private String goodTypeName;
	//当时单位名称
	private String goodUnitName;
	//商品条码
	private String goodBarcode;
	//当时商品的编码
	private String goodCode;
	//购买的时间
	private Date createTime;
	//正常价格购买的数量 订单数
	private Integer normalQuantity;
	//优惠价格购买的数量
	private Integer discountQuantity;
	//发货中的数量
	private Integer shipmentQuantity;
	//配送中的数量
	private Integer distributionQuantity;
	//送达的数量
	private Integer arriveQuantity;
	//被确认收货的数量
	private Integer receiptQuantity;
	//退货中数量
	private Integer refundQuantity;
	//退货的数量
	private Integer returnQuantity;
	//复核数量
	private Integer reviewQuantity;
	//未收到的数量
	private Integer unReceiptQuantity;
	//正常价格
	private Long normalPrice;
	//优惠价格
	private Long discountPrice;
	//当时的成本价
	private Long costPrice;
	//当时一般的市场价
	private Long marketPrice;
	//应收费用
	private Long totalPrice;
	//退货的费用(实收为应收费用-退货费用)
	private Long returnPrice;

	public String getId() {
		return id;
	}

	public Integer getUnReceiptQuantity() {
		return unReceiptQuantity;
	}

	public void setUnReceiptQuantity(Integer unReceiptQuantity) {
		this.unReceiptQuantity = unReceiptQuantity;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getGoodName() {
		return goodName;
	}

	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}

	public String getGoodTypeName() {
		return goodTypeName;
	}

	public void setGoodTypeName(String goodTypeName) {
		this.goodTypeName = goodTypeName;
	}

	public String getGoodUnitName() {
		return goodUnitName;
	}

	public void setGoodUnitName(String goodUnitName) {
		this.goodUnitName = goodUnitName;
	}

	public String getGoodBarcode() {
		return goodBarcode;
	}

	public void setGoodBarcode(String goodBarcode) {
		this.goodBarcode = goodBarcode;
	}

	public String getGoodCode() {
		return goodCode;
	}

	public void setGoodCode(String goodCode) {
		this.goodCode = goodCode;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getNormalQuantity() {
		return normalQuantity;
	}

	public void setNormalQuantity(Integer normalQuantity) {
		this.normalQuantity = normalQuantity;
	}

	public Integer getDiscountQuantity() {
		return discountQuantity;
	}

	public void setDiscountQuantity(Integer discountQuantity) {
		this.discountQuantity = discountQuantity;
	}

	public Integer getShipmentQuantity() {
		return shipmentQuantity;
	}

	public void setShipmentQuantity(Integer shipmentQuantity) {
		this.shipmentQuantity = shipmentQuantity;
	}

	public Integer getDistributionQuantity() {
		return distributionQuantity;
	}

	public void setDistributionQuantity(Integer distributionQuantity) {
		this.distributionQuantity = distributionQuantity;
	}

	public Integer getArriveQuantity() {
		return arriveQuantity;
	}

	public void setArriveQuantity(Integer arriveQuantity) {
		this.arriveQuantity = arriveQuantity;
	}

	public Integer getReceiptQuantity() {
		return receiptQuantity;
	}

	public void setReceiptQuantity(Integer receiptQuantity) {
		this.receiptQuantity = receiptQuantity;
	}

	public Integer getRefundQuantity() {
		return refundQuantity;
	}

	public void setRefundQuantity(Integer refundQuantity) {
		this.refundQuantity = refundQuantity;
	}

	public Integer getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(Integer returnQuantity) {
		this.returnQuantity = returnQuantity;
	}

	public Integer getReviewQuantity() {
		return reviewQuantity;
	}

	public void setReviewQuantity(Integer reviewQuantity) {
		this.reviewQuantity = reviewQuantity;
	}

	public Long getNormalPrice() {
		return normalPrice;
	}

	public void setNormalPrice(Long normalPrice) {
		this.normalPrice = normalPrice;
	}

	public Long getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Long discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Long getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Long costPrice) {
		this.costPrice = costPrice;
	}

	public Long getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(Long marketPrice) {
		this.marketPrice = marketPrice;
	}

	public Long getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Long getReturnPrice() {
		return returnPrice;
	}

	public void setReturnPrice(Long returnPrice) {
		this.returnPrice = returnPrice;
	}
}
