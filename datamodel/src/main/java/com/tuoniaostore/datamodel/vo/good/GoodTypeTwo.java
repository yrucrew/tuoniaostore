package com.tuoniaostore.datamodel.vo.good;

import java.io.Serializable;

/**
 * 二级标题
 * @author oy
 * @date 2019/4/15
 * @param
 * @return
 */
public class GoodTypeTwo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String twoLeaveId;
    private String twoLeaveName;
    private String icon;

    public GoodTypeTwo() {
    }

    public String getTwoLeaveId() {
        return twoLeaveId;
    }

    public void setTwoLeaveId(String twoLeaveId) {
        this.twoLeaveId = twoLeaveId;
    }

    public String getTwoLeaveName() {
        return twoLeaveName;
    }

    public void setTwoLeaveName(String twoLeaveName) {
        this.twoLeaveName = twoLeaveName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
