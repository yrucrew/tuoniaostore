package com.tuoniaostore.datamodel.vo.supplychain;

import java.io.Serializable;

/** 首页商品推广展示数据
 * @author oy
 * @description
 * @date 2019/4/25
 */
public class SupplychainHomePageGoodDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private String goodTemplateId;//模板id
    private String goodTemplateName;//商品模板名字
    private String goodTemplatePic;//商品模板图片
    private String goodPriceId;//价格id  *
    private String goodPriceName;//价格名称
    private String goodName;//商品名字  = 商品模板名字 + 价格名称
    private String barcodeId;//条码id
    private String barcode;//条码
    private String unit;//单位
    private String unitId;//单位
    private String oneTypeId;//一级分类id
    private String oneType;//一级分类
    private String twoTypeId;//二级分类id
    private String twoType;//二级分类

    //homepage表中的数据
    private String type;//类型 *
    private Integer typeNum;//类型 *
    private Integer sort;//排序   *
    private String areaId;//区域id *
    private String description;//描述
    private String areaName;//区域名称

    private String homePageGoodId;//推荐 精选商品


    public SupplychainHomePageGoodDetail() {
    }

    public void setTypeNum(Integer typeNum) {
        this.typeNum = typeNum;
    }

    public Integer getTypeNum() {
        return typeNum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGoodTemplatePic() {
        return goodTemplatePic;
    }

    public void setGoodTemplatePic(String goodTemplatePic) {
        this.goodTemplatePic = goodTemplatePic;
    }

    public String getHomePageGoodId() {
        return homePageGoodId;
    }

    public void setHomePageGoodId(String homePageGoodId) {
        this.homePageGoodId = homePageGoodId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getGoodTemplateId() {
        return goodTemplateId;
    }

    public void setGoodTemplateId(String goodTemplateId) {
        this.goodTemplateId = goodTemplateId;
    }

    public String getGoodTemplateName() {
        return goodTemplateName;
    }

    public void setGoodTemplateName(String goodTemplateName) {
        this.goodTemplateName = goodTemplateName;
    }

    public String getGoodPriceId() {
        return goodPriceId;
    }

    public void setGoodPriceId(String goodPriceId) {
        this.goodPriceId = goodPriceId;
    }

    public String getGoodPriceName() {
        return goodPriceName;
    }

    public void setGoodPriceName(String goodPriceName) {
        this.goodPriceName = goodPriceName;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getBarcodeId() {
        return barcodeId;
    }

    public void setBarcodeId(String barcodeId) {
        this.barcodeId = barcodeId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getOneTypeId() {
        return oneTypeId;
    }

    public void setOneTypeId(String oneTypeId) {
        this.oneTypeId = oneTypeId;
    }

    public String getOneType() {
        return oneType;
    }

    public void setOneType(String oneType) {
        this.oneType = oneType;
    }

    public String getTwoTypeId() {
        return twoTypeId;
    }

    public void setTwoTypeId(String twoTypeId) {
        this.twoTypeId = twoTypeId;
    }

    public String getTwoType() {
        return twoType;
    }

    public void setTwoType(String twoType) {
        this.twoType = twoType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
}
