package com.tuoniaostore.datamodel.vo.supplychain;

import com.tuoniaostore.datamodel.good.GoodBrand;
import com.tuoniaostore.datamodel.good.GoodType;

import java.io.Serializable;
import java.util.List;

/**
 * @author oy
 * @description homePageGood的一级展示
 * @date 2019/4/27
 */
public class SupplychainHomePageGood implements Serializable {

    private static final long serialVersionUID = 1L;
    private String type;//类型 * 具体值value
    private Integer typeNum;//类型 * 精选 推荐 key
    private Integer sort;//排序   *
    private String areaId;//区域id *
    private String description;//描述
    private String areaName;//区域名称

    //存放详情数据 （推荐商品，精选商品）
    List<SupplychainHomePageGoodDetailList> list;

    //存放详情数据（推荐品牌）
    List<GoodBrand> brandList;

    //存放详情数据（推荐分类）
    List<GoodType> typeList;

    public SupplychainHomePageGood() {
    }

    public List<GoodType> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<GoodType> typeList) {
        this.typeList = typeList;
    }

    public List<GoodBrand> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<GoodBrand> brandList) {
        this.brandList = brandList;
    }

    public List<SupplychainHomePageGoodDetailList> getList() {
        return list;
    }

    public void setList(List<SupplychainHomePageGoodDetailList> list) {
        this.list = list;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getTypeNum() {
        return typeNum;
    }

    public void setTypeNum(Integer typeNum) {
        this.typeNum = typeNum;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
}
