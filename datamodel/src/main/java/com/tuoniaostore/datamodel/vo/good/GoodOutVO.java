package com.tuoniaostore.datamodel.vo.good;
/**
 * @author sqd
 * @date 2019/6/2
 * @param
 * @return
 */
public class GoodOutVO {

    private   String  goodName;
    private   Integer goodNumber;
    private   String  remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public Integer getGoodNumber() {
        return goodNumber;
    }

    public void setGoodNumber(Integer goodNumber) {
        this.goodNumber = goodNumber;
    }
}
