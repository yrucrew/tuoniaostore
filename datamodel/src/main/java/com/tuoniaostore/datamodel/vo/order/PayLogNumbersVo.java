package com.tuoniaostore.datamodel.vo.order;

import java.io.Serializable;

/**
 * @author sqd
 * @description
 * @date 2019/5/9
 */
public class PayLogNumbersVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private  String number;
    private  Integer paymentType;
    private  long moneySum;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public long getMoneySum() {
        return moneySum;
    }

    public void setMoneySum(long moneySum) {
        this.moneySum = moneySum;
    }
}
