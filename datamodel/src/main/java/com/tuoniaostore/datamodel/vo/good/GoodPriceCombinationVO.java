package com.tuoniaostore.datamodel.vo.good;

import java.io.Serializable;

/**
 * @author oy
 * @description 用来存放价格标签 单位 的组合数据
 * @date 2019/4/13 update
 */
public class GoodPriceCombinationVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String priceId;//价格id
    private String priceTitle;//价格标签
    private String priceIcon;//价格标签图标
    private Integer priceNum;//价格标签数量
    private Integer priceSort;//价格标签排序
    private String priceRemark;//价格标签备注
    private String createUser;//价格标签创建人id
    private String createUserName;//价格标签创建人id

    private String unitTitleId;//单位标签id
    private String unitTitle;//单位标签
    private String barcodeId;//条码
    private String barcode;//条码
    private String brandId;//品牌id
    private String brandLogo;//品牌id
    private String brandName;//品牌名称

    private long sellPrice;//销售价
    private long costPrice;//成本价
    private Long salePrice;//建议售价
    private Long colleaguePrice;//同行价
    private Long memberPrice;//会员价
    private Long discountPrice;//折扣价
    private Long marketPrice;//市场价

    private String goodTemplateId;//商品模板id
    private String goodName;//商品名称
    private String goodBanner;//商品banner

    private Integer shelfLife;//商品保质期
    private String imageUrl;//图片地址
    private String typeNameOneId;//一级标题id
    private String typeNameOneName;//一级标题
    private String typeNameTwoId;//二级标题id
    private String typeNameTwoName;//二级标题

    private String typeIconOne;//一级分类icon
    private String typeImageOne;//一级分类image
    private String typeIconTwo;//二级分类icon
    private String typeImageTwo;//二级分类image

    private Integer priceStatus; //价格标签状态

    private Integer goodStatus;//商品模板状态

    private int checked;    //是否已选 0未选 1已选


    public GoodPriceCombinationVO() {
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getPriceIcon() {
        return priceIcon;
    }

    public void setPriceIcon(String priceIcon) {
        this.priceIcon = priceIcon;
    }

    public Integer getPriceNum() {
        return priceNum;
    }

    public void setPriceNum(Integer priceNum) {
        this.priceNum = priceNum;
    }

    public Integer getPriceSort() {
        return priceSort;
    }

    public void setPriceSort(Integer priceSort) {
        this.priceSort = priceSort;
    }

    public String getPriceRemark() {
        return priceRemark;
    }

    public void setPriceRemark(String priceRemark) {
        this.priceRemark = priceRemark;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getGoodBanner() {
        return goodBanner;
    }

    public void setGoodBanner(String goodBanner) {
        this.goodBanner = goodBanner;
    }

    public Integer getPriceStatus() {
        return priceStatus;
    }

    public void setPriceStatus(Integer priceStatus) {
        this.priceStatus = priceStatus;
    }

    public Integer getGoodStatus() {
        return goodStatus;
    }

    public void setGoodStatus(Integer goodStatus) {
        this.goodStatus = goodStatus;
    }

    public Integer getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(Integer shelfLife) {
        this.shelfLife = shelfLife;
    }

    public Long getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Long marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Long getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Long discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Long getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Long salePrice) {
        this.salePrice = salePrice;
    }

    public Long getColleaguePrice() {
        return colleaguePrice;
    }

    public void setColleaguePrice(Long colleaguePrice) {
        this.colleaguePrice = colleaguePrice;
    }

    public Long getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(Long memberPrice) {
        this.memberPrice = memberPrice;
    }

    public String getTypeIconOne() {
        return typeIconOne;
    }

    public void setTypeIconOne(String typeIconOne) {
        this.typeIconOne = typeIconOne;
    }

    public String getTypeImageOne() {
        return typeImageOne;
    }

    public void setTypeImageOne(String typeImageOne) {
        this.typeImageOne = typeImageOne;
    }

    public String getTypeIconTwo() {
        return typeIconTwo;
    }

    public void setTypeIconTwo(String typeIconTwo) {
        this.typeIconTwo = typeIconTwo;
    }

    public String getTypeImageTwo() {
        return typeImageTwo;
    }

    public void setTypeImageTwo(String typeImageTwo) {
        this.typeImageTwo = typeImageTwo;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public long getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(long costPrice) {
        this.costPrice = costPrice;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

    public String getUnitTitleId() {
        return unitTitleId;
    }

    public void setUnitTitleId(String unitTitleId) {
        this.unitTitleId = unitTitleId;
    }

    public String getBarcodeId() {
        return barcodeId;
    }

    public void setBarcodeId(String barcodeId) {
        this.barcodeId = barcodeId;
    }

    public String getGoodTemplateId() {
        return goodTemplateId;
    }

    public void setGoodTemplateId(String goodTemplateId) {
        this.goodTemplateId = goodTemplateId;
    }

    public String getTypeNameOneId() {
        return typeNameOneId;
    }

    public void setTypeNameOneId(String typeNameOneId) {
        this.typeNameOneId = typeNameOneId;
    }

    public String getTypeNameOneName() {
        return typeNameOneName;
    }

    public void setTypeNameOneName(String typeNameOneName) {
        this.typeNameOneName = typeNameOneName;
    }

    public String getTypeNameTwoId() {
        return typeNameTwoId;
    }

    public void setTypeNameTwoId(String typeNameTwoId) {
        this.typeNameTwoId = typeNameTwoId;
    }

    public String getTypeNameTwoName() {
        return typeNameTwoName;
    }

    public void setTypeNameTwoName(String typeNameTwoName) {
        this.typeNameTwoName = typeNameTwoName;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getPriceTitle() {
            return priceTitle;
    }

    public void setPriceTitle(String priceTitle) {
        this.priceTitle = priceTitle;
    }

    public String getUnitTitle() {
        return unitTitle;
    }

    public void setUnitTitle(String unitTitle) {
        this.unitTitle = unitTitle;
    }

    public long getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(long sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
