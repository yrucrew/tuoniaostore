package com.tuoniaostore.datamodel.vo.good;
/**
 * 存放下单商品id和数量
 * @author sqd
 * @date 2019/5/6
 * @param
 * @return
 */
public class GoodIdAndGoodNumberVO{

    private String goodId;//商品ID
    private Integer number;//商品数量
    private long price ;//商品价格

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

}
