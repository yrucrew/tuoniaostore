package com.tuoniaostore.datamodel.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tuoniaostore.datamodel.DefineRandom;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 商品销售快照表
 大部分字段为冗余字段
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public class OrderGoodSnapshotFormatDate implements Serializable {
	private static final long serialVersionUID = 1L;

	//
	private String id= DefineRandom.getUUID();
	//对应的订单ID
	private String orderId;
	//购买的用户ID或其他标志(如第三方用户ID)
	private String userMark;
	//相关的商品ID
	private String goodId;
	//商品模版ID
	private String goodTemplateId;
	//当时的商品名称
	private String goodName;
	//当时的归属类型ID
	private String goodTypeId;
	//当时归属的类型名
	private String goodTypeName;
	//当时的单位ID
	private String goodUnitId;
	//当时单位名称
	private String goodUnitName;
	//商品条码
	private String goodBarcode;
	//当时商品的编码
	private String goodCode;
	//商品批次
	private Integer goodBatches;
	//当时的介绍页面
	private String introductionPage;
	//购买的时间
	private Date createTime;
	//正常价格购买的数量 订单数 ***
	private Integer normalQuantity;
	//优惠价格购买的数量
	private Integer discountQuantity;
	//发货中的数量
	private Integer shipmentQuantity;
	//配送中的数量
	private Integer distributionQuantity;
	//送达的数量
	private Integer arriveQuantity;
	//被确认收货的数量  ***
	private Integer receiptQuantity;
	//退货中数量
	private Integer refundQuantity;
	//退货的数量
	private Integer returnQuantity;
	//复核数量
	private Integer reviewQuantity;
	//正常价格
	private Long normalPrice;
	//优惠价格
	private Long discountPrice;
	//当时的成本价
	private Long costPrice;
	//当时一般的市场价
	private Long marketPrice;
	//应收费用
	private Long totalPrice;
	//退货的费用(实收为应收费用-退货费用)
	private Long returnPrice;
	//区分仓库的订单号
	private String innerOrderId;
	//第三方送货的数量
	private Integer thirdPartyDeliveryQuantity;
	//第三方排线的数量
	private Integer thirdPartyAcceptQuantity;
	//是否第三方商品，否为0
	private Integer isThirdParty;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：对应的订单ID
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：对应的订单ID
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * 设置：购买的用户ID或其他标志(如第三方用户ID)
	 */
	public void setUserMark(String userMark) {
		this.userMark = userMark;
	}
	/**
	 * 获取：购买的用户ID或其他标志(如第三方用户ID)
	 */
	public String getUserMark() {
		return userMark;
	}
	/**
	 * 设置：相关的商品ID
	 */
	public void setGoodId(String goodId) {
		this.goodId = goodId;
	}
	/**
	 * 获取：相关的商品ID
	 */
	public String getGoodId() {
		return goodId;
	}
	/**
	 * 设置：商品模版ID
	 */
	public void setGoodTemplateId(String goodTemplateId) {
		this.goodTemplateId = goodTemplateId;
	}
	/**
	 * 获取：商品模版ID
	 */
	public String getGoodTemplateId() {
		return goodTemplateId;
	}
	/**
	 * 设置：当时的商品名称
	 */
	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}
	/**
	 * 获取：当时的商品名称
	 */
	public String getGoodName() {
		return goodName;
	}
	/**
	 * 设置：当时的归属类型ID
	 */
	public void setGoodTypeId(String goodTypeId) {
		this.goodTypeId = goodTypeId;
	}
	/**
	 * 获取：当时的归属类型ID
	 */
	public String getGoodTypeId() {
		return goodTypeId;
	}
	/**
	 * 设置：当时归属的类型名
	 */
	public void setGoodTypeName(String goodTypeName) {
		this.goodTypeName = goodTypeName;
	}
	/**
	 * 获取：当时归属的类型名
	 */
	public String getGoodTypeName() {
		return goodTypeName;
	}
	/**
	 * 设置：当时的单位ID
	 */
	public void setGoodUnitId(String goodUnitId) {
		this.goodUnitId = goodUnitId;
	}
	/**
	 * 获取：当时的单位ID
	 */
	public String getGoodUnitId() {
		return goodUnitId;
	}
	/**
	 * 设置：当时单位名称
	 */
	public void setGoodUnitName(String goodUnitName) {
		this.goodUnitName = goodUnitName;
	}
	/**
	 * 获取：当时单位名称
	 */
	public String getGoodUnitName() {
		return goodUnitName;
	}
	/**
	 * 设置：商品条码
	 */
	public void setGoodBarcode(String goodBarcode) {
		this.goodBarcode = goodBarcode;
	}
	/**
	 * 获取：商品条码
	 */
	public String getGoodBarcode() {
		return goodBarcode;
	}
	/**
	 * 设置：当时商品的编码
	 */
	public void setGoodCode(String goodCode) {
		this.goodCode = goodCode;
	}
	/**
	 * 获取：当时商品的编码
	 */
	public String getGoodCode() {
		return goodCode;
	}
	/**
	 * 设置：商品批次
	 */
	public void setGoodBatches(Integer goodBatches) {
		this.goodBatches = goodBatches;
	}
	/**
	 * 获取：商品批次
	 */
	public Integer getGoodBatches() {
		return goodBatches;
	}
	/**
	 * 设置：当时的介绍页面
	 */
	public void setIntroductionPage(String introductionPage) {
		this.introductionPage = introductionPage;
	}
	/**
	 * 获取：当时的介绍页面
	 */
	public String getIntroductionPage() {
		return introductionPage;
	}
	/**
	 * 设置：购买的时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：购买的时间
	 */
	@JsonFormat(pattern="yyyy年MM月dd日 hh:mm:ss",timezone = "GMT+8")
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：正常价格购买的数量
	 */
	public void setNormalQuantity(Integer normalQuantity) {
		this.normalQuantity = normalQuantity;
	}
	/**
	 * 获取：正常价格购买的数量
	 */
	public Integer getNormalQuantity() {
		return normalQuantity;
	}
	/**
	 * 设置：优惠价格购买的数量
	 */
	public void setDiscountQuantity(Integer discountQuantity) {
		this.discountQuantity = discountQuantity;
	}
	/**
	 * 获取：优惠价格购买的数量
	 */
	public Integer getDiscountQuantity() {
		return discountQuantity;
	}
	/**
	 * 设置：发货中的数量
	 */
	public void setShipmentQuantity(Integer shipmentQuantity) {
		this.shipmentQuantity = shipmentQuantity;
	}
	/**
	 * 获取：发货中的数量
	 */
	public Integer getShipmentQuantity() {
		return shipmentQuantity;
	}
	/**
	 * 设置：配送中的数量
	 */
	public void setDistributionQuantity(Integer distributionQuantity) {
		this.distributionQuantity = distributionQuantity;
	}
	/**
	 * 获取：配送中的数量
	 */
	public Integer getDistributionQuantity() {
		return distributionQuantity;
	}
	/**
	 * 设置：送达的数量
	 */
	public void setArriveQuantity(Integer arriveQuantity) {
		this.arriveQuantity = arriveQuantity;
	}
	/**
	 * 获取：送达的数量
	 */
	public Integer getArriveQuantity() {
		return arriveQuantity;
	}
	/**
	 * 设置：被确认收货的数量
	 */
	public void setReceiptQuantity(Integer receiptQuantity) {
		this.receiptQuantity = receiptQuantity;
	}
	/**
	 * 获取：被确认收货的数量
	 */
	public Integer getReceiptQuantity() {
		return receiptQuantity;
	}
	/**
	 * 设置：退货中数量
	 */
	public void setRefundQuantity(Integer refundQuantity) {
		this.refundQuantity = refundQuantity;
	}
	/**
	 * 获取：退货中数量
	 */
	public Integer getRefundQuantity() {
		return refundQuantity;
	}
	/**
	 * 设置：退货的数量
	 */
	public void setReturnQuantity(Integer returnQuantity) {
		this.returnQuantity = returnQuantity;
	}
	/**
	 * 获取：退货的数量
	 */
	public Integer getReturnQuantity() {
		return returnQuantity;
	}
	/**
	 * 设置：复核数量
	 */
	public void setReviewQuantity(Integer reviewQuantity) {
		this.reviewQuantity = reviewQuantity;
	}
	/**
	 * 获取：复核数量
	 */
	public Integer getReviewQuantity() {
		return reviewQuantity;
	}
	/**
	 * 设置：正常价格
	 */
	public void setNormalPrice(Long normalPrice) {
		this.normalPrice = normalPrice;
	}
	/**
	 * 获取：正常价格
	 */
	public Long getNormalPrice() {
		return normalPrice;
	}
	/**
	 * 设置：优惠价格
	 */
	public void setDiscountPrice(Long discountPrice) {
		this.discountPrice = discountPrice;
	}
	/**
	 * 获取：优惠价格
	 */
	public Long getDiscountPrice() {
		return discountPrice;
	}
	/**
	 * 设置：当时的成本价
	 */
	public void setCostPrice(Long costPrice) {
		this.costPrice = costPrice;
	}
	/**
	 * 获取：当时的成本价
	 */
	public Long getCostPrice() {
		return costPrice;
	}
	/**
	 * 设置：当时一般的市场价
	 */
	public void setMarketPrice(Long marketPrice) {
		this.marketPrice = marketPrice;
	}
	/**
	 * 获取：当时一般的市场价
	 */
	public Long getMarketPrice() {
		return marketPrice;
	}
	/**
	 * 设置：应收费用
	 */
	public void setTotalPrice(Long totalPrice) {
		this.totalPrice = totalPrice;
	}
	/**
	 * 获取：应收费用
	 */
	public Long getTotalPrice() {
		return totalPrice;
	}
	/**
	 * 设置：退货的费用(实收为应收费用-退货费用)
	 */
	public void setReturnPrice(Long returnPrice) {
		this.returnPrice = returnPrice;
	}
	/**
	 * 获取：退货的费用(实收为应收费用-退货费用)
	 */
	public Long getReturnPrice() {
		return returnPrice;
	}
	/**
	 * 设置：区分仓库的订单号
	 */
	public void setInnerOrderId(String innerOrderId) {
		this.innerOrderId = innerOrderId;
	}
	/**
	 * 获取：区分仓库的订单号
	 */
	public String getInnerOrderId() {
		return innerOrderId;
	}
	/**
	 * 设置：第三方送货的数量
	 */
	public void setThirdPartyDeliveryQuantity(Integer thirdPartyDeliveryQuantity) {
		this.thirdPartyDeliveryQuantity = thirdPartyDeliveryQuantity;
	}
	/**
	 * 获取：第三方送货的数量
	 */
	public Integer getThirdPartyDeliveryQuantity() {
		return thirdPartyDeliveryQuantity;
	}
	/**
	 * 设置：第三方排线的数量
	 */
	public void setThirdPartyAcceptQuantity(Integer thirdPartyAcceptQuantity) {
		this.thirdPartyAcceptQuantity = thirdPartyAcceptQuantity;
	}
	/**
	 * 获取：第三方排线的数量
	 */
	public Integer getThirdPartyAcceptQuantity() {
		return thirdPartyAcceptQuantity;
	}
	/**
	 * 设置：是否第三方商品，否为0
	 */
	public void setIsThirdParty(Integer isThirdParty) {
		this.isThirdParty = isThirdParty;
	}
	/**
	 * 获取：是否第三方商品，否为0
	 */
	public Integer getIsThirdParty() {
		return isThirdParty;
	}
}
