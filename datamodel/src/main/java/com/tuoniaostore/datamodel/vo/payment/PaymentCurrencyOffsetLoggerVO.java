package com.tuoniaostore.datamodel.vo.payment;

import java.io.Serializable;

/**
 * @author oy
 * @description
 * @date 2019/5/1
 */
public class PaymentCurrencyOffsetLoggerVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userId;
    private long totalMoney;

    public PaymentCurrencyOffsetLoggerVO() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(long totalMoney) {
        this.totalMoney = totalMoney;
    }
}
