package com.tuoniaostore.datamodel.vo.order;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OrderEntryPosVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    //序列号(对应于购物车)
    private String sequenceNumber;
    //订单的序列号(用于展示)
    private String orderSequenceNumber;
    //下游商户服务号ID
    private String partnerId;
    //下单用户ID 匿名时可为0或空
    private String partnerUserId;
    //订单类型  //订单类型 1-供应链销售订单、2-供应链调拨订单、8-供应链采购订单、16-超市POS机订单（包括超市扫码订单）、32、外卖配送订单 64增值服务订单、
    private Integer type;
    //订单状态 不要占用0  1-创建订单（未确认）、2-已取消、8-已支付、16-已接单、32-发货中、64-配送中、128-已送达、256-确认收货/已完成、260-***确认收货/已完成、512-已收款、1024-分批配送、2048-拒绝订单。
    private Integer status;
    //发货状态：0-未发货、32-发货中、64-配送中、128-已送达
    private Integer deliverStatus;
    //退款状态 0表示未退货 同时可用于记录是否已打款状态
    private Integer refundStatus;
    //支付类型 参考逻辑设定 -1未支付
    private Integer paymentType;
    //交易类型，微信系：JSAPI、NATIVE、APP等
    private String transType;
    //用户来源
    private Integer userSource;
    //当天排号 相对于发货方
    private Integer daySortNumber;
    //推送类型，0表示不推送 1时表示下单时推送 2时表示支付后推送
    private Integer pushType;
    //实际销售的用户ID（销售仓）
    private String saleUserId;
    //发货用户ID
    private String shippingUserId;
    //发货用户昵称
    private String shippingNickName;
    //发货省份
    private String shippingProvince;
    //发货城市
    private String shippingCity;
    //发货区域
    private String shippingDistrict;
    //发货详细地址(尽量不要包含省市区)
    private String shippingAddress;
    //latitude,longitude
    private String shippingLocation;
    //发货人联系号码
    private String shippingPhone;
    //收货人用户ID
    private String receiptUserId;
    //收货人昵称
    private String receiptNickName;
    //收货省份
    private String receiptProvince;
    //收货城市
    private String receiptCity;
    //收货区域
    private String receiptDistrict;
    //收货具体地址(尽量不要包含省市区)
    private String receiptAddress;
    //收货人电话
    private String receiptPhone;
    //latitude,longitude
    private String receiptLocation;
    //配送员用户ID
    private String courierUserId;
    //配送员昵称
    private String courierNickName;
    //配送员联系号码
    private String courierPhone;
    //订单总距离
    private Long totalDistance;
    //下单时所在地理位置
    private String currentLocation;
    //是否需要配送员代收费用 0表示不需要
    private Integer collectingFees;
    //订单描述
    private String remark;
    //实收费用
    private Long actualPrice;
    //订单总费用
    private Long totalPrice;
    //优惠的费用
    private Long discountPrice;
    //配送费用
    private Long distributionFee;
    //价格记录 如优惠的政策
    private String priceLogger;
    //税率(万分比)
    private Long taxRate;
    //取消的理由
    private String cancelLogger;
    //订单的生成时间
    private Date createTime;
    //支付时间
    private Date paymentTime;
    //确认订单时间
    private Date confirmTime;
    //商品或支付单简要描述
    private String body;
    //商品名称明细列表(仅记录体系外的数据)
    private String detail;
    //信用付费用
    private String creditFee;
    //是否使用第三方物流 0否 1是
    private Integer thirdPartyDelivery;
    //会员价(分)
    private Long membershipPrice;
    //第三方物流的状态 1-待推,2-全部推完,3-冲红
    private Integer thirdPartyDeliveryStatus;
    //是否为加盟店 1为是
    private Integer isJoin;
    //是否为第三方 0为平台 不等于0为第三方
    private Integer isThirdParty;
    //退款操作人用户id
    private String returnOperatorUserId;
    // 退款时间
    private Date refundTime;
    // 退款方式
    private Integer refundType;
    // 退款备注
    private String refundRemark;

    // 商品快照列表
    private List<OrderGoodSnapshotVO> orderGoodSnapshotVOList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getOrderSequenceNumber() {
        return orderSequenceNumber;
    }

    public void setOrderSequenceNumber(String orderSequenceNumber) {
        this.orderSequenceNumber = orderSequenceNumber;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerUserId() {
        return partnerUserId;
    }

    public void setPartnerUserId(String partnerUserId) {
        this.partnerUserId = partnerUserId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDeliverStatus() {
        return deliverStatus;
    }

    public void setDeliverStatus(Integer deliverStatus) {
        this.deliverStatus = deliverStatus;
    }

    public Integer getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Integer refundStatus) {
        this.refundStatus = refundStatus;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public Integer getUserSource() {
        return userSource;
    }

    public void setUserSource(Integer userSource) {
        this.userSource = userSource;
    }

    public Integer getDaySortNumber() {
        return daySortNumber;
    }

    public void setDaySortNumber(Integer daySortNumber) {
        this.daySortNumber = daySortNumber;
    }

    public Integer getPushType() {
        return pushType;
    }

    public void setPushType(Integer pushType) {
        this.pushType = pushType;
    }

    public String getSaleUserId() {
        return saleUserId;
    }

    public void setSaleUserId(String saleUserId) {
        this.saleUserId = saleUserId;
    }

    public String getShippingUserId() {
        return shippingUserId;
    }

    public void setShippingUserId(String shippingUserId) {
        this.shippingUserId = shippingUserId;
    }

    public String getShippingNickName() {
        return shippingNickName;
    }

    public void setShippingNickName(String shippingNickName) {
        this.shippingNickName = shippingNickName;
    }

    public String getShippingProvince() {
        return shippingProvince;
    }

    public void setShippingProvince(String shippingProvince) {
        this.shippingProvince = shippingProvince;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingDistrict() {
        return shippingDistrict;
    }

    public void setShippingDistrict(String shippingDistrict) {
        this.shippingDistrict = shippingDistrict;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingLocation() {
        return shippingLocation;
    }

    public void setShippingLocation(String shippingLocation) {
        this.shippingLocation = shippingLocation;
    }

    public String getShippingPhone() {
        return shippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        this.shippingPhone = shippingPhone;
    }

    public String getReceiptUserId() {
        return receiptUserId;
    }

    public void setReceiptUserId(String receiptUserId) {
        this.receiptUserId = receiptUserId;
    }

    public String getReceiptNickName() {
        return receiptNickName;
    }

    public void setReceiptNickName(String receiptNickName) {
        this.receiptNickName = receiptNickName;
    }

    public String getReceiptProvince() {
        return receiptProvince;
    }

    public void setReceiptProvince(String receiptProvince) {
        this.receiptProvince = receiptProvince;
    }

    public String getReceiptCity() {
        return receiptCity;
    }

    public void setReceiptCity(String receiptCity) {
        this.receiptCity = receiptCity;
    }

    public String getReceiptDistrict() {
        return receiptDistrict;
    }

    public void setReceiptDistrict(String receiptDistrict) {
        this.receiptDistrict = receiptDistrict;
    }

    public String getReceiptAddress() {
        return receiptAddress;
    }

    public void setReceiptAddress(String receiptAddress) {
        this.receiptAddress = receiptAddress;
    }

    public String getReceiptPhone() {
        return receiptPhone;
    }

    public void setReceiptPhone(String receiptPhone) {
        this.receiptPhone = receiptPhone;
    }

    public String getReceiptLocation() {
        return receiptLocation;
    }

    public void setReceiptLocation(String receiptLocation) {
        this.receiptLocation = receiptLocation;
    }

    public String getCourierUserId() {
        return courierUserId;
    }

    public void setCourierUserId(String courierUserId) {
        this.courierUserId = courierUserId;
    }

    public String getCourierNickName() {
        return courierNickName;
    }

    public void setCourierNickName(String courierNickName) {
        this.courierNickName = courierNickName;
    }

    public String getCourierPhone() {
        return courierPhone;
    }

    public void setCourierPhone(String courierPhone) {
        this.courierPhone = courierPhone;
    }

    public Long getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(Long totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public Integer getCollectingFees() {
        return collectingFees;
    }

    public void setCollectingFees(Integer collectingFees) {
        this.collectingFees = collectingFees;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Long actualPrice) {
        this.actualPrice = actualPrice;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Long getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Long discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Long getDistributionFee() {
        return distributionFee;
    }

    public void setDistributionFee(Long distributionFee) {
        this.distributionFee = distributionFee;
    }

    public String getPriceLogger() {
        return priceLogger;
    }

    public void setPriceLogger(String priceLogger) {
        this.priceLogger = priceLogger;
    }

    public Long getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Long taxRate) {
        this.taxRate = taxRate;
    }

    public String getCancelLogger() {
        return cancelLogger;
    }

    public void setCancelLogger(String cancelLogger) {
        this.cancelLogger = cancelLogger;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    public Date getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(Date confirmTime) {
        this.confirmTime = confirmTime;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getCreditFee() {
        return creditFee;
    }

    public void setCreditFee(String creditFee) {
        this.creditFee = creditFee;
    }

    public Integer getThirdPartyDelivery() {
        return thirdPartyDelivery;
    }

    public void setThirdPartyDelivery(Integer thirdPartyDelivery) {
        this.thirdPartyDelivery = thirdPartyDelivery;
    }

    public Long getMembershipPrice() {
        return membershipPrice;
    }

    public void setMembershipPrice(Long membershipPrice) {
        this.membershipPrice = membershipPrice;
    }

    public Integer getThirdPartyDeliveryStatus() {
        return thirdPartyDeliveryStatus;
    }

    public void setThirdPartyDeliveryStatus(Integer thirdPartyDeliveryStatus) {
        this.thirdPartyDeliveryStatus = thirdPartyDeliveryStatus;
    }

    public Integer getIsJoin() {
        return isJoin;
    }

    public void setIsJoin(Integer isJoin) {
        this.isJoin = isJoin;
    }

    public Integer getIsThirdParty() {
        return isThirdParty;
    }

    public void setIsThirdParty(Integer isThirdParty) {
        this.isThirdParty = isThirdParty;
    }

    public String getReturnOperatorUserId() {
        return returnOperatorUserId;
    }

    public void setReturnOperatorUserId(String returnOperatorUserId) {
        this.returnOperatorUserId = returnOperatorUserId;
    }

    public Date getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(Date refundTime) {
        this.refundTime = refundTime;
    }

    public Integer getRefundType() {
        return refundType;
    }

    public void setRefundType(Integer refundType) {
        this.refundType = refundType;
    }

    public String getRefundRemark() {
        return refundRemark;
    }

    public void setRefundRemark(String refundRemark) {
        this.refundRemark = refundRemark;
    }

    public List<OrderGoodSnapshotVO> getOrderGoodSnapshotVOList() {
        return orderGoodSnapshotVOList;
    }

    public void setOrderGoodSnapshotVOList(List<OrderGoodSnapshotVO> orderGoodSnapshotVOList) {
        this.orderGoodSnapshotVOList = orderGoodSnapshotVOList;
    }
}
