package com.tuoniaostore.datamodel.vo.order;

import com.tuoniaostore.datamodel.DefineRandom;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 功能描述
 * @author sqd
 * @date 2019/5/16
 * @param
 * @return
 */
public class OrderEntryPos implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//序列号(对应于购物车)
	private String sequenceNumber;
	//订单的序列号(用于展示)
	private String orderSequenceNumber;
	//下游商户服务号ID
	private String partnerId;
	//下单用户ID 匿名时可为0或空
	private String partnerUserId;
	//订单类型  //订单类型 1-供应链销售订单、2-供应链调拨订单、8-供应链采购订单、16-超市POS机订单（包括超市扫码订单）、32、外卖配送订单 64增值服务订单、
	private Integer type;
	//订单状态 不要占用0  1-创建订单（未确认）、2-已取消、8-已支付、16-已接单、32-发货中、64-配送中、128-已送达、256-确认收货/已完成、260-***确认收货/已完成、512-已收款、1024-分批配送、2048-拒绝订单。
	private Integer status;
	//发货状态：0-未发货、32-发货中、64-配送中、128-已送达
	private Integer deliverStatus;
	//退款状态 0表示未退货 同时可用于记录是否已打款状态
	private Integer refundStatus;
	//支付类型 参考逻辑设定 -1未支付
	private Integer paymentType;
	//交易类型，微信系：JSAPI、NATIVE、APP等
	private String transType;
	//用户来源
	private Integer userSource;
	//当天排号 相对于发货方
	private Integer daySortNumber;
	//推送类型，0表示不推送 1时表示下单时推送 2时表示支付后推送
	private Integer pushType;
	//实际销售的用户ID（销售仓）
	private String saleUserId;
	//发货用户ID
	private String shippingUserId;
	//发货用户昵称
	private String shippingNickName;
	//发货省份
	private String shippingProvince;
	//发货城市
	private String shippingCity;
	//发货区域
	private String shippingDistrict;
	//发货详细地址(尽量不要包含省市区)
	private String shippingAddress;
	//latitude,longitude
	private String shippingLocation;
	//发货人联系号码
	private String shippingPhone;
	//收货人用户ID
	private String receiptUserId;
	//收货人昵称
	private String receiptNickName;
	//收货省份
	private String receiptProvince;
	//收货城市
	private String receiptCity;
	//收货区域
	private String receiptDistrict;
	//收货具体地址(尽量不要包含省市区)
	private String receiptAddress;
	//收货人电话
	private String receiptPhone;
	//latitude,longitude
	private String receiptLocation;
	//配送员用户ID
	private String courierUserId;
	//配送员昵称
	private String courierNickName;
	//配送员联系号码
	private String courierPhone;
	//订单总距离
	private Long totalDistance;
	//下单时所在地理位置
	private String currentLocation;
	//是否需要配送员代收费用 0表示不需要
	private Integer collectingFees;
	//订单描述
	private String remark;
	//实收费用
	private Long actualPrice;
	//找零
	private Long findPrice;
	//订单总费用
	private Long totalPrice;
	//优惠的费用
	private Long discountPrice;
	//配送费用
	private Long distributionFee;
	//价格记录 如优惠的政策
	private String priceLogger;
	//税率(万分比)
	private Long taxRate;
	//取消的理由
	private String cancelLogger;
	//订单的生成时间
	private Date createTime;
	//支付时间
	private Date paymentTime;
	//确认订单时间
	private Date confirmTime;
	//商品或支付单简要描述
	private String body;
	//商品名称明细列表(仅记录体系外的数据)
	private String detail;
	//信用付费用
	private String creditFee;
	//是否使用第三方物流 0否 1是
	private Integer thirdPartyDelivery;
	//会员价(分)
	private Long membershipPrice;
	//第三方物流的状态 1-待推,2-全部推完,3-冲红
	private Integer thirdPartyDeliveryStatus;
	//是否为加盟店 1为是
	private Integer isJoin;
	//是否为第三方 0为平台 不等于0为第三方
	private Integer isThirdParty;
	//退款操作人用户id
	private String returnOperatorUserId;
	// 退款时间
	private Date refundTime   ;
	// 退款方式
	private Integer refundType;
	// 退款备注
	private String refundRemark;
	//
	private  String returnOperatorUser;
	private  Integer returnGoodNumber;
	private  long returnPrice;
	private  Integer goodNumber;

	public void setCourierUserId(String courierUserId) {
		this.courierUserId = courierUserId;
	}

	public Long getFindPrice() {
		return findPrice;
	}

	public void setFindPrice(Long findPrice) {
		this.findPrice = findPrice;
	}

	public String getReturnOperatorUser() {
		return returnOperatorUser;
	}

	public void setReturnOperatorUser(String returnOperatorUser) {
		this.returnOperatorUser = returnOperatorUser;
	}

	public Integer getReturnGoodNumber() {
		return returnGoodNumber;
	}

	public void setReturnGoodNumber(Integer returnGoodNumber) {
		this.returnGoodNumber = returnGoodNumber;
	}

	public long getReturnPrice() {
		return returnPrice;
	}

	public void setReturnPrice(long returnPrice) {
		this.returnPrice = returnPrice;
	}

	public Integer getGoodNumber() {
		return goodNumber;
	}

	public void setGoodNumber(Integer goodNumber) {
		this.goodNumber = goodNumber;
	}

	public Date getRefundTime() {
		return refundTime;
	}

	public void setRefundTime(Date refundTime) {
		this.refundTime = refundTime;
	}

	public Integer getRefundType() {
		return refundType;
	}

	public void setRefundType(Integer refundType) {
		this.refundType = refundType;
	}

	public String getRefundRemark() {
		return refundRemark;
	}

	public void setRefundRemark(String refundRemark) {
		this.refundRemark = refundRemark;
	}

	private List<OrderGoodSnapshot> orderGoodSnapshots;

	public String getReturnOperatorUserId() {
		return returnOperatorUserId;
	}

	public void setReturnOperatorUserId(String returnOperatorUserId) {
		this.returnOperatorUserId = returnOperatorUserId;
	}

	//下游用户名称  《《临时字段不与数据库交互》》
	@Transient
	private String partnerUserName;


	public List<OrderGoodSnapshot> getOrderGoodSnapshots() {
		return orderGoodSnapshots;
	}

	public void setOrderGoodSnapshots(List<OrderGoodSnapshot> orderGoodSnapshots) {
		this.orderGoodSnapshots = orderGoodSnapshots;
	}

	public String getPartnerUserName() {
		return partnerUserName;
	}

	public void setPartnerUserName(String partnerUserName) {
		this.partnerUserName = partnerUserName;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：序列号(对应于购物车)
	 */
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	/**
	 * 获取：序列号(对应于购物车)
	 */
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	/**
	 * 设置：订单的序列号(用于展示)
	 */
	public void setOrderSequenceNumber(String orderSequenceNumber) {
		this.orderSequenceNumber = orderSequenceNumber;
	}
	/**
	 * 获取：订单的序列号(用于展示)
	 */
	public String getOrderSequenceNumber() {
		return orderSequenceNumber;
	}
	/**
	 * 设置：下游商户服务号ID
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	/**
	 * 获取：下游商户服务号ID
	 */
	public String getPartnerId() {
		return partnerId;
	}
	/**
	 * 设置：下单用户ID 匿名时可为0或空
	 */
	public void setPartnerUserId(String partnerUserId) {
		this.partnerUserId = partnerUserId;
	}
	/**
	 * 获取：下单用户ID 匿名时可为0或空
	 */
	public String getPartnerUserId() {
		return partnerUserId;
	}
	/**
	 * 设置：订单类型 1-供应链销售订单、2-供应链调拨订单、3-供应链采购订单、4-超市POS机订单（包括超市扫码订单）、7-无人便利店订单、9-增值服务订单、10-无人货架订单、11-向1号生活扫码支付（公司收款）、12-1号生活加盟店（收加盟费）
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：订单类型 1-供应链销售订单、2-供应链调拨订单、3-供应链采购订单、4-超市POS机订单（包括超市扫码订单）、7-无人便利店订单、9-增值服务订单、10-无人货架订单、11-向1号生活扫码支付（公司收款）、12-1号生活加盟店（收加盟费）
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：订单状态 不要占用0  1-创建订单（未确认）、2-已取消、8-已支付、16-已接单、32-发货中、64-配送中、128-已送达、256-确认收货/已完成、260-***确认收货/已完成、512-已收款、1024-分批配送、2048-拒绝订单。
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：订单状态 不要占用0  1-创建订单（未确认）、2-已取消、8-已支付、16-已接单、32-发货中、64-配送中、128-已送达、256-确认收货/已完成、260-***确认收货/已完成、512-已收款、1024-分批配送、2048-拒绝订单。
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：发货状态：0-未发货、32-发货中、64-配送中、128-已送达
	 */
	public void setDeliverStatus(Integer deliverStatus) {
		this.deliverStatus = deliverStatus;
	}
	/**
	 * 获取：发货状态：0-未发货、32-发货中、64-配送中、128-已送达
	 */
	public Integer getDeliverStatus() {
		return deliverStatus;
	}
	/**
	 * 设置：退款状态 0表示未退货 同时可用于记录是否已打款状态
	 */
	public void setRefundStatus(Integer refundStatus) {
		this.refundStatus = refundStatus;
	}
	/**
	 * 获取：退款状态 0表示未退货 同时可用于记录是否已打款状态
	 */
	public Integer getRefundStatus() {
		return refundStatus;
	}
	/**
	 * 设置：支付类型 参考逻辑设定 -1未支付
	 */
	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * 获取：支付类型 参考逻辑设定 -1未支付
	 */
	public Integer getPaymentType() {
		return paymentType;
	}
	/**
	 * 设置：交易类型，微信系：JSAPI、NATIVE、APP等
	 */
	public void setTransType(String transType) {
		this.transType = transType;
	}
	/**
	 * 获取：交易类型，微信系：JSAPI、NATIVE、APP等
	 */
	public String getTransType() {
		return transType;
	}
	/**
	 * 设置：用户来源
	 */
	public void setUserSource(Integer userSource) {
		this.userSource = userSource;
	}
	/**
	 * 获取：用户来源
	 */
	public Integer getUserSource() {
		return userSource;
	}
	/**
	 * 设置：当天排号 相对于发货方
	 */
	public void setDaySortNumber(Integer daySortNumber) {
		this.daySortNumber = daySortNumber;
	}
	/**
	 * 获取：当天排号 相对于发货方
	 */
	public Integer getDaySortNumber() {
		return daySortNumber;
	}
	/**
	 * 设置：推送类型，0表示不推送 1时表示下单时推送 2时表示支付后推送
	 */
	public void setPushType(Integer pushType) {
		this.pushType = pushType;
	}
	/**
	 * 获取：推送类型，0表示不推送 1时表示下单时推送 2时表示支付后推送
	 */
	public Integer getPushType() {
		return pushType;
	}
	/**
	 * 设置：实际销售的用户ID（销售仓）
	 */
	public void setSaleUserId(String saleUserId) {
		this.saleUserId = saleUserId;
	}
	/**
	 * 获取：实际销售的用户ID（销售仓）
	 */
	public String getSaleUserId() {
		return saleUserId;
	}
	/**
	 * 设置：发货用户ID
	 */
	public void setShippingUserId(String shippingUserId) {
		this.shippingUserId = shippingUserId;
	}
	/**
	 * 获取：发货用户ID
	 */
	public String getShippingUserId() {
		return shippingUserId;
	}
	/**
	 * 设置：发货用户昵称
	 */
	public void setShippingNickName(String shippingNickName) {
		this.shippingNickName = shippingNickName;
	}
	/**
	 * 获取：发货用户昵称
	 */
	public String getShippingNickName() {
		return shippingNickName;
	}
	/**
	 * 设置：发货省份
	 */
	public void setShippingProvince(String shippingProvince) {
		this.shippingProvince = shippingProvince;
	}
	/**
	 * 获取：发货省份
	 */
	public String getShippingProvince() {
		return shippingProvince;
	}
	/**
	 * 设置：发货城市
	 */
	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}
	/**
	 * 获取：发货城市
	 */
	public String getShippingCity() {
		return shippingCity;
	}
	/**
	 * 设置：发货区域
	 */
	public void setShippingDistrict(String shippingDistrict) {
		this.shippingDistrict = shippingDistrict;
	}
	/**
	 * 获取：发货区域
	 */
	public String getShippingDistrict() {
		return shippingDistrict;
	}
	/**
	 * 设置：发货详细地址(尽量不要包含省市区)
	 */
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	/**
	 * 获取：发货详细地址(尽量不要包含省市区)
	 */
	public String getShippingAddress() {
		return shippingAddress;
	}
	/**
	 * 设置：latitude,longitude
	 */
	public void setShippingLocation(String shippingLocation) {
		this.shippingLocation = shippingLocation;
	}
	/**
	 * 获取：latitude,longitude
	 */
	public String getShippingLocation() {
		return shippingLocation;
	}
	/**
	 * 设置：发货人联系号码
	 */
	public void setShippingPhone(String shippingPhone) {
		this.shippingPhone = shippingPhone;
	}
	/**
	 * 获取：发货人联系号码
	 */
	public String getShippingPhone() {
		return shippingPhone;
	}
	/**
	 * 设置：收货人用户ID
	 */
	public void setReceiptUserId(String receiptUserId) {
		this.receiptUserId = receiptUserId;
	}
	/**
	 * 获取：收货人用户ID
	 */
	public String getReceiptUserId() {
		return receiptUserId;
	}
	/**
	 * 设置：收货人昵称
	 */
	public void setReceiptNickName(String receiptNickName) {
		this.receiptNickName = receiptNickName;
	}
	/**
	 * 获取：收货人昵称
	 */
	public String getReceiptNickName() {
		return receiptNickName;
	}
	/**
	 * 设置：收货省份
	 */
	public void setReceiptProvince(String receiptProvince) {
		this.receiptProvince = receiptProvince;
	}
	/**
	 * 获取：收货省份
	 */
	public String getReceiptProvince() {
		return receiptProvince;
	}
	/**
	 * 设置：收货城市
	 */
	public void setReceiptCity(String receiptCity) {
		this.receiptCity = receiptCity;
	}
	/**
	 * 获取：收货城市
	 */
	public String getReceiptCity() {
		return receiptCity;
	}
	/**
	 * 设置：收货区域
	 */
	public void setReceiptDistrict(String receiptDistrict) {
		this.receiptDistrict = receiptDistrict;
	}
	/**
	 * 获取：收货区域
	 */
	public String getReceiptDistrict() {
		return receiptDistrict;
	}
	/**
	 * 设置：收货具体地址(尽量不要包含省市区)
	 */
	public void setReceiptAddress(String receiptAddress) {
		this.receiptAddress = receiptAddress;
	}
	/**
	 * 获取：收货具体地址(尽量不要包含省市区)
	 */
	public String getReceiptAddress() {
		return receiptAddress;
	}
	/**
	 * 设置：收货人电话
	 */
	public void setReceiptPhone(String receiptPhone) {
		this.receiptPhone = receiptPhone;
	}
	/**
	 * 获取：收货人电话
	 */
	public String getReceiptPhone() {
		return receiptPhone;
	}
	/**
	 * 设置：latitude,longitude
	 */
	public void setReceiptLocation(String receiptLocation) {
		this.receiptLocation = receiptLocation;
	}
	/**
	 * 获取：latitude,longitude
	 */
	public String getReceiptLocation() {
		return receiptLocation;
	}
	/**
	 * 设置：配送员用户ID
	 */
	public void setCourierPassportId(String courierUserId) {
		this.courierUserId = courierUserId;
	}
	/**
	 * 获取：配送员用户ID
	 */
	public String getCourierUserId() {
		return courierUserId;
	}
	/**
	 * 设置：配送员昵称
	 */
	public void setCourierNickName(String courierNickName) {
		this.courierNickName = courierNickName;
	}
	/**
	 * 获取：配送员昵称
	 */
	public String getCourierNickName() {
		return courierNickName;
	}
	/**
	 * 设置：配送员联系号码
	 */
	public void setCourierPhone(String courierPhone) {
		this.courierPhone = courierPhone;
	}
	/**
	 * 获取：配送员联系号码
	 */
	public String getCourierPhone() {
		return courierPhone;
	}
	/**
	 * 设置：订单总距离
	 */
	public void setTotalDistance(Long totalDistance) {
		this.totalDistance = totalDistance;
	}
	/**
	 * 获取：订单总距离
	 */
	public Long getTotalDistance() {
		return totalDistance;
	}
	/**
	 * 设置：下单时所在地理位置
	 */
	public void setCurrentLocation(String currentLocation) {
		this.currentLocation = currentLocation;
	}
	/**
	 * 获取：下单时所在地理位置
	 */
	public String getCurrentLocation() {
		return currentLocation;
	}
	/**
	 * 设置：是否需要配送员代收费用 0表示不需要
	 */
	public void setCollectingFees(Integer collectingFees) {
		this.collectingFees = collectingFees;
	}
	/**
	 * 获取：是否需要配送员代收费用 0表示不需要
	 */
	public Integer getCollectingFees() {
		return collectingFees;
	}
	/**
	 * 设置：订单描述
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：订单描述
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：实收费用
	 */
	public void setActualPrice(Long actualPrice) {
		this.actualPrice = actualPrice;
	}
	/**
	 * 获取：实收费用
	 */
	public Long getActualPrice() {
		return actualPrice;
	}
	/**
	 * 设置：订单总费用
	 */
	public void setTotalPrice(Long totalPrice) {
		this.totalPrice = totalPrice;
	}
	/**
	 * 获取：订单总费用
	 */
	public Long getTotalPrice() {
		return totalPrice;
	}
	/**
	 * 设置：优惠的费用
	 */
	public void setDiscountPrice(Long discountPrice) {
		this.discountPrice = discountPrice;
	}
	/**
	 * 获取：优惠的费用
	 */
	public Long getDiscountPrice() {
		return discountPrice;
	}
	/**
	 * 设置：配送费用
	 */
	public void setDistributionFee(Long distributionFee) {
		this.distributionFee = distributionFee;
	}
	/**
	 * 获取：配送费用
	 */
	public Long getDistributionFee() {
		return distributionFee;
	}
	/**
	 * 设置：价格记录 如优惠的政策
	 */
	public void setPriceLogger(String priceLogger) {
		this.priceLogger = priceLogger;
	}
	/**
	 * 获取：价格记录 如优惠的政策
	 */
	public String getPriceLogger() {
		return priceLogger;
	}
	/**
	 * 设置：税率(万分比)
	 */
	public void setTaxRate(Long taxRate) {
		this.taxRate = taxRate;
	}
	/**
	 * 获取：税率(万分比)
	 */
	public Long getTaxRate() {
		return taxRate;
	}
	/**
	 * 设置：取消的理由
	 */
	public void setCancelLogger(String cancelLogger) {
		this.cancelLogger = cancelLogger;
	}
	/**
	 * 获取：取消的理由
	 */
	public String getCancelLogger() {
		return cancelLogger;
	}
	/**
	 * 设置：订单的生成时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：订单的生成时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：支付时间
	 */
	public void setPaymentTime(Date paymentTime) {
		this.paymentTime = paymentTime;
	}
	/**
	 * 获取：支付时间
	 */
	public Date getPaymentTime() {
		return paymentTime;
	}
	/**
	 * 设置：确认订单时间
	 */
	public void setConfirmTime(Date confirmTime) {
		this.confirmTime = confirmTime;
	}
	/**
	 * 获取：确认订单时间
	 */
	public Date getConfirmTime() {
		return confirmTime;
	}
	/**
	 * 设置：商品或支付单简要描述
	 */
	public void setBody(String body) {
		this.body = body;
	}
	/**
	 * 获取：商品或支付单简要描述
	 */
	public String getBody() {
		return body;
	}
	/**
	 * 设置：商品名称明细列表(仅记录体系外的数据)
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}
	/**
	 * 获取：商品名称明细列表(仅记录体系外的数据)
	 */
	public String getDetail() {
		return detail;
	}
	/**
	 * 设置：信用付费用
	 */
	public void setCreditFee(String creditFee) {
		this.creditFee = creditFee;
	}
	/**
	 * 获取：信用付费用
	 */
	public String getCreditFee() {
		return creditFee;
	}
	/**
	 * 设置：是否使用第三方物流 0否 1是
	 */
	public void setThirdPartyDelivery(Integer thirdPartyDelivery) {
		this.thirdPartyDelivery = thirdPartyDelivery;
	}
	/**
	 * 获取：是否使用第三方物流 0否 1是
	 */
	public Integer getThirdPartyDelivery() {
		return thirdPartyDelivery;
	}
	/**
	 * 设置：会员价(分)
	 */
	public void setMembershipPrice(Long membershipPrice) {
		this.membershipPrice = membershipPrice;
	}
	/**
	 * 获取：会员价(分)
	 */
	public Long getMembershipPrice() {
		return membershipPrice;
	}
	/**
	 * 设置：第三方物流的状态 1-待推,2-全部推完,3-冲红
	 */
	public void setThirdPartyDeliveryStatus(Integer thirdPartyDeliveryStatus) {
		this.thirdPartyDeliveryStatus = thirdPartyDeliveryStatus;
	}
	/**
	 * 获取：第三方物流的状态 1-待推,2-全部推完,3-冲红
	 */
	public Integer getThirdPartyDeliveryStatus() {
		return thirdPartyDeliveryStatus;
	}
	/**
	 * 设置：是否为加盟店 1为是
	 */
	public void setIsJoin(Integer isJoin) {
		this.isJoin = isJoin;
	}
	/**
	 * 获取：是否为加盟店 1为是
	 */
	public Integer getIsJoin() {
		return isJoin;
	}
	/**
	 * 设置：是否为第三方 0为平台 不等于0为第三方
	 */
	public void setIsThirdParty(Integer isThirdParty) {
		this.isThirdParty = isThirdParty;
	}
	/**
	 * 获取：是否为第三方 0为平台 不等于0为第三方
	 */
	public Integer getIsThirdParty() {
		return isThirdParty;
	}
}
