package com.tuoniaostore.datamodel.vo.payment;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;


/**
 * 转账记录
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public class PaymentTransactionLoggerDateFormat implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id = DefineRandom.getUUID();
	//发生交易的通行证ID
	private String userId;
	//创建人名字
	private String userName;
	//我方生成的交易序列号 tn20190325 135020 30000000（tn + 年 + 月 + 日 + 时 + 分 + 秒 + ）
	private String transSequenceNumber;
	//交易状态 枚举 TransStatusEnum  ****
	private Integer transStatus;

	//支付类型 0-余额 1-微信 2-支付宝 3-银联等 如有其他交易方式 由逻辑决定
	private String paymentType;
	//交易类型 1-支付 2-充值 3-代付 4-提现 		*****
	private Integer transType;
	//我方为合作商分配的商家服务号
	private String partnerId;
	//商户的appId
	private String appId;
	//合作商家的用户标志
	private String partnerUserId;
	//合作商家的交易序号
	private String partnerTradeNumber;
	//我方为上游合作商分配的渠道号 0-官方渠道 其他值由逻辑决定
	private Integer channelId;
	//渠道方的交易序列号
	private String channelTradeNumber;
	//渠道方的用户ID(唯一标志)
	private String channelUserId;
	//渠道方用户名
	private String channelUserName;
	//渠道方的备注信息
	private String channelRemark;
	//收款帐号(仅提现时有效)	****
	private String accountNumber;

	//提现规则id(仅提现时有效)****
	private String takeRuleId;
	private String takeDesc;
	private long lowCost;
	private long highCost;
	private long rate;

	//收款账户名
	private String accountName;
	//账户类型，如：信用卡、储值卡
	private Integer accountType;
	//交易货币类型，如RMB
	private String currencyType;
	//银行名	****
	private String bankName;
	//银行编码，如支行编码
	private String bankBranchCode;
	//银行简称代码，如：ICBC
	private String bankSimpleName;
	//交易单价 单位：分	***
	private Long transUnitAmount;
	//交易数量
	private Integer transNumber;
	//交易总价 单位：分 这里未必等于单价*数量 因为可能存在优惠	***
	private Long transTotalAmount;
	//交易标题 提现
	private String transTitle;
	//备注内容
	private String remark;
	//创建时间
	private Date createTime;
	//交易时间 ****
	private Date transCreateTime;
	//支付时间
	private Date paymentTime;
	//是否使用优惠券 0-未使用 1-使用
	private Integer useConpon;
	//优惠额度 单位：分
	private Long discountAmount;
	//退款状态 0-未退款
	private Integer refundStatus;
	//退款时间
	private Date refundTime;
	//处理完请求后，当前页面跳转到指定页面的路径
	private String notifyFrontUrl;
	//回调接口
	private String notifyUrl;
	//其他参数
	private String extendParameter;
	//操作时间
	private Date extendTime;

	public String getTakeDesc() {
		return takeDesc;
	}

	public void setTakeDesc(String takeDesc) {
		this.takeDesc = takeDesc;
	}

	public long getLowCost() {
		return lowCost;
	}

	public void setLowCost(long lowCost) {
		this.lowCost = lowCost;
	}

	public long getHighCost() {
		return highCost;
	}

	public void setHighCost(long highCost) {
		this.highCost = highCost;
	}

	public long getRate() {
		return rate;
	}

	public void setRate(long rate) {
		this.rate = rate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTakeRuleId() {
		return takeRuleId;
	}

	public void setTakeRuleId(String takeRuleId) {
		this.takeRuleId = takeRuleId;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：发生交易的通行证ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：发生交易的通行证ID
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：我方生成的交易序列号
	 */
	public void setTransSequenceNumber(String transSequenceNumber) {
		this.transSequenceNumber = transSequenceNumber;
	}
	/**
	 * 获取：我方生成的交易序列号
	 */
	public String getTransSequenceNumber() {
		return transSequenceNumber;
	}
	/**
	 * 设置：交易状态 使用二进制方式计算 如：客户端成功时为4 回调成功时为8 双方成功后为12 默认1
	 */
	public void setTransStatus(Integer transStatus) {
		this.transStatus = transStatus;
	}
	/**
	 * 获取：交易状态 使用二进制方式计算 如：客户端成功时为4 回调成功时为8 双方成功后为12 默认1
	 */
	public Integer getTransStatus() {
		return transStatus;
	}
	/**
	 * 设置：支付类型 0-余额 1-微信 2-支付宝 3-银联等 如有其他交易方式 由逻辑决定
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * 获取：支付类型 0-余额 1-微信 2-支付宝 3-银联等 如有其他交易方式 由逻辑决定
	 */
	public String getPaymentType() {
		return paymentType;
	}
	/**
	 * 设置：交易类型 1-支付 2-充值 3-代付 4-提现
	 */
	public void setTransType(Integer transType) {
		this.transType = transType;
	}
	/**
	 * 获取：交易类型 1-支付 2-充值 3-代付 4-提现
	 */
	public Integer getTransType() {
		return transType;
	}
	/**
	 * 设置：我方为合作商分配的商家服务号
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	/**
	 * 获取：我方为合作商分配的商家服务号
	 */
	public String getPartnerId() {
		return partnerId;
	}
	/**
	 * 设置：商户的appId
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * 获取：商户的appId
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * 设置：合作商家的用户标志
	 */
	public void setPartnerUserId(String partnerUserId) {
		this.partnerUserId = partnerUserId;
	}
	/**
	 * 获取：合作商家的用户标志
	 */
	public String getPartnerUserId() {
		return partnerUserId;
	}
	/**
	 * 设置：合作商家的交易序号
	 */
	public void setPartnerTradeNumber(String partnerTradeNumber) {
		this.partnerTradeNumber = partnerTradeNumber;
	}
	/**
	 * 获取：合作商家的交易序号
	 */
	public String getPartnerTradeNumber() {
		return partnerTradeNumber;
	}
	/**
	 * 设置：我方为上游合作商分配的渠道号 0-官方渠道 其他值由逻辑决定
	 */
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：我方为上游合作商分配的渠道号 0-官方渠道 其他值由逻辑决定
	 */
	public Integer getChannelId() {
		return channelId;
	}
	/**
	 * 设置：渠道方的交易序列号
	 */
	public void setChannelTradeNumber(String channelTradeNumber) {
		this.channelTradeNumber = channelTradeNumber;
	}
	/**
	 * 获取：渠道方的交易序列号
	 */
	public String getChannelTradeNumber() {
		return channelTradeNumber;
	}
	/**
	 * 设置：渠道方的用户ID(唯一标志)
	 */
	public void setChannelUserId(String channelUserId) {
		this.channelUserId = channelUserId;
	}
	/**
	 * 获取：渠道方的用户ID(唯一标志)
	 */
	public String getChannelUserId() {
		return channelUserId;
	}
	/**
	 * 设置：渠道方用户名
	 */
	public void setChannelUserName(String channelUserName) {
		this.channelUserName = channelUserName;
	}
	/**
	 * 获取：渠道方用户名
	 */
	public String getChannelUserName() {
		return channelUserName;
	}
	/**
	 * 设置：渠道方的备注信息
	 */
	public void setChannelRemark(String channelRemark) {
		this.channelRemark = channelRemark;
	}
	/**
	 * 获取：渠道方的备注信息
	 */
	public String getChannelRemark() {
		return channelRemark;
	}
	/**
	 * 设置：收款帐号(仅提现时有效)
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	/**
	 * 获取：收款帐号(仅提现时有效)
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	/**
	 * 设置：收款账户名
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	/**
	 * 获取：收款账户名
	 */
	public String getAccountName() {
		return accountName;
	}
	/**
	 * 设置：账户类型，如：信用卡、储值卡
	 */
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	/**
	 * 获取：账户类型，如：信用卡、储值卡
	 */
	public Integer getAccountType() {
		return accountType;
	}
	/**
	 * 设置：交易货币类型，如RMB
	 */
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}
	/**
	 * 获取：交易货币类型，如RMB
	 */
	public String getCurrencyType() {
		return currencyType;
	}
	/**
	 * 设置：银行名
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/**
	 * 获取：银行名
	 */
	public String getBankName() {
		return bankName;
	}
	/**
	 * 设置：银行编码，如支行编码
	 */
	public void setBankBranchCode(String bankBranchCode) {
		this.bankBranchCode = bankBranchCode;
	}
	/**
	 * 获取：银行编码，如支行编码
	 */
	public String getBankBranchCode() {
		return bankBranchCode;
	}
	/**
	 * 设置：银行简称代码，如：ICBC
	 */
	public void setBankSimpleName(String bankSimpleName) {
		this.bankSimpleName = bankSimpleName;
	}
	/**
	 * 获取：银行简称代码，如：ICBC
	 */
	public String getBankSimpleName() {
		return bankSimpleName;
	}
	/**
	 * 设置：交易单价 单位：分
	 */
	public void setTransUnitAmount(Long transUnitAmount) {
		this.transUnitAmount = transUnitAmount;
	}
	/**
	 * 获取：交易单价 单位：分
	 */
	public Long getTransUnitAmount() {
		return transUnitAmount;
	}
	/**
	 * 设置：交易数量
	 */
	public void setTransNumber(Integer transNumber) {
		this.transNumber = transNumber;
	}
	/**
	 * 获取：交易数量
	 */
	public Integer getTransNumber() {
		return transNumber;
	}
	/**
	 * 设置：交易总价 单位：分 这里未必等于单价*数量 因为可能存在优惠
	 */
	public void setTransTotalAmount(Long transTotalAmount) {
		this.transTotalAmount = transTotalAmount;
	}
	/**
	 * 获取：交易总价 单位：分 这里未必等于单价*数量 因为可能存在优惠
	 */
	public Long getTransTotalAmount() {
		return transTotalAmount;
	}
	/**
	 * 设置：交易标题
	 */
	public void setTransTitle(String transTitle) {
		this.transTitle = transTitle;
	}
	/**
	 * 获取：交易标题
	 */
	public String getTransTitle() {
		return transTitle;
	}
	/**
	 * 设置：备注内容
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注内容
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	@JsonFormat(pattern="yyyy年MM月dd日 HH:mm:ss",timezone = "GMT+8")  // HH 大写24小时制
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：交易时间
	 */
	public void setTransCreateTime(Date transCreateTime) {
		this.transCreateTime = transCreateTime;
	}
	/**
	 * 获取：交易时间
	 */
	@JsonFormat(pattern="yyyy年MM月dd日 HH:mm:ss",timezone = "GMT+8")  // HH 大写24小时制
	public Date getTransCreateTime() {
		return transCreateTime;
	}
	/**
	 * 设置：支付时间
	 */
	public void setPaymentTime(Date paymentTime) {
		this.paymentTime = paymentTime;
	}
	/**
	 * 获取：支付时间
	 */
	@JsonFormat(pattern="yyyy年MM月dd日 HH:mm:ss",timezone = "GMT+8")  // HH 大写24小时制
	public Date getPaymentTime() {
		return paymentTime;
	}
	/**
	 * 设置：是否使用优惠券 0-未使用 1-使用
	 */
	public void setUseConpon(Integer useConpon) {
		this.useConpon = useConpon;
	}
	/**
	 * 获取：是否使用优惠券 0-未使用 1-使用
	 */
	public Integer getUseConpon() {
		return useConpon;
	}
	/**
	 * 设置：优惠额度 单位：分
	 */
	public void setDiscountAmount(Long discountAmount) {
		this.discountAmount = discountAmount;
	}
	/**
	 * 获取：优惠额度 单位：分
	 */
	public Long getDiscountAmount() {
		return discountAmount;
	}
	/**
	 * 设置：退款状态 0-未退款
	 */
	public void setRefundStatus(Integer refundStatus) {
		this.refundStatus = refundStatus;
	}
	/**
	 * 获取：退款状态 0-未退款
	 */
	public Integer getRefundStatus() {
		return refundStatus;
	}
	/**
	 * 设置：退款时间
	 */
	public void setRefundTime(Date refundTime) {
		this.refundTime = refundTime;
	}
	/**
	 * 获取：退款时间
	 */
	@JsonFormat(pattern="yyyy年MM月dd日 HH:mm:ss",timezone = "GMT+8")  // HH 大写24小时制
	public Date getRefundTime() {
		return refundTime;
	}
	/**
	 * 设置：处理完请求后，当前页面跳转到指定页面的路径
	 */
	public void setNotifyFrontUrl(String notifyFrontUrl) {
		this.notifyFrontUrl = notifyFrontUrl;
	}
	/**
	 * 获取：处理完请求后，当前页面跳转到指定页面的路径
	 */
	public String getNotifyFrontUrl() {
		return notifyFrontUrl;
	}
	/**
	 * 设置：回调接口
	 */
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}
	/**
	 * 获取：回调接口
	 */
	public String getNotifyUrl() {
		return notifyUrl;
	}
	/**
	 * 设置：其他参数
	 */
	public void setExtendParameter(String extendParameter) {
		this.extendParameter = extendParameter;
	}
	/**
	 * 获取：其他参数
	 */
	public String getExtendParameter() {
		return extendParameter;
	}
	/**
	 * 设置：操作时间
	 */
	public void setExtendTime(Date extendTime) {
		this.extendTime = extendTime;
	}
	/**
	 * 获取：操作时间
	 */
	@JsonFormat(pattern="yyyy年MM月dd日 HH:mm:ss",timezone = "GMT+8")  // HH 大写24小时制
	public Date getExtendTime() {
		return extendTime;
	}
}
