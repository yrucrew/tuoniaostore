package com.tuoniaostore.datamodel.vo.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author oy
 * @description 商品的销售数据（数据库查询结果存放）
 * @date 2019/5/21
 */
public class GoodSaleMsgVO implements Serializable {

    private static final long serialVersionUID = 1L;

    //商店id
    private String shopId;

    //商品id
    private String goodId;

    //商品名称
    private String goodName;

    //商品销售价
    private BigDecimal salePrice;

    //商品销售个数
    private BigDecimal saleNum;

    //商品销售总价
    private BigDecimal saleTotlePrice;

    //商品销售时间
    private Date createDate;

    public GoodSaleMsgVO() {
    }

    public BigDecimal getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(BigDecimal saleNum) {
        this.saleNum = saleNum;
    }

    public BigDecimal getSaleTotlePrice() {
        return saleTotlePrice;
    }

    public void setSaleTotlePrice(BigDecimal saleTotlePrice) {
        this.saleTotlePrice = saleTotlePrice;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }


    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }
}
