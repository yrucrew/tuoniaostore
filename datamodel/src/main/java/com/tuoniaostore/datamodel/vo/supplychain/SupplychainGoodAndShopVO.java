package com.tuoniaostore.datamodel.vo.supplychain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 查询购物车列表
 * @author sqd
 * @date 2019/4/8
 * @param
 * @return
 */
public class SupplychainGoodAndShopVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String shopName;//仓库
    private String shopId;//仓库Id
    private int shopType ;//仓库类型
    private String shopUserId;//仓库Id
    private String parentId ;//父级仓库ID 一级 仓时为0
    private Long distributionCost;//配送费
    //联系人号码
    private String phoneNumber;
    //所在省份
    private String province;
    //所在城市
    private String city;
    //所在区域
    private String district;
    //具体地址
    private String address;
    //起订量
    private Long moq;
    //配送类型 1-我方配送 2-独立配送
    private Integer distributionType;
    //建立时间
    private Date createTime;
    private String location;

    //经度
    private Double longitude;
    //纬度
    private Double latitude;
    //覆盖距离 单位：米
    //测试
    private Long coveringDistance;

    private List<SupplychainGoodVo> list;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Long getCoveringDistance() {
        return coveringDistance;
    }

    public void setCoveringDistance(Long coveringDistance) {
        this.coveringDistance = coveringDistance;
    }



    public String getShopUserId() {
        return shopUserId;
    }

    public void setShopUserId(String shopUserId) {
        this.shopUserId = shopUserId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getDistributionType() {
        return distributionType;
    }

    public void setDistributionType(Integer distributionType) {
        this.distributionType = distributionType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getMoq() {
        return moq;
    }

    public void setMoq(Long moq) {
        this.moq = moq;
    }

    public Long getDistributionCost() {
        return distributionCost;
    }

    public void setDistributionCost(Long distributionCost) {
        this.distributionCost = distributionCost;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public int getShopType() {
        return shopType;
    }

    public void setShopType(int shopType) {
        this.shopType = shopType;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }



    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public List<SupplychainGoodVo> getList() {
        return list;
    }

    public void setList(List<SupplychainGoodVo> list) {
        this.list = list;
    }
}
