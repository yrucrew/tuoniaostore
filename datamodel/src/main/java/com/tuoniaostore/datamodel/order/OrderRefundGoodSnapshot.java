package com.tuoniaostore.datamodel.order;

import com.tuoniaostore.datamodel.DefineRandom;

import java.util.Date;

public class OrderRefundGoodSnapshot {
    private static final long serialVersionUID = 1L;

    private String id = DefineRandom.getUUID();
    //对应的订单ID
    private String orderId;
    //购买的用户ID或其他标志(如第三方用户ID)
    private String userMark;
    //相关的商品ID
    private String goodId;
    //商品模版ID
    private String goodTemplateId;
    //当时的商品名称
    private String goodName;
    //当时的归属类型ID
    private String goodTypeId;
    //当时归属的类型名
    private String goodTypeName;
    //价格标题
    private String goodPriceTitle;
    //价格id
    private String goodPriceId;
    //当时的单位ID
    private String goodUnitId;
    //当时单位名称
    private String goodUnitName;
    //商品条码
    private String goodBarcode;
    //当时商品的编码
    private String goodCode;
    //商品批次
    private Integer goodBatches;
    //当时的介绍页面
    private String introductionPage;
    //购买的时间
    private Date createTime;
    //正常价格购买的数量 订单数 ***
    private Integer normalQuantity;
    //优惠价格购买的数量
    private Integer discountQuantity;
    //发货中的数量
    private Integer shipmentQuantity;
    //配送中的数量
    private Integer distributionQuantity;
    //送达的数量
    private Integer arriveQuantity;
    //被确认收货的数量  ***
    private Integer receiptQuantity;
    //退货中数量
    private Integer refundQuantity;
    //退货的数量
    private Integer returnQuantity;
    //复核数量
    private Integer reviewQuantity;
    //正常价格
    private Long normalPrice;
    //优惠价格
    private Long discountPrice;
    //当时的成本价
    private Long costPrice;
    //当时一般的市场价
    private Long marketPrice;
    //应收费用
    private Long totalPrice;
    //退货的费用(实收为应收费用-退货费用)
    private Long returnPrice;
    //区分仓库的订单号
    private String innerOrderId;
    //第三方送货的数量
    private Integer thirdPartyDeliveryQuantity;
    //第三方排线的数量
    private Integer thirdPartyAcceptQuantity;
    //是否第三方商品，否为0
    private Integer isThirdParty;
    // 供应商id
    private String shopId;
    // 供应商名称
    private String shopName;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserMark() {
        return userMark;
    }

    public void setUserMark(String userMark) {
        this.userMark = userMark;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getGoodTemplateId() {
        return goodTemplateId;
    }

    public void setGoodTemplateId(String goodTemplateId) {
        this.goodTemplateId = goodTemplateId;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodTypeId() {
        return goodTypeId;
    }

    public void setGoodTypeId(String goodTypeId) {
        this.goodTypeId = goodTypeId;
    }

    public String getGoodTypeName() {
        return goodTypeName;
    }

    public void setGoodTypeName(String goodTypeName) {
        this.goodTypeName = goodTypeName;
    }

    public String getGoodPriceTitle() {
        return goodPriceTitle;
    }

    public void setGoodPriceTitle(String goodPriceTitle) {
        this.goodPriceTitle = goodPriceTitle;
    }

    public String getGoodPriceId() {
        return goodPriceId;
    }

    public void setGoodPriceId(String goodPriceId) {
        this.goodPriceId = goodPriceId;
    }

    public String getGoodUnitId() {
        return goodUnitId;
    }

    public void setGoodUnitId(String goodUnitId) {
        this.goodUnitId = goodUnitId;
    }

    public String getGoodUnitName() {
        return goodUnitName;
    }

    public void setGoodUnitName(String goodUnitName) {
        this.goodUnitName = goodUnitName;
    }

    public String getGoodBarcode() {
        return goodBarcode;
    }

    public void setGoodBarcode(String goodBarcode) {
        this.goodBarcode = goodBarcode;
    }

    public String getGoodCode() {
        return goodCode;
    }

    public void setGoodCode(String goodCode) {
        this.goodCode = goodCode;
    }

    public Integer getGoodBatches() {
        return goodBatches;
    }

    public void setGoodBatches(Integer goodBatches) {
        this.goodBatches = goodBatches;
    }

    public String getIntroductionPage() {
        return introductionPage;
    }

    public void setIntroductionPage(String introductionPage) {
        this.introductionPage = introductionPage;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getNormalQuantity() {
        return normalQuantity;
    }

    public void setNormalQuantity(Integer normalQuantity) {
        this.normalQuantity = normalQuantity;
    }

    public Integer getDiscountQuantity() {
        return discountQuantity;
    }

    public void setDiscountQuantity(Integer discountQuantity) {
        this.discountQuantity = discountQuantity;
    }

    public Integer getShipmentQuantity() {
        return shipmentQuantity;
    }

    public void setShipmentQuantity(Integer shipmentQuantity) {
        this.shipmentQuantity = shipmentQuantity;
    }

    public Integer getDistributionQuantity() {
        return distributionQuantity;
    }

    public void setDistributionQuantity(Integer distributionQuantity) {
        this.distributionQuantity = distributionQuantity;
    }

    public Integer getArriveQuantity() {
        return arriveQuantity;
    }

    public void setArriveQuantity(Integer arriveQuantity) {
        this.arriveQuantity = arriveQuantity;
    }

    public Integer getReceiptQuantity() {
        return receiptQuantity;
    }

    public void setReceiptQuantity(Integer receiptQuantity) {
        this.receiptQuantity = receiptQuantity;
    }

    public Integer getRefundQuantity() {
        return refundQuantity;
    }

    public void setRefundQuantity(Integer refundQuantity) {
        this.refundQuantity = refundQuantity;
    }

    public Integer getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(Integer returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public Integer getReviewQuantity() {
        return reviewQuantity;
    }

    public void setReviewQuantity(Integer reviewQuantity) {
        this.reviewQuantity = reviewQuantity;
    }

    public Long getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(Long normalPrice) {
        this.normalPrice = normalPrice;
    }

    public Long getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Long discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Long getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Long costPrice) {
        this.costPrice = costPrice;
    }

    public Long getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Long marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Long getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(Long returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getInnerOrderId() {
        return innerOrderId;
    }

    public void setInnerOrderId(String innerOrderId) {
        this.innerOrderId = innerOrderId;
    }

    public Integer getThirdPartyDeliveryQuantity() {
        return thirdPartyDeliveryQuantity;
    }

    public void setThirdPartyDeliveryQuantity(Integer thirdPartyDeliveryQuantity) {
        this.thirdPartyDeliveryQuantity = thirdPartyDeliveryQuantity;
    }

    public Integer getThirdPartyAcceptQuantity() {
        return thirdPartyAcceptQuantity;
    }

    public void setThirdPartyAcceptQuantity(Integer thirdPartyAcceptQuantity) {
        this.thirdPartyAcceptQuantity = thirdPartyAcceptQuantity;
    }

    public Integer getIsThirdParty() {
        return isThirdParty;
    }

    public void setIsThirdParty(Integer isThirdParty) {
        this.isThirdParty = isThirdParty;
    }
}
