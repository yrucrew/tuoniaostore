package com.tuoniaostore.datamodel.order;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 *  订单状态日志
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public class OrderStateLogger implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//对应的订单ID
	private String orderId;
	//发货者通行证ID
	private String shippingUserId;
	//对应的订单类型
	private Integer orderType;
	//操作前的状态
	private Integer beforeStatus;
	//操作后的状态
	private Integer status;
	//操作时间
	private Date operatorTime;
	//操作者角色ID
	private String operatorUserId;
	//操作者名字
	private String operatorName;
	//操作备注信息
	private String operatorDescribe;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：对应的订单ID
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：对应的订单ID
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * 设置：发货者通行证ID
	 */
	public void setShippingUserId(String shippingUserId) {
		this.shippingUserId = shippingUserId;
	}
	/**
	 * 获取：发货者通行证ID
	 */
	public String getShippingPassportId() {
		return shippingUserId;
	}
	/**
	 * 设置：对应的订单类型
	 */
	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}
	/**
	 * 获取：对应的订单类型
	 */
	public Integer getOrderType() {
		return orderType;
	}
	/**
	 * 设置：操作前的状态
	 */
	public void setBeforeStatus(Integer beforeStatus) {
		this.beforeStatus = beforeStatus;
	}
	/**
	 * 获取：操作前的状态
	 */
	public Integer getBeforeStatus() {
		return beforeStatus;
	}
	/**
	 * 设置：操作后的状态
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：操作后的状态
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：操作时间
	 */
	public void setOperatorTime(Date operatorTime) {
		this.operatorTime = operatorTime;
	}
	/**
	 * 获取：操作时间
	 */
	public Date getOperatorTime() {
		return operatorTime;
	}
	/**
	 * 设置：操作者角色ID
	 */
	public void setOperatorUserId(String operatorUserId) {
		this.operatorUserId = operatorUserId;
	}
	/**
	 * 获取：操作者角色ID
	 */
	public String getOperatorUserId() {
		return operatorUserId;
	}
	/**
	 * 设置：操作者名字
	 */
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	/**
	 * 获取：操作者名字
	 */
	public String getOperatorName() {
		return operatorName;
	}
	/**
	 * 设置：操作备注信息
	 */
	public void setOperatorDescribe(String operatorDescribe) {
		this.operatorDescribe = operatorDescribe;
	}
	/**
	 * 获取：操作备注信息
	 */
	public String getOperatorDescribe() {
		return operatorDescribe;
	}
}
