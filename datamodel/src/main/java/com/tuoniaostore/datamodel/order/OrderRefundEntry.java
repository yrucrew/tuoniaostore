package com.tuoniaostore.datamodel.order;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;

public class OrderRefundEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    // 退款单id
    private String id = DefineRandom.getUUID();
    // 原采购单号
    private String oldSequenceNumber;
    // 父单号
    private String sequenceNumber;
    // 自身单号
    private String orderSequenceNumber;
    // 采购单id
    private String orderId;
    // 订单类型 1-供应链采购订单[子单]、2-供应链调拨订单、8-供应链销售订单[父单]、16-超市POS机订单（包括超市扫码订单）、32、外卖配送订单 64增值服务订单、
    private Integer type;
    // 退单用户id
    private String partnerUserId;
    // 退单用户手机号
    private String partnerUserPhone;
    // 退款金额
    private Long refundPrice;
    // 退款原因
    private String reason;
    // 退款备注
    private String mark;
    // 退款图片
    private String img;
    // 退款状态
    private int status;
    // 申请时间
    private Date createTime;

    public String getOldSequenceNumber() {
        return oldSequenceNumber;
    }

    public void setOldSequenceNumber(String oldSequenceNumber) {
        this.oldSequenceNumber = oldSequenceNumber;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getOrderSequenceNumber() {
        return orderSequenceNumber;
    }

    public void setOrderSequenceNumber(String orderSequenceNumber) {
        this.orderSequenceNumber = orderSequenceNumber;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPartnerUserId() {
        return partnerUserId;
    }

    public void setPartnerUserId(String partnerUserId) {
        this.partnerUserId = partnerUserId;
    }

    public String getPartnerUserPhone() {
        return partnerUserPhone;
    }

    public void setPartnerUserPhone(String partnerUserPhone) {
        this.partnerUserPhone = partnerUserPhone;
    }

    public Long getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(Long refundPrice) {
        this.refundPrice = refundPrice;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
