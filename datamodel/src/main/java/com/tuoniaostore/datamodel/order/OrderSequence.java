package com.tuoniaostore.datamodel.order;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 订单序号（下面存放着所有订单）
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public class OrderSequence implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//合作商户号ID
	private String partnerId;
	//合作商户下级用户信息
	private String partnerUserId;
	//预创建订单序列号
	private String sequenceNumber;
	//申请的类型
	private Integer type;
	//状态 0 - 可用 其他值为不可用
	private Integer status;
	//建立时间
	private Date createTime;
	//有效期，单位：秒
	private Integer validityTermSecond;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：合作商户号ID
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	/**
	 * 获取：合作商户号ID
	 */
	public String getPartnerId() {
		return partnerId;
	}
	/**
	 * 设置：合作商户下级用户信息
	 */
	public void setPartnerUserId(String partnerUserId) {
		this.partnerUserId = partnerUserId;
	}
	/**
	 * 获取：合作商户下级用户信息
	 */
	public String getPartnerUserId() {
		return partnerUserId;
	}
	/**
	 * 设置：预创建订单序列号
	 */
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	/**
	 * 获取：预创建订单序列号
	 */
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	/**
	 * 设置：申请的类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：申请的类型
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：状态 0 - 可用 其他值为不可用
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态 0 - 可用 其他值为不可用
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：有效期，单位：秒
	 */
	public void setValidityTermSecond(Integer validityTermSecond) {
		this.validityTermSecond = validityTermSecond;
	}
	/**
	 * 获取：有效期，单位：秒
	 */
	public Integer getValidityTermSecond() {
		return validityTermSecond;
	}
}
