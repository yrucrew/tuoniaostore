package com.tuoniaostore.datamodel.order;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public class OrderStatusListener implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//合作商家的商户号
	private String partnerId;
	//监听类型
	private Integer eventType;
	//状态：0 -- 不启用 1 -- 启用
	private Integer status;
	//回调地址
	private String callbackUrl;
	//建立时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：合作商家的商户号
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	/**
	 * 获取：合作商家的商户号
	 */
	public String getPartnerId() {
		return partnerId;
	}
	/**
	 * 设置：监听类型
	 */
	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}
	/**
	 * 获取：监听类型
	 */
	public Integer getEventType() {
		return eventType;
	}
	/**
	 * 设置：状态：0 -- 不启用 1 -- 启用
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态：0 -- 不启用 1 -- 启用
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：回调地址
	 */
	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}
	/**
	 * 获取：回调地址
	 */
	public String getCallbackUrl() {
		return callbackUrl;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
