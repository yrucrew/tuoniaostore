package com.tuoniaostore.datamodel.order;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 订单的推送日志
与未接订单日志的区别为该表不物理删除数据 可以查询订单是否曾经已执行推送 且明白推送的目标
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:16:15
 */
public class OrderPushedLogger implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//订单ID
	private String orderId;
	//目标ID
	private String targetUserId;
	//推送类型
	private Integer pushType;
	//推送的标题
	private String pushTitle;
	//推送的内容
	private String pushMsgContent;
	//推送结果内容(即时)
	private String pushResult;
	//推送时间(我方)
	private Date pushTime;
	//推送状态(我方)
	private Integer status;
	//返回消息内容
	private String feedbackMsgContent;
	//返回的推送状态
	private Integer feedbackStatus;
	//返回时间
	private Date feedbackTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：订单ID
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：订单ID
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * 设置：目标ID
	 */
	public void setTargetUserId(String targetUserId) {
		this.targetUserId = targetUserId;
	}
	/**
	 * 获取：目标ID
	 */
	public String getTargetUserId() {
		return targetUserId;
	}
	/**
	 * 设置：推送类型
	 */
	public void setPushType(Integer pushType) {
		this.pushType = pushType;
	}
	/**
	 * 获取：推送类型
	 */
	public Integer getPushType() {
		return pushType;
	}
	/**
	 * 设置：推送的标题
	 */
	public void setPushTitle(String pushTitle) {
		this.pushTitle = pushTitle;
	}
	/**
	 * 获取：推送的标题
	 */
	public String getPushTitle() {
		return pushTitle;
	}
	/**
	 * 设置：推送的内容
	 */
	public void setPushMsgContent(String pushMsgContent) {
		this.pushMsgContent = pushMsgContent;
	}
	/**
	 * 获取：推送的内容
	 */
	public String getPushMsgContent() {
		return pushMsgContent;
	}
	/**
	 * 设置：推送结果内容(即时)
	 */
	public void setPushResult(String pushResult) {
		this.pushResult = pushResult;
	}
	/**
	 * 获取：推送结果内容(即时)
	 */
	public String getPushResult() {
		return pushResult;
	}
	/**
	 * 设置：推送时间(我方)
	 */
	public void setPushTime(Date pushTime) {
		this.pushTime = pushTime;
	}
	/**
	 * 获取：推送时间(我方)
	 */
	public Date getPushTime() {
		return pushTime;
	}
	/**
	 * 设置：推送状态(我方)
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：推送状态(我方)
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：返回消息内容
	 */
	public void setFeedbackMsgContent(String feedbackMsgContent) {
		this.feedbackMsgContent = feedbackMsgContent;
	}
	/**
	 * 获取：返回消息内容
	 */
	public String getFeedbackMsgContent() {
		return feedbackMsgContent;
	}
	/**
	 * 设置：返回的推送状态
	 */
	public void setFeedbackStatus(Integer feedbackStatus) {
		this.feedbackStatus = feedbackStatus;
	}
	/**
	 * 获取：返回的推送状态
	 */
	public Integer getFeedbackStatus() {
		return feedbackStatus;
	}
	/**
	 * 设置：返回时间
	 */
	public void setFeedbackTime(Date feedbackTime) {
		this.feedbackTime = feedbackTime;
	}
	/**
	 * 获取：返回时间
	 */
	public Date getFeedbackTime() {
		return feedbackTime;
	}
}
