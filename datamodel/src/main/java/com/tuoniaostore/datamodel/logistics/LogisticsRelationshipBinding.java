package com.tuoniaostore.datamodel.logistics;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
public class LogisticsRelationshipBinding implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//关系主人方标识
	private String hostUserId;
	//关系客人方标识
	private String guestUserId;
	//关系类型 逻辑设定
	private Integer type;
	//关系状态 0无效 1有效 其他值由逻辑决定
	private Integer status;
	//建立时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：关系主人方标识
	 */
	public void setHostUserId(String hostUserId) {
		this.hostUserId = hostUserId;
	}
	/**
	 * 获取：关系主人方标识
	 */
	public String getHostUserId() {
		return hostUserId;
	}
	/**
	 * 设置：关系客人方标识
	 */
	public void setGuestUserId(String guestUserId) {
		this.guestUserId = guestUserId;
	}
	/**
	 * 获取：关系客人方标识
	 */
	public String getGuestUserId() {
		return guestUserId;
	}
	/**
	 * 设置：关系类型 逻辑设定
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：关系类型 逻辑设定
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：关系状态 0无效 1有效 其他值由逻辑决定
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：关系状态 0无效 1有效 其他值由逻辑决定
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
