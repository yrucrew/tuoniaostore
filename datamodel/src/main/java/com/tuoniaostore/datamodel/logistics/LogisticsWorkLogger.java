package com.tuoniaostore.datamodel.logistics;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
public class LogisticsWorkLogger implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//用户通行证ID
	private String userId;
	//打卡类型
	private Integer clockType;
	//备注
	private String remark;
	//打卡位置
	private String location;
	//打卡地址
	private String address;
	//与正常上班地址的距离，单位：米
	private Long clockDistance;
	//打卡时间
	private Date clockTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：用户通行证ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户通行证ID
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：打卡类型
	 */
	public void setClockType(Integer clockType) {
		this.clockType = clockType;
	}
	/**
	 * 获取：打卡类型
	 */
	public Integer getClockType() {
		return clockType;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：打卡位置
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * 获取：打卡位置
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * 设置：打卡地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：打卡地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：与正常上班地址的距离，单位：米
	 */
	public void setClockDistance(Long clockDistance) {
		this.clockDistance = clockDistance;
	}
	/**
	 * 获取：与正常上班地址的距离，单位：米
	 */
	public Long getClockDistance() {
		return clockDistance;
	}
	/**
	 * 设置：打卡时间
	 */
	public void setClockTime(Date clockTime) {
		this.clockTime = clockTime;
	}
	/**
	 * 获取：打卡时间
	 */
	public Date getClockTime() {
		return clockTime;
	}
}
