package com.tuoniaostore.datamodel.logistics;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;


/**
 * 物流车类型
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
public class LogisticsCarType implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private String id = DefineRandom.getUUID();
    //车型类型值 0表示单车
    private Integer type;
    //标题(用于展示)
    private String title;
    //图片链接
    private String image;
    //简介
    private String introduction;

    /**
     * 设置：
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public String getId() {
        return id;
    }

    /**
     * 设置：车型类型值 0表示单车
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取：车型类型值 0表示单车
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置：标题(用于展示)
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取：标题(用于展示)
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置：图片链接
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 获取：图片链接
     */
    public String getImage() {
        return image;
    }

    /**
     * 设置：简介
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    /**
     * 获取：简介
     */
    public String getIntroduction() {
        return introduction;
    }
}
