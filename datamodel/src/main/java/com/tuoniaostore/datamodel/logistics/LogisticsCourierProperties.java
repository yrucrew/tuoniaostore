package com.tuoniaostore.datamodel.logistics;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 物流人员属性
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:43:59
 */
public class LogisticsCourierProperties implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//归属角色ID
	private String userId;
	//工作类型 0：全职 1：兼职
	private Integer workType;
	//工作地址中心位置经纬度，格式：longitude,latitude
	private String workLocation;
	//车型 -1时表示步行
	private Integer carType;
	//上班时间
	private Date timeToWork;
	//下班时间
	private Date timeOffWork;
	//当处于上班时间时 是否接受订单推送 0时不接受
	private Integer acceptStatus;
	//最后所在位置
	private String lastLocation;
	//配送范围 单位：米
	private Integer deliveryRange;
	//建立时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：归属角色ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：归属角色ID
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：工作类型 0：全职 1：兼职
	 */
	public void setWorkType(Integer workType) {
		this.workType = workType;
	}
	/**
	 * 获取：工作类型 0：全职 1：兼职
	 */
	public Integer getWorkType() {
		return workType;
	}
	/**
	 * 设置：工作地址中心位置经纬度，格式：longitude,latitude
	 */
	public void setWorkLocation(String workLocation) {
		this.workLocation = workLocation;
	}
	/**
	 * 获取：工作地址中心位置经纬度，格式：longitude,latitude
	 */
	public String getWorkLocation() {
		return workLocation;
	}
	/**
	 * 设置：车型 -1时表示步行
	 */
	public void setCarType(Integer carType) {
		this.carType = carType;
	}
	/**
	 * 获取：车型 -1时表示步行
	 */
	public Integer getCarType() {
		return carType;
	}
	/**
	 * 设置：上班时间
	 */
	public void setTimeToWork(Date timeToWork) {
		this.timeToWork = timeToWork;
	}
	/**
	 * 获取：上班时间
	 */
	public Date getTimeToWork() {
		return timeToWork;
	}
	/**
	 * 设置：下班时间
	 */
	public void setTimeOffWork(Date timeOffWork) {
		this.timeOffWork = timeOffWork;
	}
	/**
	 * 获取：下班时间
	 */
	public Date getTimeOffWork() {
		return timeOffWork;
	}
	/**
	 * 设置：当处于上班时间时 是否接受订单推送 0时不接受
	 */
	public void setAcceptStatus(Integer acceptStatus) {
		this.acceptStatus = acceptStatus;
	}
	/**
	 * 获取：当处于上班时间时 是否接受订单推送 0时不接受
	 */
	public Integer getAcceptStatus() {
		return acceptStatus;
	}
	/**
	 * 设置：最后所在位置
	 */
	public void setLastLocation(String lastLocation) {
		this.lastLocation = lastLocation;
	}
	/**
	 * 获取：最后所在位置
	 */
	public String getLastLocation() {
		return lastLocation;
	}
	/**
	 * 设置：配送范围 单位：米
	 */
	public void setDeliveryRange(Integer deliveryRange) {
		this.deliveryRange = deliveryRange;
	}
	/**
	 * 获取：配送范围 单位：米
	 */
	public Integer getDeliveryRange() {
		return deliveryRange;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
