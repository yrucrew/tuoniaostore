package com.tuoniaostore.datamodel.good;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;


/**
 * 商品单位
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public class GoodUnit implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private String id = DefineRandom.getUUID();
    //单位标题
    private String title;
    //状态 0-不启动 1启动
    private Integer status;
    //
    private Date createTime;
    //
    private Integer sort;
    //创建人
    private String userId;
    private String userName;
    //
    private String remarks;
    //
    private Integer retrieveStatus;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 设置：
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public String getId() {
        return id;
    }

    /**
     * 设置：单位标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取：单位标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置：状态 0-不启动 1启动
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取：状态 0-不启动 1启动
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置：
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取：
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置：
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 获取：
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * 设置：创建人
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 获取：创建人
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置：
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取：
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * 设置：
     */
    public void setRetrieveStatus(Integer retrieveStatus) {
        this.retrieveStatus = retrieveStatus;
    }

    /**
     * 获取：
     */
    public Integer getRetrieveStatus() {
        return retrieveStatus;
    }
}
