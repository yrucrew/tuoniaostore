package com.tuoniaostore.datamodel.good;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 商品模板属性具体值
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public class GoodTemplatePropertiesValue implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//
	private String itemTemplateId;
	//
	private String itemTemplatePropertiesId;
	//
	private String content;
	//
	private Integer sort;
	//创建人
	private String userId;
	//
	private Integer status;
	//
	private String remark;
	//
		private Integer retrieveStatus;
	//
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}

	public String getItemTemplateId() {
		return itemTemplateId;
	}

	public void setItemTemplateId(String itemTemplateId) {
		this.itemTemplateId = itemTemplateId;
	}

	public String getItemTemplatePropertiesId() {
		return itemTemplatePropertiesId;
	}

	public void setItemTemplatePropertiesId(String itemTemplatePropertiesId) {
		this.itemTemplatePropertiesId = itemTemplatePropertiesId;
	}

	/**
	 * 设置：
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：创建人
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：创建人
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：
	 */
	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}
	/**
	 * 获取：
	 */
	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
