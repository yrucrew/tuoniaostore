package com.tuoniaostore.datamodel.good;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 商品类型
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public class GoodType implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//商品类型标题
	private String title;
	//父级ID (归属上级)
	private String parentId;
	//状态 0-可用 1-提交审核 2-审核不通过 3-失效
	private Integer status;
	//排序 值小的靠上
	private Integer sort;
	//是否置顶 0表示不置顶
	private Integer top;
	//icon图片链接
	private String icon;
	//展示的图片
	private String image;
	//创建人
	private String userId;
	private String parentName;
	private String userName;
	//
	private String remark;
	//
	private Integer retrieveStatus;
	//
	private Date createTime;

	private Integer type;

	private Boolean CheakFlag;



	public Boolean getCheakFlag() {
		return CheakFlag;
	}

	public void setCheakFlag(Boolean cheakFlag) {
		CheakFlag = cheakFlag;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：商品类型标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：商品类型标题
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：父级ID (归属上级)
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	/**
	 * 获取：父级ID (归属上级)
	 */
	public String getParentId() {
		return parentId;
	}
	/**
	 * 设置：状态 0-可用 1-提交审核 2-审核不通过 3-失效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态 0-可用 1-提交审核 2-审核不通过 3-失效
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：排序 值小的靠上
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序 值小的靠上
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：是否置顶 0表示不置顶
	 */
	public void setTop(Integer top) {
		this.top = top;
	}
	/**
	 * 获取：是否置顶 0表示不置顶
	 */
	public Integer getTop() {
		return top;
	}
	/**
	 * 设置：icon图片链接
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	/**
	 * 获取：icon图片链接
	 */
	public String getIcon() {
		return icon;
	}
	/**
	 * 设置：展示的图片
	 */
	public void setImage(String image) {
		this.image = image;
	}
	/**
	 * 获取：展示的图片
	 */
	public String getImage() {
		return image;
	}
	/**
	 * 设置：创建人
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：创建人
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：
	 */
	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}
	/**
	 * 获取：
	 */
	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
