package com.tuoniaostore.datamodel.good;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 商品价格
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public class GoodPrice implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//价格ID
	private String id= DefineRandom.getUUID();
	//标签名称
	private String title;

	//首字符
	private String firstLetter;
	//全拼
	private String allLetter;

	//模板名称全拼
	private String templateFirstLetter;
	//模板名称首字符
	private String templateAllLetter;

	//成本价
	private Long costPrice;
	//建议售价
	private Long salePrice;
	//交易价
	private Long sellPrice;
	//同行价
	private Long colleaguePrice;
	//市场价
	private Long marketPrice;
	//会员价
	private Long memberPrice;
	//促销价
	private Long discountPrice;
	//备注
	private String remark;
	//商品ID
	private String goodId;
	private String goodName;
	//数量
	private Integer num;
	//
	private String userId;
	private String userName;
	//
	private Date createTime;
	//排序
	private Integer sort;
	//小图标
	private String icon;
	//单位名称
	private String unitId;
	private String unitName;
	private String barcode;

	private String boxBarcode;//箱码

	//默认选中项
	private Integer top;
	//0.可用1.禁用
	private Integer status;
	//回收站 0正常 1删除
	private Integer retrieveStatus;

	public String getBoxBarcode() {
		return boxBarcode;
	}

	public void setBoxBarcode(String boxBarcode) {
		this.boxBarcode = boxBarcode;
	}

	public String getTemplateFirstLetter() {
		return templateFirstLetter;
	}

	public void setTemplateFirstLetter(String templateFirstLetter) {
		this.templateFirstLetter = templateFirstLetter;
	}

	public String getTemplateAllLetter() {
		return templateAllLetter;
	}

	public void setTemplateAllLetter(String templateAllLetter) {
		this.templateAllLetter = templateAllLetter;
	}

	public String getFirstLetter() {
		return firstLetter;
	}

	public void setFirstLetter(String firstLetter) {
		this.firstLetter = firstLetter;
	}

	public String getAllLetter() {
		return allLetter;
	}

	public void setAllLetter(String allLetter) {
		this.allLetter = allLetter;
	}

	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}

	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getGoodName() {
		return goodName;
	}

	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 设置：价格ID
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：价格ID
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：标签名称
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：标签名称
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：成本价
	 */
	public void setCostPrice(Long costPrice) {
		this.costPrice = costPrice;
	}
	/**
	 * 获取：成本价
	 */
	public Long getCostPrice() {
		return costPrice;
	}
	/**
	 * 设置：建议售价
	 */
	public void setSalePrice(Long salePrice) {
		this.salePrice = salePrice;
	}
	/**
	 * 获取：建议售价
	 */
	public Long getSalePrice() {
		return salePrice;
	}
	/**
	 * 设置：交易价
	 */
	public void setSellPrice(Long sellPrice) {
		this.sellPrice = sellPrice;
	}
	/**
	 * 获取：交易价
	 */
	public Long getSellPrice() {
		return sellPrice;
	}
	/**
	 * 设置：同行价
	 */
	public void setColleaguePrice(Long colleaguePrice) {
		this.colleaguePrice = colleaguePrice;
	}
	/**
	 * 获取：同行价
	 */
	public Long getColleaguePrice() {
		return colleaguePrice;
	}
	/**
	 * 设置：市场价
	 */
	public void setMarketPrice(Long marketPrice) {
		this.marketPrice = marketPrice;
	}
	/**
	 * 获取：市场价
	 */
	public Long getMarketPrice() {
		return marketPrice;
	}
	/**
	 * 设置：会员价
	 */
	public void setMemberPrice(Long memberPrice) {
		this.memberPrice = memberPrice;
	}
	/**
	 * 获取：会员价
	 */
	public Long getMemberPrice() {
		return memberPrice;
	}
	/**
	 * 设置：促销价
	 */
	public void setDiscountPrice(Long discountPrice) {
		this.discountPrice = discountPrice;
	}
	/**
	 * 获取：促销价
	 */
	public Long getDiscountPrice() {
		return discountPrice;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：商品ID
	 */
	public void setGoodId(String goodId) {
		this.goodId = goodId;
	}
	/**
	 * 获取：商品ID
	 */
	public String getGoodId() {
		return goodId;
	}
	/**
	 * 设置：数量
	 */
	public void setNum(Integer num) {
		this.num = num;
	}
	/**
	 * 获取：数量
	 */
	public Integer getNum() {
		return num;
	}
	/**
	 * 设置：
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：小图标
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	/**
	 * 获取：小图标
	 */
	public String getIcon() {
		return icon;
	}
	/**
	 * 设置：单位名称
	 */
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	/**
	 * 获取：单位名称
	 */
	public String getUnitId() {
		return unitId;
	}
	/**
	 * 设置：默认选中项
	 */
	public void setTop(Integer top) {
		this.top = top;
	}
	/**
	 * 获取：默认选中项
	 */
	public Integer getTop() {
		return top;
	}
	/**
	 * 设置：0.可用1.禁用
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：0.可用1.禁用
	 */
	public Integer getStatus() {
		return status;
	}
}
