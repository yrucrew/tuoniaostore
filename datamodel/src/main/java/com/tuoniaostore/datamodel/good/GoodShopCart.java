package com.tuoniaostore.datamodel.good;

import com.tuoniaostore.datamodel.DefineRandom;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/***
 * 购物车
 * @author sqd
 * @date 2019/4/3
 * @param 0
 * @return
 */
public class GoodShopCart implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id=DefineRandom.getUUID();
    private String goodId;
    private String userId;
    private String sequenceNumber;
    private Date createTime;
    private Integer status ;
    private Integer goodNumber;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getGoodNumber() {
        return goodNumber;
    }

    public void setGoodNumber(Integer goodNumber) {
        this.goodNumber = goodNumber;
    }
}
