package com.tuoniaostore.datamodel.good;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;


/**
 * 商品归属品牌
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public class GoodBrand implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private String id = DefineRandom.getUUID();
    //类型
    private Integer type;
    //品牌名字
    private String name;
    //商标图
    private String logo;
    //简介
    private String introduce;
    private Integer status;
    //创建时间
    private Date createTime;
    //用户名
    private String userId;
    private String userName;
    //排序
    private Integer sort;
    //备注
    private String remark;
    //回收站状态 0正常 1删除
    private Integer retrieveStatus;

    private boolean cheakFlag;//是否已选标记 推荐品牌用的  不存数据库

    public boolean isCheakFlag() {
        return cheakFlag;
    }

    public void setCheakFlag(boolean cheakFlag) {
        this.cheakFlag = cheakFlag;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 设置：
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public String getId() {
        return id;
    }

    /**
     * 设置：类型
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取：类型
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置：品牌名字
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取：品牌名字
     */
    public String getName() {
        return name;
    }

    /**
     * 设置：商标图
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * 获取：商标图
     */
    public String getLogo() {
        return logo;
    }

    /**
     * 设置：简介
     */
    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    /**
     * 获取：简介
     */
    public String getIntroduce() {
        return introduce;
    }

    /**
     * 设置：{
     * "title": "状态",
     * "enum": [{
     * "value": 0,
     * "title": "可用"
     * }, {
     * "value": 1,
     * "title": "提交审核"
     * }, {
     * "value": 2,
     * "title": "审核不通过"
     * }, {
     * "value": 2,
     * "title": "失效"
     * }]
     * }
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取：{
     * "title": "状态",
     * "enum": [{
     * "value": 0,
     * "title": "可用"
     * }, {
     * "value": 1,
     * "title": "提交审核"
     * }, {
     * "value": 2,
     * "title": "审核不通过"
     * }, {
     * "value": 2,
     * "title": "失效"
     * }]
     * }
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置：创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取：创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置：用户名
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 获取：用户名
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置：排序
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 获取：排序
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * 设置：备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取：备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置：
     */
    public void setRetrieveStatus(Integer retrieveStatus) {
        this.retrieveStatus = retrieveStatus;
    }

    /**
     * 获取：
     */
    public Integer getRetrieveStatus() {
        return retrieveStatus;
    }
}
