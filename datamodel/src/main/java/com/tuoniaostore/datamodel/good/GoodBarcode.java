package com.tuoniaostore.datamodel.good;

import com.tuoniaostore.datamodel.DefineRandom;
import sun.applet.Main;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * 
 *  商品条形码
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public class GoodBarcode implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//条码ID
	private String id= DefineRandom.getUUID();
	//条码
	private String barcode;
	//价格ID
	private String priceId;
	//父条码Id
	private String barcodeParentId;
	//创建人ID
	private String userId;
	//
	private Date createTime;

	private String goodName;

	public String getGoodName() {
		return goodName;
	}

	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}

	/**
	 * 设置：条码ID
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：条码ID
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：条码
	 */
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	/**
	 * 获取：条码
	 */
	public String getBarcode() {
		return barcode;
	}
	/**
	 * 设置：价格ID
	 */
	public void setPriceId(String priceId) {
		this.priceId = priceId;
	}
	/**
	 * 获取：价格ID
	 */
	public String getPriceId() {
		return priceId;
	}
	/**
	 * 设置：父条码Id
	 */
	public void setBarcodeParentId(String barcodeParentId) {
		this.barcodeParentId = barcodeParentId;
	}
	/**
	 * 获取：父条码Id
	 */
	public String getBarcodeParentId() {
		return barcodeParentId;
	}
	/**
	 * 设置：添加用户ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：添加用户ID
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
