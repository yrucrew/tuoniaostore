package com.tuoniaostore.datamodel.good;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 
 * 商品模板
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 17:06:04
 */
public class GoodTemplate implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//商品名字
	private String name;

	//首字符
	private String firstLetter;

	//全拼
	private String allLetter;

	//
	private String bannerUrl;
	//品牌ID
	private String brandId;
	private String brandName;
	//归属类型
	private String typeId;
	private String typeName;
	//0.普通1.价格
	private Integer priceType;
	//状态 0-可用 1-提交审核 2-审核不通过 3-失效
	private Integer status;
	//
	private String imageUrl;
	//商品描述
	private String description;
	//上传时间
	private Date createTime;
	//保质期(天)
	private Integer shelfLife;
	//
	private Integer sort;
	private int lableCount;
	private int imageCount;
	private String lableContent;
	//创建人
	private String userId;
	private String userName;
	//
	private String remark;

	//用于接收数据 goodPrice
	private List<GoodPrice> list;

	public String getFirstLetter() {
		return firstLetter;
	}

	public void setFirstLetter(String firstLetter) {
		this.firstLetter = firstLetter;
	}

	public String getAllLetter() {
		return allLetter;
	}

	public void setAllLetter(String allLetter) {
		this.allLetter = allLetter;
	}

	public List<GoodPrice> getList() {
		return list;
	}

	public void setList(List<GoodPrice> list) {
		this.list = list;
	}

	public int getImageCount() {
		return imageCount;
	}

	public void setImageCount(int imageCount) {
		this.imageCount = imageCount;
	}

	//
	private Integer retrieveStatus;

	public String getLableContent() {
		return lableContent;
	}

	public void setLableContent(String lableContent) {
		this.lableContent = lableContent;
	}

	public int getLableCount() {
		return lableCount;
	}

	public void setLableCount(int lableCount) {
		this.lableCount = lableCount;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：商品名字
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：商品名字
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：
	 */
	public void setBannerUrl(String bannerUrl) {
		this.bannerUrl = bannerUrl;
	}
	/**
	 * 获取：
	 */
	public String getBannerUrl() {
		return bannerUrl;
	}
	/**
	 * 设置：品牌ID
	 */
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	/**
	 * 获取：品牌ID
	 */
	public String getBrandId() {
		return brandId;
	}
	/**
	 * 设置：归属类型
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	/**
	 * 获取：归属类型
	 */
	public String getTypeId() {
		return typeId;
	}
	/**
	 * 设置：0.普通1.价格
	 */
	public void setPriceType(Integer priceType) {
		this.priceType = priceType;
	}
	/**
	 * 获取：0.普通1.价格
	 */
	public Integer getPriceType() {
		return priceType;
	}
	/**
	 * 设置：状态 0-可用 1-提交审核 2-审核不通过 3-失效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态 0-可用 1-提交审核 2-审核不通过 3-失效
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	/**
	 * 获取：
	 */
	public String getImageUrl() {
		return imageUrl;
	}
	/**
	 * 设置：商品描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * 获取：商品描述
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 设置：上传时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：上传时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：保质期(天)
	 */
	public void setShelfLife(Integer shelfLife) {
		this.shelfLife = shelfLife;
	}
	/**
	 * 获取：保质期(天)
	 */
	public Integer getShelfLife() {
		return shelfLife;
	}
	/**
	 * 设置：
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：创建人
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：创建人
	 */
	public String getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 设置：
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：
	 */
	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}
	/**
	 * 获取：
	 */
	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}
}
