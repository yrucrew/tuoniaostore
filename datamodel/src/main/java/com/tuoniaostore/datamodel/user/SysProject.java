package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
public class SysProject implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//项目ID
	private String id= DefineRandom.getUUID();
	//项目名称
	private String name;
	private String parentId;
	private String parentName;
	//
	private Integer sort;
	//项目logo
	private String logo;
	//项目封面
	private String imgurl;

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	/**
	 * 设置：项目ID
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：项目ID
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：项目名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：项目名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：项目logo
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}
	/**
	 * 获取：项目logo
	 */
	public String getLogo() {
		return logo;
	}
	/**
	 * 设置：项目封面
	 */
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}
	/**
	 * 获取：项目封面
	 */
	public String getImgurl() {
		return imgurl;
	}
}
