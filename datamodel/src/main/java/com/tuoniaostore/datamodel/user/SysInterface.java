package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
public class SysInterface implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//接口名称
	private String name;
	//项目ID
	private String projectId;
	private String projectName;
	//接口访问地址
	private String url;
	//0.开发1.测试 2.上线 3.禁用
	private Integer status;
	//
	private String version;
	//排序
	private Integer sort;
	//返回类型0.application/json 1.text/html 2.x-application 3.application/xml
	private Integer returnType;
	//错误列表	
	private String errorList;
	//接口说明
	private String explain;
	//返回值
	private String response;
	private String inParam;
	private String outParam;
	//
	private String remark;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	public String getInParam() {
		return inParam;
	}

	public void setInParam(String inParam) {
		this.inParam = inParam;
	}

	public String getOutParam() {
		return outParam;
	}

	public void setOutParam(String outParam) {
		this.outParam = outParam;
	}
	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：接口名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：接口名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：项目ID
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	/**
	 * 获取：项目ID
	 */
	public String getProjectId() {
		return projectId;
	}
	/**
	 * 设置：接口访问地址
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * 获取：接口访问地址
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * 设置：0.开发1.测试 2.上线 3.禁用
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：0.开发1.测试 2.上线 3.禁用
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	/**
	 * 获取：
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：返回类型0.application/json 1.text/html 2.x-application 3.application/xml
	 */
	public void setReturnType(Integer returnType) {
		this.returnType = returnType;
	}
	/**
	 * 获取：返回类型0.application/json 1.text/html 2.x-application 3.application/xml
	 */
	public Integer getReturnType() {
		return returnType;
	}
	/**
	 * 设置：错误列表	
	 */
	public void setErrorList(String errorList) {
		this.errorList = errorList;
	}
	/**
	 * 获取：错误列表	
	 */
	public String getErrorList() {
		return errorList;
	}
	/**
	 * 设置：接口说明
	 */
	public void setExplain(String explain) {
		this.explain = explain;
	}
	/**
	 * 获取：接口说明
	 */
	public String getExplain() {
		return explain;
	}
	/**
	 * 设置：返回值
	 */
	public void setResponse(String response) {
		this.response = response;
	}
	/**
	 * 获取：返回值
	 */
	public String getResponse() {
		return response;
	}
	/**
	 * 设置：
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：
	 */
	public String getRemark() {
		return remark;
	}
}
