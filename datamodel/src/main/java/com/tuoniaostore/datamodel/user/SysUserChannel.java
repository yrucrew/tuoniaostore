package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 合作渠道，用于区分来自不同渠道的用户数据
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public class SysUserChannel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//渠道名
	private String name;
	//渠道类型，由逻辑决定；暂时为1
	private Integer type;
	//状态 0可用,1禁用
	private Integer status;
	//
	private Date createTime;
	//
	private Integer retrieveStatus;
	//
	private Integer sort;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：渠道名
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：渠道名
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：渠道类型，由逻辑决定；暂时为1
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：渠道类型，由逻辑决定；暂时为1
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：状态 0可用,1禁用
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态 0可用,1禁用
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}
	/**
	 * 获取：
	 */
	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}
	/**
	 * 设置：
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：
	 */
	public Integer getSort() {
		return sort;
	}
}
