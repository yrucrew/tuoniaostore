package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
public class SysAreaProvince implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//区域Id
	private String areaId;
	//省份ID
	private String cityId;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：区域Id
	 */
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	/**
	 * 获取：区域Id
	 */
	public String getAreaId() {
		return areaId;
	}
	/**
	 * 设置：省份ID
	 */
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	/**
	 * 获取：省份ID
	 */
	public String getCityId() {
		return cityId;
	}
}
