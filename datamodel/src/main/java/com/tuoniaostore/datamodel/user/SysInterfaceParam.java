package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-26 13:41:45
 */
public class SysInterfaceParam implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//参数名称
	private String name;
	//接口
	private String interfaceId;
	//
	private String dataType;
	//0.选填 1.必填
	private int fillType;
	//默认值
	private String defaultValue;
	//备注
	private String remark;
	//0.入参 1.输入字段说明
	private int type;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：参数名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：参数名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：接口
	 */
	public void setInterfaceId(String interfaceId) {
		this.interfaceId = interfaceId;
	}
	/**
	 * 获取：接口
	 */
	public String getInterfaceId() {
		return interfaceId;
	}
	/**
	 * 设置：
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	/**
	 * 获取：
	 */
	public String getDataType() {
		return dataType;
	}
	/**
	 * 设置：0.选填 1.必填
	 */
	public int getFillType() {
		return fillType;
	}

	public void setFillType(int fillType) {
		this.fillType = fillType;
	}

	/**
	 * 设置：默认值
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	/**
	 * 获取：默认值
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：0.入参 1.输入字段说明
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * 获取：0.入参 1.输入字段说明
	 */
	public int getType() {
		return type;
	}
}
