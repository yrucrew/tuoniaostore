package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public class SysLog implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//
	private String tableName;
	//
	private String tableField;
	//
	private String beforeContent;
	//
	private String afterContent;
	//
	private Date createTime;
	//
	private String userId;
	//
	private String remarks;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	/**
	 * 获取：
	 */
	public String getTableName() {
		return tableName;
	}
	/**
	 * 设置：
	 */
	public void setTableField(String tableField) {
		this.tableField = tableField;
	}
	/**
	 * 获取：
	 */
	public String getTableField() {
		return tableField;
	}
	/**
	 * 设置：
	 */
	public void setBeforeContent(String beforeContent) {
		this.beforeContent = beforeContent;
	}
	/**
	 * 获取：
	 */
	public String getBeforeContent() {
		return beforeContent;
	}
	/**
	 * 设置：
	 */
	public void setAfterContent(String afterContent) {
		this.afterContent = afterContent;
	}
	/**
	 * 获取：
	 */
	public String getAfterContent() {
		return afterContent;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * 设置：
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	/**
	 * 获取：
	 */
	public String getRemarks() {
		return remarks;
	}
}
