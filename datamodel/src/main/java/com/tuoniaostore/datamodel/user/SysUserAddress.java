package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 用户地址
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public class SysUserAddress implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//id
	private String id= DefineRandom.getUUID();
	//通行证ID
	private String userId;
	//用户名字
	private String userName;
	//地址别名
	private String addressAlias;
	//地址联系人姓名
	private String name;
	//联系人电话
	private String phoneNumber;
	//国家
	private String country;
	//省份
	private String province;
	//城市
	private String city;
	//行政区域
	private String district;
	//街道名
	private String street;
	//街道号
	private String streetNum;
	//对应百度地图API的displayName(name)
	private String displayName;
	//详细地址
	private String detailAddress;
	//城市编码(国标)
	private String adcode;
	//经度
	private Double longitude;
	//纬度
	private Double latitude;
	//类型；0-收货地址；1-开拓签约地址
	private Integer type;
	//状态，0-启用；1-失效；2-默认
	private Integer status;
	//建立时间
	private Date createTime;
	//0-女性；1-男性
	private Integer sex;
	//排序
	private Integer sort;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：通行证ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：通行证ID
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：地址别名
	 */
	public void setAddressAlias(String addressAlias) {
		this.addressAlias = addressAlias;
	}
	/**
	 * 获取：地址别名
	 */
	public String getAddressAlias() {
		return addressAlias;
	}
	/**
	 * 设置：地址联系人姓名
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：地址联系人姓名
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：联系人电话
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * 获取：联系人电话
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * 设置：国家
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * 获取：国家
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * 设置：省份
	 */
	public void setProvince(String province) {
		this.province = province;
	}
	/**
	 * 获取：省份
	 */
	public String getProvince() {
		return province;
	}
	/**
	 * 设置：城市
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * 获取：城市
	 */
	public String getCity() {
		return city;
	}
	/**
	 * 设置：行政区域
	 */
	public void setDistrict(String district) {
		this.district = district;
	}
	/**
	 * 获取：行政区域
	 */
	public String getDistrict() {
		return district;
	}
	/**
	 * 设置：街道名
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	/**
	 * 获取：街道名
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * 设置：街道号
	 */
	public void setStreetNum(String streetNum) {
		this.streetNum = streetNum;
	}
	/**
	 * 获取：街道号
	 */
	public String getStreetNum() {
		return streetNum;
	}
	/**
	 * 设置：对应百度地图API的displayName(name)
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	/**
	 * 获取：对应百度地图API的displayName(name)
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * 设置：详细地址
	 */
	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}
	/**
	 * 获取：详细地址
	 */
	public String getDetailAddress() {
		return detailAddress;
	}
	/**
	 * 设置：城市编码(国标)
	 */
	public void setAdcode(String adcode) {
		this.adcode = adcode;
	}
	/**
	 * 获取：城市编码(国标)
	 */
	public String getAdcode() {
		return adcode;
	}
	/**
	 * 设置：经度
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	/**
	 * 获取：经度
	 */
	public Double getLongitude() {
		return longitude;
	}
	/**
	 * 设置：纬度
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	/**
	 * 获取：纬度
	 */
	public Double getLatitude() {
		return latitude;
	}
	/**
	 * 设置：类型；0-收货地址；1-开拓签约地址
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：类型；0-收货地址；1-开拓签约地址
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：状态，0-启用；1-失效；2-默认
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态，0-启用；1-失效；2-默认
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：0-女性；1-男性
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	/**
	 * 获取：0-女性；1-男性
	 */
	public Integer getSex() {
		return sex;
	}
	/**
	 * 设置：
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：
	 */
	public Integer getSort() {
		return sort;
	}
}
