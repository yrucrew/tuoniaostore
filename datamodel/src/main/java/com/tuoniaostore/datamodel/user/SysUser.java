package com.tuoniaostore.datamodel.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tuoniaostore.datamodel.DefineRandom;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 用户表
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
@ApiModel("用户基本信息")
public class SysUser implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//通行证ID 全世界唯一　
	@ApiModelProperty(name = "id",value = "用户Id",required = false,example = "1")
	private String id= DefineRandom.getUUID();
	@ApiModelProperty(name = "defaultName",value = "默认登录用户名 全世界唯一",required = false,example = "1")
	private String defaultName;
	//
	@ApiModelProperty(name = "password",value = "登录密码",required = false,example = "admin8admin8")
	@JsonIgnore
	private String password;
	//用于展示的名字(可重复)
	@ApiModelProperty(name = "showName",value = "用于展示的名字(可重复)",required = false,example = "嘉嘉")
	private String showName;
	//用户真实名(方便扩展实名认证的功能)
	@ApiModelProperty(name = "realName",value = "用户真实名(方便扩展实名认证的功能)",required = false,example = "嘉嘉")
	private String realName;
	//身份证号
	@ApiModelProperty(name = "idNumber",value = "身份证号",required = false,example = "4500159881248751571")
	private String idNumber;
	//电话号码(不局限于手机号)
	@ApiModelProperty(name = "phoneNumber",value = "电话号码",required = false,example = "4500159881248751571")
	private String phoneNumber;
	//用户性别(0:女性 1:男性；默认值:0)
	@ApiModelProperty(name = "sex",value = "用户性别",required = false,example = "4500159881248751571")
	private Integer sex;
	//用户类型
	@ApiModelProperty(name = "type",value = "用户类型 ",required = false,example = "超级VIP")
	private Integer type;
	//当前的状态(如：正常、禁言、禁登、黑名单等)
	@ApiModelProperty(name = "status",value = "状态 ",required = false,example = "如：正常、禁言、禁登、黑名单")
	private Integer status;
	//注册时间
	@ApiModelProperty(name = "createTime",value = "注册时间 ",required = false,example = "")
	private Date createTime;
	//增值税税率
	private Double taxRate;
	//注册的渠道(如：官网、第三方渠道等；用于分析推广方式) 枚举FromChannelEnum
	private String fromChannel;
	//设备类型(分析用户群，可针对不同用户开展不同的活动) 枚举DeviceTypeEnum
	private Integer deviceType;
	//设备名称
	private String deviceName;
	//最后的访问令牌(每次登录进行重置)
	private String accessToken;
	//登录时间
	private Date loginDate;
	//角色id
	private String roleId;

	//pos机角色
	private String posRoleId;

	//终端集合 （不存放数据库，显示用）
	private List<SysTerminal> list;

	//角色名称（查询用，不存放数据库）
	private String roleName;

	//消费
	private Long consume;
	//
	private String code;
	//经度
	private Float longitude;
	//纬度
	private Float latitude;
	//
	private String passportcol;
	//排序
	private Integer sort;
	//备注
	private String remark;
	//私钥
	private String accessKey;
	//创建用户ID
	private String userId;

	//创建用户ID
	private String userName;//做显示用 不存数据库

	private List<SysUserTerminal> sysUserTerminals;//用户终端list

	private List<SysTerminal> sysTerminalList;//终端名称

	private List<String> sysTerminalIdsList;//终端数组

	private String terminalName;//终端名字 前端展示
	private String terminalIds;//终端名字 前端展示

	//回收站状态
	private Integer retrieveStatus;
	//网络类型
	private String networkType;
	//机器码
	private String machineCode;
	//机器品牌
	private String machineBrand;
	//系统版本
	private String systemVersion;
	//软件版本号
	private String versionIndex;
	// 上级领导ID
	private String leaderUserId;

	private String openId;//小程序支付的时候 需要用到

	public String getPosRoleId() {
		return posRoleId;
	}

	public void setPosRoleId(String posRoleId) {
		this.posRoleId = posRoleId;
	}

	public List<String> getSysTerminalIdsList() {
		return sysTerminalIdsList;
	}

	public void setSysTerminalIdsList(List<String> sysTerminalIdsList) {
		this.sysTerminalIdsList = sysTerminalIdsList;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<SysUserTerminal> getSysUserTerminals() {
		return sysUserTerminals;
	}

	public void setSysUserTerminals(List<SysUserTerminal> sysUserTerminals) {
		this.sysUserTerminals = sysUserTerminals;
	}

	public String getTerminalName() {
		return terminalName;
	}

	public void setTerminalName(String terminalName) {
		this.terminalName = terminalName;
	}

	public List<SysTerminal> getSysTerminalList() {
		return sysTerminalList;
	}

	public void setSysTerminalList(List<SysTerminal> sysTerminalList) {
		this.sysTerminalList = sysTerminalList;
	}

	public List<SysTerminal> getList() {
		return list;
	}

	public void setList(List<SysTerminal> list) {
		this.list = list;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	/**
	 * 设置：通行证ID 全世界唯一
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：通行证ID 全世界唯一
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：默认登录用户名 全世界唯一
	 */
	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}
	/**
	 * 获取：默认登录用户名 全世界唯一
	 */
	public String getDefaultName() {
		return defaultName;
	}
	/**
	 * 设置：登录密码
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * 获取：登录密码
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * 设置：用于展示的名字(可重复)
	 */
	public void setShowName(String showName) {
		this.showName = showName;
	}
	/**
	 * 获取：用于展示的名字(可重复)
	 */
	public String getShowName() {
		return showName;
	}
	/**
	 * 设置：用户真实名(方便扩展实名认证的功能)
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}
	/**
	 * 获取：用户真实名(方便扩展实名认证的功能)
	 */
	public String getRealName() {
		return realName;
	}
	/**
	 * 设置：身份证号
	 */
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	/**
	 * 获取：身份证号
	 */
	public String getIdNumber() {
		return idNumber;
	}
	/**
	 * 设置：电话号码(不局限于手机号)
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * 获取：电话号码(不局限于手机号)
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * 设置：用户性别(0:女性 1:男性；默认值:0)
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	/**
	 * 获取：用户性别(0:女性 1:男性；默认值:0)
	 */
	public Integer getSex() {
		return sex;
	}
	/**
	 * 设置：用户类型(普通用户，VIP用户等，由逻辑设定)51,无人便利店，52无人售货架
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：用户类型(普通用户，VIP用户等，由逻辑设定)51,无人便利店，52无人售货架
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：当前的状态(如：正常、禁言、禁登、黑名单等)
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：当前的状态(如：正常、禁言、禁登、黑名单等)
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：注册时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：注册时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：增值税税率
	 */
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	/**
	 * 获取：增值税税率
	 */
	public Double getTaxRate() {
		return taxRate;
	}
	/**
	 * 设置：注册的渠道(如：官网、第三方渠道等；用于分析推广方式)
	 */
	public void setFromChannel(String fromChannel) {
		this.fromChannel = fromChannel;
	}
	/**
	 * 获取：注册的渠道(如：官网、第三方渠道等；用于分析推广方式)
	 */
	public String getFromChannel() {
		return fromChannel;
	}
	/**
	 * 设置：设备类型(分析用户群，可针对不同用户开展不同的活动)
	 */
	public void setDeviceType(Integer deviceType) {
		this.deviceType = deviceType;
	}
	/**
	 * 获取：设备类型(分析用户群，可针对不同用户开展不同的活动)
	 */
	public Integer getDeviceType() {
		return deviceType;
	}
	/**
	 * 设置：设备名称
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	/**
	 * 获取：设备名称
	 */
	public String getDeviceName() {
		return deviceName;
	}
	/**
	 * 设置：最后的访问令牌(每次登录进行重置)
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	/**
	 * 获取：最后的访问令牌(每次登录进行重置)
	 */
	public String getAccessToken() {
		return accessToken;
	}
	/**
	 * 设置：
	 */
	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}
	/**
	 * 获取：
	 */
	public Date getLoginDate() {
		return loginDate;
	}
	/**
	 * 设置：角色
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	/**
	 * 获取：角色
	 */
	public String getRoleId() {
		return roleId;
	}
	/**
	 * 设置：
	 */
	public void setConsume(Long consume) {
		this.consume = consume;
	}
	/**
	 * 获取：
	 */
	public Long getConsume() {
		return consume;
	}
	/**
	 * 设置：
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * 获取：
	 */
	public String getCode() {
		return code;
	}
	/**
	 * 设置：
	 */
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}
	/**
	 * 获取：
	 */
	public Float getLongitude() {
		return longitude;
	}
	/**
	 * 设置：
	 */
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}
	/**
	 * 获取：
	 */
	public Float getLatitude() {
		return latitude;
	}
	/**
	 * 设置：
	 */
	public void setPassportcol(String passportcol) {
		this.passportcol = passportcol;
	}
	/**
	 * 获取：
	 */
	public String getPassportcol() {
		return passportcol;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：私钥
	 */
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	/**
	 * 获取：私钥
	 */
	public String getAccessKey() {
		return accessKey;
	}
	/**
	 * 设置：创建用户ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：创建用户ID
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：回收站状态
	 */
	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}
	/**
	 * 获取：回收站状态
	 */
	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}
	/**
	 * 设置：网络类型
	 */
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	/**
	 * 获取：网络类型
	 */
	public String getNetworkType() {
		return networkType;
	}
	/**
	 * 设置：机器码
	 */
	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}
	/**
	 * 获取：机器码
	 */
	public String getMachineCode() {
		return machineCode;
	}
	/**
	 * 设置：机器品牌
	 */
	public void setMachineBrand(String machineBrand) {
		this.machineBrand = machineBrand;
	}
	/**
	 * 获取：机器品牌
	 */
	public String getMachineBrand() {
		return machineBrand;
	}
	/**
	 * 设置：系统版本
	 */
	public void setSystemVersion(String systemVersion) {
		this.systemVersion = systemVersion;
	}
	/**
	 * 获取：系统版本
	 */
	public String getSystemVersion() {
		return systemVersion;
	}
	/**
	 * 设置：软件版本号
	 */
	public void setVersionIndex(String versionIndex) {
		this.versionIndex = versionIndex;
	}
	/**
	 * 获取：软件版本号
	 */
	public String getVersionIndex() {
		return versionIndex;
	}
	public String getLeaderUserId() {
		return leaderUserId;
	}
	public void setLeaderUserId(String leaderUserId) {
		this.leaderUserId = leaderUserId;
	}

	@Override
	public String toString() {
		return "SysUser{" +
				"id='" + id + '\'' +
				", defaultName='" + defaultName + '\'' +
				", password='" + password + '\'' +
				", showName='" + showName + '\'' +
				", realName='" + realName + '\'' +
				", idNumber='" + idNumber + '\'' +
				", phoneNumber='" + phoneNumber + '\'' +
				", sex=" + sex +
				", type=" + type +
				", status=" + status +
				", createTime=" + createTime +
				", taxRate=" + taxRate +
				", fromChannel='" + fromChannel + '\'' +
				", deviceType=" + deviceType +
				", deviceName='" + deviceName + '\'' +
				", accessToken='" + accessToken + '\'' +
				", loginDate=" + loginDate +
				", roleId='" + roleId + '\'' +
				", list=" + list +
				", roleName='" + roleName + '\'' +
				", consume=" + consume +
				", code='" + code + '\'' +
				", longitude=" + longitude +
				", latitude=" + latitude +
				", passportcol='" + passportcol + '\'' +
				", sort=" + sort +
				", remark='" + remark + '\'' +
				", accessKey='" + accessKey + '\'' +
				", userId='" + userId + '\'' +
				", userName='" + userName + '\'' +
				", sysUserTerminals=" + sysUserTerminals +
				", sysTerminalList=" + sysTerminalList +
				", terminalName='" + terminalName + '\'' +
				", retrieveStatus=" + retrieveStatus +
				", networkType='" + networkType + '\'' +
				", machineCode='" + machineCode + '\'' +
				", machineBrand='" + machineBrand + '\'' +
				", systemVersion='" + systemVersion + '\'' +
				", versionIndex='" + versionIndex + '\'' +
				", leaderUserId='" + leaderUserId + '\'' +
				", openId='" + openId + '\'' +
				'}';
	}
}
