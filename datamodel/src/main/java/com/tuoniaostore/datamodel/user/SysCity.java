package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-18 20:06:47
 */
public class SysCity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//
	private Long code;
	//
	private String sheng;
	//
	private String di;
	//
	private String xian;
	//
	private String name;
	//
	private String level;
	//
	private Date createTime;
	//
	private Integer sort;
	//
	private Integer status;
	//
	private String longitude;
	//
	private String latitude;
	//
	private Integer retrieveStatus;

	/*
	勾选
	 */
	private boolean checkFlag;

	public boolean isCheckFlag() {
		return checkFlag;
	}

	public void setCheckFlag(boolean checkFlag) {
		this.checkFlag = checkFlag;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setCode(Long code) {
		this.code = code;
	}
	/**
	 * 获取：
	 */
	public Long getCode() {
		return code;
	}
	/**
	 * 设置：
	 */
	public void setSheng(String sheng) {
		this.sheng = sheng;
	}
	/**
	 * 获取：
	 */
	public String getSheng() {
		return sheng;
	}
	/**
	 * 设置：
	 */
	public void setDi(String di) {
		this.di = di;
	}
	/**
	 * 获取：
	 */
	public String getDi() {
		return di;
	}
	/**
	 * 设置：
	 */
	public void setXian(String xian) {
		this.xian = xian;
	}
	/**
	 * 获取：
	 */
	public String getXian() {
		return xian;
	}
	/**
	 * 设置：
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：
	 */
	public void setLevel(String level) {
		this.level = level;
	}
	/**
	 * 获取：
	 */
	public String getLevel() {
		return level;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	/**
	 * 获取：
	 */
	public String getLongitude() {
		return longitude;
	}
	/**
	 * 设置：
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	/**
	 * 获取：
	 */
	public String getLatitude() {
		return latitude;
	}
	/**
	 * 设置：
	 */
	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}
	/**
	 * 获取：
	 */
	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}
}
