package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 用户短信日志
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public class SysUserSmsLogger implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//接收号码
	private String phone;
	//发送内容
	private String content;
	//验证码内容(验证码短信有效)
	private String code;
	//短信类型 0-注册 1-找回密码 2-重绑手机
	private Integer type;
	//状态 0--已发送
	private Integer status;
	//发送时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：接收号码
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：接收号码
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：发送内容
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：发送内容
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：验证码内容(验证码短信有效)
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * 获取：验证码内容(验证码短信有效)
	 */
	public String getCode() {
		return code;
	}
	/**
	 * 设置：短信类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：短信类型
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：状态 0--已发送
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态 0--已发送
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：发送时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：发送时间
	 */
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public String toString() {
		return "SysUserSmsLogger{" +
				"id='" + id + '\'' +
				", phone='" + phone + '\'' +
				", content='" + content + '\'' +
				", code='" + code + '\'' +
				", type=" + type +
				", status=" + status +
				", createTime=" + createTime +
				'}';
	}
}
