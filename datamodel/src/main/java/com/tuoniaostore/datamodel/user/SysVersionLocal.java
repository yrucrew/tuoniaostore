package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;


/**
 * 局部版本
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public class SysVersionLocal implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//版本Id
	private String userVersionId;
	//可更新用户Id
	private String userId;
	//操作人
	private String operateUserId;
	//创建时间
	private Date createTime;
	//逻辑删除 0正常 默认
	private Integer retrieveStatus;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}

	/**
	 * 获取：版本Id
	 */
	public String getUserVersionId() {
		return userVersionId;
	}

	/**
	 * 设置：版本Id
	 */
	public void setUserVersionId(String userVersionId) {
		this.userVersionId = userVersionId;
	}

	/**
	 * 获取：可更新用户Id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * 设置：可更新用户Id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * 获取：操作人
	 */
	public String getOperateUserId() {
		return operateUserId;
	}

	/**
	 * 设置：操作人
	 */
	public void setOperateUserId(String operateUserId) {
		this.operateUserId = operateUserId;
	}

	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}
	/**
	 * 获取：
	 */
	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}
}
