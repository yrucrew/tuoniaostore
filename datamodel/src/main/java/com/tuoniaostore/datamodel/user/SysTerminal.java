package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 系统终端表
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public class SysTerminal implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//ID
	private String id= DefineRandom.getUUID();
	//终端名称
	private String title;
	//排序
	private Integer sort;
	//备注
	private String remark;
	//终端状态
	private Integer status;
	//用户id
	private String userId;
	private String userName;
	//创建时间
	private Date createTime;
	//删除状态
	private Integer retrieveStatus;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 设置：ID
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：ID
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：终端名称
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：终端名称
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}
	/**
	 * 获取：
	 */
	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}
}
