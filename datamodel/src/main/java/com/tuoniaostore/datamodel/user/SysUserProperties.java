package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 用户属性
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public class SysUserProperties implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//归属的帐号ID
	private String userId;
	//类型
	private Integer type;
	//属性索引
	private String k;
	//属性值
	private String v;
	//建立时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：归属的帐号ID
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * 设置：类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：类型
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：属性索引
	 */
	public void setK(String k) {
		this.k = k;
	}
	/**
	 * 获取：属性索引
	 */
	public String getK() {
		return k;
	}
	/**
	 * 设置：属性值
	 */
	public void setV(String v) {
		this.v = v;
	}
	/**
	 * 获取：属性值
	 */
	public String getV() {
		return v;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
