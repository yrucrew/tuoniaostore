package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public class SysRetrieve implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//名称
	private String title;
	//表
	private String retrieveTableName;
	//实体id
	private String retrieveTableId;
	//用户id
	private String userId;
	//创建时间
	private Date createTime;


	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：
	 */
	public void setRetrieveTableName(String retrieveTableName) {
		this.retrieveTableName = retrieveTableName;
	}
	/**
	 * 获取：
	 */
	public String getRetrieveTableName() {
		return retrieveTableName;
	}
	/**
	 * 设置：
	 */
	public void setRetrieveTableId(String retrieveTableId) {
		this.retrieveTableId = retrieveTableId;
	}
	/**
	 * 获取：
	 */
	public String getRetrieveTableId() {
		return retrieveTableId;
	}
	/**
	 * 设置：
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
