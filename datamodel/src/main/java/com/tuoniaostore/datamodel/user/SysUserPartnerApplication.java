package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public class SysUserPartnerApplication implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//合作者ID
	private String partnerId;
	//应用产品名
	private String appName;
	//应用key
	private String appKey;
	//访问key 预留字段 必须存在
	private String accessKey;
	//
	private String accessKeyExponent;
	//访问密钥 预留字段 必须存在
	private String accessKeySecret;
	//
	private String accessKeySecretExponent;
	//
	private Date createTime;
	//
	private Integer sort;
	//
	private Integer status;
	//回收站
	private Integer retrieveStatus;
	//
	private String remark;
	//
	private String accessKeyModulus;
	//
	private String accessKeySecretModulus;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：合作者ID
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	/**
	 * 获取：合作者ID
	 */
	public String getPartnerId() {
		return partnerId;
	}
	/**
	 * 设置：应用产品名
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}
	/**
	 * 获取：应用产品名
	 */
	public String getAppName() {
		return appName;
	}
	/**
	 * 设置：应用key
	 */
	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}
	/**
	 * 获取：应用key
	 */
	public String getAppKey() {
		return appKey;
	}
	/**
	 * 设置：访问key 预留字段 必须存在
	 */
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	/**
	 * 获取：访问key 预留字段 必须存在
	 */
	public String getAccessKey() {
		return accessKey;
	}
	/**
	 * 设置：
	 */
	public void setAccessKeyExponent(String accessKeyExponent) {
		this.accessKeyExponent = accessKeyExponent;
	}
	/**
	 * 获取：
	 */
	public String getAccessKeyExponent() {
		return accessKeyExponent;
	}
	/**
	 * 设置：访问密钥 预留字段 必须存在
	 */
	public void setAccessKeySecret(String accessKeySecret) {
		this.accessKeySecret = accessKeySecret;
	}
	/**
	 * 获取：访问密钥 预留字段 必须存在
	 */
	public String getAccessKeySecret() {
		return accessKeySecret;
	}
	/**
	 * 设置：
	 */
	public void setAccessKeySecretExponent(String accessKeySecretExponent) {
		this.accessKeySecretExponent = accessKeySecretExponent;
	}
	/**
	 * 获取：
	 */
	public String getAccessKeySecretExponent() {
		return accessKeySecretExponent;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：回收站
	 */
	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}
	/**
	 * 获取：回收站
	 */
	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}
	/**
	 * 设置：
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：
	 */
	public void setAccessKeyModulus(String accessKeyModulus) {
		this.accessKeyModulus = accessKeyModulus;
	}
	/**
	 * 获取：
	 */
	public String getAccessKeyModulus() {
		return accessKeyModulus;
	}
	/**
	 * 设置：
	 */
	public void setAccessKeySecretModulus(String accessKeySecretModulus) {
		this.accessKeySecretModulus = accessKeySecretModulus;
	}
	/**
	 * 获取：
	 */
	public String getAccessKeySecretModulus() {
		return accessKeySecretModulus;
	}
}
