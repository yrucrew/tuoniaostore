package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public class SysUserPartner implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//通行证ID
	private String userId;
	//合作方名字
	private String name;
	//简介内容
	private String introduce;
	//合作类型，暂时为1
	private Integer type;
	//状态 1可用
	private Integer status;
	//回收站
	private Integer retrieveStatus;
	//
	private Date createTime;
	//排序
	private Integer sort;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：通行证ID
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * 设置：合作方名字
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：合作方名字
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：简介内容
	 */
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	/**
	 * 获取：简介内容
	 */
	public String getIntroduce() {
		return introduce;
	}
	/**
	 * 设置：合作类型，暂时为1
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：合作类型，暂时为1
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：状态 1可用
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态 1可用
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：回收站
	 */
	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}
	/**
	 * 获取：回收站
	 */
	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}
}
