package com.tuoniaostore.datamodel.user;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 系统版本
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 15:56:19
 */
public class SysVersion implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//对应的设备类型 如：Android IOS等
	private Integer deviceType;
	//客户端类型，逻辑决定，如：供应链、商家端等
	private Integer clientType;
	//内部版本号 一般递增 初始为1
	private Integer versionIndex;
	//用于展示的版本号
	private String showVersion;
	//最低兼容的版本(内部版本号)
	private Integer minVersionIndex;
	//下载地址
	private String versionUrl;
	//更新内容介绍
	private String versionIntroduce;
	//开放更新时间
	private Date openTime;
	//是否局部更新：0否，1是
	private Integer islocal;
	//发布时间
	private Date createTime;
	//用户id	*
	private String userId;
	// 0正常 默认
	private Integer retrieveStatus;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：对应的设备类型 如：Android IOS等
	 */
	public void setDeviceType(Integer deviceType) {
		this.deviceType = deviceType;
	}
	/**
	 * 获取：对应的设备类型 如：Android IOS等
	 */
	public Integer getDeviceType() {
		return deviceType;
	}
	/**
	 * 设置：客户端类型，逻辑决定，如：供应链、商家端等
	 */
	public void setClientType(Integer clientType) {
		this.clientType = clientType;
	}
	/**
	 * 获取：客户端类型，逻辑决定，如：供应链、商家端等
	 */
	public Integer getClientType() {
		return clientType;
	}
	/**
	 * 设置：内部版本号 一般递增 初始为1
	 */
	public void setVersionIndex(Integer versionIndex) {
		this.versionIndex = versionIndex;
	}
	/**
	 * 获取：内部版本号 一般递增 初始为1
	 */
	public Integer getVersionIndex() {
		return versionIndex;
	}
	/**
	 * 设置：用于展示的版本号
	 */
	public void setShowVersion(String showVersion) {
		this.showVersion = showVersion;
	}
	/**
	 * 获取：用于展示的版本号
	 */
	public String getShowVersion() {
		return showVersion;
	}
	/**
	 * 设置：最低兼容的版本(内部版本号)
	 */
	public void setMinVersionIndex(Integer minVersionIndex) {
		this.minVersionIndex = minVersionIndex;
	}
	/**
	 * 获取：最低兼容的版本(内部版本号)
	 */
	public Integer getMinVersionIndex() {
		return minVersionIndex;
	}
	/**
	 * 设置：下载地址
	 */
	public void setVersionUrl(String versionUrl) {
		this.versionUrl = versionUrl;
	}
	/**
	 * 获取：下载地址
	 */
	public String getVersionUrl() {
		return versionUrl;
	}
	/**
	 * 设置：更新内容介绍
	 */
	public void setVersionIntroduce(String versionIntroduce) {
		this.versionIntroduce = versionIntroduce;
	}
	/**
	 * 获取：更新内容介绍
	 */
	public String getVersionIntroduce() {
		return versionIntroduce;
	}
	/**
	 * 设置：开放更新时间
	 */
	public void setOpenTime(Date openTime) {
		this.openTime = openTime;
	}
	/**
	 * 获取：开放更新时间
	 */
	public Date getOpenTime() {
		return openTime;
	}
	/**
	 * 设置：是否局部更新：0否，1是
	 */
	public void setIslocal(Integer islocal) {
		this.islocal = islocal;
	}
	/**
	 * 获取：是否局部更新：0否，1是
	 */
	public Integer getIslocal() {
		return islocal;
	}
	/**
	 * 设置：发布时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：发布时间
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * 设置：用户id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * 获取：用户id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * 设置：
	 */
	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}
	/**
	 * 获取：
	 */
	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}
}
