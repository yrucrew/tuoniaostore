package com.tuoniaostore.datamodel.payment;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 余额扩展表
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public class PaymentCurrencyProperties implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//账户ID
	private String userId;

	private String userName;
	//属性类型
	private Integer type;
	//属性key
	private String k;
	//属性值
	private String v;
	//建立时间
	private Date createTime;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：账户ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：账户ID
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：属性类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：属性类型
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：属性key
	 */
	public void setK(String k) {
		this.k = k;
	}
	/**
	 * 获取：属性key
	 */
	public String getK() {
		return k;
	}
	/**
	 * 设置：属性值
	 */
	public void setV(String v) {
		this.v = v;
	}
	/**
	 * 获取：属性值
	 */
	public String getV() {
		return v;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
