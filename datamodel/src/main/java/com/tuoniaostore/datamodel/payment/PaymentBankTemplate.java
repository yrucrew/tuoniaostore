package com.tuoniaostore.datamodel.payment;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 银行卡模板
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public class PaymentBankTemplate implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();

	//银行名字
	private String name;
	//简称
	private String simpleCode;
	//支行号
	private String bankCode;
	//图片
	private String logo;
	//建立时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：银行名字
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：银行名字
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：简称
	 */
	public void setSimpleCode(String simpleCode) {
		this.simpleCode = simpleCode;
	}
	/**
	 * 获取：简称
	 */
	public String getSimpleCode() {
		return simpleCode;
	}
	/**
	 * 设置：支行号
	 */
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	/**
	 * 获取：支行号
	 */
	public String getBankCode() {
		return bankCode;
	}
	/**
	 * 设置：图片
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}
	/**
	 * 获取：图片
	 */
	public String getLogo() {
		return logo;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
