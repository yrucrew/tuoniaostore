package com.tuoniaostore.datamodel.payment;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 花费记录
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public class PaymentCurrencyOffsetLogger implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//用户ID
	private String userId;
	//用户名字
	private String userName;
	//渠道ID(用于计算归属)
	private String channelId;
	//货币类型 CurrencyTypeEnum
	private Integer currencyType;
	//变化前的额度 所有的金额都是按分算的
 	private Long beforeAmount;
	//偏移的额度
	private Long offsetAmount;
	//变化后的额度
	private Long afterAmount;
	//交易的标题
	private String transTitle;
	//交易的类型 如：支付宝、微信、银联等 具体字段内容由逻辑定义 PayTypeEnum
	private String transType;
	//关联的交易类型 如：提现、收入、消费(支出)等 RelationTransTypeEnum
	private Integer relationTransType;
	//关联的交易序号
	private String relationTransSequence;
	//0、完成(默认值) 1、逻辑删除
	private Integer status;
	//记录时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	/**
	 * 设置：货币类型
	 */
	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}
	/**
	 * 获取：货币类型
	 */
	public Integer getCurrencyType() {
		return currencyType;
	}
	/**
	 * 设置：变化前的额度
	 */
	public void setBeforeAmount(Long beforeAmount) {
		this.beforeAmount = beforeAmount;
	}
	/**
	 * 获取：变化前的额度
	 */
	public Long getBeforeAmount() {
		return beforeAmount;
	}
	/**
	 * 设置：偏移的额度
	 */
	public void setOffsetAmount(Long offsetAmount) {
		this.offsetAmount = offsetAmount;
	}
	/**
	 * 获取：偏移的额度
	 */
	public Long getOffsetAmount() {
		return offsetAmount;
	}
	/**
	 * 设置：变化后的额度
	 */
	public void setAfterAmount(Long afterAmount) {
		this.afterAmount = afterAmount;
	}
	/**
	 * 获取：变化后的额度
	 */
	public Long getAfterAmount() {
		return afterAmount;
	}
	/**
	 * 设置：交易的标题
	 */
	public void setTransTitle(String transTitle) {
		this.transTitle = transTitle;
	}
	/**
	 * 获取：交易的标题
	 */
	public String getTransTitle() {
		return transTitle;
	}
	/**
	 * 设置：交易的类型 如：支付宝、微信、银联等 具体字段内容由逻辑定义
	 */
	public void setTransType(String transType) {
		this.transType = transType;
	}
	/**
	 * 获取：交易的类型 如：支付宝、微信、银联等 具体字段内容由逻辑定义
	 */
	public String getTransType() {
		return transType;
	}
	/**
	 * 设置：关联的交易类型 如：提现、收入等
	 */
	public void setRelationTransType(Integer relationTransType) {
		this.relationTransType = relationTransType;
	}
	/**
	 * 获取：关联的交易类型 如：提现、收入等
	 */
	public Integer getRelationTransType() {
		return relationTransType;
	}
	/**
	 * 设置：关联的交易序号
	 */
	public void setRelationTransSequence(String relationTransSequence) {
		this.relationTransSequence = relationTransSequence;
	}
	/**
	 * 获取：关联的交易序号
	 */
	public String getRelationTransSequence() {
		return relationTransSequence;
	}
	/**
	 * 设置：1、完成(默认值) 4、逻辑删除
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：1、完成(默认值) 4、逻辑删除
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：记录时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：记录时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
