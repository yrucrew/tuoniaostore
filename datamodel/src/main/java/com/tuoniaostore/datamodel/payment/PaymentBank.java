package com.tuoniaostore.datamodel.payment;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 银行卡信息
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public class PaymentBank implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//通行证ID
	private String userId;
	//银行模版ID
	private String bankTemplateId;
	//创建人名字
	private String userName;
	//账户类型1、储值卡 2、信用卡
	private Integer accountType;
	//银行帐号
	private String bankAccount;
	//持有者姓名
	private String accountName;
	//预留电话
	private String reservePhone;
	//支行名
	private String branchName;
	//状态 如：默认 1、失效 -1、普通 0 等
	private Integer status;
	//建立时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBankTemplateId() {
		return bankTemplateId;
	}

	public void setBankTemplateId(String bankTemplateId) {
		this.bankTemplateId = bankTemplateId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 设置：账户类型1、储值卡 2、信用卡
	 */
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	/**
	 * 获取：账户类型1、储值卡 2、信用卡
	 */
	public Integer getAccountType() {
		return accountType;
	}
	/**
	 * 设置：银行帐号
	 */
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	/**
	 * 获取：银行帐号
	 */
	public String getBankAccount() {
		return bankAccount;
	}
	/**
	 * 设置：持有者姓名
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	/**
	 * 获取：持有者姓名
	 */
	public String getAccountName() {
		return accountName;
	}
	/**
	 * 设置：预留电话
	 */
	public void setReservePhone(String reservePhone) {
		this.reservePhone = reservePhone;
	}
	/**
	 * 获取：预留电话
	 */
	public String getReservePhone() {
		return reservePhone;
	}
	/**
	 * 设置：支行名
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	/**
	 * 获取：支行名
	 */
	public String getBranchName() {
		return branchName;
	}
	/**
	 * 设置：状态 如：默认 1、失效 -1、普通 0等
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态 如：默认 1、失效 -1、普通 0等
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
