package com.tuoniaostore.datamodel.payment;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 账户信息
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public class PaymentCurrencyAccount implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//用户ID
	private String userId;

	//用户名字
	private String userName;
	//渠道ID(用于计算归属)
	private String channelId;
	//货币类型 0 余额 1会员余额 CurrencyTypeEnum
	private Integer currencyType;
	//货币名字 余额  会员余额 CurrencyTypeEnum
	private String name;
	//当前可用额度
	private Long currentAmount;
	//冻结额度
	private Long freezeAmount;
	//总的入账额度
	private Long totalIntoAmount;
	//总的出账额度
	private Long totalOutputAmount;
	//建立时间
	private Date createTime;

	public String getUserId() {
		return userId;
	}

	/**
	 * 设置：用户ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：创建人
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * 设置：创建人名字
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：渠道ID(用于计算归属)
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：渠道ID(用于计算归属)
	 */
	public String getChannelId() {
		return channelId;
	}
	/**
	 * 设置：货币类型
	 */
	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}
	/**
	 * 获取：货币类型
	 */
	public Integer getCurrencyType() {
		return currencyType;
	}
	/**
	 * 设置：货币名字
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：货币名字
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：当前可用额度
	 */
	public void setCurrentAmount(Long currentAmount) {
		this.currentAmount = currentAmount;
	}
	/**
	 * 获取：当前可用额度
	 */
	public Long getCurrentAmount() {
		return currentAmount;
	}
	/**
	 * 设置：冻结额度
	 */
	public void setFreezeAmount(Long freezeAmount) {
		this.freezeAmount = freezeAmount;
	}
	/**
	 * 获取：冻结额度
	 */
	public Long getFreezeAmount() {
		return freezeAmount;
	}
	/**
	 * 设置：总的入账额度
	 */
	public void setTotalIntoAmount(Long totalIntoAmount) {
		this.totalIntoAmount = totalIntoAmount;
	}
	/**
	 * 获取：总的入账额度
	 */
	public Long getTotalIntoAmount() {
		return totalIntoAmount;
	}
	/**
	 * 设置：总的出账额度
	 */
	public void setTotalOutputAmount(Long totalOutputAmount) {
		this.totalOutputAmount = totalOutputAmount;
	}
	/**
	 * 获取：总的出账额度
	 */
	public Long getTotalOutputAmount() {
		return totalOutputAmount;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
