package com.tuoniaostore.datamodel.payment;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 充值
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public class PaymentRechargePresent implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//最低充值额度
	private Long minimumRechargeAmount;
	//对应的货币类型
	private Integer currencyType;
	//赠送额度
	private Long presentAmount;
	//介绍内容
	private String instruction;
	//生效时间
	private Date effectiveTime;
	//失效时间
	private Date failureTime;
	//当前状态 如：1、有效 0、无效
	private Integer status;
	//赠送金额限制设置
	private Long limits;
	//建立时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：最低充值额度
	 */
	public void setMinimumRechargeAmount(Long minimumRechargeAmount) {
		this.minimumRechargeAmount = minimumRechargeAmount;
	}
	/**
	 * 获取：最低充值额度
	 */
	public Long getMinimumRechargeAmount() {
		return minimumRechargeAmount;
	}
	/**
	 * 设置：对应的货币类型
	 */
	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}
	/**
	 * 获取：对应的货币类型
	 */
	public Integer getCurrencyType() {
		return currencyType;
	}
	/**
	 * 设置：赠送额度
	 */
	public void setPresentAmount(Long presentAmount) {
		this.presentAmount = presentAmount;
	}
	/**
	 * 获取：赠送额度
	 */
	public Long getPresentAmount() {
		return presentAmount;
	}
	/**
	 * 设置：介绍内容
	 */
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	/**
	 * 获取：介绍内容
	 */
	public String getInstruction() {
		return instruction;
	}
	/**
	 * 设置：生效时间
	 */
	public void setEffectiveTime(Date effectiveTime) {
		this.effectiveTime = effectiveTime;
	}
	/**
	 * 获取：生效时间
	 */
	public Date getEffectiveTime() {
		return effectiveTime;
	}
	/**
	 * 设置：失效时间
	 */
	public void setFailureTime(Date failureTime) {
		this.failureTime = failureTime;
	}
	/**
	 * 获取：失效时间
	 */
	public Date getFailureTime() {
		return failureTime;
	}
	/**
	 * 设置：当前状态 如：1、有效 0、无效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：当前状态 如：1、有效 0、无效
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：赠送金额限制设置
	 */
	public void setLimits(Long limits) {
		this.limits = limits;
	}
	/**
	 * 获取：赠送金额限制设置
	 */
	public Long getLimits() {
		return limits;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
