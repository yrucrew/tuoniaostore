package com.tuoniaostore.datamodel.payment;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 账期表
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-10 15:53:05
 */
public class PaymentEstimateRevenue implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//
	private String userId;
	//订单号
	private String orderId;
	//商家名称
	private String shopsName;
	//订单金额
	private Long orderAmount;
	//实际收入
	private Long actualIncome;
	//状态 如：默认 1、发起提现 2、已取消 3、打钱中 4、已完提现
	private Integer status;
	//请求时间
	private Date requestTime;
	//收款账户
	private String gatheringAccount;
	//结算方式: 0.按结算天数，1.按结算额度 2.即时到账
	private Long settlementType;
	//到账时间
	private Date accountingTime;
	//提现时间
	private Date drawTime;

	//字段 显示用 不存数据库
	private String orderNumber;
	private String userName;
	private String phoneNum;

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * 设置：商家名称
	 */
	public void setShopsName(String shopsName) {
		this.shopsName = shopsName;
	}
	/**
	 * 获取：商家名称
	 */
	public String getShopsName() {
		return shopsName;
	}
	/**
	 * 设置：订单金额
	 */
	public void setOrderAmount(Long orderAmount) {
		this.orderAmount = orderAmount;
	}
	/**
	 * 获取：订单金额
	 */
	public Long getOrderAmount() {
		return orderAmount;
	}
	/**
	 * 设置：实际收入
	 */
	public void setActualIncome(Long actualIncome) {
		this.actualIncome = actualIncome;
	}
	/**
	 * 获取：实际收入
	 */
	public Long getActualIncome() {
		return actualIncome;
	}
	/**
	 * 设置：状态 如：默认 1、发起提现 2、已取消 3、打钱中 4、已完提现
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态 如：默认 1、发起提现 2、已取消 3、打钱中 4、已完提现
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：请求时间
	 */
	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}
	/**
	 * 获取：请求时间
	 */
	public Date getRequestTime() {
		return requestTime;
	}
	/**
	 * 设置：收款账户
	 */
	public void setGatheringAccount(String gatheringAccount) {
		this.gatheringAccount = gatheringAccount;
	}
	/**
	 * 获取：收款账户
	 */
	public String getGatheringAccount() {
		return gatheringAccount;
	}
	/**
	 * 设置：结算方式: 0.按结算天数，1.按结算额度
	 */
	public void setSettlementType(Long settlementType) {
		this.settlementType = settlementType;
	}
	/**
	 * 获取：结算方式: 0.按结算天数，1.按结算额度
	 */
	public Long getSettlementType() {
		return settlementType;
	}
	/**
	 * 设置：到账时间
	 */
	public void setAccountingTime(Date accountingTime) {
		this.accountingTime = accountingTime;
	}
	/**
	 * 获取：到账时间
	 */
	public Date getAccountingTime() {
		return accountingTime;
	}
	/**
	 * 设置：提现时间
	 */
	public void setDrawTime(Date drawTime) {
		this.drawTime = drawTime;
	}
	/**
	 * 获取：提现时间
	 */
	public Date getDrawTime() {
		return drawTime;
	}

}
