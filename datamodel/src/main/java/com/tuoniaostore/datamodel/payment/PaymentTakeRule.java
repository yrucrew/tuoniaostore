package com.tuoniaostore.datamodel.payment;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;



/**
 * 支付规则
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public class PaymentTakeRule implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//提现类型 默认值：0
	private Integer takeMode;
	//展示在客户端的描述内容
	private String takeDesc;
	//应用的目标类型
	private Integer targetType;
	//最低收费费用 单位：分
	private Long lowCost;
	//最高收费费用 单位：分
	private Long highCost;
	//万份比
	private Integer rate;
	//单笔限制(上限) 单位：分
	private Long singleAmountLimit;
	//当天上限
	private Long dayAmountLimit;
	//每天最多提现次数
	private Integer dayCountLimit;
	//遇节假日是否顺延 1 是 0 否
	private Integer holidayDelay;
	//如：48 为48小时内到账
	private Integer delayHour;
	//规则生效时间
	private Date ruleEffectTime;
	//规则失效时间
	private Date ruleInvalidTime;
	//展示于前端的图片 不存在时 表示无需展示图片
	private String showImage;
	//是否为默认选项 1时是 0时否 一个目标类型只能有一种默认选项
	private Integer defaultOption;
	//可提现时间(开始)(为空时不限制)
	private Time availableTimeBegin;
	//可提现时间(结束)(为空时不限制)
	private Time availableEndBegin;
	//可提现星期, 将数字变为二进制形式, 最低位(最右)是星期日, 第七位(从右到左数)是星期一, 对应位为1的星期为可提现日(为空时不限制)
	private Integer availableDate;
	//回收状态0-正常 1-删除
	private Integer retrieveStatus;


	public Integer getRetrieveStatus() {
		return retrieveStatus;
	}

	public void setRetrieveStatus(Integer retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：提现类型 默认值：0
	 */
	public void setTakeMode(Integer takeMode) {
		this.takeMode = takeMode;
	}
	/**
	 * 获取：提现类型 默认值：0
	 */
	public Integer getTakeMode() {
		return takeMode;
	}
	/**
	 * 设置：展示在客户端的描述内容
	 */
	public void setTakeDesc(String takeDesc) {
		this.takeDesc = takeDesc;
	}
	/**
	 * 获取：展示在客户端的描述内容
	 */
	public String getTakeDesc() {
		return takeDesc;
	}
	/**
	 * 设置：应用的目标类型
	 */
	public void setTargetType(Integer targetType) {
		this.targetType = targetType;
	}
	/**
	 * 获取：应用的目标类型
	 */
	public Integer getTargetType() {
		return targetType;
	}
	/**
	 * 设置：最低收费费用 单位：分
	 */
	public void setLowCost(Long lowCost) {
		this.lowCost = lowCost;
	}
	/**
	 * 获取：最低收费费用 单位：分
	 */
	public Long getLowCost() {
		return lowCost;
	}
	/**
	 * 设置：最高收费费用 单位：分
	 */
	public void setHighCost(Long highCost) {
		this.highCost = highCost;
	}
	/**
	 * 获取：最高收费费用 单位：分
	 */
	public Long getHighCost() {
		return highCost;
	}
	/**
	 * 设置：万份比
	 */
	public void setRate(Integer rate) {
		this.rate = rate;
	}
	/**
	 * 获取：万份比
	 */
	public Integer getRate() {
		return rate;
	}
	/**
	 * 设置：单笔限制(上限) 单位：分
	 */
	public void setSingleAmountLimit(Long singleAmountLimit) {
		this.singleAmountLimit = singleAmountLimit;
	}
	/**
	 * 获取：单笔限制(上限) 单位：分
	 */
	public Long getSingleAmountLimit() {
		return singleAmountLimit;
	}
	/**
	 * 设置：当天上限
	 */
	public void setDayAmountLimit(Long dayAmountLimit) {
		this.dayAmountLimit = dayAmountLimit;
	}
	/**
	 * 获取：当天上限
	 */
	public Long getDayAmountLimit() {
		return dayAmountLimit;
	}
	/**
	 * 设置：每天最多提现次数
	 */
	public void setDayCountLimit(Integer dayCountLimit) {
		this.dayCountLimit = dayCountLimit;
	}
	/**
	 * 获取：每天最多提现次数
	 */
	public Integer getDayCountLimit() {
		return dayCountLimit;
	}
	/**
	 * 设置：遇节假日是否顺延 1 是 0 否
	 */
	public void setHolidayDelay(Integer holidayDelay) {
		this.holidayDelay = holidayDelay;
	}
	/**
	 * 获取：遇节假日是否顺延 1 是 0 否
	 */
	public Integer getHolidayDelay() {
		return holidayDelay;
	}
	/**
	 * 设置：如：48 为48小时内到账
	 */
	public void setDelayHour(Integer delayHour) {
		this.delayHour = delayHour;
	}
	/**
	 * 获取：如：48 为48小时内到账
	 */
	public Integer getDelayHour() {
		return delayHour;
	}
	/**
	 * 设置：规则生效时间
	 */
	public void setRuleEffectTime(Date ruleEffectTime) {
		this.ruleEffectTime = ruleEffectTime;
	}
	/**
	 * 获取：规则生效时间
	 */
	public Date getRuleEffectTime() {
		return ruleEffectTime;
	}
	/**
	 * 设置：规则失效时间
	 */
	public void setRuleInvalidTime(Date ruleInvalidTime) {
		this.ruleInvalidTime = ruleInvalidTime;
	}
	/**
	 * 获取：规则失效时间
	 */
	public Date getRuleInvalidTime() {
		return ruleInvalidTime;
	}
	/**
	 * 设置：展示于前端的图片 不存在时 表示无需展示图片
	 */
	public void setShowImage(String showImage) {
		this.showImage = showImage;
	}
	/**
	 * 获取：展示于前端的图片 不存在时 表示无需展示图片
	 */
	public String getShowImage() {
		return showImage;
	}
	/**
	 * 设置：是否为默认选项 1时是 0时否 一个目标类型只能有一种默认选项
	 */
	public void setDefaultOption(Integer defaultOption) {
		this.defaultOption = defaultOption;
	}
	/**
	 * 获取：是否为默认选项 1时是 0时否 一个目标类型只能有一种默认选项
	 */
	public Integer getDefaultOption() {
		return defaultOption;
	}
	/**
	 * 设置：可提现时间(开始)(为空时不限制)
	 */
	public void setAvailableTimeBegin(Time availableTimeBegin) {
		this.availableTimeBegin = availableTimeBegin;
	}
	/**
	 * 获取：可提现时间(开始)(为空时不限制)
	 */
	public Time getAvailableTimeBegin() {
		return availableTimeBegin;
	}
	/**
	 * 设置：可提现时间(结束)(为空时不限制)
	 */
	public void setAvailableEndBegin(Time availableEndBegin) {
		this.availableEndBegin = availableEndBegin;
	}
	/**
	 * 获取：可提现时间(结束)(为空时不限制)
	 */
	public Date getAvailableEndBegin() {
		return availableEndBegin;
	}
	/**
	 * 设置：可提现星期, 将数字变为二进制形式, 最低位(最右)是星期日, 第七位(从右到左数)是星期一, 对应位为1的星期为可提现日(为空时不限制)
	 */
	public void setAvailableDate(Integer availableDate) {
		this.availableDate = availableDate;
	}
	/**
	 * 获取：可提现星期, 将数字变为二进制形式, 最低位(最右)是星期日, 第七位(从右到左数)是星期一, 对应位为1的星期为可提现日(为空时不限制)
	 */
	public Integer getAvailableDate() {
		return availableDate;
	}
}
