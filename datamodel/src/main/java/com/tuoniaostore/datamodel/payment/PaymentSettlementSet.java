package com.tuoniaostore.datamodel.payment;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 结算
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-05 18:29:57
 */
public class PaymentSettlementSet implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//用户id
	private String userId;

	private String userName;
	private String phoneNum;

	//扣点(万分比)
	private Long cablePoint;
	//结算天数
	private Integer settlementDays;
	//结算金额
	private Long settlementMoney;
	//结算金额：0.按结算天数，1.按结算额度
	private Integer settlementType;
	//1、扣点处理，2、返点处理
	private Integer type;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：扣点(万分比)
	 */
	public void setCablePoint(Long cablePoint) {
		this.cablePoint = cablePoint;
	}
	/**
	 * 获取：扣点(万分比)
	 */
	public Long getCablePoint() {
		return cablePoint;
	}
	/**
	 * 设置：结算天数
	 */
	public void setSettlementDays(Integer settlementDays) {
		this.settlementDays = settlementDays;
	}
	/**
	 * 获取：结算天数
	 */
	public Integer getSettlementDays() {
		return settlementDays;
	}
	/**
	 * 设置：结算金额
	 */
	public void setSettlementMoney(Long settlementMoney) {
		this.settlementMoney = settlementMoney;
	}
	/**
	 * 获取：结算金额
	 */
	public Long getSettlementMoney() {
		return settlementMoney;
	}
	/**
	 * 设置：结算金额：0.按结算天数，1.按结算额度
	 */
	public void setSettlementType(Integer settlementType) {
		this.settlementType = settlementType;
	}
	/**
	 * 获取：结算金额：0.按结算天数，1.按结算额度
	 */
	public Integer getSettlementType() {
		return settlementType;
	}
	/**
	 * 设置：1、扣点处理，2返点处理
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：1、扣点处理，2返点处理
	 */
	public Integer getType() {
		return type;
	}
}
