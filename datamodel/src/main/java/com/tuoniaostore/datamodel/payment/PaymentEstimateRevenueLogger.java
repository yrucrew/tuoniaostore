package com.tuoniaostore.datamodel.payment;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-08 10:21:40
 */
public class PaymentEstimateRevenueLogger implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//
	private String userId;
	//订单号
	private String orderId;
	//订单金额
	private Long orderAmount;
	//实际收入
	private Long actualIncome;
	//收款账户
	private String gatheringAccount;
	//创建时间
	private Date createTime;

	private String userName;
	private String phoneNum;
	private String orderNumber;

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * 设置：订单金额
	 */
	public void setOrderAmount(Long orderAmount) {
		this.orderAmount = orderAmount;
	}
	/**
	 * 获取：订单金额
	 */
	public Long getOrderAmount() {
		return orderAmount;
	}
	/**
	 * 设置：实际收入
	 */
	public void setActualIncome(Long actualIncome) {
		this.actualIncome = actualIncome;
	}
	/**
	 * 获取：实际收入
	 */
	public Long getActualIncome() {
		return actualIncome;
	}
	/**
	 * 设置：收款账户
	 */
	public void setGatheringAccount(String gatheringAccount) {
		this.gatheringAccount = gatheringAccount;
	}
	/**
	 * 获取：收款账户
	 */
	public String getGatheringAccount() {
		return gatheringAccount;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
