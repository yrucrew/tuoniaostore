package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 首页设置表
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-04 14:40:15
 */
public class SupplychainHomepageGoodsSetting implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//主键
	private String id= DefineRandom.getUUID();
	//记录类型 0=精选商品; 1=推荐商品; 2=热门品类 3=热门品牌
	private Integer type;
	//区域id
	private String logicAreaId;
	//记录值 当type(记录类型)=0和1 priceId  type=2时 此字段时分类id; type=3时 此字段为品牌id
	private String value;
	//名称(根据value值的类型 记一下分类/品牌名称, 说不定以后有用, value是商品id的话这个值放商品名称)
	private String name;
	//标语
	private String description;
	//排序
	private Integer sort;
	//创建时间
	private Date createTime;
	//创建人id
	private String creatorId;

	/**
	 * 设置：主键
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：记录类型 0=精选商品; 1=推荐商品; 2=推荐分类 3=推荐品牌
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：记录类型 0=精选商品; 1=推荐商品; 2=推荐分类 3=推荐品牌
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：区域id
	 */
	public void setLogicAreaId(String logicAreaId) {
		this.logicAreaId = logicAreaId;
	}
	/**
	 * 获取：区域id
	 */
	public String getLogicAreaId() {
		return logicAreaId;
	}
	/**
	 * 设置：记录值 当type(记录类型)=0,1时 此字段是商品模板id; type=2时 此字段时分类id; type=3时 此字段为品牌id
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * 获取：记录值 当type(记录类型)=0,1时 此字段是商品模板id; type=2时 此字段时分类id; type=3时 此字段为品牌id
	 */
	public String getValue() {
		return value;
	}
	/**
	 * 设置：名称(根据value值的类型 记一下分类/品牌名称, 说不定以后有用, value是商品id的话这个值就留空, 没想到有什么用)
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：名称(根据value值的类型 记一下分类/品牌名称, 说不定以后有用, value是商品id的话这个值就留空, 没想到有什么用)
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：标语
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * 获取：标语
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建人id
	 */
	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}
	/**
	 * 获取：创建人id
	 */
	public String getCreatorId() {
		return creatorId;
	}
}
