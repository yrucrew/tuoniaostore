package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Date;



/**
 * 商店属性
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainShopProperties implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//归属者通行证ID
	private String userId;
	//归属渠道
	private String channelId;
	//商店的名字
	private String shopName;
	//当前仓库状态 0表示关仓 1表示开仓 2不对外开放(理论上只有一个--直营主仓)
	private Integer status;
	//仓库类型 如：0-直营仓、1-虚拟仓、2-第三方仓、3-合作仓、4-代理仓,5供应商 6.普通门店 7.直营店,8.批发,9.厂商（供应商小程序：一个user_id 只能有一个 5 8 9）
	private Integer type;
	//归属ID
	private String ownerId;
	//管理员姓名
	private String adminName;
	//联系人号码
	private String phoneNumber;
	//归属的逻辑区域ID
	private String logicAreaId;
	private String logicAreaName;
	//所在省份
	private String province;
	//所在城市
	private String city;
	//所在区域
	private String district;
	//街道
	private String street;

	//具体地址
	private String address;
	//所在地址经纬度 格式：longitude,latitude
	private String location;
	private String longitude;
	private String latitude;

	//父级仓库ID 一级仓时为0
	private String parentId;
	private String parentName;
	//是否同步父级仓库存 0-独立库存 1-本仓+父级仓库存 2-仅读取父级库存
	private Integer syncParentStock;
	//配送类型 0-我方配送 1-独立配送
	private Integer distributionType;
	//覆盖距离 单位：米
	//测试
	private Long coveringDistance;
	//配送费用 单位：分
	private Long distributionCost;
	//起订金额(废了)
	private Long minPrice;

	private String distributionCostStr;
	//打印类型 1：下单后 2：支付后 3：接单后 可扩展
	private Integer printerType;
	//绑定的小票打印机编码
	private String bindPrinterCode;
	//建立时间
	private Date createTime;
	// 审核 0-未审核，1-审核通过
	private Integer auth;
	//起订量
	private Long moq;
	//营业执照编号
	private String businessLicenseNumber;
	//排序
	private Integer sort;

	//删除状态 0正常  1删除
	private int retrieveStatus;

	private long totalMoney; //计算所有收入 不存放数据库

	//添加的时候 做字段接收
	private String password; //密码

	//查看绑定的供应商  或者门店 不存放数据库
	private String bindShopName;
	private String userName;
	private boolean checkFlag; //true 已选
	private double discount;

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public Long getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Long minPrice) {
		this.minPrice = minPrice;
	}

	public boolean isCheckFlag() {
		return checkFlag;
	}

	public void setCheckFlag(boolean checkFlag) {
		this.checkFlag = checkFlag;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBindShopName() {
		return bindShopName;
	}

	public void setBindShopName(String bindShopName) {
		this.bindShopName = bindShopName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getBusinessLicenseNumber() {
		return businessLicenseNumber;
	}

	public void setBusinessLicenseNumber(String businessLicenseNumber) {
		this.businessLicenseNumber = businessLicenseNumber;
	}

	public long getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(long totalMoney) {
		this.totalMoney = totalMoney;
	}

	public int getRetrieveStatus() {
		return retrieveStatus;
	}

	public Integer getAuth() {
		return auth;
	}

	public void setAuth(Integer auth) {
		this.auth = auth;
	}

	public void setRetrieveStatus(int retrieveStatus) {
		this.retrieveStatus = retrieveStatus;
	}

	public Long getMoq() {
		return moq;
	}

	public void setMoq(Long moq) {
		this.moq = moq;
	}

	public String getLogicAreaName() {
		return logicAreaName;
	}

	public void setLogicAreaName(String logicAreaName) {
		this.logicAreaName = logicAreaName;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getDistributionCostStr() {
        return distributionCostStr;
    }

    public void setDistributionCostStr(String distributionCostStr) {
        this.distributionCostStr = distributionCostStr;
    }

    /**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：归属者通行证ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：归属者通行证ID
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：归属渠道
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：归属渠道
	 */
	public String getChannelId() {
		return channelId;
	}
	/**
	 * 设置：仓库的名字
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	/**
	 * 获取：仓库的名字
	 */
	public String getShopName() {
		return shopName;
	}
	/**
	 * 设置：当前仓库状态 0表示关仓 1表示开仓 2不对外开放(理论上只有一个--直营主仓)
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：当前仓库状态 0表示关仓 1表示开仓 2不对外开放(理论上只有一个--直营主仓)
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：仓库类型 如：0-直营仓、1-虚拟仓、2-第三方仓、3-合作仓、4-代理仓等
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：仓库类型 如：0-直营仓、1-虚拟仓、2-第三方仓、3-合作仓、4-代理仓等
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：归属ID
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	/**
	 * 获取：归属ID
	 */
	public String getOwnerId() {
		return ownerId;
	}
	/**
	 * 设置：管理员姓名
	 */
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	/**
	 * 获取：管理员姓名
	 */
	public String getAdminName() {
		return adminName;
	}
	/**
	 * 设置：联系人号码
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * 获取：联系人号码
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * 设置：归属的逻辑区域ID
	 */
	public void setLogicAreaId(String logicAreaId) {
		this.logicAreaId = logicAreaId;
	}
	/**
	 * 获取：归属的逻辑区域ID
	 */
	public String getLogicAreaId() {
		return logicAreaId;
	}
	/**
	 * 设置：所在省份
	 */
	public void setProvince(String province) {
		this.province = province;
	}
	/**
	 * 获取：所在省份
	 */
	public String getProvince() {
		return province;
	}
	/**
	 * 设置：所在城市
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * 获取：所在城市
	 */
	public String getCity() {
		return city;
	}
	/**
	 * 设置：所在区域
	 */
	public void setDistrict(String district) {
		this.district = district;
	}
	/**
	 * 获取：所在区域
	 */
	public String getDistrict() {
		return district;
	}
	/**
	 * 设置：具体地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：具体地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：所在地址经纬度 格式：longitude,latitude
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * 获取：所在地址经纬度 格式：longitude,latitude
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * 设置：父级仓库ID 一级仓时为0
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	/**
	 * 获取：父级仓库ID 一级仓时为0
	 */
	public String getParentId() {
		return parentId;
	}
	/**
	 * 设置：是否同步父级仓库存 0-独立库存 1-本仓+父级仓库存 2-仅读取父级库存
	 */
	public void setSyncParentStock(Integer syncParentStock) {
		this.syncParentStock = syncParentStock;
	}
	/**
	 * 获取：是否同步父级仓库存 0-独立库存 1-本仓+父级仓库存 2-仅读取父级库存
	 */
	public Integer getSyncParentStock() {
		return syncParentStock;
	}
	/**
	 * 设置：配送类型 1-我方配送 2-独立配送
	 */
	public void setDistributionType(Integer distributionType) {
		this.distributionType = distributionType;
	}
	/**
	 * 获取：配送类型 1-我方配送 2-独立配送
	 */
	public Integer getDistributionType() {
		return distributionType;
	}
	/**
	 * 设置：覆盖距离 单位：米
	 */
	public void setCoveringDistance(Long coveringDistance) {
		this.coveringDistance = coveringDistance;
	}
	/**
	 * 获取：覆盖距离 单位：米
	 */
	public Long getCoveringDistance() {
		return coveringDistance;
	}
	/**
	 * 设置：配送费用 单位：分
	 */
	public void setDistributionCost(Long distributionCost) {
		this.distributionCost = distributionCost;
	}
	/**
	 * 获取：配送费用 单位：分
	 */
	public Long getDistributionCost() {
		return distributionCost;
	}
	/**
	 * 设置：打印类型 1：下单后 2：支付后 3：接单后 可扩展
	 */
	public void setPrinterType(Integer printerType) {
		this.printerType = printerType;
	}
	/**
	 * 获取：打印类型 1：下单后 2：支付后 3：接单后 可扩展
	 */
	public Integer getPrinterType() {
		return printerType;
	}
	/**
	 * 设置：绑定的小票打印机编码
	 */
	public void setBindPrinterCode(String bindPrinterCode) {
		this.bindPrinterCode = bindPrinterCode;
	}
	/**
	 * 获取：绑定的小票打印机编码
	 */
	public String getBindPrinterCode() {
		return bindPrinterCode;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
