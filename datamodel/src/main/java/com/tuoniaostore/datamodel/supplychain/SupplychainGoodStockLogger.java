package com.tuoniaostore.datamodel.supplychain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 商品的出入库日志
主要是针对商品的库存改变
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainGoodStockLogger implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	// 订单ID
	private String orderId;
	//商品ID
	private String goodId;
	//库存偏移值
	private Integer offsetStock;
	//修改之前的数量
	private Integer beforeStock;
	//修改之后的数量
	private Integer afterStock;
	//操作者角色ID
	private String operatorRoleId;
	//操作者角色名
	private String operatorRoleName;
	// 类型 对应 StockOffsetTypeEnum 枚举
	private Integer type;
	//操作类型 对应 StockOffsetOperationTypeEnum 枚举
	private Integer operationType;
	//操作的时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date operationTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * 设置：商品ID
	 */
	public void setGoodId(String goodId) {
		this.goodId = goodId;
	}
	/**
	 * 获取：商品ID
	 */
	public String getGoodId() {
		return goodId;
	}
	/**
	 * 设置：库存偏移值
	 */
	public void setOffsetStock(Integer offsetStock) {
		this.offsetStock = offsetStock;
	}
	/**
	 * 获取：库存偏移值
	 */
	public Integer getOffsetStock() {
		return offsetStock;
	}
	/**
	 * 设置：修改之前的数量
	 */
	public void setBeforeStock(Integer beforeStock) {
		this.beforeStock = beforeStock;
	}
	/**
	 * 获取：修改之前的数量
	 */
	public Integer getBeforeStock() {
		return beforeStock;
	}
	/**
	 * 设置：修改之后的数量
	 */
	public void setAfterStock(Integer afterStock) {
		this.afterStock = afterStock;
	}
	/**
	 * 获取：修改之后的数量
	 */
	public Integer getAfterStock() {
		return afterStock;
	}
	/**
	 * 设置：操作者角色ID
	 */
	public void setOperatorRoleId(String operatorRoleId) {
		this.operatorRoleId = operatorRoleId;
	}
	/**
	 * 获取：操作者角色ID
	 */
	public String getOperatorRoleId() {
		return operatorRoleId;
	}
	/**
	 * 设置：操作者角色名
	 */
	public void setOperatorRoleName(String operatorRoleName) {
		this.operatorRoleName = operatorRoleName;
	}
	/**
	 * 获取：操作者角色名
	 */
	public String getOperatorRoleName() {
		return operatorRoleName;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 设置：操作类型 1-出库 2-入库(采购) 3-退货 4-调拨
	 */
	public void setOperationType(Integer operationType) {
		this.operationType = operationType;
	}
	/**
	 * 获取：操作类型 1-出库 2-入库(采购) 3-退货 4-调拨
	 */
	public Integer getOperationType() {
		return operationType;
	}
	/**
	 * 设置：操作的时间
	 */
	public void setOperationTime(Date operationTime) {
		this.operationTime = operationTime;
	}
	/**
	 * 获取：操作的时间
	 */
	public Date getOperationTime() {
		return operationTime;
	}
}
