package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;

/**
 * @author oy
 * @description 仓库绑定供应商 门店绑定仓库的
 * @date 2019/5/28
 */
public class SupplychainWarehourseBindRelationship implements Serializable {

    private static final long serialVersionUID = 1L;

    //
    private String id= DefineRandom.getUUID();
    //仓库id
    private String shopId;
    //门店id 或者 供应商id
    private String bindShopId;
    //0 仓库绑定门店，1仓库绑定供应商
    private Integer type;
    //创建时间
    private Date createTime;
    //创建人
    private String userId;

    public SupplychainWarehourseBindRelationship() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getBindShopId() {
        return bindShopId;
    }

    public void setBindShopId(String bindShopId) {
        this.bindShopId = bindShopId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
