package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainRole implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//对应的用户ID
	private String userId;
	//角色名
	private String roleName;
	//显示的标题 用于交互时
	private String title;
	//角色类型
	private Integer type;
	//当前状态
	private Integer status;
	//联系电话
	private String contactPhone;
	//建立时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：对应的用户ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：对应的用户ID
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：角色名
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	/**
	 * 获取：角色名
	 */
	public String getRoleName() {
		return roleName;
	}
	/**
	 * 设置：显示的标题 用于交互时
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：显示的标题 用于交互时
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：角色类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：角色类型
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：当前状态
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：当前状态
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：联系电话
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	/**
	 * 获取：联系电话
	 */
	public String getContactPhone() {
		return contactPhone;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
