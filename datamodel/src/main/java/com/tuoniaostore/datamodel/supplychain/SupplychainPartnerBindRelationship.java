package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainPartnerBindRelationship implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//合作方ID
	private String partnerId;
	//绑定我方仓库ID
	private String bindShopId;
	//对应合作方仓库标识
	private String partnerShopId;
	//关系状态 1、独代
	private Integer status;
	//建立时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：合作方ID
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	/**
	 * 获取：合作方ID
	 */
	public String getPartnerId() {
		return partnerId;
	}
	/**
	 * 设置：绑定我方仓库ID
	 */
	public void setBindShopId(String bindShopId) {
		this.bindShopId = bindShopId;
	}
	/**
	 * 获取：绑定我方仓库ID
	 */
	public String getBindShopId() {
		return bindShopId;
	}
	/**
	 * 设置：对应合作方仓库标识
	 */
	public void setPartnerShopId(String partnerShopId) {
		this.partnerShopId = partnerShopId;
	}
	/**
	 * 获取：对应合作方仓库标识
	 */
	public String getPartnerShopId() {
		return partnerShopId;
	}
	/**
	 * 设置：关系状态 1、独代
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：关系状态 1、独代
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
