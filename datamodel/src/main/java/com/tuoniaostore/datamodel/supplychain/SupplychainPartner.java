package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class 	SupplychainPartner implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//合作方标题 如网址
	private String title;
	//公司名字
	private String companyName;
	//合作类型
	private Integer type;
	//代理属性类型
	private Integer proxyType;
	//合作状态 0-停止合作 1-合作中
	private Integer status;
	//建立时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：合作方标题 如网址
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：合作方标题 如网址
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：公司名字
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	/**
	 * 获取：公司名字
	 */
	public String getCompanyName() {
		return companyName;
	}
	/**
	 * 设置：合作类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：合作类型
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：代理属性类型
	 */
	public void setProxyType(Integer proxyType) {
		this.proxyType = proxyType;
	}
	/**
	 * 获取：代理属性类型
	 */
	public Integer getProxyType() {
		return proxyType;
	}
	/**
	 * 设置：合作状态 0-停止合作 1-合作中
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：合作状态 0-停止合作 1-合作中
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
