package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;


/**
 * 首页按钮配置
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-04-04 14:40:15
 */
public class SupplychainHomepageButtonSetting implements Serializable {
    private static final long serialVersionUID = 1L;

    //主键
    private String id = DefineRandom.getUUID();
    //按钮类型 0=商品分类ID; 1=特殊按钮(详细请参考value);
    private Integer type;
    //按钮名称
    private String name;
    //值 当type=0时此字段为商品分类ID 当type=1时此字段为按钮功能的描述
    private String value;
    //图片url
    private String img;
    //按钮状态 0=有效; 1=无效;
    private Integer status;
    //排序
    private Integer sort;
    //创建时间
    private Date createTime;

    /**
     * 设置：主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取：主键
     */
    public String getId() {
        return id;
    }

    /**
     * 设置：按钮类型 0=商品分类ID; 1=特殊按钮(详细请参考value);
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取：按钮类型 0=商品分类ID; 1=特殊按钮(详细请参考value);
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置：按钮名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取：按钮名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置：值 当type=0时此字段为商品分类ID 当type=1时此字段为按钮功能的描述
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 获取：值 当type=0时此字段为商品分类ID 当type=1时此字段为按钮功能的描述
     */
    public String getValue() {
        return value;
    }

    /**
     * 设置：图片url
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     * 获取：图片url
     */
    public String getImg() {
        return img;
    }

    /**
     * 设置：按钮状态 0=有效; 1=无效;
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取：按钮状态 0=有效; 1=无效;
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置：排序
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 获取：排序
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * 设置：创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取：创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }
}
