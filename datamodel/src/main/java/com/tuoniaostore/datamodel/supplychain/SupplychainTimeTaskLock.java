package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainTimeTaskLock implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//标题
	private String title;
	//类型
	private Integer type;
	//发生的时间 由逻辑决定
	private String happenTime;
	//获得者地址
	private String winnerAddress;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：标题
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：类型
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：发生的时间 由逻辑决定
	 */
	public void setHappenTime(String happenTime) {
		this.happenTime = happenTime;
	}
	/**
	 * 获取：发生的时间 由逻辑决定
	 */
	public String getHappenTime() {
		return happenTime;
	}
	/**
	 * 设置：获得者地址
	 */
	public void setWinnerAddress(String winnerAddress) {
		this.winnerAddress = winnerAddress;
	}
	/**
	 * 获取：获得者地址
	 */
	public String getWinnerAddress() {
		return winnerAddress;
	}
}
