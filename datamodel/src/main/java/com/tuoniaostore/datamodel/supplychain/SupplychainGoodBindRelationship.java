package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 用户 商品类型 绑定关系
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainGoodBindRelationship implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//通行证ID
	private String userId;
	//角色类型，如：供应商、采购员等
	private Integer roleType;
	//商品类型ID
	private String goodTypeId;
	//创建时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：通行证ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：通行证ID
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：角色类型，如：供应商、采购员等
	 */
	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}
	/**
	 * 获取：角色类型，如：供应商、采购员等
	 */
	public Integer getRoleType() {
		return roleType;
	}
	/**
	 * 设置：商品类型ID
	 */
	public void setGoodTypeId(String goodTypeId) {
		this.goodTypeId = goodTypeId;
	}
	/**
	 * 获取：商品类型ID
	 */
	public String getGoodTypeId() {
		return goodTypeId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
