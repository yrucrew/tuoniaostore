package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainPartnerCallback implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//合作方ID
	private String partnerId;
	//操作类型
	private Integer optType;
	//回调地址
	private String callbackUrl;
	//回调延迟
	private Integer callbackDelayed;
	//状态 0-禁用 1-启用
	private Integer status;
	//描述内容
	private String describe;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：合作方ID
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	/**
	 * 获取：合作方ID
	 */
	public String getPartnerId() {
		return partnerId;
	}
	/**
	 * 设置：操作类型
	 */
	public void setOptType(Integer optType) {
		this.optType = optType;
	}
	/**
	 * 获取：操作类型
	 */
	public Integer getOptType() {
		return optType;
	}
	/**
	 * 设置：回调地址
	 */
	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}
	/**
	 * 获取：回调地址
	 */
	public String getCallbackUrl() {
		return callbackUrl;
	}
	/**
	 * 设置：回调延迟
	 */
	public void setCallbackDelayed(Integer callbackDelayed) {
		this.callbackDelayed = callbackDelayed;
	}
	/**
	 * 获取：回调延迟
	 */
	public Integer getCallbackDelayed() {
		return callbackDelayed;
	}
	/**
	 * 设置：状态 0-禁用 1-启用
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态 0-禁用 1-启用
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：描述内容
	 */
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	/**
	 * 获取：描述内容
	 */
	public String getDescribe() {
		return describe;
	}
}
