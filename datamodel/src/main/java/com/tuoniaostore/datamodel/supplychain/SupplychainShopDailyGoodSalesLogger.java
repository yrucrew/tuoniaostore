package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainShopDailyGoodSalesLogger implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//仓库ID
	private String shopId;
	//商品ID
	private String goodId;
	//商品模版ID
	private String goodTemplateId;
	//商品名字
	private String goodName;
	//
	private Integer totalInStorageQuantity;
	//总销售数量
	private Integer totalSalesQuantity;
	//应销售总价
	private Long totalOriginPrice;
	//真实销售总价
	private Long totalSellPrice;
	//总成本价
	private Long totalCostPrice;
	//发生的日期
	private String happenDate;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：仓库ID
	 */
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	/**
	 * 获取：仓库ID
	 */
	public String getShopId() {
		return shopId;
	}
	/**
	 * 设置：商品ID
	 */
	public void setGoodId(String goodId) {
		this.goodId = goodId;
	}
	/**
	 * 获取：商品ID
	 */
	public String getGoodId() {
		return goodId;
	}
	/**
	 * 设置：商品模版ID
	 */
	public void setGoodTemplateId(String goodTemplateId) {
		this.goodTemplateId = goodTemplateId;
	}
	/**
	 * 获取：商品模版ID
	 */
	public String getGoodTemplateId() {
		return goodTemplateId;
	}
	/**
	 * 设置：商品名字
	 */
	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}
	/**
	 * 获取：商品名字
	 */
	public String getGoodName() {
		return goodName;
	}
	/**
	 * 设置：
	 */
	public void setTotalInStorageQuantity(Integer totalInStorageQuantity) {
		this.totalInStorageQuantity = totalInStorageQuantity;
	}
	/**
	 * 获取：
	 */
	public Integer getTotalInStorageQuantity() {
		return totalInStorageQuantity;
	}
	/**
	 * 设置：总销售数量
	 */
	public void setTotalSalesQuantity(Integer totalSalesQuantity) {
		this.totalSalesQuantity = totalSalesQuantity;
	}
	/**
	 * 获取：总销售数量
	 */
	public Integer getTotalSalesQuantity() {
		return totalSalesQuantity;
	}
	/**
	 * 设置：应销售总价
	 */
	public void setTotalOriginPrice(Long totalOriginPrice) {
		this.totalOriginPrice = totalOriginPrice;
	}
	/**
	 * 获取：应销售总价
	 */
	public Long getTotalOriginPrice() {
		return totalOriginPrice;
	}
	/**
	 * 设置：真实销售总价
	 */
	public void setTotalSellPrice(Long totalSellPrice) {
		this.totalSellPrice = totalSellPrice;
	}
	/**
	 * 获取：真实销售总价
	 */
	public Long getTotalSellPrice() {
		return totalSellPrice;
	}
	/**
	 * 设置：总成本价
	 */
	public void setTotalCostPrice(Long totalCostPrice) {
		this.totalCostPrice = totalCostPrice;
	}
	/**
	 * 获取：总成本价
	 */
	public Long getTotalCostPrice() {
		return totalCostPrice;
	}
	/**
	 * 设置：发生的日期
	 */
	public void setHappenDate(String happenDate) {
		this.happenDate = happenDate;
	}
	/**
	 * 获取：发生的日期
	 */
	public String getHappenDate() {
		return happenDate;
	}
}
