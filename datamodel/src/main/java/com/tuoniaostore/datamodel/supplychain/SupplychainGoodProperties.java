package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainGoodProperties implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//商品ID
	private String goodId;
	//属性类型
	private Integer type;
	//属性标志
	private String k;
	//具体属性值
	private String v;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：商品ID
	 */
	public void setGoodId(String goodId) {
		this.goodId = goodId;
	}
	/**
	 * 获取：商品ID
	 */
	public String getGoodId() {
		return goodId;
	}
	/**
	 * 设置：属性类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：属性类型
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：属性标志
	 */
	public void setK(String k) {
		this.k = k;
	}
	/**
	 * 获取：属性标志
	 */
	public String getK() {
		return k;
	}
	/**
	 * 设置：具体属性值
	 */
	public void setV(String v) {
		this.v = v;
	}
	/**
	 * 获取：具体属性值
	 */
	public String getV() {
		return v;
	}
}
