package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;

/**
 * @author oy
 * @description 仓库-供应商-价格id绑定表
 * @date 2019/6/3
 */
public class SupplychainWarehourseSupplierSkuRelationship implements Serializable {

    private static final long serialVersionUID = 1L;

    //
    private String id= DefineRandom.getUUID();
    //仓库id
    private String shopId;
    //价格id
    private String priceId;
    //供应商id
    private String supplierId;
    //创建人
    private String userId;
    //创建时间
    private Date createTime;
    //排序
    private Integer sort;

    public SupplychainWarehourseSupplierSkuRelationship() {

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
