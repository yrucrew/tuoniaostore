package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainShopBindRelationship implements Serializable {
	private static final long serialVersionUID = 1L;
	//
	private String id= DefineRandom.getUUID();
	//仓库ID
	private String shopId;
	//角色ID
	private String roleId;
	//绑定类型，如：/** 1-仓管绑定仓库 2-拣货员绑定仓库 3-配送员绑定仓库  4-采购绑定供应商  5-店员绑定仓库 6-采购绑定仓库 */  ShopBindRelationshipTypeEnum
	private Integer type;
	//当前状态，0-不可用 1可用
	private Integer status;
	//绑定时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：仓库ID
	 */
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	/**
	 * 获取：仓库ID
	 */
	public String getShopId() {
		return shopId;
	}
	/**
	 * 设置：角色ID
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	/**
	 * 获取：角色ID
	 */
	public String getRoleId() {
		return roleId;
	}
	/**
	 * 设置：绑定类型，如：1-仓管 2-拣货员 3-配送员
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：绑定类型，如：1-仓管 2-拣货员 3-配送员
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：当前状态，0-不可用 1可用
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：当前状态，0-不可用 1可用
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：绑定时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：绑定时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
