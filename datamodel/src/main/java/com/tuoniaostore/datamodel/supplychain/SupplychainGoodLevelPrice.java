package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainGoodLevelPrice implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//角色ID(仓库ID)
	private String roleId;
	//关联类型 如：0--商品 1--商品模版
	private Integer relationType;
	//关联实体ID
	private String relationId;
	//优惠类型，如：0--设定价格 1--折扣 2--按原价减免
	private Integer discountType;
	//对应优惠类型的具体值
	private Long levelPrice;
	//匹配的等级模版
	private Integer levelTemplateId;
	//
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：角色ID(仓库ID)
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	/**
	 * 获取：角色ID(仓库ID)
	 */
	public String getRoleId() {
		return roleId;
	}
	/**
	 * 设置：关联类型 如：0--商品 1--商品模版
	 */
	public void setRelationType(Integer relationType) {
		this.relationType = relationType;
	}
	/**
	 * 获取：关联类型 如：0--商品 1--商品模版
	 */
	public Integer getRelationType() {
		return relationType;
	}
	/**
	 * 设置：关联实体ID
	 */
	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}
	/**
	 * 获取：关联实体ID
	 */
	public String getRelationId() {
		return relationId;
	}
	/**
	 * 设置：优惠类型，如：0--设定价格 1--折扣 2--按原价减免
	 */
	public void setDiscountType(Integer discountType) {
		this.discountType = discountType;
	}
	/**
	 * 获取：优惠类型，如：0--设定价格 1--折扣 2--按原价减免
	 */
	public Integer getDiscountType() {
		return discountType;
	}
	/**
	 * 设置：对应优惠类型的具体值
	 */
	public void setLevelPrice(Long levelPrice) {
		this.levelPrice = levelPrice;
	}
	/**
	 * 获取：对应优惠类型的具体值
	 */
	public Long getLevelPrice() {
		return levelPrice;
	}
	/**
	 * 设置：匹配的等级模版
	 */
	public void setLevelTemplateId(Integer levelTemplateId) {
		this.levelTemplateId = levelTemplateId;
	}
	/**
	 * 获取：匹配的等级模版
	 */
	public Integer getLevelTemplateId() {
		return levelTemplateId;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
