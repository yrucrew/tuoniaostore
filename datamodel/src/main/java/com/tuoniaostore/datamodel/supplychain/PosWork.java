package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-06 10:47:35
 */
public class PosWork implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//门店ID
	private String shopId;
	//
	private String userId;

	private String userName;
	private String userAccount;
	//总收入
	private Long totalIncome;
	//总订单数
	private Integer totalOrder;
	//支付宝订单数
	private Integer alipayCount;
	//微信订单数
	private Integer wxCount;
	private Integer refundOrder;
	//现金总条数
	private Integer cashCount;
	//交班输入现金数
	private Long inputCashIncome;
	//支付宝收入
	private Long alipayIncome;
	//微信收入
	private Long wxIncome;
	//现金收入
	private Long cashIncome;
	private Long redundPrice;
	//创建时间
	private Date createTime;
	//上班时间
	private Date loginTime;
	//交班时间
	private Date logoutTime;
	//工作状态 0正常工作 1交接了
	private Integer workStatus;
	//实际清点现金收入
	private Integer practicalCashIncome;

	private long refundPrice;
	private Integer refundOrderNumber;
	private String createTimeStr;
	private String loginTimeStr;
	private String logoutTimeStr;

	public long getRefundPrice() {
		return refundPrice;
	}

	public void setRefundPrice(long refundPrice) {
		this.refundPrice = refundPrice;
	}

	public Integer getRefundOrderNumber() {
		return refundOrderNumber;
	}

	public void setRefundOrderNumber(Integer refundOrderNumber) {
		this.refundOrderNumber = refundOrderNumber;
	}

	public String getCreateTimeStr() {
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

	public String getLoginTimeStr() {
		return loginTimeStr;
	}

	public void setLoginTimeStr(String loginTimeStr) {
		this.loginTimeStr = loginTimeStr;
	}

	public String getLogoutTimeStr() {
		return logoutTimeStr;
	}

	public void setLogoutTimeStr(String logoutTimeStr) {
		this.logoutTimeStr = logoutTimeStr;
	}

	public Integer getRefundOrder() {
		return refundOrder;
	}

	public void setRefundOrder(Integer refundOrder) {
		this.refundOrder = refundOrder;
	}

	public Long getRedundPrice() {
		return redundPrice;
	}

	public void setRedundPrice(Long redundPrice) {
		this.redundPrice = redundPrice;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getPracticalCashIncome() {
		return practicalCashIncome;
	}

	public void setPracticalCashIncome(Integer practicalCashIncome) {
		this.practicalCashIncome = practicalCashIncome;
	}

	public PosWork() {
	}

	public Integer getWorkStatus() {
		return workStatus;
	}

	public void setWorkStatus(Integer workStatus) {
		this.workStatus = workStatus;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：门店ID
	 */
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	/**
	 * 获取：门店ID
	 */
	public String getShopId() {
		return shopId;
	}
	/**
	 * 设置：
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：总收入
	 */
	public void setTotalIncome(Long totalIncome) {
		this.totalIncome = totalIncome;
	}
	/**
	 * 获取：总收入
	 */
	public Long getTotalIncome() {
		return totalIncome;
	}
	/**
	 * 设置：总订单数
	 */
	public void setTotalOrder(Integer totalOrder) {
		this.totalOrder = totalOrder;
	}
	/**
	 * 获取：总订单数
	 */
	public Integer getTotalOrder() {
		return totalOrder;
	}
	/**
	 * 设置：支付宝订单数
	 */
	public void setAlipayCount(Integer alipayCount) {
		this.alipayCount = alipayCount;
	}
	/**
	 * 获取：支付宝订单数
	 */
	public Integer getAlipayCount() {
		return alipayCount;
	}
	/**
	 * 设置：微信订单数
	 */
	public void setWxCount(Integer wxCount) {
		this.wxCount = wxCount;
	}
	/**
	 * 获取：微信订单数
	 */
	public Integer getWxCount() {
		return wxCount;
	}
	/**
	 * 设置：现金总条数
	 */
	public void setCashCount(Integer cashCount) {
		this.cashCount = cashCount;
	}
	/**
	 * 获取：现金总条数
	 */
	public Integer getCashCount() {
		return cashCount;
	}
	/**
	 * 设置：交班输入现金数
	 */
	public void setInputCashIncome(Long inputCashIncome) {
		this.inputCashIncome = inputCashIncome;
	}
	/**
	 * 获取：交班输入现金数
	 */
	public Long getInputCashIncome() {
		return inputCashIncome;
	}
	/**
	 * 设置：支付宝收入
	 */
	public void setAlipayIncome(Long alipayIncome) {
		this.alipayIncome = alipayIncome;
	}
	/**
	 * 获取：支付宝收入
	 */
	public Long getAlipayIncome() {
		return alipayIncome;
	}
	/**
	 * 设置：微信收入
	 */
	public void setWxIncome(Long wxIncome) {
		this.wxIncome = wxIncome;
	}
	/**
	 * 获取：微信收入
	 */
	public Long getWxIncome() {
		return wxIncome;
	}
	/**
	 * 设置：现金收入
	 */
	public void setCashIncome(Long cashIncome) {
		this.cashIncome = cashIncome;
	}
	/**
	 * 获取：现金收入
	 */
	public Long getCashIncome() {
		return cashIncome;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
