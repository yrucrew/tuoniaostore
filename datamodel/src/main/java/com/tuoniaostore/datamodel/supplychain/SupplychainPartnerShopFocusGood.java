package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 *
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-07 09:58:49
 */
public class SupplychainPartnerShopFocusGood implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//合作方ID
	private String partnerId;
	//我方仓库ID
	private String shopId;
	//关注的类型 如：0-商品 1-商品类型
	private Integer focusType;
	//关注的实体数据ID 当focus_type为0时 表示商品ID 当focus_type为1时 表示商品模版ID
	private String focusEntryId;
	//状态 0-无效 1-有效
	private Integer statuc;
	//建立时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：合作方ID
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	/**
	 * 获取：合作方ID
	 */
	public String getPartnerId() {
		return partnerId;
	}
	/**
	 * 设置：我方仓库ID
	 */
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	/**
	 * 获取：我方仓库ID
	 */
	public String getShopId() {
		return shopId;
	}
	/**
	 * 设置：关注的类型 如：0-商品 1-商品类型
	 */
	public void setFocusType(Integer focusType) {
		this.focusType = focusType;
	}
	/**
	 * 获取：关注的类型 如：0-商品 1-商品类型
	 */
	public Integer getFocusType() {
		return focusType;
	}
	/**
	 * 设置：关注的实体数据ID 当focus_type为0时 表示商品ID 当focus_type为1时 表示商品模版ID
	 */
	public void setFocusEntryId(String focusEntryId) {
		this.focusEntryId = focusEntryId;
	}
	/**
	 * 获取：关注的实体数据ID 当focus_type为0时 表示商品ID 当focus_type为1时 表示商品模版ID
	 */
	public String getFocusEntryId() {
		return focusEntryId;
	}
	/**
	 * 设置：状态 0-无效 1-有效
	 */
	public void setStatuc(Integer statuc) {
		this.statuc = statuc;
	}
	/**
	 * 获取：状态 0-无效 1-有效
	 */
	public Integer getStatuc() {
		return statuc;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
