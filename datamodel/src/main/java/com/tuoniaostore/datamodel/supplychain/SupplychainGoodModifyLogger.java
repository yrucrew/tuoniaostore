package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 商品库存修改日志
注意：该表主要记录人为介入修改的数据
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainGoodModifyLogger implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//修改者角色ID 系统修改时为0
	private String roleId;
	//修改者角色名
	private String roleName;
	//商品ID
	private String goodId;
	//商品模版ID
	private String goodTemplateId;
	//修改前商品数量
	private Integer beforeStock;
	//修改后商品数量
	private Integer afterStock;
	//操作类型 0-出库 1-入库 3-盘点修复
	private Integer type;
	//修改时间
	private Date modifyTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：修改者角色ID 系统修改时为0
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	/**
	 * 获取：修改者角色ID 系统修改时为0
	 */
	public String getRoleId() {
		return roleId;
	}
	/**
	 * 设置：修改者角色名
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	/**
	 * 获取：修改者角色名
	 */
	public String getRoleName() {
		return roleName;
	}
	/**
	 * 设置：商品ID
	 */
	public void setGoodId(String goodId) {
		this.goodId = goodId;
	}
	/**
	 * 获取：商品ID
	 */
	public String getGoodId() {
		return goodId;
	}
	/**
	 * 设置：商品模版ID
	 */
	public void setGoodTemplateId(String goodTemplateId) {
		this.goodTemplateId = goodTemplateId;
	}
	/**
	 * 获取：商品模版ID
	 */
	public String getGoodTemplateId() {
		return goodTemplateId;
	}
	/**
	 * 设置：修改前商品数量
	 */
	public void setBeforeStock(Integer beforeStock) {
		this.beforeStock = beforeStock;
	}
	/**
	 * 获取：修改前商品数量
	 */
	public Integer getBeforeStock() {
		return beforeStock;
	}
	/**
	 * 设置：修改后商品数量
	 */
	public void setAfterStock(Integer afterStock) {
		this.afterStock = afterStock;
	}
	/**
	 * 获取：修改后商品数量
	 */
	public Integer getAfterStock() {
		return afterStock;
	}
	/**
	 * 设置：操作类型 0-出库 1-入库 3-盘点修复
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：操作类型 0-出库 1-入库 3-盘点修复
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：修改时间
	 */
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getModifyTime() {
		return modifyTime;
	}
}
