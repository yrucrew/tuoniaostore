package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 门店商品销售统计
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-05-20 20:05:41
 */
public class SupplychainShopGoodSale implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//id
	private String id= DefineRandom.getUUID();
	//商店id
    private String shopId;
	//商品模板id
	private String goodTempalteId;
	//商品名称
	private String goodName;
	//条码
	private String goodBarcode;
	//商品单位
	private String goodUnit;
	//商品单位id
	private String goodUnitId;
	//商品类型id
	private String goodTypeId;
	//商品类型 一级分类
	private String goodType;
	//销售数量
	private BigDecimal saleNum;
	//销售金额
	private BigDecimal saleMoney;
	//销售的时间 存放当前的
	private Date saleTime;
	//
	private Date createTime;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
    /**
	 * 设置：id
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：id
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：商品模板id
	 */
	public void setGoodTempalteId(String goodTempalteId) {
		this.goodTempalteId = goodTempalteId;
	}
	/**
	 * 获取：商品模板id
	 */
	public String getGoodTempalteId() {
		return goodTempalteId;
	}
	/**
	 * 设置：商品名称
	 */
	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}
	/**
	 * 获取：商品名称
	 */
	public String getGoodName() {
		return goodName;
	}
	/**
	 * 设置：条码
	 */
	public void setGoodBarcode(String goodBarcode) {
		this.goodBarcode = goodBarcode;
	}
	/**
	 * 获取：条码
	 */
	public String getGoodBarcode() {
		return goodBarcode;
	}
	/**
	 * 设置：商品单位
	 */
	public void setGoodUnit(String goodUnit) {
		this.goodUnit = goodUnit;
	}
	/**
	 * 获取：商品单位
	 */
	public String getGoodUnit() {
		return goodUnit;
	}
	/**
	 * 设置：商品单位id
	 */
	public void setGoodUnitId(String goodUnitId) {
		this.goodUnitId = goodUnitId;
	}
	/**
	 * 获取：商品单位id
	 */
	public String getGoodUnitId() {
		return goodUnitId;
	}
	/**
	 * 设置：商品类型id
	 */
	public void setGoodTypeId(String goodTypeId) {
		this.goodTypeId = goodTypeId;
	}
	/**
	 * 获取：商品类型id
	 */
	public String getGoodTypeId() {
		return goodTypeId;
	}
	/**
	 * 设置：商品类型
	 */
	public void setGoodType(String goodType) {
		this.goodType = goodType;
	}
	/**
	 * 获取：商品类型
	 */
	public String getGoodType() {
		return goodType;
	}

	public BigDecimal getSaleNum() {
		return saleNum;
	}

	public void setSaleNum(BigDecimal saleNum) {
		this.saleNum = saleNum;
	}

	public BigDecimal getSaleMoney() {
		return saleMoney;
	}

	public void setSaleMoney(BigDecimal saleMoney) {
		this.saleMoney = saleMoney;
	}

	/**
	 * 设置：销售的时间
	 */
	public void setSaleTime(Date saleTime) {
		this.saleTime = saleTime;
	}
	/**
	 * 获取：销售的时间
	 */
	public Date getSaleTime() {
		return saleTime;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
