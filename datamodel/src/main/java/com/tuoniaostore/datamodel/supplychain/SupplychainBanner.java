package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;



/**
 * 供应链公告
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */


public class SupplychainBanner implements Serializable {
	private static final long serialVersionUID = 1L;


	private String id= DefineRandom.getUUID();
	//城市编码
	private String adcode;

	//自定义区域ID
	private String defineAreaId;

	//仓库ID
	private String shopId;

	//仓库名称
	private String shopName;

	//banner类型，类型：0:分类连接，商品详情，跳转图片，专题活动 10:pos
	private Integer type;
	//排序 越小越前
	private Integer sort;
	//图片链接、用于展示
	private String imageUrl;
	//点击的跳转链接，与type结合
	private String clickUrl;
	//当前状态，1、开启中 0、关闭中
	private Integer status;
	//开启时间
	private Date openTime;
	//关闭时间
	private Date closeTime;
	//建立时间
	private Date createTime;


	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：城市编码
	 */
	public void setAdcode(String adcode) {
		this.adcode = adcode;
	}
	/**
	 * 获取：城市编码
	 */
	public String getAdcode() {
		return adcode;
	}
	/**
	 * 设置：自定义区域ID
	 */
	public void setDefineAreaId(String defineAreaId) {
		this.defineAreaId = defineAreaId;
	}
	/**
	 * 获取：自定义区域ID
	 */
	public String getDefineAreaId() {
		return defineAreaId;
	}
	/**
	 * 设置：仓库ID
	 */
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	/**
	 * 获取：仓库ID
	 */
	public String getShopId() {
		return shopId;
	}
	/**
	 * 设置：banner类型，类型：分类连接，商品详情，跳转图片，专题活动等等
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：banner类型，类型：分类连接，商品详情，跳转图片，专题活动等等
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：排序 越小越前
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序 越小越前
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：图片链接、用于展示
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	/**
	 * 获取：图片链接、用于展示
	 */
	public String getImageUrl() {
		return imageUrl;
	}
	/**
	 * 设置：点击的跳转链接，与type结合
	 */
	public void setClickUrl(String clickUrl) {
		this.clickUrl = clickUrl;
	}
	/**
	 * 获取：点击的跳转链接，与type结合
	 */
	public String getClickUrl() {
		return clickUrl;
	}
	/**
	 * 设置：当前状态，1、开启中 0、关闭中
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：当前状态，1、开启中 0、关闭中
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：开启时间
	 */
	public void setOpenTime(Date openTime) {
		this.openTime = openTime;
	}
	/**
	 * 获取：开启时间
	 */
	public Date getOpenTime() {
		return openTime;
	}
	/**
	 * 设置：关闭时间
	 */
	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}
	/**
	 * 获取：关闭时间
	 */
	public Date getCloseTime() {
		return closeTime;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
