package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainUserDailyBuyLogger implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//通行证ID
	private String userId;
	//商品ID
	private String itemId;
	//对应的商品模版ID
	private String itemTemplateId;
	//已购买数量
	private Integer hasBuyCount;
	//发生日期
	private Date happenDate;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：通行证ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：通行证ID
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：商品ID
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	/**
	 * 获取：商品ID
	 */
	public String getItemId() {
		return itemId;
	}
	/**
	 * 设置：对应的商品模版ID
	 */
	public void setItemTemplateId(String itemTemplateId) {
		this.itemTemplateId = itemTemplateId;
	}
	/**
	 * 获取：对应的商品模版ID
	 */
	public String getItemTemplateId() {
		return itemTemplateId;
	}
	/**
	 * 设置：已购买数量
	 */
	public void setHasBuyCount(Integer hasBuyCount) {
		this.hasBuyCount = hasBuyCount;
	}
	/**
	 * 获取：已购买数量
	 */
	public Integer getHasBuyCount() {
		return hasBuyCount;
	}
	/**
	 * 设置：发生日期
	 */
	public void setHappenDate(Date happenDate) {
		this.happenDate = happenDate;
	}
	/**
	 * 获取：发生日期
	 */
	public Date getHappenDate() {
		return happenDate;
	}
}
