package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;
import com.tuoniaostore.datamodel.good.GoodBarcode;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 供应链商品
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainGood implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id= DefineRandom.getUUID();
	//对应的模版ID
	private String goodTemplateId;
	//对应的模板NAME
	private String goodTemplateName;

	//自定义商品名字 为空时使用模版名字
	private String defineName;
	//自定义商品图片 为空时使用模版图片
	private String defineImage;
	//默认排序 值越小 越靠前
	private Integer defaultSort;
	//归属仓库ID
	private String shopId;
	//仓库名称
	private String shopName;
	//商品批次
	private Integer productBatches;
	//批次编码
	private String batchesCode;
	//当前库存(真实库存) 欠货时为负数
	private Integer stock;
	//挂起数量(已支付但未出库) 订单取消时需回滚至库存
	private Integer pendingQuantity;
	//预警数量
	private Integer warningQuantity;
	//保持数量
	private Integer keepQuantity;
	//用于展示的超卖数量(超卖+库存)
	private Integer oversoldQuantity;
	//最低的销售数量
	private Integer minimumSellCount;
	//调拨中的数量
	private Integer allocationQuantity;
	//采购中的数量
	private Integer purchaseQuantity;
	//待调拨数量(存在主仓且读取主仓库存时有效)
	private Integer waitAllocationQuantity;
	//当前状态 -1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效
	private Integer status;
	//成本价
	private Long costPrice;
	//一般销售价
	private Long sellPrice;
	//一般市场价
	private Long marketPrice;
	//促销价
	private Long discountPrice;

	//建议售价
	private Long salePrice;
	//同行价
	private Long colleaguePrice;
	//会员价
	private Long memberPrice;

	//促销类型 0-不参与促销 1-促销类型 2-特价类型
	private Integer discountType;
	//限购数量 -1-不限购 0-不出售 其他正数表示限购数量
	private Integer restrictionQuantity;
	//控制超量的情况 0：不可超出限制 1：超出时以最高价购买
	private Integer beyondControll;
	//延迟配送天数
	private Integer deliveryDelay;
	//起始销量
	private Integer initialSales;
	//实际销量(展示时为：起始销量+实际销量) -- 总出库
	private Integer actualSales;
	//总入库数量
	private Long totalStorage;
	//总出库数量
	private Long totalOutStorage;
	//商品描述
	private String description;
	//入库时间
	private Date createTime;
	//
	private Long relationItemId;
	//价格id
	private String priceId;
	private String priceName;
	//商品是否可退
	private Integer goodReturn;
	//商品类别id
	private String goodTypeId;
	//起订量
	private Integer startingQuantity;

	//一级类id name （数据展示 不存数据库）
	private String goodTypeIdOne;
	private String goodTypeNameOne;

	private String unitNmae;
	private String goodTypeName;

	public Integer getStartingQuantity() {
		return startingQuantity;
	}

	public void setStartingQuantity(Integer startingQuantity) {
		this.startingQuantity = startingQuantity;
	}

	public String getGoodTypeIdOne() {
		return goodTypeIdOne;
	}

	public void setGoodTypeIdOne(String goodTypeIdOne) {
		this.goodTypeIdOne = goodTypeIdOne;
	}

	public String getGoodTypeNameOne() {
		return goodTypeNameOne;
	}

	public void setGoodTypeNameOne(String goodTypeNameOne) {
		this.goodTypeNameOne = goodTypeNameOne;
	}

	public String getUnitNmae() {
		return unitNmae;
	}

	public void setUnitNmae(String unitNmae) {
		this.unitNmae = unitNmae;
	}

	public String getPriceName() {
		return priceName;
	}

	public void setPriceName(String priceName) {
		this.priceName = priceName;
	}

	public String getGoodTypeName() {
		return goodTypeName;
	}

	public void setGoodTypeName(String goodTypeName) {
		this.goodTypeName = goodTypeName;
	}

	private List<GoodBarcode> barCode;

	public List<GoodBarcode> getBarCode() {
		return barCode;
	}

	public void setBarCode(List<GoodBarcode> barCode) {
		this.barCode = barCode;
	}

	public String getGoodTypeId() {
		return goodTypeId;
	}

	public void setGoodTypeId(String goodTypeId) {
		this.goodTypeId = goodTypeId;
	}

	public String getPriceId() {
		return priceId;
	}

	public Integer getGoodReturn() {
		return goodReturn;
	}

	public void setGoodReturn(Integer goodReturn) {
		this.goodReturn = goodReturn;
	}

	public void setPriceId(String priceId) {
		this.priceId = priceId;
	}

	public String getGoodTemplateName() {
		return goodTemplateName;
	}

	public void setGoodTemplateName(String goodTemplateName) {
		this.goodTemplateName = goodTemplateName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Long getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Long salePrice) {
		this.salePrice = salePrice;
	}

	public Long getColleaguePrice() {
		return colleaguePrice;
	}

	public void setColleaguePrice(Long colleaguePrice) {
		this.colleaguePrice = colleaguePrice;
	}

	public Long getMemberPrice() {
		return memberPrice;
	}

	public void setMemberPrice(Long memberPrice) {
		this.memberPrice = memberPrice;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：对应的模版ID
	 */
	public void setGoodTemplateId(String goodTemplateId) {
		this.goodTemplateId = goodTemplateId;
	}
	/**
	 * 获取：对应的模版ID
	 */
	public String getGoodTemplateId() {
		return goodTemplateId;
	}
	/**
	 * 设置：自定义商品名字 为空时使用模版名字
	 */
	public void setDefineName(String defineName) {
		this.defineName = defineName;
	}
	/**
	 * 获取：自定义商品名字 为空时使用模版名字
	 */
	public String getDefineName() {
		return defineName;
	}
	/**
	 * 设置：自定义商品图片 为空时使用模版图片
	 */
	public void setDefineImage(String defineImage) {
		this.defineImage = defineImage;
	}
	/**
	 * 获取：自定义商品图片 为空时使用模版图片
	 */
	public String getDefineImage() {
		return defineImage;
	}
	/**
	 * 设置：默认排序 值越小 越靠前
	 */
	public void setDefaultSort(Integer defaultSort) {
		this.defaultSort = defaultSort;
	}
	/**
	 * 获取：默认排序 值越小 越靠前
	 */
	public Integer getDefaultSort() {
		return defaultSort;
	}
	/**
	 * 设置：归属仓库ID
	 */
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	/**
	 * 获取：归属仓库ID
	 */
	public String getShopId() {
		return shopId;
	}
	/**
	 * 设置：商品批次
	 */
	public void setProductBatches(Integer productBatches) {
		this.productBatches = productBatches;
	}
	/**
	 * 获取：商品批次
	 */
	public Integer getProductBatches() {
		return productBatches;
	}
	/**
	 * 设置：批次编码
	 */
	public void setBatchesCode(String batchesCode) {
		this.batchesCode = batchesCode;
	}
	/**
	 * 获取：批次编码
	 */
	public String getBatchesCode() {
		return batchesCode;
	}
	/**
	 * 设置：当前库存(真实库存) 欠货时为负数
	 */
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	/**
	 * 获取：当前库存(真实库存) 欠货时为负数
	 */
	public Integer getStock() {
		return stock;
	}
	/**
	 * 设置：挂起数量(已支付但未出库) 订单取消时需回滚至库存
	 */
	public void setPendingQuantity(Integer pendingQuantity) {
		this.pendingQuantity = pendingQuantity;
	}
	/**
	 * 获取：挂起数量(已支付但未出库) 订单取消时需回滚至库存
	 */
	public Integer getPendingQuantity() {
		return pendingQuantity;
	}
	/**
	 * 设置：预警数量
	 */
	public void setWarningQuantity(Integer warningQuantity) {
		this.warningQuantity = warningQuantity;
	}
	/**
	 * 获取：预警数量
	 */
	public Integer getWarningQuantity() {
		return warningQuantity;
	}
	/**
	 * 设置：保持数量
	 */
	public void setKeepQuantity(Integer keepQuantity) {
		this.keepQuantity = keepQuantity;
	}
	/**
	 * 获取：保持数量
	 */
	public Integer getKeepQuantity() {
		return keepQuantity;
	}
	/**
	 * 设置：用于展示的超卖数量(超卖+库存)
	 */
	public void setOversoldQuantity(Integer oversoldQuantity) {
		this.oversoldQuantity = oversoldQuantity;
	}
	/**
	 * 获取：用于展示的超卖数量(超卖+库存)
	 */
	public Integer getOversoldQuantity() {
		return oversoldQuantity;
	}
	/**
	 * 设置：最低的销售数量
	 */
	public void setMinimumSellCount(Integer minimumSellCount) {
		this.minimumSellCount = minimumSellCount;
	}
	/**
	 * 获取：最低的销售数量
	 */
	public Integer getMinimumSellCount() {
		return minimumSellCount;
	}
	/**
	 * 设置：调拨中的数量
	 */
	public void setAllocationQuantity(Integer allocationQuantity) {
		this.allocationQuantity = allocationQuantity;
	}
	/**
	 * 获取：调拨中的数量
	 */
	public Integer getAllocationQuantity() {
		return allocationQuantity;
	}
	/**
	 * 设置：采购中的数量
	 */
	public void setPurchaseQuantity(Integer purchaseQuantity) {
		this.purchaseQuantity = purchaseQuantity;
	}
	/**
	 * 获取：采购中的数量
	 */
	public Integer getPurchaseQuantity() {
		return purchaseQuantity;
	}
	/**
	 * 设置：待调拨数量(存在主仓且读取主仓库存时有效)
	 */
	public void setWaitAllocationQuantity(Integer waitAllocationQuantity) {
		this.waitAllocationQuantity = waitAllocationQuantity;
	}
	/**
	 * 获取：待调拨数量(存在主仓且读取主仓库存时有效)
	 */
	public Integer getWaitAllocationQuantity() {
		return waitAllocationQuantity;
	}
	/**
	 * 设置：当前状态 -1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：当前状态 -1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：成本价
	 */
	public void setCostPrice(Long costPrice) {
		this.costPrice = costPrice;
	}
	/**
	 * 获取：成本价
	 */
	public Long getCostPrice() {
		return costPrice;
	}
	/**
	 * 设置：一般销售价
	 */
	public void setSellPrice(Long sellPrice) {
		this.sellPrice = sellPrice;
	}
	/**
	 * 获取：一般销售价
	 */
	public Long getSellPrice() {
		return sellPrice;
	}
	/**
	 * 设置：一般市场价
	 */
	public void setMarketPrice(Long marketPrice) {
		this.marketPrice = marketPrice;
	}
	/**
	 * 获取：一般市场价
	 */
	public Long getMarketPrice() {
		return marketPrice;
	}
	/**
	 * 设置：促销价
	 */
	public void setDiscountPrice(Long discountPrice) {
		this.discountPrice = discountPrice;
	}
	/**
	 * 获取：促销价
	 */
	public Long getDiscountPrice() {
		return discountPrice;
	}
	/**
	 * 设置：促销类型 0-不参与促销 1-促销类型 2-特价类型
	 */
	public void setDiscountType(Integer discountType) {
		this.discountType = discountType;
	}
	/**
	 * 获取：促销类型 0-不参与促销 1-促销类型 2-特价类型
	 */
	public Integer getDiscountType() {
		return discountType;
	}
	/**
	 * 设置：限购数量 -1-不限购 0-不出售 其他正数表示限购数量
	 */
	public void setRestrictionQuantity(Integer restrictionQuantity) {
		this.restrictionQuantity = restrictionQuantity;
	}
	/**
	 * 获取：限购数量 -1-不限购 0-不出售 其他正数表示限购数量
	 */
	public Integer getRestrictionQuantity() {
		return restrictionQuantity;
	}
	/**
	 * 设置：控制超量的情况 0：不可超出限制 1：超出时以最高价购买
	 */
	public void setBeyondControll(Integer beyondControll) {
		this.beyondControll = beyondControll;
	}
	/**
	 * 获取：控制超量的情况 0：不可超出限制 1：超出时以最高价购买
	 */
	public Integer getBeyondControll() {
		return beyondControll;
	}
	/**
	 * 设置：延迟配送天数
	 */
	public void setDeliveryDelay(Integer deliveryDelay) {
		this.deliveryDelay = deliveryDelay;
	}
	/**
	 * 获取：延迟配送天数
	 */
	public Integer getDeliveryDelay() {
		return deliveryDelay;
	}
	/**
	 * 设置：起始销量
	 */
	public void setInitialSales(Integer initialSales) {
		this.initialSales = initialSales;
	}
	/**
	 * 获取：起始销量
	 */
	public Integer getInitialSales() {
		return initialSales;
	}
	/**
	 * 设置：实际销量(展示时为：起始销量+实际销量) -- 总出库
	 */
	public void setActualSales(Integer actualSales) {
		this.actualSales = actualSales;
	}
	/**
	 * 获取：实际销量(展示时为：起始销量+实际销量) -- 总出库
	 */
	public Integer getActualSales() {
		return actualSales;
	}
	/**
	 * 设置：总入库数量
	 */
	public void setTotalStorage(Long totalStorage) {
		this.totalStorage = totalStorage;
	}
	/**
	 * 获取：总入库数量
	 */
	public Long getTotalStorage() {
		return totalStorage;
	}
	/**
	 * 设置：总出库数量
	 */
	public void setTotalOutStorage(Long totalOutStorage) {
		this.totalOutStorage = totalOutStorage;
	}
	/**
	 * 获取：总出库数量
	 */
	public Long getTotalOutStorage() {
		return totalOutStorage;
	}
	/**
	 * 设置：商品描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * 获取：商品描述
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 设置：入库时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：入库时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setRelationItemId(Long relationItemId) {
		this.relationItemId = relationItemId;
	}
	/**
	 * 获取：
	 */
	public Long getRelationItemId() {
		return relationItemId;
	}
}
