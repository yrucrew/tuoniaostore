package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-28 13:46:09
 */
public class SuppliychainPrice implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//supplychain_good供应链ID
	private String goodId;
	//成本价
	private Long costPrice;
	//建议售价
	private Long salePrice;
	//交易价
	private Long sellPrice;
	//同行价
	private Long colleaguePrice;
	//市场价
	private Long marketPrice;
	//会员价
	private Long memberPrice;
	//促销价
	private Long discountPrice;
	//数量
	private Integer num;
	//单位名称
	private String unitId;
	//
	private String priceId;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：supplychain_good供应链ID
	 */
	public void setGoodId(String goodId) {
		this.goodId = goodId;
	}
	/**
	 * 获取：supplychain_good供应链ID
	 */
	public String getGoodId() {
		return goodId;
	}
	/**
	 * 设置：成本价
	 */
	public void setCostPrice(Long costPrice) {
		this.costPrice = costPrice;
	}
	/**
	 * 获取：成本价
	 */
	public Long getCostPrice() {
		return costPrice;
	}
	/**
	 * 设置：建议售价
	 */
	public void setSalePrice(Long salePrice) {
		this.salePrice = salePrice;
	}
	/**
	 * 获取：建议售价
	 */
	public Long getSalePrice() {
		return salePrice;
	}
	/**
	 * 设置：交易价
	 */
	public void setSellPrice(Long sellPrice) {
		this.sellPrice = sellPrice;
	}
	/**
	 * 获取：交易价
	 */
	public Long getSellPrice() {
		return sellPrice;
	}
	/**
	 * 设置：同行价
	 */
	public void setColleaguePrice(Long colleaguePrice) {
		this.colleaguePrice = colleaguePrice;
	}
	/**
	 * 获取：同行价
	 */
	public Long getColleaguePrice() {
		return colleaguePrice;
	}
	/**
	 * 设置：市场价
	 */
	public void setMarketPrice(Long marketPrice) {
		this.marketPrice = marketPrice;
	}
	/**
	 * 获取：市场价
	 */
	public Long getMarketPrice() {
		return marketPrice;
	}
	/**
	 * 设置：会员价
	 */
	public void setMemberPrice(Long memberPrice) {
		this.memberPrice = memberPrice;
	}
	/**
	 * 获取：会员价
	 */
	public Long getMemberPrice() {
		return memberPrice;
	}
	/**
	 * 设置：促销价
	 */
	public void setDiscountPrice(Long discountPrice) {
		this.discountPrice = discountPrice;
	}
	/**
	 * 获取：促销价
	 */
	public Long getDiscountPrice() {
		return discountPrice;
	}
	/**
	 * 设置：数量
	 */
	public void setNum(Integer num) {
		this.num = num;
	}
	/**
	 * 获取：数量
	 */
	public Integer getNum() {
		return num;
	}
	/**
	 * 设置：单位名称
	 */
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	/**
	 * 获取：单位名称
	 */
	public String getUnitId() {
		return unitId;
	}
	/**
	 * 设置：
	 */
	public void setPriceId(String priceId) {
		this.priceId = priceId;
	}
	/**
	 * 获取：
	 */
	public String getPriceId() {
		return priceId;
	}
}
