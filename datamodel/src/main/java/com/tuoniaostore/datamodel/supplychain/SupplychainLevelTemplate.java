package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainLevelTemplate implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//等级值
	private Integer levelValue;
	//其他内容
	private String defineParameter;
	//是否启用 0--无效 1--启动
	private Integer status;
	//建立的时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：等级值
	 */
	public void setLevelValue(Integer levelValue) {
		this.levelValue = levelValue;
	}
	/**
	 * 获取：等级值
	 */
	public Integer getLevelValue() {
		return levelValue;
	}
	/**
	 * 设置：其他内容
	 */
	public void setDefineParameter(String defineParameter) {
		this.defineParameter = defineParameter;
	}
	/**
	 * 获取：其他内容
	 */
	public String getDefineParameter() {
		return defineParameter;
	}
	/**
	 * 设置：是否启用 0--无效 1--启动
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：是否启用 0--无效 1--启动
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：建立的时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立的时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
