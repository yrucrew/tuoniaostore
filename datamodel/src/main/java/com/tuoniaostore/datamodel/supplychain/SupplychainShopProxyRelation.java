package com.tuoniaostore.datamodel.supplychain;

import com.tuoniaostore.datamodel.DefineRandom;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author lyb
 * @email yunbiaoli@163.com
 * @date 2019-03-12 14:57:04
 */
public class SupplychainShopProxyRelation implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private String id= DefineRandom.getUUID();
	//主要角色ID
	private String hostWarehouseId;
	//客人角色ID
	private String guestWarehouseId;
	//关系类型 如：独代、合作等
	private Integer type;
	//关系状态 1--可用 2--不可用
	private Integer status;
	//建立时间
	private Date createTime;
	//排序
	private Integer sort;

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：主要角色ID
	 */
	public void setHostWarehouseId(String hostWarehouseId) {
		this.hostWarehouseId = hostWarehouseId;
	}
	/**
	 * 获取：主要角色ID
	 */
	public String getHostWarehouseId() {
		return hostWarehouseId;
	}
	/**
	 * 设置：客人角色ID
	 */
	public void setGuestWarehouseId(String guestWarehouseId) {
		this.guestWarehouseId = guestWarehouseId;
	}
	/**
	 * 获取：客人角色ID
	 */
	public String getGuestWarehouseId() {
		return guestWarehouseId;
	}
	/**
	 * 设置：关系类型 如：独代、合作等
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：关系类型 如：独代、合作等
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：关系状态 1--可用 2--不可用
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：关系状态 1--可用 2--不可用
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：建立时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：建立时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
