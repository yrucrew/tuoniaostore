package com.tuoniaostore.commons.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.github.wxpay.sdk.WXPayConstants;
import com.github.wxpay.sdk.WXPayUtil;
import com.tuoniaostore.commons.MD5;
import com.tuoniaostore.commons.constant.pay.WeiXinPayTypeEnum;
import com.tuoniaostore.commons.constant.pay.WeiXinTerminalPayTypeEnum;
import com.tuoniaostore.commons.constant.pay.WeixinCallcackType;
import com.tuoniaostore.datamodel.payment.PaymentTransactionLogger;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.*;

/**
 * @author oy
 * @description 微信支付工具类
 * @date 2019/4/19
 */
public class WeiXinPayUtils {


    /**
     * 小程序统一下单
     * @author oy
     * @date 2019/4/20
     * @param out_trade_no：交易订单号（这是我们系统生成的订单号）
     * param  otal_fee：金额（分）
     * param  ip：ip 根据每个用户请求 获取ip
     * param  openId：注册的时候 会添加到sys_user表中的字段（小程序使用，其他传空）
     * param  body：支付信息说明（自己写）
     * param  weiXinTerminalPayType：微信支付类型 详情见 WeiXinTerminalPayTypeEnum
     * param  type：微信回调类型 详情见  WeixinCallcackType
     * param  attach：附加信息
     */
    public static Map<String,String>  createOrderBySmallProgram(String out_trade_no, String total_fee,String ip,String openId,String body,int weiXinTerminalPayType,int type,String attach){
        //读取配置文件
        WeiXinPayUtils utils = new WeiXinPayUtils();
        Map<String, String> properties = utils.readProperties();

        Map<String,String> aNative = WeiXinPayUtils.createNative(out_trade_no, total_fee, ip,
                openId,body,weiXinTerminalPayType,type,attach);
        String nonce_str = aNative.get("nonce_str");
        Map<String,String> map = new HashMap<>();
        String timeStamp = new Date().getTime() + "";
        map.put("timeStamp",timeStamp);
        map.put("nonceStr",nonce_str);
        String packageString = "prepay_id="+aNative.get("prepay_id");
        map.put("package",packageString);
        String signType = WXPayConstants.SignType.MD5.toString();
        map.put("signType",signType);

        String mchIdkey = properties.get("mchIdkey");
        String paySign = MD5.encode("appId=" + aNative.get("appid") + "&nonceStr=" + nonce_str + "&package=" + packageString + "&signType=" + signType + "&timeStamp=" + timeStamp + "&key=" + mchIdkey);
        map.put("paySign",paySign);//签名好了
        map.put("out_trade_no",out_trade_no);
        return map;//小程序返回结果
    }


    /**
     * 付款码收款
     * @author oy
     * @date 2019/5/2
     * @param out_trade_no：交易订单号（这是我们系统生成的订单号）
     * param  total_fee：金额（分）
     * param  ip：ip 根据每个用户请求 获取ip
     * param  auth_code：终端扫客户的条形码出来的一串编号
     * param  body：支付信息说明（自己写）
     * @return java.util.Map
     */
    public static Map<String,String> createOrderByPos(String out_trade_no, String total_fee,String ip,String auth_code,String body){
        //读取配置文件
        WeiXinPayUtils utils = new WeiXinPayUtils();
        Map<String, String> properties = utils.readProperties();
        //1.参数封装
        Map param=new HashMap();
        param.put("nonce_str", WXPayUtil.generateNonceStr());//随机字符串
        param.put("mch_id", properties.get("mchId"));//商户
        param.put("out_trade_no", out_trade_no);//交易订单号
        param.put("body", body);
        param.put("total_fee", total_fee);//金额（分）
        param.put("spbill_create_ip", ip);//下单的ip地址
        param.put("appid", properties.get("appId3"));//公众账号ID

        //注：用户付款码条形码规则：18位纯数字，以10、11、12、13、14、15开头
        int substring = Integer.parseInt(auth_code.substring(0, 2));
        if(auth_code.length() == 18 && substring >= 10 && substring<= 15){
            param.put("auth_code",auth_code);//付款码 授权码
        }else{
            return new HashMap();
        }

        try {
            //请求的参数
            String xmlParam = WXPayUtil.generateSignedXml(param, properties.get("mchIdkey"), WXPayConstants.SignType.MD5);//生成签名 MD5方式 并将参数弄成xml形式    mchIdkey
            //2.发送请求
            HttpClient httpClient=new HttpClient("https://api.mch.weixin.qq.com/pay/micropay");
            httpClient.setHttps(true);
            httpClient.setXmlParam(xmlParam);
            httpClient.post();

            //3.获取结果
            String xmlResult = httpClient.getContent();
            Map<String, String> mapResult = WXPayUtil.xmlToMap(xmlResult);
            System.out.println("微信返回结果"+mapResult);
            return mapResult;
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap();
        }
    }

    /**
     * 二维码支付（pos机APP）
     * @author oy
     * @date 2019/5/6
     * @param out_trade_no：交易订单号（这是我们系统生成的订单号）
     * param  otal_fee：金额（分）
     * param  ip：ip 根据每个用户请求 获取ip
     * param  body：支付信息说明（自己写）
     * param  weiXinTerminalPayType：微信支付类型 详情见 WeiXinTerminalPayTypeEnum
     * @return java.util.Map<java.lang.String,java.lang.String>
     */
    public static Map<String,String> createOrderByCode(String out_trade_no, String total_fee,String ip,String body,int type){
        Map<String,String> aNative = WeiXinPayUtils.createNative(out_trade_no, total_fee, ip,
                null,body,WeiXinTerminalPayTypeEnum.APP_CODE.getKey(),type,null);
        return aNative;//code_url=weixin://wxpay/bizpayurl?pr=N8BADys 根据这个字段生成二维码
    }


    /**
     * 小程序统一下单（APP暂时没做，APPID未知）
     * @author oy
     * @date 2019/4/19
     * @param out_trade_no：交易订单号（这是我们系统生成的订单号）
     * param  otal_fee：金额（分）
     * param  ip：ip 根据每个用户请求 获取ip
     * param  openId：注册的时候 会添加到sys_user表中的字段（小程序使用，其他传空
     * param  body：支付信息说明（自己写）
     * param  weiXinTerminalPayType：微信支付类型 详情见 WeiXinTerminalPayTypeEnum
     * param  type：微信支付回调类型 详情见 WeixinCallcackType
     * param  attach：额外参数
     */
    public static Map createNative(String out_trade_no, String total_fee,String ip,String openId,String body,int weiXinTerminalPayType,int type,String attach) {
        //读取配置文件
        WeiXinPayUtils utils = new WeiXinPayUtils();
        Map<String, String> properties = utils.readProperties();

        String url = "";
        if(type == WeixinCallcackType.BUSINESS_SMALL_PROGRAM_ORDER.getKey()){//商家端小程序下订单
            url = properties.get("url");
        }else if(type == WeixinCallcackType.BUSINESS_SMALL_PROGRAM_RECHARGE.getKey()){//商家端小程序充值
            url = properties.get("url3");
        }else if(type == WeixinCallcackType.APP_PAY_CODE.getKey()){//二维码支付
            url = properties.get("url2");
        }

        //1.参数封装
        Map param=new HashMap();
        param.put("mch_id", properties.get("mchId"));//商户
        param.put("nonce_str", WXPayUtil.generateNonceStr());//随机字符串
        param.put("body", body);
        param.put("out_trade_no", out_trade_no);//交易订单号
        param.put("total_fee", total_fee);//金额（分）
        param.put("spbill_create_ip", ip);//下单的ip地址

        if(attach != null && !attach.equals("")){
            param.put("attach", attach);//附带条件 （注意 加了附带参数 返回的时候 一定要注意 签名验证  否则回调签名失败!）
        }

        if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey()){//小程序商家端
            param.put("appid", properties.get("appId"));//公众账号ID
            param.put("trade_type", WeiXinPayTypeEnum.JSAPI.getValue());//交易类型
            param.put("openid",openId);//openId
            param.put("notify_url", url);//回调地址
        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_SUPPLIER.getKey()){//小程序供应商端
            param.put("appid", properties.get("appId2"));//公众账号ID
            param.put("trade_type", WeiXinPayTypeEnum.JSAPI.getValue());//交易类型
            param.put("openid",openId);//openId
            param.put("notify_url", properties.get("url"));//回调地址
        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_SUPPLIER.getKey()){//App商家端
            //20190420 oy 等后面申请成功了再写
        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_SUPPLIER.getKey()) {//App供应商端
            //20190420 oy 等后面申请成功了再写
        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.APP_CODE.getKey()) {//二维码付款
            param.put("appid", properties.get("appId3"));//公众账号ID
            param.put("trade_type", WeiXinPayTypeEnum.NATIVE.getValue());//交易类型
            param.put("notify_url", url);//回调地址
        }else {//说明没有
            return null;
        }
        try {
            //请求的参数
            String xmlParam = WXPayUtil.generateSignedXml(param, properties.get("mchIdkey"), WXPayConstants.SignType.MD5);//生成签名 MD5方式 并将参数弄成xml形式
            //2.发送请求
            HttpClient httpClient=new HttpClient("https://api.mch.weixin.qq.com/pay/unifiedorder");
            httpClient.setHttps(true);
            httpClient.setXmlParam(xmlParam);
            httpClient.post();

            //3.获取结果
            String xmlResult = httpClient.getContent();
            Map<String, String> mapResult = WXPayUtil.xmlToMap(xmlResult);
            System.out.println("微信返回结果"+mapResult);
            return mapResult;
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap();
        }

    }

    /**
     * 查询订单
     * @author oy
     * @date 2019/4/19
     * @param out_trade_no：交易订单号
     * @param weiXinTerminalPayType：小程序还是APP类型 WeiXinTerminalPayTypeEnum
     * @return java.util.Map
     */
    public static Map queryPayStatus(String out_trade_no,int weiXinTerminalPayType ) {
        //读取配置文件
        WeiXinPayUtils utils = new WeiXinPayUtils();
        Map<String, String> properties = utils.readProperties();

        //1.封装参数
        Map param=new HashMap();
        //根据小程序 还是APP获取不一样的appId
        if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey()){//小程序商家端
            param.put("appid", properties.get("appId"));
        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_SUPPLIER.getKey()){//小程序供应商端
            param.put("appid", properties.get("appId2"));
        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_SUPPLIER.getKey()){//App商家端

        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_SUPPLIER.getKey()) {//App供应商端

        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.APP_POS.getKey()) {//AppPOS
            param.put("appid", properties.get("appId3"));
        }else {//说明没有

        }
        param.put("mch_id", properties.get("mchId"));
        param.put("out_trade_no", out_trade_no);
        param.put("nonce_str", WXPayUtil.generateNonceStr());
        try {
            String xmlParam = WXPayUtil.generateSignedXml(param, properties.get("mchIdkey"));
            //2.发送请求
            HttpClient httpClient=new HttpClient("https://api.mch.weixin.qq.com/pay/orderquery");
            httpClient.setHttps(true);
            httpClient.setXmlParam(xmlParam);
            httpClient.post();

            //3.获取结果
            String xmlResult = httpClient.getContent();
            Map<String, String> map = WXPayUtil.xmlToMap(xmlResult);
//            System.out.println("查询订单支付返回结果："+xmlResult);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


    //注意：
    //1、交易时间超过一年的订单无法提交退款
    //2、微信支付退款支持单笔交易分多次退款，多次退款需要提交原支付订单的商户订单号和设置不同的退款单号。申请退款总金额不能超过订单金额。 一笔退款失败后重新提交，请不要更换退款单号，请使用原商户退款单号
    //3、请求频率限制：150qps，即每秒钟正常的申请退款请求次数不超过150次
    //    错误或无效请求频率限制：6qps，即每秒钟异常或错误的退款申请请求不超过6次
    //4、每个支付订单的部分退款次数不能超过50次
    /**
     * 退款操作
     * @author oy
     * @date 2019/5/21
     * @param out_trade_no 交易号 我们自己定的
     * @param out_refund_no 退款单号（自己内部定的 唯一就行）
     * @param total_fee  订单金额
     * @param refund_fee  退款金额 可以分开退 不用一次性退完
     * @param  weiXinTerminalPayType  WeiXinTerminalPayTypeEnum
     * @return java.util.Map<java.lang.String,java.lang.String>
     */
    public static Map<String,String> refund(String out_trade_no, String out_refund_no ,String total_fee, String  refund_fee,int weiXinTerminalPayType){
        //读取配置文件
        WeiXinPayUtils utils = new WeiXinPayUtils();
        Map<String, String> properties = utils.readProperties();
        //1.参数封装
        Map param=new HashMap();
        param.put("nonce_str", WXPayUtil.generateNonceStr());//随机字符串
        param.put("out_trade_no", out_trade_no);//交易订单号
        param.put("mch_id", properties.get("mchId"));//商户
        param.put("total_fee", total_fee);//订单金额（分）
        param.put("out_refund_no", out_refund_no);//订单金额
        param.put("refund_fee", refund_fee);//退款金额（分）
        //根据小程序 还是APP获取不一样的appId
        if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey()){//小程序商家端
            param.put("appid", properties.get("appId"));
        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_SUPPLIER.getKey()){//小程序供应商端
            param.put("appid", properties.get("appId2"));
        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_SUPPLIER.getKey()){//App商家端

        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_SUPPLIER.getKey()) {//App供应商端

        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.APP_POS.getKey()) {//AppPOS
            param.put("appid", properties.get("appId3"));
        }else {//说明没有

        }
        param.put("notify_url", properties.get("url4"));//回调地址

        try {
            //请求的参数
            String xmlParam = WXPayUtil.generateSignedXml(param, properties.get("mchIdkey"), WXPayConstants.SignType.MD5);//生成签名 MD5方式 并将参数弄成xml形式
            //2.发送请求
            HttpClient httpClient = new HttpClient("https://api.mch.weixin.qq.com/secapi/pay/refund");
            httpClient.setHttps(true);
            httpClient.setXmlParam(xmlParam);
            httpClient.setMch_id(properties.get("mchId"));
            httpClient.setMch_id_password(properties.get("mchIdPassword"));
            httpClient.post2();
            //3.获取结果
            String xmlResult = httpClient.getContent();
            Map<String, String> mapResult = WXPayUtil.xmlToMap(xmlResult);
            System.out.println("微信返回结果"+mapResult);
            return mapResult;
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap();
        }
    }

    /**
     * 获取openId（小程序需要）
     * @author oy
     * @date 2019/4/19
     * @param code 前端传递过来的code
     * @param weiXinTerminalPayType 小程序还是APP类型 WeiXinTerminalPayTypeEnum
     * @return void
     */
    public static String getOpenId(String code,int weiXinTerminalPayType){
        WeiXinPayUtils utils = new WeiXinPayUtils();
        Map<String, String> properties = utils.readProperties();

        String appid = null;
        String appsecret = null;
        if (weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey()) {
            appid = properties.get("appId");
            appsecret = properties.get("appsecret");
        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_SUPPLIER.getKey()){
            appid = properties.get("appId2");
            appsecret = properties.get("appsecret2");
        }
        String oepnSessionUrl = "https://api.weixin.qq.com/sns/jscode2session?appid="+ appid+"&secret="+ appsecret +"&js_code={JSCODE}&grant_type=authorization_code";
        String url= oepnSessionUrl.replace("{JSCODE}", code);
        String content = null;
        try {
            HttpClient httpClient=new HttpClient(url);
            httpClient.get();
            content = httpClient.getContent();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Map map = JSONObject.parseObject(content, Map.class);
        return (String)map.get("openid");
    }

    /**
     * @param weiXinTerminalPayType
     * @return 获取AccessToken
     */
    public static String getAccessToken(int weiXinTerminalPayType,int environment){
        WeiXinPayUtils utils = new WeiXinPayUtils();
        Map<String, String> properties = utils.readProperties();

//        Jedis jedis = new Jedis("127.0.0.1", 6379);
        Jedis jedis = null;
        if(environment == 0){//本地
            jedis = new Jedis("127.0.0.1", 6379);
        }else if(environment == 1){//测试
            jedis = new Jedis(properties.get("test"), 6379);
        }else if(environment == 2){//生产
            jedis = new Jedis(properties.get("prod"), 6379);
        }
//        jedis.auth("redisadmin");//20190615 oy 不需要密码
        jedis.connect();
        String accessToken = jedis.get("access_token");
        if (accessToken != null) {
            jedis.disconnect();//断开连接
            return accessToken;
        }

        String appid = null;
        String appsecret = null;
        if (weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey()) {
            appid = properties.get("appId");
            appsecret = properties.get("appsecret");
        }else if(weiXinTerminalPayType == WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_SUPPLIER.getKey()){
            appid = properties.get("appId2");
            appsecret = properties.get("appsecret2");
        }
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+ appid+"&secret="+ appsecret;
        String content = null;
        try {
            HttpClient httpClient=new HttpClient(url);
            httpClient.get();
            content = httpClient.getContent();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        HashMap<String, Object> res = JSONObject.parseObject(content, HashMap.class);
        accessToken = (String) res.get("access_token");
        int expiresIn = (int) res.get("expires_in");
        jedis.setex("access_token", expiresIn, accessToken);
        jedis.disconnect();//断开连接
        return accessToken;
    }

    /**
     * 发送模板消息
     * @param weiXinTerminalPayType
     * @param environment 环境配置变量 枚举EnvironmentEnum
     * @return
     */
    public static JSONObject templateMessageSend(
            int weiXinTerminalPayType,
            String openId,
            String templateId,
            String formId,
            HashMap<String, Object> map,
            int environment) {
        String accessToken = getAccessToken(weiXinTerminalPayType,environment);
        String url = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token="+accessToken;

        JSONObject requstJson = new JSONObject();
        requstJson.put("touser", openId);
        requstJson.put("template_id", templateId);
        requstJson.put("form_id", formId);
        JSONObject data = new JSONObject();
        // 提现单号
        JSONObject keyword1 = new JSONObject();
        keyword1.put("value", map.get("keyword1"));
        data.put("keyword1", keyword1);
        // 卡号
        JSONObject keyword2 = new JSONObject();
        keyword2.put("value", map.get("keyword2"));
        data.put("keyword2", keyword2);
        // 提现金额
        JSONObject keyword3 = new JSONObject();
        keyword3.put("value", map.get("keyword3"));
        keyword3.put("color", "#f44336");
        data.put("keyword3", keyword3);
        // 提现申请时间
        JSONObject keyword4 = new JSONObject();
        keyword4.put("value", map.get("keyword4"));
        data.put("keyword4",keyword4);
        // 温馨提示
        JSONObject keyword5 = new JSONObject();
        keyword5.put("value", map.get("keyword5"));
        data.put("keyword5", keyword5);
        requstJson.put("data", data);
        JSONObject jsonObject = doPost(url, requstJson);
        return jsonObject;
    }

    public static JSONObject doPost(String url,JSONObject json) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);
        JSONObject response = null;
        try {
            StringEntity s = new StringEntity(json.toString(), "UTF-8");
            s.setContentEncoding("UTF-8");
            s.setContentType("application/json");//发送json数据需要设置contentType
            post.setEntity(s);
            HttpResponse res = client.execute(post);
            if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = res.getEntity();
                String result = EntityUtils.toString(res.getEntity());// 返回json格式：
                response = JSONObject.parseObject(result);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return response;
    }


        /**
         * 获取配置文件中的参数 返回map
         * @author oy
         * @date 2019/4/20
         * @param
         * @return java.util.Map<java.lang.String,java.lang.String>
         */
    public  Map<String,String> readProperties(){
        Map<String,String> map = new HashMap<>();
        InputStream in = null;
        try {
            //从文件中读取配置内容
            Properties properties = new Properties();
            //读取属性文件a.properties
            in = this.getClass().getClassLoader().getResourceAsStream("weixinPay.properties");
            properties.load(in);
            Iterator<String> it = properties.stringPropertyNames().iterator();
            while(it.hasNext()){
                String key=it.next();
                map.put(key,properties.getProperty(key));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map;
    }




    public static void main(String[] args) {
//        String openId = WeiXinPayUtils.getOpenId("023aAmqf18u4Cs0A8Asf123gqf1aAmqJ", 1);//获取openId
//        System.out.println(openId);
// 商家端获取openId
        // 模板消息
        PaymentTransactionLogger paymentTransactionLogger = new PaymentTransactionLogger();
        paymentTransactionLogger.setTransSequenceNumber("1234567890");
        paymentTransactionLogger.setTransTotalAmount(100l);
        paymentTransactionLogger.setCreateTime(new Date());
        String bankAccount = "65219949842513515649";
        HashMap<String, Object> map = new HashMap<>();
        // 订单号
        map.put("keyword1",paymentTransactionLogger.getTransSequenceNumber());
        // 卡号
        map.put("keyword2",paymentTransactionLogger.getAccountNumber());
        // 提现金额
        map.put("keyword3",paymentTransactionLogger.getTransTotalAmount().doubleValue()/100+" 元");
        // 提现发起时间
        map.put("keyword4",DateUtils.format(new Date()));
        // 温馨提示 规则
        map.put("keyword5","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        templateMessageSend(
                0,
                "oNtx75W1En0kraS6z9MgVDNT01m4",
                "V-JFmoCkunNXwJhPciICM2lSLRUjYW6sRzKZQ17c-gs",
                "94118190dd4d4388a1605062fc492586",
                map,
                0
                );
//        Jedis jedis = new Jedis("127.0.0.1", 6379);
//        jedis.connect();
//        jedis.lpush("1","123");
//        jedis.zadd("fruit", 100,"apple");
//        jedis.zadd("fruit", 120,"watermalen");
//        jedis.zadd("fruit", 150,"pee");

//        System.out.println(new Long(122).doubleValue()/100);


//统一下单 20190420012

        //调用这个直接发起扫码支付
//        Map<String,String> aNative = WeiXinPayUtils.createOrderByCode("2019052200001111", "1", "127.0.0.1",
//                "支付内容",0);
//        aNative.forEach((x,y)->{
//            System.out.print(x);
//            System.out.println(":  "+y);
//        });

//        String s = ZhiFuBaoPayUtils.qrCodeFile("weixin://wxpay/bizpayurl?pr=wqQBryf", true);
//        System.out.println(s);

        //小程序
//        Map<String, String> orderBySmallProgram
//                = WeiXinPayUtils.createOrderBySmallProgram("2019060100010", "1", "127.0.0.1", "oNtx75URB5M7OZBL1Svw1P3KJID4", "小程序支付", WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey(),1);
//
//        orderBySmallProgram.forEach((x,y)->{
//            System.out.println("key:"+x + "  value:"+y);
//        });

        //获取支付结果
//       Map map = WeiXinPayUtils.queryPayStatus("2019060100009",WeiXinTerminalPayTypeEnum.SMALL_PROGRAM_BUSINESS.getKey());//
//        map.forEach((x,y)->{
//           System.out.print(x+":");
//            System.out.println(y);
//        });
        //pos机支付
//        Map map = WeiXinPayUtils.createOrderByPos("TN201906130000011", "1", "127.0.0.1", "135159646869470195", "测试pos机");
//        map.forEach((x,y)->{
//            System.out.print("key:"+x);
//            System.out.println("  value:"+y);
//        });

        //退款接口
//        Map<String, String> refund = WeiXinPayUtils.refund("TN201906130000011", "TK201906130001", "1", "1",  4);
//        refund.forEach((x,y)->{
//            System.out.println(x);
//            System.out.println(y);
//        });

        //字符串转换
//        try {
//            Map<String, String> stringStringMap = WXPayUtil.xmlToMap("<xml><return_code>SUCCESS</return_code><appid><![CDATA[wx032774cfad934f55]]></appid><mch_id><![CDATA[1526676211]]>></mch_id><nonce_str><![CDATA[fd8b005f31e5409022023d352c3a10c5]]></nonce_str><req_info><![CDATA[idvUNezXYT1UcDc8rZgmHNo+KAlrWlHiDyfK+NrsmGyAjqCtLKoyeg1F823xwfHhrvgTEBgNehhb9cFN7/PbHG05FNu7/52IswYSGJuACzw0g8xDx9FJ4pV1/S6klTImjszu6BoYZLdzUZLAdBehmZH69mV8OkAOoodXGkvri1xuCofQPHoTBdLAV8x3wdKxa2Y46QeWkLHvz6EeIIhi27YGuvXM0FjLn8DsMUKatjekz0QqA80EbMHCQ91J94h1XpgxOR1h+/ylYPGpsCv//ppNpZhZtmbEMSysQi3B4IItmCBGaxQlOz0gVYaV9Tf1MnfDbFHBwdVu5a4RuJoUNH9OxR/KWm4JvAVl+TU1/z/TLVRK1EyG4qW+jvB6/anTtFh0oMBZNF0V06weGNu8Wtey5qTfMDne6HHgZsZ5uQIFBecuGLFzpK/zJyXN76AaCkargD29ZMkVR9raO7/O9zRhFBnMhabDxeA92ZVPr04im7yUvxRyvTIIu4hb/5Ijqlc3q77STlRTP1RPPuWmPoDEc5xtEN5LmwHi/YNHbkf4358E6IjIttJV0dHN5oJHsMXJBjrLSR8RraSqCISwEWJX1NmPqDV/YN/6i0KS0tsBGF+dudKB3fW1zAkUsgKSiqgJFtPGLTUhC2Jkao61jo8URPSMG3wWZ70x5z1IMDSe/bLmQAPk+jGnP2q2bE67AKVIiFpgwDu66Av13QWqMdX9c4EHc5ry259GuLW9fREeIZhgPnp1ebY/fLA/mOkWmk2lmFm2ZsQxLKxCLcHgglevONcBBeAUEWXarMjtMEtA99hzlguAJ9CTXC8hirUqmedwdzDXScmfOJi1EBC9f2DxAuMheLywPf1JZb4HFGd7LgBDC8IVAQt9XE4pR+tdXwgsIkmxOR5pY2o+p0K9f7/Ds1CWgN0L9oQf7mU1g6aAZTX0TTa3M+LdvuT6AKChV7TR1XcFSrLh7Qscg25Ra+3HSDIL/+MXquRSmWG1K/lkefxIxsiNKO+Q5ZHt83zvB4S1eIJrzB4Xj+0PYASz8+RUVB8EeE3AyDMAPT2rHvwleqpkORrg25wb88VkM1Ui]]></req_info></xml>");
//            stringStringMap.forEach((x,y)->{
//                System.out.print(x);
//                System.out.println("    "+y);
//            });
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }

//    public static void main(String[] args) {
//        Jedis jedis = new Jedis("119.23.187.31",6380);
//        jedis.select(15);
//    }



}
