package com.tuoniaostore.commons.utils;

import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.Notification;

import java.util.HashMap;
import java.util.Map;

/**
 * 极光推送测试类
 * @author sqd
 * @date 2019/5/7
 * @param
 * @return
 */
public class JpushTest {
    // 设置好账号的app_key和masterSecret是必须的
    private static String APP_KEY = "0a28185223303b795362ab0f";
    private static String MASTER_SECRET = "4b86badf308b3f842a4d3e90";


    //极光推送>>Android
    //Map<String, String> parm是自己传过来的参数
    public static void jpushAndroid(Map<String, String> parm) {

        //创建JPushClient(推送实例)
        JPushClient jpushClient = new JPushClient(MASTER_SECRET, APP_KEY);
        //推送的关键,构造一个payload
        PushPayload payload = PushPayload.newBuilder()
                .setPlatform(Platform.android())//指定android平台的用户
                //.setAudience(Audience.all())//你项目中的所有用户
                .setAudience(Audience.registrationId(parm.get("id")))//registrationId指定用户
                .setNotification(Notification.android(parm.get("msg"), "这是title", parm))
                //发送内容
                .setOptions(Options.newBuilder().setApnsProduction(false).build())
                //这里是指定开发环境,不用设置也没关系
                .setMessage(Message.content(parm.get("msg")))//自定义信息
                .build();

        try {
            PushResult pu = jpushClient.sendPush(payload);
        } catch (APIConnectionException e) {
            e.printStackTrace();
        } catch (APIRequestException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<>();
        map.put("msg","测试hhh");
        map.put("id","3680716407");
        jpushAndroid(map);
    }


}
