package com.tuoniaostore.commons.utils;

/**
 * 序列号生成
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/25
 */
public class SequenceNumber {

    /**
     * 生成序列号 参数1：sequenceNumber 直接传序列号 如果不传，则是第一个 00000001 第二个参数 序列号前缀 比如tn
     * @return
     */
    public static String createTranSequenceNumber(String sequenceNumber,String type){
        String newTransSequenceNumber = "";
        if (sequenceNumber != null && !sequenceNumber.equals("")) {//说明有交易序号了
            //切割一下序列号  tn20190325 135020 30000000 取最后8位数就行
            String substring = sequenceNumber.substring(16);//取后面8位
            //把前面的0去掉 然后转int
            substring.replace("0","");
            int i = Integer.parseInt(substring);
            i++;
            //把i的字符化
            String is = i+"";
            char[] chars = is.toCharArray();
            String endString = "";
            int length = chars.length;

            int i1 = 8 - length;
            for (int j = 0; j < i1; j++) {
                endString += 0;
            }
            endString += i;
            String time = DateUtils.getTime();
            StringBuffer stringBuffer = new StringBuffer(type);
            stringBuffer.append(time);
            stringBuffer.append(endString);
            newTransSequenceNumber = stringBuffer.toString();
        }else{
            String time = DateUtils.getTime();
            StringBuffer stringBuffer = new StringBuffer(type);
            stringBuffer.append(time);
            stringBuffer.append("00000001");
            newTransSequenceNumber = stringBuffer.toString();
        }
        return newTransSequenceNumber;
    }


}
