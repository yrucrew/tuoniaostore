package com.tuoniaostore.commons.utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.*;

/**
 * @author oy
 * @description 读取Excel工具类
 * @date 2019/5/27
 */
public class ExcelUtils {

    public static void main(String[] args) {
        List<Map<String, String>> maps = reaExcel();
        System.out.println(maps);
    }

    public static List<Map<String, String>> reaExcel(){
        File file = new File("d:" + File.separator + "test" + File.separator + "good.xlsx");
        Map mapOut = new HashMap();
        List<Map<String, String>> list = null;
        try {
            InputStream is = new FileInputStream(file);
            Workbook wb = null;
            String extString = ".xlsx";
            try {
                if (".xls".equals(extString)) {
                    wb = new HSSFWorkbook(is);
                } else if (".xlsx".equals(extString)) {
                    wb = new XSSFWorkbook(is);
                } else {
                    wb = null;
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Sheet sheet = null;
            Row row = null;
            String cellData = null;
//            String columns[] = {"品牌名称", "默认条形码", "商品名称", "规格", "单位", "保质期","装箱数", "建议零售价", "零售默认价格","中分类"}; good_tempalte good_price good_barcode
            String columns[] = {"goodBrandName", "barcode", "goodName", "priceName", "unitName", "shelfLife","num", "salePrice", "costPrice","oneTypeName","twoTypeName"};
            //用来存放表中数据
            list = new ArrayList<Map<String, String>>();
            if (wb != null) {
                //获取第一个sheet
                sheet = wb.getSheetAt(0);
                //获取最大行数
                int rownum = sheet.getPhysicalNumberOfRows();
                //获取第一行
                row = sheet.getRow(0);
                //获取最大列数
                int colnum = row.getPhysicalNumberOfCells();
                for (int i = 1; i < rownum; i++) {
                    Map<String, String> map = new LinkedHashMap<String, String>();
                    row = sheet.getRow(i);
                    if (row != null) {
                        for (int j = 0; j < colnum; j++) {
                            cellData = (String) getCellFormatValue(row.getCell(j));
                            map.put(columns[j], cellData);
                        }
                    } else {
                        break;
                    }
                    list.add(map);
                }
            }
            //遍历解析出来的list
            for (Map<String, String> map : list) {
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    System.out.print(entry.getKey() + ":" + entry.getValue() + ",");
                }
                System.out.println();
            }
        } catch (Exception e) {
            mapOut.put("code", -1);
            e.printStackTrace();
        }
        return list;
    }

    public static Object getCellFormatValue(Cell cell) {
        Object cellValue = null;
        if (cell != null) {
            //判断cell类型
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_NUMERIC: {
                    cellValue = String.valueOf(cell.getNumericCellValue());
                    break;
                }
                case Cell.CELL_TYPE_FORMULA: {
                    //判断cell是否为日期格式
                    if (DateUtil.isCellDateFormatted(cell)) {
                        //转换为日期格式YYYY-mm-dd
                        cellValue = cell.getDateCellValue();
                    } else {
                        //数字
                        cellValue = String.valueOf(cell.getNumericCellValue());
                    }
                    break;
                }
                case Cell.CELL_TYPE_STRING: {
                    cellValue = cell.getRichStringCellValue().getString();
                    break;
                }
                default:
                    cellValue = "";
            }
        } else {
            cellValue = "";
        }
        return cellValue;
    }

}
