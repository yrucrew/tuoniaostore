package com.tuoniaostore.commons.utils;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.*;
import com.alipay.api.request.*;
import com.alipay.api.response.*;
import com.tuoniaostore.commons.file.oss.AliyunOSSUtils;
import com.tuoniaostore.commons.utils.QRCode.LogoConfig;
import com.tuoniaostore.commons.utils.QRCode.ZXingCodeUtil;
import com.tuoniaostore.commons.utils.QRCode.ZXingConfig;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author oy
 * @description 支付宝支付工具类
 * @date 2019/5/6
 */
public class ZhiFuBaoPayUtils {

    /***
     * 说明：所有封装的返回结果，需要根据 code去判断是否成功，调用者注意使用。
     * 响应码地址文档：https://docs.open.alipay.com/common/105806
     */
    public static void main(String[] args) {
        //付款码付款
//      AlipayTradePayResponse alipayTradePayResponse = ZhiFuBaoPayUtils.PosPay("便利店消费","0.01","281977665549784042",System.currentTimeMillis()+"",null);
        //退款
//        AlipayTradeRefundResponse alipayTradeRefundResponse = ZhiFuBaoPayUtils.returNoney("11.70","TNY60159186631275","测试退款");
        //查询订单
//        AlipayTradeQueryResponse alipayTradeQueryResponse = ZhiFuBaoPayUtils.queryTrade("1558861465762");
//        System.out.println(alipayTradeQueryResponse);

        //关闭订单
//        AlipayTradeCloseResponse alipayTradeCloseResponse = ZhiFuBaoPayUtils.closeTrade("1557801930685");
        //二维码支付
//        AlipayTradePrecreateResponse response = ZhiFuBaoPayUtils.scannerCodePay("", "0.01", "便利店消费");
        //生成二维码 可以带logo s是返回去的条码图片
//        String s = qrCodeFile("https://qr.alipay.com/bax0584449zmrmlyqmnv6097", true);
//        System.out.println(s);
    }

    /**
     * pos机扫码支付
     * @author oy
     * @date 2019/5/6
     * @param title 支付标题
     * @param money 金钱（元）
     * @param outTradeNo 订单号
     * @param goodsDetail 商品详情（支付宝封装类）
     * @return java.util.Map
     */
    public static AlipayTradePayResponse PosPay(String title,String money,String authCode,String outTradeNo,List<GoodsDetail> goodsDetail){
        ZhiFuBaoPayUtils utils = new ZhiFuBaoPayUtils();
        Map<String, String> properties = utils.readProperties();

        AlipayClient alipayClient = new
                DefaultAlipayClient ("https://openapi.alipay.com/gateway.do",properties.get("appId"),properties.get("privateKey"),"json",properties.get("charset"),
                properties.get("alipayPublicKey"),properties.get("singType"));
        AlipayTradePayRequest request = new AlipayTradePayRequest();
        AlipayTradePayModel model = new AlipayTradePayModel();
        model.setOutTradeNo(outTradeNo);//订单号
        model.setSubject(title);//订单标题
        BigDecimal bg = new BigDecimal(money);
        double f1 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        model.setTotalAmount(f1+"");//价钱 单位元
        model.setAuthCode(authCode);//付款码
        model.setScene("bar_code");//条码还是声波
        request.setNotifyUrl(properties.get("notify"));//通知地址
        if(goodsDetail != null && goodsDetail.size() > 0 ){
            model.setGoodsDetail(goodsDetail);
        }
//        model.setProductCode("tn00012456302");//销售产品码 可以放商品id
//        model.setTimeoutExpress("2m");//2分钟过期
//        model.setSellerId("oy");//销售员
        request.setBizModel(model);
        try {
            AlipayTradePayResponse response = alipayClient.execute(request);
            if(response.isSuccess()){
                System.out.println("调用成功");
            } else {
                System.out.println("调用失败");
            }
            return response;
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 扫码支付
     * @author oy
     * @date 2019/5/13
     * @param outTradeNo 商户订单号
     * @param totalAmount 总金额
     * @param title 订单标题
     * @return void
     */
    public static AlipayTradePrecreateResponse scannerCodePay(String outTradeNo,String totalAmount,String title){
        ZhiFuBaoPayUtils utils = new ZhiFuBaoPayUtils();
        Map<String, String> properties = utils.readProperties();
        AlipayClient alipayClient = new DefaultAlipayClient ("https://openapi.alipay.com/gateway.do",properties.get("appId"),properties.get("privateKey"),"json",properties.get("charset"),
                properties.get("alipayPublicKey"),properties.get("singType"));
        AlipayTradePrecreateRequest request = new AlipayTradePrecreateRequest();
        request.setNotifyUrl(properties.get("notify2"));//回调地址
        AlipayTradePrecreateModel model = new AlipayTradePrecreateModel();

        model.setOutTradeNo(outTradeNo);
        model.setTotalAmount(totalAmount);
        model.setSubject(title);
        request.setBizModel(model);

        try {
            AlipayTradePrecreateResponse response = alipayClient.execute(request);
            return response;
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 查询订单
     * @author oy
     * @date 2019/5/14
     * @param outTradeNo  商户订单号
     * @return com.alipay.api.response.AlipayTradeQueryResponse
     */
    public static AlipayTradeQueryResponse queryTrade(String outTradeNo){
        ZhiFuBaoPayUtils utils = new ZhiFuBaoPayUtils();
        Map<String, String> properties = utils.readProperties();
        AlipayClient alipayClient = new DefaultAlipayClient ("https://openapi.alipay.com/gateway.do",properties.get("appId"),properties.get("privateKey"),"json",properties.get("charset"),
                properties.get("alipayPublicKey"),properties.get("singType"));
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        AlipayTradeQueryModel model = new AlipayTradeQueryModel();
        model.setOutTradeNo(outTradeNo);//商户订单号
        request.setBizModel(model);
        try {
            AlipayTradeQueryResponse response = alipayClient.execute(request);
            return response;
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 关闭订单
     * @author oy
     * @date 2019/5/14
     * @param outTradeNo
     * @return com.alipay.api.response.AlipayTradeCloseResponse
     */
    public static AlipayTradeCloseResponse closeTrade(String outTradeNo){
        ZhiFuBaoPayUtils utils = new ZhiFuBaoPayUtils();
        Map<String, String> properties = utils.readProperties();
        AlipayClient alipayClient = new DefaultAlipayClient ("https://openapi.alipay.com/gateway.do",properties.get("appId"),properties.get("privateKey"),"json",properties.get("charset"),
                properties.get("alipayPublicKey"),properties.get("singType"));
        AlipayTradeCloseRequest request = new AlipayTradeCloseRequest();
        AlipayTradeCloseModel model = new AlipayTradeCloseModel();
        request.setBizModel(model);
        model.setOutTradeNo(outTradeNo);
        try {
            AlipayTradeCloseResponse response = alipayClient.execute(request);
            return response;
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 退款接口
     * @author oy
     * @date 2019/5/13
     * @param refundAmout 退款金额 单位元
     * @param outTradeNo 商户交易号
     * @param refundReason 退款原因
     * @return com.alipay.api.response.AlipayTradeRefundResponse
     */
    public static AlipayTradeRefundResponse returNoney(String refundAmout,String outTradeNo,String refundReason){
        ZhiFuBaoPayUtils utils = new ZhiFuBaoPayUtils();
        Map<String, String> properties = utils.readProperties();
        AlipayClient alipayClient = new DefaultAlipayClient ("https://openapi.alipay.com/gateway.do",properties.get("appId"),properties.get("privateKey"),"json",properties.get("charset"),
                properties.get("alipayPublicKey"),properties.get("singType"));
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        AlipayTradeRefundModel model = new AlipayTradeRefundModel();

        model.setRefundAmount(refundAmout);//退款金额  必须和订单的金额是一样的
        model.setOutTradeNo(outTradeNo);//商户订单号
        model.setRefundReason(refundReason);//退款原因
        request.setBizModel(model);
        try {
            AlipayTradeRefundResponse response = alipayClient.execute(request);
            return response;
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 生成二维码（带logo）
     * @author oy
     * @date 2019/5/14
     * @param content 二维码  logFlag是否生成logo
     * @return java.lang.String
     */
    public static String qrCodeFile(String content, Boolean logFlag){
        String codePath = "image/qr_code/"+System.currentTimeMillis()+".png";
        try {
            //    生成二维码
            File file = new File(codePath);
            ZXingCodeUtil zp = new ZXingCodeUtil();    //    实例化二维码工具
            ZXingConfig zxingconfig = new ZXingConfig();                //    实例化二维码配置参数
            zxingconfig.setHints(zp.getDecodeHintType());            //    设置二维码的格式参数
            zxingconfig.setContent(content);//    设置二维码生成内容
            zxingconfig.setLogoPath("image/logo.png");    //    设置Logo图片
            zxingconfig.setLogoConfig(new LogoConfig());                //    Logo图片参数设置
            zxingconfig.setLogoFlg(true);            //    设置生成Logo图片
            BufferedImage bim = zp.getQR_CODEBufferedImage(zxingconfig);//    生成二维码
            ImageIO.write(bim, "png", file);                //    图片写出
            Thread.sleep(200);        //    缓冲
            zp.parseQR_CODEImage(bim);        //    解析调用

            //将图片上传到图片文件服务器
            FileInputStream inputStream = new FileInputStream(codePath);
            String url = AliyunOSSUtils.uploadImageFiles(codePath, "ostrichapps", inputStream);
            return url;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 读取支付宝配置文件
     * @author oy
     * @date 2019/5/10
     * @param
     * @return java.util.Map<java.lang.String,java.lang.String>
     */
    public  Map<String,String> readProperties(){
        Map<String,String> map = new HashMap<>();
        InputStream in = null;
        try {
            //从文件中读取配置内容
            Properties properties = new Properties();
            //读取属性文件a.properties
            in = this.getClass().getClassLoader().getResourceAsStream("zhifubaoPay.properties");
            properties.load(in);
            Iterator<String> it = properties.stringPropertyNames().iterator();
            while(it.hasNext()){
                String key=it.next();
                map.put(key,properties.getProperty(key));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

}
