package com.tuoniaostore.commons.utils;

import java.util.LinkedList;
import java.util.List;

public class PrintFormat {
	
	private List<String> lines = new LinkedList<>();
	
	// 商品名字的最长空间(9个中文加一个空格)
    private static final int GOODS_NAME_LENGTH = 19;
    // 单价的最长空间(包括最后的空格) 即最大值：9999.99
    private static final int PRICE_LENGTH = 8;
    // 数量的最长空间(包括最后的空格) 即最大值：99999
    private static final int COUNT_LENGTH = 6;
    
    // 打印编码
    private String printCode;
    
    public PrintFormat (String printCode) {
    	this.printCode = printCode;
    }
    
    public static void main(String[] args) {
		PrintFormat printFormat = new PrintFormat("217800901|6ptmhvsr");
		
		printFormat.addTitle("测试打印");
		
		printFormat.addMenu();
		
		printFormat.append("--------------------------------");
		printFormat.enter(); // 换行
		
		printFormat.addGoods("饭", "1.00", "1", "1.00");
		
		printFormat.addGoods("炒饭", "10.00", "10", "100.00");
		
		printFormat.addGoods("蛋炒饭", "10.0", "10", "100.00");
		
		printFormat.addGoods("鸡蛋炒饭", "100.00", "5", "500.00");
		
		printFormat.addGoods("番茄蛋炒饭", "1000.00", "1", "1000.00");
		
		printFormat.addGoods("西红柿蛋炒饭", "1000.00", "100", "100000.00");
		
		printFormat.addGoods("西红柿鸡蛋炒饭西红柿鸡蛋炒饭西红柿鸡蛋炒饭西红柿鸡蛋炒饭", "100.0", "10", "1000.0");
		
		printFormat.addProperty("备注", "加辣不要辣椒");
		
		printFormat.append("------------------------------------------------<BR>");
		printFormat.append("一一一一一一一一一一一一一一一一一一一一一一一一<BR>");
		
		printFormat.addProperty("合计", "10011100000.00元");
		printFormat.addProperty("送货地点", "广州市南沙区xx路xx号");
		printFormat.addProperty("联系电话", "13888888888888");
		printFormat.addProperty("订餐时间", "2014-08-08 08:08:08");
		
		printFormat.addQRContent("www.baidu.com");
		
		printFormat.print();
	}
    
    /**
     * 打印
     * @return
     */
    public String print() {
    	StringBuilder sb = new StringBuilder();
    	lines.forEach(line -> sb.append(line));
    	return PrintFEUtils.print(printCode, 1, sb.toString());
    }
	
	/**
	 * 添加内容
	 * @param content 内容
	 * @return
	 */
	public PrintFormat append(String content) {
        lines.add(content);
        return this;
    }

	/**
	 * 换一行
	 */
    public void enter() {
        enter(1);
    }

    /**
     * 换多行
     * @param count 行数
     */
    public void enter(int count) {
        for (int i = 0; i < count; i++) {
            lines.add("<BR>");
        }
    }
    
    /**
     * 添加属性 key : value
     * @param key
     * @param value
     */
    public void addProperty(String key, String value) {
    	lines.add(key + " : " + value);
    	enter();
    }
    
    /**
     * 添加二维码
     * @param qrContent
     */
    public void addQRContent(String qrContent) {
    	lines.add(String.format("<QR>%s</QR>", qrContent));
    }
	
	/**
     * 添加 title
     */
    public void addTitle(String title) {
        lines.add(String.format("<CB>%s</CB><BR>", title));
    }

    /**
     * 固定格式：名称               单价    数量  金额
     */
    public void addMenu() {
        lines.add("名称               单价     数量    金额");
        enter();
    }
    
    // 添加空格
    public void addSpace(int count) {
    	for (int i = 0; i < count; i++){
			lines.add(" ");
		}
    }
    
    // 是否中文
    public static boolean isChinese(char c) {
    	if (c >= 0x4e00 && c <= 0x9fbb) {
			return true;
		}
		return false;
    }
    
    /**
     * 添加商品
     * @param name 名字
     * @param price 单价(1.00)
     * @param number 数量
     * @param totalPrice 总价
     */
    public void addGoods(String name, String price, String number, String totalPrice) {
    	// 名字
    	int nameLength = 0;
    	char[] charArray = name.toCharArray();
    	for (char c : charArray) {
    		nameLength++;
			if (isChinese(c)) { // 中文比其他占位置
				nameLength++;
			}
		}
    	
    	int firstIndex = name.length();

        if (nameLength > (GOODS_NAME_LENGTH - 1)) { // 当字符总长度大于规定长度时，通过以下算法计算第一行的名字内容以及截断的位置
            firstIndex = 0;
            int charLength = 0;

            char[] cs = name.toCharArray();
            for (char c : cs) {
                if ((charLength + 1) > (GOODS_NAME_LENGTH - 1)) { // 字符长度已达到规定长度时 退出整个循环
                    break;
                }
                if (isChinese(c)) { // 如果为中文时 字符长度为2
                    if ((charLength + 2) > (GOODS_NAME_LENGTH - 1)) { // 当字符长度已达到规定长度时 退出整个循环
                        break;
                    }
                    charLength++; // 中文字符长度先自增(中文字符长度为两位)
                }
                firstIndex += 1; // 位置后移
                charLength++; // 字符后移
            }
        }
        String firstValue = name.substring(0, firstIndex); // 截取第一行文字内容
        
        lines.add(firstValue);
        
        // 补剩下空格
        if (nameLength < (GOODS_NAME_LENGTH - 1)) {
        	addSpace(GOODS_NAME_LENGTH - nameLength);
        } else {
        	addSpace(1);
        }
        
    	
    	// 单价
    	lines.add(price);
    	if (price.length() < PRICE_LENGTH) {
    		addSpace(PRICE_LENGTH - price.length());
    	}
    	
    	// 数量
    	lines.add(number);
    	if (number.length() < COUNT_LENGTH) {
    		addSpace(COUNT_LENGTH - number.length());
    	}
    	
    	// 总价
    	lines.add(totalPrice);
    	// 剩余名字
    	if (firstIndex < name.length()) {
    		enter();
        	lines.add(name.substring(firstIndex));
    	}
    	
    	enter();
    }
	
}
