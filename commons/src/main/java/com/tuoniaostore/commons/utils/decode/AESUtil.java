package com.tuoniaostore.commons.utils.decode;

import com.tuoniaostore.commons.MD5;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Security;

/**
 * @author oy
 * @description AES算法工具
 * @date 2019/5/22
 */
public class AESUtil {

    static{
        try{
            Security.addProvider(new BouncyCastleProvider());
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 密钥算法
     */
    private static final String ALGORITHM = "AES";
    /**
     * 加解密算法/工作模式/填充方式
     */
    private static final String ALGORITHM_MODE_PADDING = "AES/ECB/PKCS7Padding";

    /**
     * 生成key
     */
    private static final String key = "1526676211TuoNiaoxintiandi520hwa";//配置文件中 mchIdkey 字段

    private static SecretKeySpec secretKey = new SecretKeySpec(MD5.encode(key).toLowerCase().getBytes(), ALGORITHM);


    /**
     * AES加密
     *
     * @param data
     * @return
     * @throws Exception
     */
    public static String encryptData(String data) throws Exception {
        // 创建密码器
        Cipher cipher = Cipher.getInstance(ALGORITHM_MODE_PADDING,"BC");
        // 初始化
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        return Base64Util.encode(cipher.doFinal(data.getBytes()));
    }


    /**
     * AES解密
     *
     * @param base64Data
     * @return
     * @throws Exception
     */
    public static String decryptData(String base64Data) throws Exception {
        // 创建密码器
        Cipher cipher = Cipher.getInstance(ALGORITHM_MODE_PADDING,"BC");
        // 初始化
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        return new String(cipher.doFinal(Base64Util.decode(base64Data)));
    }



    public static void main(String[] args) throws Exception {
        //String A 为测试字符串，是微信返回过来的退款通知字符串
        String A = "idvUNezXYT1UcDc8rZgmHNo+KAlrWlHiDyfK+NrsmGyAjqCtLKoyeg1F823xwfHhrvgTEBgNehhb9cFN7/PbHG05FNu7/52IswYSGJuACzw0g8xDx9FJ4pV1/S6klTImjszu6BoYZLdzUZLAdBehmZH69mV8OkAOoodXGkvri1xuCofQPHoTBdLAV8x3wdKxa2Y46QeWkLHvz6EeIIhi27YGuvXM0FjLn8DsMUKatjekz0QqA80EbMHCQ91J94h1XpgxOR1h+/ylYPGpsCv//ppNpZhZtmbEMSysQi3B4IItmCBGaxQlOz0gVYaV9Tf1MnfDbFHBwdVu5a4RuJoUNH9OxR/KWm4JvAVl+TU1/z/TLVRK1EyG4qW+jvB6/anTtFh0oMBZNF0V06weGNu8Wtey5qTfMDne6HHgZsZ5uQIFBecuGLFzpK/zJyXN76AaCkargD29ZMkVR9raO7/O9zRhFBnMhabDxeA92ZVPr04im7yUvxRyvTIIu4hb/5Ijqlc3q77STlRTP1RPPuWmPoDEc5xtEN5LmwHi/YNHbkf4358E6IjIttJV0dHN5oJHsMXJBjrLSR8RraSqCISwEWJX1NmPqDV/YN/6i0KS0tsBGF+dudKB3fW1zAkUsgKSiqgJFtPGLTUhC2Jkao61jo8URPSMG3wWZ70x5z1IMDSe/bLmQAPk+jGnP2q2bE67AKVIiFpgwDu66Av13QWqMdX9c4EHc5ry259GuLW9fREeIZhgPnp1ebY/fLA/mOkWmk2lmFm2ZsQxLKxCLcHgglevONcBBeAUEWXarMjtMEtA99hzlguAJ9CTXC8hirUqmedwdzDXScmfOJi1EBC9f2DxAuMheLywPf1JZb4HFGd7LgBDC8IVAQt9XE4pR+tdXwgsIkmxOR5pY2o+p0K9f7/Ds1CWgN0L9oQf7mU1g6aAZTX0TTa3M+LdvuT6AKChV7TR1XcFSrLh7Qscg25Ra+3HSDIL/+MXquRSmWG1K/lkefxIxsiNKO+Q5ZHt83zvB4S1eIJrzB4Xj+0PYASz8+RUVB8EeE3AyDMAPT2rHvwleqpkORrg25wb88VkM1Ui";
         String B = AESUtil.decryptData(A);
        System.out.println(B);
    }

}
