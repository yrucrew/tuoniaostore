package com.tuoniaostore.commons;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.tuoniaostore.commons.utils.DateUtils;
import com.tuoniaostore.commons.utils.WeiXinPayUtils;
import com.tuoniaostore.commons.utils.ZhiFuBaoPayUtils;
import net.minidev.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.*;

/**
 * @author lyb
 */
public class TokenUtils {

    /**
     * 1.创建一个32-byte的密匙
     */

    private static final byte[] secret = "geiwodiangasfdjsikolkjikolkijswe".getBytes();


    //生成一个token
    public static String creatToken(Map<String,Object> payloadMap) throws JOSEException {

        //3.先建立一个头部Header
        /**
         * JWSHeader参数：1.加密算法法则,2.类型，3.。。。。。。。
         * 一般只需要传入加密算法法则就可以。
         * 这里则采用HS256
         *
         * JWSAlgorithm类里面有所有的加密算法法则，直接调用。
         */
        JWSHeader jwsHeader = new JWSHeader(JWSAlgorithm.HS256);

        //建立一个载荷Payload
        Payload payload = new Payload(new JSONObject(payloadMap));

        //将头部和载荷结合在一起
        JWSObject jwsObject = new JWSObject(jwsHeader, payload);

        //建立一个密匙

        JWSSigner jwsSigner = new MACSigner(secret);

        //签名
        jwsObject.sign(jwsSigner);

        //生成token
        return jwsObject.serialize();



    }


    //解析一个token

    public static Map<String,Object> valid(String token) throws ParseException, JOSEException {

//        解析token

        JWSObject jwsObject = JWSObject.parse(token);

        //获取到载荷
        Payload payload=jwsObject.getPayload();

        //建立一个解锁密匙
        JWSVerifier jwsVerifier = new MACVerifier(secret);

        Map<String, Object> resultMap = new HashMap<>();
        //判断token
        if (jwsObject.verify(jwsVerifier)) {
            resultMap.put("Result", 0);
            //载荷的数据解析成json对象。
            JSONObject jsonObject = payload.toJSONObject();
            resultMap.put("data", jsonObject);

            //判断token是否过期
            if (jsonObject.containsKey("exp")) {
                Long expTime = Long.valueOf(jsonObject.get("exp").toString());
                Long nowTime = new Date().getTime();
                //判断是否过期
                if (nowTime > expTime) {
                    //已经过期
                    resultMap.clear();
                    resultMap.put("Result", 2);

                }
            }
        }else {
            resultMap.put("Result", 1);
        }

        return resultMap;

    }



    //生成token的业务逻辑
    public static String AddToken(String uid) {
        //获取生成token

        Map<String, Object> map = new HashMap<>();

        //建立载荷，这些数据根据业务，自己定义。
        map.put("uid", uid);
        //生成时间
        map.put("sta", new Date().getTime());
        //过期时间
        //map.put("exp", new Date().getTime()+ 20000);
        long t=3600000000L;
        map.put("exp", new Date().getTime()+t);
//        map.put("exp", new Date().getTime()+5000);
        try {
            String token = TokenUtils.creatToken(map);
            System.out.println("token="+token);
            return token;
        } catch (JOSEException e) {
            System.out.println("生成token失败");
            e.printStackTrace();
        }
        return null;

    }

    //处理解析的业务逻辑
    public static void ValidToken(String token) {
        //解析token
        try {
            if (token != null) {

                Map<String, Object> validMap = TokenUtils.valid(token);
                int i = (int) validMap.get("Result");
                if (i == 0) {
                    System.out.println("token解析成功");
                    JSONObject jsonObject = (JSONObject) validMap.get("data");
                    System.out.println("uid是" + jsonObject.get("uid"));
                    System.out.println("sta是"+jsonObject.get("sta"));
                    System.out.println("exp是"+jsonObject.get("exp"));
                } else if (i == 2) {
                    System.out.println("token已经过期");
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (JOSEException e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取配置文件中的参数 返回map
     * @author oy
     * @date 2019/4/20
     * @param
     * @return java.util.Map<java.lang.String,java.lang.String>
     */
    public  Map<String,Object> readProperties(){
        Map<String,Object> map = new HashMap<>();
        InputStream in = null;
        try {
            //从文件中读取配置内容
            Properties properties = new Properties();
            //读取属性文件a.properties
            in = this.getClass().getClassLoader().getResourceAsStream("remoteSecurity.properties");
            properties.load(in);
            Iterator<String> it = properties.stringPropertyNames().iterator();
            while(it.hasNext()){
                String key=it.next();
                map.put(key,properties.getProperty(key));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    /**
     * 验证是否token过期（远程调用的时候用）
     * @author oy
     * @date 2019/5/30
     * @param token
     * @return boolean
     */
    public static boolean validateInterFace(String token) throws Exception{
        Map<String, Object> valid = TokenUtils.valid(token);
        //为用户id添加token
        TokenUtils tokenUtils = new TokenUtils();
        Map<String, Object> properties = tokenUtils.readProperties();
        String key = (String)properties.get("key");
        String timeout = (String)properties.get("timeout");

        if(valid != null && valid.entrySet().size() != 0){
            Map<String,Object> data = (Map)valid.get("data");
            if(data != null && !data.entrySet().isEmpty()){
                String key1 = (String)data.get("key");
                System.out.println(key1);
                System.out.println(key);
                if(key1.equals(key)){
                    long date = (long)data.get("date");
                    long expretime = Long.parseLong(timeout);
                    long nowDate = System.currentTimeMillis();
                    long betweenTime = nowDate - date;
                    if(betweenTime <= expretime * 1000){
                       return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 创建token（远程调用验证）
     * @author oy
     * @date 2019/5/30
     * @param
     * @return java.lang.String
     */
    public static String createTokenForInterface(){
        //为用户id添加token
        TokenUtils tokenUtils = new TokenUtils();
        Map<String, Object> properties = tokenUtils.readProperties();
        String key = (String)properties.get("key");
        String timeout = (String)properties.get("timeout");
        properties.put("date",System.currentTimeMillis());
        String token = null;
        try {
            token = creatToken(properties);
        } catch (JOSEException e) {
            e.printStackTrace();
        }
        return token;
    }

    public static void main(String[] args) throws Exception{
        //为用户id添加token
        TokenUtils tokenUtils = new TokenUtils();
        Map<String, Object> properties = tokenUtils.readProperties();
        String key = (String)properties.get("key");
        String timeout = (String)properties.get("timeout");
        properties.put("date",System.currentTimeMillis());
//        String s = creatToken(properties);
//        System.out.println(s);
        //验证是否过期
//        ValidToken("eyJhbGciOiJIUzI1NiJ9.eyJkYXRlIjoiMiIsImtleSI6IjBiNzQxNDQzODY5YTQyNDM5MDk2YTU3MTJkMGZkNDYydWllRUFpOGUwMDc1MTI2OTA4NDcxZGE0NmE2YzU1N2ZhN2Y4YTl0cEdlbm8ifQ.3brET6YFjyU7PyZT5LWFKL3GDKXnxaXBigNFzVn_OFk");

        Map<String, Object> valid = valid("eyJhbGciOiJIUzI1NiJ9.eyJkYXRlIjoxNTU5MTg2ODA4MTM5LCJrZXkiOiIwYjc0MTQ0Mzg2OWE0MjQzOTA5NmE1NzEyZDBmZDQ2MnVpZUVBaThlMDA3NTEyNjkwODQ3MWRhNDZhNmM1NTdmYTdmOGE5dHBHZW5vIiwidGltZW91dCI6IjMwIn0.72_BfiClzgdRS1__fwqPW5ApjzaD-Da4eSn-Dbl_7vM");

        if(valid != null && valid.entrySet().size() != 0){
            Map<String,Object> data = (Map)valid.get("data");
            if(data != null && !data.entrySet().isEmpty()){
                String key1 = (String)data.get("key");
                System.out.println(key1);
                System.out.println(key);
                if(key1.equals(key)){
                    long date = (long)data.get("date");
                    long expretime = Long.parseLong(timeout);
                    long nowDate = System.currentTimeMillis();
                    System.out.println("现在的时间:"+nowDate);
                    System.out.println("token时间:"+date);
                    System.out.println("过期设置的时间:"+expretime);
                    long betweenTime = nowDate - date;
                    System.out.println("间隔时间:"+betweenTime);
                    if(betweenTime <= expretime * 1000){//没超过半分钟 没过期
                        System.out.println("验证通过！");
                    }
                }
            }
        }

        /*try {

              //  ValidToken("eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiJBNTcxOEEzNTBCQTgzNkRBMEQ5RjAzQjUyQThGODc2M0JDNkJENjFBQUQwQjdEQzMxQkVEREUzMTI3N0NDNzMwMTE3RkYyOTM0NUQ0RjdDOUU3MEMzQzI1RjI3NzdFQjE4ODRENjdEOThEMzNFRkRDMkQ0RUUyNjZCNDVEMUMzNjI2NUIzOUE3REY5Mzg2OUY5MjREM0VFQjVENDI5MTBCOTIzNDE4Q0I3RDlGMzJFNTEyOERGOUEyNzgwNTlCRUExRDI5NUZFNTI0Q0Q0RTNFRDZBOUJERDAwRkY4Mjg1RDFBOUZCNURFQkI3RjI4QzAxMzhDMTcxQTNEOUUwMUZCNzRGQTcxNTE1OUFDQUEzMjE1ODBGM0M4NkQxRTgwMEEzNjBFQzlDQkI3MEU4QzVFQTgzMzJCQUYyMTc5Q0Q4QjQwMjU2NkNEOTVFNDU3QTFBOEJGMzE1QzI1MUI3OTIwMDI1REVGODZEODdGMEM0OEIzODNCQTNFQjY3MTU4QjVDNDFBNjNDRDJBOEYzRjc2NTJDMzdDMjlBNkEwQzAxRTBCOUNGRjkxMDExQjEyMjZEREVCQjU5MTI5NDk5NjEzNUNGNzUyRTdFRjlFQTI5MTNEMTQ2M0ZFNkQzOEI2MkM2OTI2QjQxMjJGMUUzNDJCRkU0QzI0QzRBMDRGOTZGNCIsInN0YSI6MTU1MjAyNjg2MTQ5MSwiZXhwIjoxNTUyMDMwNDYxNDkxfQ.bjuI1bWVBrmN_z_jUvDWBVuxHp5PolCkRpy67rIVpJo");

        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }
}
