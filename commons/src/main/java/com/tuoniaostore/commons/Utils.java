package com.tuoniaostore.commons;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by liyunbiao on 2018/9/9.
 */
public class Utils {
    /**
     * 将分为单位的转换为元 （除100）
     *
     * @param amount
     * @return
     * @throws Exception
     */
    private static String changeF2Y(String amount) {
        return BigDecimal.valueOf(Long.valueOf(amount)).divide(new BigDecimal(100)).toString();
    }

    private static String _toYuanStr(long price) {
        try {
            String yuan = changeF2Y(String.valueOf(price));
            //如果有小数点，判断位数
            int point = yuan.indexOf(".");
            if (point < 0) {
                return yuan + ".00";
            }
            if (yuan.length() - point < 3) {
                return yuan + "0";
            }
            return yuan;
        } catch (Exception e) {
            return "0";
        }
    }


    //////////////////


    public static double toYuan(long price) {
//        double v = (double)price / 100;
//        return Double.parseDouble(doubleToTowStr(v));
        return Double.parseDouble(_toYuanStr(price));
    }

    public static double doubleToTow(double price) {
        return Double.parseDouble(doubleToTowStr(price));
    }

    public static String toYuanStr(long price) {
//        double v = (double)price / 100;
//        return doubleToTowStr(v);
        return _toYuanStr(price);
    }

    public static String doubleToTowStr(double price) {
        //截取2位double

        String vs = String.valueOf(price);
        StringBuilder sb = new StringBuilder();
        boolean point = false;
        int pointCount = 0;
        for (int i = 0; i < vs.length(); i++) {
            char c = vs.charAt(i);
            if (c == '.') {
                point = true;
                sb.append(c);
                continue;
            }
            if (point) {
                pointCount++;
                //只截2位
                if (pointCount == 3) {
                    break;
                }
            }
            sb.append(c);
        }
        //如果未够1位数
        if(pointCount != 2) {
            int c =2 - pointCount;
            for (int i = 0; i < c; i++) {
                sb.append("0");
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.err.println("====>"+toYuanStr(10000));
    }

    /** 加法 */
    public static double add(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.add(b2).doubleValue();
    }

    /** 乘法 */
    public static double mul(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.multiply(b2).doubleValue();
    }

    public static String formatDistance(double distance) {
        return distance < 1000 ? (((int) distance) + "米") : (formatNumber(distance / 1000f, "#.##") + "公里");
    }

    public static String formatNumber(double value, String format) {
        DecimalFormat decimalFormat = new DecimalFormat(format);
        return decimalFormat.format(value);
    }
}
