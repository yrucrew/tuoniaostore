package com.tuoniaostore.commons.constant.user;

public class RoleConfig {

    // 原id
//	public static final String PURCHASE_ROLE_ID = "4dcfb59beb124848b0f277a014cabed0tAhqFi";
    /**
     * 采购专员
     */
    public static final String PURCHASE_ROLE_ID = "3cf99e64b26546b3aac3e873cd45e290NlLqZa";

    /**
     * 采购管理员
     */
    public static final String PURCHASE_ADMIN_ROLE_ID = "2a480697c95d4f389a34f272bcf47c9dEKLqDk";

}
