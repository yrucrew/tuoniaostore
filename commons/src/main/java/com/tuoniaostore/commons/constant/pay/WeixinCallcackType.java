package com.tuoniaostore.commons.constant.pay;


/**
 * 判断回调通知类型
 * @author sqd
 * @date 2019/5/8
 * @param
 * @return
 */
public enum WeixinCallcackType {

    BUSINESS_SMALL_PROGRAM_ORDER(0,"商家端小程序下订单"),
    BUSINESS_SMALL_PROGRAM_RECHARGE(1,"商家端小程序充值"),
    APP_PAY_CODE(2,"二维码支付")
    ;
    private int key;
    private String value;

    WeixinCallcackType(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 通过key 获取 value
     * @param key
     * @return
     */
    public static String getValue(int key) {
        for (WeixinCallcackType ele : values()) {
            if(ele.getKey() == key){
                return ele.getValue();
            }
        }
        return null;
    }

    /**
     * 通过value 获取 key
     * @param value
     * @return
     */
    public static int getKey(String value) {
        for (WeixinCallcackType ele : values()) {
            if(ele.getValue().equals(value)){
                return ele.getKey();
            }
        }
        return -999;
    }
}
