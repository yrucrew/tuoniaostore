package com.tuoniaostore.commons.constant.user;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/19
 */
public enum AddressType {

    //0-收货地址；1-开拓签约地址
    NORMAL(0,"收货地址"),
    ALL(1,"开拓签约地址");
    private  int key;
    private String value;

    AddressType(int key,String value){
        this.key=key;
        this.value=value;
    }

    public int getKey() {
        return key;
    }


    public String getValue() {
        return value;
    }
}
