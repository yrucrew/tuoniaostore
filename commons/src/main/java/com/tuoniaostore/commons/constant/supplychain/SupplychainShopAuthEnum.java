package com.tuoniaostore.commons.constant.supplychain;

/**
 * 供应商审核
 */
public enum SupplychainShopAuthEnum {

	/** 0 - 未审核 */
	DEFAULT(0, "未审核"),
	/** 0 - 审核通过 */
	PASS(1, "审核通过");
	
	int key;
	String value;
	private SupplychainShopAuthEnum(int key, String value) {
		this.key = key;
		this.value = value;
	}
	public int getKey() {
		return key;
	}
	public String getValue() {
		return value;
	}
}
