package com.tuoniaostore.commons.constant.pay;

/**
 * 判断是小程序 还是APP支付
 * @author oy
 * @date 2019/4/20
 * @param
 * @return
 */
public enum WeiXinTerminalPayTypeEnum {
    // 0.小程序商家端 1.小程序供应商端 2.APP商家端 3.APP供应商端
    SMALL_PROGRAM_BUSINESS(0,"smallProgramBusiness"),
    SMALL_PROGRAM_SUPPLIER(1,"smallProgramSupplier"),
    APP_BUSINESS(2,"appBusiness"),
    APP_SUPPLIER(3,"appSupplier"),
    APP_POS(4,"appPOS"),
    APP_CODE(5,"appCode"),
    ;

    private int key;
    private String value;

    WeiXinTerminalPayTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 通过key 获取 value
     * @param key
     * @return
     */
    public static String getValue(int key) {
        for (WeiXinTerminalPayTypeEnum ele : values()) {
            if(ele.getKey() == key){
                return ele.getValue();
            }
        }
        return null;
    }

    /**
     * 通过value 获取 key
     * @param value
     * @return
     */
    public static int getKey(String value) {
        for (WeiXinTerminalPayTypeEnum ele : values()) {
            if(ele.getValue().equals(value)){
                return ele.getKey();
            }
        }
        return -999;
    }

}
