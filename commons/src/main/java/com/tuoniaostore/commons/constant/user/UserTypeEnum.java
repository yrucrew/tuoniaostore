package com.tuoniaostore.commons.constant.user;

/**
 * Created by liyunbiao on 2018/8/27.
 */
public enum UserTypeEnum {

    DEFAULT(0,"系统默认"),;
    private  int key;
    private String value;

    UserTypeEnum(int key, String value){
        this.key=key;
        this.value=value;
    }

    public int getKey() {
        return key;
    }


    public String getValue() {
        return value;
    }


}
