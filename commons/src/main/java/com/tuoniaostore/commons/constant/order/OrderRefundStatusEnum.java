package com.tuoniaostore.commons.constant.order;

public enum OrderRefundStatusEnum {
    UNREFUND(0, "未退款"),
    REFUNDING(1, "退款中"),
    REFUNDED(2, "已退款"),
    ;

    private int key;
    private String value;

    OrderRefundStatusEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
