package com.tuoniaostore.commons.constant.user;

/**
 * 来源渠道
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/20
 */
public enum FromChannelEnum {

    FromChannel__DEFAULT(0,"便利店渠道"),
    FromChannel__RESTAURANT(1,"餐饮渠道"),
    FromChannel__WHOLESALER(2,"批发商渠道"),
    FromChannel_FRANCHISEE(3,"加盟商渠道"),
    FromChannel_BACKSTAGE(4,"后台添加"),
    ;

    private int key;
    private String value;

    FromChannelEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }}

