package com.tuoniaostore.commons.constant.supplychain;

public class SupplyChainShopTypeConstant {
    /**
     * 直营仓
     */
    public static final int STRAIGHT_BARN = 0;
    /**
     * 虚拟仓
     */
    public static final int VIRTUAL_WAREHOUSE = 1;
    /**
     * 第三方仓
     */
    public static final int THIRD_PARTY_WAREHOUSE = 2;
    /**
     * 合作仓
     */
    public static final int COOPERATIVE_WAREHOUSE = 3;
    /**
     * 代理仓
     */
    public static final int AGENCY_WAREHOUSE = 4;
    /**
     * 供应商
     */
    public static final int SUPPLIER = 5;
    /**
     * 加盟店
     */
    public static final int ORDINARY_STORES = 6;
    /**
     * 直营店
     */
    public static final int DIRECT_SHOP = 7;
    /**
     * 批发
     */
    public static final int WHOLESALE = 8;
    /**
     * 厂商
     */
    public static final int MANUFACTURER = 9;
}
