package com.tuoniaostore.commons.constant.payment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author chinahuangxc on 2017/2/2.
 */
public enum PaymentTypeEnum {

    /** <b><font color="red">9999</font> UNKNOWN</b>  - 未知 */
    UNKNOWN(9999, "UNKNOWN", "未知", false),
    /** <b><font color="red">10000</font> BALANCE</b> - 商家余额 */
    BALANCE(10000, "BALANCE", "商家余额", true),
    /** <b><font color="red">11000</font> VIP_BALANCE</b> - 会员余额 */
    VIP_BALANCE(11000, "VIP_BALANCE", "会员余额", false),
    /** <b><font color="red">20000</font> ALIPAY</b> - 支付宝 */
    ALIPAY(20000, "ALIPAY", "支付宝", true),
    /** <b><font color="red">21000</font> ALIPAY_JS</b> - 支付宝 */
    ALIPAY_JS(21000, "ALIPAY_JS", "支付宝", false),
    /** <b><font color="red">30000</font> WEIXIN_NATIVE</b> - 微信 */
    WEIXIN_NATIVE(30000, "WEIXIN_NATIVE", "微信", true),
    /** <b><font color="red">31000</font> WEIXIN_JS</b> - 微信公众号 */
    WEIXIN_JS(31000, "WEIXIN_JS", "微信公众号", false),
    /** <b><font color="red">40000</font> DRAW_CASH</b> - 提现 */
    DRAW_CASH(40000, "DRAW_CASH", "提现", false),
    /** <b><font color="red">50000</font> CASH</b> - 现金 */
    CASH(50000,"CASH","现金", true),
    /** <b><font color="red">60000</font> NO1CREDIT</b> - 金融支付 */
    NO1CREDIT(60000,"NO1CREDIT","金融支付", false),
    /** <b><font color="red">61000</font> NO1CREDIT_A</b> - 货到付款 */
    NO1CREDIT_A(61000,"NO1CREDIT_A","货到付款", false),
    /** <b><font color="red">62000</font> NO1CREDIT_B</b> - 采购贷 */
    NO1CREDIT_B(62000,"NO1CREDIT_B","采购贷", false),
    /** <b><font color="red">70000</font> LUCKY_MONEY</b> - 红包 */
    LUCKY_MONEY(70000,"LUCKY_MONEY", "红包", true),
    /** <b><font color="red">80000</font> REFUND</b> - 退款 */
    REFUND(80000, "REFUND", "退款", true),
    /** <b><font color="red">81000</font> REBATE</b> - 商家余额支付返利 */
    REBATE(81000, "REBATE", "商家余额支付返利", true),
    /** <b><font color="red">82000</font> RETURN_CASH</b> - 进货返现 */
    RETURN_CASH(82000, "RETURN_CASH", "进货返现", true),
    /** <b><font color="red">90000</font> CARD_JS</b> - 会员卡 */
    CARD_JS(90000, "CARD_JS", "会员卡", false),
    /** <b><font color="red">91000</font> C_BUSINESS_COMMISSION</b> - 团购商城收入 */
    C_BUSINESS_COMMISSION(91000,"C_BUSINESS_COMMISSION","团购商城收入", false),
    ;

    private int channelId;
    private String key;
    private String value;
    private boolean rechargeVIPBalance;

    PaymentTypeEnum(int channelId, String key, String value, boolean rechargeVIPBalance) {
        this.channelId = channelId;
        this.key = key;
        this.value = value;
        this.rechargeVIPBalance = rechargeVIPBalance;
    }

    public int getChannelId() {
        return channelId;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public boolean isRechargeVIPBalance() {
        return rechargeVIPBalance;
    }

    private static final Map<String, PaymentTypeEnum> PAYMENT_TYPE_ENUM_MAP = new HashMap<>();
    private static final List<PaymentTypeEnum> PAYMENT_TYPE_ENUM_LIST = new ArrayList<>();

    private static final Map<Long, PaymentTypeEnum> PAYMENT_TYPE_ENUMS = new HashMap<>();

    static {
        PaymentTypeEnum[] paymentTypeEnums = PaymentTypeEnum.values();
        for (PaymentTypeEnum paymentTypeEnum : paymentTypeEnums) {
            PAYMENT_TYPE_ENUMS.put((long) paymentTypeEnum.getChannelId(), paymentTypeEnum);
            PAYMENT_TYPE_ENUM_MAP.put(paymentTypeEnum.getKey(), paymentTypeEnum);
            if (paymentTypeEnum.isRechargeVIPBalance()) {
                PAYMENT_TYPE_ENUM_LIST.add(paymentTypeEnum);
            }
        }
    }

    public static PaymentTypeEnum getPaymentTypeEnum(String key) {
        PaymentTypeEnum paymentTypeEnum = PAYMENT_TYPE_ENUM_MAP.get(key);
        if (paymentTypeEnum == null) {
            throw new IllegalArgumentException("支付类型异常，code : " + key);
        }
        return paymentTypeEnum;
    }

    public static PaymentTypeEnum getPaymentTypeEnum(long key) {
        PaymentTypeEnum paymentTypeEnum = PAYMENT_TYPE_ENUMS.get(key);
        if (paymentTypeEnum == null) {
            throw new IllegalArgumentException("支付类型异常，channel id : " + key);
        }
        return paymentTypeEnum;
    }

    public static List<PaymentTypeEnum> supportRechargePaymentType() {
        return PAYMENT_TYPE_ENUM_LIST;
    }

    @Override
    public String toString() {
        return key + " - " + value;
    }
}