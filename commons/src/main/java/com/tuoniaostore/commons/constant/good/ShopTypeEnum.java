package com.tuoniaostore.commons.constant.good;

public enum ShopTypeEnum {

    //0.供应链分类1.门店分类,
    SUPPLY_CHAIN(0,"供应链分类"),
    STORE(1,"门店分类")
    ;

    private int key;
    private String value;

    ShopTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 通过key 获取 value
     * @param key
     * @return
     */
    public static String getValue(int key) {
        for (ShopTypeEnum ele : values()) {
            if(ele.getKey() == key){
                return ele.getValue();
            }
        }
        return null;
    }

    /**
     * 通过value 获取 key
     * @param value
     * @return
     */
    public static int getKey(String value) {
        for (ShopTypeEnum ele : values()) {
            if(ele.getValue().equals(value)){
                return ele.getKey();
            }
        }
        return -999;
    }

}
