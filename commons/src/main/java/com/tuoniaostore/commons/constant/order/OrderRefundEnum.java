package com.tuoniaostore.commons.constant.order;

/**
 * 订单退款常量
 */
public enum OrderRefundEnum {

    APPLY(1, "申请"),
    AGREE(2, "同意"),
    REJECT(4, "拒绝"),
    ;

    private int key;
    private String value;

    OrderRefundEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }}

