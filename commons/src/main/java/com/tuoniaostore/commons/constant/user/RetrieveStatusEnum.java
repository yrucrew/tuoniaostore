package com.tuoniaostore.commons.constant.user;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by liyunbiao on 2019/3/8.
 */
public enum  RetrieveStatusEnum {
    NORMAL(0, "正常"),
    RETRIEVE(1,  "回收站");
    private int key;
    private String value;

    RetrieveStatusEnum(int key,String value){
        this.key=key;
        this.value=value;
    }

    public int getKey() {
        return key;
    }


    public String getValue() {
        return value;
    }

    private static final Map<Integer, RetrieveStatusEnum> RETRIEVE_TYPE_ENUM_MAP = new HashMap<>();

    static {
        RetrieveStatusEnum[] deviceTypeEnums = RetrieveStatusEnum.values();
        for (RetrieveStatusEnum deviceTypeEnum : deviceTypeEnums) {
            RETRIEVE_TYPE_ENUM_MAP.put(deviceTypeEnum.getKey(), deviceTypeEnum);
        }
    }

    public static RetrieveStatusEnum getRetrieveStatusEnum(int key) {
        return RETRIEVE_TYPE_ENUM_MAP.get(key);
    }
}

