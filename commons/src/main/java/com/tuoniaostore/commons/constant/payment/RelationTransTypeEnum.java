package com.tuoniaostore.commons.constant.payment;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/21
 */
public enum RelationTransTypeEnum {

    //如：0提现、1收入、2支出等
    RelationTrans_TYPE_withdrawal(0,"提现"),
    RelationTrans_TYPE_INCOME(1,"收入"),
    RelationTrans_TYPE_EXPENDITURE(2,"支出"),

    ;

    private int key;
    private String value;

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    RelationTransTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    /**
     * 通过key 获取 value
     * @param key
     * @return
     */
    public static String getValue(int key) {
        for (RelationTransTypeEnum ele : values()) {
            if(ele.getKey() == key){
                return ele.getValue();
            }
        }
        return null;
    }

    /**
     * 通过value 获取 key
     * @param value
     * @return
     */
    public static int getKey(String value) {
        for (RelationTransTypeEnum ele : values()) {
            if(ele.getValue().equals(value)){
                return ele.getKey();
            }
        }
        return -999;
    }
}
