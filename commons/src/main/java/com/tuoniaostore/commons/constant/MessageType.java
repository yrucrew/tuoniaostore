package com.tuoniaostore.commons.constant;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/19
 */
public enum MessageType{

    //短信验证码存放的是session中，prefix是存session的时候 加个前缀，区别验证
    MESSAGE_TYPE_REGISTER(0,"注册验证码","register"),
    MESSAGE_TYPE_FINDPASSWORD(1,"找回密码验证码","findpassword"),
    MESSAGE_TYPE_RECHANGEPHONE(2,"重绑手机验证码","rechangephone"),
    MESSAGE_TYPE_LOGIN(3,"登录验证码","register"),
    MESSAGE_TYPE_PAYMENT(4,"支付验证码","payment"),
    MESSAGE_TYPE_WITHDRAWAL(5,"提现验证码","withdrawal"),
    MESSAGE_TYPE_CHANGEPASSWORD(6,"修改密码","changepassword"),
    MESSAGE_TYPE_RESETPAYMENT(7,"重置支付验证码","resetpayment"),
    ;

    private int key;
    private String value;
    private String prefix;

    MessageType(int key, String value,String prefix) {
        this.key = key;
        this.value = value;
        this.prefix = prefix;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public String getPrefix() {
        return prefix;
    }
}
