package com.tuoniaostore.commons.constant.supplychain;

/**
 * 仓库类型
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/27
 */
public enum WarehouseType {
    //仓库类型 如：0-直营仓、1-虚拟仓、2-第三方仓、3-合作仓、4-代理仓,5-供应商 6.普通门店 7.直营店,8.批发, 9.厂商
    STRAIGHT_BARN(0,"直营仓"),
    VIRTUAL_WAREHOUSE(1,"虚拟仓"),
    THIRD_PARTY_WAREHOUSE(2,"第三方仓"),
    COOPERATIVE_WAREHOUSE(3,"合作仓"),
    AGENCY_WAREHOUSE(4,"代理仓"),
    SUPPLIER(5,"供应商"),
    ORDINARY_STORES(6,"普通门店"),
    DIRECT_SHOP(7,"直营店"),
    WHOLESALE(8,"批发"),
    MANUFACTURER(9,"厂商"),
    ;

    private int key;
    private String value;

    WarehouseType(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 通过key 获取 value
     * @param key
     * @return
     */
    public static String getValue(int key) {
        for (WarehouseType ele : values()) {
            if(ele.getKey() == key){
                return ele.getValue();
            }
        }
        return null;
    }

    /**
     * 通过value 获取 key
     * @param value
     * @return
     */
    public static int getKey(String value) {
        for (WarehouseType ele : values()) {
            if(ele.getValue().equals(value)){
                return ele.getKey();
            }
        }
        return -999;
    }
}
