package com.tuoniaostore.commons.constant.supplychain;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 *     <b>仓库状态</b>
 * </pre>
 *
 * @author chinahuangxc on 2017/02/26.
 */
public enum ShopStatusEnum {

    /** 0 - 关闭 */
    CLOSE(0, "关闭"),
    /** 1 - 开放中(默认状态) */
    OPEN(1, "开放中"),
    /** 2 -- 仅对下级仓库开放(调拨时有效) */
    ONLY(2, "仅对内开放"),;

    private byte key;
    private String value;

    ShopStatusEnum(int key, String value) {
        this.key = (byte) key;
        this.value = value;
    }

    public byte getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    private static final Map<Byte, ShopStatusEnum> STATUS_ENUM_MAP = new HashMap<>();

    static {
        ShopStatusEnum[] warehouseStatusEnums = ShopStatusEnum.values();
        for (ShopStatusEnum statusEnum : warehouseStatusEnums) {
            STATUS_ENUM_MAP.put(statusEnum.getKey(), statusEnum);
        }
    }

    public static ShopStatusEnum getWarehouseStatusEnum(int key) {
        return STATUS_ENUM_MAP.get((byte) key);
    }
}