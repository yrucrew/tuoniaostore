package com.tuoniaostore.commons.constant.payment;

/***
 * 预到账状态enum
 * @author oy
 * @date 2019/6/12
 * @param
 * @return
 */
public enum PaymentEstimateRevenueStatusEnum {

    INITIATE_WITHDRAWAL(1,"发起提现"),
    CANCELLED(2,"已取消"),
    COLLECT_CASH(3,"打钱中"),
    COMPLETED_WITHDRAWAL(4,"已完提现")
    ;

    private Integer key;
    private String value;

    PaymentEstimateRevenueStatusEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
