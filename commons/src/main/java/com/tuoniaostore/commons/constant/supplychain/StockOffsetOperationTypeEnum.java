package com.tuoniaostore.commons.constant.supplychain;

import java.util.HashMap;
import java.util.Map;

import com.tuoniaostore.commons.exception.Nearby123shopIllegalArgumentException;

/**
 * 库存偏移操作类型
 */
public enum StockOffsetOperationTypeEnum {
	
	/** 1 - 销售出库 */
	SALE(1, "销售出库", StockOffsetTypeEnum.DELIVERY),
	/** 2 - 采购入库 */
	PURCHASE(2, "采购入库", StockOffsetTypeEnum.STORAGE),
	/** 3 - 退货入库 */
	RETURNED(3, "退货入库", StockOffsetTypeEnum.STORAGE),;
	
	int type;
	String value;
	StockOffsetTypeEnum stockOffsetTypeEnum;
	
	private static Map<Integer, StockOffsetOperationTypeEnum> STOCK_OFFSET_OPERATION_TYPE_ENUM_MAP = new HashMap<>();
	
	static {
		for (StockOffsetOperationTypeEnum stockOffsetOperationTypeEnum : StockOffsetOperationTypeEnum.values()) {
			STOCK_OFFSET_OPERATION_TYPE_ENUM_MAP.put(stockOffsetOperationTypeEnum.getType(), stockOffsetOperationTypeEnum);
		}
	}
	
	public static StockOffsetOperationTypeEnum getByType(int type) {
		StockOffsetOperationTypeEnum stockOffsetOperationTypeEnum = STOCK_OFFSET_OPERATION_TYPE_ENUM_MAP.get(type);
		if (stockOffsetOperationTypeEnum == null) {
			throw new Nearby123shopIllegalArgumentException("错误的进出库操作类型");
		}
		return stockOffsetOperationTypeEnum;
	}
	
	private StockOffsetOperationTypeEnum(int type, String value, StockOffsetTypeEnum stockOffsetTypeEnum) {
		this.type = type;
		this.value = value;
		this.stockOffsetTypeEnum = stockOffsetTypeEnum;
	}
	// getter
	public int getType() {
		return type;
	}
	public String getValue() {
		return value;
	}
	public StockOffsetTypeEnum getStockOffsetTypeEnum() {
		return stockOffsetTypeEnum;
	}
}
