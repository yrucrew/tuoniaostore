package com.tuoniaostore.commons.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by liyunbiao on 2018/8/27.
 */
public enum DeviceTypeEnum {

    DEVICE_TYPE_UNKNOW(-1,"未知设备"),
    DEVICE_TYPE_ANDROID(1,"安卓"),
    DEVICE_TYPE_IOS(2,"IOS"),
    DEVICE_TYPE_H5(3,"H5"),
    DEVICE_TYPE_HTML(4,"HTML"),;

    private int key;
    private String value;

    DeviceTypeEnum(int key, String value){
        this.key=key;
        this.value=value;
    }

    public int getKey() {
        return key;
    }


    public String getValue() {
        return value;
    }

    private static final Map<Integer, DeviceTypeEnum> DEVICE_TYPE_ENUM_MAP = new HashMap<>();

    static {
        DeviceTypeEnum[] deviceTypeEnums = DeviceTypeEnum.values();
        for (DeviceTypeEnum deviceTypeEnum : deviceTypeEnums) {
            DEVICE_TYPE_ENUM_MAP.put(deviceTypeEnum.getKey(), deviceTypeEnum);
        }
    }

    public static DeviceTypeEnum getDeviceTypeEnum(int key) {
        return DEVICE_TYPE_ENUM_MAP.get(key);
    }


}
