package com.tuoniaostore.commons.constant.pay;

/**
 * 微信支付的类型：用来区分小程序，APP，网页端
 * @author oy
 * @date 2019/4/20
 * @param
 * @return
 */
public enum WeiXinPayTypeEnum {

    //JSAPI--JSAPI支付（或小程序支付）、NATIVE--Native支付、APP--app支付，MWEB--H5支付，不同trade_type决定了调起支付的方式，请根据支付产品正确上传
    JSAPI(1,"JSAPI"),
    NATIVE(2,"NATIVE"),
    APP(3,"APP"),
    MWEB(4,"MWEB"),
    MICROPAY(5,"MICROPAY")
    ;

    private int key;
    private String value;

    WeiXinPayTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 通过key 获取 value
     * @param key
     * @return
     */
    public static String getValue(int key) {
        for (WeiXinPayTypeEnum ele : values()) {
            if(ele.getKey() == key){
                return ele.getValue();
            }
        }
        return null;
    }

    /**
     * 通过value 获取 key
     * @param value
     * @return
     */
    public static int getKey(String value) {
        for (WeiXinPayTypeEnum ele : values()) {
            if(ele.getValue().equals(value)){
                return ele.getKey();
            }
        }
        return -999;
    }

}
