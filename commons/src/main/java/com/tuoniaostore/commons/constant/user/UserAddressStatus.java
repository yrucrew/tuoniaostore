package com.tuoniaostore.commons.constant.user;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/23
 */
public enum UserAddressStatus {

    NORMAL(0,"启用"),
    DISABLED(1,"失效"),
    DEFAULT(2,"默认"),;
    private  int key;
    private String value;

    UserAddressStatus(int key,String value){
        this.key=key;
        this.value=value;
    }

    public int getKey() {
        return key;
    }


    public String getValue() {
        return value;
    }
}
