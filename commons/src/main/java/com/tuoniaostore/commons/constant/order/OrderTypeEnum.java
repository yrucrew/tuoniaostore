package com.tuoniaostore.commons.constant.order;

/**
 * @author sqd
 * @email 907722435@qq.com
 * @date 2019/3/30
 */
public enum OrderTypeEnum {

    /**
     * 订单类型
     * 1-供应链采购订单[子单]
     * 2-供应链调拨订单
     * 8-供应链销售订单[父单]
     * 16-超市POS机订单（包括超市扫码订单）
     * 32-外卖配送订单 64增值服务订单
     */
    SUPPLYCHAIN(1,"供应链采购订单"),
    SUPPLYCHAINALLOCATION(2,"供应链调拨订单"),
    SUPPLYCHAINPURCHASE(8,"供应链销售订单"),
    SUPERMARKETPOS(16,"超市POS机订单（包括超市扫码订单）"),
    CONVENIENCESTORE(32,"外卖配送订单"),
    ADDSERVICES(64,"增值服务订单"),
    ;
    private int key;
    private String value;

    OrderTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }


    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}
