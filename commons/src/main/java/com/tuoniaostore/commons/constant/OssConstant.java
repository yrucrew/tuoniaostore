package com.tuoniaostore.commons.constant;

public class OssConstant {

    public static final String COMMON_PATH = "Folder/";
    /**
     * 订单退款备注图片
     */
    public static final String ORDER_REFUND_REMARK = COMMON_PATH + "order_refund_remark/";
}
