package com.tuoniaostore.commons.constant.good;


/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/28
 */
public enum GoodPriceType {

    //0.普通 1.价格  供应链：普通的价格直接在chaingood那个表上 如果是价格 那么要去查找一下对应绑定chainPrice 的价格
    ORDINARY(0,"普通"),
    PRICE(1,"价格"),
    ;
    private int key;
    private String value;

    GoodPriceType(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 通过key 获取 value
     * @param key
     * @return
     */
    public static String getValue(int key) {
        for (GoodPriceType ele : values()) {
            if(ele.getKey() == key){
                return ele.getValue();
            }
        }
        return null;
    }

    /**
     * 通过value 获取 key
     * @param value
     * @return
     */
    public static int getKey(String value) {
        for (GoodPriceType ele : values()) {
            if(ele.getValue().equals(value)){
                return ele.getKey();
            }
        }
        return -999;
    }

}
