package com.tuoniaostore.commons.constant.payment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 金额单位
 * @author sqd
 * @date 2019/4/26
 * @param
 * @return
 */
public enum TransUnitEnum {

    TRANS_UNIT_PENNY(0,"分"),
    TRANS_UNIT_ANGLE(1,"角"),
    TRANS_UNIT_DOLLAR (2,"元"),
    ;

    private int key;
    private String value;

    TransUnitEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}
