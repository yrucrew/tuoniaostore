package com.tuoniaostore.commons.constant.payment;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/21
 */
public enum PayTypeEnum {

    //支付类型 0-余额 1-微信 2-支付宝 3-银联 4-现金等 如有其他交易方式 由逻辑决定
    TRANS_TYPE_BALANCE(0,"余额"),
    TRANS_TYPE_WEI(1,"微信"),
    TRANS_TYPE_ZFB(2,"支付宝"),
    TRANS_TYPE_UNIONPAY(3,"银联"),
    TRANS_TYPE_CASH(4,"现金"),
    ;

    private int key;
    private String value;

    PayTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}
