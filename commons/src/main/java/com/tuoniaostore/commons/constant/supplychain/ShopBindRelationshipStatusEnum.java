package com.tuoniaostore.commons.constant.supplychain;

public enum ShopBindRelationshipStatusEnum {

	DISABLE(0,"不可用"),
	NORMAL(1,"可用"),
	;

	int type;
	String value;
	private ShopBindRelationshipStatusEnum(int type, String value) {
		this.type = type;
		this.value = value;
	}
	public int getType() {
		return type;
	}
	public String getValue() {
		return value;
	}
}
