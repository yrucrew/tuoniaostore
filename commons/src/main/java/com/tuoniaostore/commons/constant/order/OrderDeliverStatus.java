package com.tuoniaostore.commons.constant.order;

/**
 * @author sqd
 * @email 907722435@qq.com
 * @date 2019/3/30
 */
public enum OrderDeliverStatus {

    //发货状态：0-未发货、32-发货中、64-配送中、128-已送达
    UNSHIPPED(0,"未发货"),
    THEDELIVERY(32,"发货中"),
    DISTRIBUTION(64,"配送中"),
    HAVEARRIVED(128,"已送达"),
    ;
    private int key;
    private String value;

    OrderDeliverStatus(int key, String value) {
        this.key = key;
        this.value = value;
    }


    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}
