package com.tuoniaostore.commons.constant.user;

import java.util.HashMap;
import java.util.Map;

import com.tuoniaostore.commons.exception.Nearby123shopIllegalArgumentException;

/** 用户角色请求枚举 */
public enum UserRoleTypeEnum {
	
	PURCHASE(1, "采购专员"),
	OPERATIONS_MANAGER(2, "运营管理员"),
	OPERATE(3, "运营"),
	WARE_HOUSEMAN(4, "仓库管理员"),
	PURCHASING_MANAGER(5, "采购管理员"),
	DISTRIBUTION_MANAGER(6, "配送管理员"),
	PICKING_MANAGER(7, "拣货管理员"),
	STORE_ADMINISTRATOR(8, "门店管理员");

	int key;
	String value;
	
	private static Map<Integer, UserRoleTypeEnum> USER_ROLE_TYPE_ENUM_MAP = new HashMap<>();
	
	static {
		for (UserRoleTypeEnum userRoleReqEnum : UserRoleTypeEnum.values()) {
			USER_ROLE_TYPE_ENUM_MAP.put(userRoleReqEnum.getKey(), userRoleReqEnum);
		}
	}
	
	public static UserRoleTypeEnum getUserRoleTypeEnum(int key) {
		UserRoleTypeEnum userRoleReqEnum = USER_ROLE_TYPE_ENUM_MAP.get(key);
		if (userRoleReqEnum == null) {
			throw new Nearby123shopIllegalArgumentException("角色类型出错,code:" + key);
		}
		return userRoleReqEnum;
	}
	
	private UserRoleTypeEnum(int key, String value) {
		this.key = key;
		this.value = value;
	}

	public int getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}
	
}
