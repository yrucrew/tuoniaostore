package com.tuoniaostore.commons.constant.supplychain;

/**
 * 商家端采购：首页数据type类型
 * @author oy
 * @date 2019/4/12
 * @param
 * @return
 */
public enum HomepageGoodsSettingTypeEnum {

    //记录类型 0=精选商品; 1=推荐商品; 2=热门品类 3=热门品牌
    SELECTED_COMMODITIES(0,"精选商品"),
    RECOMMENDING_COMMODITIES(1,"推荐商品"),
    HOT_CATEGORY(2,"热门品类"),
    HOT_BRANDS(3,"热门品牌"),
    ALL(null,"全部")
    ;

    private Integer key;
    private String value;

    HomepageGoodsSettingTypeEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 通过key 获取 value
     * @param key
     * @return
     */
    public static String getValue(Integer key) {
        for (HomepageGoodsSettingTypeEnum ele : values()) {
            if(ele.getKey().equals(key)){
                return ele.getValue();
            }
        }
        return null;
    }

    /**
     * 通过value 获取 key
     * @param value
     * @return
     */
    public static int getKey(String value) {
        for (HomepageGoodsSettingTypeEnum ele : values()) {
            if(ele.getValue().equals(value)){
                return ele.getKey();
            }
        }
        return -999;
    }


    public static void main(String[] args) {
        Integer a = 3;
        String value = HomepageGoodsSettingTypeEnum.getValue(a);
        System.out.println(value);
    }

}
