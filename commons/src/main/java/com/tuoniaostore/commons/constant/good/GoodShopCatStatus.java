package com.tuoniaostore.commons.constant.good;


/**
 * @author sqd
 * @date 2019/4/10
 * @param
 * @return
 */
public enum GoodShopCatStatus {

    UNPURCHASED(1,"未购买"),
    HAVEBOUGHT(2,"已购买"),
    ;
    private int key;
    private String value;

    GoodShopCatStatus(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
