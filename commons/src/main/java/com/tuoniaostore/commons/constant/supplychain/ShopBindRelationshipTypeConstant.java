package com.tuoniaostore.commons.constant.supplychain;

public class ShopBindRelationshipTypeConstant {

    /**
     * 1-仓管绑定仓库
     */
    public static final int SHOP_BIND_SHOP = 1;
    /**
     * 2-拣货员绑定仓库
     */
    public static final int PICK_BIND_SHOP = 2;
    /**
     * 3-配送员绑定仓库
     */
    public static final int COURIER_BIND_SHOP = 3;
    /**
     * 4-采购绑定供应商
     */
    public static final int PURCHASE_BIND_SUPPLIER_SHOP = 4;
    /**
     * 5-店员绑定门店
     */
    public static final int CLERK_BIND_SHOP = 5;
    /**
     * 6-采购绑定仓库
     */
    public static final int PURCHASE_BIND_SHOP = 6;

}
