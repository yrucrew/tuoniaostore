package com.tuoniaostore.commons.constant.good;

/**
 * @author oy
 * @description
 * @date 2019/5/18
 */
public enum  GoodTableNameEnum {
    goodBrand(1,"good_brand"),
    goodTemplate(2,"good_template"),
    goodType(3,"good_type"),
    goodUnit(4,"good_unit")
    ;
    private Integer key;
    private String value;

    GoodTableNameEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 通过key 获取 value
     * @param key
     * @return
     */
    public static String getValue(Integer key) {
        for (GoodTableNameEnum ele : values()) {
            if(ele.getKey().equals(key)){
                return ele.getValue();
            }
        }
        return null;
    }

    /**
     * 通过value 获取 key
     * @param value
     * @return
     */
    public static int getKey(String value) {
        for (GoodTableNameEnum ele : values()) {
            if(ele.getValue().equals(value)){
                return ele.getKey();
            }
        }
        return -999;
    }

}
