package com.tuoniaostore.commons.constant;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/20
 */
public enum MessageStatus {

    //短信发送成功状态
    MESSAGE_STATUS_SUCCESS(0,"发送成功"),
    MESSAGE_STATUS_FAILD(1,"发送失败"),;

    private int key;
    private String value;

    MessageStatus(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}
