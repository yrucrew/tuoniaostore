package com.tuoniaostore.commons.constant.supplychain;

/**
 * 货物状态
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/29
 */
public enum ChainGoodStatus {

    //当前状态 -1-隐藏(逻辑删除) 0-未上架 1-可售 2-失效
    DELETE(-1,"逻辑删除"),
    NOT_NO_SHELVES(0,"未上架"),
    SALABLE(1,"可售"),
    INVALID(2,"失效"),
    normal(3,"正常") //正常的是 0 1 2的
    ;

    private int key;
    private String value;

    ChainGoodStatus(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 通过key 获取 value
     * @param key
     * @return
     */
    public static String getValue(int key) {
        for (ChainGoodStatus ele : values()) {
            if(ele.getKey() == key){
                return ele.getValue();
            }
        }
        return null;
    }

    /**
     * 通过value 获取 key
     * @param value
     * @return
     */
    public static int getKey(String value) {
        for (ChainGoodStatus ele : values()) {
            if(ele.getValue().equals(value)){
                return ele.getKey();
            }
        }
        return -999;
    }

}
