package com.tuoniaostore.commons.constant.payment;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/23
 */
public enum  TransStatusEnum {

    /** 1 未支付(审核中) */
    TRADE_DEFAULT(1, "未支付"),
    /** 2 -- 交易创建 */
    TRADE_CREATED(2, "交易创建"),
    /** 4 -- 支付成功 -- 客户端 */
    TRADE_SUCCESS_CLIENT(4, "支付成功 -- 客户端"),
    /** 8 -- 支付成功（提现打款） -- 服务器 */
    TRADE_SUCCESSED_SERVER(8, "支付成功 -- 服务器"),
    /** 12 -- 交易成功（提现到账） */
    TRADE_FINISHED(12, "交易成功"),
    /** 16 -- 交易关闭 */
    TRADE_CLOSED(16, "交易关闭"),
    /** 32 -- 拒绝交易（拒绝提现） */
    TRADE_REJECT(32, "拒绝交易"),
    /** 64 -- 隐藏(逻辑删除) */
    TRADE_HIDE(64, "隐藏(逻辑删除)"),
    /** 128 -- 采购员审核中 */
    TRADE_PURCHASE_EXAMINING(128, "采购员审核中"),
    /** 256 -- 平台审核中 */
    TRADE_PLATFORM_EXAMINING(256, "平台审核中"),;

    private int key;
    private String value;

    TransStatusEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    /**
     * 通过key 获取 value
     * @param key
     * @return
     */
    public static String getValue(int key) {
        for (TransStatusEnum ele : values()) {
            if(ele.getKey() == key){
                return ele.getValue();
            }
        }
        return null;
    }

    /**
     * 通过value 获取 key
     * @param value
     * @return
     */
    public static int getKey(String value) {
        for (TransStatusEnum ele : values()) {
            if(ele.getValue().equals(value)){
                return ele.getKey();
            }
        }
        return -999;
    }


    //测试通过key 获取 value
    public static void main(String[] args) {
        int key = TransStatusEnum.TRADE_CREATED.getKey();
        String value = TransStatusEnum.getValue(key);
        System.out.println(value);
    }

}

