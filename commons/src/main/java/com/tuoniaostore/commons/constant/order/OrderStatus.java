package com.tuoniaostore.commons.constant.order;

/**
 * @author shaoqidong
 * @email 907722435@qq.com
 * @date 2019/3/27
 */
public enum OrderStatus {

    /**
     * 订单状态 不要占用0
     * 1-创建订单（未确认）、2-已取消、8-已支付、16-已接单、32-发货中、64-配送中、
     * 128-已送达、256-确认收货/已完成、260-***确认收货/已完成、512-已收款、1024-分批配送、2048-拒绝订单。
     */
    ORDER_TYPE_UNCONFIRMED(1, "未确认"),
    ORDER_TYPE_CANCELED(2, "已取消"),
    ORDER_TYPE_PAID(8, "已支付"),
    ORDER_TYPE_HAVEORDER(16, "已接单"),
    ORDER_TYPE_SHIPMENTS(32, "发货中"),
    ORDER_TYPE_DISTRIBUTION(64, "配送中"),
    ORDER_TYPE_DELIVERY(128, "已送达"),
    ORDER_TYPE_CONFIRMRECEIPT(256, "确认收货"),
    ORDER_TYPE_STOCKS(260, "确认收货"),
    ORDER_TYPE_GATHERING(512, "已收款"),
    ORDER_TYPE_PARTIALDELIVERY(1024, "分批配送"),
    ORDER_TYPE_REFUSEORDER(2048, "拒绝订单"),
    ORDER_TYPE_REFUND(4096, "已退款"),
    ;
    private int key;
    private String value;
    private String prefix;

    OrderStatus(int key, String value) {
        this.key = key;
        this.value = value;
    }


    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}
