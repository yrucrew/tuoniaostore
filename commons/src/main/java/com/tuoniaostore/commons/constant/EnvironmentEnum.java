package com.tuoniaostore.commons.constant;

/**
 * spring的配置文件环境 枚举
 * @author oy
 * @date 2019/6/15
 * @param
 * @return
 */
public enum EnvironmentEnum {

    LOCAL(0,"local"),
    TEST(1,"test"),
    PROD(2,"prod")
    ;

    private int key;
    private String value;

    EnvironmentEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    /**
     * 通过key 获取 value
     * @param key
     * @return
     */
    public static String getValue(int key) {
        for (EnvironmentEnum ele : values()) {
            if(ele.getKey() == key){
                return ele.getValue();
            }
        }
        return null;
    }

    /**
     * 通过value 获取 key
     * @param value
     * @return
     */
    public static int getKey(String value) {
        for (EnvironmentEnum ele : values()) {
            if(ele.getValue().equals(value)){
                return ele.getKey();
            }
        }
        return -999;
    }

}
