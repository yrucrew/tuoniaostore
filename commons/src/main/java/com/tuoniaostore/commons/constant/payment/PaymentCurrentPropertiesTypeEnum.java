package com.tuoniaostore.commons.constant.payment;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/21
 */
public enum PaymentCurrentPropertiesTypeEnum {

    //目前做为支付的扩展表的类型在使用
    PAYMENT_CURRENT_PROPERTIES_TYPE_PAY(0,"支付密码"),;

    private  int key;
    private String value;

    PaymentCurrentPropertiesTypeEnum(int key,String value){
        this.key=key;
        this.value=value;
    }

    public int getKey() {
        return key;
    }


    public String getValue() {
        return value;
    }



}
