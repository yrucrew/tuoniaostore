package com.tuoniaostore.commons.constant;

/**
 * Created by liyunbiao on 2018/12/9.
 */
public enum StatusEnum {

    NORMAL(0,"正常"),
    ALL(-1,"全部"),
    DISABLE(1,"禁用"),;
    private  int key;
    private String value;

    StatusEnum(int key,String value){
        this.key=key;
        this.value=value;
    }

    public int getKey() {
        return key;
    }


    public String getValue() {
        return value;
    }
}
