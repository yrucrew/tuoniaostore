package com.tuoniaostore.commons.constant.payment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/23
 */
public enum TransTypeEnum {

    /** 1 -- 支付 */
    PAYMENT(1, "支付", true, true),
    /** 2 -- 充值 */
    RECHARGE(2, "充值", true, true),
    /** 3 -- 代付 */
    OTHER_PAYMENT(3, "代付", false, false),
    /** 4 -- 供应链收入 */
    SUPPLYCHAIN_INCOME(4, "供应链收入", true, true),
    /** 5 -- 供应链支付 */
    SUPPLYCHAIN_PAYMENT(5, "供应链支付", true, true),
    /** 6 -- 提现 */
    DRAW_CASH(6, "提现", false, true),
    /** 7 -- 提现退款 */
    REJECT_DRAW_CASH(7, "提现退款", true, false),
    /** 8 -- 退款 */
    REFUND(8, "退款", true, true),
    /** 9 -- 调拨支付 */
    ALLOCATION_PAYMENT(9, "调拨支出", false, false),
    ;


    private int key;
    private String value;
    private boolean income;
    private boolean payment;

    private static final Map<Integer, TransTypeEnum> TRANS_TYPE_ENUM_MAP = new HashMap<>();

    private static final List<TransTypeEnum> validTransTypes = new ArrayList<>();

    static {
        for (TransTypeEnum transTypeEnum : values()) {
            TRANS_TYPE_ENUM_MAP.put(transTypeEnum.key, transTypeEnum);
            if (transTypeEnum.income || transTypeEnum.payment) {
                validTransTypes.add(transTypeEnum);
            }
        }
    }

    TransTypeEnum(int key, String value, boolean income, boolean payment) {
        this.key = key;
        this.value = value;
        this.income = income;
        this.payment = payment;
    }

    public static TransTypeEnum getTransTypeEnum(Integer key) {
        TransTypeEnum transTypeEnum = TRANS_TYPE_ENUM_MAP.get(key);
        if (transTypeEnum == null) {
            throw new IllegalArgumentException("交易类型异常，key : " + key);
        }
        return transTypeEnum;
    }

    public static List<TransTypeEnum> getValidTransTypes() {
        return validTransTypes;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public boolean isIncome() {
        return income;
    }

    public boolean isPayment() {
        return payment;
    }

}
