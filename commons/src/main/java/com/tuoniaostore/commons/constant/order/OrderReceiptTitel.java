package com.tuoniaostore.commons.constant.order;

/**
 * 功能描述
 * @author sqd
 * @date 2019/5/9
 * @param
 * @return
 */
public class OrderReceiptTitel {
    public static final String TITLE = "鸵鸟生活";
    public static final String SUBTITLE = "让生活更轻松";
    public static final String UNDERSIGNED = "成为全球便利零售三大企业之一，帮助10万名创业者走向成功，成为一家受人尊敬的企业";
    public static final Double DISCOUNT  = 1.0;  // 折扣
    public static final String DISCOUNT_CATEGORY  = "d56f93a065294d73a82b71ffd5984b27bqlyQU";  // 不折扣分类 烟、酒类
    public static final String DISCOUNT_CATEGORY2  = "b18e79e42f6a4c7a9844b5d9cbea23edUwIGFX";  // 不折扣分类 烟、酒类
    public static final String PRIMARY_CATEGORY  = "afdd09ad5d664557b485f58f48572e32ZnfsvX";  // 不折扣分类 烟、酒类
}
