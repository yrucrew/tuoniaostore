package com.tuoniaostore.commons.constant.supplychain;

public enum ShopBindRelationshipTypeEnum {

    /**
     * 1-仓管绑定仓库
     */
    SHOP_BIND_SHOP(1, "仓管绑定仓库"),
    /**
     * 2-拣货员绑定仓库
     */
    PICK_BIND_SHOP(2, "拣货员绑定仓库"),
    /**
     * 3-配送员绑定仓库
     */
    COURIER_BIND_SHOP(3, "配送员绑定仓库"),
    /**
     * 4-采购绑定供应商
     */
    PURCHASE_BIND_SUPPLIER_SHOP(4, "采购绑定供应商"),
    /**
     * 5-店员绑定门店
     */
    CLERK_BIND_SHOP(5, "店员绑定门店"),
    /**
     * 6-采购绑定仓库
     */
    PURCHASE_BIND_SHOP(6, "采购员绑定仓库"),
    ;

    int type;
    String value;

    private ShopBindRelationshipTypeEnum(int type, String value) {
        this.type = type;
        this.value = value;
    }

    public int getType() {
        return type;
    }
    public int getKey() {
        return type;
    }

    public String getValue() {
        return value;
    }
}
