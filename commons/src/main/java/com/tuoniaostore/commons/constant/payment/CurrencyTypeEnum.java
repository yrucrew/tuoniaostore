package com.tuoniaostore.commons.constant.payment;

/**
 * 余额类型
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/21
 */
public enum CurrencyTypeEnum {

    CURRENCY_TYPE_DEFAULT(0,"余额"),
    CURRENCY_TYPE_VIP(1,"会员余额"),       ;

    private Integer key;
    private String value;

    CurrencyTypeEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }}
