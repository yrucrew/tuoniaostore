package com.tuoniaostore.commons.constant.user;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by liyunbiao on 2018/8/28.
 */
public enum ClientTypeEnum {
    /** 1、快递员 */
    COURIER(1, 1, "ae42baf420c140099b5ff1f44a555678nqwKHX", "快递员"),
    /** 2、POS */
    POS(2, 2, "6901f7e39e9843758a13a166e03d97daGLktxR", "POS"),
    /** 3、消费者(原来的商家) */
    CONSUMER(3, 3, "332db1d4b7184768be8950028393a9f0ahcSZQ", "C端用户"),
    /** 4、商家 */
    MERCHANT(4, 4, "e2896824c0684d53afd8bf8588f1f3eaTrrSVr", "商家"),
    /** 5、仓管 */
    WAREHOUSE(5, 5, "cd0c1d8f4a4146d1971dcbb8269e3121HkZANx", "仓管"),
    /** 6、采购 */
    PURCHASE(6, 5, "c161dd27a0b94adcbd8cac11f0e45718TjmZwR", "采购"),
    /** 7、供应商 */
    SUPPLIER(7, 3, "5f3691aa09024e30ad4af74928c7134bUUVnwd", "供应商"),
    /** 8、后台 */
    BACKSTAGE(8, 8, "57f5e9d0a6da43aaac0e112a95a4aa78", "后台"),
    ;

    private int key;
    private int clientVersionType;
    private String code;
    private String name;

    ClientTypeEnum(int key, int clientVersionType, String code, String name) {
        this.key = key;
        this.clientVersionType = clientVersionType;
        this.code = code;
        this.name = name;
    }

    public int getKey() {
        return key;
    }

    public int getClientVersionType() {
        return clientVersionType;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    private static final Map<Integer, ClientTypeEnum> CLIENT_TYPE_ENUM_MAP = new HashMap<>();

    static {
        ClientTypeEnum[] clientTypeEnums = ClientTypeEnum.values();
        for (ClientTypeEnum clientTypeEnum : clientTypeEnums) {
            CLIENT_TYPE_ENUM_MAP.put(clientTypeEnum.getKey(), clientTypeEnum);
        }
    }

    public static ClientTypeEnum getClientTypeEnum(int key) {
        return CLIENT_TYPE_ENUM_MAP.get(key);
    }
}
