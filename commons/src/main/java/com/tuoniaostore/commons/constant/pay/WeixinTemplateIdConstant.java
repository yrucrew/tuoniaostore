package com.tuoniaostore.commons.constant.pay;

public class WeixinTemplateIdConstant {

    //商家模板的申请提现模板
    public static final String WEIXIN_TEMPLATE_APPLY = "V-JFmoCkunNXwJhPciICM2lSLRUjYW6sRzKZQ17c-gs";
    //供应商的申请提现模板
    public static final String WEIXIN_TEMPLATE_APPLY_SUPPLIER = "qGEVNxIskEbdLawKNziuffLdEfg6aC7RB0i7IhDHo2o";
    //商家端提现成功
    public static final String WEIXIN_TEMPLATE_REMIT = "V4s9ZNc8bF0CTXQgaO5kAq_hOPWZC2rOS_ZsBMbI21M";
    //商家提现失败
    public static final String WEIXIN_TEMPLATE_REFUSE = "nE8N3bkuDsmpDE6qSn20tCNHxmprnAF1CYX49RY0j40";
    //供应商提现成功
    public static final String WEIXIN_TEMPLATE_REMIT_SUPPLIER = "MNzWyHelyaOhu-QHxiJLLX816nvgzZpS7jRHc0i5xww";
    //供应商提现失败
    public static final String WEIXIN_TEMPLATE_REFUSE_SUPPLIER = "IK4njhzK3pYtV7rUR3k0C-2fhCXaP1oD89kJ9mFGF4s";


}
