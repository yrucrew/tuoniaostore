package com.tuoniaostore.commons.constant.supplychain;

/**
 * 库存偏移类型
 */
public enum StockOffsetTypeEnum {
	
	/** 1 - 入库 */
	STORAGE(1, "入库"),
	/** 2 - 出库 */
	DELIVERY(2, "出库");
	
	int type;
	String value;
	private StockOffsetTypeEnum(int type, String value) {
		this.type = type;
		this.value = value;
	}
	public int getType() {
		return type;
	}
	public String getValue() {
		return value;
	}
}
