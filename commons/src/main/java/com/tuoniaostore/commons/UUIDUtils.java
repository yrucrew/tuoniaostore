package com.tuoniaostore.commons;

import java.util.UUID;

/**
 * Created by liyunbiao on 2018/11/23.
 */
public class UUIDUtils {

    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-", "")+DefineRandom.randomString(6);
    }
}
