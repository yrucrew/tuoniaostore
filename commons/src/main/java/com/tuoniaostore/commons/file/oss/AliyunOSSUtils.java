package com.tuoniaostore.commons.file.oss;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.Bucket;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.CreateBucketRequest;
import com.aliyun.oss.model.ObjectMetadata;
import com.tuoniaostore.commons.CommonUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 图片上传
 */
public class AliyunOSSUtils {

    private static final AtomicLong LOGIC_UNIQUE_KEY = new AtomicLong(10000);

    public static void createBucket(String bucketName) {
        // 创建OSSClient实例
        OSSClient ossClient = getOSSClient();

        CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucketName);
        // 设置bucket权限
        createBucketRequest.setCannedACL(CannedAccessControlList.PublicRead);
        // 创建bucket
        ossClient.createBucket(createBucketRequest);
        // 关闭client
        ossClient.shutdown();
    }

    public static List<Bucket> listBuckets() {
        // 创建OSSClient实例
        OSSClient ossClient = getOSSClient();
        // 列举bucket
        List<Bucket> buckets = ossClient.listBuckets();
        for (Bucket bucket : buckets) {
            System.out.println(" - " + bucket.getName());
        }
        // 关闭client
        ossClient.shutdown();
        return buckets;
    }

    public static void removeBucket(String bucketName) {
        // 创建OSSClient实例
        OSSClient ossClient = getOSSClient();
        try {
            // 删除bucket
            ossClient.deleteBucket(bucketName);
        } catch (Throwable cause) {
            String message = cause.getMessage();
            if (CommonUtils.nullToEmpty(message).contains("The specified bucket does not exist.")) {
                System.out.println(bucketName + " 已不存在");
                return;
            }
            cause.printStackTrace();
        }
        // 关闭client
        ossClient.shutdown();
    }

    public static boolean doesBucketExist(String bucketName) {
        OSSClient ossClient = getOSSClient();
        boolean exists = ossClient.doesBucketExist(bucketName);
        // 关闭client
        ossClient.shutdown();
        return exists;
    }

    public static String uploadImageFile(String bucketName, String remoteDirectory, String filePath) throws FileNotFoundException {
        OSSClient ossClient = getOSSClient();

        File file = new File(filePath);
        InputStream input = new FileInputStream(file);

        String key = remoteDirectory + "/" + file.getName();

        ObjectMetadata objectMeta = new ObjectMetadata();
        objectMeta.setContentType("image/jpeg");

        ossClient.putObject(bucketName, key, input, objectMeta);
        ossClient.shutdown();
        return "http://" + bucketName + "." + getPublicAccessURL(key);
    }

    private static String getPublicOnlyKey(String path) {
        return "public/" + path + "/" + System.currentTimeMillis() + "/" + LOGIC_UNIQUE_KEY.incrementAndGet();
    }

    private static String getPublicAccessURL(String key) {
        return AliyunConfig.getOssPublicEndPoint() + "/" + key;
    }

    private static OSSClient getOSSClient() {
        return new OSSClient(AliyunConfig.getOssPublicEndPoint(), AliyunConfig.getAccessKeyId(), AliyunConfig.getAccessKeySecret());
    }
    public  static String uploadImageFiles(String uploadFile,String bucketName,InputStream inputStream){
        // 创建OSSClient实例
        OSSClient ossClient = new OSSClient(AliyunConfig.getOssPublicEndPoint(), AliyunConfig.getAccessKeyId(), AliyunConfig.getAccessKeySecret());
        ossClient.putObject(bucketName, uploadFile, inputStream);
        // 关闭client
        ossClient.shutdown();


        return "http://" + bucketName + "." + getPublicAccessURL(uploadFile);
    }
    public  static String delImageFiles(String uploadFile,String bucketName){
        // 创建OSSClient实例
        OSSClient ossClient = new OSSClient(AliyunConfig.getOssPublicEndPoint(), AliyunConfig.getAccessKeyId(), AliyunConfig.getAccessKeySecret());
        ossClient.deleteObject(bucketName,uploadFile);
        // 关闭client
        ossClient.shutdown();
        return "http://" + bucketName + "." + getPublicAccessURL(uploadFile);
    }
    public static void  main(String[] args){

        delImageFiles("code/5e4aa67db05742e4971478bae0ce8466cmSOYI20190204.gif","ostrichapps");
    }
}