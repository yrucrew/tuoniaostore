package com.tuoniaostore.commons;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by liyunbiao on 2018/10/17.
 */
public class SignatureSecurityUtils {

    public static void signatureSecurity(HttpServletRequest request) {
        Map<String, String[]> requestParams = request.getParameterMap();
        Map<String, String> signParameters = new HashMap<>();

        String partnerId = request.getParameter("partnerUserId");
        String appId = request.getParameter("appId");

        String signature = request.getParameter("sign");
        for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
            String name = entry.getKey();
            String[] values = entry.getValue();
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            if ("sign".equals(name)) {
                continue;
            }
            signParameters.put(name, valueStr);
        }
        // 参数排序(去除内容为空的参数)
        String parameterSort = CommonUtils.parameterSort(signParameters, new ArrayList<>());

      //  signatureSecurity(partnerId, appId, signature, parameterSort);
    }
}
