package com.tuoniaostore.commons.exception;


import com.tuoniaostore.commons.BasicWebService;

/**
 * @author chinahuangxc on 2017/1/26.
 */
public class Nearby123shopRuntimeException extends RuntimeException {

    private int code = BasicWebService.FAIL_CODE;

    public Nearby123shopRuntimeException() {
        super();
    }

    public Nearby123shopRuntimeException(String message) {
        super(message);
    }

    public Nearby123shopRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public Nearby123shopRuntimeException(Throwable cause) {
        super(cause);
    }

    public Nearby123shopRuntimeException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
