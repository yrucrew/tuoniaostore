package com.tuoniaostore.commons.exception;


import com.tuoniaostore.commons.BasicWebService;

/**
 * @author chinahuangxc on 2017/1/26.
 */
public class Nearby123shopIllegalArgumentException extends IllegalArgumentException {

    private int code = BasicWebService.FAIL_CODE;

    public Nearby123shopIllegalArgumentException() {
        super();
    }

    public Nearby123shopIllegalArgumentException(String message) {
        super(message);
    }

    public Nearby123shopIllegalArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public Nearby123shopIllegalArgumentException(Throwable cause) {
        super(cause);
    }

    public Nearby123shopIllegalArgumentException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}