package com.tuoniaostore.commons;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by liyunbiao on 2018/5/23.
 */
public class RSA {

    public static final String KEY_ALGORITHM = "RSA";
    public static final String SIGNATURE_ALGORITHM = "MD5withRSA";
    private static final String PUBLIC_KEY = "RSAPublicKey";
    private static final String PRIVATE_KEY = "RSAPrivateKey";

    public static void main(String[] args) {
        long time = System.currentTimeMillis();
        // 构建密钥 其中 keys[0]为公钥 keys[1]为私钥
        String[] keys = buildRSAKey();

        // 公钥的模
        String publicModulus = getPublicModulus(keys[0]);
        // 公钥的指数
        String publicExponent = getPublicExponent(keys[0]);
        // 私钥的模
        String privateModulus = getPrivateModulus(keys[1]);
        // 私钥的指数
        String privateExponent = getPrivateExponent(keys[1]);
        System.out.println("客户端使用的密钥：\r\n" + keys[0]);
        String encrypt = encrypt("username=merchant& =123456use", publicModulus,publicExponent);
        System.out.println("加密后：" + encrypt);
        // 私钥解密
        System.out.println("服务器使用的密钥：\r\n" + keys[1]);
        System.out.println("解密后：" + decrypt(encrypt, privateModulus, privateExponent));
        System.out.println(System.currentTimeMillis() - time);
    }

    public static Map<String, Key> initKey() {
        Map<String, Key> keyMap = new HashMap<>(2, 1.0f);
        try {
            KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
            keyPairGen.initialize(1024);

            KeyPair keyPair = keyPairGen.generateKeyPair();

            RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
            RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

            keyMap.put(PUBLIC_KEY, publicKey);
            keyMap.put(PRIVATE_KEY, privateKey);
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        return keyMap;
    }

    public static String[] buildRSAKey() {
        Map<String, Key> keys = initKey();

        return new String[]{getPublicKey(keys), getPrivateKey(keys)};
    }

    public static String getPublicKey(Map<String, Key> keyMap) {
        try {
            return encryptBASE64(keyMap.get(PUBLIC_KEY).getEncoded());
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        return "";
    }

    public static String getPrivateKey(Map<String, Key> keyMap) {
        try {
            return encryptBASE64(keyMap.get(PRIVATE_KEY).getEncoded());
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        return "";
    }

    public static String getPublicModulus(String publicKey) {
        return getPublicModulus((RSAPublicKey) getPublicKey(publicKey));
    }

    public static String getPublicExponent(String publicKey) {
        return getPublicExponent((RSAPublicKey) getPublicKey(publicKey));
    }

    public static String getPublicModulus(RSAPublicKey rsaPublicKey) {
        return rsaPublicKey.getModulus().toString();
    }

    public static String getPublicExponent(RSAPublicKey rsaPublicKey) {
        return rsaPublicKey.getPublicExponent().toString();
    }

    public static String getPrivateModulus(String privateKey) {
        return getPrivateModulus((RSAPrivateKey) getPrivateKey(privateKey));
    }

    public static String getPrivateExponent(String privateKey) {
        return getPrivateExponent((RSAPrivateKey) getPrivateKey(privateKey));
    }

    public static String getPrivateModulus(RSAPrivateKey rsaPrivateKey) {
        return rsaPrivateKey.getModulus().toString();
    }

    public static String getPrivateExponent(RSAPrivateKey rsaPrivateKey) {
        return rsaPrivateKey.getPrivateExponent().toString();
    }

    // 将Base64编码后的公钥字符串转成PublicKey实例
    public static PublicKey getPublicKey(String publicModulus, String publicExponent) {
        try {
            BigInteger modulus = new BigInteger(publicModulus);
            BigInteger exponent = new BigInteger(publicExponent);
            RSAPublicKeySpec publicKeySpec = new RSAPublicKeySpec(modulus, exponent);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            return keyFactory.generatePublic(publicKeySpec);
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        return null;
    }

    // 将Base64编码后的私钥字符串转成PrivateKey实例
    public static PrivateKey getPrivateKey(String privateModulus, String privateExponent) {
        try {
            BigInteger modulus = new BigInteger(privateModulus);
            BigInteger exponent = new BigInteger(privateExponent);
            RSAPrivateKeySpec privateKeySpec = new RSAPrivateKeySpec(modulus, exponent);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            return keyFactory.generatePrivate(privateKeySpec);
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        return null;
    }

    // 将Base64编码后的公钥字符串转成PublicKey实例
    public static PublicKey getPublicKey(String publicKey) {
        try {
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(decryptBASE64(publicKey));
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            return keyFactory.generatePublic(keySpec);
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        return null;
    }

    // 将Base64编码后的私钥字符串转成PrivateKey实例
    public static PrivateKey getPrivateKey(String privateKey) {
        try {
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decryptBASE64(privateKey));
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            return keyFactory.generatePrivate(keySpec);
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        return null;
    }

    private static byte[] decryptBASE64(String key) throws Exception {
        return new BASE64Decoder().decodeBuffer(key);
    }

    private static String encryptBASE64(byte[] key) throws Exception {
        return new BASE64Encoder().encodeBuffer(key);
    }

    public static String encrypt(String content, String publicKey) {
        return encrypt(content, getPublicKey(publicKey));
    }

    public static String decrypt(String content, String privateKey) {
        return decrypt(content, getPrivateKey(privateKey));
    }

    // 公钥加密
    public static byte[] encrypt(byte[] content, Key key) throws Exception {
        Cipher cipher = Cipher.getInstance(KEY_ALGORITHM); // Java默认 "RSA" = "RSA/ECB/PKCS1Padding"
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(content);
    }

    // 私钥解密
    public static byte[] decrypt(byte[] content, Key key) throws Exception {
        Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(content);
    }

    // 编码
    public static String encrypt(String content, String publicModulus, String publicExponent) {
        return encrypt(content, getPublicKey(publicModulus, publicExponent));
    }

    public static String decrypt(String content, String privateModulus, String privateExponent) {
        return decrypt(content, getPrivateKey(privateModulus, privateExponent));
    }

    // 公钥加密，并转换成十六进制字符串打印出来
    private static String encrypt(String content, PublicKey publicKey) {
        try {
            Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);// Java默认"RSA" = "RSA/ECB/PKCS1Padding"
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);

            int splitLength = ((RSAPublicKey) publicKey).getModulus().bitLength() / 8 - 11;
            byte[][] arrays = CommonUtils.splitBytes(content.getBytes(), splitLength);
            StringBuilder builder = new StringBuilder();
            for (byte[] array : arrays) {
                builder.append(CommonUtils.byte2Hex(cipher.doFinal(array)));
            }
            return builder.toString();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        return null;
    }

    // 私钥解密，并转换成十六进制字符串打印出来
    private static String decrypt(String content, PrivateKey privateKey) {
        try {
            Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);

            int splitLength = ((RSAPrivateKey) privateKey).getModulus().bitLength() / 8;
            byte[] contentBytes = CommonUtils.hex2byte(content);
            byte[][] arrays = CommonUtils.splitBytes(contentBytes, splitLength);
            StringBuilder builder = new StringBuilder();
            for (byte[] array : arrays) {
                builder.append(new String(cipher.doFinal(array)));
            }
            return builder.toString();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        return null;
    }

}
