package com.tuoniaostore.commons.config;

import org.springframework.context.annotation.Configuration;

/**
 * Created by liyunbiao on 2018/8/16.
 */
@Configuration
public class CommonsUrlConfig {

    public static String logisticsRemoteURL;
    public static String supplychainRemoteURL;
    public static String goodRemoteURL;
    public static String orderRemoteURL;
    public static String communityRemoteURL;
    public static String paymentRemoteURL;



    public static void setCommunityRemoteURL(String communityRemoteURL) {
        CommonsUrlConfig.communityRemoteURL = communityRemoteURL;
    }


    public static void setGoodRemoteURL(String goodRemoteURL) {
        CommonsUrlConfig.goodRemoteURL = goodRemoteURL;
    }

    public static void setLogisticsRemoteURL(String logisticsRemoteURL) {
        CommonsUrlConfig.logisticsRemoteURL = logisticsRemoteURL;
    }

    public static void setSupplychainRemoteURL(String supplychainRemoteURL) {
        CommonsUrlConfig.supplychainRemoteURL = supplychainRemoteURL;
    }

    public static void setPaymentRemoteURL(String paymentRemoteURL) {
        CommonsUrlConfig.paymentRemoteURL = paymentRemoteURL;
    }

    public static void setOrderRemoteURL(String orderRemoteURL) {
        CommonsUrlConfig.orderRemoteURL = orderRemoteURL;
    }

}
