package com.tuoniaostore.commons;

import com.tuoniaostore.commons.exception.Nearby123shopIllegalArgumentException;
import com.tuoniaostore.commons.exception.Nearby123shopRuntimeException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * <pre>
 *     <b>Aop切面控制器</b>
 * </pre>
 *
 * @author chinahuangxc on 2016/11/20.
 */
@Service
@Aspect
public class ApplicationControllerAop extends BasicWebService {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationControllerAop.class);

    @Around(value = "execution(* com.tuoniaostore.*.controller.*.*(..))")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Object[] args = point.getArgs();
        try {
            return point.proceed(args);
        } catch (Nearby123shopIllegalArgumentException ex) {
            return fail(ex.getMessage());
        } catch (Nearby123shopRuntimeException ex) {
            return fail(ex.getCode(), ex.getMessage());
        } catch (Throwable cause) {
            logger.error("", cause);
            return fail("系统错误，请稍后重试！");
        }
    }
}