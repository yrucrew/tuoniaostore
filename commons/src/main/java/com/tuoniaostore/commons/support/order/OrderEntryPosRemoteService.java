package com.tuoniaostore.commons.support.order;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.vo.order.GoodSaleMsgVO;
import com.tuoniaostore.datamodel.vo.order.OrderEntryPosVO;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/5/21
 */
public class OrderEntryPosRemoteService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(OrderEntryPosRemoteService.class);

    /**
     * 获取当前商店的昨日订单商品的详细信息
     *
     * @param shopId
     * @return java.util.List<com.tuoniaostore.datamodel.order.OrderEntry>
     * @author oy
     * @date 2019/5/21
     */
    public static List<GoodSaleMsgVO> getOrderEntryForYesterDay(String shopId) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("shopId", shopId);

        Map<String, Object> headers = new HashMap();

        String tokenForInterface = TokenUtils.createTokenForInterface();
        parameters.put("token", tokenForInterface);

        JSONObject response = null;
        List<GoodSaleMsgVO> goodSaleMsgVOS = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntry/getOrderEntryForYesterDay", headers, parameters);
            if (response != null) {
                String list = response.getString("list");
                if (list != null && !list.equals("")) {
                    goodSaleMsgVOS = JSONObject.parseArray(list, GoodSaleMsgVO.class);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return goodSaleMsgVOS;
    }

    /**
     * @param saleUserId 销售仓id
     * @param type       订单类型
     * @return 订单列表
     */
    public static List<OrderEntryPosVO> getOrderEntryPosListBySaleUserIdAndType(String saleUserId, int type,int pageStartIndex, int pageSize, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        parameters.put("saleUserId", saleUserId);
        parameters.put("type", type+"");
        parameters.put("page",pageStartIndex+"");
        parameters.put("limit", pageSize+"");

        JSONObject response;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntryPos/getOrderEntryPosList", headers, parameters);
            return JSONObject.parseArray(response.getString("rows"), OrderEntryPosVO.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getOrderEntryPosListCountBySaleUserIdAndType(String saleUserId, int type,int pageStartIndex, int pageSize, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        parameters.put("saleUserId", saleUserId);
        parameters.put("type", type+"");
        parameters.put("page",pageStartIndex+"");
        parameters.put("limit", pageSize+"");

        JSONObject response;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntryPos/getOrderEntryPosList", headers, parameters);
            return JSONObject.parseObject(response.getString("total"), Integer.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}
