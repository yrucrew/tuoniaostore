package com.tuoniaostore.commons.support.supplychain;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.constant.supplychain.ShopBindRelationshipTypeEnum;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopBindRelationship;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.swing.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/4/22
 */
public class SupplychainShopBindRelationshipRemoteService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(SupplychainShopBindRelationshipRemoteService.class);

    /**
     * 远程绑定商品和用户关系
     *
     * @param supplychainShopBindRelationship, request
     * @return void
     * @author oy
     * @date 2019/4/22
     */
    public static void addSupplychainShopBindRelationship(SupplychainShopBindRelationship supplychainShopBindRelationship, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String, Object> parameters = new HashMap<>();
        //填充参数
        String s = JSONObject.toJSONString(supplychainShopBindRelationship);
        parameters.put("object", s);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainShopBindRelationship/addSupplychainShopBindRelationship2", headers, parameters);
    }

    public static List<SupplychainShopBindRelationship> getShopBindRelationships(Integer type, String userId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("sign", signature);
        parameters.put("type", type + "");
        parameters.put("userId", userId);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainShopBindRelationship/getShopBindRelationships", headers, parameters);
            return JSONObject.parseArray(response.getString("response"), SupplychainShopBindRelationship.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

    /**
     * 获取shopId下的绑定关系
     *
     * @param shopId  门店、供应商、仓库id
     * @param type    绑定类型，如：1-仓管绑定仓库 2-拣货员绑定仓库 3-配送员绑定仓库 ,4采购绑定供应商5.门店店员
     * @param request
     * @return
     */
    public static List<SupplychainShopBindRelationship> getSupplychainShopBindRelationshipByShopId(String shopId, int type, HttpServletRequest request) {

        Map<String, Object> parameters = initParameters(request);
        parameters.put("shopId", shopId);
        parameters.put("type", type+"");
        JSONObject response = null;
        try {
            response = baseHttpGet(CommonsUrlConfig.supplychainRemoteURL + "supplychainShopBindRelationship/getRelationshipsByShopIdAndType", initHeaders(request), parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert response != null;
        return JSONObject.parseArray(response.getString("response"), SupplychainShopBindRelationship.class);
    }

    /**
     * 删除关系
     * @param userId 用户id
     * @param type 绑定类型
     */
    public static void delRelationShip(String userId, int type, HttpServletRequest request) {

        Map<String, Object> parameters = initParameters(request);
        parameters.put("userId", userId);
        parameters.put("type", type);
        try {
            baseHttpGet(CommonsUrlConfig.supplychainRemoteURL + "supplychainShopBindRelationship/delRelationshipByUserIdAndType", initHeaders(request), parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 修改用户绑定关系状态
     * @param userId
     * @param type
     * @param request
     */
    public static void updateRelationshipByUserIdAndType(String userId, int type, int status, HttpServletRequest request) {

        Map<String, Object> parameters = initParameters(request);
        parameters.put("userId", userId);
        parameters.put("type", type);
        parameters.put("status", status);
        try {
            baseHttpGet(CommonsUrlConfig.supplychainRemoteURL + "supplychainShopBindRelationship/updateRelationshipByUserIdAndType", initHeaders(request), parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
