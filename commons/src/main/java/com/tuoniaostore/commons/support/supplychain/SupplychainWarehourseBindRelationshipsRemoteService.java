package com.tuoniaostore.commons.support.supplychain;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import com.tuoniaostore.datamodel.supplychain.SupplychainWarehourseBindRelationship;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sqd
 * @date 2019/6/3
 * @param
 * @return
 */
public class SupplychainWarehourseBindRelationshipsRemoteService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(SupplychainWarehourseBindRelationshipsRemoteService.class);


    /**
     * 查询仓库对应的供应商
     * @param shopId
     * @param request
     * @throws Exception
     */
    public static SupplychainWarehourseBindRelationship getSupplychainWarehourseBindRelationshipsById(String shopId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String, Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("shopId", shopId);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainWarehourseBindRelationship/SupplychainWarehourseBindRelationships", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), SupplychainWarehourseBindRelationship.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
