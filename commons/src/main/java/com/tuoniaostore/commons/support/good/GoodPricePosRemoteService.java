package com.tuoniaostore.commons.support.good;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import com.tuoniaostore.datamodel.good.GoodPrice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/4/3
 */
public class GoodPricePosRemoteService extends BasicRemoteService {

    private static final Logger logger = LoggerFactory.getLogger(GoodPricePosRemoteService.class);

    /**
     * 根据类别id 查找对应的实体
     *
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    public static GoodPrice getGoodPricePos(String id, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPricePos/getGoodPricePos", headers, parameters);
        return JSONObject.parseObject(response.getString("response"), GoodPrice.class);
    }

    /**
     * 通过价格标签 查找对应的价格标签实体
     *
     * @param typeName, httpServletRequest
     * @return com.tuoniaostore.datamodel.good.GoodPrice
     * @author oy
     * @date 2019/4/9
     */
    public static GoodPrice getGoodPriceByTitle(String typeName, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("title", typeName);

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPricePos/getGoodPricePosByTitle", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseObject(response.getString("response"), GoodPrice.class);
    }

    /**
     * 更新价格表信息
     *
     * @param title, priceId1, unitId, templateId, request
     * @return void
     * @author oy
     * @date 2019/4/9
     */
    public static void updateGoodPrice(String title, String priceId1, String unitId, String templateId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("title", title);
        parameters.put("priceId1", priceId1);
        if (unitId != null && !unitId.equals("")) {
            parameters.put("unitId", unitId);
        }
        if (templateId != null && !templateId.equals("")) {
            parameters.put("templateId", templateId);
        }
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        try {
            baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPrice/updateGoodPrice", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询两个表的模板
     *
     * @param title, request
     * @return com.tuoniaostore.datamodel.good.GoodPrice
     * @author oy
     * @date 2019/4/12
     */
    public static GoodPrice getAllGoodPriceByTitleAndGoodId(String title, String goodId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("title", title);
        parameters.put("goodId", goodId);

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPricePos/getAllGoodPriceByTitleAndGoodId", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseObject(response.getString("response"), GoodPrice.class);
    }

    /**
     * 查询当前价格id 集合对应的价格标签 和 单位
     *
     * @param
     * @return com.tuoniaostore.datamodel.good.GoodPrice
     * @author oy
     * @date 2019/4/12
     */
    public static List<GoodPriceCombinationVO> getAllGoodPricesByPriceIds(List<String> priceIds, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        String s = JSONObject.toJSONString(priceIds);
        parameters.put("priceIds", s);

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPricePos/getAllGoodPricesByPriceIds", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), GoodPriceCombinationVO.class);
    }

    /**
     * 根据价格id 查找对应的模板数据（可以后续修改sql 增加需要的品牌 条码等）
     *
     * @param priceIds, httpServletRequest
     * @return java.util.List<com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO>
     * @author oy
     * @date 2019/4/22
     */
    public static List<GoodPriceCombinationVO> getTemplateByPriceId(List<String> priceIds, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        String s = JSONObject.toJSONString(priceIds);
        parameters.put("priceIds", s);

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPricePos/getAllGoodTemplateByPriceIds", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), GoodPriceCombinationVO.class);
    }

    /**
     * 根据价格id 查找对应所有的模板数据
     *
     * @param priceIds, httpServletRequest
     * @return java.util.List<com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO>
     * @author oy
     * @date 2019/4/26
     */
    public static List<GoodPriceCombinationVO> getTemplateByPriceIdByParam(List<String> priceIds, String nameOrBarcode, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        String s = JSONObject.toJSONString(priceIds);
        parameters.put("priceIds", s);
        parameters.put("nameOrBarcode", nameOrBarcode);

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPricePos/getAllGoodTemplateByPriceIds2", headers, parameters);
            if (response.getInteger("code") == 0) {
                return JSONObject.parseArray(response.getString("list"), GoodPriceCombinationVO.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("远程调用失败！"+response.getInteger("code")+CommonsUrlConfig.goodRemoteURL + "goodPricePos/getAllGoodTemplateByPriceIds2");
        }

        return null;
    }

    /**
     * 获取拼接的模板数据 通过价格id
     *
     * @param priceId, request
     * @return com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO
     * @author oy
     * @date 2019/5/21
     */
    public static GoodPriceCombinationVO getDateByPriceId(String priceId) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("priceId", priceId);
        String tokenForInterface = TokenUtils.createTokenForInterface();
        parameters.put("token", tokenForInterface);

        Map<String, Object> headers = new HashMap();
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPrice/getDateByPriceId", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseObject(response.getString("response"), GoodPriceCombinationVO.class);
    }
}
