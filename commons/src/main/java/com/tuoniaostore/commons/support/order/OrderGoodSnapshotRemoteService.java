package com.tuoniaostore.commons.support.order;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.good.GoodBarcode;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/4/24
 */
public class OrderGoodSnapshotRemoteService extends BasicRemoteService {


    private static final Logger logger = Logger.getLogger(OrderGoodSnapshotRemoteService.class);

    public static List<OrderGoodSnapshot> getOrderEntryByOrderNum(String orderNum, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("orderNum", orderNum);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderGoodSnapshot/getOrderEntryByOrderNum", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), OrderGoodSnapshot.class);
    }

    public static List<OrderGoodSnapshot> getOrderEntryByOrderId(String orderId, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("orderId", orderId);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderGoodSnapshot/getOrderEntryByorderId", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), OrderGoodSnapshot.class);
    }
    public static List<OrderGoodSnapshot> getOrderGoodSnapshotPosMapByorderId(String orderId, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("orderId", orderId);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderGoodSnapshot/getOrderGoodSnapshotPosMapByorderId", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("response"), OrderGoodSnapshot.class);
    }

}
