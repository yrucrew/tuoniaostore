package com.tuoniaostore.commons.support.good;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.good.GoodBarcode;
import com.tuoniaostore.datamodel.good.GoodBrand;
import com.tuoniaostore.datamodel.good.GoodTemplate;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 条形码 远程调用服务
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/28
 */
public class GoodBarcodeRemoteService extends BasicRemoteService {


    private static final Logger logger = Logger.getLogger(GoodTypeRemoteService.class);
    public static List<GoodBarcode> loadGoodBarcodes(String accessToken,String signature,String appKey)
    {
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        parameters.put("sign", signature);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBarcode/getGoodBarcodeAll", headers, parameters);
            if(response.getInteger("code")==0) {
                return JSONObject.parseArray(response.getString("response"), GoodBarcode.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 根据条码查找对应的条形码
     * @param barcode
     * @param request
     * @return
     * @throws Exception
     */
    public static GoodBarcode getGoodBarcodeByBarcode(String barcode, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("barcode", barcode);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBarcode/getGoodMsgByGoodBarcode2", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseObject(response.getString("response"), GoodBarcode.class);
    }

    /**
     * 根据条码查找对应的条形码（模糊查询）
     * @param barcode
     * @param request
     * @return
     * @throws Exception
     */
    public static List<GoodBarcode> getGoodBarcodeByBarcodesLike(String barcode, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("barcode", barcode);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBarcode/getGoodBarcodeByBarcodesLike", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), GoodBarcode.class);
    }
    public static List<GoodBarcode> getGoodBarcodeAll( HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");
        Map<String,Object> parameters = new HashMap<>();

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBarcode/getGoodBarcodeAll", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("response"), GoodBarcode.class);
    }
    public static List<GoodBarcode> getGoodBarcodeAll(String accessToken,String signature,String appKey){
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        parameters.put("sign", signature);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBarcode/getGoodBarcodeAll", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("response"), GoodBarcode.class);
    }
    /**
     * 根据priceId 查找对应的条形码
     * @param priceId
     * @param request
     * @return
     * @throws Exception
     */
    public static GoodBarcode getGoodBarcodeByPriceId(String priceId, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("priceId", priceId);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBarcode/getGoodBarcodeByPriceId", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), GoodBarcode.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

    /**
     * 根据priceId 查找对应的条形码集合
     * @param priceId
     * @param request
     * @return
     * @throws Exception
     */
    public static List<GoodBarcode> getBarcodesByPriceId(String priceId, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("priceId", priceId);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBarcode/getBarcodesByPriceId", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), GoodBarcode.class);
    }


    /**
     * 添加对应的价格条码
     * @author oy
     * @date 2019/4/8
     * @param goodBarcode1, request
     * @return void
     */
    public static void addGoodBarcodeByBarCode(GoodBarcode goodBarcode1, HttpServletRequest request) throws Exception  {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("priceId", goodBarcode1.getPriceId());
        parameters.put("barcode", goodBarcode1.getBarcode());
        parameters.put("id", goodBarcode1.getId());

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBarcode/addGoodBarcode", headers, parameters);
    }


    public static List<GoodBarcode> getGoodBarcodeByPriceIds(List<String> priceIds, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        String s = JSONObject.toJSONString(priceIds);
        parameters.put("priceIds", s);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBarcode/getGoodBarcodeByPriceIds", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), GoodBarcode.class);
    }
}
