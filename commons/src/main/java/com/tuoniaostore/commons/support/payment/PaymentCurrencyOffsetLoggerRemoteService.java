package com.tuoniaostore.commons.support.payment;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyOffsetLogger;
import com.tuoniaostore.datamodel.payment.PaymentTransactionLogger;
import com.tuoniaostore.datamodel.vo.payment.PaymentCurrencyOffsetLoggerVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/5/1
 */
public class PaymentCurrencyOffsetLoggerRemoteService extends BasicRemoteService {

    private static final Logger logger = LoggerFactory.getLogger(PaymentCurrencyOffsetLoggerRemoteService.class);
    /**
     * 根据供应商用户id 和 类型获取到对应收入记录
     * @author oy
     * @date 2019/5/1
     * @param userIds, request
     * @return java.util.List<com.tuoniaostore.datamodel.vo.payment.PaymentCurrencyOffsetLoggerVO>
     */
    public static List<PaymentCurrencyOffsetLoggerVO> getPaymentTransactionLoggerTotalMoneyByUser(List<String> userIds, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("sign", signature);
        String string = JSONObject.toJSONString(userIds);
        parameters.put("ids", string);
        parameters.put("type", 1+"");
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentCurrencyOffsetLogger/getPaymentCurrencyOffsetLoggerTotalMoneyByUser", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("远程连接失败");
        }
        return JSONObject.parseArray(response.getString("list"), PaymentCurrencyOffsetLoggerVO.class);
    }

    /**
     * @author sqd
     * @date 2019/6/14
     * @return void
     */
    public static void addPaymentCurrencyOffsetLogger(PaymentCurrencyOffsetLogger paymentCurrencyOffsetLogger){
        Map<String,Object> parameters = new HashMap<>();
        Map<String,Object> packageParams = new HashMap<>();
        String string = JSONObject.toJSONString(paymentCurrencyOffsetLogger);
        parameters.put("paymentCurrencyOffsetLogger",string);
        Object json = JSONObject.toJSONString(packageParams);
        String tokenForInterface = TokenUtils.createTokenForInterface();
        parameters.put("json", json);
        parameters.put("token",tokenForInterface);
        Map<String,Object> headers = new HashMap();

        try {
            baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentCurrencyOffsetLogger/addPaymentCurrencyOffsetLoggerJson", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
