package com.tuoniaostore.commons.support.payment;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.commons.support.good.GoodTypeRemoteService;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyAccount;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;

/**
 * @author sqd
 * @date 2019/4/10
 * @param
 * @return
 */
public class PaymentCurrencyAccountService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(GoodTypeRemoteService.class);

   /**
    * 支付余额查询
    * @author sqd
    * @date 2019/4/10
    * @param
    * @return void
    */
    public static JSONObject getPaymentCurrencyAccount(HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        try {
            JSONObject response = baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentCurrencyAccount/getPaymentCurrencyAccountByCurrencyTypeEnum", headers, parameters);
            return JSONObject.parseObject(response.getString("response"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

    /**
     * 获取用户账户总余额
     * @author sqd
     * @date 2019/4/28
     * @param
     * @return com.alibaba.fastjson.JSONObject
     */
    public static JSONObject getPaymentCurrencyAccountBalanceByUserid(HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentCurrencyAccount/getPaymentCurrencyAccountBalanceByUserid", headers, parameters);
        return JSONObject.parseObject(response.getString("response"));
    }

    /**
     * 余额充值回调
     * @author sqd
     * @date 2019/5/8
     * @param
     * @return
     */
    public static void recChangeCallback(SortedMap<String, String> packageParams){
        Map<String,Object> parameters = new HashMap<>();
        Object json = JSONObject.toJSONString(packageParams);
        String tokenForInterface = TokenUtils.createTokenForInterface();
        parameters.put("json", json);
        parameters.put("token",tokenForInterface);
        Map<String,Object> headers = new HashMap();
        try {
            baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentCurrencyAccount/recChangeCallback", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取用户的余额
     */
    public static PaymentCurrencyAccount getPaymentCurrencyAccountByUserId(String userId , HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("userId", userId);
        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
             response = baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentCurrencyAccount/getPaymentCurrencyAccountBalanceByid", headers, parameters);
             return JSONObject.parseObject(response.getString("response"), PaymentCurrencyAccount.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }


    public static void changePaymentCurrencyAccount(PaymentCurrencyAccount paymentCurrencyAccount) {
        Map<String,Object> parameters = new HashMap<>();
        Map<String,Object> packageParams = new HashMap<>();
        String string = JSONObject.toJSONString(paymentCurrencyAccount);
        parameters.put("paymentCurrencyAccount",string);
        Object json = JSONObject.toJSONString(packageParams);
        String tokenForInterface = TokenUtils.createTokenForInterface();
        parameters.put("json", json);
        parameters.put("token",tokenForInterface);
        Map<String,Object> headers = new HashMap();

        try {
            baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentCurrencyAccount/changePaymentCurrencyAccountJson", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static PaymentCurrencyAccount getPaymentCurrencyAccountById(String userId) {
        Map<String,Object> parameters = new HashMap<>();
        Map<String,Object> packageParams = new HashMap<>();
        parameters.put("userId",userId);
        Object json = JSONObject.toJSONString(packageParams);
        String tokenForInterface = TokenUtils.createTokenForInterface();
        parameters.put("json", json);
        parameters.put("token",tokenForInterface);
        Map<String,Object> headers = new HashMap();

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentCurrencyAccount/getPaymentCurrencyAccountBalanceByid", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), PaymentCurrencyAccount.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }
}
