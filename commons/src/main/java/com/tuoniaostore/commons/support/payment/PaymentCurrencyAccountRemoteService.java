package com.tuoniaostore.commons.support.payment;

import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.commons.support.good.GoodTypeRemoteService;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyAccount;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/21
 */
public class PaymentCurrencyAccountRemoteService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(GoodTypeRemoteService.class);

    /**
     * 添加账户余额记录（初始化）
     * @param paymentCurrencyAccount
     * @param request
     * @throws Exception
     */
    public static void addPaymentCurrencyAccount(PaymentCurrencyAccount paymentCurrencyAccount, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("id",paymentCurrencyAccount.getId());
        parameters.put("userId",paymentCurrencyAccount.getUserId());
        parameters.put("channelId",paymentCurrencyAccount.getChannelId());

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "payAPI/initPaymentCurrencyAccount", headers, parameters);
    }

}
