package com.tuoniaostore.commons.support.payment;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.payment.PaymentEstimateRevenue;
import com.tuoniaostore.datamodel.payment.PaymentTransactionLogger;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


public class PaymentTransactionLoggerService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(PaymentTransactionLoggerService.class);

    /**
     * 远程支付记录
     * @author sqd
     * @date 2019/4/26
     * @param
     * @return
     */
    public static void addPaymentTransactionLogger(PaymentTransactionLogger paymentTransactionLogger, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("sign", signature);
        String string = JSONObject.toJSONString(paymentTransactionLogger);
        parameters.put("paymentTransactionLogger", string);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        try {
            baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentTransactionLogger/addPaymentTransactionLoggerJson", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 远程支付记录
     * @author sqd
     * @date 2019/4/26
     * @param
     * @return
     */
    public static void changePaymentTransactionLoggerMap(Map<String, Object> map, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        String tokenForInterface = TokenUtils.createTokenForInterface();
        parameters.put("token", tokenForInterface);
        //填充参数
        String transSequenceNumber = (String) map.get("primarySequenceNumbers");
        parameters.put("transSequenceNumber", transSequenceNumber);
        logger.info(transSequenceNumber+"||======退款自定义回调地址=======||" +CommonsUrlConfig.paymentRemoteURL + "paymentTransactionLogger/changePaymentTransactionLoggerByMap");
        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentTransactionLogger/changePaymentTransactionLoggerByMap", headers, parameters);
    }


    public static PaymentTransactionLogger getPaymentTransactionLogger(String id ,HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("id", id);
        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentTransactionLogger/getPaymentTransactionLogger", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseObject(response.getString("response"), PaymentTransactionLogger.class);
    }
}
