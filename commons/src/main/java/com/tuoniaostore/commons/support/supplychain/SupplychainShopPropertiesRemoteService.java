package com.tuoniaostore.commons.support.supplychain;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.constant.supplychain.ShopBindRelationshipTypeEnum;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopBindRelationship;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description 商店信息远程调用方法
 * @date 2019/4/8
 */
public class SupplychainShopPropertiesRemoteService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(SupplychainShopPropertiesRemoteService.class);


    /**
     * 通过id 查找该店铺
     *
     * @param id
     * @param request
     * @throws Exception
     */
    public static SupplychainShopProperties getSupplychainShopPropertiesById(String id, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String, Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("id", id);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainShopProperties/getSupplychainShopProperties", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), SupplychainShopProperties.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 查找该用户的店铺
     *
     * @param userId
     * @param request
     * @throws Exception
     */
    public static SupplychainShopProperties getSupplychainShopPropertiesByUserId(String userId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String, Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("userId", userId);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainShopProperties/getSupplychainShopPropertiesByUserIdCommon", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), SupplychainShopProperties.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 查找该用户的绑定的仓库 如果没有 就是找附近的仓库
     *
     * @param longitude latitude
     * @param request
     * @throws Exception
     */
    public static SupplychainShopProperties findRelationShopKey(Double longitude, Double latitude, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String, Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("longitude", longitude + "");
        parameters.put("latitude", latitude + "");

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainShopProperties/findRecentSelfShopForRemote", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseObject(response.getString("response"), SupplychainShopProperties.class);
    }

    public static JSONArray getPurchaseBindShops(String purchaseIds, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String, Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("purchaseIds", purchaseIds);

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap<>();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainShopBindRelationship/getPurchaseBindShops", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response.getJSONObject("response").getJSONArray("datas");
    }


    /**
     * 查找该用户的店铺
     *
     * @param
     * @param request
     * @throws Exception
     */
    public static List<SupplychainShopProperties> getSupplychainShopPropertiesByShopName(String shopName, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String, Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("shopName", shopName);

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainShopProperties/getSupplychainShopPropertiesByShopName", headers, parameters);
            return JSONObject.parseArray(response.getString("response"), SupplychainShopProperties.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

    public static SupplychainShopProperties getSupplychainShopPropertiesByUid(String userId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        parameters.put("userId", userId);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainShopProperties/getSupplychainShopPropertiesByUid", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), SupplychainShopProperties.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

    //通过用户ID添加打印机编码
    public static SupplychainShopProperties UpdateBindPrinterCodeByUserId(String bindPrinterCode,String userId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        parameters.put("bindPrinterCode", bindPrinterCode);
        parameters.put("userId", userId);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainShopProperties/UpdateBindPrinterCodeByUserId", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), SupplychainShopProperties.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

}
