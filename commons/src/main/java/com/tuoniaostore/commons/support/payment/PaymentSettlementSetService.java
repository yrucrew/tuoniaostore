package com.tuoniaostore.commons.support.payment;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.commons.support.good.GoodTypeRemoteService;
import com.tuoniaostore.datamodel.payment.PaymentSettlementSet;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;

/**
 * @author sqd
 * @date 2019/6/13
 * @param
 * @return
 */
public class PaymentSettlementSetService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(GoodTypeRemoteService.class);

    public static PaymentSettlementSet getPaymentSettlementSetByUserId(String id){
        Map<String,Object> parameters = new HashMap<>();
        Map<String,Object> packageParams = new HashMap<>();
        parameters.put("id",id);
        Object json = JSONObject.toJSONString(packageParams);
        String tokenForInterface = TokenUtils.createTokenForInterface();
        parameters.put("json", json);
        parameters.put("token",tokenForInterface);
        Map<String,Object> headers = new HashMap();
        try {
            JSONObject response = baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentSettlementSet/getPaymentSettlementSetByUserId", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), PaymentSettlementSet.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }
}
