package com.tuoniaostore.commons.support.supplychain;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.supplychain.PosWork;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import com.tuoniaostore.datamodel.supplychain.SupplychainShopProperties;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sqd
 * @date 2019/5/13
 */
public class PosWorkRemoteService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(PosWorkRemoteService.class);

    /**
     * 添加用户交班记录
     * @author sqd
     * @date 2019/5/13
     * @param posWork, request
     * @return void
     */
    public static void addPosWork(PosWork posWork, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        //将list转成string
        parameters.put("posWork",JSONObject.toJSONString(posWork));

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "posWork/addPosWorkModel", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询用户值班
     * @author sqd
     * @date 2019/5/22
     * @param
     * @return void
     */
    public static PosWork getPosWorkByUserId(String userId,String shopId,String workStatus, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("userId",userId);
        parameters.put("shopId",shopId);
        parameters.put("workStatus",workStatus);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "posWork/getPosWorkByUserId", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), PosWork.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }
}
