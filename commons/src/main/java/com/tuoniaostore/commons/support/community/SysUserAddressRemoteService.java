package com.tuoniaostore.commons.support.community;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.user.SysUserAddress;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author oy
 * @description 用户地址远端调用
 * @date 2019/4/12
 */
public class SysUserAddressRemoteService  extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(AreaRemoteService.class);

    public static SysUserAddress getDefaultSysUserAddress(String userId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("userId", userId);

        parameters.put("sign", signature);
        Map<String,Object>  headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUserAddress/getDefaultSysUserAddress", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.error(response.getString("response"));
        return JSONObject.parseObject(response.getString("response"), SysUserAddress.class);
    }

    public static SysUserAddress getSysUserAddressById(String id, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("id", id);

        parameters.put("sign", signature);
        Map<String,Object>  headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUserAddress/getSysUserAddress", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), SysUserAddress.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.error(response.getString("response"));
        return null;
    }
}
