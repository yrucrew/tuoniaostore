package com.tuoniaostore.commons.support.good;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.vo.supplychain.SupplychainGoodAndShopVO;
import com.tuoniaostore.datamodel.good.GoodShopCart;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 远程添加购物车商品
 * @author sqd
 * @date 2019/4/5
 * @param
 * @return
 */
public class GoodShopCartRemoteService extends BasicRemoteService {


    private static final Logger logger = Logger.getLogger(GoodTypeRemoteService.class);

    /**
     * 商家端：再次购买，商品添加购物车
     * @author sqd
     * @date 2019/4/8
     * @param
     * @return void
     */
    public static void addgoodShopCart(List<OrderGoodSnapshot> list, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        String s = JSONObject.toJSON(list).toString();
        parameters.put("list",s);
        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodShopCart/addgoodShopCart", headers, parameters);
    }

    /**
     * 商家端：查询购物车商品，和该商品数量
     * @author sqd
     * @date 2019/4/8
     * @param
     * @return
     */
    public static  List<SupplychainGoodAndShopVO> getSupplychainGoodByGoodShopCartList(List<GoodShopCart> list,HttpServletRequest request)throws Exception{
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        Map<String,Object> headers = new HashMap();
        HashMap<String, Object> map = new HashMap<>(); //数量
        HashMap<String, Object> mapTime = new HashMap<>(); //时间
        String ids="";
        for (GoodShopCart goodShopCart : list) {
            ids+=goodShopCart.getGoodId()+",";
            map.put(goodShopCart.getGoodId(),goodShopCart.getGoodNumber());
            mapTime.put(goodShopCart.getGoodId(),goodShopCart.getCreateTime());
        }
        String pageIndex = request.getParameter("pageIndex");
        String pageSize = request.getParameter("pageSize");
        parameters.put("ids",ids);
        parameters.put("sign", signature);

        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        parameters.put("ids",ids);
        parameters.put("pageIndex",pageIndex);
        parameters.put("pageSize",pageSize);
        JSONObject jsonObject = new JSONObject(map);
        String jsonString = jsonObject.toJSONString();
        parameters.put("jsonString",jsonString);

        String s = JSONObject.toJSONString(mapTime);
        parameters.put("mapTime",s);

        JSONObject response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGood/getSupplychainGoodByIdAndShopNumber", headers, parameters);

        List<SupplychainGoodAndShopVO> supplychainGoodAndShopVO = null;
        if (response.getInteger("code") == 0) {
            supplychainGoodAndShopVO = JSONObject.parseArray(response.getString("response"), SupplychainGoodAndShopVO.class);
        }
        return supplychainGoodAndShopVO;
    }


    /**
     * 商家端：查询购物车商品
     * @author sqd
     * @date 2019/4/8
     * @param
     * @return
     */
    public static  List<SupplychainGoodAndShopVO> getSupplychainGoodByUserId(String ids,HttpServletRequest request)throws Exception{
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        Map<String,Object> headers = new HashMap();
        parameters.put("ids",ids);
        parameters.put("sign", signature);

        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGood/getSupplychainGoodById", headers, parameters);
            return JSONObject.parseArray(response.getString("response"), SupplychainGoodAndShopVO.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

    /**
     * 商家端：下单远程查询购物车商品
     * @author sqd
     * @date 2019/4/8
     * @param
     * @return
     */
    public static  List<SupplychainGoodAndShopVO>  getSupplychainGoodByUserIdOrder(String ids,HttpServletRequest request)throws Exception{
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        Map<String,Object> headers = new HashMap();
        parameters.put("sign", signature);

        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        parameters.put("ids",ids);
        JSONObject response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGood/getSupplychainGoodByIdOrder", headers, parameters);
        return JSONObject.parseArray(response.getString("response"), SupplychainGoodAndShopVO.class);
    }


    /**
     * 商家端：查询购物车商品
     * @author sqd
     * @date 2019/4/8
     * @param
     * @return
     */
    public static  List<SupplychainGoodAndShopVO> getSupplychainGoodByGoodId(String ids,HttpServletRequest request)throws Exception{
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        Map<String,Object> headers = new HashMap();
        HashMap<String, Object> map = new HashMap<>();

        parameters.put("ids",ids);
        parameters.put("sign", signature);

        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGood/getSupplychainGoodById", headers, parameters);
        return JSONObject.parseArray(response.getString("response"), SupplychainGoodAndShopVO.class);
    }
    /**
     * 远程查询购物车商品
     * @author sqd
     * @date 2019/4/11
     * @param
     * @return
     */
    public static GoodShopCart getGoodShopCartById(String userId,String goodId, HttpServletRequest request)throws Exception{
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        Map<String,Object> headers = new HashMap();
        parameters.put("goodId",goodId);
        parameters.put("userId",userId);
        parameters.put("sign", signature);

        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        try {
            JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodShopCart/getGoodShopCartByUserIdAndGoodId", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), GoodShopCart.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }
    /**
     * 远程修改购物车
     * @author sqd
     * @date 2019/4/25
     * @param
     * @return void
     */
    public static void changeGoodShopCartStauts(String goodId,String status, HttpServletRequest request)throws Exception{
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        Map<String,Object> headers = new HashMap();
        parameters.put("goodId",goodId);
        parameters.put("status",status);
        parameters.put("sign", signature);

        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodShopCart/changeGoodShopCartStauts", headers, parameters);
    }

    /***
     * 通过用户获取当前的购物车列表
     * @author oy
     * @date 2019/5/29
     * @param userId, request
     * @return java.util.List<com.tuoniaostore.datamodel.good.GoodShopCart>
     */
    public static List<GoodShopCart> getGoodShopCartListByUserId(String userId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        //将list转成string
        parameters.put("userId", userId);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodShopCart/getGoodShopCartListByUserId", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), GoodShopCart.class);
    }
}
