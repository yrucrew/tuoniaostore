package com.tuoniaostore.commons.support.good;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.good.GoodBrand;
import com.tuoniaostore.datamodel.good.GoodPrice;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liyunbiao on 2019/3/14.
 */
public class GoodBrandRemoteService extends BasicRemoteService {


    private static final Logger logger = Logger.getLogger(GoodBrandRemoteService.class);
    public static List<GoodBrand> loadGoodBrands(String accessToken,String signature,String appKey)
    {
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        parameters.put("sign", signature);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBrand/getGoodBrandAll", headers, parameters);
            if(response.getInteger("code")==0)
            return JSONObject.parseArray(response.getString("response"), GoodBrand.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static GoodBrand getGoodBrand(String id,String accessToken,String signature,String appKey) {
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        parameters.put("sign", signature);
        headers.put("appKey", appKey);
        parameters.put("id", id);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBrand/getGoodBrand", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseObject(response.getString("response"), GoodBrand.class);
    }
    /**
     * 根据priceId 查找对应的条形码集合
     * @param brandIds
     * @param request
     * @return
     * @throws Exception
     */
    public static List<GoodBrand> getBarcodesByBrandIds(List<String> brandIds, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        String s = JSONObject.toJSONString(brandIds);
        parameters.put("brandIds", s);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBrand/getBarcodesByBrandIds", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), GoodBrand.class);
    }

    /**
     * 根据priceId 和品牌名称 模糊查询 查找对应的条形码集合
     * @param brandIds
     * @param request
     * @return
     * @throws Exception
     */
    public static List<GoodBrand> getBarcodesByBrandIdsAndNameLike(List<String> brandIds,String brandName, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        String s = JSONObject.toJSONString(brandIds);
        parameters.put("brandIds", s);
        parameters.put("brandName", brandName);
        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBrand/getBarcodesByBrandIds", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), GoodBrand.class);
    }


	public static GoodBrand getGoodBrand(String id, HttpServletRequest request) {
		String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("id", id);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap<>();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBrand/getGoodBrand", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return JSONObject.parseObject(response.getString("response"), GoodBrand.class);
	}

}
