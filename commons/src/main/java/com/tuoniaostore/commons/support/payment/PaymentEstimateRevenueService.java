package com.tuoniaostore.commons.support.payment;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.commons.support.good.GoodTypeRemoteService;
import com.tuoniaostore.datamodel.payment.PaymentEstimateRevenue;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


public class PaymentEstimateRevenueService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(PaymentEstimateRevenueService.class);

    /**
     * 远程支付记录
     * @author sqd
     * @date 2019/4/24
     * @param
     * @return
     */
    public static void addPaymentEstimateRevenue(PaymentEstimateRevenue paymentEstimateRevenue, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("sign", signature);
        String string = JSONObject.toJSONString(paymentEstimateRevenue);
        parameters.put("paymentEstimateRevenue", string);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        String tokenForInterface = TokenUtils.createTokenForInterface();
        parameters.put("token", tokenForInterface);

        try {
            baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentEstimateRevenue/addPaymentEstimateRevenueJson", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 远程支付记录
     * @author sqd
     * @date 2019/4/24
     * @param
     * @return
     */
    public static void addRemotePaymentEstimateRevenue(PaymentEstimateRevenue paymentEstimateRevenue){
        Map<String,Object> parameters = new HashMap<>();
        Map<String,Object> packageParams = new HashMap<>();
        String string = JSONObject.toJSONString(paymentEstimateRevenue);
        parameters.put("paymentEstimateRevenue",string);
        Object json = JSONObject.toJSONString(packageParams);
        String tokenForInterface = TokenUtils.createTokenForInterface();
        parameters.put("json", json);
        parameters.put("token",tokenForInterface);
        Map<String,Object> headers = new HashMap();

        try {
            baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentEstimateRevenue/addPaymentEstimateRevenueJson", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 远程添加支付记录
     * @author sqd
     * @date 2019/4/26
     * @param
     * @return
     */
    public static void changePaymentEstimateRevenue(PaymentEstimateRevenue paymentEstimateRevenue, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("sign", signature);
        String string = JSONObject.toJSONString(paymentEstimateRevenue);
        parameters.put("paymentEstimateRevenue", string);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentEstimateRevenue/changePaymentEstimateRevenue", headers, parameters);
    }

}
