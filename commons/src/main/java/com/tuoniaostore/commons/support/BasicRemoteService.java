package com.tuoniaostore.commons.support;


import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.BasicWebService;
import com.tuoniaostore.commons.http.HttpRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by liyunbiao on 2018/8/19.
 */
public class BasicRemoteService {

    protected static Map<String, String> initialParameter(String partnerId, String appId) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("partnerId", partnerId);
        parameters.put("appId", appId);
        return parameters;
    }

    protected static JSONObject baseHttpPost(String url, Map parameters) {
        String data = HttpRequest.post(url, parameters);
        JSONObject response = JSONObject.parseObject(data);
        return response;
    }

    protected static JSONObject baseHttpGet(String url, Map headers, Map parameters) throws Exception {
        String data = HttpRequest.doGet(url, headers, parameters);
        JSONObject response = JSONObject.parseObject(data);
        return response;
    }

    protected static JSONObject baseHttpPost(String url, Map headers, Map parameters) throws Exception {
        String data = HttpRequest.doPost(url, headers, parameters);
        JSONObject response = JSONObject.parseObject(data);
        return response;
    }

    /**
     * 初始化Header
     */
    protected static Map<String,Object> initHeaders(HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        Map<String,Object> headers = new HashMap<>();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        return headers;
    }

    /**
     * 初始化Parameters
     */
    protected static Map<String,Object> initParameters(HttpServletRequest request) {
        Map<String,Object> parameters = new HashMap<>();
        String signature = request.getParameter("sign");
        parameters.put("sign", signature);
        return parameters;
    }
}
