package com.tuoniaostore.commons.support.good;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.good.GoodType;
import com.tuoniaostore.datamodel.good.GoodUnit;
import com.tuoniaostore.datamodel.vo.good.GoodTypeOne;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by liyunbiao on 2019/3/14.
 */
public class GoodTypeRemoteService extends BasicRemoteService {

    private static final Logger logger = LoggerFactory.getLogger(GoodTypeRemoteService.class);

    /**
     * 根据类别id 查找对应的实体
     *
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    public static GoodType getGoodType(String id, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodType", headers, parameters);
        GoodType goodType = null;
        if (response.getInteger("code") == 0) {
            goodType = JSONObject.parseObject(response.getString("response"), GoodType.class);
        }
        return goodType;
    }

    public static GoodType getGoodType(String id, String accessToken, String signature, String appKey) {

        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        parameters.put("sign", signature);
        headers.put("appKey", appKey);
        parameters.put("id", id);
        GoodType goodType = null;
        try {
            JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodType", headers, parameters);
            goodType = null;
            if (response != null) {
                if (response.getInteger("code") == 0)
                    goodType = JSONObject.parseObject(response.getString("response"), GoodType.class);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return goodType;
    }

    public static List<GoodType> loadGoodTypes(String accessToken, String signature, String appKey) {
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        parameters.put("sign", signature);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypeAll", headers, parameters);
            if (response.getInteger("code") == 0)
                return JSONObject.parseArray(response.getString("response"), GoodType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据商品类别id 去查找对应的商品类别
     *
     * @param ids
     * @param request
     * @return
     * @throws Exception
     */
    public static List<GoodType> getGoodTypes(List<String> ids, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        //处理一下这个list 转成string
        String s = JSONObject.toJSONString(ids);
        parameters.put("ids", s);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypesById2", headers, parameters);
        //解析一下 返回的字段
        List<GoodType> list1 = null;
        if (response.getInteger("code") == 0) {
            String list = response.getString("list");
            list1 = JSONObject.parseArray(list, GoodType.class);
        }
        return list1;
    }

    public static List<GoodType> getGoodTypeAll(HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        //处理一下这个list 转成string
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypeAll", headers, parameters);
        //解析一下 返回的字段
        List<GoodType> list1 = null;
        if (response.getInteger("code") == 0) {
            String list = response.getString("response");
            list1 = JSONObject.parseArray(list, GoodType.class);
        }
        return list1;
    }

    /**
     * 根据商品类别id 去查找对应的商品类别
     *
     * @param request
     * @return
     * @throws Exception
     */
    public static List<GoodType> getGoodTypeParent(String id, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        //处理一下这个list 转成string
        parameters.put("id", id);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypeParent", headers, parameters);
        //解析一下 返回的字段
        if (response.getInteger("code") == 0) {
            String list = response.getString("list");
            return JSONObject.parseArray(list, GoodType.class);
        }
        return null;
    }


    /**
     * 根据商品类别id 去查找对应的商品类别 (分页的)
     *
     * @param ids
     * @param request
     * @return
     * @throws Exception
     */
    public static List<GoodType> getGoodTypesByPage(List<String> ids, int pageLimit, int pageSize, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        //处理一下这个list 转成string
        String collect = ids.stream().collect(Collectors.joining(","));//将集合转换成string “,” 隔开
        parameters.put("ids", collect);
        parameters.put("limit", pageLimit + "");
        parameters.put("page", pageSize + "");
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypesByPage", headers, parameters);
        //解析一下 返回的字段
        if (response.getInteger("code") == 0) {
            String list = response.getString("list");
            List<GoodType> list1 = JSONObject.parseArray(list, GoodType.class);
            return list1;
        }
        return null;
    }

    public static List<GoodType> getGoodTypeAll(List<String> ids, int pageLimit, int pageSize, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        //处理一下这个list 转成string
        String collect = ids.stream().collect(Collectors.joining(","));//将集合转换成string “,” 隔开
        parameters.put("ids", collect);
        parameters.put("limit", pageLimit + "");
        parameters.put("page", pageSize + "");
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypeAll", headers, parameters);
            if (response.getInteger("code") == 0) {
                return JSONObject.parseArray(response.getString("response"), GoodType.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //解析一下 返回的字段
        return null;
    }


    /**
     * 根据商品类别id 去查找对应的商品类别或者下一级的类别
     *
     * @param ids
     * @param request
     * @return
     * @throws Exception
     */
    public static List<GoodTypeOne> getGoodTypesAndSecondTypeByTypeId(List<String> ids, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        //处理一下这个list 转成string
        String s = JSONObject.toJSONString(ids);
        parameters.put("ids", s);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypesAndSecondTypeByTypeId", headers, parameters);
            if (response.getInteger("code") == 0) {
                return JSONObject.parseArray(response.getString("list"), GoodTypeOne.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("远程连接错误！");
        }
        //解析一下 返回的字段
        return null;
    }

    /**
     * 根据商品类别id 去查找对应的二级分类
     *
     * @param
     * @param request
     * @return
     * @throws Exception
     */
    public static List<GoodType> getGoodSecondTypeByTypeId(String id, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        //处理一下这个list 转成string
        parameters.put("firstId", id);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodSecondTypeByTypeId", headers, parameters);
            if (response.getInteger("code") == 0) {
                return JSONObject.parseArray(response.getString("list"), GoodType.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("远程连接错误！");
        }
        //解析一下 返回的字段
        return null;
    }


    /**
     * 添加商品类别实体
     *
     * @param goodType
     * @param request
     * @return
     * @throws Exception
     */
    public static void addGoodTypes(GoodType goodType, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        parameters.put("id", goodType.getId());
        parameters.put("title", goodType.getTitle());
        parameters.put("parentId", "afdd09ad5d664557b485f58f48572e32ZnfsvX");
        parameters.put("status", goodType.getStatus() + "");
        parameters.put("type", goodType.getType() + "");
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/addGoodType", headers, parameters);
    }

    /**
     * 根据商品类别名称获取类别信息
     *
     * @param typeName, request
     * @return com.tuoniaostore.datamodel.good.GoodType
     * @author oy
     * @date 2019/4/9
     */
    public static GoodType getGoodTypeByTitle(String typeName, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        parameters.put("title", typeName);

        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypeByTitle", headers, parameters);
            if (response.getInteger("code") == 0) {
                return JSONObject.parseObject(response.getString("response"), GoodType.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public static List<GoodType> getGoodTypesNameLike(String typeName, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("typeName", typeName);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypesNameLike", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //解析一下 返回的字段
        if (response.getInteger("code") == 0) {
            return JSONObject.parseArray(response.getString("list"), GoodType.class);
        }
        return null;
    }


    public static List<GoodType> getGoodTypesAndNameLike(List<String> ids, String typeName, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        String s = JSONObject.toJSONString(ids);
        parameters.put("typeIds", s);
        parameters.put("typeName", typeName);

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypesAndNameLike", headers, parameters);
            if (response.getInteger("code") == 0)
                return JSONObject.parseArray(response.getString("list"), GoodType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //解析一下 返回的字段

        return null;
    }

    /**
     * 查询所有的一级分类
     *
     * @param request
     * @return java.util.List<com.tuoniaostore.datamodel.good.GoodType>
     * @author oy
     * @date 2019/5/18
     */
    public static List<GoodType> getGoodTypeOne(HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypeOne", headers, parameters);
            if (response.getInteger("code") == 0)
                return JSONObject.parseArray(response.getString("list"), GoodType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //解析一下 返回的字段

        return null;
    }

    /**
     * 获取一级分类中的
     *
     * @param typeName1, httpServletRequest
     * @return java.util.List<com.tuoniaostore.datamodel.good.GoodType>
     * @author oy
     * @date 2019/5/20
     */
    public static List<GoodType> getGoodTypesOneNameLike(String typeName1, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("typeName", typeName1);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypesOneNameLike", headers, parameters);
            if (response.getInteger("code") == 0)
                return JSONObject.parseArray(response.getString("list"), GoodType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //解析一下 返回的字段
        return null;
    }

    /**
     * 查询分类
     *
     * @param
     * @return java.util.List<com.tuoniaostore.datamodel.good.GoodType>
     * @author sqd
     * @date 2019/5/25
     */
    public static List<GoodTypeOne> getGoodTypeOneTwoLeave(HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypeOneTwoLeave", headers, parameters);
            if (response.getInteger("code") == 0)
                return JSONObject.parseArray(response.getString("list"), GoodTypeOne.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //解析一下 返回的字段

        return null;
    }

    /**
     * 查找对应类的一级分类信息
     *
     * @param
     * @return java.util.List<com.tuoniaostore.datamodel.good.GoodType>
     * @author sqd
     * @date 2019/5/25
     */
    public static GoodType getGoodTypeOneByTwoTypeId(String typeTwoId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        parameters.put("typeTwoId", typeTwoId);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodType/getGoodTypeOneByTwoTypeId", headers, parameters);
            if (response.getInteger("code") == 0)
                return JSONObject.parseObject(response.getString("response"), GoodType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //解析一下 返回的字段
        return null;
    }


}
