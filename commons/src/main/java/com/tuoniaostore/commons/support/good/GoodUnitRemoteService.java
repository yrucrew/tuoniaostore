package com.tuoniaostore.commons.support.good;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.good.GoodBarcode;
import com.tuoniaostore.datamodel.good.GoodUnit;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 条形码 远程调用服务
 *
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/28
 */
public class GoodUnitRemoteService extends BasicRemoteService {


    private static final Logger logger = Logger.getLogger(GoodTypeRemoteService.class);

    /**
     * title 查找对应的条形码
     *
     * @param title
     * @param request
     * @return
     * @throws Exception
     */
    public static GoodUnit getGoodUnitByTitle(String title, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("title", title);

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodUnit/getGoodUnitByTitle", headers, parameters);
        return JSONObject.parseObject(response.getString("response"), GoodUnit.class);
    }

    public static List<GoodUnit> loadGoodsUnits(String accessToken, String appKey, String signature) {
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        parameters.put("sign", signature);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodUnit/getGoodUnitAll", headers, parameters);
            if(response.getInteger("code")==0)
            return JSONObject.parseArray(response.getString("response"), GoodUnit.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static GoodUnit getGoodUnitById(String id,String accessToken,String signature,String appKey) {
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        parameters.put("sign", signature);
        headers.put("appKey", appKey);
        parameters.put("id", id);


        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodUnit/getGoodUnit", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return JSONObject.parseObject(response.getString("response"), GoodUnit.class);
    }


    /**
     * id 查找对应的条形码
     *
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    public static GoodUnit getGoodUnitById(String id, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodUnit/getGoodUnit", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return JSONObject.parseObject(response.getString("response"), GoodUnit.class);
    }

    /**
     * 添加单位
     *
     * @param goodUnit1, httpServletRequest
     * @return void
     * @author oy
     * @date 2019/4/8
     */
    public static GoodUnit addGoodUnit(GoodUnit goodUnit1, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", goodUnit1.getId());
        parameters.put("title", goodUnit1.getTitle());
        parameters.put("userId", goodUnit1.getUserId());

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodUnit/addGoodUnit", headers, parameters);
        return JSONObject.parseObject(response.getString("response"), GoodUnit.class);
    }
}
