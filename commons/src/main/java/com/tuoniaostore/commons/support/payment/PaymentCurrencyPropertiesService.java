package com.tuoniaostore.commons.support.payment;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.payment.PaymentCurrencyProperties;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author sqd
 * @date 2019/4/28
 * @param
 * @return
 */
public class PaymentCurrencyPropertiesService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(PaymentCurrencyPropertiesService.class);

    /**
     * 获取用户支付密码
     * @author sqd
     * @date 2019/4/28
     * @param
     * @return void
     */
    public static PaymentCurrencyProperties getPaymentPasswordByUserId(HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");
        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.paymentRemoteURL + "paymentCurrencyProperties/getPaymentPasswordByUserId", headers, parameters);
        return JSONObject.parseObject(response.getString("response"), PaymentCurrencyProperties.class);
    }

}
