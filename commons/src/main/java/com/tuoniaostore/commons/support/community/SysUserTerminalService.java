package com.tuoniaostore.commons.support.community;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.commons.support.good.GoodTypeRemoteService;
import com.tuoniaostore.datamodel.user.SysUserTerminal;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/5/10
 */
public class SysUserTerminalService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(GoodTypeRemoteService.class);

    /**
     * 添加用户
     * @author oy
     * @date 2019/5/10
     * @param sysUserTerminal, request
     * @return void
     */
    public static void addSysUserTerminal(SysUserTerminal sysUserTerminal, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");
        Map<String,Object> parameters = new HashMap<>();
        String json = JSON.toJSONString(sysUserTerminal);
        parameters.put("sign", signature);
        parameters.put("userTerminalEntity", json);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUserTerminal/addSysUserTerminalByRemote", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
