package com.tuoniaostore.commons.support.supplychain;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodBindRelationship;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/4/23
 */
public class SupplychainGoodBindRelationshipRemoteService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(SupplychainGoodBindRelationship.class);

    public static List<SupplychainGoodBindRelationship> getSupplychainGoodBindRelationshipByUserId(String userId, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        //将list转成string
        parameters.put("userId", userId);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGoodBindRelationship/getSupplychainGoodBindRelationshipByUserIdForRemote", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), SupplychainGoodBindRelationship.class);

    }

}
