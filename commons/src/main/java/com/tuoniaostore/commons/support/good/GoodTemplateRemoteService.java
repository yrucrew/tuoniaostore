package com.tuoniaostore.commons.support.good;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.good.GoodTemplate;
import com.tuoniaostore.datamodel.good.GoodType;
import com.tuoniaostore.datamodel.vo.good.GoodPriceCombinationVO;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/14
 */
public class GoodTemplateRemoteService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(GoodTypeRemoteService.class);

    /**
     * 根据id  查找当前商品模板id 远程调用good模块
     *
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    public static GoodTemplate getGoodTemplate(String id, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodTemplate/getGoodTemplate", headers, parameters);
        return JSONObject.parseObject(response.getString("response"), GoodTemplate.class);
    }

    public static GoodTemplate getGoodTemplate(String id, String accessToken, String signature, String appKey) {
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        parameters.put("sign", signature);
        headers.put("appKey", appKey);
        parameters.put("id", id);


        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodTemplate/getGoodTemplate", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), GoodTemplate.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<GoodTemplate> loadGoodTemplates(String accessToken, String signature, String appKey) {
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        parameters.put("sign", signature);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodTemplate/getGoodTemplateAll", headers, parameters);
            if (response.getInteger("code") == 0) {
                return JSONObject.parseArray(response.getString("response"), GoodTemplate.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static GoodTemplate addGoodTemplate(GoodTemplate goodTemplate, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", goodTemplate.getTypeId());
        parameters.put("name", goodTemplate.getName());
        parameters.put("typeId", goodTemplate.getTypeId());
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodTemplatePos/addGoodTemplatePos", headers, parameters);
        return JSONObject.parseObject(response.getString("response"), GoodTemplate.class);
    }


    public static GoodTemplate getGoodTemplateByName(String defineName1, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", defineName1);
        parameters.put("sign", signature);

        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodTemplatePos/getGoodTemplatePosByName", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseObject(response.getString("response"), GoodTemplate.class);
    }

    public static List<GoodTemplate> getGoodTemplatesByBrandId(String brandId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("brandId", brandId);

        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodTemplate/getGoodTemplatesByBrandId", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //解析一下 返回的字段
        String list = response.getString("list");
        List<GoodTemplate> list1 = JSONObject.parseArray(list, GoodTemplate.class);
        return list1;
    }

    public static List<GoodTemplate> getGoodTemplateAll(HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        Map<String, Object> headers = new HashMap<>();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodTemplate/getGoodTemplateAll", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("response"), GoodTemplate.class);
    }

    /**
     * 分页
     *
     * @param request
     * @return
     */
    public static Map<String, Object> getGoodTemplates(HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");
        String limit = request.getParameter("limit");
        String page = request.getParameter("page");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        parameters.put("limit", limit);
        parameters.put("page", page);
        Map<String, Object> headers = new HashMap<>();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPrice/showAllGoodPriceis2", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows", JSONObject.parseArray(response.getString("rows"), GoodPriceCombinationVO.class));
        map.put("total", response.getIntValue("total"));
        return map;
    }


}
