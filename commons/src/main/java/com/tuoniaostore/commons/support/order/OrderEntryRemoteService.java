package com.tuoniaostore.commons.support.order;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.order.OrderEntry;
import com.tuoniaostore.datamodel.order.OrderGoodSnapshot;
import com.tuoniaostore.datamodel.vo.order.OrderPayTypeSumAndRefundStatisticVo;
import com.tuoniaostore.datamodel.vo.order.OrderPayTypeSumVo;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/4/24
 */
public class OrderEntryRemoteService extends BasicRemoteService {


    private static final Logger logger = Logger.getLogger(OrderEntryRemoteService.class);

    public static List<OrderEntry> getOrderEntryByOrderNumber(String orderNumber, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("orderNumber", orderNumber);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntry/getOrderEntryByOrdenumber", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return JSONObject.parseArray(response.getString("response"), OrderEntry.class);
    }
    /**
     * 获取退款金额
     * @author sqd
     * @date 2019/6/15
     * @return java.util.List<com.tuoniaostore.datamodel.order.OrderEntry>
     */
    public static List<OrderPayTypeSumAndRefundStatisticVo> getRefundOrder(String loginTime,String logoutTime,String shopId, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("loginTime", loginTime);
        parameters.put("logoutTime", logoutTime);
        parameters.put("shopId", shopId);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntry/getRefundOrder", headers, parameters);
            return JSONObject.parseArray(response.getString("response"), OrderPayTypeSumAndRefundStatisticVo.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
            return  null;
    }

    public static List<OrderEntry> getOrderEntryPosByOrdenumber(String orderNumber, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("orderNumber", orderNumber);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntry/getOrderEntryPosByOrdenumber", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String response1 = response.getString("response");
        if(response1 != null && !response1.equals("")){
            return JSONObject.parseArray(response1, OrderEntry.class);
        }
        return null;
    }
    public static List<OrderEntry> inOrderNumber(String orderNumbersStr, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("orderNumbersStr", orderNumbersStr);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntry/inOrderNumber", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("response"), OrderEntry.class);
    }

    public static List<OrderPayTypeSumVo> getOrderEntryGruopByPayTtpe(String loginTime, String logoutTime, String saleUserId, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("loginTime", loginTime);
        parameters.put("logoutTime", logoutTime);
        parameters.put("saleUserId", saleUserId);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntry/getOrderEntryGruopByPayTtpe", headers, parameters);
            return JSONObject.parseArray(response.getString("response"), OrderPayTypeSumVo.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static List<OrderEntry> getOrderEntryReturn(String loginTime, String logoutTime, String saleUserId, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("loginTime", loginTime);
        parameters.put("logoutTime", logoutTime);
        parameters.put("saleUserId", saleUserId);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntry/getOrderEntryReturn", headers, parameters);
            return JSONObject.parseArray(response.getString("response"), OrderEntry.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void payCallback(String orderNumber,String actualPrice, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("orderNumber", orderNumber);
        parameters.put("actualPrice", actualPrice);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntry/payCallback", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void zfbPosPayCallback(String orderNumber,String totalPrice, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("orderNumber", orderNumber);
        parameters.put("totalPrice", totalPrice+"");

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            logger.info("进入公共回调========================="+CommonsUrlConfig.orderRemoteURL + "orderEntry/zfbPosPayCallback");
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntry/zfbPosPayCallback", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static OrderEntry getOrderEntryAndPosById(String orderId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("orderId", orderId);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntry/getOrderEntryAndPosById", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), OrderEntry.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<OrderEntry> getOrderEntryByOrderNumberAll(String orderNum, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("orderNum", orderNum);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.orderRemoteURL + "orderEntry/getOrderEntryByOrderNumberAll", headers, parameters);
            return JSONObject.parseArray(response.getString("response"), OrderEntry.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
