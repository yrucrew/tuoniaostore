package com.tuoniaostore.commons.support.good;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/4/15
 */
public class GoodBrandTemplateRemoteService  extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(GoodBrandRemoteService.class);

    /**
     * 连表查询 通过品牌名称
     * @author oy
     * @date 2019/4/15
     * @param goodNameOrBrandName, httpServletRequest
     * @return java.util.List<java.lang.String>
     */
    public static List<String> getBarcodesByBrandName(String goodNameOrBrandName, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("goodBrandName", goodNameOrBrandName);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodBrand/getBarcodesByBrandName", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), String.class);
    }
}
