package com.tuoniaostore.commons.support.community;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.nimbusds.jose.JOSEException;
import com.tuoniaostore.commons.CryptUtils;
import com.tuoniaostore.commons.MD5;
import com.tuoniaostore.commons.RSA;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.exception.Nearby123shopRuntimeException;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.user.SysUser;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liyunbiao on 2019/3/7.
 */
public class SysUserRmoteService extends BasicRemoteService {
    private static final Logger logger = Logger.getLogger(SysUserRmoteService.class);

    public static int signatureSecurity(HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        if (accessToken == null || accessToken.equals("")) {
            return 1001;// throw new Nearby123shopRuntimeException("accessToken无效");
        }
        if (appKey == null || appKey.equals("")) {
            return 1001;//throw new Nearby123shopRuntimeException("appKey无效");
        }

        Map<String, Object> validMap = TokenUtils.valid(accessToken);
        int i = (int) validMap.get("Result");
        if (i == 0) {
            net.minidev.json.JSONObject jsonObject = (net.minidev.json.JSONObject) validMap.get("data");
            if (jsonObject.get("uid") != null) {
                int count = Math.round((System.currentTimeMillis() - Long.parseLong(jsonObject.get("sta").toString())) / 1000 / 60) + 1;
                for (int k = 0; k < 3; k++) {
                    if (signature.equals(MD5.encode(accessToken + "&" + (count + k) + "&" + appKey))) {
                        return 0;
                    }
                }
                for (int k = -3; k < 0; k++) {
                    String s = null;
                    s = CryptUtils.GetMD5Code(accessToken + "&" + (count + k) + "&" + appKey);
                    if (signature.equals(s)) {
                        return 0;
                    }
                }

            }
        } else if (i == 2) {
            System.out.println("token已经过期");
            return 1001;
        }
        return -1;
    }

    public static SysUser getSysUser(HttpServletRequest request)  {
         SysUser sysUser = null;
       /* SysUser sysUser = new SysUser();
        sysUser.setId("c135b6e096814ad59e1413a21dd93c01HJOeOS");
        //sysUser.setId("95e581d76a04489fb55cb76b24dd4fc8OACAto");
        //sysUser.setPhoneNumber("18972180648");*/
       try {
            String accessToken = request.getHeader("accessToken");
            String appKey = request.getHeader("appKey");
            sysUser = new SysUser();
            Map<String, Object> validMap = TokenUtils.valid(accessToken);
            int i = (int) validMap.get("Result");
            if (i == 0) {
                net.minidev.json.JSONObject jsonObject = (net.minidev.json.JSONObject) validMap.get("data");
                if (jsonObject.get("uid") != null) {
                    JSONObject sysUserPartnerApplication = getAppKey(appKey);
                    if (sysUserPartnerApplication == null) {
                        throw new Nearby123shopRuntimeException("appKey不存在!!");
                    }
                    String str = RSA.decrypt(jsonObject.get("uid").toString(), sysUserPartnerApplication.getString("accessKeyModulus"), sysUserPartnerApplication.getString("accessKeyExponent"));

                    JSONObject json = JSON.parseObject(str);
                    if(json!=null) {
                        sysUser.setId(json.getString("id"));
//                        sysUser.setId("334940f42a1f4aa4b3cdd5510d6562e5Wxnzhq");
                        sysUser.setUserId(sysUser.getId());
                        sysUser.setDefaultName(json.getString("defaultName"));
                        sysUser.setPhoneNumber(json.getString("phoneNumber"));
                        sysUser.setRealName(json.getString("realName"));
                        sysUser.setRoleId(json.getString("roleId"));
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (JOSEException e) {
            e.printStackTrace();
        } catch (Nearby123shopRuntimeException e) {
            e.printStackTrace();
        }
        return sysUser;
    }

    public static JSONObject getAppKey(String appKey) {
        Map parameters = new HashMap<>();
        parameters.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUserPartnerApplication/getSysUserPartnerApplicationAppKey", parameters);

        JSONObject appkey = response.getJSONObject("response");
        return appkey;

    }

    public static SysUser getSysUser(String id, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map parameters = new HashMap<>();
        parameters.put("id", id);
        parameters.put("sign", signature);
        Map headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUser/getSysUser", headers, parameters);
            if (response == null) {
                return null;
            }
            return JSONObject.parseObject(response.getString("response"), SysUser.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public static List<SysUser> getSysUserAccountOrName(String accountOrName, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map parameters = new HashMap<>();
        parameters.put("accountOrName", accountOrName);
        parameters.put("sign", signature);
        Map headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUser/getSysUserAccountOrName", headers, parameters);
            if (response == null) {
                return null;
            }
            return JSONObject.parseArray(response.getString("response"), SysUser.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
