package com.tuoniaostore.commons.support.good;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.TokenUtils;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.good.GoodBarcode;
import com.tuoniaostore.datamodel.good.GoodPrice;
import com.tuoniaostore.datamodel.good.GoodTemplate;
import com.tuoniaostore.datamodel.good.GoodUnit;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 条形码 远程调用服务
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/28
 */
public class GoodPriceRemoteService extends BasicRemoteService {


    private static final Logger logger = Logger.getLogger(GoodTypeRemoteService.class);
    public static List<GoodPrice> loadGoodPrices(String accessToken,String signature,String appKey)
    {
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        parameters.put("sign", signature);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPrice/getGoodPriceAll", headers, parameters);
            return JSONObject.parseArray(response.getString("response"), GoodPrice.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * id
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    public static GoodPrice getGoodPriceById(String id, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("id", id);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPrice/getGoodPrice", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), GoodPrice.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }
    public static GoodPrice getGoodPrice(String id,String accessToken,String signature,String appKey){
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        parameters.put("sign", signature);
        headers.put("appKey", appKey);
        parameters.put("id", id);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPrice/getGoodPrice", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), GoodPrice.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }
    public static List<GoodPrice> getGoodPriceAll(HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPrice/getGoodPriceAll", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("response"), GoodPrice.class);
    }

    /**
     * 添加价格模板表
     * @author oy
     * @date 2019/4/8
     * @param  goodPrice, request
     * @return com.tuoniaostore.datamodel.good.GoodPrice
     */
    public static GoodPrice addGoodPrice(GoodPrice goodPrice, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("id", goodPrice.getId());
        parameters.put("title", goodPrice.getTitle());
        parameters.put("goodId", goodPrice.getGoodId());
        parameters.put("unitId", goodPrice.getUnitId());
        parameters.put("salePrice", goodPrice.getSalePrice()+"");
        parameters.put("costPrice", goodPrice.getCostPrice()+"");

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPricePos/addGoodPricePos", headers, parameters);
        return JSONObject.parseObject(response.getString("response"), GoodPrice.class);
    }


    /**
     * 更新价格表信息
     * @author oy
     * @date 2019/4/9
     * @param title, priceId1, unitId, templateId, request
     * @return void
     */
    public static void updateGoodPrice(String title, String priceId1, String unitId, String templateId,HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("title", title);
        parameters.put("priceId1", priceId1);
        if(unitId != null && !unitId.equals("")){
            parameters.put("unitId", unitId);
        }
        if(templateId != null && !templateId.equals("")){
            parameters.put("templateId", templateId);
        }
        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        try {
            baseHttpPost(CommonsUrlConfig.goodRemoteURL + "goodPricePos/updateGoodPricePos", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
