package com.tuoniaostore.commons.support.community;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.commons.support.good.GoodTypeRemoteService;
import com.tuoniaostore.datamodel.user.SysUser;
import com.tuoniaostore.datamodel.user.SysUserSmsLogger;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 提供community模块远程调用服务
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/21
 */
public class SysUserSmsRemoteService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(GoodTypeRemoteService.class);

    /*
        远端添加短信记录
     */
    public static void addSysUserSmsLogger(SysUserSmsLogger sysUserSmsLogger, HttpServletRequest request) throws Exception {
        String appKey = request.getHeader("appKey");
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        //具体参数
        parameters.put("code",sysUserSmsLogger.getCode());
        parameters.put("content",sysUserSmsLogger.getContent());
        parameters.put("phone",sysUserSmsLogger.getPhone());
        parameters.put("type",sysUserSmsLogger.getType()+"");

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUserSmsLogger/addSysUserSmsLogger", headers, parameters);
        //return JSONObject.parseObject(response.getString("response"), SysUserSmsLogger.class);
    }

    /*
        远端获取短信记录
     */
    public static SysUserSmsLogger getSysUserSmsLogger(String phone, int type,HttpServletRequest request) throws Exception {
        String appKey = request.getHeader("appKey");
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        //具体参数
        parameters.put("phone",phone);
        parameters.put("type",type+"");
        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUserSmsLogger/getSysUserSmsLoggerByPhone", headers, parameters);
        HashMap response1 = JSONObject.parseObject(response.getString("response"), HashMap.class);
        String json = (String)response1.get("response");
        SysUserSmsLogger sysUserSmsLogger = JSONObject.parseObject(json, SysUserSmsLogger.class);
        return sysUserSmsLogger;
    }

    public static void main(String[] args) {
        SysUserSmsLogger sysUserSmsLogger = new SysUserSmsLogger();
        sysUserSmsLogger.setId("123");
        sysUserSmsLogger.setStatus(1);
        sysUserSmsLogger.setType(1);
        sysUserSmsLogger.setCode("123");
        sysUserSmsLogger.setPhone("123");
        sysUserSmsLogger.setContent("123");
        sysUserSmsLogger.setCreateTime(new Date());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sysUserSmsLogger",JSONObject.toJSONString(sysUserSmsLogger));
        SysUserSmsLogger sysUserSmsLogger1 = JSONObject.parseObject(jsonObject.getString("sysUserSmsLogger"), SysUserSmsLogger.class);
        System.out.println(sysUserSmsLogger1.toString());
    }


}
