package com.tuoniaostore.commons.support.supplychain;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.supplychain.SupplychainGood;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description chain_good远程调用
 * @date 2019/4/16
 */
public class SupplyChainGoodRemoteService  extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(SupplyChainGoodRemoteService.class);

    /**
     * 获取商品 通过商品ids
     * @author oy
     * @date 2019/4/17
     * @param ids, request
     * @return java.util.List<com.tuoniaostore.datamodel.supplychain.SupplychainGood>
     */
    public static List<SupplychainGood> getSupplychainGoodByIds(List<String> ids, HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        //将list转成string
        parameters.put("ids",JSONObject.toJSONString(ids));

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGood/getSupplychainGoodByIds", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(response != null){
            return JSONObject.parseArray(response.getString("list"), SupplychainGood.class);

        }else {
            return null;
        }
    }


    /***
     * 通过商店id 获取一下当前的商品
     * @param shopId
     * @param request
     * @return
     */
    public static List<SupplychainGood> getSupplychainGoodByShopId(String shopId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        //将list转成string
        parameters.put("shopId",shopId);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGood/getSupplychainGoodByShopId", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), SupplychainGood.class);
    }

    /**
     * 更新库存表
     * @author oy
     * @date 2019/4/25
     * @param goodList, httpServletRequest
     * @return void
     */
    public static void updateStockByList(List<SupplychainGood> goodList, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        //将list转成string
        String s = JSONObject.toJSONString(goodList);
        parameters.put("goodList",s);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        try {
            baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGood/updateStockByList", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param id 商品id
     * @param stock 库存
     * @param isAdd 0-减少 1-增加
     * @param request
     */
    public static void changSupplychainGoodByGoodId(String id ,Integer stock,int isAdd, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        parameters.put("id",id);
        parameters.put("stock",stock+"");
        parameters.put("isAdd",isAdd+"");

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        try {
            baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGood/changSupplychainGoodByGoodId", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据对应价格标签 和商店id 获取商店内的商品
     * @author oy
     * @date 2019/5/24
     * @param shopId, priceId, request
     * @return com.tuoniaostore.datamodel.supplychain.SupplychainGood
     */
    public static SupplychainGood getSupplychainGoodByShopIdAndPriceId(String shopId, String priceId, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        //将list转成string
        parameters.put("shopId",shopId);
        parameters.put("priceId",priceId);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGood/getSupplychainGoodByShopIdAndPriceId", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseObject(response.getString("response"), SupplychainGood.class);
    }

    /**
     * 异步更新商店的商品信息
     * @author oy
     * @date 2019/5/27
     * @param map, request
     * @return void
     */
    public static void updateSupplychainGoodStatus(Map<String,Object> map,HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        //将list转成string
        String s = JSONObject.toJSONString(map);
        parameters.put("sign", signature);
        parameters.put("map", s);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGood/updateSupplychainGoodStatus", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     *
     * @author oy
     * @date 2019/6/3
     * @param goodsId, status, request
     * @return com.tuoniaostore.datamodel.supplychain.SupplychainGood
     */
    public static SupplychainGood getSupplychainGoodByIdAndStatus(String goodsId, int status, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        parameters.put("goodsId", goodsId);
        parameters.put("status", status+"");
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGood/getSupplychainGoodByIdAndStatus", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseObject(response.getString("response"), SupplychainGood.class);
    }
}
