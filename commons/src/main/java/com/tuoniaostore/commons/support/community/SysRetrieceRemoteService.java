package com.tuoniaostore.commons.support.community;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.user.SysRetrieve;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description 回收站远端调用
 * @date 2019/5/17
 */
public class SysRetrieceRemoteService extends BasicRemoteService {

    private static final Logger logger = LoggerFactory.getLogger(SysRetrieceRemoteService.class);

    /**
     * 添加到回收站
     * @author oy
     * @date 2019/5/17
     * @param sysRetrieve, request
     * @return void
     */
    public static void addSysRetrieve(SysRetrieve sysRetrieve, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        String s = JSONObject.toJSONString(sysRetrieve);
        parameters.put("sysRetrieveEntity", s);
        parameters.put("sign", signature);
        Map<String,Object>  headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysRetrieve/addSysRetrieveForRemote", headers, parameters);
    }

    /**
     * 添加到回收站 list
     * @author oy
     * @date 2019/5/17
     * @param sysRetrieve, request
     * @return void
     */
    public static void addSysRetrieves(List<SysRetrieve> sysRetrieve, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String,Object> parameters = new HashMap<>();
        String s = JSONObject.toJSONString(sysRetrieve);
        parameters.put("sysRetrieveEntityList", s);
        parameters.put("sign", signature);
        Map<String,Object>  headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysRetrieve/addSysRetrievesForRemote", headers, parameters);
    }


}
