package com.tuoniaostore.commons.support.supplychain;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.supplychain.SupplychainGoodStockLogger;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description 库存记录远端调用接口
 * @date 2019/4/25
 */
public class SupplychainGoodStockLoggersRemoteService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(SupplyChainGoodRemoteService.class);

    /**
     * 功能描述
     * @author oy
     * @date 2019/4/25
     * @param supplychainGoodStockLoggers, httpServletRequest
     * @return void
     */
    public static void addSupplychainGoodStockLoggers(List<SupplychainGoodStockLogger> supplychainGoodStockLoggers, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        //填充参数
        //将list转成string
        String s = JSONObject.toJSONString(supplychainGoodStockLoggers);
        parameters.put("list", s);

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainGoodStockLogger/addSupplychainGoodStockLoggers", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
