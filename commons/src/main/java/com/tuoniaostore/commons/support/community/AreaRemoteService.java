package com.tuoniaostore.commons.support.community;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.commons.support.good.GoodTypeRemoteService;
import com.tuoniaostore.datamodel.user.SysArea;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liyunbiao on 2019/3/21.
 */
public class AreaRemoteService  extends BasicRemoteService {
    private static final Logger logger = Logger.getLogger(AreaRemoteService.class);
    public static SysArea getSysArea(String id, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");
        logger.error(appKey+"   "+signature+"   "+id);
        Map<String,Object> parameters = new HashMap<>();
        parameters.put("id", id);
        parameters.put("sign", signature);
        Map<String,Object>  headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysArea/getSysArea", headers, parameters);
        logger.error(response.getString("response"));
        return JSONObject.parseObject(response.getString("response"), SysArea.class);

    }

    public static List<SysArea> getSysAreasByIds(List<String> areaIds, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");
        Map<String,Object> parameters = new HashMap<>();
        String s = JSONObject.toJSONString(areaIds);
        parameters.put("ids", s);
        parameters.put("sign", signature);
        Map<String,Object>  headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysArea/getSysAreasByIds", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), SysArea.class);
    }


    public static List<SysArea> getSysAreaByNameLike(String areaName, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");
        Map<String,Object> parameters = new HashMap<>();

        parameters.put("areaName", areaName);

        parameters.put("sign", signature);
        Map<String,Object>  headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysArea/getSysAreasByNameLike", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseArray(response.getString("list"), SysArea.class);
    }
}
