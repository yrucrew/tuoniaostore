package com.tuoniaostore.commons.support.supplychain;

import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.datamodel.supplychain.SupplychainHomepageGoodsSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oy
 * @description
 * @date 2019/5/1
 */
public class SupplychainHomepageGoodsSettingRemoteService extends BasicRemoteService {

    private static final Logger logger = LoggerFactory.getLogger(SupplychainHomepageGoodsSettingRemoteService.class);

    public static List<SupplychainHomepageGoodsSetting> getHomePageGoodByAreaId(String areaId, int type,HttpServletRequest request){
        String accessToken = request.getHeader("accessToken");
        String signature = request.getParameter("sign");
        String appKey = request.getHeader("appKey");

        Map<String,Object> parameters = new HashMap<>();
        parameters.put("areaId", areaId);
        parameters.put("type", type+"");

        parameters.put("sign", signature);
        Map<String,Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);

        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.supplychainRemoteURL + "supplychainHomepageGoodsSetting/getHomePageGoodByAreaId", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("远程连接失败");
        }
        return JSONObject.parseArray(response.getString("list"), SupplychainHomepageGoodsSetting.class);

    }

}
