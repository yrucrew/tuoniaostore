package com.tuoniaostore.commons.support.community;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tuoniaostore.commons.config.CommonsUrlConfig;
import com.tuoniaostore.commons.support.BasicRemoteService;
import com.tuoniaostore.commons.support.good.GoodTypeRemoteService;
import com.tuoniaostore.datamodel.user.SysUser;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author oy
 * @email 1262014533@qq.com
 * @date 2019/3/19
 */
public class SysUserRemoteService extends BasicRemoteService {

    private static final Logger logger = Logger.getLogger(GoodTypeRemoteService.class);

    /**
     * 获取用户信息 通过id
     *
     * @param id, request
     * @return com.tuoniaostore.datamodel.user.SysUser
     * @author oy
     * @date 2019/5/9
     */
    public static SysUser getSysUser(String id, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);
        parameters.put("sign", signature);

        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUser/getSysUser", headers, parameters);
            return JSONObject.parseObject(response.getString("response"), SysUser.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据手机号码获取用户
     *
     * @param phoneNum, request
     * @return com.tuoniaostore.datamodel.user.SysUser
     * @author oy
     * @date 2019/5/9
     */
    public static SysUser getSysUserByPhoneNumForRemote(String phoneNum, HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("phoneNum", phoneNum);
        parameters.put("sign", signature);

        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
         JSONObject response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUser/getSysUserByPhoneNumForRemote", headers, parameters);
        return JSONObject.parseObject(response.getString("response"), SysUser.class);

    }

    /**
     * 通过ids查找对应的信息
     *
     * @param ids, request
     * @return java.util.List<com.tuoniaostore.datamodel.user.SysUser>
     * @author oy
     * @date 2019/5/9
     */
    public static List<SysUser> getSysUserInIds(List<String> ids, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        List<String> collect = ids.stream().distinct().collect(Collectors.toList());
        parameters.put("sign", signature);
        if (collect != null && collect.size() > 0) {
            String json = JSONObject.toJSONString(collect);
            parameters.put("ids", json);
        } else {
            return null;
        }
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUser/getSysUserInIds", headers, parameters);
            return JSONObject.parseArray(response.getString("response"), SysUser.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 添加用户
     *
     * @param sysUser, request
     * @return void
     * @author oy
     * @date 2019/5/10
     */
    public static void addSysUser(SysUser sysUser, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        String json = JSON.toJSONString(sysUser);
        parameters.put("sign", signature);
        parameters.put("userEntity", json);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        try {
            baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUser/addSysUserByRemote", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addSysUserByPos(SysUser sysUser, String terminal, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        String json = JSON.toJSONString(sysUser);
        parameters.put("sign", signature);
        parameters.put("userEntity", json);
        parameters.put("terminal", terminal);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        try {
            baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUser/addSysUserForPosByRemote", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * pos-门店管理-员工管理-编辑
     *
     * @param sysUser
     * @param request
     */
    public static void updateSysUserByPos(SysUser sysUser, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        String json = JSON.toJSONString(sysUser);
        parameters.put("sign", signature);
        parameters.put("userEntity", json);
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        try {
            baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUser/updateSysUserByPosByRemote", headers, parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<SysUser> getSysUserByPhoneNumLike(String phoneNum, HttpServletRequest request) {
        String accessToken = request.getHeader("accessToken");
        String appKey = request.getHeader("appKey");
        String signature = request.getParameter("sign");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("sign", signature);
        parameters.put("phoneNum", phoneNum);

        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", accessToken);
        headers.put("appKey", appKey);
        JSONObject response = null;
        try {
            response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUser/getSysUserByPhoneNumLike", headers, parameters);
            return JSONObject.parseArray(response.getString("response"), SysUser.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }
    public static List<SysUser> getSysUserByName(String name, HttpServletRequest request) {
        Map<String, Object> headers = new HashMap();
        headers.put("accessToken", request.getHeader("accessToken"));
        headers.put("appKey", request.getHeader("appKey"));
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("sign", request.getParameter("sign"));
        parameters.put("name", name);
        try {
            JSONObject response = baseHttpPost(CommonsUrlConfig.communityRemoteURL + "sysUser/getSysUserByName", headers, parameters);
            return JSONObject.parseArray(response.getString("response"), SysUser.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
