package com.tuoniaostore.commons.service;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Alex on 2014/12/12
 */
public class BaseParser {

	private static final Logger logger = LoggerFactory.getLogger(BaseParser.class);

	private static final char CHARKEY = 7;


//	private Set<String> properties =  new HashSet<>();
	private static Multimap<Class<?>, String> fieldMap = HashMultimap.create();


	public static boolean isPrimitiveOrWrapper(final Class<?> type) {
		if (type == null) {
			return false;
		}
		return type.isPrimitive() || wrapperPrimitiveMap.containsKey(type);
	}

	/**
	 * Maps primitive {@code Class}es to their corresponding wrapper {@code Class}.
	 */
	private static final Map<Class<?>, Class<?>> primitiveWrapperMap = new HashMap<Class<?>, Class<?>>();
	static {
		primitiveWrapperMap.put(Boolean.TYPE, Boolean.class);
		primitiveWrapperMap.put(Byte.TYPE, Byte.class);
		primitiveWrapperMap.put(Character.TYPE, Character.class);
		primitiveWrapperMap.put(Short.TYPE, Short.class);
		primitiveWrapperMap.put(Integer.TYPE, Integer.class);
		primitiveWrapperMap.put(Long.TYPE, Long.class);
		primitiveWrapperMap.put(Double.TYPE, Double.class);
		primitiveWrapperMap.put(Float.TYPE, Float.class);
		primitiveWrapperMap.put(Void.TYPE, Void.TYPE);
	}

	/**
	 * Maps wrapper {@code Class}es to their corresponding primitive types.
	 */
	private static final Map<Class<?>, Class<?>> wrapperPrimitiveMap = new HashMap<Class<?>, Class<?>>();
	static {
		for (final Class<?> primitiveClass : primitiveWrapperMap.keySet()) {
			final Class<?> wrapperClass = primitiveWrapperMap.get(primitiveClass);
			if (!primitiveClass.equals(wrapperClass)) {
				wrapperPrimitiveMap.put(wrapperClass, primitiveClass);
			}
		}
	}
	public static Class<?> primitiveToWrapper(final Class<?> cls) {
		Class<?> convertedClass = cls;
		if (cls != null && cls.isPrimitive()) {
			convertedClass = primitiveWrapperMap.get(cls);
		}
		return convertedClass;
	}



	public static <T> T parseFrom(Map<String, String[]> map, Class<T> clazz) {
//		Field[] fields = clazz.getDeclaredFields();
		try{
			List<Field> fields = getAllFieldsList(clazz);
			T instance = clazz.newInstance();
			for (Field field : fields) {
				String fieldName = field.getName();
				if (!fieldMap.containsEntry(clazz, fieldName)) {
					synchronized (clazz) {
						if (!fieldMap.containsEntry(clazz, fieldName)) {
							fieldMap.put(clazz, fieldName);
						}
					}
				}
				String[] rawValues = map.get(fieldName);
				if (rawValues == null || rawValues.length <= 0)
					continue;
				String values = rawValues[0];

				Class<?> fieldType = field.getType();
				if (fieldType != String.class && isBlank(values)) {
					continue;
				}

				field.setAccessible(true);

				Object value = null;
  				if (isIterable(fieldType)) {
//					values = values.replace(',', CHARKEY);
					String[] strings = values.split(String.valueOf(CHARKEY));
					value = signIterable(field, strings);
				} else if (isSimpleType(fieldType)) {
					value = signPrimitive(fieldType, values);
				} else
					throw new NotImplementedException(fieldName + " " + fieldType
							+ "此类型不能自动转换");
				setValue(field, instance, value);
			}
			return instance;
		}catch (Exception ex){
			ex.printStackTrace();
			return null;
		}

	}

	private static void setValue(Field field, final Object instance,
								 Object value) throws IllegalAccessException {
		field.set(instance, value);
//		BaseParser json = (BaseParser) instance;
//		json.properties.add(field.getName());
	}

	private static boolean isSimpleType(Class<?> fieldType) {
		return isPrimitiveOrWrapper(fieldType)
				|| fieldType == String.class || fieldType == Date.class;
	}

	private static Object signPrimitive(Class<?> fieldType, String rawValue)
			throws Exception {
		Object value = null;
		if (isBlank(rawValue)) {
			return null;
		}
		if (isPrimitiveOrWrapper(fieldType)) {
			Class<?> wrapperClazz = primitiveToWrapper(fieldType);
			try {
				value = MethodUtils.invokeStaticMethod(wrapperClazz, "valueOf", rawValue);
			} catch (Exception ex) {
				logger.info("数组内容类型不对,转换失败!" + rawValue);
				ex.printStackTrace();
			}
		} else if (fieldType == Date.class) {
			if (rawValue.matches("\\d+")) {
				long time = Long.valueOf(rawValue);
				value = new Date(time);
			} else {
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				value = format.parse(String.valueOf(rawValue));
			}
		} else {
			value = rawValue;
		}
		return value;
	}

	private static Object signIterable(Field field, String[] strings)
			throws Exception {
		Class<?> fieldType = field.getType();
		if (fieldType.isArray()) {
			Class<?> cpnType = fieldType.getComponentType();
			Object array = Array.newInstance(cpnType, strings.length);
			for (int i = 0; i < strings.length; i++) {
				Object primitive = signPrimitive(cpnType, strings[i]);
				if (null != primitive) {
					Array.set(array, i, primitive);
				}
			}
			return array;
		}
		Collection c = newCollection(fieldType);
		if (!(field.getGenericType() instanceof ParameterizedType)) {
			throw new NotImplementedException(field.getName() + " 泛型必须写上泛型的类型");
		}
		ParameterizedType ptType = (ParameterizedType) field.getGenericType();
		Type genericType = ptType.getActualTypeArguments()[0];
		for (String string : strings) {
			c.add(signPrimitive((Class) genericType, string.trim()));
		}
		return c;
	}

	private static Object signIterable(Field field, byte[] strings)
			throws Exception {
		Class<?> fieldType = field.getType();
		if (fieldType.isArray()) {
			Class<?> cpnType = fieldType.getComponentType();
			Object array = Array.newInstance(cpnType, strings.length);
			array = strings;
			return array;
		}
		return null;
	}

	private static <T> Collection<T> newCollection(Class<T> clazz) {
		if (clazz == List.class) {
			return new ArrayList<T>();
		} else if (clazz == Set.class) {
			return new HashSet<T>();
		} else
			throw new NotImplementedException(clazz.getSimpleName()
					+ " 此类型尚未实现自动转换");
	}

	private static boolean isIterable(Class<?> clazz) {
		return (clazz.isArray() || Collection.class.isAssignableFrom(clazz))
				&& !byte[].class.isAssignableFrom(clazz);
	}

	private static boolean isByte(Class<?> clazz) {
		return (clazz.isArray() || Collection.class.isAssignableFrom(clazz))
				&& byte[].class.isAssignableFrom(clazz);
	}



	/**
	 * 判断前端有没有设置 property这个属性
	 *
//	 * @param property
	 *            属性名称
	 * @return 前端有设置就返回true，否则false
	 * @IllegalStateException 若property不是java对象里面的属性，抛此异常（这种肯定是拼写错误，检查拼写是否正确）
	 */
//	public boolean hasProperty(String property) {
//		if (!fieldMap.containsEntry(this.getClass(), property)) {
//			throw new IllegalStateException(this.getClass() + " 类型没有此字段:"
//					+ property + " 拼写错误?");
//		}
//		return properties.contains(property);
//	}





	public static boolean isBlank(final CharSequence cs) {
		int strLen;
		if (cs == null || (strLen = cs.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if (Character.isWhitespace(cs.charAt(i)) == false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 排除 泛型,扩展性,或静态,或final类型
	 * @return
	 */
	private static boolean isTypeClass(Field property) {
		boolean isStatic = Modifier.isStatic(property.getModifiers());
		boolean isFinal = Modifier.isFinal(property.getModifiers());
		if(isStatic || isFinal){
			return true;
		}return false;
	}


	private static List<Field> getAllFieldsList(Class<?> cls) {
		final List<Field> allFields = new ArrayList<Field>();
		Class<?> currentClass = cls;
		while (currentClass != null) {
			final Field[] declaredFields = currentClass.getDeclaredFields();
			addAll(allFields, declaredFields);
			currentClass = currentClass.getSuperclass();
		}
		return allFields;
	}
	/**
	 * 将类型转换成map
	 * @param object
	 * @return
	 */
	public static Map<String, String> objectToMap(Object object) {
//		Field[] fields = FieldUtils.getAllFields(object.getClass());// klass.getFields();
		List<Field> fields = getAllFieldsList(object.getClass());
		Map<String, String> map = new HashMap<>();
		if(null == fields){
			return map;
		}
		for (Field field : fields) {
			try {
				field.setAccessible(true);
				String va = String.valueOf(field.get(object));
				map.put(field.getName(),va);
			} catch (Exception e) {
			}
		}
		return  map;
	}

	@SafeVarargs
	public static boolean addAll(Collection<Field> c, Field... elements) {
		boolean result = false;
		for (Field element : elements){
			if(isTypeClass(element)){
				continue;
			}
			result |= c.add(element);
		}
		return result;
	}



}