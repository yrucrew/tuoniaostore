//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tuoniaostore.commons.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class HttpClientTest extends BaseConfigTest {
    private static TreeMap<String, Object> map = new TreeMap();

    private static String http_env = "test";

    public static String setEnv(String env) {
        if(env != null && !"".equals(env)){
            http_env = env;
        }
        return http_env;
    }

    public static HttpHost getHttpProxy(String url){
        HttpHost proxy = null;
        try {
            //http://bopaiapp.com/json/cdn/address/get_address_ship_list?appid=1005&pageNo=1&pageSize=100&platform=1&userId=27&version=7046
            String hostname = "127.0.0.1";
            int port = 8080;
            //由于旧版本用seturl改变环境，兼容使用
//            String[]  str = requestURLBase.split(":");
//            hostname = str[1].substring(2);
//            port = Integer.parseInt(str[2]);
            if(http_env.equals("prod")){
                if(url.startsWith("supplychain") ){
                    hostname = "sc.0085.com";//供应链
                }else {
                    hostname = "db.0085.com";//其它模块
                }port = 80;
            }
            if(http_env.equals("stage")){
                if(url.startsWith("/supplychain") ){
                    hostname = "sc.0085.com";//供应链
                    port =8080;
                }else if(url.startsWith("/passport") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }else if(url.startsWith("/community") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }else if(url.startsWith("/order") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }else if(url.startsWith("/item") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }else if(url.startsWith("/logistics") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }else if(url.startsWith("/gis") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }else if(url.startsWith("/achieve") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }
            }
            if(http_env.equals("test")){
                if(url.startsWith("/supplychain") ){
                    hostname = "sc.0085.com";//供应链
                    port =8080;
                }else if(url.startsWith("/passport") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }else if(url.startsWith("/community") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }else if(url.startsWith("/order") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }else if(url.startsWith("/item") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }else if(url.startsWith("/logistics") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }else if(url.startsWith("/gis") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }else if(url.startsWith("/achieve") ) {
                    hostname = "db.0085.com";//其它模块
                    port =8080;
                }
            }
            if(http_env.equals("dev")){
                if(url.startsWith("/supplychain") ){
                    hostname = "127.0.0.1";//供应链
                    port =8080;
                }else if(url.startsWith("/passport") ) {
                    hostname = "127.0.0.1";//其它模块
                    port =8080;
                }else if(url.startsWith("/community") ) {
                    hostname = "127.0.0.1";//其它模块
                    port =8080;
                }else if(url.startsWith("/order") ) {
                    hostname = "127.0.0.1";//其它模块
                    port =8080;
                }else if(url.startsWith("/item") ) {
                    hostname = "127.0.0.1";//其它模块
                    port =8080;
                }else if(url.startsWith("/logistics") ) {
                    hostname = "127.0.0.1";//其它模块
                    port =8080;
                }else if(url.startsWith("/gis") ) {
                    hostname = "127.0.0.1";//其它模块
                    port =8080;
                }else if(url.startsWith("/achieve") ) {
                    hostname = "127.0.0.1";//其它模块
                    port =8080;
                }
            }
//            hostname = "bopaiapp.com";
//            port =80;
            hostname = "127.0.0.1";
            port =8080;
            proxy = new HttpHost(hostname,port,"HTTP");
            if(proxy == null){
                hostname = "127.0.0.1";
                port =8080;
                proxy =  new HttpHost(hostname,port,"HTTP");
            }
            return proxy;
        } catch (Exception e) {
            e.printStackTrace();
            proxy =  new HttpHost("127.0.0.1",8080,"HTTP");
        }
        return proxy;
    }

    public static void sslGet(String requestParam) {
        CloseableHttpClient httpclient = null;

        try {
            KeyStore e = KeyStore.getInstance(KeyStore.getDefaultType());
            FileInputStream instream = new FileInputStream(new File(sshKeyStorePath));

            try {
                e.load(instream, sshKeystorePass.toCharArray());
            } catch (CertificateException var61) {
                var61.printStackTrace();
            } finally {
                try {
                    instream.close();
                } catch (Exception var59) {
                    ;
                }

            }

            SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(e, new TrustSelfSignedStrategy()).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[]{"TLSv1"}, (String[])null, SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
            httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
            String httpgetparam = requestURLBase + requestURL + "?" + requestParam;
            System.out.println("executing request  url " + httpgetparam);
            HttpGet httpget = new HttpGet(httpgetparam);
            System.out.println("executing request" + httpget.getRequestLine());
            CloseableHttpResponse response = httpclient.execute(httpget);

            try {
                HttpEntity entity = response.getEntity();
                System.out.println("----------------------------------------");
                System.out.println(response.getStatusLine());
                if(entity != null) {
                    System.out.println("Response content length: " + entity.getContentLength());
                    System.out.println(EntityUtils.toString(entity));
                    EntityUtils.consume(entity);
                }
            } finally {
                response.close();
            }
        } catch (ParseException var63) {
            var63.printStackTrace();
        } catch (IOException var64) {
            var64.printStackTrace();
        } catch (KeyManagementException var65) {
            var65.printStackTrace();
        } catch (NoSuchAlgorithmException var66) {
            var66.printStackTrace();
        } catch (KeyStoreException var67) {
            var67.printStackTrace();
        } finally {
            if(httpclient != null) {
                try {
                    httpclient.close();
                } catch (IOException var58) {
                    var58.printStackTrace();
                }
            }

        }

    }

    public static String postForm(String requestURL, Map<Object, Object> map) {
        String result = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httppost = new HttpPost( requestURL);
        ArrayList formparams = new ArrayList();
        Iterator e = map.keySet().iterator();

        while(e.hasNext()) {
            Object uefEntity = e.next();
            String entity = uefEntity instanceof byte[]?new String((byte[])uefEntity):String.valueOf(uefEntity);
            String pv = map.get(uefEntity) instanceof byte[]?new String((byte[])map.get(uefEntity)):String.valueOf(map.get(uefEntity));
            formparams.add(new BasicNameValuePair(entity, pv));
        }

        String var10;
        try {
            UrlEncodedFormEntity uefEntity1 = new UrlEncodedFormEntity(formparams, "UTF-8");
            httppost.setEntity(uefEntity1);
            System.out.println("executing request " + httppost.getURI());
            HttpHost proxy = getHttpProxy(requestURL);
            CloseableHttpResponse e1 = httpclient.execute(proxy,httppost);

            try {
                HttpEntity entity1 = e1.getEntity();
                if(entity1 == null) {
                    return result;
                }

                System.out.println("--------------------------------------");
                result = EntityUtils.toString(entity1, "UTF-8");
                System.out.println("Response content: " + result);
                System.out.println("--------------------------------------");
                var10 = result;
            } finally {
                e1.close();
            }
        } catch (ClientProtocolException var32) {
            var32.printStackTrace();
            return result;
        } catch (UnsupportedEncodingException var33) {
            var33.printStackTrace();
            return result;
        } catch (IOException var34) {
            var34.printStackTrace();
            return result;
        } finally {
            try {
                httpclient.close();
            } catch (IOException var30) {
                var30.printStackTrace();
            }

        }

        return var10;
    }

    public static String post(String requestURL, Map<String, Object> map) {
        String result = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();

        HttpPost httppost = new HttpPost(requestURL);
        ArrayList formparams = new ArrayList();
        Iterator e = map.keySet().iterator();

        while(e.hasNext()) {
            Object uefEntity = e.next();
            String entity = uefEntity instanceof byte[]?new String((byte[])uefEntity):String.valueOf(uefEntity);
            String pv = map.get(uefEntity) instanceof byte[]?new String((byte[])map.get(uefEntity)):String.valueOf(map.get(uefEntity));
            formparams.add(new BasicNameValuePair(entity, pv));
        }

        try {
            UrlEncodedFormEntity uefEntity1 = new UrlEncodedFormEntity(formparams, "UTF-8");
            httppost.setEntity(uefEntity1);
            System.out.println("executing request start post  ====================================");
            System.out.println("executing request post url: " + httppost.getURI());
            System.out.println("executing request post param:  " + JSON.toJSONString(map));
            System.out.println("executing request end post  ====================================");
            HttpHost proxy = getHttpProxy(requestURL);
            CloseableHttpResponse e1 = httpclient.execute(proxy,httppost);

            try {
                HttpEntity entity1 = e1.getEntity();
                if(entity1 != null) {
                    System.out.println("------------ executing request post start --------------------------");
                    result = EntityUtils.toString(entity1, "UTF-8");
                    System.out.println("Response content: " + result);
                    System.out.println("------------ executing request post end --------------------------");
                }
            } finally {
                e1.close();
            }
        } catch (ClientProtocolException var31) {
            var31.printStackTrace();
        } catch (UnsupportedEncodingException var32) {
            var32.printStackTrace();
        } catch (IOException var33) {
            var33.printStackTrace();
        } finally {
            try {
                httpclient.close();
            } catch (IOException var29) {
                var29.printStackTrace();
            }

        }

        return result;
    }



    public static String get(String requestURL, String requestParam) {
        String result = null;

        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpHost proxy = getHttpProxy(requestURL);
            System.out.println("executing request start get  ====================================");
            String url = requestURL + "?" + requestParam;
            System.out.println("executing request url " +proxy.getHostName()+":"+proxy.getPort() + url);
            System.out.println("executing request end get  ====================================");
            HttpGet httprequest = new HttpGet(url);

            CloseableHttpResponse response = httpclient.execute(proxy,httprequest);

            try {
                HttpEntity entity = response.getEntity();
                System.out.println("-------- executing request get start ------------------------------");
                System.out.println(response.getStatusLine());
                if(entity != null) {
                    result = EntityUtils.toString(entity, "UTF-8");
                    System.out.println("Response content: " + result);
                }

                System.out.println("------------- executing request get end -----------------------");
            } finally {
                response.close();
            }
        } catch (ClientProtocolException var29) {
            var29.printStackTrace();
        } catch (ParseException var30) {
            var30.printStackTrace();
        } catch (IOException var31) {
            var31.printStackTrace();
        } finally {
            try {
                httpclient.close();
            } catch (IOException var27) {
                var27.printStackTrace();
            }

        }

        return result;
    }

//    public static void upload() {
//        CloseableHttpClient httpclient = HttpClients.createDefault();
//
//        try {
//            HttpPost e = new HttpPost("http://localhost:8080/myDemo/Ajax/serivceFile.action");
//            FileBody bin = new FileBody(new File("F:\\image\\sendpix0.jpg"));
//            StringBody comment = new StringBody("A binary file of some kind", ContentType.TEXT_PLAIN);
//            HttpEntity reqEntity = MultipartEntityBuilder.create().addPart("bin", bin).addPart("comment", comment).build();
//            e.setEntity(reqEntity);
//            System.out.println("executing request " + e.getRequestLine());
//            CloseableHttpResponse response = httpclient.execute(e);
//
//            try {
//                System.out.println("----------------------------------------");
//                System.out.println(response.getStatusLine());
//                HttpEntity resEntity = response.getEntity();
//                if(resEntity != null) {
//                    System.out.println("Response content length: " + resEntity.getContentLength());
//                }
//
//                EntityUtils.consume(resEntity);
//            } finally {
//                response.close();
//            }
//        } catch (ClientProtocolException var25) {
//            var25.printStackTrace();
//        } catch (IOException var26) {
//            var26.printStackTrace();
//        } finally {
//            try {
//                httpclient.close();
//            } catch (IOException var23) {
//                var23.printStackTrace();
//            }
//
//        }
//
//    }



    private static void coventBodyMap(Object o) {
        if(o != null && !"".equals(o)) {
            String json = JSON.toJSONString(o);// JsonUtils.toJsonString(o);
            TreeMap map1 = (TreeMap)JSON.parseObject(json, TreeMap.class);
            if(map1 != null) {
                map.putAll(map1);
            }

        }
    }

    private static TreeMap<String, Object> postHearTreeMap() {
        TokenData tokenData = new TokenData();
        tokenData.setUserid(userId);
        tokenData.setToken(token);
        TreeMap parametersMap = new TreeMap();
        parametersMap.put("platform", Integer.valueOf(1));
        parametersMap.put("version", Integer.valueOf(7046));
        parametersMap.put("userId", Integer.valueOf(tokenData.getUserid()));
        parametersMap.put("token", tokenData.getToken());
        parametersMap.put("device", "eb7bc204c5f3f534afa76c481aafa4143693f9a8a13f25760905cd333755240e");
        parametersMap.put("times", Long.valueOf(System.currentTimeMillis()));
        map.putAll(parametersMap);
        return parametersMap;
    }

    private static TreeMap<String, Object> getHeadTreeMap() {
        TreeMap parametersMap = new TreeMap();
        parametersMap.put("platform", Integer.valueOf(1));
        parametersMap.put("version", Integer.valueOf(7046));
        parametersMap.put("appid", Integer.valueOf(1005));
        map.putAll(parametersMap);
        return parametersMap;
    }

    private static String covert() {
        StringBuilder sb = new StringBuilder();
        Iterator var2 = map.entrySet().iterator();

        while(var2.hasNext()) {
            Entry len = (Entry)var2.next();
            sb.append((String)len.getKey());
            sb.append("=");
            sb.append(len.getValue());
            sb.append("&");
        }

        int len1 = sb.length();
        sb.delete(len1 - 1, len1);
        return sb.toString();
    }

    public static String postURL(String url, Object o) {
        postHearTreeMap();
        coventBodyMap(o);
        String content = covert();
        System.out.println("post url =:" + content);
        return post(url, map);
    }

    public static String getURL(String url, Object o) {
        getHeadTreeMap();
        coventBodyMap(o);
        String content = covert();
        System.out.println("get url =:" + content);
        return get(url, content);
    }

    public static void main(String[] agr){
        JSONObject object = new JSONObject();
        object.put("appid",1005);
        object.put("pageNo",1);
        object.put("pageSize",100);
        object.put("platform",1);
        object.put("userId",27);
        object.put("version",7046);
       // /json/cdn/address/get_address_ship_list?appid=1005&pageNo=1&pageSize=100&platform=1&userId=27&version=7046
        getURL("/json/cdn/address/get_address_ship_list",object);
    }
}
